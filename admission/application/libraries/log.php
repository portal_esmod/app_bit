<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
	class Log
	{
		var $CI = null;
		function Log()
		{
			$this->CI =& get_instance();
			$this->CI->load->database();
		}
		
		function getIpAdress()
		{
			if(!empty($_SERVER['HTTP_CLIENT_IP']))
			{
				$ip=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
		
		function getBrowser()
		{
			$useragent = $_SERVER ['HTTP_USER_AGENT'];
			if(strpos($useragent,"Firefox"))
			{
				$browser="Mozilla Firefox";
			}
			else
				if(strpos($useragent,"Chrome"))
				{
					$browser="Google Chrome";
				}
				else
					if(strpos($useragent,"Opera"))
					{
						$browser="Opera";
					}
					else
						if(strpos($useragent,"MSIE"))
						{
							$browser="Internet Explorer";
						}
						else
							if(strpos($useragent,"SeaMonkey"))
							{
								$browser="Sea Monkey";
							}
							else
								if(strpos($useragent,"Flock"))
								{
									$browser="Flock";
								}
								else
									if(strpos($useragent,"Safari"))
									{
										$browser="Safari";
									}
									else
										if(strpos($useragent,"Orca"))
										{
											$browser="Orca";
										}
			return $browser;
		}
		
		function getOs()
		{
			if (@ereg("Windows NT 5.1", $_SERVER["HTTP_USER_AGENT"])){
				$os="Windows eXPerience(XP)";
			}elseif(@ereg("Windows",$_SERVER["HTTP_USER_AGENT"])){
				$os="Windows"; 
			}elseif((@ereg("Mac",$_SERVER["HTTP_USER_AGENT"]))||(@ereg("PPC",$_SERVER["HTTP_USER_AGENT"]))){
				$os="MacOS";
			}elseif(@ereg("Linux",$_SERVER["HTTP_USER_AGENT"])){
				$os="Linux";
			}elseif(@ereg("FreeBSD",$_SERVER["HTTP_USER_AGENT"])){
				$os="FreeBSD";
			}elseif(@ereg("SunOS",$_SERVER["HTTP_USER_AGENT"])){
				$os="SunOS";
			}elseif(@ereg("IRIX",$_SERVER["HTTP_USER_AGENT"])){
				$os="IRIX";
			}elseif(@ereg("BeOS",$_SERVER["HTTP_USER_AGENT"])){
				$os="BeOS";
			}elseif(@ereg("OS/2",$_SERVER["HTTP_USER_AGENT"])){
				$os="OS/2";
			}elseif(@ereg("AIX",$_SERVER["HTTP_USER_AGENT"])){
				$os="AIX";
			}else{
				$os="Sistem Operasi Belum Dikenal";
			}
			return $os;
		}
		
		function getPerangkat()
		{
			$mystring = $_SERVER['HTTP_USER_AGENT'];
			$findme   = 'Android';
			$pos = strpos(strtolower($mystring), strtolower($findme));
			$ismobile = false;
			if ($pos !== false)
			{
				$ismobile = true;
			}
			else
			{
				//echo "The string '$findme' was not found in the string '$mystring'";
			}
			$findme   = 'iPhone';
			$pos = strpos(strtolower($mystring), strtolower($findme));
			if ($pos !== false)
			{
				$ismobile = true;
			}
			$findme   = 'Mobile Safari';
			$pos = strpos(strtolower($mystring), strtolower($findme));
			if ($pos !== false)
			{
				$ismobile = true;
			}
			$findme   = 'Blackberry';
			$pos = strpos(strtolower($mystring), strtolower($findme));
			if ($pos !== false)
			{
				$ismobile = true;
			}
			$findme   = 'MeeGo';
			$pos = strpos(strtolower($mystring), strtolower($findme));
			if ($pos !== false)
			{
				$ismobile = true;
			}
			if ($ismobile == true)
			{
				$perangkat = "Mobile";
			}
			else
			{
				$perangkat = "Desktop";
			}
			return $perangkat;
		}
	}
?>