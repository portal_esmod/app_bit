<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Entry extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_entry');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_entry->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_entry',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_entry_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'StatusAwalID' => $this->input->post('StatusAwalID'),
						'Nama' => $this->input->post('Nama'),
						'BeliFormulir' => $this->input->post('BeliFormulir'),
						'JalurKhusus' => $this->input->post('JalurKhusus'),
						'TanpaTest' => $this->input->post('TanpaTest'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA')
						);
			$this->m_entry->PTL_insert($data);
			echo warning("Data ENTRY successfully added.","../entry");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$StatusAwalID = $this->uri->segment(3);
			$result = $this->m_entry->PTL_select($StatusAwalID);
			$data['StatusAwalID'] = $result['StatusAwalID'];
			$data['Nama'] = $result['Nama'];
			$data['BeliFormulir'] = $result['BeliFormulir'];
			$data['JalurKhusus'] = $result['JalurKhusus'];
			$data['TanpaTest'] = $result['TanpaTest'];
			$data['Catatan'] = $result['Catatan'];
			$data['NA'] = $result['NA'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_entry_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$StatusAwalID = $this->input->post('StatusAwalID');
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'BeliFormulir' => $this->input->post('BeliFormulir'),
						'JalurKhusus' => $this->input->post('JalurKhusus'),
						'TanpaTest' => $this->input->post('TanpaTest'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA')
						);
			$this->m_entry->PTL_update($StatusAwalID,$data);
			echo warning("Data ENTRY with CODE '".$StatusAwalID."' successfully changed.","../entry");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$StatusAwalID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_entry->PTL_update($StatusAwalID,$data);
			echo warning("Data ENTRY with CODE '".$StatusAwalID."' successfully deleted.","../entry");
		}
	}
?>