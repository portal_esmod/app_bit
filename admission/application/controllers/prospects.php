<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Prospects extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_lead');
			$this->load->model('m_mail');
			$this->load->model('m_pendidikanortu');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_sumberinfo');
			$this->load->model('m_maintenance');
			$this->load->model('m_mix');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup_kota()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mix->lookup_kota($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->KotaID,
											'value' => $row->KotaKabupaten,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('prospects/index',$data);
			}
		}
		
		function lookup_propinsi()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mix->lookup_propinsi($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->PropinsiID,
											'value' => $row->Propinsi,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('prospects/index',$data);
			}
		}
		
		function lookup_agama()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mix->lookup_agama($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->Agama,
											'value' => $row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('prospects/index',$data);
			}
		}
		
		function lookup_sekolah()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mix->lookup_sekolah($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->id_sekolah,
											'value' => $row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('prospects/index',$data);
			}
		}
		
		function ptl_filter_pros_periode()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('pros_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('pros_filter_periode','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('pros_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('pros_filter_marketing','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			if($cektype != "")
			{
				$this->session->set_userdata('pros_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('pros_filter_type','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			if($cekstatus != "")
			{
				$this->session->set_userdata('pros_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('pros_filter_status','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_program()
		{
			$this->authentification();
			$cekpogram = $this->input->post('cekpogram');
			if($cekpogram != "")
			{
				$this->session->set_userdata('pros_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('pros_filter_program','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_no()
		{
			$this->authentification();
			$cekno = $this->input->post('cekno');
			if($cekno != "")
			{
				$this->session->set_userdata('pros_filter_no',$cekno);
			}
			else
			{
				$this->session->unset_userdata('pros_filter_no');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('pros_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('pros_filter_tahun','');
			}
			redirect("prospects");
		}
		
		function ptl_filter_pros_month()
		{
			$this->authentification();
			$cekbulan = $this->input->post('cekbulan');
			if($cekbulan != "")
			{
				$this->session->set_userdata('pros_filter_month',$cekbulan);
			}
			else
			{
				$this->session->set_userdata('pros_filter_month','');
			}
			redirect("prospects");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			if($this->session->userdata('pros_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('pros_filter_no');
			}
			$uri_segment = 3;
			$offset = $this->uri->segment($uri_segment);
			$data['cariproduk'] = $this->m_aplikan->PTL_all_cari_all($cekno,$offset);
			$jml = $this->m_aplikan->PTL_all_cari_jumlah_all();
			$data['cek'] = $this->m_aplikan->PTL_all_cari_cek_all();
			$data['total'] = count($this->m_aplikan->PTL_all_cari_total_all());
			$this->load->library('pagination');
			$config['base_url'] = site_url("prospects/index");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "prospects/index";
			
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$data['pencarian'] = '';
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Prospects/v_prospects',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function search($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			if($this->session->userdata('pros_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('pros_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_aplikan->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_aplikan->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_aplikan->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_aplikan->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("prospects/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "prospects/search/$cari";
			
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Prospects/v_prospects',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			$id_lead = $this->uri->segment(3);
			$data['cekreadonly'] = '';
			if($id_lead != "")
			{
				$data['cekreadonly'] = 'readonly';
				$result = $this->m_lead->FHM_select($id_lead);
				$ses_data = array(
								'pros_id_lead' => $result['id_lead'],
								'pros_kode_lead' => $result['kode_lead'],
								'pros_Nama' => strtoupper($result['nama']),
								'pros_Handphone' => $result['mobile1'],
								'pros_Email' => strtolower($result['email1']),
								'pros_PresenterID' => strtoupper($result['PresenterID'])
								);
				$this->session->set_userdata($ses_data);
			}
			$data['marketing'] = $this->m_akses->PTL_all_marketing_active();
			$data['pmbperiod'] = $this->m_pmbperiod->PTL_all_active();
			$data['ortu'] = $this->m_pendidikanortu->PTL_all();
			$data['d3'] = $this->m_prodi->PTL_all();
			$data['d1'] = $this->m_prodi->PTL_all_d1();
			$data['kursussingkat'] = $this->m_kursussingkat->PTL_all();
			$data['sumberinfo'] = $this->m_sumberinfo->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form_applicant',$hdata);
			$this->load->view('Prospects/v_prospects_form',$data);
			$this->load->view('Portal/v_footer_form_applicant');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$no = $this->input->post('no');
			for($i=0;$i<$no;$i++)
			{
				if($this->input->post("SumberInformasi$i")!=""){@$SumberInformasi.=$this->input->post("SumberInformasi$i")."_";}else{@$SumberInformasi.="";}
			}
			$ses_data = array(
							'pros_id_lead' => $this->input->post('id_lead'),
							'pros_kode_lead' => $this->input->post('kode_lead'),
							'pros_PMBPeriodID' => $this->input->post('PMBPeriodID'),
							'pros_nomor_identitas' => strtoupper($this->input->post('nomor_identitas')),
							'pros_Nama' => strtoupper($this->input->post('Nama')),
							'pros_Kelamin' => strtoupper($this->input->post('Kelamin')),
							'pros_GolonganDarah' => strtoupper($this->input->post('GolonganDarah')),
							'pros_WargaNegara' => strtoupper($this->input->post('WargaNegara')),
							'pros_Kebangsaan' => strtoupper($this->input->post('Kebangsaan')),
							'pros_TempatLahir' => strtoupper($this->input->post('TempatLahir')),
							'pros_TanggalLahir' => $this->input->post('TanggalLahir'),
							'pros_umur' => $this->input->post('umur'),
							'pros_Agama' => strtoupper($this->input->post('Agama')),
							'pros_TinggiBadan' => $this->input->post('TinggiBadan'),
							'pros_BeratBadan' => $this->input->post('BeratBadan'),
							'pros_Alamat' => strtoupper($this->input->post('Alamat')),
							'pros_Kota' => strtoupper($this->input->post('Kota')),
							'pros_RT' => $this->input->post('RT'),
							'pros_RW' => $this->input->post('RW'),
							'pros_KodePos' => $this->input->post('KodePos'),
							'pros_Propinsi' => strtoupper($this->input->post('Propinsi')),
							'pros_Negara' => strtoupper($this->input->post('Negara')),
							'pros_Telepon' => $this->input->post('Telepon'),
							'pros_Handphone' => $this->input->post('Handphone'),
							'pros_Email' => strtolower($this->input->post('Email')),
							'pros_PendidikanTerakhir' => strtoupper($this->input->post('PendidikanTerakhir')),
							'pros_AsalSekolah' => strtoupper($this->input->post('AsalSekolah')),
							'pros_KotaAsalSekolah' => strtoupper($this->input->post('KotaAsalSekolah')),
							'pros_JurusanSekolah' => strtoupper($this->input->post('JurusanSekolah')),
							'pros_TahunLulus' => $this->input->post('TahunLulus'),
							'pros_NilaiSekolah' => $this->input->post('NilaiSekolah'),
							'pros_SudahBekerja' => $this->input->post('SudahBekerja'),
							'pros_NamaAyah' => strtoupper($this->input->post('NamaAyah')),
							'pros_AgamaAyah' => strtoupper($this->input->post('AgamaAyah')),
							'pros_PendidikanAyah' => strtoupper($this->input->post('PendidikanAyah')),
							'pros_PekerjaanAyah' => strtoupper($this->input->post('PekerjaanAyah')),
							'pros_AlamatOrtu' => strtoupper($this->input->post('AlamatOrtu')),
							'pros_KotaOrtu' => strtoupper($this->input->post('KotaOrtu')),
							'pros_RTOrtu' => $this->input->post('RTOrtu'),
							'pros_RWOrtu' => $this->input->post('RWOrtu'),
							'pros_KodePosOrtu' => strtoupper($this->input->post('KodePosOrtu')),
							'pros_PropinsiOrtu' => strtoupper($this->input->post('PropinsiOrtu')),
							'pros_NegaraOrtu' => strtoupper($this->input->post('NegaraOrtu')),
							'pros_TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'pros_HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'pros_EmailOrtu' => strtolower($this->input->post('EmailOrtu')),
							'pros_NamaIbu' => strtoupper($this->input->post('NamaIbu')),
							'pros_AgamaIbu' => strtoupper($this->input->post('AgamaIbu')),
							'pros_PendidikanIbu' => strtoupper($this->input->post('PendidikanIbu')),
							'pros_PekerjaanIbu' => strtoupper($this->input->post('PekerjaanIbu')),
							'pros_TeleponIbu' => $this->input->post('TeleponIbu'),
							'pros_HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'pros_EmailIbu' => strtolower($this->input->post('EmailIbu')),
							'pros_Harga' => $this->input->post('Harga'),
							'pros_StatusProspekID' => strtoupper($this->input->post('StatusProspekID')),
							'pros_NilaiUjian' => $this->input->post('NilaiUjian'),
							'pros_PresenterID' => strtoupper($this->input->post('PresenterID')),
							'pros_CatatanPresenter' => $this->input->post('CatatanPresenter'),
							'pros_Catatan' => strtoupper($this->input->post('Catatan')),
							'pros_SumberInformasi' => $SumberInformasi,
							'pros_NA' => $this->input->post('NA'),
							'pros_SudahBayar' => $this->input->post('SudahBayar'),
							'pros_LulusUjian' => $this->input->post('LulusUjian'),
							'pros_SyaratLengkap' => $this->input->post('SyaratLengkap'),
							'pros_diterima' => $this->input->post('diterima'),
							'pros_total_biaya' => $this->input->post('total_biaya'),
							'pros_total_bayar' => $this->input->post('total_bayar'),
							'pros_postpone' => $this->input->post('postpone')
							);
			$this->session->set_userdata($ses_data);
			
			$PMBPeriodID = $this->input->post('PMBPeriodID');
			$result2 = $this->m_pmbperiod->PTL_select($PMBPeriodID);
			
			$kd = $result2['FormatNoAplikan'];
			$result = $this->m_aplikan->PTL_urut_aplikan($kd);
			$lastid = $result['LAST'];
			$lastnourut = substr($lastid,7,4);
			$nextnourut = $lastnourut + 1;
			$nextid = $kd.sprintf('%0'.$result2['DigitNoAplikan'].'s', $nextnourut);
			
			$kd2 = $result2['FormatNoPMB'];
			$result22 = $this->m_aplikan->PTL_urut_pmb2($kd2);
			$lastid22 = $result22['LAST'];
			$lastnourut22 = substr($lastid22,7,4);
			$nextnourut22 = $lastnourut22 + 1;
			$nextid22 = $kd2.sprintf('%0'.$result2['DigitNoPMB'].'s', $nextnourut22);
			
			$Pilihan1 = $this->input->post('Pilihan1');
			$Pilihan2 = $this->input->post('Pilihan2');
			$Pilihan3 = $this->input->post('Pilihan3');
			$Pilihan4 = $this->input->post('Pilihan4');
			$prog = "";
			$NamaProdi1 = "";
			$NamaProdi2 = "";
			$NamaProdi3 = "";
			if(($Pilihan1 != "") AND ($Pilihan1 != "0"))
			{
				$d3 = $this->m_prodi->PTL_all();
				if($d3)
				{
					foreach($d3 as $rowd3)
					{
						if($Pilihan1 == $rowd3->ProdiID)
						{
							$pil = $rowd3->ProdiID;
							$NamaProdi1 = $rowd3->Nama;
							$prog = "REG";
						}
					}
				}
			}
			if(($Pilihan2 != "") AND ($Pilihan2 != "0"))
			{
				$d1 = $this->m_prodi->PTL_all_d1();
				if($d1)
				{
					foreach($d1 as $rowd1)
					{
						if($Pilihan2 == $rowd1->ProdiID)
						{
							$pil = $rowd1->ProdiID;
							$NamaProdi2 = $rowd1->Nama;
							$prog = "INT";
						}
					}
				}
			}
			if(($Pilihan3 != "") AND ($Pilihan3 != "0"))
			{
				$kursussingkat = $this->m_kursussingkat->PTL_all();
				if($kursussingkat)
				{
					foreach($kursussingkat as $rowks)
					{
						if($Pilihan3 == $rowks->KursusSingkatID)
						{
							$pil = $rowks->KursusSingkatID;
							$NamaProdi3 = $rowks->Nama;
							$prog = "SC";
						}
					}
				}
			}
			if(($Pilihan4 != "") AND ($Pilihan4 != "0"))
			{
				$pil = $Pilihan4;
				if($Pilihan4 == "MAS")
				{
					$NamaProdi4 = "MAS";
					$prog = "MAS";
				}
				else
				{
					$NamaProdi4 = $Pilihan4;
					$prog = "SPC";
				}
			}
			$password = gmdate("iHs", time()-($ms));
			$pesan = "";
			if($result2['FormatNoAplikan'] == "")
			{
				$pesan .= 'APL Format not set at Menu Setup Admission --> Admission Period --> PMB ID '.$this->input->post('PMBPeriodID').'.\n\n';
			}
			if($result2['DigitNoAplikan'] == "")
			{
				$pesan .= 'APL Digit not set at Menu Setup Admission --> Admission Period --> PMB ID '.$this->input->post('PMBPeriodID').'.\n\n';
			}
			if($result2['FormatNoPMB'] == "")
			{
				$pesan .= 'PMB Format not set at Menu Setup Admission --> Admission Period --> PMB ID '.$this->input->post('PMBPeriodID').'.\n\n';
			}
			if($result2['DigitNoPMB'] == "")
			{
				$pesan .= 'PMB Digit not set at Menu Setup Admission --> Admission Period --> PMB ID '.$this->input->post('PMBPeriodID').'.\n\n';
			}
			if($prog == "")
			{
				$pesan .= 'Program not selected.\n\n';
			}
			if($this->input->post('NA') == "")
			{
				$pesan .= 'Already Register not selected.\n\n';
			}
			if($this->input->post('SudahBayar') == "")
			{
				$pesan .= 'Already Payment Form not selected.\n\n';
			}
			if($this->input->post('LulusUjian') == "")
			{
				$pesan .= 'Already Test not selected.\n\n';
			}
			if($this->input->post('SyaratLengkap') == "")
			{
				$pesan .= 'Already Completeness File not selected.\n\n';
			}
			if($this->input->post('diterima') == "")
			{
				$pesan .= 'Already Payment 1st Semester not selected.\n\n';
			}
			if($this->input->post('postpone') == "")
			{
				$pesan .= 'Postpone not selected.\n\n';
			}
			$PMBPeriodID = $this->input->post('PMBPeriodID');
			$Nama = strtoupper($this->input->post('Nama'));
			$TanggalLahir = $this->input->post('TanggalLahir');
			$resduplicate = $this->m_aplikan->PTL_cek_duplicate($PMBPeriodID,$Nama,$TanggalLahir,$Pilihan1,$Pilihan2,$Pilihan3);
			if($resduplicate)
			{
				$pesan .= 'Data already exist.\n\n';
			}
			if($pesan != "")
			{
				$Copy = $this->input->post('Copy');
				if($Copy == "YES")
				{
					$CopyAplikan = $this->input->post('CopyAplikan');
					echo warning($pesan,"../prospects/ptl_copy/$CopyAplikan");
				}
				else
				{
					echo warning($pesan,"../prospects/ptl_form");
				}
			}
			else
			{
				$id_akun = $this->input->post('PresenterID');
				$resakses = $this->m_akun->PTL_select($id_akun);
				$NamaMarketing = '';
				if($resakses)
				{
					$NamaMarketing = $resakses['nama'];
				}
				$data = array(
							'AplikanID' => $nextid,
							'id_lead' => $this->input->post('id_lead'),
							'kode_lead' => $this->input->post('kode_lead'),
							'ProgramID' => $prog,
							'ProdiID' => $pil,
							'StatusAplikanID' => 'MKT',
							'PMBID' => $nextid22,
							'PMBPeriodID' => $this->input->post('PMBPeriodID'),
							'SudahBayar' => $this->input->post('SudahBayar'),
							'LulusUjian' => $this->input->post('LulusUjian'),
							'SyaratLengkap' => $this->input->post('SyaratLengkap'),
							'diterima' => $this->input->post('diterima'),
							'status_awal' => 'B',
							'nomor_identitas' => $this->input->post('nomor_identitas'),
							'Nama' => strtoupper($this->input->post('Nama')),
							'Kelamin' => strtoupper($this->input->post('Kelamin')),
							'GolonganDarah' => strtoupper($this->input->post('GolonganDarah')),
							'WargaNegara' => strtoupper($this->input->post('WargaNegara')),
							'Kebangsaan' => strtoupper($this->input->post('Kebangsaan')),
							'TempatLahir' => strtoupper($this->input->post('TempatLahir')),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'umur' => $this->input->post('umur'),
							'Agama' => strtoupper($this->input->post('Agama')),
							'TinggiBadan' => $this->input->post('TinggiBadan'),
							'BeratBadan' => $this->input->post('BeratBadan'),
							'Alamat' => strtoupper($this->input->post('Alamat')),
							'Kota' => strtoupper($this->input->post('Kota')),
							'RT' => $this->input->post('RT'),
							'RW' => $this->input->post('RW'),
							'KodePos' => $this->input->post('KodePos'),
							'Propinsi' => strtoupper($this->input->post('Propinsi')),
							'Negara' => strtoupper($this->input->post('Negara')),
							'Telepon' => $this->input->post('Telepon'),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => strtolower($this->input->post('Email')),
							'PendidikanTerakhir' => strtoupper($this->input->post('PendidikanTerakhir')),
							'AsalSekolah' => strtoupper($this->input->post('AsalSekolah')),
							'KotaAsalSekolah' => strtoupper($this->input->post('KotaAsalSekolah')),
							'JurusanSekolah' => strtoupper($this->input->post('JurusanSekolah')),
							'TahunLulus' => $this->input->post('TahunLulus'),
							'NilaiSekolah' => $this->input->post('NilaiSekolah'),
							'SudahBekerja' => $this->input->post('SudahBekerja'),
							'NamaAyah' => strtoupper($this->input->post('NamaAyah')),
							'AgamaAyah' => strtoupper($this->input->post('AgamaAyah')),
							'PendidikanAyah' => strtoupper($this->input->post('PendidikanAyah')),
							'PekerjaanAyah' => strtoupper($this->input->post('PekerjaanAyah')),
							'AlamatOrtu' => strtoupper($this->input->post('AlamatOrtu')),
							'KotaOrtu' => strtoupper($this->input->post('KotaOrtu')),
							'RTOrtu' => $this->input->post('RTOrtu'),
							'RWOrtu' => $this->input->post('RWOrtu'),
							'KodePosOrtu' => strtoupper($this->input->post('KodePosOrtu')),
							'PropinsiOrtu' => strtoupper($this->input->post('PropinsiOrtu')),
							'NegaraOrtu' => strtoupper($this->input->post('NegaraOrtu')),
							'TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'EmailOrtu' => strtolower($this->input->post('EmailOrtu')),
							'NamaIbu' => strtoupper($this->input->post('NamaIbu')),
							'AgamaIbu' => strtoupper($this->input->post('AgamaIbu')),
							'PendidikanIbu' => strtoupper($this->input->post('PendidikanIbu')),
							'PekerjaanIbu' => strtoupper($this->input->post('PekerjaanIbu')),
							'TeleponIbu' => $this->input->post('TeleponIbu'),
							'HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'EmailIbu' => strtolower($this->input->post('EmailIbu')),
							'Pilihan1' => $Pilihan1,
							'Pilihan2' => $Pilihan2,
							'Pilihan3' => $Pilihan3,
							'Pilihan4' => $Pilihan4,
							'TahunKe' => $this->input->post('TahunKe'),
							'Harga' => $this->input->post('Harga'),
							'ujian' => '1',
							'StatusProspekID' => strtoupper($this->input->post('StatusProspekID')),
							'NilaiUjian' => $this->input->post('NilaiUjian'),
							'PresenterID' => $id_akun,
							'CatatanPresenter' => $this->input->post('CatatanPresenter'),
							'Catatan' => strtoupper($this->input->post('Catatan')),
							'SumberInformasi' => $SumberInformasi,
							'NA' => $this->input->post('NA'),
							'total_biaya' => $this->input->post('total_biaya'),
							'total_bayar' => $this->input->post('total_bayar'),
							'postpone' => $this->input->post('postpone'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->input->post('TanggalBuat')
							);
				$this->m_aplikan->PTL_insert($data);
				$dataa = array(
							'id_akun' => $nextid,
							'nama' => strtoupper($this->input->post('Nama')),
							'username' => strtolower($this->input->post('Email')),
							'password' => $password,
							'divisi' => 'ACADEMIC',
							'jabatan' => 'STUDENT',
							'akses' => 'STUDENT',
							'aplikasi' => 'al_',
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => gmdate("Y-m-d H:i:s", time()-($ms))
							);
				$this->m_akses->PTL_insert($dataa);
				$da = array(
							'id_akun' => $nextid,
							'nama' => strtoupper($this->input->post('Nama')),
							'aktivitas' => 'REGISTRATION: Registration successful'
							);
				$this->m_aplikan_log->PTL_insert($da);
				
				$error = array();
				$gambar = array();
				$no = 1;
				foreach($_FILES as $field_name => $file)
				{
					$h = "-7";
					$hm = $h * 60;
					$ms = $hm * 60;
					$storage = gmdate("Y-m", time()-($ms));
					$tgl = gmdate("ymdHis", time()-($ms));
					$download = $file['name'];
					$downloadin = $tgl."_".str_replace(' ','_',$download);
					if($no == 1)
					{
						$uploadFile = '../applicant/ptl_storage/admisi/aplikan/'.$storage;
						if(!is_dir($uploadFile))
						{
							mkdir($uploadFile, DIR_WRITE_MODE);
						}
					}
					if($no == 2)
					{
						$uploadFile = '../applicant/ptl_storage/admisi/bukti_setor/'.$storage;
						if(!is_dir($uploadFile))
						{
							mkdir($uploadFile, DIR_WRITE_MODE);
						}
					}
					if($no == 3)
					{
						$uploadFile = '../applicant/ptl_storage/admisi/ujian/'.$storage;
						if(!is_dir($uploadFile))
						{
							mkdir($uploadFile, DIR_WRITE_MODE);
						}
					}
					if($no == 4)
					{
						$uploadFile = '../applicant/ptl_storage/admisi/syarat/'.$storage;
						if(!is_dir($uploadFile))
						{
							mkdir($uploadFile, DIR_WRITE_MODE);
						}
					}
					if($no == 5)
					{
						$uploadFile = '../applicant/ptl_storage/admisi/pembayaran/'.$storage;
						if(!is_dir($uploadFile))
						{
							mkdir($uploadFile, DIR_WRITE_MODE);
						}
					}
					$config['upload_path'] = $uploadFile;
					$config['allowed_types'] = '*';
					$config['max_size'] = '1024';
					$config['max_width'] = '1024';
					$config['max_height'] = '768';
					$config['remove_spaces'] = true;
					$config['overwrite'] = false;
					$config['file_name'] = $downloadin;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload($field_name))
					{
						$error[] = $this->upload->display_errors();
						$gambar[] = '';
					}
					else
					{ 
						$gambar0 = $this->upload->data();
						$gambar[] = $gambar0['file_name'];
					}
					$no++;
				}
				$this->m_aplikan->PTL_insert_file($nextid,$gambar,$gambar,$gambar,$gambar,$gambar);
				
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				
				$AplEmail = strtolower($this->input->post('Email'));
				
				$EmailAyah = strtolower($this->input->post('EmailOrtu'));
				$EmailIbu = strtolower($this->input->post('EmailIbu'));
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to($AplEmail);
				$this->email->cc("$EmailAyah,$EmailIbu");
				$this->email->bcc("$resakun[email],$resakun[email2],anita@esmodjakarta.com,ichaa@esmodjakarta.com,miha@esmodjakarta.com,");
				// $this->email->bcc("lendra.permana@gmail.com");
				$this->email->subject('REGISTRATION SUCCESSFUL (NO REPLY)');
				$message = "
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ADMISSION</h2></font>
					</center>
					<h1>REGISTRATION SUCCESSFUL!</h1>
					<br/>
					<br/>
					Dear ".strtoupper($this->input->post('Nama')).",
					<br/>
					<br/>
					<b>ESMOD JAKARTA</b>: Your data has been inputted by $_COOKIE[nama].
					<br/>
					<table>
						<tr>
							<td>Program</td>
							<td>:</td>
							<td>$NamaProdi1$NamaProdi2$NamaProdi3$NamaProdi4</td>
						</tr>
						<tr>
							<td>Intake</td>
							<td>:</td>
							<td>".strtoupper($this->input->post('PMBPeriodID'))." - $result2[Nama]</td>
						</tr>
						<tr>
							<td>Marketing</td>
							<td>:</td>
							<td>".strtoupper($NamaMarketing)."</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>:</td>
							<td>".$this->input->post('CatatanPresenter')."</td>
						</tr>
					</table>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				";
				$this->email->message($message);
				if($this->email->send())
				{
					echo warning("Your data with APPLICANT CODE '".$nextid."' Successfully added. Verification will be sent to your email: ".strtolower($this->input->post('Email')),"../prospects");
				}
				else
				{
					$data = array(
								'aplikasi' => 'APPLICANT',
								'from' => 'no.reply.esmodjakarta@gmail.com',
								'to' => 'lendra.permana@gmail.com',
								'subject' => 'REGISTRATION SUCCESSFUL',
								'message' => $message,
								'id_akun' => $nextid,
								'nama' => strtoupper($this->input->post('Nama')),
								'login_buat' => strtoupper($this->input->post('Nama')),
								'tanggal_buat' => gmdate("Y-m-d H:i:s", time()-($ms))
								);
					$this->m_mail->PTL_insert($data);
					echo warning("Email server is not active. Your data with APPLICANT CODE '".$nextid."' Successfully added. Verification will be sent to your email: ".strtolower($this->input->post('Email')),"../prospects");
				}
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['AplikanID'] = $result['AplikanID'];
			$data['id_lead'] = $result['id_lead'];
			$data['kode_lead'] = $result['kode_lead'];
			$data['PMBID'] = $result['PMBID'];
			$data['MhswID'] = $result['MhswID'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['PMBPeriodID'] = $result['PMBPeriodID'];
			$data['foto'] = $result['foto'];
			$data['BuktiSetoran'] = $result['BuktiSetoran'];
			$data['file_ujian1'] = $result['file_ujian1'];
			$data['file_syarat'] = $result['file_syarat'];
			$data['bukti_setoran'] = $result['bukti_setoran'];
			$data['SudahBayar'] = $result['SudahBayar'];
			$data['LulusUjian'] = $result['LulusUjian'];
			$data['SyaratLengkap'] = $result['SyaratLengkap'];
			$data['diterima'] = $result['diterima'];
			$data['nomor_identitas'] = $result['nomor_identitas'];
			$data['Nama'] = $result['Nama'];
			$data['Kelamin'] = $result['Kelamin'];
			$data['GolonganDarah'] = $result['GolonganDarah'];
			$data['WargaNegara'] = $result['WargaNegara'];
			$data['Kebangsaan'] = $result['Kebangsaan'];
			$data['TempatLahir'] = $result['TempatLahir'];
			$data['TanggalLahir'] = $result['TanggalLahir'];
			$data['umur'] = $result['umur'];
			$data['Agama'] = $result['Agama'];
			$data['TinggiBadan'] = $result['TinggiBadan'];
			$data['BeratBadan'] = $result['BeratBadan'];
			$data['Alamat'] = $result['Alamat'];
			$data['Kota'] = $result['Kota'];
			$data['RT'] = $result['RT'];
			$data['RW'] = $result['RW'];
			$data['KodePos'] = $result['KodePos'];
			$data['Propinsi'] = $result['Propinsi'];
			$data['Negara'] = $result['Negara'];
			$data['Telepon'] = $result['Telepon'];
			$data['Handphone'] = $result['Handphone'];
			$data['Email'] = $result['Email'];
			$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
			$data['AsalSekolah'] = $result['AsalSekolah'];
			$data['KotaAsalSekolah'] = $result['KotaAsalSekolah'];
			$data['JurusanSekolah'] = $result['JurusanSekolah'];
			$data['TahunLulus'] = $result['TahunLulus'];
			$data['NilaiSekolah'] = $result['NilaiSekolah'];
			$data['SudahBekerja'] = $result['SudahBekerja'];
			$data['NamaAyah'] = $result['NamaAyah'];
			$data['AgamaAyah'] = $result['AgamaAyah'];
			$data['PendidikanAyah'] = $result['PendidikanAyah'];
			$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
			$data['AlamatOrtu'] = $result['AlamatOrtu'];
			$data['KotaOrtu'] = $result['KotaOrtu'];
			$data['RTOrtu'] = $result['RTOrtu'];
			$data['RWOrtu'] = $result['RWOrtu'];
			$data['KodePosOrtu'] = $result['KodePosOrtu'];
			$data['PropinsiOrtu'] = $result['PropinsiOrtu'];
			$data['NegaraOrtu'] = $result['NegaraOrtu'];
			$data['TeleponOrtu'] = $result['TeleponOrtu'];
			$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
			$data['EmailOrtu'] = $result['EmailOrtu'];
			$data['NamaIbu'] = $result['NamaIbu'];
			$data['AgamaIbu'] = $result['AgamaIbu'];
			$data['PendidikanIbu'] = $result['PendidikanIbu'];
			$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
			$data['TeleponIbu'] = $result['TeleponIbu'];
			$data['HandphoneIbu'] = $result['HandphoneIbu'];
			$data['EmailIbu'] = $result['EmailIbu'];
			$data['Pilihan1'] = $result['Pilihan1'];
			$data['Pilihan2'] = $result['Pilihan2'];
			$data['Pilihan3'] = $result['Pilihan3'];
			$data['Pilihan4'] = $result['Pilihan4'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['Harga'] = $result['Harga'];
			$data['StatusProspekID'] = $result['StatusProspekID'];
			$data['NilaiUjian'] = $result['NilaiUjian'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['CatatanPresenter'] = $result['CatatanPresenter'];
			$data['Catatan'] = $result['Catatan'];
			$data['SumberInformasi'] = $result['SumberInformasi'];
			$data['total_biaya'] = $result['total_biaya'];
			$data['total_bayar'] = $result['total_bayar'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['postpone'] = $result['postpone'];
			$data['NA'] = $result['NA'];
			
			$data['marketing'] = $this->m_akses->PTL_all_marketing();
			$data['pmbperiod'] = $this->m_pmbperiod->PTL_all_active();
			$data['ortu'] = $this->m_pendidikanortu->PTL_all();
			$data['d3'] = $this->m_prodi->PTL_all();
			$data['d1'] = $this->m_prodi->PTL_all_d1();
			$data['kursussingkat'] = $this->m_kursussingkat->PTL_all();
			$data['sumberinfo'] = $this->m_sumberinfo->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form_applicant',$hdata);
			$this->load->view('Prospects/v_prospects_edit',$data);
			$this->load->view('Portal/v_footer_form_applicant');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$no = $this->input->post('no');
			for($i=0;$i<=$no;$i++)
			{
				if($this->input->post("SumberInformasi$i")!=""){@$SumberInformasi.=$this->input->post("SumberInformasi$i")."_";}else{@$SumberInformasi.="";}
			}
			$PMBPeriodID = $this->input->post('PMBPeriodID');
			$result2 = $this->m_pmbperiod->PTL_select($PMBPeriodID);
			$AplikanID = $this->input->post('AplikanID');
			$id_aplikan = $AplikanID;
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$Pilihan1 = $this->input->post('Pilihan1');
			$Pilihan2 = $this->input->post('Pilihan2');
			$Pilihan3 = $this->input->post('Pilihan3');
			$Pilihan4 = $this->input->post('Pilihan4');
			$prog = "";
			$NamaProdi1 = "";
			$NamaProdi2 = "";
			$NamaProdi3 = "";
			if(($Pilihan1 != "") AND ($Pilihan1 != "0"))
			{
				$d3 = $this->m_prodi->PTL_all();
				if($d3)
				{
					foreach($d3 as $rowd3)
					{
						if($Pilihan1 == $rowd3->ProdiID)
						{
							$pil = $rowd3->ProdiID;
							$NamaProdi1 = $rowd3->Nama;
							$prog = "REG";
						}
					}
				}
			}
			if(($Pilihan2 != "") AND ($Pilihan2 != "0"))
			{
				$d1 = $this->m_prodi->PTL_all_d1();
				if($d1)
				{
					foreach($d1 as $rowd1)
					{
						if($Pilihan2 == $rowd1->ProdiID)
						{
							$pil = $rowd1->ProdiID;
							$NamaProdi2 = $rowd1->Nama;
							$prog = "INT";
						}
					}
				}
			}
			if(($Pilihan3 != "") AND ($Pilihan3 != "0"))
			{
				$kursussingkat = $this->m_kursussingkat->PTL_all();
				if($kursussingkat)
				{
					foreach($kursussingkat as $rowks)
					{
						if($Pilihan3 == $rowks->KursusSingkatID)
						{
							$pil = $rowks->KursusSingkatID;
							$NamaProdi3 = $rowks->Nama;
							$prog = "SC";
						}
					}
				}
			}
			if(($Pilihan4 != "") AND ($Pilihan4 != "0"))
			{
				$pil = $Pilihan4;
				if($Pilihan4 == "MAS")
				{
					$NamaProdi4 = "MAS";
					$prog = "MAS";
				}
				else
				{
					$NamaProdi4 = $Pilihan4;
					$prog = "SPC";
				}
			}
			$id_akun = $this->input->post('PresenterID');
			$resakses = $this->m_akun->PTL_select($id_akun);
			$NamaMarketing = '';
			if($resakses)
			{
				$NamaMarketing = $resakses['nama'];
			}
			if($result['PMBID'] == "")
			{
				$kd2 = $result2['FormatNoPMB'];
				$result22 = $this->m_aplikan->PTL_urut_pmb2($kd2);
				$lastid22 = $result22['LAST'];
				$lastnourut22 = substr($lastid22,7,4);
				$nextnourut22 = $lastnourut22 + 1;
				$nextid22 = $kd2.sprintf('%0'.$result2['DigitNoPMB'].'s', $nextnourut22);
				$data = array(
							'PMBID' => $nextid22
							);
				$this->m_aplikan->PTL_update($AplikanID,$data);
			}
			$data = array(
						'AplikanID' => $AplikanID,
						'id_lead' => $this->input->post('id_lead'),
						'kode_lead' => $this->input->post('kode_lead'),
						'ProgramID' => $prog,
						'ProdiID' => $pil,
						'PMBPeriodID' => $this->input->post('PMBPeriodID'),
						'SudahBayar' => $this->input->post('SudahBayar'),
						'LulusUjian' => $this->input->post('LulusUjian'),
						'SyaratLengkap' => $this->input->post('SyaratLengkap'),
						'diterima' => $this->input->post('diterima'),
						'nomor_identitas' => $this->input->post('nomor_identitas'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'Kelamin' => strtoupper($this->input->post('Kelamin')),
						'GolonganDarah' => strtoupper($this->input->post('GolonganDarah')),
						'WargaNegara' => strtoupper($this->input->post('WargaNegara')),
						'Kebangsaan' => strtoupper($this->input->post('Kebangsaan')),
						'TempatLahir' => strtoupper($this->input->post('TempatLahir')),
						'TanggalLahir' => $this->input->post('TanggalLahir'),
						'umur' => $this->input->post('umur'),
						'Agama' => strtoupper($this->input->post('Agama')),
						'TinggiBadan' => $this->input->post('TinggiBadan'),
						'BeratBadan' => $this->input->post('BeratBadan'),
						'Alamat' => strtoupper($this->input->post('Alamat')),
						'Kota' => strtoupper($this->input->post('Kota')),
						'RT' => $this->input->post('RT'),
						'RW' => $this->input->post('RW'),
						'KodePos' => $this->input->post('KodePos'),
						'Propinsi' => strtoupper($this->input->post('Propinsi')),
						'Negara' => strtoupper($this->input->post('Negara')),
						'Telepon' => $this->input->post('Telepon'),
						'Handphone' => $this->input->post('Handphone'),
						'Email' => strtolower($this->input->post('Email')),
						'PendidikanTerakhir' => strtoupper($this->input->post('PendidikanTerakhir')),
						'AsalSekolah' => strtoupper($this->input->post('AsalSekolah')),
						'KotaAsalSekolah' => strtoupper($this->input->post('KotaAsalSekolah')),
						'JurusanSekolah' => strtoupper($this->input->post('JurusanSekolah')),
						'TahunLulus' => $this->input->post('TahunLulus'),
						'NilaiSekolah' => $this->input->post('NilaiSekolah'),
						'SudahBekerja' => $this->input->post('SudahBekerja'),
						'NamaAyah' => strtoupper($this->input->post('NamaAyah')),
						'AgamaAyah' => strtoupper($this->input->post('AgamaAyah')),
						'PendidikanAyah' => strtoupper($this->input->post('PendidikanAyah')),
						'PekerjaanAyah' => strtoupper($this->input->post('PekerjaanAyah')),
						'AlamatOrtu' => strtoupper($this->input->post('AlamatOrtu')),
						'KotaOrtu' => strtoupper($this->input->post('KotaOrtu')),
						'RTOrtu' => $this->input->post('RTOrtu'),
						'RWOrtu' => $this->input->post('RWOrtu'),
						'KodePosOrtu' => strtoupper($this->input->post('KodePosOrtu')),
						'PropinsiOrtu' => strtoupper($this->input->post('PropinsiOrtu')),
						'NegaraOrtu' => strtoupper($this->input->post('NegaraOrtu')),
						'TeleponOrtu' => $this->input->post('TeleponOrtu'),
						'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
						'EmailOrtu' => strtolower($this->input->post('EmailOrtu')),
						'NamaIbu' => strtoupper($this->input->post('NamaIbu')),
						'AgamaIbu' => strtoupper($this->input->post('AgamaIbu')),
						'PendidikanIbu' => strtoupper($this->input->post('PendidikanIbu')),
						'PekerjaanIbu' => strtoupper($this->input->post('PekerjaanIbu')),
						'TeleponIbu' => $this->input->post('TeleponIbu'),
						'HandphoneIbu' => $this->input->post('HandphoneIbu'),
						'EmailIbu' => strtolower($this->input->post('EmailIbu')),
						'Pilihan1' => $Pilihan1,
						'Pilihan2' => $Pilihan2,
						'Pilihan3' => $Pilihan3,
						'Pilihan4' => $Pilihan4,
						'TahunKe' => $this->input->post('TahunKe'),
						'Harga' => $this->input->post('Harga'),
						'StatusProspekID' => strtoupper($this->input->post('StatusProspekID')),
						'NilaiUjian' => $this->input->post('NilaiUjian'),
						'PresenterID' => $this->input->post('PresenterID'),
						'CatatanPresenter' => $this->input->post('CatatanPresenter'),
						'Catatan' => strtoupper($this->input->post('Catatan')),
						'SumberInformasi' => $SumberInformasi,
						'NA' => $this->input->post('NA'),
						'total_biaya' => $this->input->post('total_biaya'),
						'total_bayar' => $this->input->post('total_bayar'),
						'postpone' => $this->input->post('postpone'),
						'login_edit' => strtoupper($this->input->post('Nama')),
						'tanggal_edit' => $this->input->post('tanggal_buat')
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			$id_akun = $AplikanID;
			$dataa = array(
						'nama' => strtoupper($this->input->post('Nama')),
						'username' => strtolower($this->input->post('Email')),
						'divisi' => 'ACADEMIC',
						'jabatan' => 'STUDENT',
						'akses' => 'STUDENT',
						'aplikasi' => 'al_',
						'login_edit' => strtoupper($this->input->post('Nama')),
						'tanggal_edit' => gmdate("Y-m-d H:i:s", time()-($ms))
						);
			$this->m_akses->PTL_update_2($id_akun,$dataa);
			$da = array(
						'id_akun' => $id_akun,
						'nama' => strtoupper($this->input->post('Nama')),
						'aktivitas' => 'UPDATED REGISTRATION: Registration successfully updated'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$error = array();
			$gambar = array();
			$no = 1;
			foreach($_FILES as $field_name => $file)
			{
				$h = "-7";
				$hm = $h * 60;
				$ms = $hm * 60;
				$storage = gmdate("Y-m", time()-($ms));
				$tgl = gmdate("ymdHis", time()-($ms));
				$download = $file['name'];
				$downloadin = $tgl."_".str_replace(' ','_',$download);
				if($no == 1)
				{
					$uploadFile = '../applicant/ptl_storage/admisi/aplikan/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 2)
				{
					$uploadFile = '../applicant/ptl_storage/admisi/bukti_setor/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 3)
				{
					$uploadFile = '../applicant/ptl_storage/admisi/ujian/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 4)
				{
					$uploadFile = '../applicant/ptl_storage/admisi/syarat/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 5)
				{
					$uploadFile = '../applicant/ptl_storage/admisi/pembayaran/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				$config['upload_path'] = $uploadFile;
				$config['allowed_types'] = '*';
				$config['max_size'] = '1024';
				$config['max_width'] = '1024';
				$config['max_height'] = '768';
				$config['remove_spaces'] = true;
				$config['overwrite'] = false;
				$config['file_name'] = $downloadin;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload($field_name))
				{
					$error[] = $this->upload->display_errors();
					if($no == 1)
					{
						$file = substr($this->input->post('foto2'),8);
					}
					if($no == 2)
					{
						$file = substr($this->input->post('BuktiSetoran2'),8);
					}
					if($no == 3)
					{
						$file = substr($this->input->post('file_ujian12'),8);
					}
					if($no == 4)
					{
						$file = substr($this->input->post('file_syarat2'),8);
					}
					if($no == 5)
					{
						$file = substr($this->input->post('bukti_setoran2'),8);
					}
					$gambar[] = $file;
				}
				else
				{ 
					$gambar0 = $this->upload->data();
					if($no == 1)
					{
						$gambar_lama = $this->input->post('foto2');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							unlink("../applicant/ptl_storage/admisi/aplikan/".$dir."/".$file);
						}
					}
					if($no == 2)
					{
						$gambar_lama = $this->input->post('BuktiSetoran2');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							unlink("../applicant/ptl_storage/admisi/bukti_setor/".$dir."/".$file);
						}
					}
					if($no == 3)
					{
						$gambar_lama = $this->input->post('file_ujian12');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							unlink("../applicant/ptl_storage/admisi/ujian/".$dir."/".$file);
						}
					}
					if($no == 4)
					{
						$gambar_lama = $this->input->post('file_syarat2');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							unlink("../applicant/ptl_storage/admisi/syarat/".$dir."/".$file);
						}
					}
					if($no == 5)
					{
						$gambar_lama = $this->input->post('bukti_setoran2');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							unlink("../applicant/ptl_storage/admisi/pembayaran/".$dir."/".$file);
						}
					}
					$gambar[] = $gambar0['file_name'];
				}
				$no++;
			}
			$nextid = $AplikanID;
			$this->m_aplikan->PTL_insert_file($nextid,$gambar,$gambar,$gambar,$gambar,$gambar);
			
			$id_akun = $_COOKIE["id_akun"];
			$resakun = $this->m_akun->PTL_select($id_akun);
			
			$AplEmail = strtolower($this->input->post('Email'));
			
			$EmailAyah = strtolower($this->input->post('EmailOrtu'));
			$EmailIbu = strtolower($this->input->post('EmailIbu'));
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($AplEmail);
			$this->email->cc("$EmailAyah,$EmailIbu");
			$this->email->bcc("$resakun[email],$resakun[email2],anita@esmodjakarta.com,ichaa@esmodjakarta.com,miha@esmodjakarta.com");
			// $this->email->bcc("lendra.permana@gmail.com");
			$this->email->subject('UPDATE PROFILE SUCCESSFUL (NO REPLY)');
			$message = "
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<h1>UPDATE PROFILE SUCCESSFUL!</h1>
				<br/>
				<br/>
				Dear ".strtoupper($this->input->post('Nama')).",
				<br/>
				<br/>
				<b>ESMOD JAKARTA</b>: Your data has been updated by $_COOKIE[nama].
				<br/>
				<table>
					<tr>
						<td>Program</td>
						<td>:</td>
						<td>$NamaProdi1$NamaProdi2$NamaProdi3</td>
					</tr>
					<tr>
						<td>Intake</td>
						<td>:</td>
						<td>".strtoupper($this->input->post('PMBPeriodID'))." - ".@$result2['Nama']."</td>
					</tr>
					<tr>
						<td>Marketing</td>
						<td>:</td>
						<td>".strtoupper($NamaMarketing)."</td>
					</tr>
						<tr>
							<td>Notes</td>
							<td>:</td>
							<td>".$this->input->post('CatatanPresenter')."</td>
						</tr>
				</table>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			";
			$this->email->message($message);
			if($this->email->send())
			{
				echo warning("Your data with APPLICANT CODE '".$AplikanID."' Successfully update. Verification will be sent to your email: ".strtolower($this->input->post('Email')),"../prospects");
			}
			else
			{
				$data = array(
							'aplikasi' => 'APPLICANT',
							'from' => 'no.reply.esmodjakarta@gmail.com',
							'to' => 'lendra.permana@gmail.com',
							'subject' => 'REGISTRATION SUCCESSFUL',
							'message' => $message,
							'id_akun' => $nextid,
							'nama' => strtoupper($this->input->post('Nama')),
							'login_buat' => strtoupper($this->input->post('Nama')),
							'tanggal_buat' => gmdate("Y-m-d H:i:s", time()-($ms))
							);
				$this->m_mail->PTL_insert($data);
				echo warning("Email server is not active. Your data with APPLICANT CODE '".$nextid."' Successfully added. Verification will be sent to your email: ".strtolower($this->input->post('Email')),"../prospects");
			}
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			echo warning("Data APPLICANT with CODE '".$AplikanID."' successfully deleted.","../prospects");
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['AplikanID'] = $result['AplikanID'];
			$data['id_lead'] = $result['id_lead'];
			$data['kode_lead'] = $result['kode_lead'];
			$data['PMBID'] = $result['PMBID'];
			$data['MhswID'] = $result['MhswID'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['PMBPeriodID'] = $result['PMBPeriodID'];
			$data['foto'] = $result['foto'];
			$data['BuktiSetoran'] = $result['BuktiSetoran'];
			$data['file_ujian1'] = $result['file_ujian1'];
			$data['file_syarat'] = $result['file_syarat'];
			$data['bukti_setoran'] = $result['bukti_setoran'];
			$data['SudahBayar'] = $result['SudahBayar'];
			$data['LulusUjian'] = $result['LulusUjian'];
			$data['SyaratLengkap'] = $result['SyaratLengkap'];
			$data['diterima'] = $result['diterima'];
			$data['nomor_identitas'] = $result['nomor_identitas'];
			$data['Nama'] = $result['Nama'];
			$data['Kelamin'] = $result['Kelamin'];
			$data['GolonganDarah'] = $result['GolonganDarah'];
			$data['WargaNegara'] = $result['WargaNegara'];
			$data['Kebangsaan'] = $result['Kebangsaan'];
			$data['TempatLahir'] = $result['TempatLahir'];
			$data['TanggalLahir'] = $result['TanggalLahir'];
			$data['umur'] = $result['umur'];
			$data['Agama'] = $result['Agama'];
			$data['TinggiBadan'] = $result['TinggiBadan'];
			$data['BeratBadan'] = $result['BeratBadan'];
			$data['Alamat'] = $result['Alamat'];
			$data['Kota'] = $result['Kota'];
			$data['RT'] = $result['RT'];
			$data['RW'] = $result['RW'];
			$data['KodePos'] = $result['KodePos'];
			$data['Propinsi'] = $result['Propinsi'];
			$data['Negara'] = $result['Negara'];
			$data['Telepon'] = $result['Telepon'];
			$data['Handphone'] = $result['Handphone'];
			$data['Email'] = $result['Email'];
			$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
			$data['AsalSekolah'] = $result['AsalSekolah'];
			$data['KotaAsalSekolah'] = $result['KotaAsalSekolah'];
			$data['JurusanSekolah'] = $result['JurusanSekolah'];
			$data['TahunLulus'] = $result['TahunLulus'];
			$data['NilaiSekolah'] = $result['NilaiSekolah'];
			$data['SudahBekerja'] = $result['SudahBekerja'];
			$data['NamaAyah'] = $result['NamaAyah'];
			$data['AgamaAyah'] = $result['AgamaAyah'];
			$data['PendidikanAyah'] = $result['PendidikanAyah'];
			$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
			$data['AlamatOrtu'] = $result['AlamatOrtu'];
			$data['KotaOrtu'] = $result['KotaOrtu'];
			$data['RTOrtu'] = $result['RTOrtu'];
			$data['RWOrtu'] = $result['RWOrtu'];
			$data['KodePosOrtu'] = $result['KodePosOrtu'];
			$data['PropinsiOrtu'] = $result['PropinsiOrtu'];
			$data['NegaraOrtu'] = $result['NegaraOrtu'];
			$data['TeleponOrtu'] = $result['TeleponOrtu'];
			$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
			$data['EmailOrtu'] = $result['EmailOrtu'];
			$data['NamaIbu'] = $result['NamaIbu'];
			$data['AgamaIbu'] = $result['AgamaIbu'];
			$data['PendidikanIbu'] = $result['PendidikanIbu'];
			$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
			$data['TeleponIbu'] = $result['TeleponIbu'];
			$data['HandphoneIbu'] = $result['HandphoneIbu'];
			$data['EmailIbu'] = $result['EmailIbu'];
			$data['Pilihan1'] = $result['Pilihan1'];
			$data['Pilihan2'] = $result['Pilihan2'];
			$data['Pilihan3'] = $result['Pilihan3'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['Harga'] = $result['Harga'];
			$data['StatusProspekID'] = $result['StatusProspekID'];
			$data['NilaiUjian'] = $result['NilaiUjian'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['CatatanPresenter'] = $result['CatatanPresenter'];
			$data['Catatan'] = $result['Catatan'];
			$data['SumberInformasi'] = $result['SumberInformasi'];
			$data['total_biaya'] = $result['total_biaya'];
			$data['total_bayar'] = $result['total_bayar'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['postpone'] = $result['postpone'];
			$data['NA'] = $result['NA'];
			
			$data['marketing'] = $this->m_akses->PTL_all_marketing();
			$data['pmbperiod'] = $this->m_pmbperiod->PTL_all_active();
			$data['ortu'] = $this->m_pendidikanortu->PTL_all();
			$data['d3'] = $this->m_prodi->PTL_all();
			$data['d1'] = $this->m_prodi->PTL_all_d1();
			$data['kursussingkat'] = $this->m_kursussingkat->PTL_all();
			$data['sumberinfo'] = $this->m_sumberinfo->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form_applicant',$hdata);
			$this->load->view('Prospects/v_prospects_copy',$data);
			$this->load->view('Portal/v_footer_form_applicant');
		}
	}
?>