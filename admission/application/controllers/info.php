<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Info extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_info');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$data['rowrecord'] = $this->m_info->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_info',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_info_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'judul' => strtoupper($this->input->post('judul')),
						'isi' => $this->input->post('isi'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_info->PTL_insert($data);
			echo warning("Data INFORMATION FOR APPLICANT successfully added.","../info");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','Setup');
			$id_info = $this->uri->segment(3);
			$result = $this->m_info->PTL_select($id_info);
			$data['id_info'] = $result['id_info'];
			$data['judul'] = $result['judul'];
			$data['isi'] = $result['isi'];
			$data['na'] = $result['na'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_info_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$id_info = $this->input->post('id_info');
			$na = "N";
			if($this->input->post('na') == "Y")
			{
				$na = "Y";
			}
			$data = array(
						'judul' => strtoupper($this->input->post('judul')),
						'isi' => $this->input->post('isi'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'na' => $na
						);
			$this->m_info->PTL_update($id_info,$data);
			echo warning("Data INFORMATION FOR APPLICANT with TITLE '".strtoupper($this->input->post('judul'))."' successfully updated.","../info");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$id_info = $this->uri->segment(3);
			$data = array(
						'na' => 'Y'
						);
			$this->m_info->PTL_update($id_info,$data);
			echo warning("Data INFORMATION FOR APPLICANT with CODE '".$id_info."' successfully deleted.","../info");
		}
	}
?>