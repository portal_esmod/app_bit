<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Religion extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_religion');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_religion->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_religion',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_religion_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Agama' => $this->input->post('Agama'),
						'Nama' => $this->input->post('Nama')
						);
			$this->m_religion->PTL_insert($data);
			echo warning("Data RELIGION successfully added.","../religion");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$Agama = $this->uri->segment(3);
			$result = $this->m_religion->PTL_select($Agama);
			$data['Agama'] = $result['Agama'];
			$data['Nama'] = $result['Nama'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_religion_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$Agama = $this->input->post('Agama');
			$data = array(
						'Agama' => $this->input->post('Agama'),
						'Nama' => $this->input->post('Nama')
						);
			$this->m_religion->PTL_update($Agama,$data);
			echo warning("Data RELIGION with CODE '".$Agama."' successfully changed.","../religion");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$Agama = $this->uri->segment(3);
			$this->m_religion->PTL_delete($Agama);
			echo warning("Data RELIGION with CODE '".$Agama."' successfully deleted.","../religion");
		}
	}
?>