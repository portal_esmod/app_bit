<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Sms extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_waktu = gmdate("YmdHis", time()-($ms));
			$this->tanggal_kirim = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_mail');
			$this->load->model('m_maintenance');
			$this->load->model('m_sms');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','prospects');
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$data['Handphone'] = $result['Handphone'];
			$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
			$data['Email'] = $result['Email'];
			$data['EmailOrtu'] = $result['EmailOrtu'];
			$data['pesan'] = 'Kami informasikan bahwa ';
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('SMS/v_sms',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$kirim1 = $this->input->post('kirim1');
			if($kirim1 == "Y")
			{
				$hp_mahasiswa = $this->input->post('hp_mahasiswa');
				if($hp_mahasiswa != "")
				{
					$data = array(
								'judul_sms' => $this->input->post('subjek'),
								'nomor_hp' => $this->input->post('hp_mahasiswa'),
								'pesan' => $this->input->post('pesan'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_sms->PTL_insert($data);
				}
			}
			$kirim2 = $this->input->post('kirim2');
			if($kirim2 == "Y")
			{
				$hp_orang_tua = $this->input->post('hp_orang_tua');
				if($hp_orang_tua != "")
				{
					$data = array(
								'judul_sms' => $this->input->post('subjek'),
								'nomor_hp' => $this->input->post('hp_orang_tua'),
								'pesan' => $this->input->post('pesan'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_sms->PTL_insert($data);
				}
			}
			$pesan = "
					<br/>
					<br/>
					Admission ESMOD Jakarta telah mengirimkan Anda pesan melalui SMS.
					<br/>
					(<b><i>Admission ESMOD Jakarta has sent you a message via SMS.</i></b>)
					<br/>
					<br/>
					Isi pesan (<b><i>Messages</i></b>) : <span style='color:blue;'><b>".$this->input->post('pesan')."</b></span>
					<br>
					";
			$kirim3 = $this->input->post('kirim3');
			if($kirim3 == "Y")
			{
				$email_mahasiswa = $this->input->post('email_mahasiswa');
				if($email_mahasiswa != "")
				{
					$data = array(
								'kode_waktu' => $this->kode_waktu,
								'aplikasi' => 'ADMISSION',
								'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
								'to' => $email_mahasiswa,
								'subject' => $this->input->post('subjek'),
								'tanggal_kirim' => $this->tanggal_kirim,
								'message' => $pesan,
								'id_akun' => $this->input->post('AplikanID'),
								'nama' => $this->input->post('Nama'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_mail->PTL_insert($data);
				}
			}
			$kirim4 = $this->input->post('kirim4');
			if($kirim4 == "Y")
			{
				$email_orang_tua = $this->input->post('email_orang_tua');
				if($email_orang_tua != "")
				{
					$data = array(
								'kode_waktu' => $this->kode_waktu,
								'aplikasi' => 'ADMISSION',
								'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
								'to' => $email_orang_tua,
								'subject' => $this->input->post('subjek'),
								'tanggal_kirim' => $this->tanggal_kirim,
								'message' => $pesan,
								'id_akun' => $this->input->post('AplikanID'),
								'nama' => $this->input->post('Nama'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_mail->PTL_insert($data);
				}
			}
			$id_akun = $_COOKIE["id_akun"];
			$resakun = $this->m_akun->PTL_select($id_akun);
			$data = array(
						'kode_waktu' => $this->kode_waktu,
						'aplikasi' => 'ADMISSION',
						'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
						'to' => $resakun['email'],
						'subject' => $this->input->post('subjek'),
						'tanggal_kirim' => $this->tanggal_kirim,
						'message' => $pesan,
						'id_akun' => $this->input->post('AplikanID'),
						'nama' => $this->input->post('Nama'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_mail->PTL_insert($data);
			$AplikanID = $this->input->post('AplikanID');
			echo warning("SMS successfully sent.","../sms/ptl_form/$AplikanID");
		}
	}
?>