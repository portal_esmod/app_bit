<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Education extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_education');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_education->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_education',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_education_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Pendidikan' => $this->input->post('Pendidikan'),
						'Nama' => $this->input->post('Nama')
						);
			$this->m_education->PTL_insert($data);
			echo warning("Data EDUCATION successfully added.","../education");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$Pendidikan = $this->uri->segment(3);
			$result = $this->m_education->PTL_select($Pendidikan);
			$data['Pendidikan'] = $result['Pendidikan'];
			$data['Nama'] = $result['Nama'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_education_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$Pendidikan = $this->input->post('Pendidikan');
			$data = array(
						'Pendidikan' => $this->input->post('Pendidikan'),
						'Nama' => $this->input->post('Nama')
						);
			$this->m_education->PTL_update($Pendidikan,$data);
			echo warning("Data EDUCATION with CODE '".$Pendidikan."' successfully changed.","../education");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$Pendidikan = $this->uri->segment(3);
			$this->m_education->PTL_delete($Pendidikan);
			echo warning("Data EDUCATION with CODE '".$Pendidikan."' successfully deleted.","../education");
		}
	}
?>