<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Province extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_province');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_province->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_province',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_province_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Propinsi' => $this->input->post('Propinsi')
						);
			$this->m_province->PTL_insert($data);
			echo warning("Data PROVINCE successfully added.","../province");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$PropinsiID = $this->uri->segment(3);
			$result = $this->m_province->PTL_select($PropinsiID);
			$data['PropinsiID'] = $result['PropinsiID'];
			$data['Propinsi'] = $result['Propinsi'];
			$data['NA'] = $result['NA'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_province_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$PropinsiID = $this->input->post('PropinsiID');
			$data = array(
						'Propinsi' => $this->input->post('Propinsi'),
						'NA' => $this->input->post('NA')
						);
			$this->m_province->PTL_update($PropinsiID,$data);
			echo warning("Data PROVINCE with CODE '".$PropinsiID."' successfully changed.","../province");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$PropinsiID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_province->PTL_update($PropinsiID,$data);
			echo warning("Data PROVINCE with CODE '".$PropinsiID."' successfully deleted.","../province");
		}
	}
?>