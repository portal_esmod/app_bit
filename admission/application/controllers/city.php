<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class City extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_city');
			$this->load->model('m_maintenance');
			$this->load->model('m_province');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_province->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->PropinsiID,
											'value' => $row->Propinsi,
											'propinsi' => $row->PropinsiID,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('city/index',$data);
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_city->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_city',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_city_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$kd = $this->input->post('PropinsiID');
			$result = $this->m_city->PTL_urut($kd);
			$lastid = $result['LAST'];
			
			$nextnourut = $lastid + 1;
			
			$data = array(
						'KotaID' => $nextnourut,
						'KotaKabupaten' => $this->input->post('KotaKabupaten'),
						'PropinsiID' => $kd
						);
			$this->m_city->PTL_insert($data);
			echo warning("Data CITY successfully added.","../city");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$KotaKabupaten = $this->uri->segment(3);
			$this->m_city->PTL_delete($KotaKabupaten);
			echo warning("Data CITY with CODE '".$KotaKabupaten."' successfully deleted.","../city");
		}
	}
?>