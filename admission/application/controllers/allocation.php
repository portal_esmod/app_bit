<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Allocation extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_allocation');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_spesialisasi');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','allocation');
			$data['rowrecord'] = $this->m_allocation->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Allocation/v_allocation',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','allocation');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_university_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'id_sekolah' => $this->input->post('id_sekolah'),
						'Nama' => $this->input->post('Nama'),
						'SingkatanNama' => $this->input->post('SingkatanNama'),
						'jenis' => 'UNIVERSITAS',
						'alamat' => $this->input->post('alamat'),
						'Kota' => $this->input->post('Kota'),
						'KodePos' => $this->input->post('KodePos'),
						'Telephone' => $this->input->post('Telephone'),
						'Fax' => $this->input->post('Fax'),
						'Website' => $this->input->post('Website'),
						'Email' => $this->input->post('Email')
						);
			$this->m_sekolah->PTL_insert($data);
			echo warning("Data UNIVERSITY successfully added.","../university");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','allocation');
			$id_sekolah = $this->uri->segment(3);
			$result = $this->m_sekolah->PTL_select($id_sekolah);
			$data['id_sekolah'] = $result['id_sekolah'];
			$data['Nama'] = $result['Nama'];
			$data['SingkatanNama'] = $result['SingkatanNama'];
			$data['alamat'] = $result['alamat'];
			$data['Kota'] = $result['Kota'];
			$data['KodePos'] = $result['KodePos'];
			$data['Telephone'] = $result['Telephone'];
			$data['Fax'] = $result['Fax'];
			$data['Website'] = $result['Website'];
			$data['Email'] = $result['Email'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_university_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$id_sekolah = $this->input->post('id_sekolah');
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'SingkatanNama' => $this->input->post('SingkatanNama'),
						'alamat' => $this->input->post('alamat'),
						'Kota' => $this->input->post('Kota'),
						'KodePos' => $this->input->post('KodePos'),
						'Telephone' => $this->input->post('Telephone'),
						'Fax' => $this->input->post('Fax'),
						'Website' => $this->input->post('Website'),
						'Email' => $this->input->post('Email')
						);
			$this->m_sekolah->PTL_update($id_sekolah,$data);
			echo warning("Data UNIVERSITY with CODE '".$id_sekolah."' successfully changed.","../university");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$id_sekolah = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_university->PTL_update($id_sekolah,$data);
			echo warning("Data UNIVERSITY with CODE '".$id_sekolah."' successfully deleted.","../university");
		}
	}
?>