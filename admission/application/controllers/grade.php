<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Grade extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_grade');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_grade->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_grade',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_grade_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Grade' => $this->input->post('Grade'),
						'GradeNilai' => $this->input->post('GradeNilai'),
						'NilaiUjianMin' => $this->input->post('NilaiUjianMin'),
						'Wawancara' => $this->input->post('Wawancara'),
						'Lulus' => $this->input->post('Lulus'),
						'Keterangan' => $this->input->post('Keterangan')
						);
			$this->m_grade->PTL_insert($data);
			echo warning("Data GRADE successfully added.","../grade");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$GradeID = $this->uri->segment(3);
			$result = $this->m_grade->PTL_select($GradeID);
			$data['GradeID'] = $result['GradeID'];
			$data['Grade'] = $result['Grade'];
			$data['GradeNilai'] = $result['GradeNilai'];
			$data['NilaiUjianMin'] = $result['NilaiUjianMin'];
			$data['Wawancara'] = $result['Wawancara'];
			$data['Lulus'] = $result['Lulus'];
			$data['Keterangan'] = $result['Keterangan'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_grade_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$GradeID = $this->input->post('GradeID');
			$data = array(
						'Grade' => $this->input->post('Grade'),
						'GradeNilai' => $this->input->post('GradeNilai'),
						'NilaiUjianMin' => $this->input->post('NilaiUjianMin'),
						'Wawancara' => $this->input->post('Wawancara'),
						'Lulus' => $this->input->post('Lulus'),
						'Keterangan' => $this->input->post('Keterangan')
						);
			$this->m_grade->PTL_update($GradeID,$data);
			echo warning("Data GRADE with CODE '".$GradeID."' successfully changed.","../grade");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$GradeID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_grade->PTL_update($GradeID,$data);
			echo warning("Data GRADE with CODE '".$GradeID."' successfully deleted.","../grade");
		}
	}
?>