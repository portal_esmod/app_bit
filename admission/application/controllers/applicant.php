<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Applicant extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('finance');
			$this->load->library('log');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_grade');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_lead');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_apl_periode()
		{
			$this->authentification();
			$link = $this->input->post('cekperiode');
			$word = explode("_",$link);
			$back = $word[1];
			$cekperiode = $word[0];
			if($cekperiode != "")
			{
				$this->session->set_userdata('apl_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('apl_filter_periode','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function ptl_filter_apl_marketing()
		{
			$this->authentification();
			$link = $this->input->post('cekmarketing');
			$word = explode("_",$link);
			$back = $word[1];
			$cekmarketing = $word[0];
			if($cekmarketing != "")
			{
				$this->session->set_userdata('apl_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('apl_filter_marketing','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function ptl_filter_apl_type()
		{
			$this->authentification();
			$link = $this->input->post('cektype');
			$word = explode("_",$link);
			$back = $word[1];
			$cektype = $word[0];
			if($cektype != "")
			{
				$this->session->set_userdata('apl_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('apl_filter_type','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function ptl_filter_apl_status()
		{
			$this->authentification();
			$link = $this->input->post('cekstatus');
			$word = explode("_",$link);
			$back = $word[1];
			$cekstatus = $word[0];
			if($cekstatus != "")
			{
				$this->session->set_userdata('apl_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('apl_filter_status','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function ptl_filter_apl_program()
		{
			$this->authentification();
			$link = $this->input->post('cekpogram');
			$word = explode("_",$link);
			$back = $word[1];
			$cekpogram = $word[0];
			if($cekpogram != "")
			{
				$this->session->set_userdata('apl_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('apl_filter_program','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function ptl_filter_apl_tahun()
		{
			$this->authentification();
			$link = $this->input->post('cektahun');
			$word = explode("_",$link);
			$back = $word[1];
			$cektahun = $word[0];
			if($cektahun != "")
			{
				$this->session->set_userdata('apl_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('apl_filter_tahun','');
			}
			if($back == "PYM")
			{
				redirect("applicant");
			}
			if($back == "OLT")
			{
				redirect("applicant/ptl_test");
			}
			if($back == "CPT")
			{
				redirect("applicant/ptl_completeness");
			}
			if($back == "PIS")
			{
				redirect("applicant/ptl_semester");
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','applicant');
			$cekperiode = $this->session->userdata('apl_filter_periode');
			$cekmarketing = $this->session->userdata('apl_filter_marketing');
			$cektype = $this->session->userdata('apl_filter_type');
			$cekstatus = $this->session->userdata('apl_filter_status');
			$cekpogram = $this->session->userdata('apl_filter_program');
			$cektahun = $this->session->userdata('apl_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_payment_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_payment',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_payment_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/bukti_setor/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant');
			}
		}
		
		function ptl_payment_ok()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			$data = array(
						'SudahBayar' => 'Y',
						'NA' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'PAYMENT FORM: Your payment is approved by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ProgramID = $resapl['ProgramID'];
			$ProdiID = $resapl['ProdiID'];
			if($ProgramID == "REG")
			{
				$resprodi = $this->m_prodi->PTL_select($ProdiID);
			}
			if($ProgramID == "SC")
			{
				$resprodi = $this->m_kursussingkat->PTL_select($ProdiID);
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('PAYMENT APPROVE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<br/>
				Dear ".strtoupper($resapl['Nama']).",
				<br/>
				<br/>
				We are happy to announce that your registration fee has been received for enrollment in <b>$resprodi[Nama]</b> program.
				<br/>
				<br/>
				To proceed further in the matter, you are required to submit the following documents in English latest by Admission:
				<br/>
				<br/>
				1. Curriculum vitae
				<br/>
				2. Motivation letter (stating why you have chosen fashion and why Esmod in particular)
				<br/>
				3. Photocopy of School Certificate
				<br/>
				4. Photocopy of scores obtained during the last two years
				<br/>
				5. Photocopy of your identity card 
				<br/>
				6. Photocopy of parent’s (father\mother) identity card
				<br/>
				7. Reference letter from three (3) school teachers in accordance with the form enclosed
				<br/>
				8. Attached is the entrance test which you are required to complete return to us latest by…….
				<br/>
				<br/>
				Please note that we require the above documents by way of 'HARD COPY' which could be mailed to us at the following address:
				<br/>
				registration.documents@esmodjakarta.com
				<br/>
				<br/>
				The next step <a href='http://app.esmodjakarta.com/applicant/ujian.xhtml'>(STEP 3/6) ONLINE TEST</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Yours sincerely,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data PAYMENT with CODE '".$AplikanID."' successfully done.","../applicant");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant');
			}
		}
		
		function ptl_payment_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			$data = array(
						'SudahBayar' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'PAYMENT FORM: Your payment is rejected by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('PAYMENT REJECT (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your payment is rejected.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data PAYMENT with CODE '".$AplikanID."' successfully deleted.","../applicant");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant');
			}
		}
		
		function ptl_test()
		{
			$this->session->set_userdata('menu','applicant');
			$cekperiode = $this->session->userdata('apl_filter_periode');
			$cekmarketing = $this->session->userdata('apl_filter_marketing');
			$cektype = $this->session->userdata('apl_filter_type');
			$cekstatus = $this->session->userdata('apl_filter_status');
			$cekpogram = $this->session->userdata('apl_filter_program');
			$cektahun = $this->session->userdata('apl_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_test_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_test',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_test_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/ujian/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant');
			}
		}
		
		function ptl_test_time()
		{
			$AplikanID = $this->uri->segment(3);
			$data = array(
						'akhir_ujian' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			echo warning('The time limit has been reset.','../applicant/ptl_test');
		}
		
		function ptl_test_view()
		{
			$this->session->set_userdata('menu','applicant');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['file_ujian2'] = $result['file_ujian2'];
			$data['id_aplikan'] = $id_aplikan;
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_test_view',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_test_score()
		{
			$this->authentification();
			$NilaiUjian = $this->input->post('NilaiUjian');
			if($NilaiUjian > 20)
			{
				echo warning('Sorry, you enter a value outside the range (0 - 20).','../applicant/ptl_test');
			}
			else
			{
				$AplikanID = $this->input->post('AplikanID');
				$result = $this->m_aplikan->PTL_select($AplikanID);
				if(($result["file_ujian1"] == "") AND ($result["file_ujian2"] == ""))
				{
					echo warning("Sorry, applicant '$AplikanID' is not doing the test.",'../applicant/ptl_test');
				}
				else
				{
					$grade = "";
					$rowgrade = $this->m_grade->PTL_all_active();
					if($rowgrade)
					{
						foreach($rowgrade as $rg)
						{
							if($NilaiUjian >= $rg->NilaiUjianMin)
							{
								$grade = $rg->Grade;
							}
						}
					}
					$LulusUjian = "";
					if($grade == "F")
					{
						$LulusUjian = "N";
					}
					else
					{
						if($grade == "E")
						{
							$LulusUjian = "I";
						}
						else
						{
							$LulusUjian = "Y";
						}
					}
					$data = array(
								'NilaiUjian' => $NilaiUjian,
								'LulusUjian' => $LulusUjian,
								'GradeNilai' => $grade,
								'NA' => 'N',
								'korektor' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_aplikan->PTL_update($AplikanID,$data);
					
					$da = array(
								'id_akun' => $AplikanID,
								'nama' => $_COOKIE["nama"],
								'aktivitas' => 'ONLINE TEST: Your test have been considered by marketing team'
								);
					$this->m_aplikan_log->PTL_insert($da);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($result['Email']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR TEST HAVE BEEN CONSIDERED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>ADMISSION</h2></font>
						</center>
						<br/>
						<b>ESMOD JAKARTA</b>: Your test have been considered by marketing team.
						<br/>
						<br/>
						To see the results of the assessment <a href='http://app.esmodjakarta.com/applicant/penilaian.xhtml'>(STEP 4/6) APPRAISAL</a>
						<br/>
						The next step <a href='http://app.esmodjakarta.com/applicant/berkas.xhtml'>(STEP 5/6) COMPLETENESS</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully updated.","../applicant/ptl_test");
					}
					else
					{
						echo warning('Email server is not active. Please try again...','../applicant/ptl_test');
					}
				}
			}
		}
		
		function ptl_test_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$file_order = $result['file_ujian1'];
			if($file_order != '')
			{
				$direktori = substr($file_order,0,7);
				$file = substr($file_order,8);
				unlink("../applicant/ptl_storage/admisi/ujian/".$direktori."/".$file);
			}
			$data = array(
						'NilaiUjian' => '',
						'LulusUjian' => 'N',
						'GradeNilai' => '',
						'akhir_ujian' => '',
						'file_ujian1' => '',
						'file_ujian2' => '',
						'ujian' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'ONLINE TEST: Your test have been deleted by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('TEST DELETED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your test is deleted by marketing team.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully deleted.","../applicant/ptl_test");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant/ptl_test');
			}
		}
		
		function ptl_completeness()
		{
			$this->session->set_userdata('menu','applicant');
			$cekperiode = $this->session->userdata('apl_filter_periode');
			$cekmarketing = $this->session->userdata('apl_filter_marketing');
			$cektype = $this->session->userdata('apl_filter_type');
			$cekstatus = $this->session->userdata('apl_filter_status');
			$cekpogram = $this->session->userdata('apl_filter_program');
			$cektahun = $this->session->userdata('apl_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_completeness_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_completeness',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_completeness_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/syarat/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant/ptl_completeness');
			}
		}
		
		function ptl_completeness_set()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			$data = array(
						'StatusAplikanID' => 'LLS',
						'SyaratLengkap' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'FILE: Your file has been approved by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$PMBPeriodID = $resapl['PMBPeriodID'];
			$respmbperiod = $this->m_pmbperiod->PTL_select($PMBPeriodID);
			$id_lead = $resapl['id_lead'];
			$reslead = $this->m_lead->FHM_select($id_lead);
			$NamaAkun = "";
			$handphone = "";
			if($reslead)
			{
				$id_akun = $reslead["PresenterID"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				if($resakun)
				{
					$NamaAkun = $resakun["Nama"];
					$handphone = $resakun["handphone"];
				}
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR FILE HAVE BEEN APPROVED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<br/>
				Dear ".strtoupper($resapl['Nama']).",
				<br/>
				<br/>
				We would like to thank you for sending all the documents required. You are requested to come for an interview with <b>$respmbperiod[Interviewer] on $respmbperiod[TglInterview] at ESMOD Jakarta</b>.
				<br/>
				Kindly contact School Ambassador <b>$NamaAkun</b> at No <b>$handphone</b> to confirm your Attendance.
				<br/>
				<br/>
				The next step <a href='http://app.esmodjakarta.com/applicant/pembayaran.xhtml'>(STEP 6/6) PAYMENT OF 1ST SEMESTER</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Yours sincerely,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data FILE with APPLICANT CODE '".$AplikanID."' successfully approved.","../applicant/ptl_completeness");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant/ptl_completeness');
			}
		}
		
		function ptl_completeness_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$file_order = $result['file_syarat'];
			if($file_order != '')
			{
				$direktori = substr($file_order,0,7);
				$file = substr($file_order,8);
				unlink("../applicant/ptl_storage/admisi/syarat/".$direktori."/".$file);
			}
			$data = array(
						'SyaratLengkap' => 'N',
						'file_syarat' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'FILE: Your file has been rejected by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('FILE DELETED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your file has been rejected by marketing team.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data FILE with APPLICANT CODE '".$AplikanID."' successfully deleted.","../applicant/ptl_completeness");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant/ptl_completeness');
			}
		}
		
		function ptl_semester()
		{
			$this->session->set_userdata('menu','applicant');
			$cekperiode = $this->session->userdata('apl_filter_periode');
			$cekmarketing = $this->session->userdata('apl_filter_marketing');
			$cektype = $this->session->userdata('apl_filter_type');
			$cekstatus = $this->session->userdata('apl_filter_status');
			$cekpogram = $this->session->userdata('apl_filter_program');
			$cektahun = $this->session->userdata('apl_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_semester_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_semester',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_semester_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/pembayaran/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant/ptl_semester');
			}
		}
		
		function ptl_semester_view()
		{
			$this->session->set_userdata('menu','applicant');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['Nama'] = $result['Nama'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['catatan_transfer'] = $result['catatan_transfer'];
			$data['id_aplikan'] = $id_aplikan;
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_semester_view',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_semester_set()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			$data = array(
						'diterima' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'PAYMENT 1st SEMESTER: Your payment 1st semester has been approved by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR PAYMENT 1st SEMESTER HAVE BEEN APPROVED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your payment 1st semester has been approved by marketing team.
				<br/>
				<br/>
				The next step <a href='http://app.esmodjakarta.com/applicant/pembayaran.xhtml'>(STEP 6/6) FINISH</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data PAYMENT 1st SEMESTER with APPLICANT CODE '".$AplikanID."' successfully approved.","../applicant/ptl_semester");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant/ptl_semester');
			}
		}
		
		function ptl_semester_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$file_order = $result['bukti_setoran'];
			if($file_order != '')
			{
				$direktori = substr($file_order,0,7);
				$file = substr($file_order,8);
				unlink("../applicant/ptl_storage/admisi/pembayaran/".$direktori."/".$file);
			}
			$data = array(
						'diterima' => 'N',
						'bukti_setoran' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'PAYMENT 1st SEMESTER: Your payment 1st semester has been rejected by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('FILE PAYMENT 1st SEMESTER DELETED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your payment 1st semester has been rejected by marketing team.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data PAYMENT 1st SEMESTER with APPLICANT CODE '".$AplikanID."' successfully deleted.","../applicant/ptl_semester");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../applicant/ptl_semester');
			}
		}
	}
?>