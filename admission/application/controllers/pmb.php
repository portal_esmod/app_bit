<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Pmb extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_pmb');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$data['rowrecord'] = $this->m_pmb->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmb',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_pmb_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/soal/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = $tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$result = $this->m_pmb->PTL_select_file($dwn);
			if($result)
			{
				echo warning("Sorry, the file name '".$downloadin."' already available. Try to replace the file with another name.","../pmb/ptl_form");
			}
			else
			{
				$TglMulaiTglSelesai = $this->input->post('TglMulaiTglSelesai');
				$wordd = explode(" - ", @$TglMulaiTglSelesai);
				$config['upload_path'] = $admin_dp;
				$config['allowed_types'] = '*';
				$config['max_size'] = '5120000';
				$config['remove_spaces'] = true;
				$config['overwrite'] = false;
				$config['file_name'] = $downloadin;
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload())
				{
					$kd = $this->input->post('Tahun');
					$result = $this->m_pmb->PTL_urut($kd);
					$lastid = $result['LAST'];
					$lastnourut = substr($lastid,4,2);
					$nextnourut = $lastnourut + 1;
					$PMBPeriodID = $kd.sprintf('%02s', $nextnourut);
					$urut = sprintf('%02s', $nextnourut);
					$FormatNoAplikan = substr($kd,2,2)."APL$urut";
					$FormatNoPMB = substr($kd,2,2)."PMB$urut";
					if($this->input->post('ol') == "N"){$ol = "N";}else{$ol = "Y";}
					if($this->input->post('NA') == "N"){$NA = "N";}else{$NA = "Y";}
					$data = array(
								'PMBPeriodID' => $PMBPeriodID,
								'Nama' => strtoupper($this->input->post('Nama')),
								'Tahun' => $this->input->post('Tahun'),
								'FormatNoAplikan' => $FormatNoAplikan,
								'DigitNoAplikan' => '4',
								'FormatNoPMB' => $FormatNoPMB,
								'DigitNoPMB' => '4',
								'ol' => $ol,
								'NA' => $NA,
								'soal2' => $this->input->post('soal2'),
								'TglInterview' => $this->input->post('TglInterview'),
								'Interviewer' => $this->input->post('Interviewer'),
								'TglMulai' => $wordd[0],
								'TglSelesai' => $wordd[1],
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_pmb->PTL_insert($data);
					$error = $this->upload->display_errors('','.');
					echo warning("Data ADMISSION PERIOD with CODE '".$PMBPeriodID."' successfully added. MESSAGE: ".$error,"../pmb");
				}
				else
				{
					$kd = $this->input->post('Tahun');
					$result = $this->m_pmb->PTL_urut($kd);
					$lastid = $result['LAST'];
					$lastnourut = substr($lastid,4,2);
					$nextnourut = $lastnourut + 1;
					$PMBPeriodID = $kd.sprintf('%02s', $nextnourut);
					$FormatNoAplikan = substr($kd,0,2)."APL$nextnourut";
					$FormatNoPMB = substr($kd,0,2)."PMB$nextnourut";
					if($this->input->post('ol') == "N"){$ol = "N";}else{$ol = "Y";}
					if($this->input->post('NA') == "N"){$NA = "N";}else{$NA = "Y";}
					$data = array(
								'PMBPeriodID' => $PMBPeriodID,
								'Nama' => strtoupper($this->input->post('Nama')),
								'Tahun' => $this->input->post('Tahun'),
								'FormatNoAplikan' => $FormatNoAplikan,
								'DigitNoAplikan' => '4',
								'FormatNoPMB' => $FormatNoPMB,
								'DigitNoPMB' => '4',
								'ol' => $ol,
								'NA' => $NA,
								'soal1' => $dwn,
								'soal2' => $this->input->post('soal2'),
								'TglInterview' => $this->input->post('TglInterview'),
								'Interviewer' => $this->input->post('Interviewer'),
								'TglMulai' => $wordd[0],
								'TglSelesai' => $wordd[1],
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_pmb->PTL_insert($data);
					echo warning("Data ADMISSION PERIOD with CODE '".$PMBPeriodID."' successfully added.","../pmb");
				}
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$PMBPeriodID = $this->uri->segment(3);
			$result = $this->m_pmb->PTL_select($PMBPeriodID);
			$data['PMBPeriodID'] = $result['PMBPeriodID'];
			$data['Nama'] = $result['Nama'];
			$data['Tahun'] = $result['Tahun'];
			$data['FormatNoAplikan'] = $result['FormatNoAplikan'];
			$data['DigitNoAplikan'] = $result['DigitNoAplikan'];
			$data['FormatNoPMB'] = $result['FormatNoPMB'];
			$data['DigitNoPMB'] = $result['DigitNoPMB'];
			$data['ol'] = $result['ol'];
			$data['NA'] = $result['NA'];
			$data['soal'] = $result['soal'];
			$data['soal1'] = $result['soal1'];
			$data['soal2'] = $result['soal2'];
			$data['TglInterview'] = $result['TglInterview'];
			$data['Interviewer'] = $result['Interviewer'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_pmb_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("./ptl_storage/soal/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../pmb');
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$PMBPeriodID = $this->input->post('PMBPeriodID');
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/soal/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = $tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$result = $this->m_pmb->PTL_select_file($dwn);
			if($result)
			{
				echo warning("Sorry, the file name '".$downloadin."' already available. Try to replace the file with another name.","../pmb/ptl_edit/$PMBPeriodID");
			}
			else
			{
				if($this->input->post('ol') == "N"){$ol = "N";}else{$ol = "Y";}
				if($this->input->post('NA') == "N"){$NA = "N";}else{$NA = "Y";}
				$TglMulaiTglSelesai = $this->input->post('TglMulaiTglSelesai');
				$wordd = explode(" - ", @$TglMulaiTglSelesai);
				$config['upload_path'] = $admin_dp;
				$config['allowed_types'] = '*';
				$config['max_size'] = '5120000';
				$config['remove_spaces'] = true;
				$config['overwrite'] = false;
				$config['file_name'] = $downloadin;
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload())
				{
					$data = array(
								'Nama' => strtoupper($this->input->post('Nama')),
								'Tahun' => $this->input->post('Tahun'),
								'FormatNoAplikan' => $this->input->post('FormatNoAplikan'),
								'DigitNoAplikan' => $this->input->post('DigitNoAplikan'),
								'FormatNoPMB' => $this->input->post('FormatNoPMB'),
								'DigitNoPMB' => $this->input->post('DigitNoPMB'),
								'ol' => $ol,
								'NA' => $NA,
								'soal' => $this->input->post('soal'),
								'soal2' => $this->input->post('soal2'),
								'TglInterview' => $this->input->post('TglInterview'),
								'Interviewer' => $this->input->post('Interviewer'),
								'TglMulai' => $wordd[0],
								'TglSelesai' => $wordd[1],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_pmb->PTL_update($PMBPeriodID,$data);
					$error = $this->upload->display_errors('','.');
					echo warning("MESSAGE: Data in addition to the file has been successfully changed. ".$error,"../pmb");
				}
				else
				{
					$file_order = $this->input->post('file_order');
					if($file_order != '')
					{
						$direktori = substr($file_order,0,7);
						$file = substr($file_order,8);
						unlink("./ptl_storage/soal/".$direktori."/".$file);
					}
					$data = array(
								'Nama' => strtoupper($this->input->post('Nama')),
								'Tahun' => $this->input->post('Tahun'),
								'FormatNoAplikan' => $this->input->post('FormatNoAplikan'),
								'DigitNoAplikan' => $this->input->post('DigitNoAplikan'),
								'FormatNoPMB' => $this->input->post('FormatNoPMB'),
								'DigitNoPMB' => $this->input->post('DigitNoPMB'),
								'ol' => $ol,
								'NA' => $NA,
								'soal' => $this->input->post('soal'),
								'soal1' => $dwn,
								'soal2' => $this->input->post('soal2'),
								'TglInterview' => $this->input->post('TglInterview'),
								'Interviewer' => $this->input->post('Interviewer'),
								'TglMulai' => $wordd[0],
								'TglSelesai' => $wordd[1],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_pmb->PTL_update($PMBPeriodID,$data);
					echo warning("Data ADMISSION PERIOD with CODE '".$PMBPeriodID."' successfully changed.","../pmb");
				}
			}
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$PMBPeriodID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_pmb->PTL_update($PMBPeriodID,$data);
			echo warning("Data ADMISSION PERIOD with CODE '".$PMBPeriodID."' successfully disabled.","../pmb");
		}
	}
?>