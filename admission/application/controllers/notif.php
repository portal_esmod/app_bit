<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Notif extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_akses');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','');
			$data['rowrecord'] = $this->m_aplikan_log->PTL_all_notif_list();
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_notif',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_insert()
		{
			$id_akun = $this->input->post('id_akun');
			$da = array(
						'id_akun' => $this->input->post('id_akun'),
						'presenter' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"],
						'nama' => $this->input->post('nama'),
						'dari' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"],
						'untuk' => $this->input->post('untuk'),
						'aktivitas' => $this->input->post('aktivitas'),
						'tanggal' => $this->waktu
						);
			$this->m_aplikan_log->PTL_insert($da);
			redirect("notif/ptl_edit/$id_akun");
		}
		
		function ptl_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','');
			$kodelog = $this->uri->segment(3);
			$result = $this->m_aplikan_log->PTL_select($kodelog);
			$data['kodelog'] = $result['kodelog'];
			$data['id_akun'] = $result['id_akun'];
			$data['presenter'] = $result['presenter'];
			$data['nama'] = $result['nama'];
			$data['aktivitas'] = $result['aktivitas'];
			$data['tanggal'] = $result['tanggal'];
			$data['status'] = $result['status'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_notif_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','');
			$id_akun = $this->uri->segment(3);
			$data['id_akun'] = $id_akun;
			$data['rowrecord'] = $this->m_aplikan_log->PTL_all_chat($id_akun);
			
			$datachat = array(
						'tanggal' => $this->waktu,
						'status' => 'READ'
						);
			$this->m_aplikan_log->PTL_update_chat($id_akun,$datachat);
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_notif_messages',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$kodelog =  $this->input->post('kodelog');
			$data = array(
						'status' => 'READ'
						);
			$this->m_aplikan_log->PTL_update($kodelog,$data);
			redirect("notif");
		}
		
		function ptl_update_chat_online()
		{
			$this->authentification();
			$id_akun2 = $this->uri->segment(3);
			$id_akun =  $_COOKIE["id_akun"];
			$dataa = array(
						'last_login' => $this->waktu
						);
			$this->m_akses->PTL_update_2($id_akun,$dataa);
			redirect("notif/ptl_edit/$id_akun2");
		}
		
		function ptl_update_chat_offline()
		{
			$this->authentification();
			$id_akun2 = $this->uri->segment(3);
			$id_akun =  $_COOKIE["id_akun"];
			$dataa = array(
						'last_login' => ''
						);
			$this->m_akses->PTL_update_2($id_akun,$dataa);
			redirect("notif/ptl_edit/$id_akun2");
		}
	}
?>