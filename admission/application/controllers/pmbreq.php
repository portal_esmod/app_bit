<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Pmbreq extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_pmbreq');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$data['rowrecord'] = $this->m_pmbreq->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbreq',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_pmbreq_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => $this->input->post('Nama'),
						'Keterangan' => $this->input->post('Keterangan'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_pmbreq->PTL_insert($data);
			echo warning("Data PMB REQUIREMENT successfully added.","../pmbreq");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$PMBSyaratID = $this->uri->segment(3);
			$result = $this->m_pmbreq->PTL_select($PMBSyaratID);
			$data['PMBSyaratID'] = $result['PMBSyaratID'];
			$data['Urutan'] = $result['Urutan'];
			$data['Nama'] = $result['Nama'];
			$data['Keterangan'] = $result['Keterangan'];
			$data['NA'] = $result['NA'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_pmbreq_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$PMBSyaratID = $this->input->post('PMBSyaratID');
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => $this->input->post('Nama'),
						'Keterangan' => $this->input->post('Keterangan'),
						'NA' => $this->input->post('NA'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_pmbreq->PTL_update($PMBSyaratID,$data);
			echo warning("Data PMB REQUIREMENT with CODE '".$PMBSyaratID."' successfully changed.","../pmbreq");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$PMBSyaratID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y',
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu
						);
			$this->m_pmbreq->PTL_update($PMBSyaratID,$data);
			echo warning("Data PMB REQUIREMENT with CODE '".$PMBSyaratID."' successfully deleted.","../pmbreq");
		}
	}
?>