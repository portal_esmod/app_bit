<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Postpone extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('finance');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_psp_periode()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('psp_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('psp_filter_periode','');
			}
			redirect("postpone");
		}
		
		function ptl_filter_psp_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('psp_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('psp_filter_marketing','');
			}
			redirect("postpone");
		}
		
		function ptl_filter_psp_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			if($cektype != "")
			{
				$this->session->set_userdata('psp_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('psp_filter_type','');
			}
			redirect("postpone");
		}
		
		function ptl_filter_psp_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			if($cekstatus != "")
			{
				$this->session->set_userdata('psp_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('psp_filter_status','');
			}
			redirect("postpone");
		}
		
		function ptl_filter_psp_program()
		{
			$this->authentification();
			$cekpogram = $this->input->post('cekpogram');
			if($cekpogram != "")
			{
				$this->session->set_userdata('psp_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('psp_filter_program','');
			}
			redirect("postpone");
		}
		
		function ptl_filter_psp_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('psp_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('psp_filter_tahun','');
			}
			redirect("postpone");
		}
		
		function index()
		{
			$this->session->set_userdata('menu','postpone');
			$cekperiode = $this->session->userdata('psp_filter_periode');
			$cekmarketing = $this->session->userdata('psp_filter_marketing');
			$cektype = $this->session->userdata('psp_filter_type');
			$cekstatus = $this->session->userdata('psp_filter_status');
			$cekpogram = $this->session->userdata('psp_filter_program');
			$cektahun = $this->session->userdata('psp_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_postpone_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Applicant/v_postpone',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_semester_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/pembayaran/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant/ptl_semester');
			}
		}
	}
?>