<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Pmbform extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
			$this->load->model('m_pmbform');
			$this->load->model('m_pmbform_biaya');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$data['rowrecord'] = $this->m_pmbform->PTL_all();
			$data['rowrecord2'] = $this->m_pmbform->PTL_all_sc();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbform',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbform_form2');
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$KursusSingkatID = $this->input->post('KursusSingkatID');
			$result = $this->m_pmbform->PTL_select_sc($KursusSingkatID);
			if($result)
			{
				echo warning("Data ADMISSION FORM with CODE '".$KursusSingkatID."' has been used. Try another code.","../pmbform/ptl_form");
			}
			else
			{
				$data = array(
							'ProgramID' => $this->input->post('ProgramID'),
							'KursusSingkatID' => $this->input->post('KursusSingkatID'),
							'Nama' => $this->input->post('Nama'),
							'harga_formulir' => $this->input->post('harga_formulir'),
							'biaya' => $this->input->post('biaya'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_pmbform->PTL_insert2($data);
				echo warning("Data ADMISSION FORM with CODE '".$KursusSingkatID."' successfully added.","../pmbform");
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$ProdiID = $this->uri->segment(3);
			$result = $this->m_pmbform->PTL_select($ProdiID);
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['Nama'] = $result['Nama'];
			$data['Jenjang'] = $result['Jenjang'];
			$data['harga_formulir'] = $result['harga_formulir'];
			$data['biaya_awal'] = $result['biaya_awal'];
			$data['minimal_pembayaran'] = $result['minimal_pembayaran'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$data['rowrecord'] = $this->m_pmbform_biaya->PTL_all($ProdiID);
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbform_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_addcost()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$data['ProdiID'] = $this->uri->segment(3);
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_pmbform_cost',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_addcost_insert()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$ProdiID = $this->input->post('ProdiID');
			$data = array(
						'ProdiID' => $ProdiID,
						'nama' => $this->input->post('nama'),
						'catatan' => $this->input->post('catatan'),
						'biaya' => $this->input->post('biaya'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_pmbform_biaya->PTL_insert($data);
			$result = $this->m_pmbform->PTL_select($ProdiID);
			if($result)
			{
				echo warning("Data VARIABLE COST 1ST SEMESTER for CODE '".$ProdiID."' successfully added.","../pmbform/ptl_edit/$ProdiID");
			}
			else
			{
				echo warning("Data VARIABLE COST 1ST SEMESTER for CODE '".$ProdiID."' successfully added.","../pmbform/ptl_edit2/$ProdiID");
			}
		}
		
		function ptl_addcost_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$id_prodi_biaya = $this->uri->segment(3);
			$result = $this->m_pmbform_biaya->PTL_select($id_prodi_biaya);
			$data['id_prodi_biaya'] = $result['id_prodi_biaya'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['nama'] = $result['nama'];
			$data['catatan'] = $result['catatan'];
			$data['biaya'] = $result['biaya'];
			$data['na'] = $result['na'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbform_cost_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_addcost_update()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$id_prodi_biaya = $this->input->post('id_prodi_biaya');
			$ProdiID = $this->input->post('ProdiID');
			$data = array(
						'ProdiID' => $this->input->post('ProdiID'),
						'nama' => $this->input->post('nama'),
						'catatan' => $this->input->post('catatan'),
						'biaya' => $this->input->post('biaya'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'na' => $this->input->post('na')
						);
			$this->m_pmbform_biaya->PTL_update($id_prodi_biaya,$data);
			$result = $this->m_pmbform->PTL_select($ProdiID);
			if($result)
			{
				echo warning("Data VARIABLE COST 1ST SEMESTER for CODE '".$ProdiID."' successfully changed.","../pmbform/ptl_edit/$ProdiID");
			}
			else
			{
				echo warning("Data VARIABLE COST 1ST SEMESTER for CODE '".$ProdiID."' successfully changed.","../pmbform/ptl_edit2/$ProdiID");
			}
		}
		
		function ptl_addcost_delete()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$wordd = explode("-",$this->uri->segment(3));
			$id_prodi_biaya = $wordd[1];
			$ProdiID = $wordd[0];
			$data = array(
						'na' => 'Y'
						);
			$this->m_pmbform_biaya->PTL_update($id_prodi_biaya,$data);
			echo warning("Data VARIABLE COST 1ST SEMESTER with CODE '".$id_prodi_biaya."' successfully deleted.","../pmbform/ptl_edit/$ProdiID");
		}
		
		function ptl_edit2()
		{
			$this->authentification();
			$this->session->set_userdata('menu','setup');
			$KursusSingkatID = $this->uri->segment(3);
			$ProdiID = $this->uri->segment(3);
			$result = $this->m_pmbform->PTL_select_sc($KursusSingkatID);
			$data['ProgramID'] = $result['ProgramID'];
			$data['KursusSingkatID'] = $result['KursusSingkatID'];
			$data['ProdiID'] = $result['KursusSingkatID'];
			$data['Nama'] = $result['Nama'];
			$data['Jenjang'] = "Short Course";
			$data['harga_formulir'] = $result['harga_formulir'];
			$data['biaya_awal'] = $result['Biaya'];
			$data['NA'] = $result['NA'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$data['rowrecord'] = $this->m_pmbform_biaya->PTL_all($ProdiID);
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Setup/v_pmbform_edit2',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$ProdiID = $this->input->post('ProdiID');
			$data = array(
						'harga_formulir' => $this->input->post('harga_formulir'),
						'biaya_awal' => $this->input->post('biaya_awal'),
						'minimal_pembayaran' => $this->input->post('minimal_pembayaran'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_pmbform->PTL_update($ProdiID,$data);
			echo warning("Data ADMISSION FORM with CODE '".$ProdiID."' successfully changed.","../pmbform");
		}
		
		function ptl_update2()
		{
			$this->authentification();
			$KursusSingkatID = $this->input->post('KursusSingkatID');
			$NA = "N";
			if($this->input->post('NA') == "Y")
			{
				$NA = "Y";
			}
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'harga_formulir' => $this->input->post('harga_formulir'),
						'Biaya' => $this->input->post('biaya_awal'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $NA
						);
			$this->m_pmbform->PTL_update2($KursusSingkatID,$data);
			echo warning("Data ADMISSION FORM with CODE '".$KursusSingkatID."' successfully changed.","../pmbform");
		}
	}
?>