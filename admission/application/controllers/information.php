<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Information extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aplikan_log');
			$this->load->model('m_information');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$data['rowrecord'] = $this->m_information->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Additional/v_information',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_information_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => $this->input->post('Nama'),
						'NA' => $this->input->post('NA')
						);
			$this->m_information->PTL_insert($data);
			echo warning("Data INFORMATION successfully added.","../information");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$InfoID = $this->uri->segment(3);
			$result = $this->m_information->PTL_select($InfoID);
			$data['InfoID'] = $result['InfoID'];
			$data['Urutan'] = $result['Urutan'];
			$data['Nama'] = $result['Nama'];
			$data['NA'] = $result['NA'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Additional/v_information_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$InfoID = $this->input->post('InfoID');
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => $this->input->post('Nama'),
						'Urutan' => $this->input->post('Urutan'),
						'NA' => $this->input->post('NA')
						);
			$this->m_information->PTL_update($InfoID,$data);
			echo warning("Data INFORMATION with CODE '".$InfoID."' successfully changed.","../information");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$InfoID = $this->uri->segment(3);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_information->PTL_update($InfoID,$data);
			echo warning("Data INFORMATION with CODE '".$InfoID."' successfully deleted.","../information");
		}
	}
?>