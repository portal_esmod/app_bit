<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Import extends CI_Controller
	{
		public $nama_tabel = 'cr_lead';
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->tanggal_saja = gmdate("Y-m-d", time()-($ms));
			$this->tanggal = gmdate("y-m-d, H.i.s", time()-($ms));
			$this->sesi = gmdate("YmdHis", time()-($ms));
			$this->lead = gmdate("ym", time()-($ms));
			$this->load->library('log');
			$this->load->library('PHPExcel');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_crm";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/fhm_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','import');
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Import/v_import',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function fhm_import_exe()
		{
			$this->authentification();
			$download = $_FILES['userfile']['name'];
			$downloadin = str_replace(' ','_',$download);
			$config['upload_path'] = './fhm_storage/temp/';
			$config['allowed_types'] = '*';
			$config['max_size']	= 10000;
			$this->load->library('upload',$config);
			if(! $this->upload->do_upload())
			{
				$pesan = $this->upload->display_errors();
				echo warning("Sorry, ".$pesan,"../import");
			}
			else
			{
				$data = array('error' => false);
				$upload_data = $this->upload->data();
				$this->load->library('excel_reader');
				$this->excel_reader->setOutputEncoding('CP1251');
				$file =  $upload_data['full_path'];
				$this->excel_reader->read($file);
				error_reporting(E_ALL ^ E_NOTICE);
				$data = $this->excel_reader->sheets[0] ;
				$dataarray = Array();
				$totdup = 0;
				for($i = 1; $i <= $data['numRows']; $i++)
				{
					if($data['cells'][$i][1] == '') break;
					{
						$tanggal_distribusi = $data['cells'][$i][2];
						$tanggal_terima = $data['cells'][$i][3];
						$nama = $data['cells'][$i][4];
						$mobile1 = str_replace(' ','',$data['cells'][$i][5]);
						$mobile2 = str_replace(' ','',$data['cells'][$i][6]);
						$email1 = str_replace(' ','',$data['cells'][$i][7]);
						$email2 = str_replace(' ','',$data['cells'][$i][8]);
						$jenjang = $data['cells'][$i][9];;
						$sumber_data = $data['cells'][$i][10];
						$catatan = $data['cells'][$i][11];
						$nama_marketing = $data['cells'][$i][12];
						$minat = $data['cells'][$i][13];
						$pertanyaan = $data['cells'][$i][14];
						$totduplicate = 0;
						$duplicate = $this->m_lead->FHM_select_duplicate($mobile1,$email1);
						// $dup1 = $this->m_lead->FHM_select_duplicate1($mobile1);
						// if($dup1)
						// {
							// $duplicate = $dup1;
							// $totduplicate++;
						// }
						// $dup2 = $this->m_lead->FHM_select_duplicate2($mobile2);
						// if($dup2)
						// {
							// $duplicate = $dup2;
							// $totduplicate++;
						// }
						// $dup3 = $this->m_lead->FHM_select_duplicate3($email1);
						// if($dup3)
						// {
							// $duplicate = $dup3;
							// $totduplicate++;
						// }
						// $dup4 = $this->m_lead->FHM_select_duplicate4($email2);
						// if($dup4)
						// {
							// $duplicate = $dup4;
							// $totduplicate++;
						// }
						if($duplicate)
						{
							$data_duplicate = array(
													'kode_lead' => $duplicate['kode_lead'],
													'PMBPeriodID' => $duplicate['PMBPeriodID'],
													'id_import' => $duplicate['id_import'],
													'SupervisorID' => $duplicate['SupervisorID'],
													'PresenterID' => $duplicate['PresenterID'],
													'nama' => strtoupper($duplicate['nama']),
													'mobile1' => $duplicate['mobile1'],
													'mobile2' => $duplicate['mobile2'],
													'email1' => strtolower($duplicate['email1']),
													'email2' => strtolower($duplicate['email2']),
													'jenjang' => $duplicate['jenjang'],
													'sumber_data' => strtoupper($data['cells'][$i][10]), //strtoupper($duplicate['sumber_data']),
													'minat' => $data['cells'][$i][13],
													'pertanyaan' => $data['cells'][$i][14],
													'sumber_data_detail' => $duplicate['sumber_data_detail'],
													'nama_marketing' => strtoupper($duplicate['nama_marketing']),
													'tanggal_expired' => $duplicate['tanggal_expired'],
													'login_terima' => $duplicate['login_terima'],
													'tanggal_terima' => $duplicate['tanggal_terima'],
													'login_distribusi' => $duplicate['login_distribusi'],
													'tanggal_distribusi' => $duplicate['tanggal_distribusi'],
													'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
													'tanggal_buat' => $this->waktu
													);
							$this->m_lead_duplicate->FHM_insert($data_duplicate);
							$dataarray[$i-1]['id_import'] = "";
							$totdup++;
						}
						else
						{
							$word = explode("/",$data['cells'][$i][3]);
							if(($word == "") OR ($word == "-"))
							{
								$tanggal_terima = $this->tanggal_saja;
							}
							else
							{
								$tanggal_terima = $word[2]."-".$word[1]."-".($word[0] - 1);
							}
							$dataarray[$i-1]['id_import'] = $this->sesi;
							$dataarray[$i-1]['nama'] = strtoupper($data['cells'][$i][4]);
							$dataarray[$i-1]['mobile1'] = str_replace(' ','',$data['cells'][$i][5]);
							$dataarray[$i-1]['mobile2'] = str_replace(' ','',$data['cells'][$i][6]);
							$dataarray[$i-1]['email1'] = str_replace(' ','',strtolower($data['cells'][$i][7]));
							$dataarray[$i-1]['email2'] = str_replace(' ','',strtolower($data['cells'][$i][8]));
							$dataarray[$i-1]['jenjang'] = $data['cells'][$i][9];
							$dataarray[$i-1]['sumber_data'] = strtoupper($data['cells'][$i][10]);
							$dataarray[$i-1]['minat'] = $data['cells'][$i][13];
							$dataarray[$i-1]['pertanyaan'] = $data['cells'][$i][14];
							$dataarray[$i-1]['sumber_data_detail'] = $data['cells'][$i][11];
							$dataarray[$i-1]['nama_marketing'] = strtoupper($data['cells'][$i][12]);
							$dataarray[$i-1]['tanggal_terima'] = $tanggal_terima;
							$dataarray[$i-1]['tanggal_distribusi'] = $data['cells'][$i][2];
							$dataarray[$i-1]['login_buat'] = $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"];
							$dataarray[$i-1]['tanggal_buat'] = $this->waktu;
						}
					}
				}
				unlink("./fhm_storage/temp/".$downloadin);
				$this->m_lead->FHM_import_exe($dataarray);
				$total = $i - 2 - $totdup;
			}
			$pesan = "";
			if($total > 0)
			{
				$pesan .= "Successfully import total $total Leads to database. ";
			}
			if($totdup > 0)
			{
				$pesan .= "$totdup data duplicate can not entry to database.";
			}
			echo warning($pesan,"../import/fhm_check_dup1");
		}
	}
?>