<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Interview extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->library('log');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_wawancara');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_int_periode()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('int_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('int_filter_periode','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('int_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('int_filter_marketing','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			if($cektype != "")
			{
				$this->session->set_userdata('int_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('int_filter_type','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			if($cekstatus != "")
			{
				$this->session->set_userdata('int_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('int_filter_status','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_program()
		{
			$this->authentification();
			$cekpogram = $this->input->post('cekpogram');
			if($cekpogram != "")
			{
				$this->session->set_userdata('int_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('int_filter_program','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('int_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('int_filter_tahun','');
			}
			redirect("interview");
		}
		
		function index()
		{
			$this->session->set_userdata('menu','interview');
			$cekperiode = $this->session->userdata('int_filter_periode');
			$cekmarketing = $this->session->userdata('int_filter_marketing');
			$cektype = $this->session->userdata('int_filter_type');
			$cekstatus = $this->session->userdata('int_filter_status');
			$cekpogram = $this->session->userdata('int_filter_program');
			$cektahun = $this->session->userdata('int_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_interview_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Interview/v_interview',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','interview');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$data['Email'] = $result['Email'];
			$data['nilai_interview'] = $result['nilai_interview'];
			$data['hasil_interview'] = $result['hasil_interview'];
			
			$data['wawancara'] = $this->m_wawancara->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Interview/v_interview_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			if($this->input->post('hasil_interview') == "Y")
			{
				$hasil_interview = "Y";
			}
			else
			{
				$hasil_interview = "N";
			}
			$data = array(
						'nilai_interview' => $this->input->post('nilai_interview'),
						'hasil_interview' => $hasil_interview,
						'korektor_interview' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'INTERVIEW: Your interview has been done'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR INTERVIEW HAVE BEEN DONE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ADMISSION</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your interview has been done.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Data INTERVIEW with APPLICANT CODE '".$AplikanID."' successfully updated.","../interview");
			}
			else
			{
				echo warning('Email server is not active. Please try again...','../interview');
			}
		}
	}
?>