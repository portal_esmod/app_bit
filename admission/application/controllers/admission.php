<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Admission extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->library('fpdf');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_adm_periode()
		{
			$this->authentification();
			$link = $this->input->post('cekperiode');
			$word = explode("_",$link);
			$back = $word[1];
			$cekperiode = $word[0];
			if($cekperiode != "")
			{
				$this->session->set_userdata('adm_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('adm_filter_periode','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function ptl_filter_adm_marketing()
		{
			$this->authentification();
			$link = $this->input->post('cekmarketing');
			$word = explode("_",$link);
			$back = $word[1];
			$cekmarketing = $word[0];
			if($cekmarketing != "")
			{
				$this->session->set_userdata('adm_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('adm_filter_marketing','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function ptl_filter_adm_type()
		{
			$this->authentification();
			$link = $this->input->post('cektype');
			$word = explode("_",$link);
			$back = $word[1];
			$cektype = $word[0];
			if($cektype != "")
			{
				$this->session->set_userdata('adm_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('adm_filter_type','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function ptl_filter_adm_status()
		{
			$this->authentification();
			$link = $this->input->post('cekstatus');
			$word = explode("_",$link);
			$back = $word[1];
			$cekstatus = $word[0];
			if($cekstatus != "")
			{
				$this->session->set_userdata('adm_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('adm_filter_status','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function ptl_filter_adm_program()
		{
			$this->authentification();
			$link = $this->input->post('cekpogram');
			$word = explode("_",$link);
			$back = $word[1];
			$cekpogram = $word[0];
			if($cekpogram != "")
			{
				$this->session->set_userdata('adm_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('adm_filter_program','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function ptl_filter_adm_tahun()
		{
			$this->authentification();
			$link = $this->input->post('cektahun');
			$word = explode("_",$link);
			$back = $word[1];
			$cektahun = $word[0];
			if($cektahun != "")
			{
				$this->session->set_userdata('adm_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('adm_filter_tahun','');
			}
			if($back == "REG")
			{
				redirect("admission");
			}
			if($back == "SC")
			{
				redirect("admission/ptl_short_course");
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','admission');
			$cekperiode = $this->session->userdata('adm_filter_periode');
			$cekmarketing = $this->session->userdata('adm_filter_marketing');
			$cektype = $this->session->userdata('adm_filter_type');
			$cekstatus = $this->session->userdata('adm_filter_status');
			$cekpogram = $this->session->userdata('adm_filter_program');
			$cektahun = $this->session->userdata('adm_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_reguler_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Admission/v_admission_reg',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_short_course()
		{
			$this->authentification();
			$this->session->set_userdata('menu','admission');
			$cekperiode = $this->session->userdata('adm_filter_periode');
			$cekmarketing = $this->session->userdata('adm_filter_marketing');
			$cektype = $this->session->userdata('adm_filter_type');
			$cekstatus = $this->session->userdata('adm_filter_status');
			$cekpogram = $this->session->userdata('adm_filter_program');
			$cektahun = $this->session->userdata('adm_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_sc_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Admission/v_admission_sc',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf_public()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$menu = $this->uri->segment(3);
			if($menu == "PROS")
			{
				$cekperiode = $this->session->userdata('pros_filter_periode');
				$cekmarketing = $this->session->userdata('pros_filter_marketing');
				$cektype = $this->session->userdata('pros_filter_type');
				$cekstatus = $this->session->userdata('pros_filter_status');
				$cekpogram = $this->session->userdata('pros_filter_program');
				$cektahun = $this->session->userdata('pros_filter_tahun');
				$cekbulan = $this->session->userdata('pros_filter_month');
				$result = $this->m_aplikan->PTL_all_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun,$cekbulan);
				$title = "PROSPECTS";
			}
			if($menu == "PYM")
			{
				$cekperiode = $this->session->userdata('apl_filter_periode');
				$cekmarketing = $this->session->userdata('apl_filter_marketing');
				$cektype = $this->session->userdata('apl_filter_type');
				$cekstatus = $this->session->userdata('apl_filter_status');
				$cekpogram = $this->session->userdata('apl_filter_program');
				$cektahun = $this->session->userdata('apl_filter_tahun');
				$result = $this->m_aplikan->PTL_all_payment_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "PAYMENT";
			}
			if($menu == "OLT")
			{
				$cekperiode = $this->session->userdata('apl_filter_periode');
				$cekmarketing = $this->session->userdata('apl_filter_marketing');
				$cektype = $this->session->userdata('apl_filter_type');
				$cekstatus = $this->session->userdata('apl_filter_status');
				$cekpogram = $this->session->userdata('apl_filter_program');
				$cektahun = $this->session->userdata('apl_filter_tahun');
				$result = $this->m_aplikan->PTL_all_test_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "ONLINE TEST";
			}
			if($menu == "CPT")
			{
				$cekperiode = $this->session->userdata('apl_filter_periode');
				$cekmarketing = $this->session->userdata('apl_filter_marketing');
				$cektype = $this->session->userdata('apl_filter_type');
				$cekstatus = $this->session->userdata('apl_filter_status');
				$cekpogram = $this->session->userdata('apl_filter_program');
				$cektahun = $this->session->userdata('apl_filter_tahun');
				$result = $this->m_aplikan->PTL_all_completeness_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "COMPLETENESS";
			}
			if($menu == "PIS")
			{
				$cekperiode = $this->session->userdata('apl_filter_periode');
				$cekmarketing = $this->session->userdata('apl_filter_marketing');
				$cektype = $this->session->userdata('apl_filter_type');
				$cekstatus = $this->session->userdata('apl_filter_status');
				$cekpogram = $this->session->userdata('apl_filter_program');
				$cektahun = $this->session->userdata('apl_filter_tahun');
				$result = $this->m_aplikan->PTL_all_semester_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "PAYMENT 1ST SEMESTER";
			}
			if($menu == "INT")
			{
				$cekperiode = $this->session->userdata('int_filter_periode');
				$cekmarketing = $this->session->userdata('int_filter_marketing');
				$cektype = $this->session->userdata('int_filter_type');
				$cekstatus = $this->session->userdata('int_filter_status');
				$cekpogram = $this->session->userdata('int_filter_program');
				$cektahun = $this->session->userdata('int_filter_tahun');
				$result = $this->m_aplikan->PTL_all_interview_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "INTERVIEW";
			}
			if($menu == "PSP")
			{
				$cekperiode = $this->session->userdata('int_filter_periode');
				$cekmarketing = $this->session->userdata('int_filter_marketing');
				$cektype = $this->session->userdata('int_filter_type');
				$cekstatus = $this->session->userdata('int_filter_status');
				$cekpogram = $this->session->userdata('int_filter_program');
				$cektahun = $this->session->userdata('int_filter_tahun');
				$result = $this->m_aplikan->PTL_all_postpone_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "POSTPONE";
			}
			if($menu == "REG")
			{
				$cekperiode = $this->session->userdata('adm_filter_periode');
				$cekmarketing = $this->session->userdata('adm_filter_marketing');
				$cektype = $this->session->userdata('adm_filter_type');
				$cekstatus = $this->session->userdata('adm_filter_status');
				$cekpogram = $this->session->userdata('adm_filter_program');
				$cektahun = $this->session->userdata('adm_filter_tahun');
				$result = $this->m_aplikan->PTL_all_reguler_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "REGULAR";
			}
			if($menu == "SC")
			{
				$cekperiode = $this->session->userdata('adm_filter_periode');
				$cekmarketing = $this->session->userdata('adm_filter_marketing');
				$cektype = $this->session->userdata('adm_filter_type');
				$cekstatus = $this->session->userdata('adm_filter_status');
				$cekpogram = $this->session->userdata('adm_filter_program');
				$cektahun = $this->session->userdata('adm_filter_tahun');
				$result = $this->m_aplikan->PTL_all_sc_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
				$title = "SHORT COURSE";
			}
			$filter = @$cekperiode." - ".@$cekmarketing." - ".@$cektype." - ".@$cekstatus." - ".@$cekpogram." - ".@$cektahun;
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(29,0.7,$title,0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(29,0.5,"MARKETING",0,0,"C");
			$this->fpdf->Line(1,2.5,28.5,2.5);
			$this->fpdf->Line(1,2.55,28.5,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(2.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(2.5 , 0, "NIK" , 0, "", "L");
			$this->fpdf->Cell(2 , 0, ": ".$id_akun , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(2.5 , -1, "NAME", 0, "", "L");
			$this->fpdf->Cell(2 , -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(2.5 , -2, "FILTER", 0, "", "L");
			$this->fpdf->Cell(1 , -2, ": ".$filter, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(29,-2,"DETAIL",0,0,"C");
			$this->fpdf->Ln(-0.5);
			$this->fpdf->SetFont("Times","B",8);
			$this->fpdf->Cell(0.5 , 0.7, "NO" , "LTB", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "APL ID" , "LTB", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "PMB ID" , "LTB", 0, "C");
			$this->fpdf->Cell(1 , 0.7, "PRD" , "LTB", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "PRESENTER" , "LTB", 0, "C");
			$this->fpdf->Cell(4 , 0.7, "NAME" , "LTB", 0, "C");
			$this->fpdf->Cell(1 , 0.7, "PRG" , "LTB", 0, "C");
			$this->fpdf->Cell(1.5 , 0.7, "PRD" , "LTB", 0, "C");
			$this->fpdf->Cell(0.5 , 0.7, "G" , "LTB", 0, "C");
			$this->fpdf->Cell(0.5 , 0.7, "A" , "LTB", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "LAST SCH" , "LTB", 0, "C");
			$this->fpdf->Cell(1 , 0.7, "SP" , "LTB", 0, "C");
			$this->fpdf->Cell(1.5 , 0.7, "FORM" , "LTB", 0, "C");
			$this->fpdf->Cell(1.5 , 0.7, "COST" , "LTB", 0, "C");
			$this->fpdf->Cell(1.5 , 0.7, "PYM" , "LTB", 0, "C");
			$this->fpdf->Cell(2.5 , 0.7, "DATE" , "LTBR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$this->fpdf->SetFont("Times","",8);
					if($no == $tot)
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.4, $no, "LB", 0, "C");
						$this->fpdf->Cell(2 , 0.4, $r->AplikanID, "LB", 0, "L");
						$this->fpdf->Cell(2 , 0.4, $r->PMBID, "LB", 0, "L");
						$this->fpdf->Cell(1 , 0.4, $r->PMBPeriodID, "LB", 0, "L");
						$this->fpdf->Cell(2 , 0.4, $r->PresenterID, "LB", 0, "L");
						$this->fpdf->Cell(4 , 0.4, $r->Nama, "LB", 0, "L");
						$this->fpdf->Cell(1 , 0.4, $r->ProgramID, "LB", 0, "C");
						$this->fpdf->Cell(1.5 , 0.4, $r->ProdiID, "LB", 0, "C");
						$this->fpdf->Cell(0.5 , 0.4, $r->Kelamin, "LB", 0, "C");
						$this->fpdf->Cell(0.5 , 0.4, $r->umur, "LB", 0, "C");
						$this->fpdf->Cell(2 , 0.4, $r->PendidikanTerakhir, "LB", 0, "C");
						$this->fpdf->Cell(1 , 0.4, $r->StatusProspekID, "LB", 0, "C");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->Harga), "LB", 0, "R");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->total_biaya), "LB", 0, "R");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->total_bayar), "LB", 0, "R");
						$this->fpdf->Cell(2.5 , 0.4, $r->tanggal_buat, "LBR", 0, "C");
					}
					else
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.4, $no, "L", 0, "C");
						$this->fpdf->Cell(2 , 0.4, $r->AplikanID, "L", 0, "L");
						$this->fpdf->Cell(2 , 0.4, $r->PMBID, "L", 0, "L");
						$this->fpdf->Cell(1 , 0.4, $r->PMBPeriodID, "L", 0, "L");
						$this->fpdf->Cell(2 , 0.4, $r->PresenterID, "L", 0, "L");
						$this->fpdf->Cell(4 , 0.4, $r->Nama, "L", 0, "L");
						$this->fpdf->Cell(1 , 0.4, $r->ProgramID, "L", 0, "C");
						$this->fpdf->Cell(1.5 , 0.4, $r->ProdiID, "L", 0, "C");
						$this->fpdf->Cell(0.5 , 0.4, $r->Kelamin, "L", 0, "C");
						$this->fpdf->Cell(0.5 , 0.4, $r->umur, "L", 0, "C");
						$this->fpdf->Cell(2 , 0.4, $r->PendidikanTerakhir, "L", 0, "C");
						$this->fpdf->Cell(1 , 0.4, $r->StatusProspekID, "L", 0, "C");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->Harga), "L", 0, "R");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->total_biaya), "L", 0, "R");
						$this->fpdf->Cell(1.5 , 0.4, formatRupiah2($r->total_bayar), "L", 0, "R");
						$this->fpdf->Cell(2.5 , 0.4, $r->tanggal_buat, "LR", 0, "C");

					}
					$no++;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(25 , 0.4, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "AMARILLA NIRMALA", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "MARKETING DEPT.", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s")."",0,"LR","L");
			$this->fpdf->Cell(18.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output("laporan_".@$ses_bulan." - ".@$ses_tahun." BY LENDRA.pdf","I");
		}
	}
?>