<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Charts extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
		}
		
		function index()
		{
			$this->session->set_userdata('menu','charts');
			$this->load->view('Portal/v_header_charts');
			$this->load->view('Charts/v_prospects');
			$this->load->view('Portal/v_footer_charts');
		}
	}
?>