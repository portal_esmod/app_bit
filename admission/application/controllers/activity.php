<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Activity extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->model('m_aktivitas');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_akun');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup_nama()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_akun->lookup_nama($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->id_akun,
											'value' => $row->nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('activity/index',$data);
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','activity');
			$data['rowrecord'] = $this->m_aktivitas->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Activity/v_activity',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','activity');
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Activity/v_activity_form');
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/aktivitas/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "ACT_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '5120000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$error = $this->upload->display_errors('','.');
				echo warning("MESSAGE: ".$error,"../activity");
			}
			else
			{
				$TglMulaiTglSelesai = $this->input->post('TglMulaiTglSelesai');
				$wordd = explode(" - ", @$TglMulaiTglSelesai);
				$data = array(
							'nama_aktivitas' => $this->input->post('nama_aktivitas'),
							'isi' => $this->input->post('isi'),
							'file' => $dwn,
							'penanggung_jawab' => $this->input->post('penanggung_jawab'),
							'dari_tanggal' => $wordd[0],
							'sampai_tanggal' => $wordd[1],
							'publikasi' => $this->input->post('publikasi1').','.$this->input->post('publikasi2'),
							'status' => $this->input->post('status'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_aktivitas->PTL_insert($data);
				echo warning("Your ACTIVITY with NAME '".$this->input->post('nama_aktivitas')."' Successfully added.","../activity");
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','activity');
			$id_aktivitas = $this->uri->segment(3);
			$result = $this->m_aktivitas->PTL_select($id_aktivitas);
			$data['id_aktivitas'] = $result['id_aktivitas'];
			$data['nama_aktivitas'] = $result['nama_aktivitas'];
			$data['isi'] = $result['isi'];
			$data['file'] = $result['file'];
			$data['penanggung_jawab'] = $result['penanggung_jawab'];
			$data['dari_tanggal'] = $result['dari_tanggal'];
			$data['sampai_tanggal'] = $result['sampai_tanggal'];
			$data['status'] = $result['status'];
			$word = explode(",",$result['publikasi']);
			$data['publikasi1'] = @$word[0];
			$data['publikasi2'] = @$word[1];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Activity/v_activity_edit',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("./ptl_storage/aktivitas/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../activity');
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/aktivitas/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "ACT_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '5120000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$TglMulaiTglSelesai = $this->input->post('TglMulaiTglSelesai');
				$wordd = explode(" - ", @$TglMulaiTglSelesai);
				$id_aktivitas = $this->input->post('id_aktivitas');
				$data = array(
							'nama_aktivitas' => $this->input->post('nama_aktivitas'),
							'isi' => $this->input->post('isi'),
							'penanggung_jawab' => $this->input->post('penanggung_jawab'),
							'dari_tanggal' => $wordd[0],
							'sampai_tanggal' => $wordd[1],
							'publikasi' => $this->input->post('publikasi1').','.$this->input->post('publikasi2'),
							'status' => $this->input->post('status'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_aktivitas->PTL_update($id_aktivitas,$data);
				$error = $this->upload->display_errors('','.');
				echo warning("MESSAGE: Besides the data file has been successfully changed. ".$error,"../activity");
			}
			else
			{
				$file_order = $this->input->post('file_order');
				if($file_order != '')
				{
					$direktori = substr($file_order,0,7);
					$file = substr($file_order,8);
					unlink("./ptl_storage/aktivitas/".$direktori."/".$file);
				}
				$TglMulaiTglSelesai = $this->input->post('TglMulaiTglSelesai');
				$wordd = explode(" - ", @$TglMulaiTglSelesai);
				$id_aktivitas = $this->input->post('id_aktivitas');
				$data = array(
							'nama_aktivitas' => $this->input->post('nama_aktivitas'),
							'isi' => $this->input->post('isi'),
							'file' => $dwn,
							'penanggung_jawab' => $this->input->post('penanggung_jawab'),
							'dari_tanggal' => $wordd[0],
							'sampai_tanggal' => $wordd[1],
							'publikasi' => $this->input->post('publikasi1').','.$this->input->post('publikasi2'),
							'status' => $this->input->post('status'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_aktivitas->PTL_update($id_aktivitas,$data);
				echo warning("Your ACTIVITY with NAME '".$this->input->post('nama_aktivitas')."' Successfully updated.","../activity");
			}
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$id_aktivitas = $this->uri->segment(3);
			$data = array(
						'na' => 'Y',
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu
						);
			$this->m_aktivitas->PTL_update($id_aktivitas,$data);
			echo warning("Your ACTIVITY with CODE '".$id_aktivitas."' Successfully deleted.","../activity");
		}
	}
?>