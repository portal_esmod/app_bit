<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Today extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_mk');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi');
			$this->load->model('m_subjek');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_tanggal()
		{
			$this->authentification();
			$cektanggal = $this->input->post('cektanggal');
			if($cektanggal != "")
			{
				$this->session->set_userdata('today_filter_tanggal',$cektanggal);
			}
			else
			{
				$this->session->unset_userdata('today_filter_tanggal');
			}
			redirect("today");
		}
		
		function index()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-d", time()-($ms));
			$this->session->set_userdata('menu','schedule');
			$cektanggal = $this->session->userdata('today_filter_tanggal');
			if($cektanggal == "")
			{
				$data['today'] = $tanggal;
				$today = $tanggal;
			}
			else
			{
				$data['today'] = $cektanggal;
				$today = $cektanggal;
			}
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$data['rowrecord'] = $this->m_presensi->PTL_all_today($today);
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Today/v_today',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>