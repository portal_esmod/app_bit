		</div>
		<div id="footer">
			<p>&copy;  Esmod Jakarta &nbsp;2015 &nbsp;</p>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-2.0.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-ui.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/uniform/jquery.uniform.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/chosen/chosen.jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/tagsinput/jquery.tagsinput.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/validVal/js/jquery.validVal.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/daterangepicker/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/switch/static/js/bootstrap-switch.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jasny/js/bootstrap-inputmask.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/formsInit.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jasny/js/bootstrap-fileupload.js"></script>
        <script>
			$(function () { formInit(); });
			
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
        </script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>