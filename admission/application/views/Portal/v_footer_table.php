		</div>
		<div id="footer">
			<p>&copy;  Esmod Jakarta &nbsp;2015 &nbsp;</p>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-2.0.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/dataTables/jquery.dataTables.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/dataTables/dataTables.bootstrap.js"></script>
		<script>
			$(document).ready(function ()
			{
				$('#dataTables-example').dataTable();
			});
			
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
		</script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/zebra_datepicker.js"></script>
		<script>
			$('#Date1').Zebra_DatePicker({
				show_week_number: 'Wk',
				format: 'Y-m-d',
				onSelect : function (dateText, inst) {
					$('#formId').submit();
				}
			});
		</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>