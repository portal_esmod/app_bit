<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>Esmod Jakarta - Marketing</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/main.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/theme.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/MoneAdmin.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/Font-Awesome/css/font-awesome.css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/css/jquery-ui.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/uniform/themes/default/css/uniform.default.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/chosen/chosen.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/colorpicker/css/colorpicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/tagsinput/jquery.tagsinput.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/daterangepicker/daterangepicker-bs3.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/datepicker/css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/timepicker/css/bootstrap-timepicker.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/switch/static/stylesheets/bootstrap-switch.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap-fileupload.min.css" />
		
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116206017-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-116206017-1');
		</script>
	</head>
	<body class="padTop53 " >
		<div id="wrap" >
			<div id="top">
				<nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">
					<a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
						<i class="icon-align-justify"></i>
					</a>
					<header class="navbar-header">
						<a href="<?php echo base_url("login/ptl_home"); ?>" class="navbar-brand">
							<img src="<?php echo base_url(); ?>assets/dashboard/img/logo_header.png" alt="" />
						</a>
					</header>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<?php
									$no = 0;
									if($rowrecord)
									{
										foreach($rowrecord as $row)
										{
											if(($row->dari == "") AND ($row->untuk == ""))
											{
												$no++;
											}
											if($row->untuk == $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"])
											{
												$no++;
											}
										}
									}
								?>
								<span class="label label-success"><?php echo $no; ?></span>
								<i class="icon-envelope-alt"></i>&nbsp; <i class="icon-chevron-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-messages">
								<?php
									if($rowrecord)
									{
										$no = 0;
										foreach($rowrecord as $row)
										{
											if($no < 3)
											{
												if($row->presenter == "")
												{
													$not = "MKT To ";
												}
												else
												{
													$not = "";
												}
												if(($row->dari == "") AND ($row->untuk == ""))
												{
													echo "<li>
															<a href='".base_url()."notif/ptl_detail/$row->kodelog'>
																<div>
																   <strong>$not$row->nama</strong>
																	<span class='pull-right text-muted'>
																		<em>$row->tanggal</em>
																	</span>
																</div>
																<div>$row->aktivitas
																	<br />
																	<span class='label label-primary'>Information</span>
																</div>
															</a>
														</li>
														<li class='divider'></li>";
												}
												if($row->untuk == $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"])
												{
													echo "<li>
															<a href='".base_url()."notif/ptl_edit/$row->id_akun'>
																<div>
																   <strong>$not$row->nama</strong>
																	<span class='pull-right text-muted'>
																		<em>$row->tanggal</em>
																	</span>
																</div>
																<div>$row->aktivitas
																	<br />
																	<span class='label label-danger'>Messages</span>
																</div>
															</a>
														</li>
														<li class='divider'></li>";
												}
												$no++;
											}
										}
										echo "<li>
												<a class='text-center' href='".base_url()."notif'>
													<strong>Read All Messages</strong>
													<i class='icon-angle-right'></i>
												</a>
											</li>";
									}
									else
									{
										echo "<li>
												<a class='text-center' href='".base_url()."notif'>
													<strong>No data available</strong>
													<i class='icon-angle-right'></i>
												</a>
											</li>";
									}
								?>
							</ul>
						</li>
						<!--<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<span class="label label-danger">5</span>   <i class="icon-tasks"></i>&nbsp; <i class="icon-chevron-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-tasks">
								<li>
									<a href="#">
										<div>
											<p>
												<strong> Grade A </strong>
												<span class="pull-right text-muted">40%</span>
											</p>
											<div class="progress progress-striped active">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
													<span class="sr-only">40% Complete (success)</span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#">
										<div>
											<p>
												<strong> Grade B </strong>
												<span class="pull-right text-muted">20%</span>
											</p>
											<div class="progress progress-striped active">
												<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
													<span class="sr-only">20% Complete</span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#">
										<div>
											<p>
												<strong> Grade C </strong>
												<span class="pull-right text-muted">60%</span>
											</p>
											<div class="progress progress-striped active">
												<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
													<span class="sr-only">60% Complete (warning)</span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#">
										<div>
											<p>
												<strong> Grade D </strong>
												<span class="pull-right text-muted">80%</span>
											</p>
											<div class="progress progress-striped active">
												<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
													<span class="sr-only">80% Complete (danger)</span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a class="text-center" href="#">
										<strong>See All Detail Grade</strong>
										<i class="icon-angle-right"></i>
									</a>
								</li>
							</ul>
						</li>-->
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="<?php echo base_url("../crm"); ?>"><i class="icon-user"></i> Go To CRM </a></li>
								<li><a href="<?php echo site_url("login/ptl_profil"); ?>"><i class="icon-user"></i> User Profile </a></li>
								<li><a href="<?php echo site_url("login/ptl_ganti_password"); ?>"><i class="icon-gear"></i> Settings </a></li>
								<li class="divider"></li>
								<li><a href="<?php echo site_url("login/ptl_logout"); ?>"><i class="icon-signout"></i> Logout </a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div id="left" >
				<div class="media user-media well-small">
					<a class="user-link" href="<?php echo site_url("login/ptl_profil"); ?>">
						<?php
							$foto = "ptl_storage/foto_umum/user.jpg";
							$foto_preview = "ptl_storage/foto_umum/user.jpg";
							$Fotoku = @$_COOKIE["foto"];
							if($Fotoku != "")
							{
								$foto = "../hris/system_storage/foto_karyawan/$Fotoku";
								$exist = file_exists_remote(base_url("$foto"));
								if($exist)
								{
									$foto = $foto;
									$source_photo = base_url("$foto");
									$info = pathinfo($source_photo);
									$foto_preview = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
									$exist1 = file_exists_remote(base_url("$foto_preview"));
									if($exist1)
									{
										$foto_preview = $foto_preview;
									}
									else
									{
										$foto_preview = $foto;
									}
								}
								else
								{
									$foto = "ptl_storage/foto_umum/user.jpg";
									$foto_preview = "ptl_storage/foto_umum/user.jpg";
								}
							}
						?>
						<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
							<br/>
							<img class="img-polaroid" src="<?php echo base_url("$foto_preview"); ?>" width="220" height="320" alt=""/>
						</a>
						<!--<img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo base_url(); ?>assets/dashboard/img/user.gif" />-->
					</a>
					<br />
					<div class="media-body">
						<h5 class="media-heading"><?php $word = explode(" ",$_COOKIE["nama"]); echo $word[0]; ?></h5>
						<ul class="list-unstyled user-info">
							<?php
								$ci =&get_instance();
								$ci->load->model('m_akses');
								$id_akun = $_COOKIE["id_akun"];
								$result = $ci->m_akses->PTL_select_marketing($id_akun);
								if($result)
								{
									$status = "Online";
									$button = "success";
								}
								else
								{
									$status = "Offline";
									$button = "danger";
								}
							?>
							<li>
								 <a class="btn btn-<?php echo $button; ?> btn-xs btn-circle" style="width: 10px;height: 12px;"></a> <?php echo $status; ?>
							</li>
						</ul>
					</div>
					<br/>
				</div>
				<?php
					$CI =&get_instance();
					$CI->load->model('m_mix_menu');
					$data1 = $CI->m_mix_menu->PTL_all_prospects();
					$data2 = $CI->m_mix_menu->PTL_all_payments();
					$data3 = $CI->m_mix_menu->PTL_all_test();
					$data4 = $CI->m_mix_menu->PTL_all_completeness();
					$data5 = $CI->m_mix_menu->PTL_all_semester();
					$d1 = count($data2) + count($data3) + count($data4) + count($data5);
					$data6 = $CI->m_mix_menu->PTL_all_interview();
					$data7 = $CI->m_mix_menu->PTL_all_postpone();
					$data8 = $CI->m_mix_menu->PTL_all_reguler();
					$data9 = $CI->m_mix_menu->PTL_all_sc();
					$d2 = count($data8) + count($data9);
				?>
				<ul id="menu" class="collapse">
					<?php $menu = $this->session->userdata('menu');?>
					<li class="panel <?php if($menu == "home"){ echo "active";} ?>">
						<a href="<?php echo site_url("login/ptl_home"); ?>" >
							<i class="icon-table"></i> Dashboard
						</a>
					</li>
					<li class="panel <?php if($menu == "notes"){ echo "active";} ?>">
						<a href="<?php echo site_url("login/ptl_notes"); ?>" >
							<i class="icon-table"></i> Instructions and Notes
						</a>
					</li>
					<!--<li class="panel <?php if($menu == "import"){ echo "active";} ?>">
						<a href="<?php echo site_url("import"); ?>" >
							<i class="icon-table"></i> Import Data
						</a>
					</li>-->
					<li class="panel <?php if($menu == "prospects"){ echo "active";} ?>">
						<a href="<?php echo site_url("prospects"); ?>" >
							<i class="icon-download-alt"></i> Prospects
							&nbsp; <span class="label label-danger"><?php echo count($data1); ?></span>&nbsp;
						</a>
					</li>
					<li class="panel <?php if($menu == "applicant"){ echo "active";} ?>">
						<a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
							<i class="icon-indent-right"> </i> Applicant
							<span class="pull-right">
								<i class="icon-angle-left"></i>
							</span>
							&nbsp; <span class="label label-danger"><?php echo $d1; ?></span>&nbsp;
						</a>
						<ul class="collapse" id="component-nav">
							<li class=""><a href="<?php echo site_url("applicant"); ?>"><i class="icon-angle-right"></i> Form Payment <span class="label label-warning"><?php echo count($data2); ?></span></a></li>
							<li class=""><a href="<?php echo site_url("applicant/ptl_test"); ?>"><i class="icon-angle-right"></i> Test <span class="label label-warning"><?php echo count($data3); ?></span></a></li>
							<li class=""><a href="<?php echo site_url("applicant/ptl_completeness"); ?>"><i class="icon-angle-right"></i> Completeness <span class="label label-warning"><?php echo count($data4); ?></span></a></li>
							<li class=""><a href="<?php echo site_url("applicant/ptl_semester"); ?>"><i class="icon-angle-right"></i> Payment of 1st  Semester <span class="label label-warning"><?php echo count($data5); ?></span></a></li>
						</ul>
					</li>
					<li class="panel <?php if($menu == "interview"){ echo "active";} ?>">
						<a href="<?php echo site_url("interview"); ?>" >
							<i class="icon-edit"></i> Interview
							&nbsp; <span class="label label-info"><?php echo count($data6); ?></span>&nbsp;
						</a>
					</li>
					<li class="panel <?php if($menu == "postpone"){ echo "active";} ?>">
						<a href="<?php echo site_url("postpone"); ?>" >
							<i class="icon-warning-sign"></i> Postpone
							&nbsp; <span class="label label-info"><?php echo count($data7); ?></span>&nbsp;
						</a>
					</li>
					<li class="panel <?php if($menu == "admission"){ echo "active";} ?>">
						<a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#form-nav">
							<i class="icon-pencil"></i> Admission
							<span class="pull-right">
								<i class="icon-angle-left"></i>
							</span>
							  &nbsp; <span class="label label-success"><?php echo $d2; ?></span>&nbsp;
						</a>
						<ul class="collapse" id="form-nav">
							<li class=""><a href="<?php echo site_url("admission"); ?>"><i class="icon-angle-right"></i> Regular <span class="label label-success"><?php echo count($data8); ?></span></a></li>
							<li class=""><a href="<?php echo site_url("admission/ptl_short_course"); ?>"><i class="icon-angle-right"></i> Short Course <span class="label label-success"><?php echo count($data9); ?></span></a></li>
						</ul>
					</li>
					<!--<li class="panel">
						<a href="<?php echo site_url(""); ?>" >
							<i class="icon-folder-open"></i> Orientation
							&nbsp; <span class="label label-info">6</span>&nbsp;
						</a>
					</li>-->
					<li class="panel <?php if($menu == "allocation"){ echo "active";} ?>">
						<a href="<?php echo site_url("allocation"); ?>" >
							<i class="icon-list"></i> Allocation Class
						</a>
					</li>
					<li class="panel <?php if($menu == "activity"){ echo "active";} ?>">
						<a href="<?php echo site_url("activity"); ?>" >
							<i class="icon-camera-retro"></i> Event Activity
						</a>
					</li>
					<li class="panel <?php if($menu == "schedule"){ echo "active";} ?>">
						<a href="<?php echo site_url("today"); ?>" >
							<i class="icon-list"></i> Schedule Today
						</a>
					</li>
					<!--<li class="panel">
						<a href="<?php echo site_url(""); ?>" >
							<i class="icon-list-alt"></i> Reports
						</a>
					</li>
					<li class="panel">
						<a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav">
							<i class="icon-bar-chart"></i> Charts
							<span class="pull-right">
								<i class="icon-angle-left"></i>
							</span>
							&nbsp; <span class="label label-default">4</span>&nbsp;
						</a>
						<ul class="collapse" id="chart-nav">
							<li><a href="<?php echo site_url("charts"); ?>"><i class="icon-angle-right"></i> Prospects Charts </a></li>
							<li><a href="<?php echo site_url("charts/ptl_applicant"); ?>"><i class="icon-angle-right"></i> Applicant Charts</a></li>
							<li><a href="<?php echo site_url("charts/ptl_admission"); ?>"><i class="icon-angle-right"></i> Admission Charts </a></li>
							<li><a href="<?php echo site_url("charts/ptl_interview"); ?>"><i class="icon-angle-right"></i> Interview Charts </a></li>
						</ul>
					</li>-->
					<li class="panel <?php if($menu == "additional"){ echo "active";} ?>">
						<a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#blank-nav">
							<i class="icon-columns"></i> Additional Data
							<span class="pull-right">
								<i class="icon-angle-left"></i>
							</span>
							&nbsp; <span class="label label-default">9</span>&nbsp;
						</a>
						<ul class="collapse" id="blank-nav">
							<li><a href="<?php echo site_url("school"); ?>"><i class="icon-angle-right"></i> School Data </a></li>
							<li><a href="<?php echo site_url("university"); ?>"><i class="icon-angle-right"></i> University Data </a></li>
							<li><a href="<?php echo site_url("province"); ?>"><i class="icon-angle-right"></i> Province </a></li>
							<li><a href="<?php echo site_url("city"); ?>"><i class="icon-angle-right"></i> City </a></li>
							<li><a href="<?php echo site_url("information"); ?>"><i class="icon-angle-right"></i> Information Source </a></li>
							<li><a href="<?php echo site_url("entry"); ?>"><i class="icon-angle-right"></i> Entry Status </a></li>
							<li><a href="<?php echo site_url("grade"); ?>"><i class="icon-angle-right"></i> Admission Grade </a></li>
							<li><a href="<?php echo site_url("education"); ?>"><i class="icon-angle-right"></i> Education </a></li>
							<li><a href="<?php echo site_url("religion"); ?>"><i class="icon-angle-right"></i> Religion </a></li>
						</ul>
					</li>
					<li class="panel <?php if($menu == "setup"){ echo "active";} ?>">
						<a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav">
							<i class="icon-wrench"></i> Set Up Admission
							<span class="pull-right">
								<i class="icon-angle-left"></i>
							</span>
							&nbsp; <span class="label label-default">5</span>&nbsp;
						</a>
						<ul class="collapse" id="pagesr-nav">
							<li><a href="<?php echo site_url("pmb"); ?>"><i class="icon-angle-right"></i> Admission Period </a></li>
							<li><a href="<?php echo site_url("pmbform"); ?>"><i class="icon-angle-right"></i> Admission Form </a></li>
							<li><a href="<?php echo site_url("pmbreq"); ?>"><i class="icon-angle-right"></i> Form Requirement </a></li>
							<li><a href="<?php echo site_url("inter"); ?>"><i class="icon-angle-right"></i> Standard Interview </a></li>
							<li><a href="<?php echo site_url("info"); ?>"><i class="icon-angle-right"></i> Information for Applicant </a></li>
							<li><a href="<?php echo site_url("team"); ?>"><i class="icon-angle-right"></i> Marketing Team </a></li>
						</ul>
					</li>
				</ul>
			</div>