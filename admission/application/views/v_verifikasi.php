<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>Esmod Jakarta - Admission</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/login.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/magic/magic.css" />
	</head>
	<body>
		<div class="container">
			<div class="text-center">
				<img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" id="logoimg" alt=" Logo" />
			</div>
			<div class="tab-content">
				<div id="login" class="tab-pane active">
					<form action="<?php echo site_url("login/ptl_verification_sms_check"); ?>" class="form-signin" method="POST" >
						<p class="text-muted text-center btn-block btn btn-primary btn-rect">
							Validate your OTP code
						</p>
						<input type="text" name="kode_sms" class="form-control" placeholder="ESMOD OTP" autofocus required>
						<input type="hidden" name="kode_verifikasi" value="<?php echo $kode_verifikasi; ?>">
						<br/>
						<button class="btn text-muted text-center btn-success" type="submit">Confirm</button>
					</form>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-2.0.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/login.js"></script>
	</body>
</html>