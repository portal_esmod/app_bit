			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Regular</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Regular
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of students regular.
									</div>
									<div class="table-responsive">
										<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_periode"); ?>" method="POST">
															<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- PERIOD --</option>
																<?php
																	if($periode)
																	{
																		$cekperiode = $this->session->userdata('adm_filter_periode');
																		foreach($periode as $per)
																		{
																			echo "<option value='".$per->PMBPeriodID."_REG'";
																			if($cekperiode == $per->PMBPeriodID)
																			{
																				echo "selected";
																			}
																			echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_marketing"); ?>" method="POST">
															<select name="cekmarketing" title="Filter by Marketing Staff" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- MARKETING --</option>
																<?php
																	if($marketing)
																	{
																		$cekmarketing = $this->session->userdata('adm_filter_marketing');
																		echo "<option value='OLREG_REG'";
																		if($cekmarketing == "OLREG")
																		{
																			echo "selected";
																		}
																		echo ">OLREG - ONLINE REGISTRATION</option>";
																		foreach($marketing as $mar)
																		{
																			$word = explode(" ",$mar->nama);
																			echo "<option value='".$word[0]."_REG'";
																			if($cekmarketing == $word[0])
																			{
																				echo "selected";
																			}
																			echo ">".$word[0]." - $mar->nama</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_type"); ?>" method="POST">
															<select name="cektype" title="Filter by Type" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- TYPE --</option>
																<?php
																	$cektype = $this->session->userdata('adm_filter_type');
																	echo "<option value='HOT_REG'"; if($cektype == "HOT") { echo "selected"; } echo ">HOT - Potensial</option>";
																	echo "<option value='USUAL_REG'"; if($cektype == "USUAL") { echo "selected"; } echo ">USUAL - Biasa</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th></th>
												</tr>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_status"); ?>" method="POST">
															<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- STATUS --</option>
																<?php
																	$cekstatus = $this->session->userdata('adm_filter_status');
																	echo "<option value='Y_REG'"; if($cekstatus == "Y") { echo "selected"; } echo ">STEP DOWN</option>";
																	echo "<option value='N_REG'"; if($cekstatus == "N") { echo "selected"; } echo ">NOT STEP DOWN</option>";
																	echo "<option value='P_REG'"; if($cekstatus == "P") { echo "selected"; } echo ">POSTPONE</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_program"); ?>" method="POST">
															<select name="cekpogram" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- PROGRAM --</option>
																<?php
																	$cekpogram = $this->session->userdata('adm_filter_program');
																	echo "<option value='INT_REG'"; if($cekpogram == "INT") { echo "selected"; } echo ">INT - INTENSIVE</option>";
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='".$d1->ProdiID."_REG'";
																			if($cekpogram == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																	echo "<option value='REG_REG'"; if($cekpogram == "REG") { echo "selected"; } echo ">REG - REGULAR</option>";
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='".$d3->ProdiID."_REG'";
																			if($cekpogram == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																	echo "<option value='SC_REG'"; if($cekpogram == "SC") { echo "selected"; } echo ">SC - SHORT COURSE</option>";
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='".$sc->KursusSingkatID."_REG'";
																			if($cekpogram == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("admission/ptl_filter_adm_tahun"); ?>" method="POST">
															<select name="cektahun" title="Filter by Year" class="form-control round-form" onchange="this.form.submit()">
																<option value='_REG'>-- YEAR --</option>
																<?php
																	if($tahun)
																	{
																		$cektahun = $this->session->userdata('adm_filter_tahun');
																		foreach($tahun as $thn)
																		{
																			echo "<option value='".$thn->tahun."_REG'";
																			if($cektahun == $thn->tahun)
																			{
																				echo "selected";
																			}
																			echo ">$thn->tahun</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center"><a href="<?php echo site_url("admission/ptl_pdf_public/REG"); ?>" title="Print This Result" class="btn btn-primary">Print This Result</a></p></th>
												</tr>
											</thead>
										</table>
									</div>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">NO</p></th>
													<th><p valign="middle" align="center">APL ID<hr></p><p valign="middle" align="center">PRESENTER ID</p></th>
													<th><p valign="middle" align="center">STAGE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">AGE</p></th>
													<th><p valign="middle" align="center">HANDPHONE</p></th>
													<th><p valign="middle" align="center">PROGRAM</p><hr><p valign="middle" align="center">PRODI</p></th>
													<th><p valign="middle" align="center">TERM</p></th>
													<th><p valign="middle" align="center">PAYMENT</p></th>
													<th><p valign="middle" align="center">SCORE</p><hr><p valign="middle" align="center">GRADE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															$ProdiID = $row->ProdiID;
															$result2 = $this->m_prodi->PTL_select($ProdiID);
															$nama = "";
															if($result2)
															{
																$nama = $result2['Nama'];
															}
															if($row->NA =="Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr class='success'>";
															}
															echo "<td>";
															?>
																	<a href="<?php echo base_url()."prospects/ptl_delete/$row->AplikanID"; ?>" title="Delete this Prospect" onclick="return confirm('Are you sure to remove the admission with CODE <?php echo $row->PMBID.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line"><?php echo $no; ?></a>
															<?php
															echo "<td><p align='center'>
																		<b><a href='".base_url()."prospects/ptl_edit/$row->AplikanID' title='Edit this Prospect' class='btn btn-primary btn-sm btn-round btn-line'>$row->PMBID</a></b>
																	</p>
																	<hr>
																	<p align='center'>
																		$row->PresenterID
																	</p>
																</td>
																<td>$row->StatusAplikanID</td>
																<td>$row->Nama</td>
																<td><p valign='middle' align='center'>$row->umur</p></td>
																<td><p valign='middle' align='center'>$row->Handphone</p></td>
																<td><p valign='middle' align='center'>$row->ProgramID, $row->ProdiID</p><hr><p valign='middle' align='center'>$nama</p></td>
																<td><p valign='middle' align='center'>$row->SyaratLengkap</p></td>
																<td><p valign='middle' align='center'>$row->diterima</p></td>
																<td><p valign='middle' align='center'>$row->NilaiUjian</p><hr><p valign='middle' align='center'>$row->GradeNilai</p></td>
															</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>