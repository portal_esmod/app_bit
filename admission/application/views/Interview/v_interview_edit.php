			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Interview Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("interview"); ?>">Interview</a>
									>
									<a href="<?php echo site_url("interview/ptl_edit/$AplikanID"); ?>">Interview Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('interview/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Applicant ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="AplikanID" value="<?php echo $AplikanID; ?>" placeholder="" class="form-control" />
													<input type="hidden" name="Email" value="<?php echo $Email; ?>" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Question</label>
												<div class="col-lg-8">
													<?php
														if($wawancara)
														{
															foreach($wawancara as $row)
															{
																echo $row->pertanyaan." (".$row->standard.")<br/>";
															}
														}
													?>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Note Interview</label>
												<div class="col-lg-8">
													<textarea name="nilai_interview" col="3" class="form-control"><?php echo $nilai_interview; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Pass?</label>
												<div class="col-lg-8">
													<input type="checkbox" name="hasil_interview" value="Y" <?php if($hasil_interview == "Y"){echo "checked";} ?>> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/umur/jquery-ui.css"/>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-1.9.1.js"></script>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-ui.js"></script>
			
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
			<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
			<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
			<?php $url = base_url()."assets/auto/loading.gif"; ?>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
			<style>
				/* Autocomplete
				----------------------------------*/
				.ui-autocomplete { position: absolute; cursor: default; }
				.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

				/* workarounds */
				* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

				/* Menu
				----------------------------------*/
				.ui-menu {
					list-style:none;
					padding: 2px;
					margin: 0;
					display:block;
				}
				.ui-menu .ui-menu {
					margin-top: -3px;
				}
				.ui-menu .ui-menu-item {
					margin:0;
					padding: 0;
					zoom: 1;
					float: left;
					clear: left;
					width: 100%;
					font-size:80%;
				}
				.ui-menu .ui-menu-item a {
					text-decoration:none;
					display:block;
					padding:.2em .4em;
					line-height:1.5;
					zoom:1;
				}
				.ui-menu .ui-menu-item a.ui-state-hover,
				.ui-menu .ui-menu-item a.ui-state-active {
					font-weight: normal;
					margin: -1px;
				}
			</style>
			<script type="text/javascript">
				$(this).ready( function()
				{
					$("#KotaID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_kota",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#PropinsiID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_propinsi",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#KotaID2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_kota",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#PropinsiID2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_propinsi",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#Agama").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#Agama2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#Agama3").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#SekolahID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_sekolah",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
				$(this).ready( function()
				{
					$("#PerguruanTinggiID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_kampus",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
			</script>