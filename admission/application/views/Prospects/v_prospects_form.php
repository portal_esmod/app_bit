			<script language="javascript">
				function pilihanmu()
				{
					var val = 0;
					for( i = 0; i < document.contoh.WargaNegara.length; i++ ){
						if( document.contoh.WargaNegara[i].checked == true ){
							val = document.contoh.WargaNegara[i].value;
							if(val=='WNA')
							{
								document.contoh.Kebangsaan.readOnly = false;
								document.contoh.Kebangsaan.value = '';
							}
							else
							{
								document.contoh.Kebangsaan.readOnly = true;
								document.contoh.Kebangsaan.value = 'INDONESIA';
							}
						}
					}
				}
				
				function idem_ayah()
				{
					var val = 0;
					if( document.contoh.IdemAyah.checked == true )
					{
						document.contoh.AlamatOrtu.readOnly = true;
						document.contoh.KotaOrtu.readOnly = true;
						document.contoh.RTOrtu.readOnly = true;
						document.contoh.RWOrtu.readOnly = true;
						document.contoh.KodePosOrtu.readOnly = true;
						document.contoh.PropinsiOrtu.readOnly = true;
						document.contoh.NegaraOrtu.readOnly = true;
						document.contoh.AlamatOrtu.value = document.contoh.Alamat.value;
						document.contoh.KotaOrtu.value = document.contoh.Kota.value;
						document.contoh.RTOrtu.value = document.contoh.RT.value;
						document.contoh.RWOrtu.value = document.contoh.RW.value;
						document.contoh.KodePosOrtu.value = document.contoh.KodePos.value;
						document.contoh.PropinsiOrtu.value = document.contoh.Propinsi.value;
						document.contoh.NegaraOrtu.value = document.contoh.Negara.value;
					}
					else
					{
						document.contoh.AlamatOrtu.readOnly = false;
						document.contoh.KotaOrtu.readOnly = false;
						document.contoh.RTOrtu.readOnly = false;
						document.contoh.RWOrtu.readOnly = false;
						document.contoh.KodePosOrtu.readOnly = false;
						document.contoh.PropinsiOrtu.readOnly = false;
						document.contoh.NegaraOrtu.readOnly = false;
						document.contoh.AlamatOrtu.value = '';
						document.contoh.KotaOrtu.value = '';
						document.contoh.RTOrtu.value = '';
						document.contoh.RWOrtu.value = '';
						document.contoh.KodePosOrtu.value = '';
						document.contoh.PropinsiOrtu.value = '';
						document.contoh.NegaraOrtu.value = '';
					}
				}
			</script>
			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Prospects Form</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("prospects"); ?>">Prospects</a>
									>
									<a href="<?php echo site_url("prospects/ptl_form"); ?>">Prospects Form</a>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php echo form_open_multipart('prospects/ptl_insert',array('name' => 'contoh','class' => 'form-horizontal')); ?>
											<table class="table table-striped table-hover">
												<tr>
													<td width="15%">
														<label for="text1" class="control-label col-lg-12">Date Coming</label>
													</td>
													<td width="15%">
														<div class="col-lg-12">
															<?php
																$h = "-7";
																$hm = $h * 60; 
																$ms = $hm * 60;
																$tanggal = gmdate("Y-m-d H:i:s", time()-($ms));
																$date = gmdate("d M Y", time()-($ms));
															?>
															<input type="hidden" name="TanggalBuat" value="<?php echo $tanggal; ?>" class="form-control round-form">
															<input type="hidden" name="Copy" value="NO" class="form-control round-form">
															<input type="hidden" name="CopyAplikan" value="" class="form-control round-form">
															<input type="text" value="<?php echo $date; ?>" class="form-control round-form" readonly>
														</div>
													</td>
													<td width="15%">
														<label for="text1" class="control-label col-lg-12">ID Lead</label>
													</td>
													<td width="15%">
														<div class="col-lg-12">
															<input type="text" name="id_lead" value="<?php echo $this->session->userdata('pros_id_lead'); ?>" class="form-control round-form" <?php echo $cekreadonly; ?>>
														</div>
													</td>
													<td width="20%">
														<label for="text1" class="control-label col-lg-12">Reference Code</label>
													</td>
													<td width="20%">
														<div class="col-lg-12">
															<input type="text" name="kode_lead" value="<?php echo $this->session->userdata('pros_kode_lead'); ?>" class="form-control round-form" <?php echo $cekreadonly; ?>>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Marketing</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PresenterID" class="form-control round-form">
																<?php
																	if($marketing)
																	{
																		echo "<option value=''>ONLINE REGISTRATION</option>";
																		foreach($marketing as $row)
																		{
																			echo "<option value='$row->id_akun'";
																			if($this->session->userdata('pros_PresenterID') == $row->id_akun)
																			{
																				echo "selected";
																			}
																			echo ">$row->id_akun - $row->nama</option>";
																		}
																	}
																?>
															</select>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Status</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="StatusProspekID" class="form-control round-form" required>
																<option value="" <?php if($this->session->userdata('pros_StatusProspekID') == ""){ echo "selected"; } ?>>-- CHOOSE --</option>
																<option value="HOT" <?php if($this->session->userdata('pros_StatusProspekID') == "HOT"){ echo "selected"; } ?>>HOT - Potensial</option>
																<option value="USUAL" <?php if($this->session->userdata('pros_StatusProspekID') == "USUAL"){ echo "selected"; } ?>>USUAL - Biasa</option>
															</select>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">PMB Period</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PMBPeriodID" class="form-control round-form" required>
																<option value="">NONE</option>
																<?php
																	if($pmbperiod)
																	{
																		foreach($pmbperiod as $rowpmb)
																		{
																			echo "<option value='$rowpmb->PMBPeriodID'";
																			if($this->session->userdata('pros_PMBPeriodID') == $rowpmb->PMBPeriodID)
																			{
																				echo "selected";
																			}
																			echo ">$rowpmb->PMBPeriodID - $rowpmb->Nama</option>";
																		}
																	}
																?>
															</select>
														</div>
													</td>
												</tr>
												<tr>
													<td rowspan="4">
														<label for="text1" class="control-label col-lg-12">Photo</label>
													</td>
													<td rowspan="4">
														<div class="col-lg-12">
															<div class="fileupload fileupload-new" data-provides="fileupload">
																<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
																	<img src="<?php echo base_url(); ?>assets/dashboard/img/demoUpload.jpg" alt="" />
																</div>
																<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
																<div>
																	<span class="btn btn-file btn-primary">
																		<span class="fileupload-new">Select image</span>
																		<span class="fileupload-exists">Change</span>
																		<input type="file" name="gambar" accept="image/*" class="btn btn-warning">
																	</span>
																	<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
																</div>
															</div>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Identity ID</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="nomor_identitas" value="<?php echo $this->session->userdata('pros_nomor_identitas'); ?>" placeholder="ex: KTP/KITAS/PASSPORT" class="form-control" required/>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Name</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="Nama" value="<?php echo $this->session->userdata('pros_Nama'); ?>" placeholder="Your complete name" class="form-control" required/>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Gender</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="radio" name="Kelamin" value="P" <?php if($this->session->userdata('pros_Kelamin') == "P"){ echo "checked"; } ?>> MALE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" name="Kelamin" value="W" <?php if($this->session->userdata('pros_Kelamin') == "W"){ echo "checked"; } ?>> FEMALE
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Type of Blood</label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="GolonganDarah" value="<?php echo $this->session->userdata('pros_GolonganDarah'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Status</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="radio" name="WargaNegara" value="WNI" onClick="pilihanmu()" <?php if($this->session->userdata('pros_WargaNegara') == "WNI"){ echo "checked"; } ?>> WNI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" name="WargaNegara" value="WNA" onClick="pilihanmu()" <?php if($this->session->userdata('pros_WargaNegara') == "WNA"){ echo "checked"; } ?>> WNA
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Country</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="Kebangsaan" value="<?php echo $this->session->userdata('pros_Kebangsaan'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Place of Birth</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TempatLahir" value="<?php echo $this->session->userdata('pros_TempatLahir'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Date of Birth</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TanggalLahir" value="<?php echo $this->session->userdata('pros_TanggalLahir'); ?>" id="datepicker" placeholder="YYYY-MM-DD" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Age</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="umur" value="<?php echo $this->session->userdata('pros_umur'); ?>" placeholder="Automatic" id="umur" class="form-control" readonly>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Religion</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="Agama" value="<?php echo $this->session->userdata('pros_Agama'); ?>" placeholder="If not in the list, you can still write a religion other than in list" title="If not in the list, you can still write a religion other than in list" id="Agama" class="agama1 form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Height</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TinggiBadan" value="<?php echo $this->session->userdata('pros_TinggiBadan'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Weight</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="BeratBadan" value="<?php echo $this->session->userdata('pros_BeratBadan'); ?>" class="form-control">
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Address</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<textarea name="Alamat" col="3" class="form-control" required><?php echo $this->session->userdata('pros_Alamat'); ?></textarea>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">City</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="Kota" value="<?php echo $this->session->userdata('pros_Kota'); ?>" id="KotaID" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">RT</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="RT" value="<?php echo $this->session->userdata('pros_RT'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">RW</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="RW" value="<?php echo $this->session->userdata('pros_RW'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Postal Code</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="KodePos" value="<?php echo $this->session->userdata('pros_KodePos'); ?>" class="form-control">
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Province</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="Propinsi" value="<?php echo $this->session->userdata('pros_Propinsi'); ?>" id="PropinsiID" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Country of Address</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="Negara" value="<?php echo $this->session->userdata('pros_Negara'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Phone</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="Telepon" value="<?php echo $this->session->userdata('pros_Telepon'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Mobile Phone</font></label>
													</td>
													<td colspan="2">
														<div class="col-lg-12">
															<input type="text" name="Handphone" value="<?php echo $this->session->userdata('pros_Handphone'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Email</font></label>
													</td>
													<td colspan="4">
														<div class="col-lg-12">
															<input type="email" name="Email" value="<?php echo $this->session->userdata('pros_Email'); ?>" placeholder="The system will send any information to prospect. So, make sure the email is valid" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</table>
											<legend>Last Education</legend>
											<table class="table table-striped table-hover">
												<tr>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Last School</font></label>
													</td>
													<td width="23%">
														<div class="col-lg-12">
															<select name="PendidikanTerakhir" placeholder="Ex: UNIVERSITY/SMA/SMP/SD" class="form-control" required>
																<option value="">-- CHOOSE --</option>
																<option value="UNIVERSITY" <?php if($this->session->userdata('pros_PendidikanTerakhir') == "UNIVERSITY"){ echo "selected"; } ?>>UNIVERSITY</option>
																<option value="SMA" <?php if($this->session->userdata('pros_PendidikanTerakhir') == "SMA"){ echo "selected"; } ?>>SMA</option>
																<option value="SMP" <?php if($this->session->userdata('pros_PendidikanTerakhir') == "SMP"){ echo "selected"; } ?>>SMP</option>
																<option value="SD" <?php if($this->session->userdata('pros_PendidikanTerakhir') == "SD"){ echo "selected"; } ?>>SD</option>
															</select>
														</div>
													</td>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Name of Last School</font></label>
													</td>
													<td width="31%">
														<div class="col-lg-12">
															<input type="text" name="AsalSekolah" value="<?php echo $this->session->userdata('pros_AsalSekolah'); ?>" placeholder="If not in the list, you can still write a school other than in list" id="SekolahID" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">City of Last School</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="KotaAsalSekolah" value="<?php echo $this->session->userdata('pros_KotaAsalSekolah'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Specialize</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="JurusanSekolah" value="<?php echo $this->session->userdata('pros_JurusanSekolah'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Graduation Year</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TahunLulus" value="<?php echo $this->session->userdata('pros_TahunLulus'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Final Exam Scores</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="NilaiSekolah" value="<?php echo $this->session->userdata('pros_NilaiSekolah'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Already Working?</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="checkbox" name="SudahBekerja" value="Y" <?php if($this->session->userdata('pros_SudahBekerja') == "Y"){ echo "checked"; } ?>> Yes
														</div>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</table>
											<legend>Parents Data</legend>
											<hr>
											<h4>FATHER</h4>
											<hr>
											<table class="table table-striped table-hover">
												<tr>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Name</font></label>
													</td>
													<td width="23%">
														<div class="col-lg-12">
															<input type="text" name="NamaAyah" value="<?php echo $this->session->userdata('pros_NamaAyah'); ?>" placeholder="Your complete father's name" class="form-control" required>
														</div>
													</td>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Religion</font></label>
													</td>
													<td width="31%">
														<div class="col-lg-12">
															<input type="text" name="AgamaAyah" value="<?php echo $this->session->userdata('pros_AgamaAyah'); ?>" placeholder="If not in the list, you can still write a religion other than in list" id="Agama2" class="agama2 form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Last Education</label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PendidikanAyah" class="form-control">
																<?php
																	if($ortu)
																	{
																		echo "<option value=''>-- CHOOSE --</option>";
																		foreach($ortu as $rowortu)
																		{
																			echo "<option value='".strtoupper($rowortu->Nama)."'";
																			if($this->session->userdata('pros_PendidikanAyah') == strtoupper($rowortu->Nama))
																			{
																				echo "selected";
																			}
																			echo ">".strtoupper($rowortu->Nama)."</option>";
																		}
																	}
																?>
															</select>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Job</label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PekerjaanAyah" class="form-control">
																<option value="" >-- CHOOSE --</option>
																<option value="KAR" <?php if($this->session->userdata('pros_PekerjaanAyah') == "KAR"){ echo "selected"; } ?>>Karyawan Swasta</option>
																<option value="PNS" <?php if($this->session->userdata('pros_PekerjaanAyah') == "PNS"){ echo "selected"; } ?>>Pegawai Negeri Sipil</option>
																<option value="PRO" <?php if($this->session->userdata('pros_PekerjaanAyah') == "PRO"){ echo "selected"; } ?>>Profesional</option>
															</select>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Address</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="checkbox" name="IdemAyah" onClick="idem_ayah()"> IDEM
															<textarea name="AlamatOrtu" col="3" class="form-control" required><?php echo $this->session->userdata('pros_AlamatOrtu'); ?></textarea>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">City</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="KotaOrtu" value="<?php echo $this->session->userdata('pros_KotaOrtu'); ?>" id="KotaID2" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">RT</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="RTOrtu" value="<?php echo $this->session->userdata('pros_RTOrtu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">RW</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="RWOrtu" value="<?php echo $this->session->userdata('pros_RWOrtu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Postal Code</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="KodePosOrtu" value="<?php echo $this->session->userdata('pros_KodePosOrtu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Province</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="PropinsiOrtu" value="<?php echo $this->session->userdata('pros_PropinsiOrtu'); ?>" id="PropinsiID2" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Country of Address</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="NegaraOrtu" value="<?php echo $this->session->userdata('pros_NegaraOrtu'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Phone</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TeleponOrtu" value="<?php echo $this->session->userdata('pros_TeleponOrtu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Mobile Phone</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="HandphoneOrtu" value="<?php echo $this->session->userdata('pros_HandphoneOrtu'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Email</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="email" name="EmailOrtu" value="<?php echo $this->session->userdata('pros_EmailOrtu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</table>
											<h4>MOTHER</h4>
											<hr>
											<table class="table table-striped table-hover">
												<tr>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Name</font></label>
													</td>
													<td width="23%">
														<div class="col-lg-12">
															<input type="text" name="NamaIbu" value="<?php echo $this->session->userdata('pros_NamaIbu'); ?>" placeholder="Your complete mother's name" class="form-control" required>
														</div>
													</td>
													<td width="14%">
														<label for="text1" class="control-label col-lg-12"><font color="red">Religion</font></label>
													</td>
													<td width="31%">
														<div class="col-lg-12">
															<input type="text" name="AgamaIbu" value="<?php echo $this->session->userdata('pros_AgamaIbu'); ?>" placeholder="If not in the list, you can still write a religion other than in list" id="Agama3" class="agama3 form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Last Education</label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PendidikanIbu" class="form-control">
																<?php
																	if($ortu)
																	{
																		echo "<option value=''>-- CHOOSE --</option>";
																		foreach($ortu as $rowortu)
																		{
																			echo "<option value='".strtoupper($rowortu->Nama)."'";
																			if($this->session->userdata('pros_PendidikanIbu') == strtoupper($rowortu->Nama))
																			{
																				echo "selected";
																			}
																			echo ">".strtoupper($rowortu->Nama)."</option>";
																		}
																	}
																?>
															</select>
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12">Job</label>
													</td>
													<td>
														<div class="col-lg-12">
															<select name="PekerjaanIbu" class="form-control">
																<option value="" >-- CHOOSE --</option>
																<option value="KAR" <?php if($this->session->userdata('pros_PekerjaanIbu') == "KAR"){ echo "selected"; } ?>>Karyawan Swasta</option>
																<option value="PNS" <?php if($this->session->userdata('pros_PekerjaanIbu') == "PNS"){ echo "selected"; } ?>>Pegawai Negeri Sipil</option>
																<option value="PRO" <?php if($this->session->userdata('pros_PekerjaanIbu') == "PRO"){ echo "selected"; } ?>>Profesional</option>
															</select>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Phone</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="TeleponIbu" value="<?php echo $this->session->userdata('pros_TeleponIbu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														<label for="text1" class="control-label col-lg-12"><font color="red">Mobile Phone</font></label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="text" name="HandphoneIbu" value="<?php echo $this->session->userdata('pros_HandphoneIbu'); ?>" class="form-control" required>
														</div>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<label for="text1" class="control-label col-lg-12">Email</label>
													</td>
													<td>
														<div class="col-lg-12">
															<input type="email" name="EmailIbu" value="<?php echo $this->session->userdata('pros_EmailIbu'); ?>" class="form-control">
														</div>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</table>
											<legend>Elective Courses</legend>
											<hr>
											<!--<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Three Years Program (D3)</label>
												<div class="col-lg-8">
													<?php
														// if($d3)
														// {
															// echo "<input type='radio' name='Pilihan1' value=''> None<br>";
															// foreach($d3 as $rowd3)
															// {
																// echo "<input type='radio' name='Pilihan1' value='$rowd3->ProdiID'> $rowd3->Nama<br>";
															// }
														// }
													?>
												</div>
											</div>-->
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Short Course</label>
												<div class="col-lg-8">
													<select name="Pilihan3" class="form-control">
														<option value="">-- NONE --</option>
														<?php
															if($kursussingkat)
															{
																foreach($kursussingkat as $rowks)
																{
																	echo "<option value='$rowks->KursusSingkatID'>".strtoupper($rowks->Nama)."</option>";
																}
															}
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">One Year Program (D1)</label>
												<div class="col-lg-8">
													<select name="Pilihan2" class="form-control">
														<option value="">-- NONE --</option>
														<?php
															if($d1)
															{
																foreach($d1 as $rowd1)
																{
																	echo "<option value='$rowd1->ProdiID'>".strtoupper($rowd1->Nama)."</option>";
																}
															}
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Three Years Program (D3)</label>
												<div class="col-lg-8">
													<select name="Pilihan1" class="form-control">
														<option value="">-- NONE --</option>
														<?php
															if($d3)
															{
																foreach($d3 as $rowd3)
																{
																	echo "<option value='$rowd3->ProdiID'>".strtoupper($rowd3->Nama)."</option>";
																}
															}
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Additional Program</label>
												<div class="col-lg-8">
													<select name="Pilihan4" class="form-control">
														<option value="">-- NONE --</option>
														<?php
															echo "
																<option value='SPCC'>Specialization - Couture</option>
																<option value='SPCK'>Specialization - Kidswear</option>
																<option value='SPCL'>Specialization - Lingerie</option>
																<option value='SPCM'>Specialization - Menswear</option>
																<option value='SPCW'>Specialization - Womenswear</option>
																<option value='MD2'>Master Program</option>
																";
														?>
													</select>
												</div>
											</div>
											<!--<div class="form-group">
												<label for="text1" class="control-label col-lg-4">One Year Program (D1)</label>
												<div class="col-lg-8">
													<?php
														// if($d1)
														// {
															// echo "<input type='radio' name='Pilihan2' value=''> None<br>";
															// foreach($d1 as $rowd1)
															// {
																// echo "<input type='radio' name='Pilihan2' value='$rowd1->ProdiID'> $rowd1->Nama<br>";
															// }
														// }
													?>
												</div>
											</div>-->
											<!--<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Short Course</label>
												<div class="col-lg-8">
													<?php
														// if($kursussingkat)
														// {
															// echo "<input type='radio' name='Pilihan3' value=''> None<br>";
															// foreach($kursussingkat as $rowks)
															// {
																// echo "<input type='radio' name='Pilihan3' value='$rowks->KursusSingkatID'> $rowks->Nama<br>";
															// }
														// }
													?>
												</div>
											</div>-->
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">Register To</font></label>
												<div class="col-lg-8">
													<select name="TahunKe" class="form-control" required>
														<option value="">-- CHOOSE --</option>
														<option value="0">Short Course Program</option>
														<option value="1">1st Year - 1st Semester</option>
														<option value="2">2nd Year - 3rd Semester</option>
														<option value="3">3rd Year - 5th Semester</option>
														<option value="5">Specialization Program</option>
														<option value="6">Master Program</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">On the initiative of whom Mr/s come to ESMOD Jakarta?</label>
												<div class="col-lg-8">
													<textarea name="Catatan" col="3" class="form-control"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Resources</label>
												<div class="col-lg-8">
													<?php
														if($sumberinfo)
														{
															$no = 0;
															foreach($sumberinfo as $rowsi)
															{
																$no++;
																echo "<input type='checkbox' name='SumberInformasi$no' value='$rowsi->InfoID'> $rowsi->Nama<br>";
															}
														}
													?>
													<input type="hidden" name="no" value="<?php echo $no; ?>" class="form-control round-form">
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">1. Already Register?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="NA" value="Y" <?php if($this->session->userdata('pros_NA') == "Y"){ echo "checked"; } ?>> No
													<input type="radio" name="NA" value="N" <?php if($this->session->userdata('pros_NA') == "N"){ echo "checked"; } ?>> Yes
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">2. Already Payment Form?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="SudahBayar" value="N" <?php if($this->session->userdata('pros_SudahBayar') == "N"){ echo "checked"; } ?>> No
													<input type="radio" name="SudahBayar" value="Y" <?php if($this->session->userdata('pros_SudahBayar') == "Y"){ echo "checked"; } ?>> Yes
													<br/>
													<br/>
													<input type="file" name="gambar1">
													<br/>
													<br/>
													<input type="number" name="Harga" placeholder="Cost of Form" class="form-control">
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">3. Already Test?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="LulusUjian" value="N" <?php if($this->session->userdata('pros_LulusUjian') == "N"){ echo "checked"; } ?>> No
													<input type="radio" name="LulusUjian" value="Y" <?php if($this->session->userdata('pros_LulusUjian') == "Y"){ echo "checked"; } ?>> Yes
													<br/>
													<br/>
													<input type="file" name="gambar2">
													<br/>
													<br/>
													<input type="hidden" name="NilaiUjian" placeholder="Exam Score" class="form-control">
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">4. Already Completeness File?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="SyaratLengkap" value="N" <?php if($this->session->userdata('pros_SyaratLengkap') == "N"){ echo "checked"; } ?>> No
													<input type="radio" name="SyaratLengkap" value="Y" <?php if($this->session->userdata('pros_SyaratLengkap') == "Y"){ echo "checked"; } ?>> Yes
													<br/>
													<br/>
													<input type="file" name="gambar3">
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">5. Already Payment 1st Semester?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="diterima" value="N" <?php if($this->session->userdata('pros_diterima') == "N"){ echo "checked"; } ?>> No
													<input type="radio" name="diterima" value="Y" <?php if($this->session->userdata('pros_diterima') == "Y"){ echo "checked"; } ?>> Yes
													<br/>
													<br/>
													<input type="file" name="gambar4">
													<br/>
													<br/>
													<input type="hidden" name="total_biaya" placeholder="Total Cost" >
													<input type="number" name="total_bayar" value="<?php echo $this->session->userdata('pros_total_bayar'); ?>" placeholder="Total Payment" class="form-control">
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">Note</font></label>
												<div class="col-lg-8">
													<textarea name="CatatanPresenter" col="3" class="form-control" placeholder="These notes will be appear at the finance department"><?php echo $this->session->userdata('pros_CatatanPresenter'); ?></textarea>
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"><font color="red">Postpone?</font></label>
												<div class="col-lg-8">
													<input type="radio" name="postpone" value="N" <?php if($this->session->userdata('pros_postpone') == "N"){ echo "checked"; } ?>> No
													<input type="radio" name="postpone" value="Y" <?php if($this->session->userdata('pros_postpone') == "Y"){ echo "checked"; } ?>> Yes
												</div>
											</div>
											<hr>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/umur/jquery-ui.css"/>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-1.9.1.js"></script>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-ui.js"></script>
			
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
			<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
			<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
			<?php $url = base_url()."assets/auto/loading.gif"; ?>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
			<style>
				/* Autocomplete
				----------------------------------*/
				.ui-autocomplete { position: absolute; cursor: default; }
				.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

				/* workarounds */
				* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

				/* Menu
				----------------------------------*/
				.ui-menu {
					list-style:none;
					padding: 2px;
					margin: 0;
					display:block;
				}
				.ui-menu .ui-menu {
					margin-top: -3px;
				}
				.ui-menu .ui-menu-item {
					margin:0;
					padding: 0;
					zoom: 1;
					float: left;
					clear: left;
					width: 100%;
					font-size:80%;
				}
				.ui-menu .ui-menu-item a {
					text-decoration:none;
					display:block;
					padding:.2em .4em;
					line-height:1.5;
					zoom:1;
				}
				.ui-menu .ui-menu-item a.ui-state-hover,
				.ui-menu .ui-menu-item a.ui-state-active {
					font-weight: normal;
					margin: -1px;
				}
			</style>
			<script type="text/javascript">
				$(function(){
					$("#datepicker").datepicker({
						showButtonPanel: true,
						dateFormat: 'yy-mm-dd',
						maxDate: '-16Y',
						showTime: true
					});
				});
				$(this).ready( function()
				{
					$("#KotaID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_kota",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#PropinsiID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_propinsi",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#KotaID2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_kota",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#PropinsiID2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_propinsi",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#Agama").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
								var agama1 = $(".agama1").val();
								$(".agama2").val(agama1);
								$(".agama3").val(agama1);
							},
					});
				});
				$(this).ready( function()
				{
					$("#Agama2").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#Agama3").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_agama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				$(this).ready( function()
				{
					$("#SekolahID").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/prospects/lookup_sekolah",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);
							},
					});
				});
				window.onload=function(){
					$('#datepicker').on('change', function(){
						var dob = new Date(this.value);
						var today = new Date();
						var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
						$('#umur').val(age);
					});
				}
				$(function(){
					$('select.styled').customSelect();
				});
			</script>