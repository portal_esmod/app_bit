			<style>
				div.paging {
					padding		: 2px;
					margin		: 2px;
					text-align	: center;
					font-family	: Tahoma;
					font-size	: 16px;
					font-weight	: bold;
				}
				div.paging a {
					padding				: 2px 6px;
					margin-right		: 2px;
					border				: 1px solid #DEDFDE;
					text-decoration		: none;
					color				: #dc0203;
					background-position	: bottom;
				}
				div.paging a:hover {
					background-color: #0063dc;
					border : 1px solid #fff;
					color  : #fff;
				}
				div.paging span.current {
					border : 1px solid #DEDFDE;
					padding		 : 2px 6px;
					margin-right : 2px;
					font-weight  : bold;
					color        : #FF0084;
				}
				div.paging span.disabled {
					padding      : 2px 6px;
					margin-right : 2px;
					color        : #ADAAAD;
					font-weight  : bold;
				}
				div.paging span.prevnext {    
				  font-weight : bold;
				}
				div.paging span.prevnext a {
					 border : none;
				}
				div.paging span.prevnext a:hover {
					display: block;
					border : 1px solid #fff;
					color  : #fff;
				}
			</style>
			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Prospects</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Prospects
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of prospects.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_periode"); ?>" method="POST">
														<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PERIOD --</option>
															<?php
																if($periode)
																{
																	$cekperiode = $this->session->userdata('pros_filter_periode');
																	foreach($periode as $per)
																	{
																		echo "<option value='".$per->PMBPeriodID."'";
																		if($cekperiode == $per->PMBPeriodID)
																		{
																			echo "selected";
																		}
																		echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_marketing"); ?>" method="POST">
														<select name="cekmarketing" title="Filter by Marketing Staff" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- MARKETING --</option>
															<?php
																if($marketing)
																{
																	$cekmarketing = $this->session->userdata('pros_filter_marketing');
																	echo "<option value='OLREG'";
																	if($cekmarketing == "OLREG")
																	{
																		echo "selected";
																	}
																	echo ">OLREG - ONLINE REGISTRATION</option>";
																	foreach($marketing as $mar)
																	{
																		echo "<option value='$mar->id_akun'";
																		if($cekmarketing == $mar->id_akun)
																		{
																			echo "selected";
																		}
																		echo ">$mar->id_akun - $mar->nama</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_type"); ?>" method="POST">
														<select name="cektype" title="Filter by Type" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- TYPE --</option>
															<?php
																$cektype = $this->session->userdata('pros_filter_type');
																echo "<option value='HOT'"; if($cektype == "HOT") { echo "selected"; } echo ">HOT - Potensial</option>";
																echo "<option value='USUAL'"; if($cektype == "USUAL") { echo "selected"; } echo ">USUAL - Biasa</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("prospects/ptl_form"); ?>" title="Add New Data" class="btn btn-success">Add New</a></p></th>
											</tr>
											<tr>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_status"); ?>" method="POST">
														<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- STATUS --</option>
															<?php
																$cekstatus = $this->session->userdata('pros_filter_status');
																echo "<option value='Y'"; if($cekstatus == "Y") { echo "selected"; } echo ">STEP DOWN</option>";
																echo "<option value='N'"; if($cekstatus == "N") { echo "selected"; } echo ">NOT STEP DOWN</option>";
																echo "<option value='P'"; if($cekstatus == "P") { echo "selected"; } echo ">POSTPONE</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_program"); ?>" method="POST">
														<select name="cekpogram" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PROGRAM --</option>
															<?php
																$cekpogram = $this->session->userdata('pros_filter_program');
																echo "<option value='INT'"; if($cekpogram == "INT") { echo "selected"; } echo ">INT - INTENSIVE</option>";
																if($programd1)
																{
																	foreach($programd1 as $d1)
																	{
																		echo "<option value='$d1->ProdiID'";
																		if($cekpogram == $d1->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																	}
																}
																echo "<option value='REG'"; if($cekpogram == "REG") { echo "selected"; } echo ">REG - REGULAR</option>";
																if($programd3)
																{
																	foreach($programd3 as $d3)
																	{
																		echo "<option value='$d3->ProdiID'";
																		if($cekpogram == strtoupper($d3->ProdiID))
																		{
																			echo "selected";
																		}
																		echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																	}
																}
																echo "<option value='SC'"; if($cekpogram == "SC") { echo "selected"; } echo ">SC - SHORT COURSE</option>";
																if($shortcourse)
																{
																	foreach($shortcourse as $sc)
																	{
																		echo "<option value='$sc->KursusSingkatID'";
																		if($cekpogram == $sc->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_no"); ?>" method="POST">
														<select name="cekno" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>10 data</option>
															<?php
																$cekno = $this->session->userdata('pros_filter_no');
																echo "<option value='25'"; if($cekno == '25'){ echo "selected"; } echo ">25 data</option>";
																echo "<option value='50'"; if($cekno == '50'){ echo "selected"; } echo ">50 data</option>";
																echo "<option value='100'"; if($cekno == '100'){ echo "selected"; } echo ">100 data</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("admission/ptl_pdf_public/PROS"); ?>" title="Print This Result" class="btn btn-primary">Print This Result</a></p></th>
											</tr>
											<tr>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_tahun"); ?>" method="POST">
														<select name="cektahun" title="Filter by Year" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- YEAR --</option>
															<?php
																if($tahun)
																{
																	$cektahun = $this->session->userdata('pros_filter_tahun');
																	foreach($tahun as $thn)
																	{
																		echo "<option value='$thn->tahun'";
																		if($cektahun == $thn->tahun)
																		{
																			echo "selected";
																		}
																		echo ">$thn->tahun</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/ptl_filter_pros_month"); ?>" method="POST">
														<select name="cekbulan" title="Filter by Month" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- MONTH --</option>
															<?php
																if($cektahun != "")
																{
																	$cekbulan = $this->session->userdata('pros_filter_month');
																	for($ibln=1;$ibln<=12;$ibln++)
																	{
																		echo "<option value='".sprintf('%02s', $ibln)."'";
																		if($cekbulan == sprintf('%02s', $ibln))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper(bulan_eng(sprintf('%02s', $ibln)))."</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</p></th>
												<th><p valign="middle" align="center">
													<form action="<?php echo site_url("prospects/search"); ?>" method="POST">
														<input type="text" name="cari" value="<?php echo $pencarian; ?>" class="form-control" placeholder="Search.....">
												</p></th>
												<th><p valign="middle" align="center">
														<button type="submit" class="btn btn-primary">Search</button>
													</form>
													<?php
														if($pencarian != "")
														{
															echo "<a href='".site_url("prospects")."' class='btn btn-warning'>Clear</a>";
														}
													?>
												</p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><p valign="middle" align="center">NO</p></th>
													<th><p valign="middle" align="center">APL ID</p></th>
													<th><p valign="middle" align="center">STAGE</p></th>
													<th><p valign="middle" align="center">NAME</p><hr><p valign="middle" align="center">MARKETING</p></th>
													<th><p valign="middle" align="center">BIRTH</p></th>
													<th><p valign="middle" align="center">AGE</p></th>
													<th><p valign="middle" align="center">SCHOOL</p></th>
													<th><p valign="middle" align="center">THREE YEARS</p><hr><p valign="middle" align="center">ONE YEAR</p></th>
													<th><p valign="middle" align="center">NOTE</p></th>
													<th><p valign="middle" align="center">ACTIONS</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($cek)
													{
														$no = 1;
														foreach($cariproduk->result() as $row)
														{
															if($row->NA =="Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																if($row->StatusAplikanID =="APL")
																{
																	echo "<tr class='info'>";
																}
																else
																{
																	echo "<tr class='success'>";
																}
															}
															$id_akun = $row->PresenterID;
															$resakses = $this->m_akun->PTL_select($id_akun);
															$NamaMarketing = '';
															if($resakses)
															{
																$word = explode(" ",$resakses['nama']);
																$NamaMarketing = @$word[0];
															}
															$Pilihan1 = $row->Pilihan1;
															// if($row->Pilihan1 == 0)
															// {
																// $Pilihan1 = "-";
															// }
															$Pilihan2 = $row->Pilihan2;
															if($row->Pilihan2 == 0)
															{
																$Pilihan2 = "-";
															}
															echo "<td>$no</td>
																<td>
																	$row->AplikanID
																	<br/>
																	<br/>";
																	$id_lead = $row->id_lead;
																	$reslead = $this->m_lead->FHM_select($id_lead);
																	if($reslead)
																	{
																		echo "<a href='".base_url("../crm/sales/fhm_edit/$id_lead")."' title='Go to CRM' target='_blank'><span class='label-success label label-default'>CRM Data</span></a>";
																	}
															echo "</td>
																<td>$row->StatusAplikanID</td>
																<td>$row->Nama<hr><p align='right'>$NamaMarketing $row->PMBPeriodID</p></td>
																<td>$row->TempatLahir, ".tgl_singkat_eng($row->TanggalLahir)."</td>
																<td>$row->umur</td>
																<td>$row->AsalSekolah</td>
																<td>$Pilihan1<hr>$Pilihan2</td>
																<td>$row->CatatanPresenter</td>
																<td>
																	<a href='".site_url("prospects/ptl_edit/$row->AplikanID")."' title='Edit this Prospect' class='btn btn-primary'><i class='icon-list'></i> Edit&nbsp;&nbsp;&nbsp;</a>
																	<a href='".site_url("prospects/ptl_copy/$row->AplikanID")."' title='Copy this Prospect' class='btn btn-success'><i class='icon-list'></i> Copy&nbsp;</a>
																	<a href='".site_url("notif/ptl_edit/$row->AplikanID")."' title='Chat with this Prospect' class='btn btn-info' target='_blank'><i class='icon-comments-alt'></i> Chat&nbsp;&nbsp;</a>
																	<a href='".site_url("sms/ptl_form/$row->AplikanID")."' title='Send SMS to this Prospect' class='btn btn-warning' target='_blank'><i class='icon-phone'></i> SMS&nbsp;&nbsp;</a>";
																	?>
																		<a href="<?php echo base_url()."prospects/ptl_delete/$row->AplikanID"; ?>" title="Delete this Prospect" onclick="return confirm('Are you sure to remove the APPLICANT with CODE <?php echo $row->AplikanID.' - '.$row->Nama; ?>?')" class="btn btn-danger"><i class="icon-remove"></i> Delete</a>
																	<?php
															echo "</td>
															</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
										<?php
											echo $pagination;
											echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>