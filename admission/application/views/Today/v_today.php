			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Schedule Today</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Schedule Today
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is the schedule for students.
									</div>
									<div class="table-responsive">
										<form action="<?php echo site_url("today/ptl_filter_tanggal"); ?>" method="POST" id="formId">
											<table class="table table-striped table-bordered table-hover">
												<tr>
													<td><input type="text" name="cektanggal" value="<?php echo $today; ?>" id="Date1" class="form-control"></td>
												</tr>
											</table>
										</form>
										<?php
											echo "<center><h2>".day_name($today).", ".tgl_singkat_eng($today)."</h2></center>";
										?>
									</div>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>Date</th>
													<th>Time</th>
													<th>Subjects</th>
													<th>Class</th>
													<th>Session</th>
													<th>Topic</th>
													<th>Lecturer</th>
													<th>Room</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$h = "-7";
														$hm = $h * 60;
														$ms = $hm * 60;
														$Jam = gmdate("Hi", time()-($ms));
														$no = 1;
														foreach($rowrecord as $row)
														{
															$JadwalID = $row->JadwalID;
															$jdw = $this->m_jadwal->PTL_select($JadwalID);
															$sub = "";
															$kelas = "";
															if($jdw)
															{
																if($jdw["Gabungan"] == "Y")
																{
																	$KelasIDGabungan = $jdw["KelasIDGabungan"];
																	$wordgab = explode(".",$KelasIDGabungan);
																	for($i=0;$i<30;$i++)
																	{
																		$wg = explode("^",@$wordgab[$i]);
																		$KelasID = @$wg[1];
																		$res = $this->m_kelas->PTL_select_kelas($KelasID);
																		if($res)
																		{
																			$kls = $res["Nama"];
																		}
																		else
																		{
																			$kls = "";
																		}
																		if($i == 0)
																		{
																			$kelas .= @$wg[0].$kls;
																		}
																		else
																		{
																			if($wg[0] != "")
																			{
																				$kelas .= @$wg[0].$kls.", ";
																			}
																		}
																	}
																}
																else
																{
																	$KelasID = $jdw["KelasID"];
																	$res = $this->m_kelas->PTL_select_kelas($KelasID);
																	if($res)
																	{
																		if($res["ProgramID"] == "INT")
																		{
																			$kelas = "O".$res["Nama"];
																		}
																		else
																		{
																			$kelas = $jdw["TahunKe"].$res["Nama"];
																		}
																	}
																}
																$SubjekID = $jdw["SubjekID"];
																$subjek = $this->m_subjek->PTL_select($SubjekID);
																if($subjek)
																{
																	$sub = $subjek["Nama"];
																}
															}
															$MKID = $row->MKID;
															$mk = $this->m_mk->PTL_select($MKID);
															$topic = "<font color='red'><b>Not set</b></font>";
															if($mk)
															{
																$topic = $mk["Nama"];
															}
															$DosenID = $row->DosenID;
															$dosen = $this->m_dosen->PTL_select($DosenID);
															$dsn = "<font color='red'><b>Not set</b></font>";
															if($dosen)
															{
																$dsn = $dosen["Nama"];
															}
															$b1 = "";
															$b2 = "";
															if($_COOKIE["nama"] == $dsn)
															{
																$b1 = "<font color='red'><b>";
																$b2 = "</b></font>";
															}
															$JamA = str_replace(":","",substr($row->JamMulai,0,5));
															$JamB = str_replace(":","",substr($row->JamSelesai,0,5));
															if(($Jam >= $JamA) AND ($Jam <= $JamB))
															{
																echo "<tr class='warning'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
																echo "<td title='PresensiID : $row->PresensiID'>$b1$no$b2</td>
																	<td>$b1$row->Tanggal$b2</td>
																	<td>$b1".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."$b2</td>
																	<td>$b1$sub$b2</td>
																	<td>$b1$kelas$b2</td>
																	<td align='right'>$b1$row->Pertemuan$b2</td>
																	<td>$b1$topic$b2</td>
																	<td>$b1$dsn$b2</td>
																	<td>$b1$row->RuangID$b2</td>
																</tr>";
															$no++;
														}
													}
													else
													{
														echo "<tr class='info'>
																<td colspan='9' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>