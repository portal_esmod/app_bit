			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Activity Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("activity"); ?>">Activity</a>
									>
									<a href="<?php echo site_url("activity/ptl_edit/$id_aktivitas"); ?>">Activity Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('activity/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Activity Name</label>
												<div class="col-lg-8">
													<input type="text" name="nama_aktivitas" value="<?php echo $nama_aktivitas; ?>" placeholder="" class="form-control" />
													<input type="hidden" name="id_aktivitas" value="<?php echo $id_aktivitas; ?>" />
													<input type="hidden" name="file_order" value="<?php echo $file; ?>" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Content</label>
												<div class="col-lg-8">
													<textarea name="isi" id="cleditor" col="3" class="form-control"><?php echo $isi; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-4">File</label>
												<div class="col-lg-8">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<a href="<?php echo base_url(); ?>activity/ptl_download/<?php echo $file; ?>" title="<?php echo $file; ?>" class="btn btn-success btn-sm btn-round btn-line" ><?php $file_order = substr($file,0,33); echo $file_order; ?></a>
														<br/>
														<br/>
														<span class="btn btn-file btn-default">
															<span class="fileupload-new">Select file</span>
															<span class="fileupload-exists">Change</span>
															<input name="userfile" type="file" class="default" accept="*" />
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Responsible Person</label>
												<div class="col-lg-8">
													<input type="text" name="penanggung_jawab" value="<?php echo $penanggung_jawab; ?>" id="nama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Start - End Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglMulaiTglSelesai" value="<?php echo $dari_tanggal." - ".$sampai_tanggal; ?>" id="reservation" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Publication</label>
												<div class="col-lg-8">
													<input type="checkbox" name="publikasi1" value="PRIVATE" <?php if($publikasi1 == "PRIVATE"){echo "checked";}?> > Private&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="checkbox" name="publikasi2" value="LMS" <?php if($publikasi2 == "LMS"){echo "checked";}?> > LMS&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Status</label>
												<div class="col-lg-8">
													<input type="radio" name="status" value="Y" <?php if($status == "Y"){echo "checked";}?> > Done&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="status" value="P" <?php if($status == "P"){echo "checked";}?> > Process&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="status" value="N" <?php if($status == "N"){echo "checked";}?> > Planning&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/umur/jquery-ui.css"/>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-1.9.1.js"></script>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-ui.js"></script>
			
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
			<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
			<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
			<?php $url = base_url()."assets/auto/loading.gif"; ?>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
			<style>
				/* Autocomplete
				----------------------------------*/
				.ui-autocomplete { position: absolute; cursor: default; }
				.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

				/* workarounds */
				* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

				/* Menu
				----------------------------------*/
				.ui-menu {
					list-style:none;
					padding: 2px;
					margin: 0;
					display:block;
				}
				.ui-menu .ui-menu {
					margin-top: -3px;
				}
				.ui-menu .ui-menu-item {
					margin:0;
					padding: 0;
					zoom: 1;
					float: left;
					clear: left;
					width: 100%;
					font-size:80%;
				}
				.ui-menu .ui-menu-item a {
					text-decoration:none;
					display:block;
					padding:.2em .4em;
					line-height:1.5;
					zoom:1;
				}
				.ui-menu .ui-menu-item a.ui-state-hover,
				.ui-menu .ui-menu-item a.ui-state-active {
					font-weight: normal;
					margin: -1px;
				}
			</style>
			<script type="text/javascript">
				$(this).ready( function()
				{
					$("#nama").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/activity/lookup_nama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
			</script>