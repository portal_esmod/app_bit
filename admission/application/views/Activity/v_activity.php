			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Event Activity</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Event Activity
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of event activity.
									</div>
									<div class="table-responsive">
										<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><p valign="middle" align="center"><a href="<?php echo site_url("activity/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
													<!--<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Period" class="form-control round-form">
															<option value=''>-- PERIOD --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>
													<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Marketing Staff" class="form-control round-form">
															<option value=''>-- MARKETING --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>
													<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Type" class="form-control round-form">
															<option value=''>-- TYPE --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>-->
												</tr>
												<!--<tr>
													<th><p valign="middle" align="center"><a href="#" title="Print This Result" class="btn btn-primary">Print This Result</a></p></th>
													<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Status" class="form-control round-form">
															<option value=''>-- STATUS --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>
													<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Program" class="form-control round-form">
															<option value=''>-- PROGRAM --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>
													<th><p valign="middle" align="center">
														<select name="PendidikanIbu" title="Filter by Year" class="form-control round-form">
															<option value=''>-- YEAR --</option>
															<?php
																if($ortu)
																{
																	foreach($ortu as $rowortu)
																	{
																		echo "<option value='".strtoupper($rowortu->Nama)."'";
																		if($PendidikanIbu == strtoupper($rowortu->Nama))
																		{
																			echo "selected";
																		}
																		echo ">".strtoupper($rowortu->Nama)."</option>";
																	}
																}
															?>
														</select>
													</p></th>
												</tr>-->
											</thead>
										</table>
									</div>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ACTIVITY ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">FILE</p></th>
													<th><p valign="middle" align="center">RESPONSIBLE PERSON</p></th>
													<th><p valign="middle" align="center">FROM</p></th>
													<th><p valign="middle" align="center">UNTIL</p></th>
													<th><p valign="middle" align="center">STATUS</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->status == "N")
															{
																echo "<tr class='danger'>";
															}
															if($row->status == "P")
															{
																echo "<tr class='warning'>";
															}
															if($row->status == "Y")
															{
																echo "<tr class='success'>";
															}
																echo "<td><p align='center'>";
												?>
																			<a href="<?php echo site_url("activity/ptl_delete/$row->id_aktivitas"); ?>" onclick="return confirm('Are you sure to remove ACTIVITY with CODE <?php echo $row->id_aktivitas." - ".$row->nama_aktivitas; ?>?')" title='Delete this Activity' class='btn btn-danger btn-sm btn-round btn-line'>X</a>
												<?php
																		echo "<a href='".base_url()."activity/ptl_edit/$row->id_aktivitas' title='Edit this Activity' class='btn btn-primary btn-sm btn-round btn-line'>$row->id_aktivitas</a>
																		</p>
																	</td>
																	<td>$row->nama_aktivitas</td>
																	<td>";
												?>
																	<p align='center'><a href="<?php echo base_url(); ?>activity/ptl_download/<?php echo $row->file; ?>" title="<?php echo $row->file; ?>" class="btn btn-danger btn-sm btn-round btn-line" ><?php $file_order = substr($row->file,0,33); echo $file_order; ?></a></p>
												<?php
																echo "
																	<td><p align='center'>$row->penanggung_jawab</p></td>
																	<td><p align='center'>$row->dari_tanggal</p></td>
																	<td><p align='center'>$row->sampai_tanggal</p></td>
																	<td>
																		<p align='center'>";
																		if($row->status == "N")
																		{
																			echo "PLANNING";
																		}
																		if($row->status == "P")
																		{
																			echo "PROCESS";
																		}
																		if($row->status == "Y")
																		{
																			echo "DONE";
																		}
																	echo "</p>
																	</td>
																</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>