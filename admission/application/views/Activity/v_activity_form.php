			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Activity Form</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("activity"); ?>">Activity</a>
									>
									<a href="<?php echo site_url("activity/ptl_form"); ?>">Activity Form</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('activity/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Activity Name</label>
												<div class="col-lg-8">
													<input type="text" name="nama_aktivitas" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Content</label>
												<div class="col-lg-8">
													<textarea name="isi" id="cleditor" col="3" class="form-control"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-4">File</label>
												<div class="col-lg-8">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn btn-file btn-default">
															<span class="fileupload-new">Select file</span>
															<span class="fileupload-exists">Change</span>
															<input name="userfile" type="file" class="default" accept="*" />
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Responsible Person</label>
												<div class="col-lg-8">
													<input type="text" name="penanggung_jawab" id="nama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Start - End Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglMulaiTglSelesai" id="reservation" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Publication</label>
												<div class="col-lg-8">
													<input type="checkbox" name="publikasi1" value="PRIVATE"> Private&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="checkbox" name="publikasi2" value="LMS"> LMS&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Status</label>
												<div class="col-lg-8">
													<input type="radio" name="status" value="Y" > Done&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="status" value="P" > Process&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="status" value="N" > Planing&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/umur/jquery-ui.css"/>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-1.9.1.js"></script>
			<script src="<?php echo base_url(); ?>assets/umur/jquery-ui.js"></script>
			
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
			<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
			<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
			<?php $url = base_url()."assets/auto/loading.gif"; ?>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
			<style>
				/* Autocomplete
				----------------------------------*/
				.ui-autocomplete { position: absolute; cursor: default; }
				.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

				/* workarounds */
				* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

				/* Menu
				----------------------------------*/
				.ui-menu {
					list-style:none;
					padding: 2px;
					margin: 0;
					display:block;
				}
				.ui-menu .ui-menu {
					margin-top: -3px;
				}
				.ui-menu .ui-menu-item {
					margin:0;
					padding: 0;
					zoom: 1;
					float: left;
					clear: left;
					width: 100%;
					font-size:80%;
				}
				.ui-menu .ui-menu-item a {
					text-decoration:none;
					display:block;
					padding:.2em .4em;
					line-height:1.5;
					zoom:1;
				}
				.ui-menu .ui-menu-item a.ui-state-hover,
				.ui-menu .ui-menu-item a.ui-state-active {
					font-weight: normal;
					margin: -1px;
				}
			</style>
			<script type="text/javascript">
				$(this).ready( function()
				{
					$("#nama").autocomplete(
					{
						minLength: 1,
						source: 
						function(req, add)
						{
							$.ajax(
							{
								url: "<?php echo base_url(); ?>index.php/activity/lookup_nama",
								dataType: 'json',
								type: 'POST',
								data: req,
								success:    
								function(data)
								{
									if(data.response =="true")
									{
										add(data.message);
									}
								},
							});
						},
						select: 
							function(event, ui)
							{
								$("#result").append
								(
									"<li>"+ ui.item.value + "</li>"
								);           		
							},		
					});
				});
			</script>