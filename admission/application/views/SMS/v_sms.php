			<script>
				function hitungkarakter(field,sisa,maksimal)
				{
					field.maxLength = maksimal;
					sisa.value = maksimal - field.value.length;
				}
			</script>
			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>SMS Gateway</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("prospects"); ?>">Prospects</a>
									>
									<a href="<?php echo site_url("sms/ptl_form/$AplikanID"); ?>">SMS Gateway</a>
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>You can send SMS to your prospects in this page.
									</div>
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('sms/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<table class="table table-striped table-bordered table-hover">
													<tr>
														<td>Applicant ID</td>
														<td>
															<input readonly type="text" name="AplikanID" value="<?php echo $AplikanID; ?>" placeholder="" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td>Name</td>
														<td>
															<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td>Subject</td>
														<td>
															<input type="text" name="subjek" value="Information" placeholder="" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td align="right">
															<?php
																if($Handphone == "")
																{
																	echo "<font color='red'><b>No data</b></font>";
																}
																else
																{
																	echo "<input type='checkbox' name='kirim1' value='Y' checked/>";
																}
															?>
														</td>
														<td>
															<input type="hidden" name="hp_mahasiswa" value="<?php echo $Handphone; ?>">
															<label>Send SMS to student (<?php echo $Handphone; ?>)</label>
														</td>
													</tr>
													<tr>
														<td align="right">
															<?php
																if($HandphoneOrtu == "")
																{
																	echo "<font color='red'><b>No data</b></font>";
																}
																else
																{
																	echo "<input type='checkbox' name='kirim2' value='Y'/>";
																}
															?>
														</td>
														<td>
															<input type="hidden" name="hp_orang_tua" value="<?php echo $HandphoneOrtu; ?>">
															<label>Send SMS to parent (<?php echo $HandphoneOrtu; ?>)</label>
														</td>
													</tr>
													<tr>
														<td align="right">
															<?php
																if($Email == "")
																{
																	echo "<font color='red'><b>No data</b></font>";
																}
																else
																{
																	echo "<input type='checkbox' name='kirim3' value='Y'/>";
																}
															?>
														</td>
														<td>
															<input type="hidden" name="email_mahasiswa" value="<?php echo $Email; ?>">
															<label>Send email to student (<?php echo $Email; ?>)</label>
														</td>
													</tr>
													<tr>
														<td align="right">
															<?php
																if($EmailOrtu == "")
																{
																	echo "<font color='red'><b>No data</b></font>";
																}
																else
																{
																	echo "<input type='checkbox' name='kirim4' value='Y'/>";
																}
															?>
														</td>
														<td>
															<input type="hidden" name="email_orang_tua" value="<?php echo $EmailOrtu; ?>">
															<label>Send email to parent (<?php echo $EmailOrtu; ?>)</label>
														</td>
													</tr>
													<tr>
														<td>Messages</td>
														<td>
															<textarea name="pesan" class="form-control" onKeyUp="hitungkarakter(pesan,sisa,240);" required><?php echo $pesan; ?></textarea>
														</td>
													</tr>
													<tr>
														<td></td>
														<td>
															<input readonly disabled type="text" name="sisa" style="text-align:right;" value="<?php echo (240 - strlen($pesan)); ?>" size="3"/> characters remaining.
														</td>
													</tr>
												</table>
												<div class="form-group">
													<label for="text1" class="control-label col-lg-4"></label>
													<div class="col-lg-8">
														<a href="<?php echo site_url("prospects"); ?>" class="btn btn-warning">Back</a>
														<button type="reset" class="btn btn-danger">Reset</button>
														<input type="submit" value="Send SMS" class="btn btn-primary">
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>