			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Entry Add</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("entry"); ?>">Entry</a>
									>
									<a href="<?php echo site_url("entry/ptl_form"); ?>">Entry Add</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('entry/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Entry ID</label>
												<div class="col-lg-8">
													<input type="text" name="StatusAwalID" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Buy Of Form</label>
												<div class="col-lg-8">
													<input type="radio" name="BeliFormulir" value="N" placeholder="" /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="BeliFormulir" value="Y" placeholder="" /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Special</label>
												<div class="col-lg-8">
													<input type="radio" name="JalurKhusus" value="N" placeholder="" /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="JalurKhusus" value="Y" placeholder="" /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Special</label>
												<div class="col-lg-8">
													<input type="radio" name="TanpaTest" value="N" placeholder="" /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="TanpaTest" value="Y" placeholder="" /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Note</label>
												<div class="col-lg-8">
													<textarea name="Catatan" placeholder="" class="form-control" ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Active?</label>
												<div class="col-lg-8">
													<input type="radio" name="NA" value="Y" placeholder="" /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="NA" value="N" placeholder="" /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>