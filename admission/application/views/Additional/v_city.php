			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>City</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									City
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of city.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("city/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">CITY ID</p></th>
													<th><p valign="middle" align="center">PROVINCE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."city/ptl_delete/$row->KotaKabupaten"; ?>" title="Delete this City" onclick="return confirm('Are you sure to remove the CITY with CODE <?php echo $row->KotaKabupaten; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "$row->KotaID</p></td>";
																$PropinsiID = $row->PropinsiID;
																$result = $this->m_province->PTL_select($PropinsiID);
															echo "<td>".$result['Propinsi']."</td>
																<td>$row->KotaKabupaten</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>