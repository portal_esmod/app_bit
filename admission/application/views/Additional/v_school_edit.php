			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>School Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("school"); ?>">School</a>
									>
									<a href="<?php echo site_url("school/ptl_edit/$id_sekolah"); ?>">School Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('school/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">School ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="id_sekolah" value="<?php echo $id_sekolah; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Abbreviations</label>
												<div class="col-lg-8">
													<input type="text" name="SingkatanNama" value="<?php echo $SingkatanNama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Address</label>
												<div class="col-lg-8">
													<textarea name="alamat" placeholder="" class="form-control" ><?php echo $alamat; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">City</label>
												<div class="col-lg-8">
													<input type="text" name="Kota" value="<?php echo $Kota; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Postal Code</label>
												<div class="col-lg-8">
													<input type="number" name="KodePos" value="<?php echo $KodePos; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Phone</label>
												<div class="col-lg-8">
													<input type="number" name="Telephone" value="<?php echo $Telephone; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Fax</label>
												<div class="col-lg-8">
													<input type="number" name="Fax" value="<?php echo $Fax; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Website</label>
												<div class="col-lg-8">
													<input type="text" name="Website" value="<?php echo $Website; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Email</label>
												<div class="col-lg-8">
													<input type="email" name="Email" value="<?php echo $Email; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>