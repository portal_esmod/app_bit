			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Province Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("province"); ?>">Province</a>
									>
									<a href="<?php echo site_url("province/ptl_edit/$PropinsiID"); ?>">Province Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('province/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Province ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="PropinsiID" value="<?php echo $PropinsiID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Propinsi" value="<?php echo $Propinsi; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Active?</label>
												<div class="col-lg-8">
													<input type="radio" name="NA" value="Y" placeholder="" <?php if($NA == "Y"){echo "checked";}?> /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="NA" value="N" placeholder="" <?php if($NA == "N"){echo "checked";}?> /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>