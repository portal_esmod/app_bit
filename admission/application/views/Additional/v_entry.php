			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Entry</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Entry
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of entry.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("entry/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ENTRY ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">BUY OF FORM</p></th>
													<th><p valign="middle" align="center">SPECIAL</p></th>
													<th><p valign="middle" align="center">NO TEST</p></th>
													<th><p valign="middle" align="center">NOTE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."entry/ptl_delete/$row->StatusAwalID"; ?>" title="Delete this Entry" onclick="return confirm('Are you sure to remove the ENTRY with CODE <?php echo $row->StatusAwalID.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "<a href='".base_url()."entry/ptl_edit/$row->StatusAwalID' title='Edit this Entry' class='btn btn-primary btn-sm btn-round btn-line'>$row->StatusAwalID</a>
																</p></td>
																<td>$row->Nama</td>
																<td><p align='center'>$row->BeliFormulir</p></td>
																<td><p align='center'>$row->JalurKhusus</p></td>
																<td><p align='center'>$row->TanpaTest</p></td>
																<td>$row->Catatan</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>