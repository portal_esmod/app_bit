			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>School</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									School
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of school.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("school/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">SCHOOL ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">CITY</p></th>
													<th><p valign="middle" align="center">PHONE</p></th>
													<th><p valign="middle" align="center">WEBSITE</p></th>
													<th><p valign="middle" align="center">EMAIL</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$total = 0;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."school/ptl_delete/$row->id_sekolah"; ?>" title="Delete this School" onclick="return confirm('Are you sure to remove the SCHOOL with CODE <?php echo $row->id_sekolah.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
															echo "<a href='".base_url()."school/ptl_edit/$row->id_sekolah' title='Edit this School' class='btn btn-primary btn-sm btn-round btn-line'>$row->id_sekolah</a>
															</p></td>
																<td>$row->Nama</td>
																<td>$row->Kota $row->KodePos</td>
																<td>$row->Telephone</td>
																<td>$row->Website</td>
																<td>$row->Email</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>