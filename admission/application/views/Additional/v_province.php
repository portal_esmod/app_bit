			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Province</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Province
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of province.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("province/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">PROVINCE ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."province/ptl_delete/$row->PropinsiID"; ?>" title="Delete this Province" onclick="return confirm('Are you sure to remove the PROVINCE with CODE <?php echo $row->PropinsiID.' - '.$row->Propinsi; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "<a href='".base_url()."province/ptl_edit/$row->PropinsiID' title='Edit this Province' class='btn btn-primary btn-sm btn-round btn-line'>$row->PropinsiID</a>
																</p></td>
																<td>$row->Propinsi</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>