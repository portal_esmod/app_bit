			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Notification Detail</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("notif"); ?>">Notification</a>
									>
									<a href="<?php echo site_url("notif/ptl_detail/$kodelog"); ?>">Notification Detail</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('notif/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Log ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="kodelog" value="<?php echo $kodelog; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Account ID</label>
												<div class="col-lg-8">
													<input readonly type="text" value="<?php echo $id_akun; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Presenter</label>
												<div class="col-lg-8">
													<input readonly type="text" value="<?php echo $presenter; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Activity</label>
												<div class="col-lg-8">
													<input readonly type="text" value="<?php echo $aktivitas; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Date</label>
												<div class="col-lg-8">
													<input readonly type="text" value="<?php echo $tanggal; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<input type="submit" value="READ" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>