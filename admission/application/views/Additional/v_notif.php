			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Notification</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Notification
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">NO</p></th>
													<th><p valign="middle" align="center">ACCOUNT ID</p></th>
													<th><p valign="middle" align="center">PRESENTER</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">ACTIVITY</p></th>
													<th><p valign="middle" align="center">DATE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if(($row->dari == "") AND ($row->untuk == "") OR ($row->untuk == $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"]))
															{
																if($row->status == "READ")
																{
																	echo "<tr class='success'>";
																}
																if(($row->status == "UNREAD") OR ($row->status == ""))
																{
																	echo "<tr class='danger'>";
																}
																if(($row->dari == "") AND ($row->untuk == ""))
																{
																	echo "<td><p align='center'>
																		<a href='notif/ptl_detail/$row->kodelog' class='btn btn-primary btn-sm btn-round btn-line'>$no</a>
																	</p></td>";
																}
																else
																{
																	echo "<td><p align='center'>
																		<a href='notif/ptl_edit/$row->id_akun' class='btn btn-primary btn-sm btn-round btn-line'>$no</a>
																	</p></td>";
																}
																echo "<td><p align='center'>$row->id_akun</p></td>
																	<td>$row->presenter</td>
																	<td>$row->nama</td>
																	<td>$row->aktivitas</td>
																	<td><p align='center'>$row->tanggal</p></td>
																</tr>";
																$no++;
															}
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>