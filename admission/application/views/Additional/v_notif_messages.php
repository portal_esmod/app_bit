			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Messages</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("notif"); ?>">Notification</a>
									>
									<a href="<?php echo site_url("notif/ptl_edit/$id_akun"); ?>">Messages</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<div class="col-lg-13">
											<div class="chat-panel panel panel-primary">
												<div class="panel-heading">
													<i class="icon-comments"></i>
													Messages
													<div class="btn-group pull-right">
														<button type="button" data-toggle="dropdown">
															<i class="icon-chevron-down"></i>
														</button>
														<ul class="dropdown-menu slidedown">
															<li>
																<a href="<?php echo site_url("notif/ptl_edit/$id_akun"); ?>">
																	<i class="icon-refresh"></i> Refresh
																</a>
															</li>
															<li>
																<a href="<?php echo site_url("notif/ptl_update_chat_online/$id_akun"); ?>">
																	<i class=" icon-comment"></i> Available
																</a>
															</li>
															<li class="divider"></li>
															<li>
																<a href="<?php echo site_url("notif/ptl_update_chat_offline/$id_akun"); ?>">
																	<i class="icon-signout"></i> Offline
																</a>
															</li>
														</ul>
													</div>
												</div>
												<div class="panel-body">
													<ul class="chat">
														<?php
															$id_aplikan = $id_akun;
															$result = $this->m_aplikan->PTL_select($id_aplikan);
															$dir = substr($result['foto'],0,7);
															$file = substr($result['foto'],8);
															if($result['foto'] == "-_-")
															{
																$img_apl = "<img src='".base_url()."../applicant/ptl_storage/admisi/aplikan/user.png' alt='User Avatar' width='80px' class='img-circle'/>";
															}
															else
															{
																$img_apl = "<img src='".base_url()."../applicant/ptl_storage/admisi/aplikan/$dir/$file' alt='User Avatar' width='80px' class='img-circle'/>";
															}
															$foto = "foto_karyawan/user.jpg";
															$exist = file_exists_remote(base_url("ptl_storage/$foto"));
															if($exist)
															{
																$foto = $foto;
															}
															else
															{
																$foto = "foto_umum/user.jpg";
															}
															$img_mkt = "<img src='".base_url("ptl_storage/$foto")."' alt='User Avatar' width='80px' class='img-circle' />";
															foreach($rowrecord as $row)
															{
																if($row->untuk == $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"])
																{
																	$word = explode("_",$row->dari);
																	echo "<li class='left clearfix'>
																			<span class='chat-img pull-left'>
																				$img_apl
																			</span>
																			<div class='chat-body clearfix'>
																				<div class='header'>
																					<strong class='primary-font'>".$word[0]." ".$word[1]."</strong>
																					<small class='pull-right text-muted'>
																						<i class='icon-time'></i> $row->tanggal $row->status
																					</small>
																				</div>
																				<br/>
																				<p>$row->aktivitas</p>
																			</div>
																		</li>";
																}
																if($row->dari == $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"])
																{
																	echo "<li class='right clearfix'>
																			<span class='chat-img pull-right'>
																				$img_mkt
																			</span>
																			<div class='chat-body clearfix'>
																				<div class='header'>
																					<small class='text-muted'>
																						<i class='icon-time'></i> $row->tanggal $row->status
																					</small>
																					<strong class='pull-right primary-font'>YOU</strong>
																				</div>
																				<br/>
																				<p>$row->aktivitas</p>
																			</div>
																		</li>";
																}
															}
														?>
													</ul>
												</div>
												<?php echo form_open_multipart('notif/ptl_insert',array('class' => 'form-horizontal style-form')); ?>
													<div class="panel-footer">
														<div class="input-group">
															<input type="hidden" name="id_akun" value="<?php echo $id_akun; ?>" />
															<input type="hidden" name="nama" value="<?php echo $result['Nama']; ?>" />
															<input type="hidden" name="untuk" value="<?php echo $id_akun."_".$result['Nama']."_STUDENT"; ?>" />
															<input type="text" name="aktivitas" id="btn-input" class="form-control input-sm" placeholder="Type your message here..." />
															<span class="input-group-btn">
																<button type="submit" class="btn btn-warning btn-sm" id="btn-chat">
																	Send
																</button>
															</span>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>