			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Religion Add</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("religion"); ?>">Religion</a>
									>
									<a href="<?php echo site_url("religion/ptl_form"); ?>">Religion Add</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('religion/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Religion ID</label>
												<div class="col-lg-8">
													<input type="text" name="Agama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>