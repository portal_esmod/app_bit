			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Grade Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("grade"); ?>">Grade</a>
									>
									<a href="<?php echo site_url("grade/ptl_edit/$GradeID"); ?>">Grade Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('grade/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Grade ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="GradeID" value="<?php echo $GradeID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Grade</label>
												<div class="col-lg-8">
													<input type="text" name="Grade" value="<?php echo $Grade; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Grade Score</label>
												<div class="col-lg-8">
													<input type="text" name="GradeNilai" value="<?php echo $GradeNilai; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Minimum Score</label>
												<div class="col-lg-8">
													<input type="text" name="NilaiUjianMin" value="<?php echo $NilaiUjianMin; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Interview</label>
												<div class="col-lg-8">
													<input type="radio" name="Wawancara" value="N" placeholder="" <?php if($Wawancara == "N"){echo "checked";}?> /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="Wawancara" value="Y" placeholder="" <?php if($Wawancara == "Y"){echo "checked";}?> /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Graduated</label>
												<div class="col-lg-8">
													<input type="radio" name="Lulus" value="N" placeholder="" <?php if($Lulus == "N"){echo "checked";}?> /> No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="Lulus" value="Y" placeholder="" <?php if($Lulus == "Y"){echo "checked";}?> /> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Description</label>
												<div class="col-lg-8">
													<textarea name="Keterangan" placeholder="" class="form-control" ><?php echo $Keterangan; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>