			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Grade</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Grade
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of grade.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("grade/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">GRADE ID</p></th>
													<th><p valign="middle" align="center">GRADE</p></th>
													<th><p valign="middle" align="center">GRADE SCORE</p></th>
													<th><p valign="middle" align="center">MINIMUM SCORE</p></th>
													<th><p valign="middle" align="center">INTERVIEW</p></th>
													<th><p valign="middle" align="center">GRADUATED</p></th>
													<th><p valign="middle" align="center">DESCRIPTION</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."grade/ptl_delete/$row->GradeID"; ?>" title="Delete this Grade" onclick="return confirm('Are you sure to remove the GRADE with CODE <?php echo $row->GradeID.' - '.$row->GradeNilai; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "<a href='".base_url()."grade/ptl_edit/$row->GradeID' title='Edit this Grade' class='btn btn-primary btn-sm btn-round btn-line'>$no</a>
																</p></td>
																<td>$row->Grade</td>
																<td>$row->GradeNilai</td>
																<td><p align='right'>$row->NilaiUjianMin</p></td>
																<td><p align='center'>$row->Wawancara</p></td>
																<td><p align='center'>$row->Lulus</p></td>
																<td>$row->Keterangan</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>