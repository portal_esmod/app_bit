			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Allocation Class</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Allocation Class
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of allocation class.
									</div>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ALLOCATION ID</p></th>
													<th><p valign="middle" align="center">SPECIALIZE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">CAPACITY NOW</p></th>
													<th><p valign="middle" align="center">MAX CAPACITY</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>$row->KelasID</p></td>";
																$SpesialisasiID = $row->SpesialisasiID;
																$result = $this->m_spesialisasi->PTL_select($SpesialisasiID);
															echo "<td>".@$result['Nama']."</td>
																<td>$row->Nama</td>
																<td><p align='right'>$row->KapasitasSekarang</p></td>
																<td><p align='right'>$row->KapasitasMaksimum</p></td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>