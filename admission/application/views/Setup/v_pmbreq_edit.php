			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Requirement Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmbreq"); ?>">Requirement</a>
									>
									<a href="<?php echo site_url("pmbreq/ptl_edit/$PMBSyaratID"); ?>">Requirement Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmbreq/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Requirement ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="PMBSyaratID" value="<?php echo $PMBSyaratID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Sequence</label>
												<div class="col-lg-8">
													<input type="number" name="Urutan" value="<?php echo $Urutan; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Description</label>
												<div class="col-lg-8">
													<textarea name="Keterangan" placeholder="" class="form-control" ><?php echo $Keterangan; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Active?</label>
												<div class="col-lg-8">
													<input type="radio" name="NA" value="Y" <?php if($NA == "Y"){echo "checked";}?> > No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="NA" value="N" <?php if($NA == "N"){echo "checked";}?> > Yes&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>