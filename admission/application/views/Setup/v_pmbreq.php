			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Requirement</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Requirement
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of requirement.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("pmbreq/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">REQUIREMENT ID</p></th>
													<th><p valign="middle" align="center">SEQUENCE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."pmbreq/ptl_delete/$row->PMBSyaratID"; ?>" title="Delete this PMB Requirement" onclick="return confirm('Are you sure to remove the PMB REQUIREMENT with CODE <?php echo $row->PMBSyaratID.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "<a href='".base_url()."pmbreq/ptl_edit/$row->PMBSyaratID' title='Edit this PMB Requirement' class='btn btn-primary btn-sm btn-round btn-line'>$row->PMBSyaratID</a>
																</p></td>
																<td><p align='center'>$row->Urutan</p></td>
																<td>$row->Nama</td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>