			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Information for Applicant</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Information for Applicant
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of information for applicant.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("info/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ID</p></th>
													<th><p valign="middle" align="center">TITILE</p></th>
													<th><p valign="middle" align="center">CREATED</p></th>
													<th><p valign="middle" align="center">DATE CREATED</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->na == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->na == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."info/ptl_delete/$row->id_info"; ?>" title="Delete this Info" onclick="return confirm('Are you sure to remove the INFORMATION FOR APPLICANT with CODE <?php echo $row->id_info.' - '.$row->judul; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																echo "<a href='".base_url()."info/ptl_edit/$row->id_info' title='Edit this Info' class='btn btn-primary btn-sm btn-round btn-line'>$no</a>
																</p></td>
																<td>$row->judul</td>
																<td>$row->login_buat</td>
																<td>$row->tanggal_buat</td>
															</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>