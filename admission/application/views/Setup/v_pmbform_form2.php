			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Form</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmbform"); ?>">Admission Form</a>
									>
									<a href="<?php echo site_url("pmbform/ptl_form"); ?>">Admission Form</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmbform/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Program ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="ProgramID" value="SC" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Prodi ID</label>
												<div class="col-lg-8">
													<input type="text" name="KursusSingkatID" placeholder="" class="form-control" required/>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Program</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" placeholder="" class="form-control" required/>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Prodi</label>
												<div class="col-lg-8">
													<input readonly type="text" value="Short Course" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Cost of Form</label>
												<div class="col-lg-8">
													<input type="number" name="harga_formulir" style="text-align:right;" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Total Cost</label>
												<div class="col-lg-8">
													<input type="number" name="biaya" style="text-align:right;" placeholder="" class="form-control" required/>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
										<br/>
										<br/>
										<br/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>