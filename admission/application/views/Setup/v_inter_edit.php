			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Standard Interview Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("inter"); ?>">Standard Interview</a>
									>
									<a href="<?php echo site_url("inter/ptl_edit/$WawancaraID"); ?>">Standard Interview Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('inter/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Standard ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="WawancaraID" value="<?php echo $WawancaraID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Sequence</label>
												<div class="col-lg-8">
													<input type="text" name="urutan" value="<?php echo $urutan; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Question</label>
												<div class="col-lg-8">
													<input type="text" name="pertanyaan" value="<?php echo $pertanyaan; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Standard Answer</label>
												<div class="col-lg-8">
													<textarea name="standard" placeholder="" class="form-control" ><?php echo $standard; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Active?</label>
												<div class="col-lg-8">
													<input type="radio" name="NA" value="Y" <?php if($NA == "Y"){echo "checked";}?> > No&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="NA" value="N" <?php if($NA == "N"){echo "checked";}?> > Yes&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>