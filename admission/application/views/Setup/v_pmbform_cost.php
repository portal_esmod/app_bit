			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Add Cost</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmbform"); ?>">Admission Form</a>
									>
									<a href="<?php echo site_url("pmbform/ptl_edit2/$ProdiID"); ?>">Admission Form Edit</a>
									>
									<a href="<?php echo site_url("pmbform/ptl_addcost/$ProdiID"); ?>">Add Cost</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmbform/ptl_addcost_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Prodi ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="ProdiID" value="<?php echo $ProdiID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="nama" placeholder="" class="form-control" required />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Note</label>
												<div class="col-lg-8">
													<textarea name="catatan" placeholder="" class="form-control" required ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Cost of Form</label>
												<div class="col-lg-8">
													<input type="number" name="biaya" style="text-align:right;" placeholder="" class="form-control" required />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>