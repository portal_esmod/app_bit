			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Form Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmbform"); ?>">Admission Form</a>
									>
									<a href="<?php echo site_url("pmbform/ptl_edit/$ProdiID"); ?>">Admission Form Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmbform/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Program ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="ProgramID" value="<?php echo $ProgramID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Prodi ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="ProdiID" value="<?php echo $ProdiID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Program</label>
												<div class="col-lg-8">
													<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Prodi</label>
												<div class="col-lg-8">
													<input readonly type="text" name="Jenjang" value="<?php echo $Jenjang; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Cost of Form</label>
												<div class="col-lg-8">
													<input type="number" name="harga_formulir" value="<?php echo $harga_formulir; ?>" style="text-align:right;" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Total Cost</label>
												<div class="col-lg-8">
													<input type="number" name="biaya_awal" value="<?php echo $biaya_awal; ?>" style="text-align:right;" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Down Payment</label>
												<div class="col-lg-8">
													<input type="number" name="minimal_pembayaran" value="<?php echo $minimal_pembayaran; ?>" style="text-align:right;" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
										<a href="<?php echo site_url("pmbform/ptl_addcost/$ProdiID"); ?>" class="btn btn-warning">ADD</a>
										<br/>
										<br/>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">NOTE</p></th>
													<th><p valign="middle" align="center">COST</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													$total = 0;
													if($rowrecord)
													{
														foreach($rowrecord as $row)
														{
															if($row->na == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->na == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																	<a href="<?php echo site_url("pmbform/ptl_addcost_delete/$row->ProdiID-$row->id_prodi_biaya"); ?>" onclick="return confirm('Are you sure to remove VARIABLE COST 1ST SEMESTER for CODE <?php echo $row->ProdiID." - ".$row->id_prodi_biaya." - ".$row->nama; ?>?')" title='Delete this VARIABLE COST' class='btn btn-danger btn-sm btn-round btn-line'><?php echo $no; ?></a>
												<?php
															echo "</p></td>
																<td><a href='".site_url("pmbform/ptl_addcost_edit/$row->id_prodi_biaya")."' title='Edit this VARIABLE COST' class='btn btn-primary btn-sm btn-round btn-line'>$row->nama</a></td>
																<td>$row->catatan</td>
																<td><p align='right'>".formatRupiah($row->biaya)."</p></td>
															</tr>";
															if($row->na == "N")
															{
																$total = $total + $row->biaya;
															}
															$no++;
														}
													}
													if($total == $biaya_awal)
													{
														$tot = formatRupiah($total);
													}
													if($total > $biaya_awal)
													{
														$tot = formatRupiah($total)."<br/><h4>NOT BALANCE, OVER ".formatRupiah($total - $biaya_awal)."</h4>";
													}
													if($total < $biaya_awal)
													{
														$tot = formatRupiah($total)."<br/><h4>NOT BALANCE, LESS ".formatRupiah($biaya_awal - $total)."</h4>";
													}
												?>
											</tbody>
										</table>
										<center><h2>TOTAL: <?php echo $tot; ?></h2></center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>