			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Marketing Team</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Marketing Team
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of marketing team.
									</div>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">ACCOUNT ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">BIRTH</p></th>
													<th><p valign="middle" align="center">HANDPHONE</p></th>
													<th><p valign="middle" align="center">EMAIL</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->na == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->na == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>$row->id_akun</p></td>
																<td>$row->nama</td>
																<td><p align='center'>$row->tempat, $row->tanggal_lahir</p></td>
																<td><p align='center'>$row->handphone, $row->handphone2</p></td>
																<td><p align='center'>$row->email</p></td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>