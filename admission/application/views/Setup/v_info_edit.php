			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Information for Applicant Add</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("info"); ?>">Information for Applicant</a>
									>
									<a href="<?php echo site_url("info/ptl_edit/$id_info"); ?>">Information for Applicant Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('info/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Titile</label>
												<div class="col-lg-8">
													<input type="text" name="judul" value="<?php echo $judul; ?>" placeholder="" class="form-control" required/>
													<input type="hidden" name="id_info" value="<?php echo $id_info; ?>"/>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Content</label>
												<div class="col-lg-8">
													<script src="<?php echo base_url(); ?>assets/cekeditor/ckeditor.js"></script>
													<textarea class="ckeditor" cols="80000" id="editor1" name="isi" rows="10000" class="form-control" required><?php echo $isi; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Not Active?</label>
												<div class="col-lg-8">
													<input type="checkbox" name="na" value="Y" <?php if($na == "Y"){ echo "checked"; } ?> placeholder="" class="form-control"/>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>