			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Form</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Admission Form
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of admission form.
									</div>
									<div class="table-responsive">
										<center>
											<a href="<?php echo site_url("pmbform/ptl_form"); ?>" class="btn btn-success">Add New</a>
										</center>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">PRODI ID</p></th>
													<th><p valign="middle" align="center">PROGRAM</p></th>
													<th><p valign="middle" align="center">PRODI</p></th>
													<th><p valign="middle" align="center">COST OF FORM</p></th>
													<th><p valign="middle" align="center">INITIAL COSTS</p></th>
													<th><p valign="middle" align="center">MINIMUM PAYMENT</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>
																	<a href='".base_url()."pmbform/ptl_edit/$row->ProdiID' title='Edit this PMB Form' class='btn btn-primary btn-sm btn-round btn-line'>$row->ProdiID</a>
																</p></td>
																<td>$row->Nama</td>
																<td><p align='center'>$row->Jenjang</p></td>
																<td><p align='right'>".formatRupiah($row->harga_formulir)."</p></td>
																<td><p align='right'>".formatRupiah($row->biaya_awal)."</p></td>
																<td><p align='right'>".formatRupiah($row->minimal_pembayaran)."</p></td>
															</tr>";
														}
													}
													if($rowrecord2)
													{
														$no = 1;
														foreach($rowrecord2 as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>
																		<a href='".base_url()."pmbform/ptl_edit2/$row->KursusSingkatID' title='Edit this PMB Form' class='btn btn-primary btn-sm btn-round btn-line'>$row->KursusSingkatID</a>
																	</p>
																</td>
																<td>$row->Nama</td>
																<td><p align='center'>Short Course</p></td>
																<td><p align='right'>".formatRupiah($row->harga_formulir)."</p></td>
																<td><p align='right'>".formatRupiah($row->Biaya)."</p></td>
																<td><p align='right'>".formatRupiah($row->Biaya)."</p></td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>