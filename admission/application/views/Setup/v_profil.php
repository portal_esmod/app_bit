			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Profile</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Profile
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('login/ptl_profil_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Account ID</label>
												<div class="col-lg-8">
													<input readonly type="text" name="id_akun" value="<?php echo $id_akun; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Photo</label>
												<div class="col-lg-8">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
															<?php
																if($foto == "-_-")
																{
															?>
																	<img src="<?php echo base_url(); ?>../hris/ptl_storage/foto_karyawan/user.jpg" alt="" />
															<?php
																}
																else
																{
															?>
																	<img src="http://localhost/esmod/hris/ptl_storage/foto_karyawan/<?php echo $foto; ?>" alt="" />
															<?php
																}
															?>
														</div>
														<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
														<div>
															<span class="btn btn-file btn-primary">
																<span class="fileupload-new">Select image</span>
																<span class="fileupload-exists">Change</span>
																<input name="userfile" type="file" class="btn btn-warning" accept="image/*"/>
															</span>
															<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Place of Birth</label>
												<div class="col-lg-8">
													<input type="text" name="tempat" value="<?php echo $tempat; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Date of Birth</label>
												<div class="col-lg-8">
													<input type="text" name="tanggal_lahir" value="<?php echo $tanggal_lahir; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Email</label>
												<div class="col-lg-8">
													<input type="text" name="email" value="<?php echo $email; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Handphone</label>
												<div class="col-lg-8">
													<input type="text" name="telepon" value="<?php echo $telepon; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Access</label>
												<div class="col-lg-8">
													<input readonly type="text" name="akses" value="<?php echo $akses; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>