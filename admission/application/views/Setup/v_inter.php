			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Standard Interview</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Standard Interview
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of standard interview.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("inter/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">#</p></th>
													<th><p valign="middle" align="center">QUESTION</p></th>
													<th><p valign="middle" align="center">SEQUENCE</p></th>
													<th><p valign="middle" align="center">STANDARD</p></th>
													<th><p valign="middle" align="center">POSTING DATE</p></th>
													<th><p valign="middle" align="center">UPDATE DATE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															echo "<td><p align='center'>";
												?>
																<a href="<?php echo base_url()."inter/ptl_delete/$row->WawancaraID"; ?>" title="Delete this Standard Interview" onclick="return confirm('Are you sure to remove STANDARD INTERVIEW with CODE <?php echo $row->WawancaraID.' - '.$row->pertanyaan; ?>?')" class="btn btn-danger btn-sm btn-round btn-line"><?php echo $no; ?></a>
												<?php
																echo "</p>
																</td>
																<td><p align='center'><a href='".base_url()."inter/ptl_edit/$row->WawancaraID' title='Edit this Standard Interview' class='btn btn-primary btn-sm btn-round btn-line'>$row->pertanyaan</a></p></td>
																<td><p align='center'>$row->urutan</p></td>
																<td><p align='center'>$row->standard</p></td>
																<td><p align='center'>$row->tanggal_buat</p></td>
																<td><p align='center'>$row->tanggal_edit</p></td>
															</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>