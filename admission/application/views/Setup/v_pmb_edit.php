			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Period Edit</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmb"); ?>">Admission Period</a>
									>
									<a href="<?php echo site_url("pmb/ptl_edit/$PMBPeriodID"); ?>">Admission Period Edit</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmb/ptl_update',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">PMB Period</label>
												<div class="col-lg-8">
													<input readonly type="text" name="PMBPeriodID" value="<?php echo $PMBPeriodID; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Year</label>
												<div class="col-lg-8">
													<input readonly type="text" name="Tahun" value="<?php echo $Tahun; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">APL Format</label>
												<div class="col-lg-8">
													<input readonly type="text" name="FormatNoAplikan" value="<?php echo $FormatNoAplikan; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">APL Digit</label>
												<div class="col-lg-8">
													<input readonly type="text" name="DigitNoAplikan" value="<?php echo $DigitNoAplikan; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">PMB Format</label>
												<div class="col-lg-8">
													<input readonly type="text" name="FormatNoPMB" value="<?php echo $FormatNoPMB; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">PMB Digit</label>
												<div class="col-lg-8">
													<input readonly type="text" name="DigitNoPMB" value="<?php echo $DigitNoPMB; ?>" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-4">Exam 1</label>
												<div class="col-lg-8">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<a href="<?php echo base_url(); ?>pmb/ptl_download/<?php echo $soal1; ?>" title="<?php echo $soal1; ?>" class="btn btn-success btn-sm btn-round btn-line" ><?php $file_order = substr($soal1,21); echo $file_order; ?></a>
														<br/>
														<span class="btn btn-file btn-default">
															<span class="fileupload-new">Select file</span>
															<span class="fileupload-exists">Change</span>
															<input type="hidden" name="file_order" value="<?php echo $soal1; ?>" />
															<input name="userfile" type="file" class="default" accept="*" />
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Exam 2</label>
												<div class="col-lg-8">
													<script src="<?php echo base_url(); ?>assets/cekeditor/ckeditor.js"></script>
													<textarea class="ckeditor" cols="80000" id="editor1" name="soal2" rows="10000" class="form-control" ><?php echo $soal2; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Interview Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglInterview" value="<?php echo $TglInterview; ?>" data-date-format="yyyy-mm-dd" id="dp2" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Interviewer</label>
												<div class="col-lg-8">
													<input type="text" name="Interviewer" value="<?php echo $Interviewer; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Activation Matter</label>
												<div class="col-lg-8">
													<input type="radio" name="soal" value="0" <?php if($soal == "0"){echo "checked";} ?>> EXAM 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" name="soal" value="1" <?php if($soal == "1"){echo "checked";} ?>> EXAM 2
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Start - End Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglMulaiTglSelesai" value="<?php echo $TglMulai." - ".$TglSelesai; ?>" id="reservation" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">ONLINE</label>
												<div class="col-lg-8">
													<input type="checkbox" name="ol" value="N" <?php if($ol=="N"){echo"checked";}?> > Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">ENABLE</label>
												<div class="col-lg-8">
													<input type="checkbox" name="NA" value="N" <?php if($NA=="N"){echo"checked";}?> > Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>