			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Period Form</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a href="<?php echo site_url("pmb"); ?>">Admission Period</a>
									>
									<a href="<?php echo site_url("pmb/ptl_form"); ?>">Admission Period Form</a>
								</div>
								<div class="panel-body">
									<div id="div-1" class="accordion-body collapse in body">
										<?php echo form_open_multipart('pmb/ptl_insert',array('class' => 'form-horizontal')); ?>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Name</label>
												<div class="col-lg-8">
													<input type="text" name="Nama" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Year</label>
												<div class="col-lg-8">
													<input type="number" name="Tahun" placeholder="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-4">Exam 1</label>
												<div class="col-lg-8">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<span class="btn btn-file btn-default">
															<span class="fileupload-new">Select file</span>
															<span class="fileupload-exists">Change</span>
															<input name="userfile" type="file" class="default" accept="*" />
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Exam 2</label>
												<div class="col-lg-8">
													<script src="<?php echo base_url(); ?>assets/cekeditor/ckeditor.js"></script>
													<textarea class="ckeditor" cols="80000" id="editor1" name="soal2" rows="10000" class="form-control" ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Interview Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglInterview" data-date-format="yyyy-mm-dd" id="dp2" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Interviewer</label>
												<div class="col-lg-8">
													<input type="text" name="Interviewer" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">Start - End Date</label>
												<div class="col-lg-8">
													<input type="text" name="TglMulaiTglSelesai" id="reservation" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">ONLINE</label>
												<div class="col-lg-8">
													<input type="checkbox" name="ol" value="N"> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4">ENABLED</label>
												<div class="col-lg-8">
													<input type="checkbox" name="NA" value="N"> Yes
												</div>
											</div>
											<div class="form-group">
												<label for="text1" class="control-label col-lg-4"></label>
												<div class="col-lg-8">
													<button type="reset" class="btn btn-danger">RESET</button>
													<input type="submit" value="SAVE" id="my_button" class="btn btn-primary">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>