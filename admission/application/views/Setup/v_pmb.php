			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Admission Period</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Admission Period
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of admission period.
									</div>
									<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th><p valign="middle" align="center"><a href="<?php echo site_url("pmb/ptl_form"); ?>" title="Add New Data" class="btn btn-primary">Add New</a></p></th>
											</tr>
										</thead>
									</table>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">PMB ID</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">YEAR</p></th>
													<th><p valign="middle" align="center">APL FORMAT</p></th>
													<th><p valign="middle" align="center">PMB FORMAT</p></th>
													<th><p valign="middle" align="center">EXAM 1</p><hr><p valign="middle" align="center">EXAM 2</p></th>
													<th><p valign="middle" align="center">START DATE</p><hr><p valign="middle" align="center">END DATE</p></th>
													<th><p valign="middle" align="center">ONLINE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "N")
															{
																echo "<tr class='success'>";
															}
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															echo "<td><p align='center'>";
												?>
																		<a href="<?php echo base_url()."pmb/ptl_delete/$row->PMBPeriodID"; ?>" title="Delete this Admission Period" onclick="return confirm('Are you sure to remove the ADMISSION PERIOD with CODE <?php echo $row->PMBPeriodID.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line">X</a>
												<?php
																	echo "<b><a href='".base_url()."pmb/ptl_edit/$row->PMBPeriodID' title='Edit this Admission Period' class='btn btn-primary btn-sm btn-round btn-line'>$row->PMBPeriodID</a></b>
																	</p>
																</td>
																<td>$row->Nama</td>
																<td><p align='center'>$row->Tahun</p></td>
																<td><p align='center'>$row->FormatNoAplikan, $row->DigitNoAplikan</p></td>
																<td><p align='center'>$row->FormatNoPMB, $row->DigitNoPMB</p></td>";
												?>
																<td><p align='center'><a href="<?php echo base_url(); ?>pmb/ptl_download/<?php echo $row->soal1; ?>" title="<?php echo $row->soal1; ?>" class="btn btn-danger btn-sm btn-round btn-line" >DOWNLOAD <?php $file_order = substr($row->soal1,-6); echo "..".$file_order; ?></a></p>
																	<hr>
																	<p align='center'><a href="<?php echo base_url(); ?>pmb/ptl_download/<?php echo $row->soal2; ?>" title="<?php echo $row->soal2; ?>" class="btn btn-danger btn-sm btn-round btn-line" >DOWNLOAD <?php $file_order = substr($row->soal2,-6); echo "..".$file_order; ?></a></p>
																</td>
												<?php
															echo "<td><p align='center'>$row->TglMulai</p><hr><p align='center'>$row->TglSelesai</p></td>
																<td><p align='center'>$row->ol</p></td>
															</tr>";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								   
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>