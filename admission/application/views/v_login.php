<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>Esmod Jakarta - Admission</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/css/login.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/magic/magic.css" />
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body>
		<div class="container">
			<div class="text-center">
				<img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" id="logoimg" alt=" Logo" />
			</div>
			<div class="tab-content">
				<div id="login" class="tab-pane active">
					<form action="<?php echo site_url("login/ptl_do_login"); ?>" class="form-signin" method="POST" >
						<p class="text-muted text-center btn-block btn btn-primary btn-rect">
							Enter your access
						</p>
						<input type="text" name="username" placeholder="Username" class="form-control" autofocus required>
						<input type="password" name="password" placeholder="Password" class="form-control" required>
						<?php
							if($_SERVER["HTTP_HOST"] != "localhost")
							{
						?>
								<div class="form-group">
									<input type="checkbox" name="remember" value="7" id="myCheck" onclick="myFunction()">
									Remember me for 7 days.
								</div>
								<div id="text" style="display:none" class="g-recaptcha" data-sitekey="6LeXKk4UAAAAABzBI25f2lqIDGLmfqA1u3HJGmKx"></div>
								<br/>
						<?php
							}
						?>
						<button class="btn text-muted text-center btn-success" type="submit">Sign in</button>
					</form>
				</div>
				<div id="forgot" class="tab-pane">
					<form action="<?php echo site_url("login/ptl_lupa_password"); ?>" class="form-signin" method="POST">
						<p class="text-muted text-center btn-block btn btn-primary btn-rect">Enter your e-mail</p>
						<input type="email" name="email" class="form-control" placeholder="Your E-mail" autofocus required>
						<br/>
						<?php
							if($_SERVER["HTTP_HOST"] != "localhost")
							{
						?>
								<div class="g-recaptcha" data-sitekey="6LeXKk4UAAAAABzBI25f2lqIDGLmfqA1u3HJGmKx"></div>
						<?php
							}
						?>
						<br/>
						<button class="btn text-muted text-center btn-success" type="submit">Recover Password</button>
					</form>
				</div>
			</div>
			<div class="text-center">
				<ul class="list-inline">
					<li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
					<li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
				</ul>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-2.0.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/login.js"></script>
		<script>
			function myFunction()
			{
				var checkBox = document.getElementById("myCheck");
				var text = document.getElementById("text");
				if (checkBox.checked == true)
				{
					text.style.display = "block";
				}
				else
				{
					text.style.display = "none";
				}
			}
		</script>
	</body>
</html>