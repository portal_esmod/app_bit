			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Payments</h1>
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Payments
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of payments.
									</div>
									<div class="table-responsive">
										<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_periode"); ?>" method="POST">
															<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- PERIOD --</option>
																<?php
																	if($periode)
																	{
																		$cekperiode = $this->session->userdata('apl_filter_periode');
																		foreach($periode as $per)
																		{
																			echo "<option value='".$per->PMBPeriodID."_PYM'";
																			if($cekperiode == $per->PMBPeriodID)
																			{
																				echo "selected";
																			}
																			echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_marketing"); ?>" method="POST">
															<select name="cekmarketing" title="Filter by Marketing Staff" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- MARKETING --</option>
																<?php
																	if($marketing)
																	{
																		$cekmarketing = $this->session->userdata('apl_filter_marketing');
																		echo "<option value='OLREG_PYM'";
																		if($cekmarketing == "OLREG")
																		{
																			echo "selected";
																		}
																		echo ">OLREG - ONLINE REGISTRATION</option>";
																		foreach($marketing as $mar)
																		{
																			$word = explode(" ",$mar->nama);
																			echo "<option value='".$word[0]."_PYM'";
																			if($cekmarketing == $word[0])
																			{
																				echo "selected";
																			}
																			echo ">".$word[0]." - $mar->nama</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_type"); ?>" method="POST">
															<select name="cektype" title="Filter by Type" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- TYPE --</option>
																<?php
																	$cektype = $this->session->userdata('apl_filter_type');
																	echo "<option value='HOT_PYM'"; if($cektype == "HOT") { echo "selected"; } echo ">HOT - Potensial</option>";
																	echo "<option value='USUAL_PYM'"; if($cektype == "USUAL") { echo "selected"; } echo ">USUAL - Biasa</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th></th>
												</tr>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_status"); ?>" method="POST">
															<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- STATUS --</option>
																<?php
																	$cekstatus = $this->session->userdata('apl_filter_status');
																	echo "<option value='Y_PYM'"; if($cekstatus == "Y") { echo "selected"; } echo ">STEP DOWN</option>";
																	echo "<option value='N_PYM'"; if($cekstatus == "N") { echo "selected"; } echo ">NOT STEP DOWN</option>";
																	echo "<option value='P_PYM'"; if($cekstatus == "P") { echo "selected"; } echo ">POSTPONE</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_program"); ?>" method="POST">
															<select name="cekpogram" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- PROGRAM --</option>
																<?php
																	$cekpogram = $this->session->userdata('apl_filter_program');
																	echo "<option value='INT_PYM'"; if($cekpogram == "INT") { echo "selected"; } echo ">INT - INTENSIVE</option>";
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='".$d1->ProdiID."_PYM'";
																			if($cekpogram == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																	echo "<option value='REG_PYM'"; if($cekpogram == "REG") { echo "selected"; } echo ">REG - REGULAR</option>";
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='".$d3->ProdiID."_PYM'";
																			if($cekpogram == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																	echo "<option value='SC_PYM'"; if($cekpogram == "SC") { echo "selected"; } echo ">SC - SHORT COURSE</option>";
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='".$sc->KursusSingkatID."_PYM'";
																			if($cekpogram == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_tahun"); ?>" method="POST">
															<select name="cektahun" title="Filter by Year" class="form-control round-form" onchange="this.form.submit()">
																<option value='_PYM'>-- YEAR --</option>
																<?php
																	if($tahun)
																	{
																		$cektahun = $this->session->userdata('apl_filter_tahun');
																		foreach($tahun as $thn)
																		{
																			echo "<option value='".$thn->tahun."_PYM'";
																			if($cektahun == $thn->tahun)
																			{
																				echo "selected";
																			}
																			echo ">$thn->tahun</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center"><a href="<?php echo site_url("admission/ptl_pdf_public/PYM"); ?>" title="Print This Result" class="btn btn-primary">Print This Result</a></p></th>
												</tr>
											</thead>
										</table>
									</div>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">NO</p></th>
													<th><p valign="middle" align="center">PMB ID</p></th>
													<th><p valign="middle" align="center">STAGE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">RECEIPT</p></th>
													<th><p valign="middle" align="center">PAY</p></th>
													<th><p valign="middle" align="center">PROGRAM</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													$total = 0;
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->SudahBayar == "Y")
															{
																echo "<tr class='success'>";
															}
															if($row->SudahBayar == "P")
															{
																echo "<tr class='danger'>";
															}
															if($row->SudahBayar == "N")
															{
																echo "<tr class='warning'>";
															}
															echo "<td>";
																?>
																		<a href="<?php echo base_url()."applicant/ptl_payment_delete/$row->AplikanID"; ?>" title="Delete this Prospect" onclick="return confirm('Are you sure to remove PAYMENT FORM with APPLICANT ID <?php echo $row->PMBID.' - '.$row->Nama; ?>?')" class="btn btn-danger btn-sm btn-round btn-line"><?php echo $no; ?></a>
																<?php
															echo "</td>
																<td><p align='center'>
																	<a href='".base_url()."prospects/ptl_edit/$row->AplikanID' target='_blank' title='Edit this Prospect' class='btn btn-primary btn-sm btn-round btn-line'>$row->PMBID</a>
																	<a href='".base_url()."applicant/ptl_payment_ok/$row->AplikanID' title='Approve this Prospect' class='btn btn-success btn-sm btn-round btn-line'>OK</a>
																</p>
																</td>
																<td>$row->StatusAplikanID</td>
																<td><a href='".site_url("notif/ptl_edit/$row->AplikanID")."' title='Chat with this Prospect' target='_blank'>$row->Nama</a></td>
																<td>";
												?>
																<p align='center'><a href="<?php echo base_url(); ?>applicant/ptl_payment_download/<?php echo $row->BuktiSetoran; ?>" title="<?php echo $row->BuktiSetoran; ?>" class="btn btn-danger btn-sm btn-round btn-line" ><?php $file_order = substr($row->BuktiSetoran,0,35); echo $file_order; ?></a></p>
												<?php
																echo "</td>
																	<td><p align='right'>".formatRupiah($row->Harga)."</p></td>
																	<td><p align='center'>$row->ProgramID, $row->ProdiID</p></td>
																</tr>";
																$total = $total + $row->Harga;
															$no++;
														}
													}
												?>
											</tbody>
										</table>
										<center><h3><b>TOTAL <?php echo formatRupiah($total); ?></b></h3></center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>