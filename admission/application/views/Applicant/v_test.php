			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Test</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Test
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of test.
									</div>
									<div class="table-responsive">
										<table style="background:#CCCCCC" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_periode"); ?>" method="POST">
															<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- PERIOD --</option>
																<?php
																	if($periode)
																	{
																		$cekperiode = $this->session->userdata('apl_filter_periode');
																		foreach($periode as $per)
																		{
																			echo "<option value='".$per->PMBPeriodID."_OLT'";
																			if($cekperiode == $per->PMBPeriodID)
																			{
																				echo "selected";
																			}
																			echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_marketing"); ?>" method="POST">
															<select name="cekmarketing" title="Filter by Marketing Staff" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- MARKETING --</option>
																<?php
																	if($marketing)
																	{
																		$cekmarketing = $this->session->userdata('apl_filter_marketing');
																		echo "<option value='OLREG_OLT'";
																		if($cekmarketing == "OLREG")
																		{
																			echo "selected";
																		}
																		echo ">OLREG - ONLINE REGISTRATION</option>";
																		foreach($marketing as $mar)
																		{
																			$word = explode(" ",$mar->nama);
																			echo "<option value='".$word[0]."_OLT'";
																			if($cekmarketing == $word[0])
																			{
																				echo "selected";
																			}
																			echo ">".$word[0]." - $mar->nama</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_type"); ?>" method="POST">
															<select name="cektype" title="Filter by Type" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- TYPE --</option>
																<?php
																	$cektype = $this->session->userdata('apl_filter_type');
																	echo "<option value='HOT_OLT'"; if($cektype == "HOT") { echo "selected"; } echo ">HOT - Potensial</option>";
																	echo "<option value='USUAL_OLT'"; if($cektype == "USUAL") { echo "selected"; } echo ">USUAL - Biasa</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th></th>
												</tr>
												<tr>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_status"); ?>" method="POST">
															<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- STATUS --</option>
																<?php
																	$cekstatus = $this->session->userdata('apl_filter_status');
																	echo "<option value='Y_OLT'"; if($cekstatus == "Y") { echo "selected"; } echo ">STEP DOWN</option>";
																	echo "<option value='N_OLT'"; if($cekstatus == "N") { echo "selected"; } echo ">NOT STEP DOWN</option>";
																	echo "<option value='P_OLT'"; if($cekstatus == "P") { echo "selected"; } echo ">POSTPONE</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_program"); ?>" method="POST">
															<select name="cekpogram" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- PROGRAM --</option>
																<?php
																	$cekpogram = $this->session->userdata('apl_filter_program');
																	echo "<option value='INT_OLT'"; if($cekpogram == "INT") { echo "selected"; } echo ">INT - INTENSIVE</option>";
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='".$d1->ProdiID."_OLT'";
																			if($cekpogram == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																	echo "<option value='REG_OLT'"; if($cekpogram == "REG") { echo "selected"; } echo ">REG - REGULAR</option>";
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='".$d3->ProdiID."_OLT'";
																			if($cekpogram == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																	echo "<option value='SC_OLT'"; if($cekpogram == "SC") { echo "selected"; } echo ">SC - SHORT COURSE</option>";
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='".$sc->KursusSingkatID."_OLT'";
																			if($cekpogram == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center">
														<form action="<?php echo site_url("applicant/ptl_filter_apl_tahun"); ?>" method="POST">
															<select name="cektahun" title="Filter by Year" class="form-control round-form" onchange="this.form.submit()">
																<option value='_OLT'>-- YEAR --</option>
																<?php
																	if($tahun)
																	{
																		$cektahun = $this->session->userdata('apl_filter_tahun');
																		foreach($tahun as $thn)
																		{
																			echo "<option value='".$thn->tahun."_OLT'";
																			if($cektahun == $thn->tahun)
																			{
																				echo "selected";
																			}
																			echo ">$thn->tahun</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</p></th>
													<th><p valign="middle" align="center"><a href="<?php echo site_url("admission/ptl_pdf_public/OLT"); ?>" title="Print This Result" class="btn btn-primary">Print This Result</a></p></th>
												</tr>
											</thead>
										</table>
									</div>
									<br/>
									<br/>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th><p valign="middle" align="center">NO</p></th>
													<th><p valign="middle" align="center">PMB ID</p></th>
													<th><p valign="middle" align="center">STAGE</p></th>
													<th><p valign="middle" align="center">NAME</p></th>
													<th><p valign="middle" align="center">ANSWER TEST</p></th>
													<th><p valign="middle" align="center">SCORE</p></th>
													<th><p valign="middle" align="center">GRADE</p></th>
													<th><p valign="middle" align="center">TEST DATE</p></th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->LulusUjian == "N")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																if($row->LulusUjian == "I")
																{
																	echo "<tr class='warning'>";
																}
																else
																{
																	echo "<tr class='success'>";
																}
															}
																echo "<td>";
																?>
																		<a href="<?php echo site_url("applicant/ptl_test_delete/$row->AplikanID"); ?>" onclick="return confirm('Are you sure to remove TEST with APPLICANT ID <?php echo $row->PMBID." - ".$row->Nama; ?>?')" title='Delete this Prospect Test' class='btn btn-danger btn-sm btn-round btn-line'><?php echo $no; ?></a>
																<?php
																echo "</td>
																	<td><p align='center'>
																			<b><a href='".base_url()."prospects/ptl_edit/$row->AplikanID' target='_blank' title='Edit this Prospect' class='btn btn-primary btn-sm btn-round btn-line'>$row->PMBID</a></b>
																		</p>
																	</td>
																	<td>$row->StatusAplikanID</td>
																	<td><a href='".site_url("notif/ptl_edit/$row->AplikanID")."' title='Chat with this Prospect' target='_blank'>$row->Nama</a></td>
																	<td>
																	<p align='center'>";
																$h = "-7";
																$hm = $h * 60;
																$ms = $hm * 60;
																if(($row->akhir_ujian <= gmdate("m/d/Y H:i", time()-($ms))) AND ($row->ujian == ""))
																{
																	echo "<a href='".site_url("applicant/ptl_test_time/$row->AplikanID")."' target='_blank' class='btn btn-danger btn-sm btn-round btn-line' >EXPIRED, GIVE MORE TIME?</a>";
																}
																else
																{
																	if($row->ujian == '1')
																	{
																		echo "<a href='".site_url("applicant/ptl_test_download/$row->file_ujian1")."' title='$row->file_ujian1' class='btn btn-danger btn-sm btn-round btn-line' >".substr($row->file_ujian1,0,35)."</a>";
																	}
																	else
																	{
																		echo "<a href='".site_url("applicant/ptl_test_view/$row->AplikanID")."' target='_blank' class='btn btn-danger btn-sm btn-round btn-line' >VIEW RESULT</a>";
																	}
																}
																echo "</p>
																	<td><p align='center'>";
																		if($row->LulusUjian == "N")
																		{
																			echo "<form method='POST' action='".site_url("applicant/ptl_test_score")."'>
																					<input type='hidden' name='AplikanID' value='$row->AplikanID' />
																					<input type='number' name='NilaiUjian' min='0' max='20' title='Range (0 - 20)' value='$row->NilaiUjian' style='width:40px;text-align:right;' />
																					<input type='submit' value='SET' class='btn btn-success btn-sm btn-round btn-line' />
																				</form>";
																		}
																		else
																		{
																			echo $row->NilaiUjian;
																		}
																	echo "</p>
																	</td>
																	<td><p align='center'>$row->GradeNilai</p></td>
																	<td><p align='center'>$row->TanggalUjian</p></td>
																</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>