			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Instructions and Notes</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<h4><font color="red">Students want to change program (Prodi):</font></h4>
								<table class="table table-striped table-bordered table-hover">
									<tr><td align="left">1. Please inform to Finance that your prospect want to change program.</td></tr>
									<tr><td align="left">2. Remove or Delete your prospects in current program.</td></tr>
									<tr><td align="left">3. Add Data again.</td></tr>
									<tr><td align="left">3. Payment scheme will process by Finance.</td></tr>
									<tr><td align="left">4. DONE.</td></tr>
								</table>
								<h4><font color="red">Students want to register for specialization after graduation:</font></h4>
								<table class="table table-striped table-bordered table-hover">
									<tr><td align="left">1. Please inform to Finance that your prospects want to register for specialization.</td></tr>
									<tr><td align="left">2. Finance must be create new BIPOT for specialization.</td></tr>
									<tr><td align="left">3. After Finance create BIPOT, then SA must be add data to Admission Portal (this application).</td></tr>
									<tr><td align="left">4. Payment scheme will process by Finance.</td></tr>
									<tr><td align="left">5. After Payment scheme processed by Finance, SA must be inform to Students Service for setup schedule for your prospects.</td></tr>
									<tr><td align="left">6. DONE.</td></tr>
								</table>
								<h4><font color="red">Students want to register next program (from INTENSIVE to REGULAR):</font></h4>
								<table class="table table-striped table-bordered table-hover">
									<tr><td align="left">1. Please inform to Finance that your prospects want to register from INTENSIVE to REGULAR.</td></tr>
									<tr><td align="left">2. Finance must be create new BIPOT for specialization.</td></tr>
									<tr><td align="left">3. After Finance create BIPOT, then SA must be add data to Admission Portal (this application).</td></tr>
									<tr><td align="left">4. Payment scheme will process by Finance.</td></tr>
									<tr><td align="left">5. After Payment scheme processed by Finance, SA must be inform to Students Service for setup schedule for your prospects.</td></tr>
									<tr><td align="left">6. DONE.</td></tr>
								</table>
								<br/>
							</div>
						</div>
					</div>
				</div>
			</div>