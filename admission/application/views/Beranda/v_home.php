			<div id="content">
				<div class="inner" style="min-height: 700px;">
					<div class="row">
						<div class="col-lg-12">
							<h1>Dashboard</h1>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-info">
								<i class="fa fa-folder-open"></i><b>&nbsp;Hello ! </b>Welcome Back <b><?php echo $_COOKIE["id_akun"]."_".$_COOKIE["nama"];?></b>. <?php echo $last_login.$last_logout.$total_hour; if($tanggal_pengingat != ""){ echo "<br/><font color='red'><b> System will remember you until $tanggal_pengingat.</b></font>"; } ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div style="text-align: center;">
								<a title="All prospects, not only register" class="quick-btn" href="<?php echo site_url("prospects"); ?>">
									<i class="icon-check icon-2x"></i>
									<span>Prospects</span>
									<span class="label label-danger"><?php echo count($rowrecord); ?></span>
								</a>
								<a title="All messages, not only for you" class="quick-btn" href="<?php echo site_url("notif"); ?>">
									<i class="icon-envelope icon-2x"></i>
									<span>Messages</span>
									<span class="label label-success"><?php echo count($rowrecord2); ?></span>
								</a>
								<a title="Only register" class="quick-btn" href="<?php echo site_url("applicant"); ?>">
									<i class="icon-signal icon-2x"></i>
									<span>Register</span>
									<span class="label label-warning"><?php echo count($rowrecord3); ?></span>
								</a>
								<?php
									$total = 0;
									foreach($rowrecord3 as $row)
									{
										$total = $total + $row->Harga;
									}
								?>
								<a title="All form payment" class="quick-btn" href="<?php echo site_url("applicant"); ?>">
									<i class="icon-external-link icon-2x"></i>
									<span>Value</span>
									<span class="label btn-metis-2"><?php echo formatRupiah($total); ?></span>
								</a>
								<a title="All accepted become new students" class="quick-btn" href="<?php echo site_url("applicant/ptl_semester"); ?>">
									<i class="icon-bolt icon-2x"></i>
									<span>Accepted</span>
									<span class="label btn-metis-4"><?php echo count($rowrecord4); ?></span>
								</a>
							</div>
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<h3><font color="red">If you encounter any problems related to this kind of thing, you can contact the person in charge:</font></h3>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<th>ITEMS</th>
										<th>PIC</th>
										<th>&nbsp;</th>
										<th>ITEMS</th>
										<th>PIC</th>
									</tr>
									<tr>
										<td align="left">Prospects</td>
										<td align="left">All School Ambassador</td>
										<td>&nbsp;</td>
										<td align="left">Applicant</td>
										<td align="left">All School Ambassador</td>
									</tr>
									<tr>
										<td align="left">Interview</td>
										<td align="left">All School Ambassador and Patrice</td>
										<td>&nbsp;</td>
										<td align="left">Postpone</td>
										<td align="left">All School Ambassador</td>
									</tr>
									<tr>
										<td align="left">Admission</td>
										<td align="left">All School Ambassador</td>
										<td>&nbsp;</td>
										<td align="left">Allocation Class</td>
										<td align="left">Patrice and Anisa Fajar</td>
									</tr>
									<tr>
										<td align="left">Event Activity</td>
										<td align="left">All School Ambassador</td>
										<td>&nbsp;</td>
										<td align="left">Schedule Today</td>
										<td align="left">All School Ambassador</td>
									</tr>
									<tr>
										<td align="left">Additional Data</td>
										<td align="left">Theresia and Imelda Chandra</td>
										<td>&nbsp;</td>
										<td align="left">Set Up Admission</td>
										<td align="left">Theresia and Imelda Chandra</td>
									</tr>
								</table>
								<center>
									<h4><font color="red">For system error like this </font></h4><?php echo $error; ?>
									<h4><font color="red">Please contact Lendra Permana as IT Developer.</font></h4>
									<h4><font color="red">If you have problem with your internet connection or Wi-Fi Register. Please contact Wishnu Prasasti as IT Network.</font></h4>
									<h3><font color="red"><b>We hope that you contact the right person.</b></font></h3>
								</center>
								<br/>
							</div>
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									Last Login
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Account ID</th>
													<th>Name</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowakses1)
													{
														$n = 1;
														foreach($rowakses1 as $row)
														{
															if(strpos($row->aplikasi,"ad") !== false)
															{
																if($n < 21)
																{
																	echo "<tr>
																			<td>$n</td>
																			<td>$row->id_akun</td>
																			<td title='$row->akses'>$row->nama</td>
																			<td>$row->last_login</td>
																		</tr>";
																	$n++;
																}
															}
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									Last Logout
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Account ID</th>
													<th>Name</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowakses2)
													{
														$n = 1;
														foreach($rowakses2 as $row)
														{
															if(strpos($row->aplikasi,"ad") !== false)
															{
																if($n < 21)
																{
																	echo "<tr>
																			<td>$n</td>
																			<td>$row->id_akun</td>
																			<td title='$row->akses'>$row->nama</td>
																			<td>$row->last_logout</td>
																		</tr>";
																	$n++;
																}
															}
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="right">
				<div class="well well-small">
					<ul class="list-unstyled">
						<li>Visitor &nbsp; : <span><?php echo count($visit); ?></span></li>
						<li>Users &nbsp; : <span><?php echo count($user); ?></span></li>
						<li>Registrations &nbsp; : <span><?php echo count($registration); ?></span></li>
					</ul>
				</div>
				<div class="well well-small">
					<a href="https://www.facebook.com/pages/Esmod-Jakarta/19882372881" target="_blank" class="btn btn-primary btn-block"><i class="icon-facebook"></i> Facebook</a>
					<a href="https://twitter.com/esmodjakarta" target="_blank" class="btn btn-info btn-block"><i class="icon-twitter"></i> Twitter </a>
					<a href="https://instagram.com/esmodjakarta/" target="_blank" class="btn btn-block"><i class="icon-instagram"></i> Instagram </a>
					<a href="https://www.youtube.com/channel/UCFK9326GANeMVlE2A45z45A?feature=results_main" target="_blank" class="btn btn-danger btn-block"><i class="icon-youtube"></i> Youtube </a>
				</div>
			</div>