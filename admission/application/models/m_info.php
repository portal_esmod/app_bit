<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_info extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('judul','ASC');
			$query = $this->db->get('al_info');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_info',$data);
			return;
		}
		
		function PTL_select($id_info)
		{
			$this->db->where('id_info',$id_info);
			$query = $this->db->get('al_info');
			return $query->row_array();
		}
		
		function PTL_update($id_info,$data)
		{
			$this->db->where('id_info',$id_info);
			$this->db->update('al_info',$data);
		}
	}
?>