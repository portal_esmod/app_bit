<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mix_menu extends CI_Model
	{
		function PTL_all_prospects()
		{
			$h = "713";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$q = $this->db->query("SELECT * FROM al_aplikan
				WHERE DATE_FORMAT(tanggal_buat , '%Y-%m-%d %H:%i:%s') >= '$waktu' AND NA = 'N'");
			return $q->result();
		}
		
		function PTL_all_payments()
		{
			$this->db->where('SudahBayar','P');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_test()
		{
			$this->db->where('NA','N');
			$this->db->where('LulusUjian','N');
			$this->db->where('akhir_ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_completeness()
		{
			$this->db->where('NA','N');
			$this->db->where('SyaratLengkap','P');
			$this->db->where('file_syarat !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_semester()
		{
			$this->db->where('NA','N');
			$this->db->where('diterima','P');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_interview()
		{
			$this->db->where('LulusUjian','N');
			$this->db->where('NA','N');
			$this->db->where('ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_postpone()
		{
			$this->db->where('postpone','Y');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_reguler()
		{
			$this->db->where('NA','P');
			$this->db->or_where('NA','N');
			$this->db->where('ProgramID','REG');
			$this->db->or_where('ProgramID','INT');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_sc()
		{
			$this->db->where('NA','P');
			$this->db->or_where('NA','N');
			$this->db->where('ProgramID','SC');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
	}
?>