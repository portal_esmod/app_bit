<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pmbreq extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('PMBSyaratID','ASC');
			$query = $this->db->get('al_pmbsyarat');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_pmbsyarat',$data);
			return;
		}
		
		function PTL_select($PMBSyaratID)
		{
			$this->db->where('PMBSyaratID',$PMBSyaratID);
			$query = $this->db->get('al_pmbsyarat');
			return $query->row_array();
		}
		
		function PTL_update($PMBSyaratID,$data)
		{
			$this->db->where('PMBSyaratID',$PMBSyaratID);
			$this->db->update('al_pmbsyarat',$data);
		}
	}
?>