<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pmbform extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_prodi');
			return $query->result();
		}
		
		function PTL_all_sc()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_kursussingkat');
			return $query->result();
		}
		
		function PTL_insert2($data)
		{
			$this->db->insert('ac_kursussingkat',$data);
			return;
		}
		
		function PTL_select($ProdiID)
		{
			$this->db->where('ProdiID',$ProdiID);
			$query = $this->db->get('ac_prodi');
			return $query->row_array();
		}
		
		function PTL_select_sc($KursusSingkatID)
		{
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$query = $this->db->get('ac_kursussingkat');
			return $query->row_array();
		}
		
		function PTL_update($ProdiID,$data)
		{
			$this->db->where('ProdiID',$ProdiID);
			$this->db->update('ac_prodi',$data);
		}
		
		function PTL_update2($KursusSingkatID,$data)
		{
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$this->db->update('ac_kursussingkat',$data);
		}
	}
?>