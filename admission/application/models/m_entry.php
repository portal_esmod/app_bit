<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_entry extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('StatusAwalID','ASC');
			$query = $this->db->get('al_statusawal');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_statusawal',$data);
			return;
		}
		
		function PTL_select($StatusAwalID)
		{
			$this->db->where('StatusAwalID',$StatusAwalID);
			$query = $this->db->get('al_statusawal');
			return $query->row_array();
		}
		
		function PTL_update($StatusAwalID,$data)
		{
			$this->db->where('StatusAwalID',$StatusAwalID);
			$this->db->update('al_statusawal',$data);
		}
	}
?>