<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_akses extends CI_Model
	{
		function PTL_update_2($id_akun,$dataa)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->update('dv_akses',$dataa);
		}
		
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_all_aplikan()
		{
			$this->db->where('akses','STUDENT');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_all_last_login()
		{
			$this->db->where('na','N');
			$this->db->order_by('last_login','DESC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_all_last_logout()
		{
			$this->db->where('na','N');
			$this->db->order_by('last_logout','DESC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_all_marketing()
		{
			$this->db->where('divisi','MARKETING');
			$this->db->order_by('nama','ASC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_all_marketing_active()
		{
			$this->db->where('divisi','MARKETING');
			$this->db->where('na','N');
			$this->db->order_by('nama','ASC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
		
		function PTL_insert($dataa)
		{
			$this->db->insert('dv_akses',$dataa);
			return;
		}
		
		function PTL_select($id_akses)
		{
			$this->db->where('id_akses',$id_akses);
			$query = $this->db->get('dv_akses');
			return $query->row_array();
		}
		
		function PTL_select_akun($id_akun)
		{
			$this->db->where('id_akun',$id_akun);
			$query = $this->db->get('dv_akses');
			return $query->row_array();
		}
		
		function PTL_select_apl($username,$password)
		{
			$this->db->where('username',$username);
			$this->db->where('password',$password);
			return $this->db->get('dv_akses');
		}
		
		function PTL_select_marketing($id_akun)
		{
			$h = "-4";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$q = $this->db->query("SELECT * FROM dv_akses
				WHERE DATE_FORMAT(last_login , '%Y-%m-%d %H:%i:%s') >= '$waktu' AND id_akun = '".$id_akun."'");
			return $q->row_array();
		}
		
		function PTL_update($id_akses,$data)
		{
			$this->db->where('id_akses',$id_akses);
			$this->db->update('dv_akses',$data);
		}
		
		function PTL_update_akun($id_akun,$dataa)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->update('dv_akses',$dataa);
		}
		
		function PTL_update_username($id_akun,$email)
		{
			$data = array(
						'username' => $email
						);
			$this->db->where('id_akun',$id_akun);
			$this->db->update('dv_akses',$data);
		}
		
		function PTL_email($email)
		{
			$this->db->select('a.id_akun, a.username, a.password, a.divisi, a.jabatan, a.akses,
				a.id_akses, b.id_cabang, b.nama, b.email, b.email2');
			$this->db->from('dv_akses AS a');
			$this->db->where('b.email',$email);
			$this->db->or_where('b.email2',$email);
			$this->db->join('dv_akun AS b', 'b.id_akun = a.id_akun', 'INNER');
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return FALSE;
			}
		}
		
		function PTL_password_lama($username,$dataku)
		{
			$this->db->where('username',$username);
			$this->db->where('password',$dataku);
			$query = $this->db->get('dv_akses');
			return $query->row_array();
		}
		
		function PTL_update_password($username,$data)
		{
			$in['password'] = $data;
			$this->db->where('username',$username);
			$this->db->update('dv_akses',$in);
		}
		
		function PTL_reset($id_akses,$data_reset)
		{
			$this->db->where('id_akses',$id_akses);
			$this->db->update('dv_akses',$data_reset);
		}
	}
?>