<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_education extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('Pendidikan','ASC');
			$query = $this->db->get('al_pendidikanortu');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_pendidikanortu',$data);
			return;
		}
		
		function PTL_select($Pendidikan)
		{
			$this->db->where('Pendidikan',$Pendidikan);
			$query = $this->db->get('al_pendidikanortu');
			return $query->row_array();
		}
		
		function PTL_update($Pendidikan,$data)
		{
			$this->db->where('Pendidikan',$Pendidikan);
			$this->db->update('al_pendidikanortu',$data);
		}
		
		function PTL_delete($Pendidikan)
		{
			$this->db->where('Pendidikan',$Pendidikan);
			$this->db->delete('al_pendidikanortu');
		}
	}
?>