<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_presensi extends CI_Model
	{
		function PTL_all_today($today)
		{
			$this->db->where('Tanggal',$today);
			$this->db->order_by('JamMulai','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
	}
?>