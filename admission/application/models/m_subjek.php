<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_subjek extends CI_Model
	{
		function PTL_select($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
	}
?>