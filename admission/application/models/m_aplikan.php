<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aplikan extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun,$cekbulan)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				$this->db->where('StatusMundur',$cekstatus);
			}
			if($cekpogram != "")
			{
				$this->db->where('ProgramID',$cekpogram);
			}if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_all($limit,$offset)
		{
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			return $this->db->get('al_aplikan',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("al_aplikan");
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek_all()
		{
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_total_all()
		{
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari($cari,$limit,$offset)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('PMBID',$cari);
			$this->db->or_like('MhswID',$cari);
			$this->db->or_like('PresenterID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('Kebangsaan',$cari);
			$this->db->or_like('TempatLahir',$cari);
			$this->db->or_like('Kota',$cari);
			$this->db->or_like('Propinsi',$cari);
			$this->db->or_like('Negara',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			return $this->db->get('al_aplikan',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah($cari)
		{
			$data = 0;
			$this->db->select('*');
			$this->db->from('al_aplikan');
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('PMBID',$cari);
			$this->db->or_like('MhswID',$cari);
			$this->db->or_like('PresenterID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('Kebangsaan',$cari);
			$this->db->or_like('TempatLahir',$cari);
			$this->db->or_like('Kota',$cari);
			$this->db->or_like('Propinsi',$cari);
			$this->db->or_like('Negara',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek($cari)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('PMBID',$cari);
			$this->db->or_like('MhswID',$cari);
			$this->db->or_like('PresenterID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('Kebangsaan',$cari);
			$this->db->or_like('TempatLahir',$cari);
			$this->db->or_like('Kota',$cari);
			$this->db->or_like('Propinsi',$cari);
			$this->db->or_like('Negara',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_total($cari)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('PMBID',$cari);
			$this->db->or_like('MhswID',$cari);
			$this->db->or_like('PresenterID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('Kebangsaan',$cari);
			$this->db->or_like('TempatLahir',$cari);
			$this->db->or_like('Kota',$cari);
			$this->db->or_like('Propinsi',$cari);
			$this->db->or_like('Negara',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			if($this->session->userdata('pros_filter_periode') != "")
			{
				$this->db->like('PMBPeriodID',$this->session->userdata('pros_filter_periode'));
			}
			if($this->session->userdata('pros_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('pros_filter_marketing'));
			}
			if($this->session->userdata('pros_filter_type') != "")
			{
				$this->db->where('StatusProspekID',$this->session->userdata('pros_filter_type'));
			}
			$cekstatus = $this->session->userdata('pros_filter_status');
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			$cekpogram = $this->session->userdata('pros_filter_program');
			if($this->session->userdata('pros_filter_program') != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			$cektahun = $this->session->userdata('pros_filter_tahun');
			$cekbulan = $this->session->userdata('pros_filter_month');
			if($cektahun != "")
			{
				if($cekbulan != "")
				{
					$this->db->like('tanggal_buat',$cektahun.'-'.$cekbulan);
				}
				else
				{
					$this->db->like('tanggal_buat',$cektahun);
				}
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_completeness()
		{
			$this->db->where('NA','N');
			$this->db->where('file_syarat !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_completeness_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('file_syarat !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_interview()
		{
			$this->db->where('NA','N');
			$this->db->where('ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_interview_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_payment()
		{
			$this->db->where('NA','N');
			$this->db->where('SudahBayar !=','N');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_payment_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('SudahBayar !=','N');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_postpone()
		{
			$this->db->where('postpone','Y');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_postpone_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('postpone','Y');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_reguler()
		{
			$this->db->where('ProgramID','REG');
			$this->db->or_where('ProgramID','INT');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_reguler_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('ProgramID','REG');
			$this->db->or_where('ProgramID','INT');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_sc()
		{
			$this->db->where('ProgramID','SC');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_sc_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('ProgramID','SC');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_semester()
		{
			$this->db->where('NA','N');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_semester_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_test()
		{
			$this->db->where('NA','N');
			$this->db->where('akhir_ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_test_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('akhir_ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_urut_aplikan($kd)
		{
			$q = $this->db->query("SELECT max(AplikanID) AS LAST FROM al_aplikan WHERE AplikanID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_urut_pmb($kd)
		{
			$q = $this->db->query("SELECT max(PMBID) AS LAST FROM al_aplikan WHERE PMBID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_urut_pmb2($kd2)
		{
			$q = $this->db->query("SELECT max(PMBID) AS LAST FROM al_aplikan WHERE PMBID like '$kd2%'");
			return $q->row_array();
		}
		
		function PTL_tahun()
		{
			$q = $this->db->query("SELECT distinct(substring(tanggal_buat,1,4)) AS tahun FROM al_aplikan");
			return $q->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_aplikan',$data);
			return;
		}
		
		function PTL_insert_file($nextid,$gambar,$gambar,$gambar,$gambar,$gambar)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$storage = gmdate("Y-m", time()-($ms));
			$folder = $storage.'_';
			$dt = array(
						'foto'=>$folder.@$gambar[0],
						'BuktiSetoran'=>$folder.@$gambar[1],
						'file_ujian1'=>$folder.@$gambar[2],
						'file_syarat'=>$folder.@$gambar[3],
						'bukti_setoran'=>$folder.@$gambar[4]
						);
			$this->db->where('AplikanID',$nextid);
			return $this->db->update('al_aplikan',$dt);
		}
		
		function PTL_cek_duplicate($PMBPeriodID,$Nama,$TanggalLahir,$Pilihan1,$Pilihan2,$Pilihan3)
		{
			$this->db->where('PMBPeriodID',$PMBPeriodID);
			$this->db->where('Nama',$Nama);
			$this->db->where('TanggalLahir',$TanggalLahir);
			$this->db->where('Pilihan1',$Pilihan1);
			$this->db->where('Pilihan2',$Pilihan2);
			$this->db->where('Pilihan3',$Pilihan3);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select($AplikanID)
		{
			$this->db->where('AplikanID',$AplikanID);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select_file($dwn)
		{
			$this->db->where('foto',$dwn);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select_file_bukti($dwn)
		{
			$this->db->where('BuktiSetoran',$dwn);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select_file_ujian($dwn)
		{
			$this->db->where('file_ujian',$dwn);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_update($AplikanID,$data)
		{
			$this->db->where('AplikanID',$AplikanID);
			$this->db->update('al_aplikan',$data);
		}
		
		//sync
		
		function PTL_aplikan_all()
		{
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('aplikan');
			return $query->result();
		}
		
		function EXPORT_PMB_select($id_aplikan)
		{
			$this->db->where('AplikanID',$id_aplikan);
			$query = $this->db->get('pmb');
			return $query->row_array();
		}
		
		function EXPORT_MHSW_select_pmb($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$query = $this->db->get('mhsw');
			return $query->row_array();
		}
	}
?>