<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pmb extends CI_Model
	{
		function PTL_urut($kd)
		{
			$q = $this->db->query("SELECT max(PMBPeriodID) AS LAST FROM al_pmbperiod WHERE PMBPeriodID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_all()
		{
			$this->db->order_by('Tahun','DESC');
			$query = $this->db->get('al_pmbperiod');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_pmbperiod',$data);
			return;
		}
		
		function PTL_select($PMBPeriodID)
		{
			$this->db->where('PMBPeriodID',$PMBPeriodID);
			$query = $this->db->get('al_pmbperiod');
			return $query->row_array();
		}
		
		function PTL_select_file($dwn)
		{
			$this->db->where('soal1',$dwn);
			$query = $this->db->get('al_pmbperiod');
			return $query->row_array();
		}
		
		function PTL_update($PMBPeriodID,$data)
		{
			$this->db->where('PMBPeriodID',$PMBPeriodID);
			$this->db->update('al_pmbperiod',$data);
		}
	}
?>