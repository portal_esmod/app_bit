<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_maintenance extends CI_Model
	{
		function PTL_select($is_login)
		{
			$this->db->where('id_maintenance',$is_login);
			$query = $this->db->get('dv_maintenance');
			return $query->row_array();
		}
	}
?>