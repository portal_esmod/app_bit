<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pmbform_biaya extends CI_Model
	{
		function PTL_all($ProdiID)
		{
			$this->db->where('ProdiID',$ProdiID);
			$this->db->order_by('id_prodi_biaya','DESC');
			$query = $this->db->get('ac_prodi_biaya');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_prodi_biaya',$data);
			return;
		}
		
		function PTL_select($id_prodi_biaya)
		{
			$this->db->where('id_prodi_biaya',$id_prodi_biaya);
			$query = $this->db->get('ac_prodi_biaya');
			return $query->row_array();
		}
		
		function PTL_update($id_prodi_biaya,$data)
		{
			$this->db->where('id_prodi_biaya',$id_prodi_biaya);
			$this->db->update('ac_prodi_biaya',$data);
		}
	}
?>