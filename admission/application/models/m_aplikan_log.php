<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aplikan_log extends CI_Model
	{
		function PTL_akun()
		{
			$this->db->where('id_akun',$_COOKIE["id_akun"]);
			$this->db->order_by('kodelog','DESC');
			$query = $this->db->get('al_aplikan_log');
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->order_by('kodelog', 'DESC');
			$query = $this->db->get('al_aplikan_log');
			return $query->result();
		}
		
		function PTL_all_chat($id_akun)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->order_by('kodelog','ASC');
			$query = $this->db->get('al_aplikan_log');
			return $query->result();
		}
		
		function PTL_all_notif()
		{
			$this->db->where('status','UNREAD');
			$this->db->order_by('kodelog','DESC');
			$query = $this->db->get('al_aplikan_log');
			return $query->result();
		}
		
		function PTL_all_notif_list()
		{
			$this->db->or_like('presenter','');
			$this->db->or_like('presenter',$_COOKIE["nama"]);
			$this->db->order_by('kodelog','DESC');
			$query = $this->db->get('al_aplikan_log',100);
			return $query->result();
		}
		
		function PTL_insert($da)
		{
			$this->db->insert('al_aplikan_log',$da);
			return;
		}
		
		function PTL_select($kodelog)
		{
			$this->db->where('kodelog',$kodelog);
			$query = $this->db->get('al_aplikan_log');
			return $query->row_array();
		}
		
		function PTL_update($kodelog,$data)
		{
			$this->db->where('kodelog',$kodelog);
			$this->db->update('al_aplikan_log',$data);
		}
		
		function PTL_update_chat($id_akun,$datachat)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->where('untuk',$_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["divisi"]);
			$this->db->where('status','UNREAD');
			$this->db->update('al_aplikan_log',$datachat);
		}
		
		function PTL_marketing()
		{
			$this->db->where('divisi','MARKETING');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akses');
			return $query->result();
		}
	}
?>