<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aktivitas extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('id_aktivitas','DESC');
			$query = $this->db->get('al_aktivitas');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_aktivitas',$data);
			return;
		}
		
		function PTL_select($id_aktivitas)
		{
			$this->db->where('id_aktivitas',$id_aktivitas);
			$query = $this->db->get('al_aktivitas');
			return $query->row_array();
		}
		
		function PTL_update($id_aktivitas,$data)
		{
			$this->db->where('id_aktivitas',$id_aktivitas);
			$this->db->update('al_aktivitas',$data);
		}
		
		// function PTL_all_s()
		// {
			// $this->db->like('kodeetraining','S');
			// $this->db->where('na','N');
			// $this->db->order_by('kodeetraining','DESC');
			// $query = $this->db->get('hd_etraining');
			// return $query->result();
		// }
		
		// function PTL_select_file($dwn)
		// {
			// $this->db->where('file',$dwn);
			// $this->db->like('kodeetraining','E');
			// $query = $this->db->get('hd_etraining');
			// return $query->row_array();
		// }
		
		// function PTL_select_file_s($dwn)
		// {
			// $this->db->where('file',$dwn);
			// $this->db->like('kodeetraining','S');
			// $query = $this->db->get('hd_etraining');
			// return $query->row_array();
		// }
		
		// function PTL_urut_inventory($kd)
		// {
			// $q = $this->db->query("SELECT max(kodeetraining) AS LAST FROM hd_etraining WHERE kodeetraining like '$kd%'");
			// return $q->row_array();
		// }
	}
?>