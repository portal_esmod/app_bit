<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_province extends CI_Model
	{
		function lookup($keyword)
		{
			$this->db->where('na','N');
			$this->db->like('Propinsi',$keyword);
			$query = $this->db->get('al_propinsi',10);
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->order_by('PropinsiID','ASC');
			$query = $this->db->get('al_propinsi');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_propinsi',$data);
			return;
		}
		
		function PTL_select($PropinsiID)
		{
			$this->db->where('PropinsiID',$PropinsiID);
			$query = $this->db->get('al_propinsi');
			return $query->row_array();
		}
		
		function PTL_update($PropinsiID,$data)
		{
			$this->db->where('PropinsiID',$PropinsiID);
			$this->db->update('al_propinsi',$data);
		}
	}
?>