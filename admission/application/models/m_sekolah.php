<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_sekolah extends CI_Model
	{
		function PTL_all_sekolah()
		{
			$this->db->where('na','N');
			$this->db->where('jenis','SEKOLAH');
			$this->db->order_by('id_sekolah','DESC');
			$query = $this->db->get('al_sekolah');
			return $query->result();
		}
		
		function PTL_all_university()
		{
			$this->db->where('na','N');
			$this->db->where('jenis','UNIVERSITAS');
			$this->db->order_by('id_sekolah','DESC');
			$query = $this->db->get('al_sekolah');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_sekolah',$data);
			return;
		}
		
		function PTL_select($id_sekolah)
		{
			$this->db->where('id_sekolah',$id_sekolah);
			$query = $this->db->get('al_sekolah');
			return $query->row_array();
		}
		
		function PTL_update($id_sekolah,$data)
		{
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->update('al_sekolah',$data);
		}
	}
?>