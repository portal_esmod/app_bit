<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mix extends CI_Model
	{
		function lookup_kota($keyword)
		{
			$this->db->where('na','N');
			$this->db->like('KotaKabupaten',$keyword);
			$query = $this->db->get('al_kota',10);
			return $query->result();
		}
		
		function lookup_propinsi($keyword)
		{
			$this->db->where('na','N');
			$this->db->like('Propinsi',$keyword);
			$query = $this->db->get('al_propinsi',10);
			return $query->result();
		}
		
		function lookup_agama($keyword)
		{
			$this->db->select('*')->from('al_agama');
			$this->db->where('na','N');
			$this->db->like('Nama',$keyword);
			$query = $this->db->get();
			return $query->result();
		}
		
		function lookup_sekolah($keyword)
		{
			$this->db->where('na','N');
			$this->db->like('Nama',$keyword);
			$query = $this->db->get('al_sekolah',10);
			return $query->result();
		}
	}
?>