<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_grade extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('NilaiUjianMin','DESC');
			$query = $this->db->get('al_pmbgrade');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$this->db->where('NA','N');
			$this->db->order_by('NilaiUjianMin','ASC');
			$query = $this->db->get('al_pmbgrade');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_pmbgrade',$data);
			return;
		}
		
		function PTL_select($GradeID)
		{
			$this->db->where('GradeID',$GradeID);
			$query = $this->db->get('al_pmbgrade');
			return $query->row_array();
		}
		
		function PTL_update($GradeID,$data)
		{
			$this->db->where('GradeID',$GradeID);
			$this->db->update('al_pmbgrade',$data);
		}
	}
?>