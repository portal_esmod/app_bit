<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_information extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->order_by('InfoID','ASC');
			$query = $this->db->get('al_sumberinfo');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_sumberinfo',$data);
			return;
		}
		
		function PTL_select($InfoID)
		{
			$this->db->where('InfoID',$InfoID);
			$query = $this->db->get('al_sumberinfo');
			return $query->row_array();
		}
		
		function PTL_update($InfoID,$data)
		{
			$this->db->where('InfoID',$InfoID);
			$this->db->update('al_sumberinfo',$data);
		}
	}
?>