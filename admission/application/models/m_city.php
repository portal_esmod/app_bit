<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_city extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('PropinsiID','ASC');
			$query = $this->db->get('al_kota');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_kota',$data);
			return;
		}
		
		function PTL_urut($kd)
		{
			$q = $this->db->query("SELECT max(KotaID) AS LAST FROM al_kota WHERE PropinsiID = '$kd'");
			return $q->row_array();
		}
		
		function PTL_update($KotaKabupaten,$data)
		{
			$this->db->where('KotaKabupaten',$KotaKabupaten);
			$this->db->update('al_kota',$data);
		}
		
		function PTL_delete($KotaKabupaten)
		{
			$this->db->where('KotaKabupaten',$KotaKabupaten);
			$this->db->delete('al_kota');
		}
	}
?>