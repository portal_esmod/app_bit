/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = 'http://localhost/app/admission/assets/dra_full/browse.php?type=files';
	config.filebrowserImageBrowseUrl = 'http://localhost/app/admission/assets/dra_full/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = 'http://localhost/app/admission/assets/dra_full/browse.php?type=flash';
	config.filebrowserUploadUrl = 'http://localhost/app/admission/assets/dra_full/upload.php?type=files';
	config.filebrowserImageUploadUrl = 'http://localhost/app/admission/assets/dra_full/upload.php?type=images';
	config.filebrowserFlashUploadUrl = 'http://localhost/app/admission/assets/dra_full/upload.php?type=flash';
};
