				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Copy Master
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("master/ptl_bipot/$BIPOTID"); ?>"><i class="fa fa-cc-mastercard"></i> Costs and Discounts of Student</a></li>
						<li class="active">Copy Master</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header">
									<?php
										$master_program = $this->session->userdata('master_program2');
										$master_pro = $this->session->userdata('master_program');
										$master_prodi = $this->session->userdata('master_prodi2');
										$master_pr = $this->session->userdata('master_prodi');
										$master_bipot = $this->session->userdata('master_bipot2');
									?>
									<h3 class="box-title">FOR MASTER: <?php echo "$master_pro - $master_pr - $BIPOTID ~ $Tahun ~ $Nama"; ?></h3>
									<table class="table table-bordered table-striped">
										<tr>
											<td>
												<form action="<?php echo site_url("master/ptl_filter_program2"); ?>" method="POST">
													<select name="program" class="form-control" onchange="this.form.submit()">
														<option value="">-- PROGRAM --</option>
														<option value="<?php echo "REG_".$BIPOTID; ?>" <?php if($master_program == "REG"){ echo "selected"; } ?>>REG - Regular</option>
														<option value="<?php echo "INT_".$BIPOTID; ?>" <?php if($master_program == "INT"){ echo "selected"; } ?>>INT - Intensive</option>
														<option value="<?php echo "SC_".$BIPOTID; ?>" <?php if($master_program == "SC"){ echo "selected"; } ?>>SC - Short Course</option>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("master/ptl_filter_prodi2"); ?>" method="POST">
													<select name="prodi" class="form-control" onchange="this.form.submit()">
														<option value="">-- STUDY PROGRAM --</option>
														<?php
															if($master_program == "SC")
															{
																foreach($filter as $f)
																{
																	echo "<option value='".$f->KursusSingkatID."_$BIPOTID'";
																	if($master_prodi == $f->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">$f->KursusSingkatID - $f->Nama</option>";
																}
															}
															else
															{
																foreach($filter as $f)
																{
																	echo "<option value='".$f->ProdiID."_$BIPOTID'";
																	if($master_prodi == $f->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$f->Jenjang - $f->Nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("master/ptl_filter_bipot"); ?>" method="POST">
													<select name="BIPOTID" class="form-control" onchange="this.form.submit()">
														<option value="<?php echo "_$BIPOTID" ?>">-- BIPOT --</option>
														<?php
															if($rowrecord)
															{
																foreach($rowrecord as $r)
																{
																	if($r->BIPOTID != $BIPOTID)
																	{
																		echo "<option value='".$r->BIPOTID."_$BIPOTID'";
																		if($master_bipot == $r->BIPOTID)
																		{
																			echo "selected";
																		}
																		echo ">$r->BIPOTID - $r->Tahun - $r->Nama</option>";
																	}
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("master/ptl_bipot_copy_insert"); ?>" method="POST">
													<input type="hidden" name="new_BIPOTID" value="<?php echo $master_bipot; ?>"/>
													<input type="hidden" name="link" value="<?php echo $BIPOTID; ?>"/>
													<input type="submit" value="Copy" id="my_button" class="btn btn-primary" onclick="return confirm('Are you sure want copy this data for BIPOT <?php echo "$master_pro - $master_pr - $BIPOTID ~ $Tahun ~ $Nama"; ?>?')">
												</form>
											</td>
										</tr>
									</table>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Total</th>
												<th><u>Automatic</u><br/>Status</th>
												<th><u>Begin</u><br/>How Many</th>
												<th>Category Value</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if(($rowrecord) AND ($master_bipot != ""))
												{
													$cek = "0";
													foreach($rowrecord as $r)
													{
														if($master_bipot == $r->BIPOTID)
														{
															$cek = "1";
														}
													}
													if($cek == "1")
													{
														if($detail)
														{
															$no = 1;
															$jumlah_potongan = "";
															$jumlah_cost = "";
															foreach($detail as $row)
															{
																if($row->TrxID == "-1")
																{
																	$BIPOT2IDRef = $row->BIPOT2IDRef;
																	if($BIPOT2IDRef == 0)
																	{
																		$TambahanNama = "";
																		if($row->TambahanNama != "")
																		{
																			$TambahanNama = "<br/>".$row->TambahanNama;
																		}
																		$BIPOTNamaID = $row->BIPOTNamaID;
																		$res = $this->m_master->PTL_select($BIPOTNamaID);
																		$Nama = "<font color='red'>No master available</font>";
																		if($res)
																		{
																			$Nama = $res['Nama'];
																		}
																		echo"
																			<tr class='warning'>
																				<td>$no</td>
																				<td title='BIPOT2ID : $row->BIPOT2ID'><b>$Nama</b>$TambahanNama</td>
																				<td><p align='right'>".formatRupiah($row->Jumlah)."</p></td>
																				<td><p align='center'><u>$row->Otomatis</u><br/>$row->StatusMhswID</p></td>
																				<td><p align='center'><u>$row->MulaiSesi</u><br/>$row->KaliSesi</p></td>
																				<td>$row->GunakanKategoriNilai</td>
																			</tr>
																			";
																		$no++;
																		$jumlah_potongan = $jumlah_potongan + $row->Jumlah;
																	}
																}
															}
															foreach($detail as $row)
															{
																if($row->TrxID == "1")
																{
																	$BIPOTID = $row->BIPOTID;
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	$BIPOT2IDRef = $row->BIPOT2ID;
																	$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOT2IDRef);
																	$ref_detail = "";
																	$ref_jumlah2 = 0;
																	if($resref_detail)
																	{
																		foreach($resref_detail as $rd)
																		{
																			$BIPOTNamaID = $rd->BIPOTNamaID;
																			$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																			if($resref_detail2)
																			{
																				$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$resref_detail2["Nama"]."(".formatRupiah($rd->Jumlah).")";
																				$ref_jumlah2 = $ref_jumlah2 + $rd->Jumlah;
																			}
																		}
																	}
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	echo"
																		<tr class='success'>
																			<td>$no</td>
																			<td title='BIPOT2ID : $row->BIPOT2ID'>
																				<b>$Nama</b>
																				$TambahanNama
																				$ref_detail
																			</td>
																			<td title='(".formatRupiah($row->Jumlah).") - (".formatRupiah($ref_jumlah2).")'><p align='right'>".formatRupiah($row->Jumlah - $ref_jumlah2)."</p></td>
																			<td><p align='center'><u>$row->Otomatis</u><br/>$row->StatusMhswID</p></td>
																			<td><p align='center'><u>$row->MulaiSesi</u><br/>$row->KaliSesi</p></td>
																			<td>$row->GunakanKategoriNilai</td>
																		</tr>
																		";
																	$no++;
																	$jumlah_cost = $jumlah_cost + ($row->Jumlah - $ref_jumlah2);
																}
															}
															echo"
																<tr>
																	<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																	<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($jumlah_cost - $jumlah_potongan)."</p></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																</tr>
																";
														}
													}
												}
												else
												{
													echo"
														<tr>
															<td colspan='7' align='center'><font color='red'><b>No data available</b></font></td>
														</tr>
														";
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>