				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Master Cost and Discounts
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li class="active"><i class="fa fa-cc-mastercard"></i> Master Cost and Discounts</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box">
								<div class="box-header">
									<table class="table table-bordered table-striped">
										<tr>
											<td><h3 class="box-title">Cost</h3></td>
											<td align="right"><a href="<?php echo site_url("master/ptl_cost_form"); ?>"><h5 class="box-title">Add Cost</h5></a></td>
										</tr>
									</table>
								</div>
								<div class="box-body">
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<?php
												if($browser == "MOBILE")
												{
													echo"
														<tr>
															<th>#</th>
															<th>Name</th>
															<th>Account</th>
															<th>NA</th>
														</tr>
														";
												}
												else
												{
													echo"
														<tr>
															<th>#</th>
															<th>Name</th>
															<th>Account</th>
															<th>Discount</th>
															<th>Penalty</th>
															<th>Deposit</th>
															<th>NA</th>
														</tr>
														";
												}
											?>
										</thead>
										<tbody>
											<?php
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														if($row->NA == "Y")
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr class='info'>";
														}
														if($browser == "MOBILE")
														{
															echo"
																	<td title='$row->Urutan'>$no</td>
																	<td><a href='".site_url("master/ptl_cost_edit/$row->BIPOTNamaID")."'>$row->Nama</a></td>
																	<td>$row->RekeningID</td>
																	<td>$row->NA</td>
																</tr>
																";
														}
														else
														{
															echo"
																	<td title='$row->Urutan'>$no</td>
																	<td><a href='".site_url("master/ptl_cost_edit/$row->BIPOTNamaID")."'>$row->Nama</a></td>
																	<td>$row->RekeningID</td>
																	<td>$row->DipotongBeasiswa</td>
																	<td>$row->KenaDenda</td>
																	<td>$row->Deposit</td>
																	<td>$row->NA</td>
																</tr>
																";
														}
														$no++;
													}
												}
											?>
										</tbody>
										<tfoot>
											<?php
												if($browser == "MOBILE")
												{
													echo"
														<tr>
															<th>#</th>
															<th>Name</th>
															<th>Account</th>
															<th>NA</th>
														</tr>
														";
												}
												else
												{
													echo"
														<tr>
															<th>#</th>
															<th>Name</th>
															<th>Account</th>
															<th>Discount</th>
															<th>Penalty</th>
															<th>Deposit</th>
															<th>NA</th>
														</tr>
														";
												}
											?>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="box">
								<div class="box-header">
									<table class="table table-bordered table-striped">
										<tr>
											<td><h3 class="box-title">Discounts</h3></td>
											<td align="right"><a href="<?php echo site_url("master/ptl_discount_form"); ?>"><h5 class="box-title">Add Discounts</h5></a></td>
										</tr>
									</table>
								</div>
								<div class="box-body">
									<table id="example2" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Account</th>
												<th>NA</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($rowrecord2)
												{
													$no = 0;
													foreach($rowrecord2 as $row)
													{
														if($row->NA == "Y")
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr class='info'>";
														}
														echo"
																<td title='$row->Urutan'>$no</td>
																<td><a href='".site_url("master/ptl_discount_edit/$row->BIPOTNamaID")."'>$row->Nama</a></td>
																<td>$row->RekeningID</td>
																<td>$row->NA</td>
															</tr>
															";
														$no++;
													}
												}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Account</th>
												<th>NA</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>