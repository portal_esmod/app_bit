				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Cost and Discounts of Student
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li class="active"><i class="fa fa-cc-mastercard"></i> Cost and Discounts of Student</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-5">
							<div class="box">
								<div class="box-header">
									<table class="table table-bordered table-striped">
										<tr>
											<?php
												$master_program = $this->session->userdata('master_program');
												$master_prodi = $this->session->userdata('master_prodi');
											?>
											<td><h3 class="box-title">Master Name</h3></td>
											<?php
												if((($master_program != "") AND ($master_program != "NONE")) AND (($master_prodi != "") AND ($master_prodi != "NONE")))
												{
											?>
													<td align="right"><a href="<?php echo site_url("master/ptl_bipot_form"); ?>"><h5 class="box-title">Add Master</h5></a></td>
											<?php
												}
											?>
										</tr>
										<tr>
											<td>
												<form action="<?php echo site_url("master/ptl_filter_program"); ?>" method="POST">
													<select name="program" class="form-control" onchange="this.form.submit()">
														<option value="">-- PROGRAM --</option>
														<?php
															if($rowprogram)
															{
																foreach($rowprogram as $rp)
																{
																	echo "<option value='$rp->ProgramID'";
																	if($rp->ProgramID == $master_program)
																	{
																		echo "selected";
																	}
																	echo ">$rp->ProgramID - $rp->Nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("master/ptl_filter_prodi"); ?>" method="POST">
													<select name="prodi" class="form-control" onchange="this.form.submit()">
														<option value="">-- STUDY PROGRAM --</option>
														<?php
															if($master_program == "SC")
															{
																foreach($filter as $f)
																{
																	echo "<option value='$f->KursusSingkatID'";
																	if($master_prodi == $f->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">$f->KursusSingkatID - $f->Nama</option>";
																}
															}
															else
															{
																foreach($filter as $f)
																{
																	echo "<option value='$f->ProdiID'";
																	if($master_prodi == $f->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$f->Jenjang - $f->Nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
										</tr>
									</table>
								</div>
								<?php
									if(($master_program != "") AND ($master_program != "NONE"))
									{
								?>
										<div class="box-body">
											<table id="example1" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>#</th>
														<th>Year Code</th>
														<th>Name</th>
														<th>Def</th>
														<th>NA</th>
													</tr>
												</thead>
												<tbody>
													<?php
														if($rowrecord)
														{
															$no = 1;
															foreach($rowrecord as $row)
															{
																if($BIPOTID == $row->BIPOTID)
																{
																	echo"
																		<tr>
																			<td style='background-color:#FEE730;border:5px double;'>$no</td>
																			<td style='background-color:#FEE730;border:5px double;'><a href='".site_url("master/ptl_bipot_edit/$row->BIPOTID")."' title='Change Data'>$row->Tahun</a></td>
																			<td style='background-color:#FEE730;border:5px double;'><a href='".site_url("master/ptl_bipot/$row->BIPOTID")."' title='View Detail'>$row->Nama</a></td>
																			<td style='background-color:#FEE730;border:5px double;'>$row->Def</td>
																			<td style='background-color:#FEE730;border:5px double;'>$row->NA</td>
																		</tr>
																		";
																}
																else
																{
																	echo"
																		<tr>
																			<td>$no</td>
																			<td><a href='".site_url("master/ptl_bipot_edit/$row->BIPOTID")."' title='Change Data'>$row->Tahun</a></td>
																			<td><a href='".site_url("master/ptl_bipot/$row->BIPOTID")."' title='View Detail'>$row->Nama</a></td>
																			<td>$row->Def</td>
																			<td>$row->NA</td>
																		</tr>
																		";
																}
																$no++;
															}
														}
													?>
												</tbody>
												<tfoot>
													<tr>
														<th>#</th>
														<th>Year Code</th>
														<th>Name</th>
														<th>Def</th>
														<th>NA</th>
													</tr>
												</tfoot>
											</table>
										</div>
								<?php
									}
								?>
							</div>
						</div>
						<?php
							if(($master_program != "") AND ($master_program != "NONE") AND ($BIPOTID != ""))
							{
						?>
								<div class="col-md-7">
									<div class="box">
										<div class="box-header">
											<table class="table table-bordered table-striped">
												<tr>
													<td><h3 class="box-title">Detail Cost</h3></td>
													<td align="right"><a href="<?php echo site_url("master/ptl_bipot_detail_cost_form/$BIPOTID"); ?>"><h5 class="box-title">Add Detail Cost</h5></a></td>
												</tr>
												<tr>
													<td><h3 class="box-title"><a href="<?php echo site_url("master/ptl_bipot_copy_form/$BIPOTID"); ?>"><h5 class="box-title">Copy From Other Bipot</h5></a></h3></td>
													<td align="right"><a href="<?php echo site_url("master/ptl_bipot_detail_discount_form/$BIPOTID"); ?>"><h5 class="box-title">Add Detail Discounts</h5></a></td>
												</tr>
											</table>
										</div>
										<div class="box-body">
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>#</th>
														<th>Name</th>
														<th>Total</th>
														<th><u>Automatic</u><br/>Status</th>
														<th><u>Begin</u><br/>How Many</th>
														<th>Category Value</th>
														<th>NA</th>
													</tr>
												</thead>
												<tbody>
													<?php
														if($detail)
														{
															$no = 1;
															$jumlah_potongan = "";
															$jumlah_cost = "";
															foreach($detail as $row)
															{
																if($row->TrxID == "-1")
																{
																	$BIPOT2IDRef = $row->BIPOT2IDRef;
																	if($BIPOT2IDRef == 0)
																	{
																		$TambahanNama = "";
																		if($row->TambahanNama != "")
																		{
																			$TambahanNama = "<br/>".$row->TambahanNama;
																		}
																		$BIPOTNamaID = $row->BIPOTNamaID;
																		$res = $this->m_master->PTL_select($BIPOTNamaID);
																		$Nama = "<font color='red'>No master available</font>";
																		if($res)
																		{
																			$Nama = $res['Nama'];
																		}
																		echo"
																			<tr class='warning'>
																				<td>$no</td>
																				<td title='BIPOT2ID : $row->BIPOT2ID'><b><a href='".site_url("master/ptl_bipot_detail_discount_edit/$row->BIPOT2ID")."' title='Change Data'>$Nama</a></b>$TambahanNama</td>
																				<td><p align='right'>".formatRupiah($row->Jumlah)."</p></td>
																				<td><p align='center'><u>$row->Otomatis</u><br/>$row->StatusMhswID</p></td>
																				<td><p align='center'><u>$row->MulaiSesi</u><br/>$row->KaliSesi</p></td>
																				<td>$row->GunakanKategoriNilai</td>
																				<td>";
													?>
																					<a href="<?php echo site_url("master/ptl_bipot_detail_delete/$row->BIPOTID/$row->BIPOT2ID"); ?>" title="Delete Data" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
													<?php
																			echo "</td>
																			</tr>
																			";
																		$no++;
																		$jumlah_potongan = $jumlah_potongan + $row->Jumlah;
																	}
																}
															}
															foreach($detail as $row)
															{
																if($row->TrxID == "1")
																{
																	$BIPOTID = $row->BIPOTID;
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	$BIPOT2IDRef = $row->BIPOT2ID;
																	$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOT2IDRef);
																	$ref_detail = "";
																	$ref_jumlah2 = 0;
																	if($resref_detail)
																	{
																		foreach($resref_detail as $rd)
																		{
																			$BIPOTNamaID = $rd->BIPOTNamaID;
																			$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																			if($resref_detail2)
																			{
																				$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$resref_detail2["Nama"]."(".formatRupiah($rd->Jumlah).") <a href='".site_url("master/ptl_bipot_detail_delete/$rd->BIPOTID/$rd->BIPOT2ID")."' title='Delete Data' class='btn btn-danger'><i class='fa fa-trash'></i></a>";
																				$ref_jumlah2 = $ref_jumlah2 + $rd->Jumlah;
																			}
																		}
																	}
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	echo"
																		<tr class='success'>
																			<td>$no</td>
																			<td title='BIPOT2ID : $row->BIPOT2ID'>
																				<b><a href='".site_url("master/ptl_bipot_detail_cost_edit/$row->BIPOT2ID")."' title='Change Data'>$Nama</a></b>
																				$TambahanNama
																				$ref_detail
																			</td>
																			<td title='(".formatRupiah($row->Jumlah).") - (".formatRupiah($ref_jumlah2).")'><p align='right'>".formatRupiah($row->Jumlah - $ref_jumlah2)."</p></td>
																			<td><p align='center'><u>$row->Otomatis</u><br/>$row->StatusMhswID</p></td>
																			<td><p align='center'><u>$row->MulaiSesi</u><br/>$row->KaliSesi</p></td>
																			<td>$row->GunakanKategoriNilai</td>
																			<td>";
													?>
																					<a href="<?php echo site_url("master/ptl_bipot_detail_delete/$row->BIPOTID/$row->BIPOT2ID"); ?>" title="Delete Data" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
													<?php
																			echo "</td>
																		</tr>
																		";
																	$no++;
																	$jumlah_cost = $jumlah_cost + ($row->Jumlah - $ref_jumlah2);
																}
															}
															echo"
																<tr>
																	<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																	<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($jumlah_cost - $jumlah_potongan)."</p></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																	<td style='background-color:#D8E5D8;border:5px;'></td>
																</tr>
																";
														}
														else
														{
															echo"
																<tr>
																	<td colspan='7' align='center'><font color='red'><b>No data available</b></font></td>
																</tr>
																";
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
						<?php
							}
						?>
					</div>
				</section>