<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Scholarship extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function index()
		{
			$this->session->set_userdata('menu','scholarship');
			$this->load->view('Portal/v_header');
			$this->load->view('Scholarship/v_scholarship_list');
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pending()
		{
			$this->session->set_userdata('menu','scholarship');
			$this->load->view('Portal/v_header');
			$this->load->view('Scholarship/v_scholarship_pending');
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_report()
		{
			$this->session->set_userdata('menu','scholarship');
			$this->load->view('Portal/v_header');
			$this->load->view('Scholarship/v_scholarship_report');
			$this->load->view('Portal/v_footer_table');
		}
	}
?>