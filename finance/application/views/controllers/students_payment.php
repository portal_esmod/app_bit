<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Students_payment extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->kd_bayar = gmdate("Y", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('encryption');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_bayar');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_rekening');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('pym_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_jur');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('pym_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_tahun');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('pym_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_prodi');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			if($cekkelas != "")
			{
				$this->session->set_userdata('pym_filter_kelas',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_kelas');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_no()
		{
			$this->authentification();
			$cekno = $this->input->post('cekno');
			if($cekno != "")
			{
				$this->session->set_userdata('pym_filter_no',$cekno);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_no');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_sesi()
		{
			$this->authentification();
			$ceksesi = $this->input->post('ceksesi');
			if($ceksesi != "")
			{
				$this->session->set_userdata('pym_filter_sesi',$ceksesi);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_sesi');
			}
			redirect("students_payment");
		}
		
		function ptl_filter_payment()
		{
			$this->authentification();
			$ceklunas = $this->input->post('ceklunas');
			if($ceklunas != "")
			{
				$this->session->set_userdata('pym_filter_payment',$ceklunas);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_payment');
			}
			redirect("students_payment");
		}
		
		function ptl_edit_filter_bipot()
		{
			$this->authentification();
			$link = $this->input->post('BIPOTID');
			$word = explode(" ",$link);
			$cekBIPOTID = $word[0];
			$KHSID = $word[1];
			if($cekBIPOTID != "")
			{
				$this->session->set_userdata('pym_edit_master_bipot',$cekBIPOTID);
			}
			else
			{
				$this->session->set_userdata('pym_edit_master_bipot','NONE');
			}
			redirect("students_payment/ptl_edit/$KHSID");
		}
		
		function ptl_edit_filter_tahun()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('cektahun'));
			$cektahun = $word[0];
			$KHSID = $word[1];
			if($cektahun != "")
			{
				$this->session->set_userdata('pym_edit_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('pym_edit_filter_tahun','');
			}
			redirect("students_payment/ptl_edit/$KHSID");
		}
		
		function ptl_filter_bipotmhs_add_cost_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID2');
			$word = explode(" ",$link);
			$cekBIPOTNamaID2 = $word[0];
			$MhswID = $word[1];
			$BIPOTID = $word[2];
			$KHSID = $word[3];
			$TahunID = $word[4];
			$BIPOT2ID = $word[5];
			if($cekBIPOTNamaID2 != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result1 = $this->m_master->PTL_select($BIPOTNamaID);
				$result2 = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$this->session->set_userdata('cekRekeningID2',$result1['RekeningID']);
				$this->session->set_userdata('cekDefJumlah2',$result2['Jumlah']);
				$this->session->set_userdata('cekBIPOTNamaID2',$cekBIPOTNamaID2);
				$this->session->set_userdata('cekBIPOT2ID2',$BIPOT2ID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID2','NONE');
				$this->session->set_userdata('cekDefJumlah2','NONE');
				$this->session->set_userdata('cekBIPOTNamaID2','NONE');
				$this->session->unset_userdata('cekBIPOT2ID2');
			}
			redirect("students_payment/ptl_bipotmhs_cost_form/$MhswID/$BIPOTID/$KHSID/$TahunID");
		}
		
		function ptl_filter_bipotmhs_add_discounts_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID');
			$word = explode(" ",$link);
			$cekBIPOTNamaID = $word[0];
			$MhswID = $word[1];
			$BIPOTID = $word[2];
			$KHSID = $word[3];
			$TahunID = $word[4];
			$BIPOT2ID = $word[5];
			if($cekBIPOTNamaID != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result1 = $this->m_master->PTL_select($BIPOTNamaID);
				$result2 = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$this->session->set_userdata('cekRekeningID',$result1['RekeningID']);
				$this->session->set_userdata('cekDefJumlah',$result2['Jumlah']);
				$this->session->set_userdata('cekBIPOTNamaID',$cekBIPOTNamaID);
				$this->session->set_userdata('cekBIPOT2ID',$BIPOT2ID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID','NONE');
				$this->session->set_userdata('cekDefJumlah','NONE');
				$this->session->set_userdata('cekBIPOTNamaID','NONE');
				$this->session->unset_userdata('cekBIPOT2ID');
			}
			redirect("students_payment/ptl_bipotmhs_discounts_form/$MhswID/$BIPOTID/$KHSID/$TahunID");
		}
		
		function ptl_report_filter_jur1()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('pym_report_filter_jur1',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_jur1');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_tahun1()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('pym_report_filter_tahun1',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_tahun1');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_prodi1()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('pym_report_filter_prodi1',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_prodi1');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_kelas1()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			if($cekkelas != "")
			{
				$this->session->set_userdata('pym_report_filter_kelas1',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_kelas1');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_jur2()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('pym_report_filter_jur2',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_jur2');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_tahun2()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('pym_report_filter_tahun2',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_tahun2');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_prodi2()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('pym_report_filter_prodi2',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_prodi2');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_report_filter_kelas2()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			if($cekkelas != "")
			{
				$this->session->set_userdata('pym_report_filter_kelas2',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('pym_report_filter_kelas2');
			}
			redirect("students_payment/ptl_report");
		}
		
		function ptl_filter_regiter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$KHSID = $this->input->post('KHSID');
			if($cektahun != "")
			{
				$this->session->set_userdata('pym_filter_register_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('pym_filter_register_tahun');
			}
			redirect("students_payment/ptl_register_next_semester/$KHSID");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			if($this->session->userdata('pym_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('pym_filter_no');
			}
			$uri_segment = 3;
			$offset = $this->uri->segment($uri_segment);
			$data['cariproduk'] = $this->m_khs->PTL_all_cari_all($cekno,$offset);
			$jml = $this->m_khs->PTL_all_cari_jumlah_all();
			$data['cek'] = $this->m_khs->PTL_all_cari_cek_all();
			$data['total'] = count($this->m_khs->PTL_all_cari_total_all());
			$this->load->library('pagination');
			$config['base_url'] = site_url("students_payment/index");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students_payment/index";
			
			$cekjurusan = $this->session->userdata('pym_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_filter($cekjurusan);
			$res = $this->m_khs->PTL_all_totcostbi();
			$data['totcost'] = $res['totcost'];
			$data['totpot'] = $res['totpot'];
			$data['totbi'] = $res['totbi'];
			$data['pencarian'] = '';
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function search($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			if($this->session->userdata('pym_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('pym_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_khs->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_khs->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_khs->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_khs->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("students_payment/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students_payment/search/$cari";
			
			$cekjurusan = $this->session->userdata('pym_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_filter($cekjurusan);
			$res = $this->m_khs->PTL_all_totcostbi_all($cari);
			$data['totcost'] = $res['totcost'];
			$data['totpot'] = $res['totpot'];
			$data['totbi'] = $res['totbi'];
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$data['CekBayarMhswID'] = $this->uri->segment(4);
			$res = $this->m_khs->PTL_select($KHSID);
			$TahunID = '';
			$data['pym_TahunID'] = '';
			$MhswID = '';
			$data['StatusMhswID'] = '';
			$data['Sesi'] = '';
			$data['Biaya'] = 0;
			$data['Potongan'] = 0;
			$data['Bayar'] = 0;
			$data['Deposit'] = 0;
			$data['suspend'] = '';
			if($res)
			{
				$TahunID = $res['TahunID'];
				$data['pym_TahunID'] = $res['TahunID'];
				$MhswID = $res['MhswID'];
				$data['StatusMhswID'] = $res['StatusMhswID'];
				$data['Sesi'] = $res['Sesi'];
				$data['Biaya'] = $res['Biaya'];
				$data['Potongan'] = $res['Potongan'];
				$data['Bayar'] = $res['Bayar'];
				$data['Deposit'] = $res['Deposit'];
				$data['suspend'] = $res['suspend'];
			}
			$data['KHSID'] = $KHSID;
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$MhswID = '';
			$data['MhswID'] = '';
			$data['virtual_account'] = '';
			$data['PMBID'] = '';
			$data['Nama'] = '';
			$data['ProgramID'] = '';
			$data['ProdiID'] = '';
			$data['StatusMhswID'] = '';
			$data['login_buat'] = '';
			$data['tanggal_buat'] = '';
			$data['login_edit'] = '';
			$data['tanggal_edit'] = '';
			$master_program = '';
			$master_prodi = '';
			$cekjurusan = '';
			if($result)
			{
				$MhswID = $result['MhswID'];
				$data['MhswID'] = $result['MhswID'];
				$data['virtual_account'] = $result['virtual_account'];
				$data['PMBID'] = $result['PMBID'];
				$data['Nama'] = $result['Nama'];
				$data['ProgramID'] = $result['ProgramID'];
				$data['ProdiID'] = $result['ProdiID'];
				$data['StatusMhswID'] = $result['StatusMhswID'];
				$data['login_buat'] = $result['login_buat'];
				$data['tanggal_buat'] = $result['tanggal_buat'];
				$data['login_edit'] = $result['login_edit'];
				$data['tanggal_edit'] = $result['tanggal_edit'];
				$master_program = $result['ProgramID'];
				$master_prodi = $result['ProdiID'];
				$cekjurusan = $result['ProgramID'];
			}
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord'] = $this->m_master->PTL_bipot($master_program,$master_prodi);
			
			$mhsw = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			}
			else
			{
				$BIPOTID = $this->session->userdata('pym_edit_master_bipot');
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = $BIPOTID;
				$data['detail'] = $this->m_master->PTL_bipot_detail($BIPOTID);
			}
			$data['rowbukti'] = $this->m_bayar->PTL_pym_all_select($MhswID,$BIPOTID);
			$this->load->view('Portal/v_header');
			if($result)
			{
				$this->load->view('Students_Payment/v_students_edit',$data);
			}
			else
			{
				$data['pesan'] = "Student data with KHSID <b>'$KHSID'</b> is invalid.";
				$this->load->view('Students_Payment/v_students_messages',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_withdrawal_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['KHSID'] = $KHSID;
			$data['MhswID'] = $MhswID;
			$data['TahunID'] = $TahunID;
			$data['Nama'] = $result['Nama'];
			$mhsw = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			}
			$data['rowrekening'] = $this->m_rekening->PTL_all_active();
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			// if((stristr($_COOKIE["akses"],"MANAGER")) OR (stristr($_COOKIE["akses"],"PRESIDENT")))
			// {
				$this->load->view('Portal/v_header',$hdata);
				$this->load->view('Students_Payment/v_students_edit_withdrawal_form',$data);
				$this->load->view('Portal/v_footer_table',$fdata);
			// }
			// else
			// {
				// $data['pesan'] = "You do not have access to this page. Please contact your manager.";
				// $this->load->view('Portal/v_header');
				// $this->load->view('Students_Payment/v_students_messages',$data);
				// $this->load->view('Portal/v_footer_table');
			// }
		}
		
		function ptl_withdrawal_insert()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$type = $this->input->post('type');
			$t = "-1";
			if($type == "C")
			{
				$t = "-2";
			}
			$total = $this->input->post('total');
			$pesan = "";
			for($i=1;$i<=$total;$i++)
			{
				$nama = $this->input->post("nama$i");
				$besar = $this->input->post("besar$i");
				$dibayar = $this->input->post("dibayar$i");
				$payment = str_replace(".","",$this->input->post("payment$i"));
				$sisa = $besar - $dibayar;
				if($payment > $dibayar)
				{
					$pesan .= "Withdrawal for '$nama' beyond the limit.".'\n'."Paid ".formatRupiah2($dibayar)." - Withdrawal ".formatRupiah2($payment).'\n\n';
				}
				if($payment < 0)
				{
					$pesan .= "Withdrawal for '$nama' not allowed minus value.".'\n'."Withdrawal ".formatRupiah2($payment).'\n\n';
				}
			}
			if($pesan != "")
			{
				echo warning($pesan,"../students_payment/ptl_withdrawal_form/$KHSID/$MhswID/$TahunID");
			}
			else
			{
				$kd = $this->kd_bayar."-";
				$result = $this->m_bayar->PTL_urut_bayar($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,5,5);
				$nextnourut = $lastnourut + 1;
				$nextid = $kd.sprintf('%05s',$nextnourut);
				$res = $this->m_mahasiswa->PTL_select($MhswID);
				$Jumlah = 0;
				for($i2=1;$i2<=$total;$i2++)
				{
					$BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if(str_replace(".","",$this->input->post("payment$i2")) > 0)
					{
						$data = array(
									'Dibayar' => $resbipot['Dibayar'] - str_replace(".","",$this->input->post("payment$i2")),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						$Jumlah = $Jumlah + str_replace(".","",$this->input->post("payment$i2"));
					
						$data_bayar2 = array(
									'BayarMhswID' => $nextid,
									'BIPOTMhswID' => $BIPOTMhswID,
									'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									'Jumlah' => str_replace(".","",$this->input->post("payment$i2")),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_bayar->PTL_insert2($data_bayar2);
						$BIPOTID = $this->input->post("BIPOTID$i2");
					}
				}
				if($Jumlah == 0)
				{
					echo warning("Sorry! You do not input a nominal payment.","../students_payment/ptl_withdrawal_form/$KHSID/$MhswID/$TahunID");
				}
				else
				{
					$data_bayar = array(
										'BayarMhswID' => $nextid,
										'BIPOTID' => $BIPOTID,
										'TahunID' => $TahunID,
										'RekeningID' => $this->input->post('RekeningID'),
										'MhswID' => $MhswID,
										'TrxID' => $t,
										'Bank' => strtoupper($this->input->post('Bank')),
										'BuktiSetoran' => $this->input->post('BuktiSetoran'),
										'Tanggal' => $this->input->post('Tanggal'),
										'Jumlah' => $Jumlah,
										'Keterangan' => $this->input->post('Keterangan'),
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
					$this->m_bayar->PTL_insert($data_bayar);
					$res_khs = $this->m_khs->PTL_select($KHSID);
					$Bayar = $Jumlah;
					$Tarik = 0;
					if($res_khs)
					{
						$Bayar = $res_khs['Bayar'] - $Jumlah;
						$Tarik = $res_khs['Tarik'] + $Jumlah;
					}
					$data = array(
								'Bayar' => $Bayar,
								'Tarik' => $Tarik,
								'Tutup' => 'N',
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$key1 = $this->encryption->encode($nextid);
					$key2 = $this->encryption->encode($MhswID);
					$key3 = $this->encryption->encode($this->waktu);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($res['Email']);
					$this->email->cc($res['Email2']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR WITHDRAWAL DATA HAS BEEN PROCESSED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						Dear '<b>$res[Nama] - $res[ProgramID] - $res[ProdiID]</b>', your withdrawal data has been processed. Please check your PORTAL LMS or contact Finance Team.
						<br/>
						<br/>
						Reason : ".$this->input->post('Keterangan')."
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/student_key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>WITHDRAWAL $nextid</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://app.esmodjakarta.com/lms'>PORTAL LMS (Learning Management System)</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					// echo warning("Withdrawal data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.","../students_payment/ptl_edit/$KHSID");
					if($this->email->send())
					{
						echo warning("Withdrawal data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.","../students_payment/ptl_edit/$KHSID");
					}
					else
					{
						echo warning("Email server is not active. Withdrawal data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.","../students_payment/ptl_edit/$KHSID");
					}
				}
			}
		}
		
		function ptl_print_payment()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$BayarMhswID = $this->uri->segment(3);
			$KHSID = $this->uri->segment(4);
			$MhswID = $this->uri->segment(5);
			$TahunID = $this->uri->segment(6);
			$status = $this->uri->segment(7);
			$resmain = $this->m_bayar->PTL_select($BayarMhswID);
			$result = $this->m_bayar->PTL_all_select2($BayarMhswID);
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$ProgramID = $res['ProgramID'];
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$tambah = $resmain['Cetak'] + 1;
			$data_bayar = array(
							'Cetak' => $tambah
							);
			$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
			
			$judul = "OFFICIAL RECEIPT";
			$to1 = "Sudah Terima Dari";
			$to2 = "(Received From)";
			$pay1 = "Untuk Pembayaran";
			$pay2 = "(Being Payment For)";
			$pesan = "NB : Pembayaran yang sudah ditransfer tidak dapat ditarik kembali";
			$ttd = "Yang Menerima (Received By)";
			$file = "PAYMENT_";
			if($resmain['TrxID'] == "-1")
			{
				$judul = "WITHDRAWAL";
				$to1 = "Penarikan Oleh";
				$to2 = "(Withdrawal by)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Yang Menerima (Received By)";
				$file = "WITHDRAWAL_";
			}
			if($resmain['TrxID'] == "-2")
			{
				$judul = "CORRECTIONS";
				$to1 = "Dikoreksi Untuk";
				$to2 = "(Corrected for)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Dikoreksi Oleh (Corrected By)";
				$file = "CORRECTIONS_";
			}
			
			$BIPOTID = $resmain['BIPOTID'];
			$resbipot = $this->m_master->PTL_bipot_select($BIPOTID);
			
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(28,1,"$judul",0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,1,"NO : $BayarMhswID",0,0,"R");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY()-0.6,12.5,1.6);
			
			$this->fpdf->Ln(1.2);
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->SetFont("helvetica","BI",20);
			$this->fpdf->Cell(26,0.7,$status,0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->Line(1,4.7,28.9,4.7);
			$this->fpdf->Line(1,4.75,28.9,4.75);
			
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $to1, "", 0, "L");
			$this->fpdf->Cell(3, 0.7, "Student ID :", "", 0, "L");
			$this->fpdf->Cell(5.5, 0.7, $MhswID, "", 0, "C");
			$this->fpdf->Cell(2.2, 0.7, "Nama :", "", 0, "L");
			$this->fpdf->Cell(5.8, 0.7, $nama, "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $to2, "", 0, "L");
			$this->fpdf->Cell(3.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(2.2, 0.5, "(Name)", "", 0, "L");
			$this->fpdf->Cell(10.6, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$terbilang = ucwords(toTerbilang($resmain['Jumlah'])) . ' Rupiah';  
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Banyaknya", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, $terbilang, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "(The amount of)", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Check/BG/Trans./Cash", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.7, $resmain['Bank'], "", 0, "L");
			$this->fpdf->Cell(7.2, 0.7, "Tanggal Pembayaran :", "", 0, "L");
			$this->fpdf->Cell(2.8, 0.7, tgl_singkat_eng($resmain["Tanggal"]), "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "(Payment Date)", "", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $pay1, "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $pay2, "", 0, "L");
			$no = 1;
			$tot = count($result);
			$Jumlah = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$BIPOTMhswID = $r->BIPOTMhswID;
					$hasil2 = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($no == 1)
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->SetFont("Times","B",18);
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					else
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(6.5 , 0.7, "", "", 0, "L");
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					$Jumlah = $Jumlah + $r->Jumlah;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln(1.2);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5 , 1, "Jumlah / Amount", "", 0, "L");
			$this->fpdf->Cell(7 , 1, formatRupiah3($Jumlah), "LBTR", 0, "C");
			$this->fpdf->Cell(6 , 1, "", "", 0, "C");
			$this->fpdf->Cell(8 , 1, $ttd, "", 0, "C");
			$this->fpdf->Ln();
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(21.3 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(4.6 , 0.5, $pesan, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(4.6 , 1, "PROGRAM : $ProgramID - ".$resbipot['Nama'], "", 0, "L");
			$this->fpdf->Ln(3.6);
			$this->fpdf->Cell(20 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(7 , 0.5, "", "B", 0, "C");
			$this->fpdf->Output($file.$MhswID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function ptl_virtual_account_insert()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$data = array(
						'virtual_account' => $this->input->post('virtual_account'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			echo warning("Virtual account number successfully saved.","../students_payment/ptl_edit/$KHSID");
		}
		
		function ptl_bipot_create()
		{
			$this->authentification();
			$link = $this->uri->segment(3);
			$word = explode("_",$link);
			$MhswID = $word[0];
			$BIPOTID = $word[1];
			$KHSID = $word[2];
			$Sesi = $word[3];
			$cektahun = $word[4];
			$nsave = 0;
			if($BIPOTID == 0)
			{
				$this->session->set_userdata('pym_status','NOT');
				echo warning("BIPOTID has not been set.","../students_payment/ptl_edit/$KHSID");
			}
			else
			{
				if($cektahun == "")
				{
					$this->session->set_userdata('pym_status','NOT');
					echo warning("Year ID has not been set.","../students_payment/ptl_edit/$KHSID");
				}
				else
				{
					$result = $this->m_mahasiswa->PTL_select($MhswID);
					$result2 = $this->m_master->PTL_bipot_detail($BIPOTID);
					if($result2)
					{
						foreach($result2 as $r)
						{
							$nsave++;
							$BIPOTNamaID = $r->BIPOTNamaID;
							$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
							$data = array(
										'BIPOTMhswRef' => $r->BIPOT2IDRef,
										'KodeID' => 'ES01',
										'PMBID' => $result['PMBID'],
										'MhswID' => $MhswID,
										'TahunID' => $cektahun,
										'BIPOTID' => $BIPOTID,
										'BIPOT2ID' => $r->BIPOT2ID,
										'BIPOTNamaID' => $r->BIPOTNamaID,
										'TambahanNama' => $r->TambahanNama,
										'Nama' => $bipotnama['Nama'],
										'TrxID' => $r->TrxID,
										'Jumlah' => '1',
										'Besar' => $r->Jumlah,
										'Dibayar' => '0',
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_master->PTL_bipot_mhsw_insert($data);
						}
					}
					$data = array(
								'BIPOTID' => $BIPOTID,
								'Biaya' => $this->uri->segment(4),
								'Potongan' => $this->uri->segment(5),
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$smt = "";
					if($Sesi == 1){ $smt = "st"; }
					if($Sesi == 2){ $smt = "nd"; }
					if($Sesi == 3){ $smt = "rd"; }
					if($Sesi == 4){ $smt = "th"; }
					if($Sesi == 5){ $smt = "th"; }
					if($Sesi == 6){ $smt = "th"; }
					$da = array(
								'id_akun' => $MhswID,
								'nama' => $_COOKIE["nama"],
								'aktivitas' => "PAYMENT SCHEME $Sesi$smt SEMESTER: Your payment has been approved by Finance Team"
								);
					$this->m_aplikan_log->PTL_insert($da);
					$key1 = $this->encryption->encode($BIPOTID);
					$key2 = $this->encryption->encode($MhswID);
					$key3 = $this->encryption->encode($cektahun);
					$key4 = $this->encryption->encode($this->waktu);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($result['Email']);
					$this->email->cc($result['Email2']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('PAYMENT SCHEME HAS BEEN CREATED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						<b>ESMOD JAKARTA</b>: Your payment scheme $Sesi$smt semester has been created by Finance Team.
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/student_pdf_invoice/$key1/$key2/$key3/$key4/ESMOD%20JAKARTA'>PAYMENT SCHEME $MhswID$BIPOTID</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://app.esmodjakarta.com/lms'>PORTAL LMS (Learning Management System)</a>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					// echo warning("PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID." - ".$result['Nama']."' successfully created. $nsave data saved.","../students_payment/ptl_edit/$KHSID");
					if($this->email->send())
					{
						echo warning("PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID." - ".$result['Nama']."' successfully created. $nsave data saved.","../students_payment/ptl_edit/$KHSID");
					}
					else
					{
						echo warning("Email server is not active. PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID." - ".$result['Nama']."' successfully created. $nsave data saved.","../students_payment/ptl_edit/$KHSID");
					}
				}
			}
		}
		
		function ptl_bipot_delete()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			$KHSID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$Sesi = $this->uri->segment(6);
			$BIPOTID = $this->uri->segment(7);
			$data = array(
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_master->PTL_pym_bipotmhsw_update_delete($MhswID,$TahunID,$data);
			$res = $this->m_khs->PTL_select($KHSID);
			$Deposit = "";
			$pesan = "";
			if($res)
			{
				if($res['Bayar'] > 0)
				{
					$Deposit = $res['Deposit'] + $res['Bayar'];
					$pesan = '\n\nDEPOSIT '.formatRupiah3($Deposit);
					$data = array(
								'BIPOTID' => '',
								'Deposit' => $Deposit,
								'Biaya' => '',
								'Potongan' => '',
								'Bayar' => '',
								'Tutup' => 'N',
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
				}
				else
				{
					$data = array(
								'BIPOTID' => '',
								'Biaya' => '',
								'Potongan' => '',
								'Bayar' => '',
								'Tutup' => 'N',
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
				}
			}
			
			$rowrecord = $this->m_bayar->PTL_pym_all_select($MhswID,$BIPOTID);
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$BayarMhswID = $row->BayarMhswID;
					$this->m_bayar->PTL_delete($BayarMhswID);
					$this->m_bayar->PTL_delete2($BayarMhswID);
				}
			}
			
			$smt = "";
			if($Sesi == 1){ $smt = "st"; }
			if($Sesi == 2){ $smt = "nd"; }
			if($Sesi == 3){ $smt = "rd"; }
			if($Sesi == 4){ $smt = "th"; }
			if($Sesi == 5){ $smt = "th"; }
			if($Sesi == 6){ $smt = "th"; }
			$da = array(
						'id_akun' => $MhswID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => "PAYMENT SCHEME $Sesi$smt SEMESTER: Your payment scheme has been deleted by Finance Team"
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			$this->email->cc($result['Email2']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR PAYMENT SCHEME HAS BEEN DELETED BY FINANCE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>FINANCE</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your payment scheme $Sesi$smt semester has been deleted by Finance Team.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			// echo warning("PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID."' successfully deleted.".$pesan,"../students_payment/ptl_edit/$KHSID");
			if($this->email->send())
			{
				echo warning("PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID."' successfully deleted.".$pesan,"../students_payment/ptl_edit/$KHSID");
			}
			else
			{
				echo warning("Email server is not active. PAYMENT SCHEME $Sesi$smt SEMESTER with SIN '".$MhswID."' successfully deleted.","../students_payment/ptl_edit/$KHSID");
			}
		}
		
		function ptl_bipotmhs_delete()
		{
			$this->authentification();
			$KHSID = $this->uri->segment(3);
			$BIPOTMhswID = $this->uri->segment(4);
			$BIPOTNamaID = $this->uri->segment(5);
			$Dibayar = $this->uri->segment(6);
			$result = $this->m_khs->PTL_select($KHSID);
			if($Dibayar > 0)
			{
				$Deposit = $result['Deposit'] + $Dibayar;
				$data = array(
							'Deposit' => $Deposit,
							'Tutup' => 'N',
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
			}
			$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
			if($resbipot)
			{
				if($resbipot['TrxID'] == "1")
				{
					$data = array(
								'Biaya' => $result['Biaya'] - $resbipot['Besar'],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
				}
				if($resbipot['TrxID'] == "-1")
				{
					$data = array(
								'Potongan' => $result['Potongan'] - $resbipot['Besar'],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					if($resbipot['BIPOTMhswRef'] == 0)
					{
						$data = array(
									'Biaya' => $result['Biaya'] - $resbipot['Besar'],
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_khs->PTL_update($KHSID,$data);
					}
				}
			}
			$data = array(
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
			$res = $this->m_master->PTL_select($BIPOTNamaID);
			$nama = "";
			if($res)
			{
				$nama = $res['Nama'];
			}
			$res2 = $this->m_bayar->PTL_select_bipot($BIPOTMhswID);
			if($res2)
			{
				$BayarMhswID = $res2['BayarMhswID'];
				$res3 = $this->m_bayar->PTL_select($BayarMhswID);
				if($res3)
				{
					if($res2['Jumlah'] == $res3['Jumlah'])
					{
						$this->m_bayar->PTL_delete($BayarMhswID);
						$this->m_bayar->PTL_delete_bipot2($BIPOTMhswID);
					}
					else
					{
						$jum = $res3['Jumlah'] - $res2['Jumlah'];
						$data_bayar = array(
									'Jumlah' => $jum,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu,
									);
						$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
						$this->m_bayar->PTL_delete_bipot2($BIPOTMhswID);
					}
				}
			}
			echo warning("Data BIPOT with ID '$BIPOTMhswID - ".$nama."' successfully deleted.","../students_payment/ptl_edit/$KHSID");
		}
		
		function ptl_bipotmhs_cost_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$data['MhswID'] = $this->uri->segment(3);
			$BIPOTID = $this->uri->segment(4);
			$data['BIPOTID'] = $this->uri->segment(4);
			$data['KHSID'] = $this->uri->segment(5);
			$data['TahunID'] = $this->uri->segment(6);
			$MhswID = $this->uri->segment(3);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = $result['Nama'];
			$data['biaya'] = $this->m_master->PTL_pym_bipot_detail_biaya($BIPOTID);
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_edit_bipot_cost_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipotmhs_cost_insert()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			if($BIPOT2ID == "")
			{
				echo warning("Variable Name not selected.","../students_payment/ptl_bipotmhs_cost_form/$MhswID/$BIPOTID/$KHSID/$TahunID");
			}
			else
			{
				$Jumlah = str_replace(".","",$this->input->post('Jumlah'));
				
				$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$BIPOTNamaID = $result['BIPOTNamaID'];
				$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
				
				$data = array(
							'BIPOTMhswRef' => $result['BIPOT2IDRef'],
							'KodeID' => 'ES01',
							'MhswID' => $MhswID,
							'TahunID' => $TahunID,
							'BIPOTID' => $BIPOTID,
							'BIPOT2ID' => $BIPOT2ID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Nama' => $bipotnama['Nama'],
							'TrxID' => '1',
							'Jumlah' => '1',
							'Besar' => $Jumlah,
							'Dibayar' => '0',
							'login_buat' => $this->waktu,
							'tanggal_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_mhsw_insert($data);
				$reskhs = $this->m_khs->PTL_select($KHSID);
				if($reskhs)
				{
					$data = array(
								'Biaya' => $reskhs['Biaya'] + $Jumlah,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
				}
				echo warning("Data COST with CODE '$BIPOT2ID - $bipotnama[Nama]' successfully added.","../students_payment/ptl_edit/$KHSID");
			}
		}
		
		function ptl_bipotmhs_discounts_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$data['MhswID'] = $this->uri->segment(3);
			$BIPOTID = $this->uri->segment(4);
			$data['BIPOTID'] = $this->uri->segment(4);
			$data['KHSID'] = $this->uri->segment(5);
			$data['TahunID'] = $this->uri->segment(6);
			$MhswID = $this->uri->segment(3);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = $result['Nama'];
			$data['potongan'] = $this->m_master->PTL_pym_bipot_detail_potongan($BIPOTID);
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_edit_bipot_discounts_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipotmhs_discounts_insert()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			if($BIPOT2ID == "")
			{
				echo warning("Variable Name not selected.","../students_payment/ptl_bipotmhs_discounts_form/$MhswID/$BIPOTID/$KHSID/$TahunID");
			}
			else
			{
				$Jumlah = str_replace(".","",$this->input->post('Jumlah'));
				
				$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$BIPOTNamaID = $result['BIPOTNamaID'];
				$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
				
				$data = array(
							'BIPOTMhswRef' => $result['BIPOT2IDRef'],
							'KodeID' => 'ES01',
							'MhswID' => $MhswID,
							'TahunID' => $TahunID,
							'BIPOTID' => $BIPOTID,
							'BIPOT2ID' => $BIPOT2ID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Nama' => $bipotnama['Nama'],
							'TrxID' => '-1',
							'Jumlah' => '1',
							'Besar' => $Jumlah,
							'Dibayar' => '0',
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_master->PTL_bipot_mhsw_insert($data);
				$reskhs = $this->m_khs->PTL_select($KHSID);
				if($reskhs)
				{
					$data = array(
								'Potongan' => $reskhs['Potongan'] + $Jumlah,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					if(($result['BIPOT2IDRef'] == 0) OR ($result['BIPOT2IDRef'] == ""))
					{
						$rowrecord = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
						if($rowrecord)
						{
							$sisa = $Jumlah;
							$pengurangan = $Jumlah;
							foreach($rowrecord as $row)
							{
								if($row->TrxID == 1)
								{
									$Besar = $row->Besar - $row->Dibayar;
									if($Besar > 0)
									{
										if($Besar > $sisa)
										{
											$NilaiBaru = $Besar - $sisa;
										}
										else
										{
											$sisa = $sisa - $Besar;
											if($sisa >= 0)
											{
												$NilaiBaru = 0;
											}
											else
											{
												if($sisa < 0)
												{
													$NilaiBaru = $Besar;
												}
											}
										}
										$BIPOTMhswID = $row->BIPOTMhswID;
										$data = array(
													'Besar' => $NilaiBaru,
													'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
													'tanggal_buat' => $this->waktu
													);
										$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
									}
								}
							}
						}
					}
				}
				echo warning("Data PIECE with CODE '".$BIPOTID."' successfully added.","../students_payment/ptl_edit/$KHSID");
			}
		}
		
		function ptl_pdf_invoice()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$BIPOTID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$result = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"INVOICE ID $MhswID$BIPOTID",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "APL ID / PMB ID" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$id_akun." / ".$res['PMBID'] , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -1, "NAME", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -2, "PROGRAM", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1 , -2, ": ".$res['ProgramID']." - ".strtoupper($prod['Nama'])." ($res[ProdiID])", 0, "", "L");
			$this->fpdf->Ln(-0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.5 , 0.7, "#" , "T", 0, "C");
			$this->fpdf->Cell(4.5 , 0.7, "DESCRIPTION" , "T", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "AMOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "TOTAL" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "DISCOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "REMAINING" , "T", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$pot = 0;
			$bia = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "-1")
					{
						$BIPOTNamaID_detail = $hasil['BIPOTNamaIDRef'];
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						if(!$resref_detail)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							if($TambahanNama != "")
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
								$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							}
							$no++;
							$pot = $pot + $r->Besar;
						}
					}
				}
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "1")
					{
						$BIPOTNamaIDRef = $BIPOTNamaID;
						$resref = $this->m_master->PTL_select_ref($BIPOTNamaIDRef);
						$ref = "";
						if($resref)
						{
							$ref = $resref['BIPOTNamaID'];
						}
						$BIPOTNamaID_detail = $ref;
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$ref_detail = "";
						$ref_jumlah = "";
						$ref_jumlah2 = "";
						if($resref_detail)
						{
							$BIPOTNamaID = $BIPOTNamaID_detail;
							$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
							if($resref_detail2)
							{
								$ref_detail = $resref_detail2["Nama"];
								$ref_jumlah = $resref_detail2["DefJumlah"];
								$ref_jumlah2 = $resref_detail2["DefJumlah"];
							}
						}
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
						$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Dibayar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($ref_jumlah).")", "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar - $r->Dibayar - $ref_jumlah), "", 0, "R");
						if($TambahanNama != "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
						}
						$no++;
						$bia = $bia + ($r->Besar - $r->Dibayar - $ref_jumlah);
					}
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "THE TOTAL TO BE PAID", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($bia - $pot), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Ln();
			$rowbukti = $this->m_bayar->PTL_pym_all_select($MhswID,$BIPOTID);
			if($rowbukti)
			{
				$this->fpdf->SetFont("Times","",8);
				$n0 = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n0++;
					}
				}
				if($n0 > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(7.2 , 0.5, "PAYMENT HISTORY", "LBTR", 0, "C");
				}
				$this->fpdf->Ln();
				$this->fpdf->Cell(0.5 , 0.5, "#", "LBTR", 0, "C");
				$this->fpdf->Cell(1.7 , 0.5, "Date", "LBTR", 0, "C");
				$this->fpdf->Cell(2 , 0.5, "Reference", "LBTR", 0, "C");
				$this->fpdf->Cell(3 , 0.5, "Nominal", "LBTR", 0, "C");
				$n = 0;
				$total_bayar = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n++;
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $n, "LBTR", 0, "C");
						$this->fpdf->Cell(1.7 , 0.5, tgl_singkat_eng($row->Tanggal), "LBTR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $row->BayarMhswID, "LBTR", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($row->Jumlah), "LBTR", 0, "R");
						$total_bayar = $total_bayar + $row->Jumlah;
					}
				}
				if($n > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(4.2 , 0.5, "TOTAL", "LBTR", 0, "C");
					$this->fpdf->Cell(3 , 0.5, formatRupiah3($total_bayar), "LBTR", 0, "R");
				}
			}
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(4.6 , 0.5, "NB", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Pembayaran dapat di transfer ke rekening", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "An: Yayasan PDKI-BDN-ESMOD", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "No. Rekening: 218-300-3496", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Bank: Bank BCA", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Cabang: Cabang Blok A Cipete, Jakarta", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Swift Code: CENAIDA", "", 0, "L");
			$this->fpdf->Output("INVOICE_".$MhswID.$BIPOTID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function ptl_payment_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['KHSID'] = $KHSID;
			$data['MhswID'] = $MhswID;
			$data['TahunID'] = $TahunID;
			$data['Nama'] = $result['Nama'];
			$mhsw = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			}
			$data['rowrekening'] = $this->m_rekening->PTL_all_active();
			$res = $this->m_khs->PTL_select($KHSID);
			$data['deposit'] = "";
			$data['status_akhir'] = "";
			if($res)
			{
				if($res['Deposit'] == 0)
				{
					$data['deposit'] = "";
				}
				else
				{
					$data['deposit'] = $res['Deposit'];
				}
				$hasil = $res['Biaya'] - $res['Potongan'] - $res['Bayar'];
				if($hasil == 0)
				{
					$data['status_akhir'] = "<font color='green'><b>PAY OFF</b></font>";
				}
				if($hasil < 0)
				{
					$data['status_akhir'] = "<font color='purple'><b>OVER PAYMENT</b></font>";
				}
			}
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('Students_Payment/v_students_edit_payment_form',$data);
			$this->load->view('Portal/v_footer_table',$fdata);
		}
		
		function ptl_payment_insert()
		{
			$this->authentification();
			$Deposit = $this->input->post('Deposit');
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$total = $this->input->post('total');
			$pesan = "";
			$paydep = 0;
			for($i=1;$i<=$total;$i++)
			{
				$nama = $this->input->post("nama$i");
				$besar = $this->input->post("besar$i");
				$dibayar = $this->input->post("dibayar$i");
				$payment = str_replace(".","",$this->input->post("payment$i"));
				$sisa = $besar - $dibayar;
				if($payment > $sisa)
				{
					$pesan .= "Payment for '$nama' beyond the limit.".'\n'."Bill ".formatRupiah2($sisa)." - Payment ".formatRupiah2($payment).'\n\n';
				}
				if($payment < 0)
				{
					$pesan .= "Payment for '$nama' not allowed minus value.".'\n'."Payment -".formatRupiah2($payment).'\n\n';
				}
				$type = $this->input->post("type$i");
				if($type == "D")
				{
					$paydep = $paydep + $payment;
				}
			}
			if($paydep > $Deposit)
			{
				$pesan .= "Payment with DEPOSIT beyond the limit.".'\n'."Deposit ".formatRupiah2($Deposit)." - Payment ".formatRupiah2($paydep).'\n\n';
			}
			if($pesan != "")
			{
				echo warning($pesan,"../students_payment/ptl_payment_form/$KHSID/$MhswID/$TahunID");
			}
			else
			{
				$kd = $this->kd_bayar."-";
				$result = $this->m_bayar->PTL_urut_bayar($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,5,5);
				$nextnourut = $lastnourut + 1;
				$nextid = $kd.sprintf('%05s',$nextnourut);
				$res = $this->m_mahasiswa->PTL_select($MhswID);
				$Jumlah = 0;
				for($i2=1;$i2<=$total;$i2++)
				{
					$BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if(str_replace(".","",$this->input->post("payment$i2")) > 0)
					{
						$data = array(
									'Dibayar' => $resbipot['Dibayar'] + str_replace(".","",$this->input->post("payment$i2")),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						$Jumlah = $Jumlah + str_replace(".","",$this->input->post("payment$i2"));
					
						$data_bayar2 = array(
									'BayarMhswID' => $nextid,
									'BIPOTMhswID' => $BIPOTMhswID,
									'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									'Jumlah' => str_replace(".","",$this->input->post("payment$i2")),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_bayar->PTL_insert2($data_bayar2);
						$BIPOTID = $this->input->post("BIPOTID$i2");
					}
				}
				if($Jumlah == 0)
				{
					echo warning("Sorry! You do not input a nominal payment.","../students_payment/ptl_payment_form/$KHSID/$MhswID/$TahunID");
				}
				else
				{
					$pesan_akhir = "";
					if(($Deposit > 0) AND ($paydep > 0))
					{
						$resdep = $this->m_khs->PTL_select($KHSID);
						$dep = "";
						if($resdep)
						{
							$dep = $resdep['Deposit'] - $paydep;
						}
						$pesan_akhir .= '\n\nDEPOSIT USED '.formatRupiah3($paydep);
						$data = array(
									'Deposit' => $dep,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_khs->PTL_update($KHSID,$data);
					}
					$data_bayar = array(
										'BayarMhswID' => $nextid,
										'BIPOTID' => $BIPOTID,
										'TahunID' => $TahunID,
										'RekeningID' => $this->input->post('RekeningID'),
										'MhswID' => $MhswID,
										'TrxID' => 1,
										'Bank' => strtoupper($this->input->post('Bank')),
										'BuktiSetoran' => $this->input->post('BuktiSetoran'),
										'Tanggal' => $this->input->post('Tanggal'),
										'Jumlah' => $Jumlah,
										'Keterangan' => $this->input->post('Keterangan'),
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
					$this->m_bayar->PTL_insert($data_bayar);
					$res_khs = $this->m_khs->PTL_select($KHSID);
					$Bayar = $Jumlah;
					if($res_khs)
					{
						$Bayar = $res_khs['Bayar'] + $Jumlah;
					}
					$data = array(
								'Bayar' => $Bayar,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$res_tutup = $this->m_khs->PTL_select($KHSID);
					if($res_tutup)
					{
						$hasil = $res_tutup["Biaya"] - $res_tutup["Potongan"] - $res_tutup["Bayar"];
						if($hasil == 0)
						{
							$pesan_akhir .= '\n\nPAYMENT IS COMPLETE.';
							$data = array(
										'Tutup' => 'Y',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_khs->PTL_update($KHSID,$data);
						}
						if($hasil < 0)
						{
							$pesan_akhir .= '\n\nBILL PAYMENT EXCEED.';
						}
					}
					$key1 = $this->encryption->encode($nextid);
					$key2 = $this->encryption->encode($MhswID);
					$key3 = $this->encryption->encode($this->waktu);
					
					$id_akun = $res['PresenterID'];
					$resakun = $this->m_akun->PTL_select($id_akun);
					$email = "";
					$email2 = "";
					if($resakun)
					{
						$rowkhs = $this->m_khs->PTL_all_khs($MhswID);
						if(count($rowkhs) > 1)
						{
							$email = $resakun['email'];
							$email2 = $resakun['email2'];
						}
					}
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($res['Email']);
					$this->email->cc($res['Email2']);
					$this->email->bcc("$email,$email2");
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR PAYMENT DATA HAS BEEN INPUTTED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						Dear '<b>$res[Nama] - $res[ProgramID] - $res[ProdiID]</b>', your payment data has been inputted. Please check your PORTAL LMS or contact Finance Team.
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/student_key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>PAYMENT $nextid</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://app.esmodjakarta.com/lms'>PORTAL LMS (Learning Management System)</a>
						<br/>
						<br/>
						<b><font color='red'>You need to login first!</font></b>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					// echo warning("Payment data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.".$pesan_akhir,"../students_payment/ptl_edit/$KHSID");
					if($this->email->send())
					{
						echo warning("Payment data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.".$pesan_akhir,"../students_payment/ptl_edit/$KHSID");
					}
					else
					{
						echo warning("Email server is not active. Payment data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.","../students_payment/ptl_edit/$KHSID");
					}
				}
			}
		}
		
		function ptl_reminder_expired()
		{
			$this->authentification();
			$KHSID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = $result['Nama'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			$this->email->cc($result['Email2']);
			$this->email->bcc('ichaa@esmodjakarta.com,anita@esmodjakarta.com');
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR PAYMENT THIS SEMESTER HAS BEEN EXPIRED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>FINANCE</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Dear '$result[Nama] - $result[ProgramID] - $result[ProdiID]', your payment this semester has been expired. Please check your LMS or contact Finance Team.
				<br/>
				<br/>
				For more information, please login to <a href='http://lms.esmodjakarta.com'>LMS (Learning Management System)</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("REMINDER to Students ID '".$MhswID." - ".$result['Nama']." - ".$result['ProgramID']." - ".$result['ProdiID']."' successfully sent.","../students_payment/ptl_edit/$KHSID");
			}
			else
			{
				echo warning("Email server is not active. Please try again...","../students_payment/ptl_edit/$KHSID");
			}
		}
		
		function ptl_report()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$cekjurusan = $this->session->userdata('pym_report_filter_jur1');
			$data['rowkelas1'] = $this->m_kelas->PTL_filter($cekjurusan);
			$data['rowtahun1'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$cekjurusan = $this->session->userdata('pym_report_filter_jur2');
			$data['rowkelas2'] = $this->m_kelas->PTL_filter($cekjurusan);
			$data['rowtahun2'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord1'] = $this->m_khs->PTL_all_report_cari_all1();
			$data['rowrecord2'] = $this->m_khs->PTL_all_report_cari_all2();
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_report',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_search_report($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			if($this->session->userdata('pym_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('pym_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_khs->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_khs->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_khs->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_khs->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("students_payment/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students_payment/search/$cari";
			
			$cekjurusan = $this->session->userdata('pym_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_filter($cekjurusan);
			$res = $this->m_khs->PTL_all_totcostbi_all($cari);
			$data['totcost'] = $res['totcost'];
			$data['totpot'] = $res['totpot'];
			$data['totbi'] = $res['totbi'];
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_report',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_payment_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$BayarMhswID = $this->uri->segment(3);
			$data['BayarMhswID'] = $this->uri->segment(3);
			$data['KHSID'] = $this->uri->segment(4);
			$data['MhswID'] = $this->uri->segment(5);
			$TahunID = $this->uri->segment(6);
			$result = $this->m_bayar->PTL_select($BayarMhswID);
			$data['RekeningID'] = $result['RekeningID'];
			$data['Bank'] = $result['Bank'];
			$data['BuktiSetoran'] = $result['BuktiSetoran'];
			$data['Tanggal'] = $result['Tanggal'];
			$data['Keterangan'] = $result['Keterangan'];
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('Students_Payment/v_students_edit_payment_date',$data);
			$this->load->view('Portal/v_footer_table',$fdata);
		}
		
		function ptl_payment_update()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$BayarMhswID = $this->input->post('BayarMhswID');
			$data_bayar = array(
							'BuktiSetoran' => $this->input->post('BuktiSetoran'),
							'Tanggal' => $this->input->post('Tanggal'),
							'Keterangan' => $this->input->post('Keterangan'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
			$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
			echo warning("Data PAYMENT DATE successfully updated.","../students_payment/ptl_edit/$KHSID");
		}
		
		function ptl_print()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$ProgramID1 = $this->session->userdata('pym_report_filter_jur1');
			$TahunID1 = $this->session->userdata('pym_report_filter_tahun1');
			$ProdiID1 = $this->session->userdata('pym_report_filter_prodi1');
			$KelasID1 = $this->session->userdata('pym_report_filter_kelas1');
			$ProgramID2 = $this->session->userdata('pym_report_filter_jur2');
			$TahunID2 = $this->session->userdata('pym_report_filter_tahun2');
			$ProdiID2 = $this->session->userdata('pym_report_filter_prodi2');
			$KelasID2 = $this->session->userdata('pym_report_filter_kelas2');
			$rowrecord1 = $this->m_khs->PTL_all_report_cari_all1();
			$rowrecord2 = $this->m_khs->PTL_all_report_cari_all2();
			
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(28,0.7,"STUDENTS PAYMENT REPORT",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(23.5,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,29,2.5);
			$this->fpdf->Line(1,2.55,29,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "FILTER", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$ProgramID1.$TahunID1.$ProdiID1.$KelasID1.$ProgramID2.$TahunID2.$ProdiID2.$KelasID2, 0, "", "L");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.8 , 0.7, "#" , "LBTR", 0, "C");
			$this->fpdf->Cell(2.5 , 0.7, "SIN" , "BTR", 0, "C");
			$this->fpdf->Cell(7.2 , 0.7, "NAME" , "BTR", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "PRODI" , "BTR", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "CLASS" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "BILLS" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "DISCOUNTS" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "REMAINING" , "BTR", 0, "C");
			$this->fpdf->Cell(1.5 , 0.7, "STATUS" , "BTR", 0, "C");
			$no = 0;
			$tot = count($rowrecord1) + count($rowrecord2);
			$pay = 0;
			$totcost = 0;
			$totdisc = 0;
			$totpaym = 0;
			$totrema = 0;
			if($rowrecord1)
			{
				foreach($rowrecord1 as $r)
				{
					$no++;
					$MhswID = $r->MhswID;
					$res = $this->m_mahasiswa->PTL_select($MhswID);
					if($res)
					{
						$nama = $res["Nama"];
					}
					$KelasID = $r->KelasID;
					$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
					if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
					$tutup = "Pay On";
					if($r->Tutup == "Y")
					{
						$tutup = "Pay Off";
					}
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->MhswID, "BR", 0, "C");
						$this->fpdf->Cell(7.2 , 0.5, $nama, "BR", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->ProdiID, "BR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->TahunKe.$kelas, "BR", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Potongan), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Bayar), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya - $r->Potongan - $r->Bayar), "BR", 0, "R");
						$this->fpdf->Cell(1.5 , 0.5, $tutup, "BR", 0, "C");
						$pay = $pay + $r->Biaya;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->MhswID, "R", 0, "C");
						$this->fpdf->Cell(7.2 , 0.5, $nama, "R", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->ProdiID, "R", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->TahunKe.$kelas, "R", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Potongan), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Bayar), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya - $r->Potongan - $r->Bayar), "R", 0, "R");
						$this->fpdf->Cell(1.5 , 0.5, $tutup, "R", 0, "C");
						$pay = $pay + $r->Biaya;
					}
					$totcost = $totcost + $r->Biaya;
					$totdisc = $totdisc + $r->Potongan;
					$totpaym = $totpaym + $r->Bayar;
					$totrema = $totrema + ($r->Biaya - $r->Potongan - $r->Bayar);
				}
			}
			if($rowrecord2)
			{
				foreach($rowrecord2 as $r)
				{
					$no++;
					$MhswID = $r->MhswID;
					$res = $this->m_mahasiswa->PTL_select($MhswID);
					if($res)
					{
						$nama = $res["Nama"];
					}
					$KelasID = $r->KelasID;
					$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
					if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
					$tutup = "Pay On";
					if($r->Tutup == "Y")
					{
						$tutup = "Pay Off";
					}
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->MhswID, "BR", 0, "C");
						$this->fpdf->Cell(7.2 , 0.5, $nama, "BR", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->ProdiID, "BR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->TahunKe.$kelas, "BR", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Potongan), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Bayar), "BR", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya - $r->Potongan - $r->Bayar), "BR", 0, "R");
						$this->fpdf->Cell(1.5 , 0.5, $tutup, "BR", 0, "C");
						$pay = $pay + $r->Biaya;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->MhswID, "R", 0, "C");
						$this->fpdf->Cell(7.2 , 0.5, $nama, "R", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->ProdiID, "R", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->TahunKe.$kelas, "R", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Potongan), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Bayar), "R", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah4($r->Biaya - $r->Potongan - $r->Bayar), "R", 0, "R");
						$this->fpdf->Cell(1.5 , 0.5, $tutup, "R", 0, "C");
						$pay = $pay + $r->Biaya;
					}
					$totcost = $totcost + $r->Biaya;
					$totdisc = $totdisc + $r->Potongan;
					$totpaym = $totpaym + $r->Bayar;
					$totrema = $totrema + ($r->Biaya - $r->Potongan - $r->Bayar);
				}
			}
			if($no == 0)
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(28 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(14.5 , 1, "TOTAL", "LBT", 0, "C");
			$this->fpdf->Cell(3 , 1, formatRupiah2($totcost), "LBTR", 0, "R");
			$this->fpdf->Cell(3 , 1, formatRupiah2($totdisc), "BTR", 0, "R");
			$this->fpdf->Cell(3 , 1, formatRupiah2($totpaym), "BTR", 0, "R");
			$this->fpdf->Cell(3 , 1, formatRupiah2($totrema), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(22 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_eng($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(22 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(22 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, $_COOKIE["nama"], "", 0, "C");
			$this->fpdf->Output("STUDENTS_PAYMENT_REPORT_$inv".".pdf","I");
		}
		
		function ptl_filter_next_jur()
		{
			$this->authentification();
			$ProgramID = $this->input->post('ProgramID');
			$CekKHSID = $this->input->post('CekKHSID');
			if($ProgramID != "")
			{
				$this->session->set_userdata('pym_next_filter_jur',$ProgramID);
			}
			else
			{
				$this->session->unset_userdata('pym_next_filter_jur');
			}
			redirect("students_payment/ptl_register_next_semester/$CekKHSID");
		}
		
		function ptl_register_next_semester()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$result = $this->m_khs->PTL_select($KHSID);
			$MhswID = "";
			$cekjurusan = "";
			if($result)
			{
				$MhswID = $result['MhswID'];
				$cekjurusan = $result['ProgramID'];
			}
			$data['KHSID'] = $KHSID;
			$data['rowkhs'] = $this->m_khs->PTL_all_khs($MhswID);
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$TahunID = $this->session->userdata('pym_filter_register_tahun');
			$resregister = $this->m_khs->PTL_select_register($MhswID,$TahunID);
			if($resregister)
			{
				$data['notif'] = "success";
				$data['pesan'] = "ALREADY REGISTERED";
			}
			else
			{
				$resmx = $this->m_khs->PTL_select_max_max($MhswID);
				$TahunID = "";
				if($resmx)
				{
					$TahunID = $resmx['tahun'];
				}
				$resmax = $this->m_khs->PTL_select_register($MhswID,$TahunID);
				$Sesi = $resmax["Sesi"] + 1;
				$TahunKe = 1;
				if($Sesi == '1'){ $TahunKe = 1; }
				if($Sesi == '2'){ $TahunKe = 1; }
				if($Sesi == '3'){ $TahunKe = 2; }
				if($Sesi == '4'){ $TahunKe = 2; }
				if($Sesi == '5'){ $TahunKe = 3; }
				if($Sesi == '6'){ $TahunKe = 3; }
				$data['tahun'] = $this->session->userdata('pym_filter_register_tahun');
				$data['ProgramID'] = $resmax["ProgramID"];
				$data['ProdiID'] = $resmax["ProdiID"];
				$data['MhswID'] = $resmax["MhswID"];
				$data['TahunKe'] = $TahunKe;
				$data['languageID'] = $resmax["languageID"];
				$data['Spesialisasi'] = $resmax["SpesialisasiID"];
				$data['StatusMhsw'] = $resmax["StatusMhswID"];
				$data['Sesi'] = $Sesi;
				$data['MaxSKS'] = $resmax["MaxSKS"];
				$data['notif'] = "danger";
				$data['pesan'] = "NOT REGISTERED";
				$resmax = $this->m_prodi->PTL_all();
				$data['programd1'] = $this->m_prodi->PTL_all_d1();
				$data['programd3'] = $this->m_prodi->PTL_all();
				$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_edit_register_next_semester',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_register_next_semester_insert()
		{
			$this->authentification();
			$KHSID = $this->input->post('KHSID');
			$pesan = "";
			$TahunKe = $this->input->post('TahunKe');
			if(($TahunKe == "") OR ($TahunKe == 0))
			{
				$pesan .= "Class must be filled. ";
			}
			$Sesi = $this->input->post('Sesi');
			if(($Sesi == "") OR ($Sesi == 0))
			{
				$pesan .= "Semester must be filled. ";
			}
			if($pesan != "")
			{
				echo warning("$pesan","../students_payment/ptl_register_next_semester/$KHSID");
			}
			else
			{
				$MhswID = $this->input->post('MhswID');
				$ProgramID = $this->input->post('ProgramID');
				$ProdiID = $this->input->post('ProdiID');
				$languageID = $this->input->post('languageID');
				$SpesialisasiID = $this->input->post('SpesialisasiID');
				$TahunID = $this->input->post('TahunID');
				$restahun = $this->m_year->PTL_select($TahunID);
				$NamaTahun = '';
				if($restahun)
				{
					$NamaTahun = $restahun['Nama'];
				}
				$resprodi = $this->m_prodi->PTL_select($ProdiID);
				$NamaProdi = '';
				if($resprodi)
				{
					$NamaProdi = $resprodi['Nama'];
				}
				$resspc = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				$NamaSpc = '';
				if($resspc)
				{
					$NamaSpc = $resspc['Nama'];
				}
				$data_khs = array(
								'ProgramID' => $ProgramID,
								'ProdiID' => $ProdiID,
								'TahunKe' => $TahunKe,
								'languageID' => $languageID,
								'SpesialisasiID' => $SpesialisasiID,
								'StatusMhswID' => $this->input->post('StatusMhswID'),
								'Sesi' => $Sesi,
								'MhswID' => $MhswID,
								'MaxSKS' => $this->input->post('MaxSKS'),
								'TahunID' => $TahunID,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_khs->PTL_insert($data_khs);
				$resmahasiswa = $this->m_mahasiswa->PTL_select($MhswID);
				$Nama = "";
				$Email = "";
				$Email2 = "";
				if($resmahasiswa)
				{
					$Nama = $resmahasiswa['Nama'];
					$Email = $resmahasiswa['Email'];
					$Email2 = $resmahasiswa['Email2'];
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$Email");
				$this->email->cc("$Email2");
				$this->email->bcc('ichaa@esmodjakarta.com,anita@esmodjakarta.com,miha@esmodjakarta.com,anisa@esmodjakarta.com,rossy@esmodjakarta.com');
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('HAS BEEN REGISTERED FOR THE NEXT SEMESTER (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>FINANCE</h2></font>
					</center>
					<br/>
					<br/>
					We hereby inform you that student with <font color='blue'><b>SIN $MhswID, Name $Nama</b></font> has been registered for the next semester.
					<br/>
					<br/>
					<table>
						<tr>
							<td>Year</td>
							<td>:</td>
							<td>$TahunID - $NamaTahun</td>
						</tr>
						<tr>
							<td>Program ID</td>
							<td>:</td>
							<td>$ProgramID</td>
						</tr>
						<tr>
							<td>Prodi</td>
							<td>:</td>
							<td>$ProdiID - $NamaProdi</td>
						</tr>
						<tr>
							<td>Language ID</td>
							<td>:</td>
							<td>$languageID</td>
						</tr>
						<tr>
							<td>Specialization</td>
							<td>:</td>
							<td>$SpesialisasiID - $NamaSpc</td>
						</tr>
						<tr>
							<td>Semester</td>
							<td>:</td>
							<td>$Sesi</td>
						</tr>
					</table>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("Students have been enrolled into the next semester.","../students_payment/ptl_register_next_semester/$KHSID");
				}
				else
				{
					echo warning("Email server is not active. Students have been enrolled into the next semester.","../students_payment/ptl_register_next_semester/$KHSID");
				}
			}
		}
		
		function ptl_correction_this_semester()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$result = $this->m_khs->PTL_select($KHSID);
			$MhswID = $result['MhswID'];
			$data['TahunID'] = $result['TahunID'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['Sesi'] = $result['Sesi'];
			$data['suspend'] = $result['suspend'];
			$data['KHSID'] = $KHSID;
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $resmhsw['MhswID'];
			$data['Foto'] = $resmhsw['Foto'];
			$data['Nama'] = $resmhsw['Nama'];
			$data['ProgramID'] = $resmhsw['ProgramID'];
			$data['ProdiID'] = $resmhsw['ProdiID'];
			$data['alasan_suspend'] = $resmhsw['alasan_suspend'];
			$data['rowtahun'] = '';
			if($resmhsw['ProgramID'] == 'REG')
			{
				$data['rowtahun'] = $this->m_year->PTL_all_d3();
			}
			if($resmhsw['ProgramID'] == 'INT')
			{
				$data['rowtahun'] = $this->m_year->PTL_all_d1();
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_edit_correction_this_semester',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_correction_this_semester_update()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$KHSID = $this->input->post('KHSID');
			$suspend = $this->input->post('suspend');
			$alasan_suspend = $this->input->post('alasan_suspend');
			if(($suspend == "Y") AND ($alasan_suspend == ""))
			{
				echo warning("Sorry, please fill 'Messages To Student'. Try again...","../students_payment/ptl_correction_this_semester/$KHSID");
			}
			else
			{
				$data = array(
							'suspend' => $suspend,
							'alasan_suspend' => $alasan_suspend,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				if($_COOKIE["id_akun"] == "00001111")
				{
					$OldTahunID = $this->input->post('OldTahunID');
					$TahunID = $this->input->post('TahunID');
					$data = array(
								'TahunID' => $TahunID,
								'TahunKe' => $this->input->post('TahunKe'),
								'Sesi' => $this->input->post('Sesi'),
								'suspend' => $suspend,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$data = array(
								'TahunID' => $TahunID,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_master->PTL_correction_update($MhswID,$OldTahunID,$data);
					$data = array(
								'TahunID' => $TahunID,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_bayar->PTL_correction_update($MhswID,$OldTahunID,$data);
					echo warning("Your data has been changed.","../students_payment/ptl_edit/$KHSID");
				}
				else
				{
					$username = $this->input->post('username');
					$password = $this->input->post('password');
					if($_COOKIE["id_akun"] == $username)
					{
						echo warning("Sorry, you can not use your username. Please contact your superior.","../students_payment/ptl_correction_this_semester/$KHSID");
					}
					else
					{
						$auth = $this->m_akses->PTL_select_auth($username,$password);
						if($auth)
						{
							$OldTahunID = $this->input->post('OldTahunID');
							$TahunID = $this->input->post('TahunID');
							$data = array(
										'TahunID' => $TahunID,
										'TahunKe' => $this->input->post('TahunKe'),
										'Sesi' => $this->input->post('Sesi'),
										'suspend' => $suspend,
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_khs->PTL_update($KHSID,$data);
							$data = array(
										'TahunID' => $TahunID,
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_master->PTL_correction_update($MhswID,$OldTahunID,$data);
							$data = array(
										'TahunID' => $TahunID,
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_bayar->PTL_correction_update($MhswID,$OldTahunID,$data);
							$datalog = array(
										'pk1' => $KHSID,
										'pk2' => $MhswID,
										'id_akun' => $_COOKIE["id_akun"],
										'nama' => $_COOKIE["nama"],
										'aplikasi' => 'FINANCE',
										'menu' => $this->uri->segment(1),
										'submenu' => $this->uri->segment(2),
										'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
										'kode_halaman' => "SPYM01-COR01",
										'aktifitas' => $this->input->post('alasan'),
										'data' => "no data",
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_aktifitas->PTL_insert($datalog);
							echo warning("Your data has been changed.","../students_payment/ptl_edit/$KHSID");
						}
						else
						{
							$auth2 = $this->m_akses->PTL_select_user($username,$password);
							if($auth2)
							{
								echo warning("Sorry, your superior must be in the same department.","../students_payment/ptl_correction_this_semester/$KHSID");
							}
							else
							{
								echo warning("Username or Password incorrect.","../students_payment/ptl_correction_this_semester/$KHSID");
							}
						}
					}
				}
			}
		}
		
		function ptl_reset()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$KHSID = $this->uri->segment(3);
			$result = $this->m_khs->PTL_select($KHSID);
			$data['KHSID'] = $result['KHSID'];
			$data['MhswID'] = $result['MhswID'];
			$data['TahunID'] = $result['TahunID'];
			$MhswID = $result['MhswID'];
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = $res['Nama'];
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_reset',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_reset_insert()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$Nama = $this->input->post('Nama');
			$KHSID = $this->input->post('KHSID');
			$TahunID = $this->input->post('TahunID');
			$reason = $this->input->post('reason');
			$data = array(
						'Deposit' => 0,
						'Biaya' => 0,
						'Potongan' => 0,
						'Bayar' => 0,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_khs->PTL_update($KHSID,$data);
			$this->m_master->PTL_pym_bipotmhsw_delete($MhswID,$TahunID);
			$rowbayar = $this->m_bayar->PTL_pym_all_select_tahun($MhswID,$TahunID);
			if($rowbayar)
			{
				foreach($rowbayar as $row)
				{
					$BayarMhswID = $row->BayarMhswID;
					$this->m_bayar->PTL_delete2($BayarMhswID);
					$this->m_bayar->PTL_delete($BayarMhswID);
				}
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to('ichaa@esmodjakarta.com');
			$this->email->cc('anita@esmodjakarta.com');
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('PAYMENT DATA HAS BEEN RESET (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>FINANCE</h2></font>
				</center>
				<br/>
				We hereby inform you that student with <font color='blue'><b>SIN $MhswID, Name $Nama</b></font> <b>PAYMENT DATA</b> was reset permanently by ".$_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"].".
				<br/>
				<font color='red'><b>Reason</b></font> : $reason
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("Payment data has been reset.","../students_payment/ptl_edit/$KHSID");
			}
			else
			{
				echo warning("Email server is not active. Payment data has been reset.","../students_payment/ptl_edit/$KHSID");
			}
		}
		
		function ptl_correction_cost()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$BIPOTMhswID = $this->uri->segment(3);
			$data['BIPOTMhswID'] = $BIPOTMhswID;
			$res = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
			$data['Besar'] = formatRupiah2($res['Besar']);
			$data['Dibayar'] = formatRupiah2($res['Dibayar']);
			$data['login_buat'] = $res['login_buat'];
			$data['tanggal_buat'] = $res['tanggal_buat'];
			$data['login_edit'] = $res['login_edit'];
			$data['tanggal_edit'] = $res['tanggal_edit'];
			$MhswID = $this->uri->segment(4);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $result['MhswID'];
			$data['Nama'] = $result['Nama'];
			$data['KHSID'] = $this->uri->segment(5);
			$this->load->view('Portal/v_header_master');
			$this->load->view('Students_Payment/v_students_edit_correction_cost',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_correction_cost_update()
		{
			$this->authentification();
			$BIPOTMhswID = $this->input->post('BIPOTMhswID');
			$MhswID = $this->input->post('MhswID');
			$KHSID = $this->input->post('KHSID');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if($_COOKIE["id_akun"] == $username)
			{
				echo warning("Sorry, you can not use your username. Please contact your superior.","../students_payment/ptl_correction_cost/$BIPOTMhswID/$MhswID/$KHSID");
			}
			else
			{
				$auth = $this->m_akses->PTL_select_auth($username,$password);
				if($auth)
				{
					$Besar = $this->input->post('Besar');
					$Dibayar = $this->input->post('Dibayar');
					$alasan = $this->input->post('alasan');
					$data = array(
								'Besar' => str_replace(".","",$Besar),
								'Dibayar' => str_replace(".","",$Dibayar),
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
					$datalog = array(
								'pk1' => $BIPOTMhswID,
								'pk2' => $MhswID,
								'pk3' => $KHSID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => 'FINANCE',
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "SPYM01-COR02",
								'aktifitas' => $alasan,
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("Your data has been changed.","../students_payment/ptl_edit/$KHSID");
				}
				else
				{
					$auth2 = $this->m_akses->PTL_select_user($username,$password);
					if($auth2)
					{
						echo warning("Sorry, your superior must be in the same department.","../students_payment/ptl_correction_cost/$BIPOTMhswID/$MhswID/$KHSID");
					}
					else
					{
						echo warning("Username or Password incorrect.","../students_payment/ptl_correction_cost/$BIPOTMhswID/$MhswID/$KHSID");
					}
				}
			}
		}
	}
?>