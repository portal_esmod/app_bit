<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Students extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_catatan2');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_prodi');
			$this->load->model('m_status');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_tahun_masuk()
		{
			$this->authentification();
			$cektahunmasuk = $this->input->post('cektahunmasuk');
			if($cektahunmasuk != "")
			{
				$this->session->set_userdata('students_filter_tahun_masuk',$cektahunmasuk);
			}
			else
			{
				$this->session->unset_userdata('students_filter_tahun_masuk');
			}
			redirect("students");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('students_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('students_filter_jur');
			}
			redirect("students");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('students_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('students_filter_prodi');
			}
			redirect("students");
		}
		
		function ptl_filter_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('students_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->unset_userdata('students_filter_marketing');
			}
			redirect("students");
		}
		
		function ptl_filter_no()
		{
			$this->authentification();
			$cekno = $this->input->post('cekno');
			if($cekno != "")
			{
				$this->session->set_userdata('students_filter_no',$cekno);
			}
			else
			{
				$this->session->unset_userdata('students_filter_no');
			}
			redirect("students");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students');
			if($this->session->userdata('students_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('students_filter_no');
			}
			$uri_segment = 3;
			$offset = $this->uri->segment($uri_segment);
			$data['cariproduk'] = $this->m_mahasiswa->PTL_all_cari_all($cekno,$offset);
			$jml = $this->m_mahasiswa->PTL_all_cari_jumlah_all();
			$data['cek'] = $this->m_mahasiswa->PTL_all_cari_cek_all();
			$data['total'] = count($this->m_mahasiswa->PTL_all_cari_total_all());
			$this->load->library('pagination');
			$config['base_url'] = site_url("students/index");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students/index";
			
			$data['rowtahun'] = $this->m_mahasiswa->PTL_tahun();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['pencarian'] = '';
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function search($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','students');
			if($this->session->userdata('students_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('students_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_mahasiswa->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_mahasiswa->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_mahasiswa->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_mahasiswa->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("students/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students/search/$cari";
			
			$data['rowtahun'] = $this->m_mahasiswa->PTL_tahun();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students');
			$MhswID = $this->uri->segment(3);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result)
			{
				$data['MhswID'] = $result['MhswID'];
				$data['Nama'] = $result['Nama'];
				$data['ProgramID'] = $result['ProgramID'];
				$data['ProdiID'] = $result['ProdiID'];
				$data['PresenterID'] = $result['PresenterID'];
				$data['Foto'] = $result['Foto'];
				$data['TempatLahir'] = $result['TempatLahir'];
				$data['TanggalLahir'] = $result['TanggalLahir'];
				$data['Kelamin'] = $result['Kelamin'];
				$data['WargaNegara'] = $result['WargaNegara'];
				$data['Agama'] = $result['Agama'];
				$data['StatusSipil'] = $result['StatusSipil'];
				$data['Alamat'] = $result['Alamat'];
				$data['RT'] = $result['RT'];
				$data['RW'] = $result['RW'];
				$data['Kota'] = $result['Kota'];
				$data['Propinsi'] = $result['Propinsi'];
				$data['KodePos'] = $result['KodePos'];
				$data['Negara'] = $result['Negara'];
				$data['Telepon'] = $result['Telepon'];
				$data['Handphone'] = $result['Handphone'];
				$data['Email'] = $result['Email'];
				$data['Email2'] = $result['Email2'];
				$data['NamaAyah'] = $result['NamaAyah'];
				$data['AlamatOrtu'] = $result['AlamatOrtu'];
				$data['KotaOrtu'] = $result['KotaOrtu'];
				$data['KodePosOrtu'] = $result['KodePosOrtu'];
				$data['TeleponOrtu'] = $result['TeleponOrtu'];
				$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
				$data['EmailOrtu'] = $result['EmailOrtu'];
				$data['NamaIbu'] = $result['NamaIbu'];
				$data['TeleponIbu'] = $result['TeleponIbu'];
				$data['HandphoneIbu'] = $result['HandphoneIbu'];
				$data['EmailIbu'] = $result['EmailIbu'];
				$data['StatusMhswID'] = $result['StatusMhswID'];
				$data['tanggal_lulus'] = $result['tanggal_lulus'];
				$data['rowstatus'] = $this->m_status->PTL_all();
				$data['rowcatatan'] = $this->m_catatan2->PTL_all_select($MhswID);
				$this->load->view('Portal/v_header');
				$this->load->view('Students/v_students_edit',$data);
				$this->load->view('Portal/v_footer');
			}
			else
			{
				echo warning("Data with ID '$MhswID' does not exist in the database.","../students");
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$StatusMhswID = $this->input->post('StatusMhswID');
			$resstatus = $this->m_status->PTL_select($StatusMhswID);
			$NamaStatus = '';
			if($resstatus)
			{
				$NamaStatus = $resstatus['Nama'];
			}
			$data = array(
						'StatusMhswID' => $StatusMhswID,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			if($StatusMhswID != "A")
			{
				$resmax = $this->m_khs->PTL_select_max($MhswID);
				$TahunID = $resmax['tahun'];
				$result2 = $this->m_khs->PTL_select_register($MhswID,$TahunID);
				$KHSID = $result2['KHSID'];
				$Biaya = $result2['Biaya'];
				$Potongan = $result2['Potongan'];
				$Bayar = $result2['Bayar'];
				$AkhirPotongan = $Potongan + ($Biaya - $Potongan - $Bayar);
				$data = array(
							'StatusMhswID' => $StatusMhswID,
							'Potongan' => $AkhirPotongan,
							'Tutup' => 'Y',
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
				$rowrecord = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
				if($rowrecord)
				{
					foreach($rowrecord as $row)
					{
						$BIPOTMhswID = $row->BIPOTMhswID;
						if($row->Besar != $row->Dibayar)
						{
							if($row->TrxID != "-1")
							{
								
								$MhswID = $row->MhswID;
								$BIPOTMhswRef = $row->BIPOT2ID;
								$TahunID = $row->TahunID;
								$rowmhsw = $this->m_master->PTL_pym_detail_ref_cek_aplikan_otw($MhswID,$BIPOTMhswRef,$TahunID);
								$BesarDiskon = 0;
								if($rowmhsw)
								{
									foreach($rowmhsw as $rmhsw)
									{
										$BesarDiskon = $BesarDiskon + $rmhsw->Besar;
									}
								}
								$AkhirPotonganBpt = $row->Besar - $row->Dibayar;
								$TambahanNama = $row->TambahanNama."_-_Adjustment ".formatRupiah3($AkhirPotonganBpt)." (Status : $NamaStatus)";
								$data = array(
											'TambahanNama' => $TambahanNama,
											'Besar' => $row->Dibayar + $BesarDiskon,
											'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_edit' => $this->waktu
											);
								$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
							}
						}
					}
				}
			}
			echo warning("Your data successfully updated.","../students/ptl_edit/$MhswID");
		}
	}
?>