<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Login extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_verifikasi = gmdate("dis", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('auth');
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_catatan');
			$this->load->model('m_khs');
			$this->load->model('m_log');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			@$is_login_finance = $this->session->userdata('is_login_finance');
			if($is_login_finance == "notlogged")
			{
				$this->session->unset_userdata('is_login_finance');
				setcookie("is_login_finance", "", time()+1);
				setcookie("is_login_finance", "", time()+1, "/");
				echo warning('You are not logged in.','../login');
			}
			@$cis_login_app = $_COOKIE["is_login_app"];
			@$cis_login_finance = $_COOKIE["is_login_finance"];
			if(($cis_login_app == "logged") AND ($cis_login_finance == "logged"))
			{
				redirect('login/ptl_home');
			}
			else
			{
				if($cis_login_finance == "logged")
				{
					echo warning('Sorry, it looks like you are already logged ...','../login/ptl_home');
				}
			}
			$this->load->view('v_login');
		}
		
		function ptl_do_login()
		{
			$this->authentification();
			$remember = $this->input->post('remember');
			if($remember == "7")
			{
				$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
				$recaptcha = $this->input->post('g-recaptcha-response');
				$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
				$response = @file_get_contents($api_url);
				$datacaptcha = json_decode($response, true);
				if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
				{
					$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
					if ($this->form_validation->run() == FALSE)
					{
						$this->index();
					}
					else
					{
						$id_akun = $this->input->post('username');
						$NamaAkun = "Unknown ($id_akun)";
						$email = "";
						$AkunNa = "Y";
						$AksesNa = "Y";
						$resakun = $this->m_akun->PTL_select($id_akun);
						if($resakun)
						{
							$NamaAkun = $resakun['nama'];
							$email = $resakun['email'];
							$AkunNa = $resakun['na'];
						}
						$resakses = $this->m_akses->PTL_select_akun($id_akun);
						if($resakses)
						{
							$AksesNa = $resakses['na'];
						}
						if(($AkunNa == "N") AND ($AkunNa == "N"))
						{
							$login = array(
										'username'=>$this->input->post('username'),
										'password'=>$this->input->post('password')
										);
							$return = $this->auth->do_login_remember($login);
							$h = "-7";
							$hm = $h * 60;
							$ms = $hm * 60;
							$ip_client = $this->log->getIpAdress();
							$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$os_client = $this->log->getOs();
							$browser_client = $this->log->getBrowser();
							$perangkat_client = $this->log->getPerangkat();
							$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
							$tanggal = gmdate("Y-m-d", time()-($ms));
							$jam = gmdate("H:i:s", time()-($ms));
							$id_akses = from_session('id_akses');
							if($return)
							{
								$word = explode("-",$tanggal);
								$dataTime = mktime(0,0,0,$word[1],$word[2]+7,$word[0]);
								$tanggal_pengingat = date("Y-m-d",$dataTime)." ".$jam;
								$data = array(
											'tanggal_pengingat' => $tanggal_pengingat,
											'verifikasi' => 'N',
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'last_login' => $waktu
											);
								$this->m_akses->PTL_update($id_akses,$data);
								
								$datadir = array(
											'id_akun' => from_session('id_akun'),
											'id_akses' => $id_akses,
											'id_cabang' => from_session('id_cabang'),
											'in' => $this->input->post('username'),
											'nama' => from_session('nama'),
											'divisi' => from_session('divisi'),
											'jabatan' => from_session('jabatan'),
											'akses' => from_session('akses'),
											'aplikasi' => 'FINANCE',
											'tanggal' => $tanggal,
											'jam' => $jam,
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'status' => 'LOGIN'
											);
								$this->m_log->PTL_input($datadir);
								
								$username = $this->input->post('username');
								$password = $this->input->post('password');
								$query = $this->m_akses->PTL_select_apl($username,$password);
								foreach($query->result() as $row)
								{
									$aplikasi = $row->aplikasi;
								}
								$wordd = explode("_", @$aplikasi);
								for($i=0;$i<20;$i++)
								{
									if(@$wordd[$i] == "ac"){$ac = "logged";}
									if(@$wordd[$i] == "ad"){$ad = "logged";}
									if(@$wordd[$i] == "al"){$al = "logged";}
									if(@$wordd[$i] == "ap"){$ap = "logged";}
									if(@$wordd[$i] == "as"){$as = "logged";}
									if(@$wordd[$i] == "at"){$at = "logged";}
									if(@$wordd[$i] == "bs"){$bs = "logged";}
									if(@$wordd[$i] == "ca"){$ca = "logged";}
									if(@$wordd[$i] == "cr"){$cr = "logged";}
									if(@$wordd[$i] == "dv"){$dv = "logged";}
									if(@$wordd[$i] == "fb"){$fb = "logged";}
									if(@$wordd[$i] == "fn"){$fn = "logged";}
									if(@$wordd[$i] == "ft"){$ft = "logged";}
									if(@$wordd[$i] == "hd"){$hd = "logged";}
									if(@$wordd[$i] == "hr"){$hr = "logged";}
									if(@$wordd[$i] == "jb"){$jb = "logged";}
									if(@$wordd[$i] == "lb"){$lb = "logged";}
									if(@$wordd[$i] == "lm"){$lm = "logged";}
									if(@$wordd[$i] == "ml"){$ml = "logged";}
									if(@$wordd[$i] == "qr"){$qr = "logged";}
								}
								if(@$fn == "logged")
								{
									$ip_client = $this->log->getIpAdress();
									$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
									$os_client = $this->log->getOs();
									$browser_client = $this->log->getBrowser();
									$perangkat_client = $this->log->getPerangkat();
									
									$this->load->library('email');
									$config = array();
									$config['charset'] = 'utf-8';
									$config['useragent'] = 'Codeigniter';
									$config['protocol']= "smtp";
									$config['mailtype']= "html";
									$config['smtp_host']= "mail.esmodjakarta.com";
									$config['smtp_port']= "25";
									$config['smtp_timeout']= "5";
									$config['smtp_user']= "no-reply@esmodjakarta.com";
									$config['smtp_pass']= "noreplyesmod";
									$config['crlf']="\r\n"; 
									$config['newline']="\r\n"; 
									$config['wordwrap'] = TRUE;
									$this->email->initialize($config);
									$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
									$this->email->to("$email");
									$this->email->bcc('lendra.permana@gmail.com');
									$this->email->subject('FINANCE - REMEMBER LOGGED IN FOR 7 DAYS (NO REPLY)');
									$this->email->message("
										<center>
											<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
											<font color='red'><h2>FINANCE</h2></font>
										</center>
										<br/>
										Dear <b>".strtoupper($NamaAkun)."</b>,
										<br/>
										<br/>
										This email want to inform you that you have logged in for the next 7 days in Finance Application.
										<br/>
										Contact your administrator immediately if you do not perform this action.
										<br/>
										<br/>
										<br/>
										Thanks,
										<br/>
										<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
										<center>
											<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
											Ip: $ip_client
											<br/>
											Hostname: $hostname_client
											<br/>
											OS: $os_client
											<br/>
											Browser: $browser_client
											<br/>
											Devices: $perangkat_client
										</center>
									");
									if($this->email->send())
									{
										echo warning('Welcome '.from_session('nama').'\n'.
												'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
												'Login Now  :  '.$waktu.'\n'.
												'Last Online  :  '.from_session('in').'\n'.
												'Last Logout :  '.from_session('out'),'../login/ptl_home');
									}
									else
									{
										echo warning('Welcome '.from_session('nama').'\n'.
												'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
												'Login Now  :  '.$waktu.'\n'.
												'Last Online  :  '.from_session('in').'\n'.
												'Last Logout :  '.from_session('out').' Code: ESNA.','../login/ptl_home');
									}
								}
								else
								{
									echo warning('Sorry, you do not have access to this application ...','../login/ptl_logout');
								}
							}
							else
							{
								$datadir = array(
											'id_akun' => "",
											'id_akses' => @$id_akses,
											'id_cabang' => "",
											'in' => $this->input->post('username'),
											'aplikasi' => 'FINANCE',
											'tanggal' => $tanggal,
											'jam' => $jam,
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'status' => 'LOGIN FAILED'
											);
								$this->m_log->PTL_input($datadir);
								echo warning('Sorry, username or password you entered is incorrect ...','../login');
							}
						}
						else
						{
							$ip_client = $this->log->getIpAdress();
							$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$os_client = $this->log->getOs();
							$browser_client = $this->log->getBrowser();
							$perangkat_client = $this->log->getPerangkat();
							
							$this->load->library('email');
							$config = array();
							$config['charset'] = 'utf-8';
							$config['useragent'] = 'Codeigniter';
							$config['protocol']= "smtp";
							$config['mailtype']= "html";
							$config['smtp_host']= "mail.esmodjakarta.com";
							$config['smtp_port']= "25";
							$config['smtp_timeout']= "5";
							$config['smtp_user']= "no-reply@esmodjakarta.com";
							$config['smtp_pass']= "noreplyesmod";
							$config['crlf']="\r\n"; 
							$config['newline']="\r\n"; 
							$config['wordwrap'] = TRUE;
							$this->email->initialize($config);
							$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
							$this->email->to('wprasasti@esmodjakarta.com');
							$this->email->bcc('lendra.permana@gmail.com');
							$this->email->subject('FAILED LOGIN ATTEMPTS (NO REPLY)');
							$this->email->message("
								<center>
									<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
									<font color='red'><h2>FINANCE</h2></font>
								</center>
								<br/>
								<font color='red'><b>".strtoupper($NamaAkun)."</b></font> has resigned trying to login finance application.
								<br/>
								<br/>
								If you receive this email is more than one, then the user is trying to attack your finance application.
								<br/>
								<br/>
								Data :
								<br/>
								<table>
									<tr>
										<td>Username Input</td>
										<td>:</td>
										<td>".$this->input->post('username')."</td>
									</tr>
									<tr>
										<td>Password Input</td>
										<td>:</td>
										<td>".$this->input->post('password')."</td>
									</tr>
								</table>
								<br/>
								<br/>
								<br/>
								Thanks,
								<br/>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
								<center>
									<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
									Ip: $ip_client
									<br/>
									Hostname: $hostname_client
									<br/>
									OS: $os_client
									<br/>
									Browser: $browser_client
									<br/>
									Devices: $perangkat_client
								</center>
							");
							if($this->email->send())
							{
								echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta.','../login');
							}
							else
							{
								echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta. Code: ESNA.','../login');
							}
						}
					}
				}
				else
				{
					echo warning('Please complete the form and captcha.','../login');
				}
			}
			else
			{
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$id_akun = $this->input->post('username');
					$NamaAkun = "Unknown ($id_akun)";
					$AkunNa = "Y";
					$AksesNa = "Y";
					$resakun = $this->m_akun->PTL_select($id_akun);
					if($resakun)
					{
						$NamaAkun = $resakun['nama'];
						$AkunNa = $resakun['na'];
					}
					$resakses = $this->m_akses->PTL_select_akun($id_akun);
					if($resakses)
					{
						$AksesNa = $resakses['na'];
					}
					if(($AkunNa == "N") AND ($AkunNa == "N"))
					{
						$login = array(
									'username'=>$this->input->post('username'),
									'password'=>$this->input->post('password')
									);
						$return = $this->auth->do_login($login);
						$h = "-7";
						$hm = $h * 60;
						$ms = $hm * 60;
						$ip_client = $this->log->getIpAdress();
						$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
						$os_client = $this->log->getOs();
						$browser_client = $this->log->getBrowser();
						$perangkat_client = $this->log->getPerangkat();
						$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
						$tanggal = gmdate("Y-m-d", time()-($ms));
						$jam = gmdate("H:i:s", time()-($ms));
						$id_akses = from_session('id_akses');
						if($return)
						{
							$data = array(
										'tanggal_pengingat' => '',
										'verifikasi' => 'N',
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'last_login' => $waktu
										);
							$this->m_akses->PTL_update($id_akses,$data);
							
							$datadir = array(
										'id_akun' => from_session('id_akun'),
										'id_akses' => $id_akses,
										'id_cabang' => from_session('id_cabang'),
										'in' => $this->input->post('username'),
										'nama' => from_session('nama'),
										'divisi' => from_session('divisi'),
										'jabatan' => from_session('jabatan'),
										'akses' => from_session('akses'),
										'aplikasi' => 'FINANCE',
										'tanggal' => $tanggal,
										'jam' => $jam,
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'status' => 'LOGIN'
										);
							$this->m_log->PTL_input($datadir);
							
							$username = $this->input->post('username');
							$password = $this->input->post('password');
							$query = $this->m_akses->PTL_select_apl($username,$password);
							foreach($query->result() as $row)
							{
								$aplikasi = $row->aplikasi;
							}
							$wordd = explode("_", @$aplikasi);
							for($i=0;$i<20;$i++)
							{
								if(@$wordd[$i] == "ac"){$ac = "logged";}
								if(@$wordd[$i] == "ad"){$ad = "logged";}
								if(@$wordd[$i] == "al"){$al = "logged";}
								if(@$wordd[$i] == "ap"){$ap = "logged";}
								if(@$wordd[$i] == "as"){$as = "logged";}
								if(@$wordd[$i] == "at"){$at = "logged";}
								if(@$wordd[$i] == "bs"){$bs = "logged";}
								if(@$wordd[$i] == "ca"){$ca = "logged";}
								if(@$wordd[$i] == "cr"){$cr = "logged";}
								if(@$wordd[$i] == "dv"){$dv = "logged";}
								if(@$wordd[$i] == "fb"){$fb = "logged";}
								if(@$wordd[$i] == "fn"){$fn = "logged";}
								if(@$wordd[$i] == "ft"){$ft = "logged";}
								if(@$wordd[$i] == "hd"){$hd = "logged";}
								if(@$wordd[$i] == "hr"){$hr = "logged";}
								if(@$wordd[$i] == "jb"){$jb = "logged";}
								if(@$wordd[$i] == "lb"){$lb = "logged";}
								if(@$wordd[$i] == "lm"){$lm = "logged";}
								if(@$wordd[$i] == "ml"){$ml = "logged";}
								if(@$wordd[$i] == "qr"){$qr = "logged";}
							}
							if(@$fn == "logged")
							{
								echo warning('Welcome '.from_session('nama').'\n'.
										'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
										'Login Now  :  '.$waktu.'\n'.
										'Last Online  :  '.from_session('in').'\n'.
										'Last Logout :  '.from_session('out'),'../login/ptl_home');
							}
							else
							{
								echo warning('Sorry, you do not have access to this application ...','../login/ptl_logout');
							}
						}
						else
						{
							$datadir = array(
										'id_akun' => "",
										'id_akses' => @$id_akses,
										'id_cabang' => "",
										'in' => $this->input->post('username'),
										'aplikasi' => 'FINANCE',
										'tanggal' => $tanggal,
										'jam' => $jam,
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'status' => 'LOGIN FAILED'
										);
							$this->m_log->PTL_input($datadir);
							echo warning('Sorry, username or password you entered is incorrect ...','../login');
						}
					}
					else
					{
						$ip_client = $this->log->getIpAdress();
						$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
						$os_client = $this->log->getOs();
						$browser_client = $this->log->getBrowser();
						$perangkat_client = $this->log->getPerangkat();
						
						$this->load->library('email');
						$config = array();
						$config['charset'] = 'utf-8';
						$config['useragent'] = 'Codeigniter';
						$config['protocol']= "smtp";
						$config['mailtype']= "html";
						$config['smtp_host']= "mail.esmodjakarta.com";
						$config['smtp_port']= "25";
						$config['smtp_timeout']= "5";
						$config['smtp_user']= "no-reply@esmodjakarta.com";
						$config['smtp_pass']= "noreplyesmod";
						$config['crlf']="\r\n"; 
						$config['newline']="\r\n"; 
						$config['wordwrap'] = TRUE;
						$this->email->initialize($config);
						$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
						$this->email->to('wprasasti@esmodjakarta.com');
						$this->email->bcc('lendra.permana@gmail.com');
						$this->email->subject('FAILED LOGIN ATTEMPTS (NO REPLY)');
						$this->email->message("
							<center>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
								<font color='red'><h2>FINANCE</h2></font>
							</center>
							<br/>
							<font color='red'><b>".strtoupper($NamaAkun)."</b></font> has resigned trying to login finance application.
							<br/>
							<br/>
							If you receive this email is more than one, then the user is trying to attack your finance application.
							<br/>
							<br/>
							Data :
							<br/>
							<table>
								<tr>
									<td>Username Input</td>
									<td>:</td>
									<td>".$this->input->post('username')."</td>
								</tr>
								<tr>
									<td>Password Input</td>
									<td>:</td>
									<td>".$this->input->post('password')."</td>
								</tr>
							</table>
							<br/>
							<br/>
							<br/>
							Thanks,
							<br/>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
							<center>
								<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
								Ip: $ip_client
								<br/>
								Hostname: $hostname_client
								<br/>
								OS: $os_client
								<br/>
								Browser: $browser_client
								<br/>
								Devices: $perangkat_client
							</center>
						");
						if($this->email->send())
						{
							echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta.','../login');
						}
						else
						{
							echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta. Code: ESNA.','../login');
						}
					}
				}
			}
		}
		
		function ptl_forgot()
		{
			$this->load->view('v_forgot');
		}
		
		function ptl_lupa_password()
		{
			$this->authentification();
			$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
			$recaptcha = $this->input->post('g-recaptcha-response');
			$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
			$response = @file_get_contents($api_url);
			$datacaptcha = json_decode($response, true);
			if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
			{
				$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$h = "-7";
					$hm = $h * 60; 
					$ms = $hm * 60;
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
					$tanggal = gmdate("Y-m-d", time()-($ms));
					$jam = gmdate("H:i:s", time()-($ms));
					$tgl = gmdate("d M Y", time()-($ms));
					$email = $this->input->post('email');
					$return = $this->m_akses->PTL_email($email);
					if($return)
					{
						$datadir = array(
									'id_akun' => $return['id_akun'],
									'id_akses' => $return['id_akses'],
									'id_cabang' => $return['id_cabang'],
									'nama' => $return['nama'],
									'divisi' => $return['divisi'],
									'jabatan' => $return['jabatan'],
									'akses' => $return['akses'],
									'aplikasi' => 'FINANCE',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD'
									);
						$this->m_log->PTL_input($datadir);
						
						$datacat = array(
										'jenis' => 'MAINTENANCE',
										'aplikasi' => 'FINANCE',
										'dari' => $return['nama'],
										'dr_divisi' => $return['divisi'],
										'dr_jabatan' => $return['jabatan'],
										'pada' => 'LENDRA PERMANA',
										'pd_divisi' => 'IT',
										'pd_jabatan' => 'DEVELOPER',
										'masalah' => 'Forgot Password',
										'solusi' => 'Reset Password in Database',
										'tanggal_posting' => $waktu,
										'tanggal_done' => $waktu,
										'status' => 'DONE'
										);
						$this->m_catatan->PTL_input($datacat);
						
						$id_akses = $return['id_akses'];
						$pass = gmdate("is", time()-($ms));
						$data_reset = array(
										'password' => $pass
										);
						$this->m_akses->PTL_reset($id_akses,$data_reset);
						
						$id_akun = $return['id_akun'];
						$nama = $return['nama'];
						$resakun = $this->m_akun->PTL_select($id_akun);
						$handphone = "";
						if($resakun)
						{
							$handphone = $resakun['handphone'];
						}
						if($handphone != "")
						{
							if(substr($handphone,0,2) == "62")
							{
								$prov = "0".substr($handphone,2,3);
							}
							else
							{
								$prov = substr($handphone,0,4);
							}
							if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
							{
								$data = array(
											'id_user' => $id_akun,
											'nama' => $nama,
											'aplikasi' => 'Finance',
											'judul_sms' => 'Pengiriman Untuk Lupa Password',
											'nomor_hp' => $handphone,
											'pesan' => "ESMOD (NO-REPLY): Dear $nama. Your new password is $pass",
											'login_buat' => $id_akun."_".$nama,
											'tanggal_buat' => $this->waktu
											);
								$this->m_sms->PTL_insert($data);
							}
							else
							{
								$url = "http://103.16.199.187/masking/send_post.php";
								$rows = array(
											"username" => "esmodjkt",
											"password" => "esmj@kr00t",
											"hp" => "$handphone",
											"message" => "Dear $nama. Your new password is $pass"
											);
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_URL, $url );
								curl_setopt($curl, CURLOPT_POST, TRUE );
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
								curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
								curl_setopt($curl, CURLOPT_HEADER, FALSE );
								curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
								curl_setopt($curl, CURLOPT_TIMEOUT, 60);
								$htm = curl_exec($curl);
								if(curl_errno($curl) !== 0)
								{
									$data = array(
												'id_user' => $id_akun,
												'nama' => $nama,
												'aplikasi' => 'Finance',
												'judul_sms' => 'Pengiriman Untuk Lupa Password',
												'nomor_hp' => $handphone,
												'pesan' => "ESMOD (NO-REPLY): Dear $nama. Your new password is $pass",
												'login_buat' => $id_akun."_".$nama,
												'tanggal_buat' => $this->waktu
												);
									$this->m_sms->PTL_insert($data);
									error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
								}
								curl_close($curl);
								print_r($htm);
							}
						}
						
						$this->load->library('email');
						$config = array();
						$config['charset'] = 'utf-8';
						$config['useragent'] = 'Codeigniter';
						$config['protocol']= "smtp";
						$config['mailtype']= "html";
						$config['smtp_host']= "mail.esmodjakarta.com";
						$config['smtp_port']= "25";
						$config['smtp_timeout']= "5";
						$config['smtp_user']= "no-reply@esmodjakarta.com";
						$config['smtp_pass']= "noreplyesmod";
						$config['crlf']="\r\n"; 
						$config['newline']="\r\n"; 
						$config['wordwrap'] = TRUE;
						$this->email->initialize($config);
						$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
						$this->email->to($return['email']);
						$this->email->cc($return['email2']);
						$this->email->bcc('lendra.permana@gmail.com');
						$this->email->subject('FORGOT PASSWORD (NO REPLY)');
						$this->email->message("
							<center>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
								<font color='red'><h2>FINANCE</h2></font>
							</center>
							<br/>
							Hi ".strtoupper($return['nama'])."!
							<br/>
							<br/>
							On this $tgl at $jam, you made a request to reset your password.
							<br/>
							<br/>
							This is your new access
							<br/>
							URL: http://app.esmodjakarta.com/finance
							<br/>
							Employee ID: <b>$return[username]</b>
							<br/>
							Your new password: <b>$pass</b>
							<br/>
							<br/>
							<br/>
							Thanks,
							<br/>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
							<center>
								<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
								Ip: $ip_client
								<br/>
								Hostname: $hostname_client
								<br/>
								OS: $os_client
								<br/>
								Browser: $browser_client
								<br/>
								Devices: $perangkat_client
							</center>
						");
						if($this->email->send())
						{
							echo warning('Verification will be sent to your email:\n\n'.$return['email'],'../login');
						}
						else
						{
							echo warning('Email server is not active. Please try again ...','../login');
						}
					}
					else
					{
						$datadir = array(
									'in' => $email,
									'aplikasi' => 'ADMISSION',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD FAILED'
									);
						$this->m_log->PTL_input($datadir);
						echo warning('Sorry, your Username or Email you entered is incorrect ...','../login');
					}
				}
			}
			else
			{
				echo warning('Please complete the form and captcha.','../login/ptl_forgot');
			}
		}
	
		function ptl_logout()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			if(@$_COOKIE["id_akun"] != "")
			{
				$id_akses = @$_COOKIE["id_akses"];
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				$tanggal = gmdate("Y-m-d", time()-($ms));
				$jam = gmdate("H:i:s", time()-($ms));
				$data = array(
							'tanggal_pengingat' => '',
							'verifikasi' => 'N',
							'ip_client' => $ip_client,
							'hostname_client' => $hostname_client,
							'os_client' => $os_client,
							'browser_client' => $browser_client,
							'perangkat_client' => $perangkat_client,
							'last_logout' => $waktu
							);
				$this->m_akses->PTL_update($id_akses,$data);
				
				$datadir = array(
								'id_akun' => @$_COOKIE["id_akun"]."",
								'id_akses' => $id_akses."",
								'id_cabang' => @$_COOKIE["id_cabang"]."",
								'in' => @$_COOKIE["username"]."",
								'nama' => @$_COOKIE["nama"]."",
								'divisi' => @$_COOKIE["divisi"]."",
								'jabatan' => @$_COOKIE["jabatan"]."",
								'akses' => @$_COOKIE["akses"]."",
								'aplikasi' => 'ADMISSION',
								'tanggal' => $tanggal,
								'jam' => $jam,
								'ip_client' => $ip_client,
								'hostname_client' => $hostname_client,
								'os_client' => $os_client,
								'browser_client' => $browser_client,
								'perangkat_client' => $perangkat_client,
								'status' => 'LOGOUT'
								);
				$this->m_log->PTL_input($datadir);
			}
			$this->auth->logout();
			echo warning('You have successfully logged out ...'.'\n'.
						'Logout Now :  '.$waktu,'../login');
		}
		
		function ptl_home()
		{
			$this->authentification();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance != 'logged') 
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','home');
			$id_akses = $_COOKIE["id_akses"];
			$result = $this->m_akses->PTL_select($id_akses);
			if(($result['tanggal_pengingat'] != "") AND ($result['verifikasi'] == "N"))
			{
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				$handphone = "";
				if($resakun)
				{
					$handphone = $resakun['handphone'];
				}
				if($handphone != "")
				{
					if(substr($handphone,0,2) == "62")
					{
						$prov = "0".substr($handphone,2,3);
					}
					else
					{
						$prov = substr($handphone,0,4);
					}
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => 'Finance',
									'judul_sms' => 'OTP untuk login 7 hari',
									'nomor_hp' => $handphone,
									'pesan' => "ESMOD (NO-REPLY): Finance App Login for 7 days. Your OTP $this->kode_verifikasi",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$handphone",
									"message" => "Finance App Login for 7 days. Your OTP $this->kode_verifikasi"
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $_COOKIE["id_akun"],
										'nama' => $_COOKIE["nama"],
										'aplikasi' => 'Finance',
										'judul_sms' => 'OTP untuk login 7 hari',
										'nomor_hp' => $handphone,
										'pesan' => "ESMOD (NO-REPLY): Finance App Login for 7 days. Your OTP $this->kode_verifikasi",
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
				}
				$data = array(
							'kode_verifikasi' => $this->kode_verifikasi
							);
				$this->m_akses->PTL_update($id_akses,$data);
				echo warning('OTP was sent.','../login/ptl_verification_sms');
			}
			else
			{
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				if(($resakun['verifikasi'] == "N") AND ($id_akun != "00001111"))
				{
					if($_SERVER["HTTP_HOST"] == "localhost")
					{
						header("Location: http://localhost/app/hris_verifikasi/personnel/lendra_edit/$id_akun/finance.xhtml");
					}
					else
					{
						header("Location: http://app.esmodjakarta.com/hris_verifikasi/personnel/lendra_edit/$id_akun/finance.xhtml");
					}
				}
				else
				{
					if(($resakun['verifikasi_handphone'] == "N") AND ($id_akun != "00001111"))
					{
						if($_SERVER["HTTP_HOST"] == "localhost")
						{
							header("Location: http://localhost/app/hris_verifikasi/personnel/lendra_personnel_handphone/$id_akun/finance.xhtml");
						}
						else
						{
							header("Location: http://app.esmodjakarta.com/hris_verifikasi/personnel/lendra_personnel_handphone/$id_akun/finance.xhtml");
						}
					}
					else
					{
						$data['tanggal_pengingat'] = '';
						$data['last_login'] = '';
						$data['last_logout'] = '';
						$data['total_hour'] = '';
						if($result)
						{
							$data['tanggal_pengingat'] = $result['tanggal_pengingat'];
							$tin = explode(" ",$result['last_login']);
							$data['last_login'] = ' Last Login '.tgl_singkat_eng(@$tin[0]).' '.@$tin[1];
							$tout = explode(" ",$result['last_logout']);
							$data['last_logout'] = ', Last Logout '.tgl_singkat_eng(@$tout[0]).' '.@$tout[1];
							$data['total_hour'] = ', Total Access '.selisih_jam(@$tin[1],@$tout[1]).'.';
						}
						$result1 = $this->m_khs->PTL_select_sum_biaya();
						$result2 = $this->m_khs->PTL_select_sum_potongan();
						$result3 = $this->m_khs->PTL_select_sum_bayar();
						$data['biaya'] = $result1['biaya'];
						$data['potongan'] = $result2['potongan'];
						$data['bayar'] = $result3['bayar'];
						$data['rowrecord1'] = $this->m_akses->PTL_all_last_login();
						$data['rowrecord2'] = $this->m_akses->PTL_all_last_logout();
						$this->load->view('Portal/v_header');
						$this->load->view('Beranda/v_home',$data);
						$this->load->view('Portal/v_footer');
					}
				}
			}
		}
		
		function ptl_verification_sms()
		{
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance != 'logged') 
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$id_akses = $_COOKIE["id_akses"];
			$result = $this->m_akses->PTL_select($id_akses);
			$data['kode_verifikasi'] = $result['kode_verifikasi'];
			$this->load->view('v_verifikasi',$data);
		}
		
		function ptl_verification_sms_check()
		{
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance != 'logged') 
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$kode_sms = $this->input->post('kode_sms');
			$kode_verifikasi = $this->input->post('kode_verifikasi');
			if($kode_sms == $kode_verifikasi)
			{
				$id_akses = $_COOKIE["id_akses"];
				$data = array(
							'verifikasi' => 'Y'
							);
				$this->m_akses->PTL_update($id_akses,$data);
				echo warning('Validation Success!','../login/ptl_home');
			}
			else
			{
				echo warning('Invalid OTP!','../login/ptl_verification_sms');
			}
		}
		
		function ptl_profil()
		{
			$is_login_admission = $_COOKIE["is_login_admission"];
			if ($is_login_admission!=='logged')
			{
				$this->session->set_userdata('is_login_admission','notlogged');
				redirect('login');
			}
			$this->authentification();
			$this->session->set_userdata('menu','');
			$id_akun = $_COOKIE["id_akun"];
			$result = $this->m_akun->PTL_select($id_akun);
			$data['id_akun'] = $result['id_akun'];
			$data['nama'] = $result['nama'];
			$data['foto'] = $result['foto'];
			$data['tempat'] = $result['tempat'];
			$data['tanggal_lahir'] = $result['tanggal_lahir'];
			$data['email'] = $result['email'];
			$data['telepon'] = $result['telepon'];
			$data['akses'] = $result['akses'];
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_form',$hdata);
			$this->load->view('Setup/v_profil',$data);
			$this->load->view('Portal/v_footer_form');
		}
		
		function ptl_profil_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$id_akun = $_COOKIE["id_akun"];
			$admin_dp = '../../esmod/hris/ptl_storage/foto_karyawan/';
			$download = $_FILES['userfile']['name'];
			$downloadin = $_COOKIE["nama"];
			
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '5120000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload',$config);
			
			$id_akun = $this->input->post('id_akun');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$result = $this->m_akun->PTL_cek_status($email);
			if(!$this->upload->do_upload())
			{
				if($result)
				{
					$id_akun_lama = $result['id_akun'];
					if($id_akun == $id_akun_lama)
					{
						$data = array(
									'id_akun' => $id_akun,
									'nama' => $nama,
									'tempat' => $this->input->post('tempat'),
									'tanggal_lahir' => $this->input->post('tanggal_lahir'),
									'email' => $email,
									'telepon' => $this->input->post('telepon'),
									'akses' => $this->input->post('akses')
									);
						$this->m_akun->PTL_update($id_akun,$data);
						$error = $this->upload->display_errors('','.');
						echo warning("MESSAGE: ".$error.". Data USER with CODE '".$id_akun."', NAME '".$nama."' successfully changed.","../login/ptl_profil/$id_akun");
					}
					else
					{
						$nama_lama = $result['nama'];
						echo warning("Data USER with EMAIL '".$email."' NAME '".$nama_lama."' already available. Please try with another EMAIL address.","../login/ptl_profil/$id_akun");
					}
				}
				else
				{
					$data = array(
								'id_akun' => $id_akun,
								'nama' => $nama,
								'tempat' => $this->input->post('tempat'),
								'tanggal_lahir' => $this->input->post('tanggal_lahir'),
								'email' => $email,
								'telepon' => $this->input->post('telepon'),
								'akses' => $this->input->post('akses')
								);
					$this->m_akun->PTL_update($id_akun,$data);
					$this->m_akses->PTL_update_username($id_akun,$email);
					$error = $this->upload->display_errors('','.');
					echo warning("MESSAGE: ".$error.". Data USER with CODE '".$id_akun."', NAME '".$nama."' successfully changed. Your new username is '".$email."'.","../login/ptl_profil/$id_akun");
				}
			}
			else
			{
				if($result)
				{
					$id_akun_lama = $result['id_akun'];
					if($id_akun == $id_akun_lama)
					{
						$result = $this->m_akun->PTL_select($id_akun);
						unlink("../../esmod/hris/ptl_storage/foto_karyawan/".$result['foto']);
						$gbr = $this->upload->data();
						$data = array(
									'id_akun' => $id_akun,
									'nama' => $nama,
									'foto' => $gbr['file_name'],
									'tempat' => $this->input->post('tempat'),
									'tanggal_lahir' => $this->input->post('tanggal_lahir'),
									'email' => $email,
									'telepon' => $this->input->post('telepon'),
									'akses' => $this->input->post('akses')
									);
						$this->m_akun->PTL_update($id_akun,$data);
						echo warning("Data USER with CODE '".$id_akun."', NAME '".$nama."' successfully changed.","../login/ptl_profil/$id_akun");
					}
					else
					{
						$nama_lama = $result['nama'];
						echo warning("Data USER with EMAIL '".$email."' NAME '".$nama_lama."' already available. Please try with another EMAIL address.","../login/ptl_profil/$id_akun");
					}
				}
				else
				{
					$result = $this->m_akun->PTL_select($id_akun);
					unlink("../../esmod/hris/ptl_storage/foto_karyawan/".$result['foto']);
					$gbr = $this->upload->data();
					$data = array(
								'id_akun' => $id_akun,
								'nama' => $nama,
								'foto' => $gbr['file_name'],
								'tempat' => $this->input->post('tempat'),
								'tanggal_lahir' => $this->input->post('tanggal_lahir'),
								'email' => $email,
								'telepon' => $this->input->post('telepon'),
								'akses' => $this->input->post('akses')
								);
					$this->m_akun->PTL_update($id_akun,$data);
					$this->m_akses->PTL_update_username($id_akun,$email);
					echo warning("Data USER with CODE '".$id_akun."', NAME '".$nama."' successfully changed. Your new username is '".$email."'.","../login/ptl_profil/$id_akun");
				}
			}
		}
		
		function ptl_ganti_password()
		{
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance != 'logged') 
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','');
			$this->load->view('Portal/v_header');
			$this->load->view('Setup/v_ganti_password');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update_password()
		{
			$this->authentification();
			$username = from_session('username');
			$dataku = $this->input->post('pass_lama');
			
			$result1 = $this->m_akses->PTL_password_lama($username,$dataku);
			if($result1)
			{
				$data = $this->input->post('pass_baru1');
				$data1 = $this->input->post('pass_baru2');
				if($data == $data1)
				{
					$this->m_akses->PTL_update_password($username,$data);
					echo warning("Password successfully changed.","../login/ptl_ganti_password/");
				}
				else
				{
					echo warning("Confirm password is not the same. Please repeat.","../login/ptl_ganti_password/");
				}
			}
			else
			{
				echo warning("Old password is incorrect. Please repeat.","../login/ptl_ganti_password/");
			}
		}
		
		function ptl_maintenance()
		{
			$this->load->view('v_maintenance');
		}
	}
?>