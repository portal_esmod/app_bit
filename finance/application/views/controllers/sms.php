<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Sms extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_waktu = gmdate("YmdHis", time()-($ms));
			$this->tanggal_kirim = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_mail');
			$this->load->model('m_maintenance');
			$this->load->model('m_sms');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/PTL_maintenance');
			}
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','');
			$data['AplikanID'] = $this->uri->segment(3);
			$kode = $this->uri->segment(6);
			$AplikanID = $this->uri->segment(3);
			if($kode == "A")
			{
				$result = $this->m_aplikan->PTL_select($AplikanID);
				$data['Handphone'] = $result['Handphone'];
				$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
				$data['Email'] = $result['Email'];
				$data['EmailOrtu'] = $result['EmailOrtu'];
			}
			else
			{
				$MhswID = $AplikanID;
				$result = $this->m_mahasiswa->PTL_select($MhswID);
				$data['Handphone'] = $result['Handphone'];
				$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
				$data['Email'] = $result['Email'];
				$data['EmailOrtu'] = $result['EmailOrtu'];
			}
			$data['Nama'] = $result['Nama'];
			$sisa = $this->uri->segment(4) - $this->uri->segment(5);
			$data['pesan'] = "Tagihan anda ".formatRupiah4($sisa).". Mhn segera melakukan pembayaran demi kelancaran proses akademik. Tks.";
			$this->load->view('Portal/v_header');
			$this->load->view('SMS/v_sms',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$Nama = $this->input->post('Nama');
			$kirim1 = $this->input->post('kirim1');
			if($kirim1 == "Y")
			{
				$hp_mahasiswa = $this->input->post('hp_mahasiswa');
				if($hp_mahasiswa != "")
				{
					if(substr($hp_mahasiswa,0,2) == "62")
					{
						$prov = "0".substr($hp_mahasiswa,2,3);
					}
					else
					{
						$prov = substr($hp_mahasiswa,0,4);
					}
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $AplikanID,
									'nama' => $Nama,
									'aplikasi' => 'FINANCE',
									'judul_sms' => $this->input->post('subjek'),
									'nomor_hp' => $this->input->post('hp_mahasiswa'),
									'pesan' => "ESMOD (NO-REPLY): ".$this->input->post('pesan'),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$hp_mahasiswa",
									"message" => $this->input->post('pesan')
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $AplikanID,
										'nama' => $Nama,
										'aplikasi' => 'FINANCE',
										'judul_sms' => $this->input->post('subjek'),
										'nomor_hp' => $this->input->post('hp_mahasiswa'),
										'pesan' => "ESMOD (NO-REPLY): ".$this->input->post('pesan'),
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
				}
			}
			$kirim2 = $this->input->post('kirim2');
			if($kirim2 == "Y")
			{
				$hp_orang_tua = $this->input->post('hp_orang_tua');
				if($hp_orang_tua != "")
				{
					if(substr($hp_orang_tua,0,2) == "62")
					{
						$prov = "0".substr($hp_orang_tua,2,3);
					}
					else
					{
						$prov = substr($hp_orang_tua,0,4);
					}
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $AplikanID,
									'nama' => $Nama,
									'aplikasi' => 'FINANCE',
									'judul_sms' => $this->input->post('subjek'),
									'nomor_hp' => $this->input->post('hp_orang_tua'),
									'pesan' => "ESMOD (NO-REPLY): ".$this->input->post('pesan'),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$hp_mahasiswa",
									"message" => $this->input->post('pesan')
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $AplikanID,
										'nama' => $Nama,
										'aplikasi' => 'FINANCE',
										'judul_sms' => $this->input->post('subjek'),
										'nomor_hp' => $this->input->post('hp_orang_tua'),
										'pesan' => "ESMOD (NO-REPLY): ".$this->input->post('pesan'),
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
				}
			}
			$pesan = "
					<br/>
					<br/>
					Finance ESMOD Jakarta telah mengirimkan Anda pesan melalui SMS.
					<br/>
					(<b><i>Finance ESMOD Jakarta has sent you a message via SMS.</i></b>)
					<br/>
					<br/>
					Isi pesan (<b><i>Messages</i></b>) : <span style='color:blue;'><b>".$this->input->post('pesan')."</b></span>
					<br>
					";
			$kirim3 = $this->input->post('kirim3');
			if($kirim3 == "Y")
			{
				$email_mahasiswa = $this->input->post('email_mahasiswa');
				if($email_mahasiswa != "")
				{
					$data = array(
								'kode_waktu' => $this->kode_waktu,
								'aplikasi' => 'FINANCE',
								'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
								'to' => $email_mahasiswa,
								'subject' => $this->input->post('subjek'),
								'tanggal_kirim' => $this->tanggal_kirim,
								'message' => $pesan,
								'id_akun' => $this->input->post('AplikanID'),
								'nama' => $this->input->post('Nama'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_mail->PTL_insert($data);
				}
			}
			$kirim4 = $this->input->post('kirim4');
			if($kirim4 == "Y")
			{
				$email_orang_tua = $this->input->post('email_orang_tua');
				if($email_orang_tua != "")
				{
					$data = array(
								'kode_waktu' => $this->kode_waktu,
								'aplikasi' => 'FINANCE',
								'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
								'to' => $email_orang_tua,
								'subject' => $this->input->post('subjek'),
								'tanggal_kirim' => $this->tanggal_kirim,
								'message' => $pesan,
								'id_akun' => $this->input->post('AplikanID'),
								'nama' => $this->input->post('Nama'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_mail->PTL_insert($data);
				}
			}
			$id_akun = $_COOKIE["id_akun"];
			$resakun = $this->m_akun->PTL_select($id_akun);
			$data = array(
						'kode_waktu' => $this->kode_waktu,
						'aplikasi' => 'FINANCE',
						'from' => $_COOKIE["nama"]." - ".$_COOKIE["divisi"]." ".$_COOKIE["jabatan"],
						'to' => $resakun['email'],
						'subject' => $this->input->post('subjek'),
						'tanggal_kirim' => $this->tanggal_kirim,
						'message' => $pesan,
						'id_akun' => $this->input->post('AplikanID'),
						'nama' => $this->input->post('Nama'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_mail->PTL_insert($data);
			$data['Nama'] = $this->input->post('Nama');
			$this->load->view('Portal/v_header');
			$this->load->view('SMS/v_sms_status',$data);
			$this->load->view('Portal/v_footer');
		}
	}
?>