<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Android_login extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			// $is_login_finance = $_COOKIE["is_login_finance"];
			// if ($is_login_finance!=='logged')
			// {
				// $this->session->set_userdata('is_login_finance','notlogged');
				// redirect('login');
			// }
			// if($_COOKIE["id_akun"] == "")
			// {
				// $this->session->set_userdata('is_login_finance','notlogged');
				// redirect('login');
			// }
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('auth');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_catatan');
			$this->load->model('m_khs');
			$this->load->model('m_log');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->LENDRA_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/dra_maintenance');
			}
		}
		
		function dra_do_login()
		{
			$login = array(
						'username'=>$this->input->post('username'),
						'password'=>$this->input->post('password')
						);
			$return = $this->auth->do_login($login);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$tanggal = gmdate("Y-m-d", time()-($ms));
			$jam = gmdate("H:i:s", time()-($ms));
			$id_akun = $this->input->post('username');
			if($return)
			{
				$dataa = array(
							'last_login' => $waktu
							);
				$this->m_akses->LENDRA_update_akun($id_akun,$dataa);
				
				$datadir = array(
							'id_akun' => $id_akun,
							'aplikasi' => 'FINANCE',
							'tanggal' => $tanggal,
							'jam' => $jam,
							'status' => 'LOGIN'
							);
				$this->m_log->LENDRA_input($datadir);
				
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$query = $this->m_akses->LENDRA_select_apl($username,$password);
				foreach($query->result() as $row)
				{
					$aplikasi = $row->aplikasi;
				}
				$wordd = explode("_", @$aplikasi);
				for($i=0;$i<20;$i++)
				{
					if(@$wordd[$i] == "ac"){$ac = "logged";}
					if(@$wordd[$i] == "ad"){$ad = "logged";}
					if(@$wordd[$i] == "al"){$al = "logged";}
					if(@$wordd[$i] == "ap"){$ap = "logged";}
					if(@$wordd[$i] == "as"){$as = "logged";}
					if(@$wordd[$i] == "at"){$at = "logged";}
					if(@$wordd[$i] == "bs"){$bs = "logged";}
					if(@$wordd[$i] == "ca"){$ca = "logged";}
					if(@$wordd[$i] == "cr"){$cr = "logged";}
					if(@$wordd[$i] == "dv"){$dv = "logged";}
					if(@$wordd[$i] == "fb"){$fb = "logged";}
					if(@$wordd[$i] == "fn"){$fn = "logged";}
					if(@$wordd[$i] == "ft"){$ft = "logged";}
					if(@$wordd[$i] == "hd"){$hd = "logged";}
					if(@$wordd[$i] == "hr"){$hr = "logged";}
					if(@$wordd[$i] == "jb"){$jb = "logged";}
					if(@$wordd[$i] == "lb"){$lb = "logged";}
					if(@$wordd[$i] == "lm"){$lm = "logged";}
					if(@$wordd[$i] == "ml"){$ml = "logged";}
					if(@$wordd[$i] == "qr"){$qr = "logged";}
				}
				if(@$fn == "logged")
				{
					// echo warning('Welcome '.from_session('nama').'\n'.
							// 'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
							// 'Login Now  :  '.$waktu.'\n'.
							// 'Last Online  :  '.from_session('in').'\n'.
							// 'Last Logout :  '.from_session('out'),'../login/dra_home');
					print "login_ok";
					exit;
				}
				else
				{
					print "login_error";
					exit;
					// print "Sorry, you do not have access to this application ...";
				}
			}
			else
			{
				$datadir = array(
							'in' => $this->input->post('username'),
							'aplikasi' => 'FINANCE',
							'tanggal' => $tanggal,
							'jam' => $jam,
							'status' => 'LOGIN FAILED'
							);
				$this->m_log->LENDRA_input($datadir);
				print "login_error";
				exit;
				// print "Sorry, username or password you entered is incorrect ...";
			}
		}
	}
?>