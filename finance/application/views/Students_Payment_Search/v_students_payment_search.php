				<section class="content-header">
					<h1>
						Students Payment Search
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment_search"); ?>"><i class="fa fa-files-o"></i> Students Payment Search</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Students Payment Search</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of students payment search.
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment_search/ptl_filter_tanggal"); ?>" method="POST" id="formId">
														<input type="text" name="cektanggal" value="<?php echo $today; ?>" id="Date1" class="form-control">
													</form>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>TRX ID</th>
													<th><u>PMB</u><br>NIM</th>
													<th>Photo</th>
													<th>Name<hr>Semester</th>
													<th>Program<hr>Prodi</th>
													<th>Year ID</th>
													<th>Account ID</th>
													<th>Total</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													if($rowrecord)
													{
														foreach($rowrecord as $row)
														{
															$MhswID = $row->MhswID;
															if($MhswID != "")
															{
																$res = $this->m_mahasiswa->PTL_select($MhswID);
																$pmb = "";
																$nama = "";
																$foto = "ptl_storage/foto_umum/user.jpg";
																$foto_preview = "ptl_storage/foto_umum/user.jpg";
																if($res)
																{
																	$pmb = $res["PMBID"];
																	$nama = $res["Nama"];
																	if($res["Foto"] != "")
																	{
																		$foto = "../academic/ptl_storage/foto_mahasiswa/".$res["Foto"];
																		$exist = file_exists_remote(base_url("$foto"));
																		if($exist)
																		{
																			$foto = $foto;
																			$source_photo = base_url("$foto");
																			$info = pathinfo($source_photo);
																			$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																			$exist1 = file_exists_remote(base_url("$foto_preview"));
																			if($exist1)
																			{
																				$foto_preview = $foto_preview;
																			}
																			else
																			{
																				$foto_preview = $foto;
																			}
																		}
																		else
																		{
																			$foto = "ptl_storage/foto_umum/user.jpg";
																			$foto_preview = "ptl_storage/foto_umum/user.jpg";
																		}
																	}
																}
																$TahunID = $row->TahunID;
																$reskhs = $this->m_khs->PTL_select_register($MhswID,$TahunID);
																$KHSID = '';
																$ProgramID = '';
																$ProdiID = '';
																$Sesi = '';
																if($reskhs)
																{
																	$KHSID = $reskhs['KHSID'];
																	$ProgramID = $reskhs['ProgramID'];
																	$ProdiID = $reskhs['ProdiID'];
																	$Sesi = $reskhs['Sesi'];
																}
																$smt = "";
																if($Sesi == 1){ $smt = "st"; }
																if($Sesi == 2){ $smt = "nd"; }
																if($Sesi == 3){ $smt = "rd"; }
																if($Sesi == 4){ $smt = "th"; }
																if($Sesi == 5){ $smt = "th"; }
																if($Sesi == 6){ $smt = "th"; }
																$res3 = $this->m_program->PTL_select($ProgramID);
																$program = "";
																if($res3)
																{
																	$program = $res3["Nama"];
																}
																$res4 = $this->m_prodi->PTL_select($ProdiID);
																$prodi = "";
																if($res4)
																{
																	$prodi = $res4["Nama"];
																}
																$RekeningID = $row->RekeningID;
																$resrek = $this->m_rekening->PTL_select($RekeningID);
																$Bank = '';
																if($resrek)
																{
																	$Bank = $resrek['Bank'];
																}
																echo "
																	<tr>
																		<td title='KHSID : $KHSID'>$no</td>
																		<td>$row->BayarMhswID</td>
																		<td><u>$pmb</u><br>
																			<a href='".site_url("students/ptl_edit/$MhswID")."' target='_blank'>$MhswID</a>
																		</td>
																		<td>
																			<a class='fancybox' title='$MhswID - $nama' href='".base_url("$foto")."' data-fancybox-group='gallery' >
																				<img class='img-polaroid' src='".base_url("$foto_preview")."' width='50px' alt='' />
																			</a>
																		</td>
																		<td>$nama<hr/>$Sesi$smt&nbsp;Semester</td>
																		<td>$program<hr/>$prodi</td>
																		<td>$row->TahunID</td>
																		<td>$Bank<br/>$row->RekeningID</td>
																		<td align='right'>".formatRupiah($row->Jumlah)."</td>
																		<td>
																			<a class='btn btn-info' href='".site_url("students_payment/ptl_edit/$KHSID/$row->BayarMhswID")."' target='_blank'>Detail&nbsp;</a>
																		</td>
																	</tr>
																	";
																$no++;
															}
														}
													}
													if($no == 1)
													{
														echo "
															<tr>
																<td colspan='12' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>