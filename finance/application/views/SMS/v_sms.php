				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Send SMS
						<small>Form</small>
					</h1>
				</section>
				<script>
					function hitungkarakter(field,sisa,maksimal)
					{
						field.maxLength = maksimal;
						sisa.value = maksimal - field.value.length;
					}
				</script>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box box-primary">
									<div class="box-header">
										<h3 class="box-title">Send SMS to <b><?php echo $Nama; ?></b></h3>
									</div>
									<form role="form" action="<?php echo site_url("sms/ptl_insert"); ?>" method="POST" />
										<div class="box-body">
											<div class="form-group">
												<label>Subject</label>
												<input type="text" name="subjek" value="Billing" class="form-control" required>
												<input type="hidden" name="AplikanID" value="<?php echo $AplikanID; ?>">
												<input type="hidden" name="Nama" value="<?php echo $Nama; ?>">
											</div>
											<div class="form-group">
												<input type="checkbox" name="kirim1" value="Y" checked>
												<input type="hidden" name="hp_mahasiswa" value="<?php echo $Handphone; ?>">
												<label>Send SMS to student (<?php echo $Handphone; ?>)</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="kirim2" value="Y">
												<input type="hidden" name="hp_orang_tua" value="<?php echo $HandphoneOrtu; ?>">
												<label>Send SMS to parent (<?php echo $HandphoneOrtu; ?>)</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="kirim3" value="Y">
												<input type="hidden" name="email_mahasiswa" value="<?php echo $Email; ?>">
												<label>Send email to student (<?php echo $Email; ?>)</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="kirim4" value="Y">
												<input type="hidden" name="email_orang_tua" value="<?php echo $EmailOrtu; ?>">
												<label>Send email to parent (<?php echo $EmailOrtu; ?>)</label>
											</div>
											<div class="form-group">
												<label>Messages</label>
												<textarea name="pesan" class="form-control" onKeyUp="hitungkarakter(pesan,sisa,240);" required><?php echo $pesan; ?></textarea>
												<input readonly disabled type="text" name="sisa" style="text-align:right;" value="<?php echo (240 - strlen($pesan)); ?>" size="3"/> characters remaining.
											</div>
										</div>
										<div class="box-footer">
											<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>