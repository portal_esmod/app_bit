				<section class="content-header">
					<h1>
						Correction This Semester
						<small>Detail</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i>Students Payment</a></li>
						<li><a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>">Master Cost and Discounts</a></li>
						<li class="active">Correction This Semester</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-6">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Detail</h3>
								</div>
								<?php echo form_open_multipart('students_payment/ptl_correction_this_semester_update',array('name' => 'contoh')); ?>
									<div class="box-body">
										<?php
											$foto = "ptl_storage/foto_umum/user.jpg";
											if($Foto != "")
											{
												$foto = "../academic/ptl_storage/foto_mahasiswa/".$Foto;
												$exist = file_exists_remote(base_url("$foto"));
												if($exist)
												{
													$foto = $foto;
												}
												else
												{
													$foto = "ptl_storage/foto_umum/user.jpg";
												}
											}
										?>
										<center>
											<img src="<?php echo base_url("$foto"); ?>" width="150px"/>
										</center>
										<div class="form-group">
											<label>SIN</label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control" placeholder="" >
											<input type="hidden" name="KHSID" value="<?php echo $KHSID; ?>">
											<input type="hidden" name="OldTahunID" value="<?php echo $TahunID; ?>">
										</div>
										<div class="form-group">
											<label>Name</label>
											<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Program</label>
											<input readonly type="text" value="<?php echo $ProgramID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Prodi</label>
											<input readonly type="text" value="<?php echo $ProdiID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label><font color="red">Year ID</font></label>
											<select name="TahunID" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if($rt->NA == "N")
															{
																$f = "style='background-color: #5BB734;'";
															}
															if($rt->NA == "N")
															{
																echo "<option value='$rt->TahunID'$f";
																if($rt->TahunID == $TahunID)
																{
																	echo "selected";
																}
																echo ">$rt->TahunID - $rt->Nama</option>";
															}
														}
													}
												?>
											</select>
										</div>
										<div class="form-group">
											<label><font color="red">Class</font></label>
											<select name="TahunKe" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="1" <?php if($TahunKe == 1){ echo "selected"; } ?>>1</option>
												<option value="2" <?php if($TahunKe == 2){ echo "selected"; } ?>>2</option>
												<option value="3" <?php if($TahunKe == 3){ echo "selected"; } ?>>3</option>
											</select>
										</div>
										<div class="form-group">
											<label><font color="red">Semester</font></label>
											<select name="Sesi" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="1" <?php if($Sesi == 1){ echo "selected"; } ?>>SEMESTER 1</option>
												<option value="2" <?php if($Sesi == 2){ echo "selected"; } ?>>SEMESTER 2</option>
												<option value="3" <?php if($Sesi == 3){ echo "selected"; } ?>>SEMESTER 3</option>
												<option value="4" <?php if($Sesi == 4){ echo "selected"; } ?>>SEMESTER 4</option>
												<option value="5" <?php if($Sesi == 5){ echo "selected"; } ?>>SEMESTER 5</option>
												<option value="6" <?php if($Sesi == 6){ echo "selected"; } ?>>SEMESTER 6</option>
											</select>
										</div>
										<div class="form-group">
											<label><font color="red">LMS Status</font></label>
											<select name="suspend" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="N" <?php if($suspend == "N"){ echo "selected"; } ?>>GIVE ACCESS TO LMS</option>
												<option value="Y" <?php if($suspend == "Y"){ echo "selected"; } ?>>PENDING ACCESS TO LMS</option>
											</select>
										</div>
										<div class="form-group">
											<label>Messages To Student</label>
											<textarea name="alasan_suspend" class="form-control"><?php echo $alasan_suspend; ?></textarea>
											<font color="red">Fill this field if you choose PENDING ACCESS TO LMS.</font>
										</div>
										<?php
											if($_COOKIE["id_akun"] == "00001111")
											{
												
											}
											else
											{
										?>
												<div class="form-group">
													<label><font color="red">Reason</font></label>
													<textarea name="alasan" class="form-control" placeholder="Your reason" required></textarea>
												</div>
												<h3>Authorization</h3>
												<div class="form-group">
													<label><font color="red">Username Supervisor</font></label>
													<input type="text" name="username" class="form-control" placeholder="Username other user" required>
												</div>
												<div class="form-group">
													<label><font color="red">Password Supervisor</font></label>
													<input type="password" name="password" class="form-control" placeholder="Password other user" required>
												</div>
										<?php
											}
										?>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Update" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>