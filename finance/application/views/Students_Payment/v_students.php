				<style>
					div.paging {
						padding		: 2px;
						margin		: 2px;
						text-align	: center;
						font-family	: Tahoma;
						font-size	: 16px;
						font-weight	: bold;
					}
					div.paging a {
						padding				: 2px 6px;
						margin-right		: 2px;
						border				: 1px solid #DEDFDE;
						text-decoration		: none;
						color				: #dc0203;
						background-position	: bottom;
					}
					div.paging a:hover {
						background-color: #0063dc;
						border : 1px solid #fff;
						color  : #fff;
					}
					div.paging span.current {
						border : 1px solid #DEDFDE;
						padding		 : 2px 6px;
						margin-right : 2px;
						font-weight  : bold;
						color        : #FF0084;
					}
					div.paging span.disabled {
						padding      : 2px 6px;
						margin-right : 2px;
						color        : #ADAAAD;
						font-weight  : bold;
					}
					div.paging span.prevnext {    
					  font-weight : bold;
					}
					div.paging span.prevnext a {
						 border : none;
					}
					div.paging span.prevnext a:hover {
						display: block;
						border : 1px solid #fff;
						color  : #fff;
					}
				</style>
				<section class="content-header">
					<h1>
						Students Payment
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i> Students Payment</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Students</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of students.
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_jur"); ?>" method="POST">
														<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PROGRAM --</option>
															<?php
																$cekjurusan = $this->session->userdata('pym_filter_jur');
																if($rowprogram)
																{
																	foreach($rowprogram as $rp)
																	{
																		echo "<option value='$rp->ProgramID'";
																		if($rp->ProgramID == $cekjurusan)
																		{
																			echo "selected";
																		}
																		echo ">$rp->ProgramID - $rp->Nama</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_tahun"); ?>" method="POST">
														<?php
															$cektahun = $this->session->userdata('pym_filter_tahun');
															$font = "";
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	$f = "";
																	if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																	{
																		$font = "style='background-color: #FFCD41;'";
																	}
																}
															}
														?>
														<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- ACADEMIC YEAR --</option>
															<?php
																if($cekjurusan != "")
																{
																	if($rowtahun)
																	{
																		foreach($rowtahun as $rt)
																		{
																			// if($rt->NA == "N")
																			// {
																				$f = "";
																				if($rt->NA == "N")
																				{
																					$f = "style='background-color: #5BB734;'";
																				}
																				echo "<option value='$rt->TahunID' $f";
																				if($cektahun == $rt->TahunID)
																				{
																					echo "selected";
																				}
																				echo ">$rt->TahunID - $rt->Nama</option>";
																			// }
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_prodi"); ?>" method="POST">
														<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PRODI --</option>
															<?php
																$cekprodi = $this->session->userdata('pym_filter_prodi');
																if($cekjurusan == "INT")
																{
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='$d1->ProdiID'";
																			if($cekprodi == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "REG")
																{
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='$d3->ProdiID'";
																			if($cekprodi == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "SC")
																{
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='$sc->KursusSingkatID'";
																			if($cekprodi == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_kelas"); ?>" method="POST">
														<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- CLASS --</option>
															<?php
																if($cekjurusan == "REG")
																{
																	$init = "";
																}
																if($cekjurusan == "INT")
																{
																	$init = "O";
																}
																$cekkelas = $this->session->userdata('pym_filter_kelas');
																if($rowkelas)
																{
																	foreach($rowkelas as $rk)
																	{
																		if($rk->ProgramID == $cekjurusan)
																		{
																			echo "<option value='$rk->KelasID'";
																			if($cekkelas == $rk->KelasID)
																			{
																				echo "selected";
																			}
																			echo ">$init$rk->Nama</option>";
																		}
																	}
																}
																echo "<option value='MERGE'";
																if($cekkelas == "MERGE")
																{
																	echo "selected";
																}
																echo ">*** MERGE CLASS ***</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_no"); ?>" method="POST">
														<select name="cekno" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>10 data</option>
															<?php
																$cekno = $this->session->userdata('pym_filter_no');
																echo "<option value='25'"; if($cekno == '25'){ echo "selected"; } echo ">25 data</option>";
																echo "<option value='50'"; if($cekno == '50'){ echo "selected"; } echo ">50 data</option>";
																echo "<option value='100'"; if($cekno == '100'){ echo "selected"; } echo ">100 data</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_sesi"); ?>" method="POST">
														<select name="ceksesi" title="Filter by Semester" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- SEMESTER --</option>
															<?php
																$ceksesi = $this->session->userdata('pym_filter_sesi');
																echo "<option value='1'"; if($ceksesi == '1'){ echo "selected"; } echo ">1st Semester</option>";
																echo "<option value='2'"; if($ceksesi == '2'){ echo "selected"; } echo ">2nd Semester</option>";
																echo "<option value='3'"; if($ceksesi == '3'){ echo "selected"; } echo ">3rd Semester</option>";
																echo "<option value='4'"; if($ceksesi == '4'){ echo "selected"; } echo ">4th Semester</option>";
																echo "<option value='5'"; if($ceksesi == '5'){ echo "selected"; } echo ">5th Semester</option>";
																echo "<option value='6'"; if($ceksesi == '6'){ echo "selected"; } echo ">6th Semester</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_payment"); ?>" method="POST">
														<select name="ceklunas" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PAYMENT STATUS --</option>
															<?php
																$ceklunas = $this->session->userdata('pym_filter_payment');
																echo "<option value='Y'"; if($ceklunas == 'Y'){ echo "selected"; } echo ">PAY OFF</option>";
																echo "<option value='N'"; if($ceklunas == 'N'){ echo "selected"; } echo ">PAY ON</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/search"); ?>" method="POST">
														<input type="text" name="cari" value="<?php echo $pencarian; ?>" class="form-control" placeholder="Search.....">
												</td>
												<td>
														<input type="submit" value="Search" id="my_button" class="btn btn-primary">
													</form>
													<?php
														if($pencarian != "")
														{
															echo "<a href='".site_url("students_payment")."' class='btn btn-warning'>Clear</a>";
														}
													?>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th><u>PMB</u><br>NIM</th>
													<th>Photo</th>
													<th>Name<hr>Semester</th>
													<th>Program<hr>Prodi</th>
													<th>Phone</th>
													<th>Father's Name</th>
													<th>Mother's Name</th>
													<th>Status</th>
													<th>Bills<hr>Payment</th>
													<th>Acad. Year</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($cek)
													{
														$no = 1;
														foreach($cariproduk->result() as $row)
														{
															$MhswID = $row->MhswID;
															$res = $this->m_mahasiswa->PTL_select($MhswID);
															$pmb = "";
															$nama = "";
															$foto = "foto_umum/user.jpg";
															$Handphone = "";
															$NamaAyah = "";
															$NamaIbu = "";
															$StatusMhswID = "";
															$status = "";
															$smt = "";
															if($row->Sesi == 1){ $smt = "st"; }
															if($row->Sesi == 2){ $smt = "nd"; }
															if($row->Sesi == 3){ $smt = "rd"; }
															if($row->Sesi == 4){ $smt = "th"; }
															if($row->Sesi == 5){ $smt = "th"; }
															if($row->Sesi == 6){ $smt = "th"; }
															if($res)
															{
																$pmb = $res["PMBID"];
																$nama = $res["Nama"];
																$foto = "ptl_storage/foto_umum/user.jpg";
																$foto_preview = "ptl_storage/foto_umum/user.jpg";
																if($res["Foto"] != "")
																{
																	$foto = "../academic/ptl_storage/foto_mahasiswa/".$res["Foto"];
																	$exist = file_exists_remote(base_url("$foto"));
																	if($exist)
																	{
																		$foto = $foto;
																		$source_photo = base_url("$foto");
																		$info = pathinfo($source_photo);
																		$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																		$exist1 = file_exists_remote(base_url("$foto_preview"));
																		if($exist1)
																		{
																			$foto_preview = $foto_preview;
																		}
																		else
																		{
																			$foto_preview = $foto;
																		}
																	}
																	else
																	{
																		$foto = "ptl_storage/foto_umum/user.jpg";
																		$foto_preview = "ptl_storage/foto_umum/user.jpg";
																	}
																}
																$Handphone = $res["Handphone"];
																$NamaAyah = $res["NamaAyah"];
																$NamaIbu = $res["NamaIbu"];
																$StatusMhswID = $res["StatusMhswID"];
																$res2 = $this->m_status->PTL_select($StatusMhswID);
																if($res2)
																{
																	$status = $res2["Nama"];
																}
															}
															$ProgramID = $row->ProgramID;
															$res3 = $this->m_program->PTL_select($ProgramID);
															$program = "";
															if($res3)
															{
																$program = $res3["Nama"];
															}
															$ProdiID = $row->ProdiID;
															$res4 = $this->m_prodi->PTL_select($ProdiID);
															$prodi = "";
															if($res4)
															{
																$prodi = $res4["Nama"];
															}
															$KelasID = $row->KelasID;
															$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
															if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
															
															$TahunID = $row->TahunID;
															$ryear = $this->m_year->PTL_select($TahunID);
															if($ryear) { $tahun = $ryear["Nama"]; } else { $tahun = ""; }
															$hasil = $row->Biaya - $row->Potongan - $row->Bayar;
															$status_akhir = "";
															if($row->Biaya == 0)
															{
																$status_akhir = "<font color='blue'><b>NO SCHEME</b></font>";
															}
															else
															{
																if($hasil == 0)
																{
																	$status_akhir = "<font color='green'><b>PAY OFF</b></font>";
																}
																if($hasil > 0)
																{
																	$status_akhir = "<font color='red'><b>PAY ON</b></font>";
																}
																if($hasil < 0)
																{
																	$status_akhir = "<font color='purple'><b>OVER PAYMENT</b></font>";
																}
															}
															$TahunKe = $row->TahunKe;
															if($program == "INTENSIVE")
															{
																$TahunKe = "O";
															}
															echo "
																<tr>
																	<td title='KHSID : $row->KHSID'>$no</td>
																	<td><u>$pmb</u><br>
																		<a href='".site_url("students/ptl_edit/$MhswID")."' target='_blank'>$MhswID</a>
																	</td>
																	<td>
																		<a class='fancybox' title='$MhswID - $nama' href='".base_url("$foto")."' data-fancybox-group='gallery' >
																			<img class='img-polaroid' src='".base_url("$foto_preview")."' width='50px' alt='' />
																		</a>
																	</td>
																	<td>$nama<hr>$row->Sesi$smt&nbsp;Semester</td>
																	<td>$program<hr>$prodi ($TahunKe$kelas)</td>
																	<td>$Handphone</td>
																	<td>$NamaAyah</td>
																	<td>$NamaIbu</td>
																	<td>$status<hr>$status_akhir</td>
																	<td align='right'>".formatRupiah($row->Biaya - $row->Potongan)."<hr>".formatRupiah($row->Bayar)."</td>
																	<td>$tahun</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("students_payment/ptl_edit/$row->KHSID")."'>Edit&nbsp;</a>
																		<a href='".site_url("sms/ptl_form/$MhswID/$hasil/0/B")."' title='Send SMS to this Prospect' class='btn btn-warning' target='_blank'><i class='icon-phone'></i>SMS</a>
																	</td>
																</tr>
																";
															$no++;
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='12' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
										<?php
											echo $pagination;
											echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
										?>
										<center>
											<h3>
												TOTAL (COST) : <?php echo formatRupiah($totcost - $totpot); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												TOTAL (PAYMENT) : <?php echo formatRupiah($totbi); ?><br/><br/>
												TOTAL (REMAINING) : <?php echo formatRupiah($totcost - $totpot - $totbi); ?>
											</h3>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>