				<style>
					div.paging {
						padding		: 2px;
						margin		: 2px;
						text-align	: center;
						font-family	: Tahoma;
						font-size	: 16px;
						font-weight	: bold;
					}
					div.paging a {
						padding				: 2px 6px;
						margin-right		: 2px;
						border				: 1px solid #DEDFDE;
						text-decoration		: none;
						color				: #dc0203;
						background-position	: bottom;
					}
					div.paging a:hover {
						background-color: #0063dc;
						border : 1px solid #fff;
						color  : #fff;
					}
					div.paging span.current {
						border : 1px solid #DEDFDE;
						padding		 : 2px 6px;
						margin-right : 2px;
						font-weight  : bold;
						color        : #FF0084;
					}
					div.paging span.disabled {
						padding      : 2px 6px;
						margin-right : 2px;
						color        : #ADAAAD;
						font-weight  : bold;
					}
					div.paging span.prevnext {    
					  font-weight : bold;
					}
					div.paging span.prevnext a {
						 border : none;
					}
					div.paging span.prevnext a:hover {
						display: block;
						border : 1px solid #fff;
						color  : #fff;
					}
				</style>
				<section class="content-header">
					<h1>
						Students Payment
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i> Students Payment</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Students</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of student payments report.
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_jur1"); ?>" method="POST">
														<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PROGRAM --</option>
															<?php
																$cekjurusan = $this->session->userdata('pym_report_filter_jur1');
																if($rowprogram)
																{
																	foreach($rowprogram as $rp)
																	{
																		echo "<option value='$rp->ProgramID'";
																		if($rp->ProgramID == $cekjurusan)
																		{
																			echo "selected";
																		}
																		echo ">$rp->ProgramID - $rp->Nama</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_tahun1"); ?>" method="POST">
														<?php
															$cektahun1 = $this->session->userdata('pym_report_filter_tahun1');
															$font = "";
															if($rowtahun1)
															{
																foreach($rowtahun1 as $rt)
																{
																	$f = "";
																	if(($cektahun1 == $rt->TahunID) AND ($rt->NA == "N"))
																	{
																		$font = "style='background-color: #FFCD41;'";
																	}
																}
															}
														?>
														<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- ACADEMIC YEAR --</option>
															<?php
																if($cekjurusan != "")
																{
																	if($rowtahun1)
																	{
																		foreach($rowtahun1 as $rt)
																		{
																			// if($rt->NA == "N")
																			// {
																				$f = "";
																				if($rt->NA == "N")
																				{
																					$f = "style='background-color: #5BB734;'";
																				}
																				echo "<option value='$rt->TahunID' $f";
																				if($cektahun1 == $rt->TahunID)
																				{
																					echo "selected";
																				}
																				echo ">$rt->TahunID - $rt->Nama</option>";
																			// }
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_prodi1"); ?>" method="POST">
														<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PRODI --</option>
															<?php
																$cekprodi1 = $this->session->userdata('pym_report_filter_prodi1');
																if($cekjurusan == "INT")
																{
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='$d1->ProdiID'";
																			if($cekprodi1 == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "REG")
																{
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='$d3->ProdiID'";
																			if($cekprodi1 == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "SC")
																{
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='$sc->KursusSingkatID'";
																			if($cekprodi1 == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_kelas1"); ?>" method="POST">
														<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
															<option value='_'>-- CLASS --</option>
															<?php
																if($cekjurusan == "REG")
																{
																	$init = "";
																}
																if($cekjurusan == "INT")
																{
																	$init = "O";
																}
																$ni = "";
																$cekkelas = $this->session->userdata('pym_report_filter_kelas1');
																if($cekjurusan != "")
																{
																	if($rowkelas1)
																	{
																		for($i=1;$i<=3;$i++)
																		{
																			foreach($rowkelas1 as $kls)
																			{
																				if($init == "")
																				{
																					$ni = $i;
																				}
																				else
																				{
																					$ni = "";
																				}
																				if($kls->ProgramID == $cekjurusan)
																				{
																					echo "<option value='".$i."_".$kls->KelasID."'";
																					if($cekkelas == $i."_".$kls->KelasID)
																					{
																						echo "selected";
																					}
																					echo ">$init$ni$kls->Nama</option>";
																				}
																			}
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_jur2"); ?>" method="POST">
														<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PROGRAM --</option>
															<?php
																$cekjurusan = $this->session->userdata('pym_report_filter_jur2');
																echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
																echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
																echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_tahun2"); ?>" method="POST">
														<?php
															$cektahun2 = $this->session->userdata('pym_report_filter_tahun2');
															$font = "";
															if($rowtahun2)
															{
																foreach($rowtahun2 as $rt)
																{
																	$f = "";
																	if(($cektahun2 == $rt->TahunID) AND ($rt->NA == "N"))
																	{
																		$font = "style='background-color: #FFCD41;'";
																	}
																}
															}
														?>
														<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- ACADEMIC YEAR --</option>
															<?php
																if($cekjurusan != "")
																{
																	if($rowtahun2)
																	{
																		foreach($rowtahun2 as $rt)
																		{
																			// if($rt->NA == "N")
																			// {
																				$f = "";
																				if($rt->NA == "N")
																				{
																					$f = "style='background-color: #5BB734;'";
																				}
																				echo "<option value='$rt->TahunID' $f";
																				if($cektahun2 == $rt->TahunID)
																				{
																					echo "selected";
																				}
																				echo ">$rt->TahunID - $rt->Nama</option>";
																			// }
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_prodi2"); ?>" method="POST">
														<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PRODI --</option>
															<?php
																$cekprodi2 = $this->session->userdata('pym_report_filter_prodi2');
																if($cekjurusan == "INT")
																{
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='$d1->ProdiID'";
																			if($cekprodi2 == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "REG")
																{
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='$d3->ProdiID'";
																			if($cekprodi2 == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "SC")
																{
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='$sc->KursusSingkatID'";
																			if($cekprodi2 == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("students_payment/ptl_report_filter_kelas2"); ?>" method="POST">
														<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
															<option value='_'>-- CLASS --</option>
															<?php
																if($cekjurusan == "REG")
																{
																	$init = "";
																}
																if($cekjurusan == "INT")
																{
																	$init = "O";
																}
																$ni = "";
																$cekkelas = $this->session->userdata('pym_report_filter_kelas2');
																if($cekjurusan != "")
																{
																	if($rowkelas2)
																	{
																		for($i=1;$i<=3;$i++)
																		{
																			foreach($rowkelas2 as $kls)
																			{
																				if($init == "")
																				{
																					$ni = $i;
																				}
																				else
																				{
																					$ni = "";
																				}
																				if($kls->ProgramID == $cekjurusan)
																				{
																					echo "<option value='".$i."_".$kls->KelasID."'";
																					if($cekkelas == $i."_".$kls->KelasID)
																					{
																						echo "selected";
																					}
																					echo ">$init$ni$kls->Nama</option>";
																				}
																			}
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>SIN</th>
													<th>Prodi</th>
													<th>Class</th>
													<th>Bills</th>
													<th>Discounts</th>
													<th>Paid</th>
													<th>Remaining</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 0;
													$totcost = 0;
													$totdisc = 0;
													$totpaym = 0;
													$totrema = 0;
													if($rowrecord1)
													{
														foreach($rowrecord1 as $row)
														{
															$KelasID2 = "";
															$no++;
															$MhswID = $row->MhswID;
															$res = $this->m_mahasiswa->PTL_select($MhswID);
															$pmb = "";
															$nama = "";
															$Handphone = "";
															$NamaAyah = "";
															$NamaIbu = "";
															$StatusMhswID = "";
															$status = "";
															$smt = "";
															if($row->Sesi == 1){ $smt = "st"; }
															if($row->Sesi == 2){ $smt = "nd"; }
															if($row->Sesi == 3){ $smt = "rd"; }
															if($row->Sesi == 4){ $smt = "th"; }
															if($row->Sesi == 5){ $smt = "th"; }
															if($row->Sesi == 6){ $smt = "th"; }
															if($res)
															{
																$pmb = $res["PMBID"];
																$nama = $res["Nama"];
																$Handphone = $res["Handphone"];
																$NamaAyah = $res["NamaAyah"];
																$NamaIbu = $res["NamaIbu"];
																$StatusMhswID = $res["StatusMhswID"];
																$res2 = $this->m_status->PTL_select($StatusMhswID);
																if($res2)
																{
																	$status = $res2["Nama"];
																}
															}
															$ProgramID = $row->ProgramID;
															$res3 = $this->m_program->PTL_select($ProgramID);
															$program = "";
															if($res3)
															{
																$program = $res3["Nama"];
															}
															$ProdiID = $row->ProdiID;
															$res4 = $this->m_prodi->PTL_select($ProdiID);
															$prodi = "";
															if($res4)
															{
																$prodi = $res4["Nama"];
															}
															$KelasID = $row->KelasID;
															$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
															if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
															
															$TahunID = $row->TahunID;
															$ryear = $this->m_year->PTL_select($TahunID);
															if($ryear) { $tahun = $ryear["Nama"]; } else { $tahun = ""; }
															$hasil = $row->Biaya - $row->Potongan - $row->Bayar;
															$status_akhir = "";
															if($row->Biaya == 0)
															{
																$status_akhir = "<font color='blue'><b>NO SCHEME</b></font>";
															}
															else
															{
																if($hasil == 0)
																{
																	$status_akhir = "<font color='green'><b>PAY OFF</b></font>";
																}
																if($hasil > 0)
																{
																	$status_akhir = "<font color='red'><b>PAY ON</b></font>";
																}
																if($hasil < 0)
																{
																	$status_akhir = "<font color='purple'><b>OVER PAYMENT</b></font>";
																}
															}
															$tutup = "<font color='red'><b>Pay&nbsp;On</b></font>";
															if($row->Tutup == "Y")
															{
																$tutup = "<font color='green'><b>Pay&nbsp;Off</b></font>";
															}
															echo "
																<tr>
																	<td title='KHSID : $row->KHSID'>$no</td>
																	<td>$nama</td>
																	<td>$MhswID</td>
																	<td>$prodi</td>
																	<td>$row->TahunKe$kelas</td>
																	<td align='right'>".formatRupiah($row->Biaya)."</td>
																	<td align='right'>".formatRupiah($row->Potongan)."</td>
																	<td align='right'>".formatRupiah($row->Bayar)."</td>
																	<td align='right'>".formatRupiah(($row->Biaya - $row->Potongan) - $row->Bayar)."</td>
																	<td>$tutup</td>
																</tr>
																";
															$totcost = $totcost + $row->Biaya;
															$totdisc = $totdisc + $row->Potongan;
															$totpaym = $totpaym + $row->Bayar;
															$totrema = $totrema + (($row->Biaya - $row->Potongan) - $row->Bayar);
														}
														foreach($rowrecord2 as $row)
														{
															$no++;
															$MhswID = $row->MhswID;
															$res = $this->m_mahasiswa->PTL_select($MhswID);
															$pmb = "";
															$nama = "";
															$Handphone = "";
															$NamaAyah = "";
															$NamaIbu = "";
															$StatusMhswID = "";
															$status = "";
															$smt = "";
															if($row->Sesi == 1){ $smt = "st"; }
															if($row->Sesi == 2){ $smt = "nd"; }
															if($row->Sesi == 3){ $smt = "rd"; }
															if($row->Sesi == 4){ $smt = "th"; }
															if($row->Sesi == 5){ $smt = "th"; }
															if($row->Sesi == 6){ $smt = "th"; }
															if($res)
															{
																$pmb = $res["PMBID"];
																$nama = $res["Nama"];
																$Handphone = $res["Handphone"];
																$NamaAyah = $res["NamaAyah"];
																$NamaIbu = $res["NamaIbu"];
																$StatusMhswID = $res["StatusMhswID"];
																$res2 = $this->m_status->PTL_select($StatusMhswID);
																if($res2)
																{
																	$status = $res2["Nama"];
																}
															}
															$ProgramID = $row->ProgramID;
															$res3 = $this->m_program->PTL_select($ProgramID);
															$program = "";
															if($res3)
															{
																$program = $res3["Nama"];
															}
															$ProdiID = $row->ProdiID;
															$res4 = $this->m_prodi->PTL_select($ProdiID);
															$prodi = "";
															if($res4)
															{
																$prodi = $res4["Nama"];
															}
															$KelasID = $row->KelasID;
															$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
															if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
															
															$TahunID = $row->TahunID;
															$ryear = $this->m_year->PTL_select($TahunID);
															if($ryear) { $tahun = $ryear["Nama"]; } else { $tahun = ""; }
															$hasil = $row->Biaya - $row->Potongan - $row->Bayar;
															$status_akhir = "";
															if($row->Biaya == 0)
															{
																$status_akhir = "<font color='blue'><b>NO SCHEME</b></font>";
															}
															else
															{
																if($hasil == 0)
																{
																	$status_akhir = "<font color='green'><b>PAY OFF</b></font>";
																}
																if($hasil > 0)
																{
																	$status_akhir = "<font color='red'><b>PAY ON</b></font>";
																}
																if($hasil < 0)
																{
																	$status_akhir = "<font color='purple'><b>OVER PAYMENT</b></font>";
																}
															}
															$tutup = "<font color='red'><b>Pay&nbsp;On</b></font>";
															if($row->Tutup == "Y")
															{
																$tutup = "<font color='green'><b>Pay&nbsp;Off</b></font>";
															}
															echo "
																<tr>
																	<td title='KHSID : $row->KHSID'>$no</td>
																	<td>$nama</td>
																	<td>$MhswID</td>
																	<td>$prodi</td>
																	<td>$row->TahunKe$kelas</td>
																	<td align='right'>".formatRupiah($row->Biaya)."</td>
																	<td align='right'>".formatRupiah($row->Potongan)."</td>
																	<td align='right'>".formatRupiah($row->Bayar)."</td>
																	<td align='right'>".formatRupiah($row->Biaya - $row->Potongan - $row->Bayar)."</td>
																	<td>$tutup</td>
																</tr>
																";
															$totcost = $totcost + $row->Biaya;
															$totdisc = $totdisc + $row->Potongan;
															$totpaym = $totpaym + $row->Bayar;
															$totrema = $totrema + (($row->Biaya - $row->Potongan) - $row->Bayar);
														}
														echo "
															<tr>
																<td colspan='5' align='center'><h4>TOTAL</h4></td>
																<td align='right'><h4>".formatRupiah($totcost)."</h4></td>
																<td align='right'><h4>".formatRupiah($totdisc)."</h4></td>
																<td align='right'><h4>".formatRupiah($totpaym)."</h4></td>
																<td align='right'><h4>".formatRupiah($totrema)."</h4></td>
																<td></td>
															</tr>
															";
													}
													if($no == 0)
													{
														echo "
															<tr>
																<td colspan='11' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
										<center>
											<a class="btn btn-info" href="<?php echo site_url("students_payment/ptl_print/ESMOD%20JAKARTA"); ?>" id="my_button">Print Result</a>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>