				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Withdrawal
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i>Students Payment</a></li>
						<li><a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>">Master Cost and Discounts</a></li>
						<li class="active">Withdrawal </li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-5">
							<div class="box">
								<div class="box-body">
									<form action="<?php echo site_url("students_payment/ptl_withdrawal_insert"); ?>" method="POST">
										<table class="table table-bordered table-striped">
											<tr>
												<td align="right">Withdrawal by</td>
												<td><?php echo $MhswID." - <b>".$Nama."</b>"; ?></td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>From Account</b></font></td>
												<td>
													<select name="RekeningID" class="form-control" required>
														<option value=''>-- ACCOUNT --</option>
														<?php
															if($rowrekening)
															{
																foreach($rowrekening as $rr)
																{
																	echo "<option value='$rr->RekeningID'>$rr->RekeningID - $rr->Nama - $rr->Bank</option>";
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Withdrawal To Bank</b></font></td>
												<td>
													<input type="text" name="Bank" class="form-control" required>
													Fill with `CASH` when paid with cash.
												</td>
											</tr>
											<tr>
												<td align="right">Reference ID</td>
												<td>
													<input type="text" name="BuktiSetoran" class="form-control">
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Type</b></font></td>
												<td>
													<select name="type" class="form-control" required>
														<option value="W">WITHDRAWAL</option>
														<option value="C">CORRECTION</option>
													</select>
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Withdrawal Date</b></font></td>
												<td>
													<input type="text" name="Tanggal" id="datepicker1" class="form-control" required>
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Note</b></font></td>
												<td>
													<textarea name="Keterangan" class="form-control" required></textarea>
												</td>
											</tr>
										</table>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="box">
								<div class="box-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Total</th>
													<th>Paid</th>
													<th>Input</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													$no2 = 1;
													$totpot1 = 0;
													$totpot2 = 0;
													if($detail)
													{
														foreach($detail as $row)
														{
															$BIPOTNamaID = $row->BIPOTNamaID;
															$res = $this->m_master->PTL_select($BIPOTNamaID);
															if($row->TrxID == "-1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID_detail = $res['BIPOTNamaIDRef'];
																$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																if(!$resref_detail)
																{
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td><b>".$res['Nama']."</b>$TambahanNama</td>
																			<td><p align='right'>(".formatRupiah($row->Besar).")</p></td>
																			<td><p align='right'>-</p></td>
																			<td>-</td>
																		</tr>
																		";
																	$no++;
																	$totpot1 = $totpot1 + $row->Besar;
																	$totpot2 = $totpot2 + $row->Dibayar;
																}
															}
														}
														$totbi1 = 0;
														$totbi2 = 0;
														foreach($detail as $row)
														{
															$BIPOTNamaID = $row->BIPOTNamaID;
															$res = $this->m_master->PTL_select($BIPOTNamaID);
															if($row->TrxID == "1")
															{
																$BIPOTNamaIDRef = $BIPOTNamaID;
																$resref = $this->m_master->PTL_select_ref($BIPOTNamaIDRef);
																$ref = "";
																if($resref)
																{
																	$ref = $resref['BIPOTNamaID'];
																}
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID_detail = $ref;
																$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
																$ref_detail = "";
																$ref_jumlah = "";
																$ref_jumlah2 = "";
																if($resref_detail)
																{
																	$BIPOTNamaID = $BIPOTNamaID_detail;
																	$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																	if($resref_detail2)
																	{
																		$ref_detail = "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$resref_detail2["Nama"];
																		$ref_jumlah = "(".formatRupiah($resref_detail2["DefJumlah"]).")";
																		$ref_jumlah2 = $resref_detail2["DefJumlah"];
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																$besar = $row->Besar - $ref_jumlah2;
																$readonly = "";
																if($row->Dibayar == 0)
																{
																	$readonly = "readonly";
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td>
																			<b>".$res['Nama']."</b></b>
																			$TambahanNama
																			$ref_detail $ref_jumlah
																		</td>
																		<td><p align='right'>".formatRupiah($besar)."</p></td>
																		<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																		<td>
																			<input type='hidden' name='BIPOTID$no2' value='$BIPOTID'>
																			<input type='hidden' name='bipotmhsw$no2' value='$row->BIPOTMhswID'>
																			<input type='hidden' name='bipotnama$no2' value='$row->BIPOTNamaID'>
																			<input type='hidden' name='nama$no2' value='$res[Nama]'>
																			<input type='hidden' name='besar$no2' value='$besar'>
																			<input type='hidden' name='dibayar$no2' value='$row->Dibayar'>
																			<input $readonly type='text' name='payment$no2'
																				style='text-align:right;' onmouseover='this.focus()'
																				id='inputku' onkeydown='return numbersonly(this, event);'
																				onkeyup='hitung();javascript:tandaPemisahTitik(this);' autocomplete='off'>
																		</td>
																	</tr>
																	";
																$no++;
																$no2++;
																$totbi1 = $totbi1 + ($row->Besar - $ref_jumlah2);
																$totbi2 = $totbi2 + $row->Dibayar;
															}
														}
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
													$total = $no2 - 1;
													echo"
														<tr>
															<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
															<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi1 - $totpot1)."</p></td>
															<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi2 - $totpot2)."</p></td>
															<td style='background-color:#D8E5D8;border:5px;'>
																<input type='hidden' name='KHSID' value='$KHSID'>
																<input type='hidden' name='MhswID' value='$MhswID'>
																<input type='hidden' name='TahunID' value='$TahunID'>
																<input type='hidden' name='total' value='$total'>";
												?>
																<input type="submit" value="Submit" id="my_button" class="btn btn-primary" onclick="return confirm('Are you sure want to CREATE WITHDRAWAL this Student?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
												<?php
														echo "</td>
														</tr>
														";
												?>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>