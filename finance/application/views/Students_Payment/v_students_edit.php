				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Master Cost and Discounts
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i> Students Payment</a></li>
						<li class="active">Master Cost and Discounts </li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-4">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Master</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<?php
											$pen = "";
											$fstatus0 = "";
											$fstatus1 = "";
											$fstatus2 = "";
											if($this->session->userdata('pym_status') == "NOT")
											{
												if($this->session->userdata('pym_edit_master_bipot') == 0)
												{
													$fstatus0 = "style='background-color: #FFCD41;'";
												}
												if($this->session->userdata('pym_edit_filter_tahun') == "")
												{
													$fstatus1 = "style='background-color: #FFCD41;'";
												}
												if($Sesi == "")
												{
													$fstatus2 = "style='background-color: #FFCD41;'";
												}
											}
											if($cekBIPOTID == "YES")
											{
										?>
												<tr>
													<td><label>Payment Scheme</label></td>
													<td>:</td>
													<td>
														<?php
															$resbipot = $this->m_master->PTL_bipot_select($BIPOTID);
															if($resbipot)
															{
																echo $resbipot['BIPOTID'].' - '.$resbipot['Tahun'].' - '.$resbipot['Nama'];
															}
														?>
													</td>
												</tr>
												<?php
													if($MhswID == "")
													{
												?>
														<tr>
															<td <?php echo $fstatus1; ?>><label>Year To Register</label></td>
															<td>:</td>
															<td>
																<?php
																	$cektahun = $pym_TahunID;
																	$font = "";
																	if($rowtahun)
																	{
																		foreach($rowtahun as $rt)
																		{
																			$f = "";
																			if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																			{
																				$font = "style='background-color: #FFCD41;'";
																			}
																		}
																	}
																?>
																<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																	<?php
																		if($rowtahun)
																		{
																			foreach($rowtahun as $rt)
																			{
																				$f = "";
																				if($rt->NA == "N")
																				{
																					$f = "style='background-color: #5BB734;'";
																				}
																				if($cektahun == $rt->TahunID)
																				{
																					echo "<option value='$rt->TahunID - $KHSID' $f";
																					if($cektahun == $rt->TahunID)
																					{
																						echo "selected";
																					}
																					echo ">$rt->TahunID - $rt->Nama</option>";
																				}
																			}
																		}
																	?>
																</select>
															</td>
														</tr>
														<tr>
															<td <?php echo $fstatus2; ?>><label>Semester To Register</label></td>
															<td>:</td>
															<td>
																<form action="<?php echo site_url("new_students/ptl_filter_sesi"); ?>" method="POST">
																	<select name="ceksesi" title="Filter by Semester" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																		<option value='<?php echo " - $KHSID"; ?>'>-- SEMESTER --</option>
																		<?php
																			$ceksesi = $this->session->userdata('new_student_filter_sesi');
																			for($is=1;$is<=6;$is++)
																			{
																				echo "<option value='$is - $KHSID' $f";
																				if($ceksesi == $is)
																				{
																					echo "selected";
																				}
																				echo ">$is</option>";
																			}
																		?>
																	</select>
																	<noscript><input type="submit" value="Submit"></noscript>
																</form>
															</td>
														</tr>
												<?php
													}
													else
													{
														$reskhs = $this->m_khs->PTL_select_bipot($MhswID,$BIPOTID);
														$year = "";
														$semester = "";
														$thn = "";
														if($reskhs)
														{
															$year = $reskhs["TahunID"];
															$TahunID = $reskhs["TahunID"];
															$semester = $reskhs["Sesi"];
															$resthn = $this->m_year->PTL_select($TahunID);
															if($resthn)
															{
																$thn = $resthn["Nama"];
															}
														}
												?>
														<tr>
															<td><label>Year To Register</label></td>
															<td>:</td>
															<td><?php echo $year." - ".$thn; ?></td>
														</tr>
														<tr>
															<td><label>Semester To Register</label></td>
															<td>:</td>
															<td><?php echo $semester; ?></td>
														</tr>
												<?php
													}
												?>
										<?php
											}
											else
											{
										?>
												<tr>
													<td <?php echo $fstatus0; ?>><label>Payment Scheme</label></td>
													<td>:</td>
													<td>
														<form action="<?php echo site_url("students_payment/ptl_edit_filter_bipot"); ?>" method="POST">
															<select name="BIPOTID" class="form-control" onchange="this.form.submit()">
																<?php
																	if($rowrecord)
																	{
																		echo "<option value='0 $KHSID'>-- CHOOSE --";
																		$master_bipot = $this->session->userdata('pym_edit_master_bipot');
																		foreach($rowrecord as $r)
																		{
																			echo "<option value='$r->BIPOTID $KHSID'";
																			if($master_bipot == $r->BIPOTID)
																			{
																				echo "selected";
																			}
																			echo ">$r->BIPOTID - $r->Tahun - $r->Nama</option>";
																		}
																	}
																?>
															</select>
															<noscript>
																<input type="submit" value="Submit">
															</noscript>
														</form>
													</td>
												</tr>
												<tr>
													<td <?php echo $fstatus1; ?>><label>Year To Register</label></td>
													<td>:</td>
													<td>
														<?php
															$cektahun = $pym_TahunID;
															$font = "";
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	$f = "";
																	if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																	{
																		$font = "style='background-color: #FFCD41;'";
																	}
																}
															}
														?>
														<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<?php
																if($rowtahun)
																{
																	foreach($rowtahun as $rt)
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		if($cektahun == $rt->TahunID)
																		{
																			echo "<option value='$rt->TahunID - $KHSID' $f";
																			if($cektahun == $rt->TahunID)
																			{
																				echo "selected";
																			}
																			echo ">$rt->TahunID - $rt->Nama</option>";
																		}
																	}
																}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td <?php echo $fstatus2; ?>><label>Semester To Register</label></td>
													<td>:</td>
													<td>
														<select name="ceksesi" title="Filter by Semester" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<?php
																$ceksesi = $Sesi;
																for($is=1;$is<=6;$is++)
																{
																	if($ceksesi == $is)
																	{
																		echo "<option value='$is - $KHSID' $f";
																		if($ceksesi == $is)
																		{
																			echo "selected";
																		}
																		echo ">$is</option>";
																	}
																}
															?>
														</select>
													</td>
												</tr>
										<?php
											}
										?>
										<tr>
											<td><label>KHS ID</label></td>
											<td>:</td>
											<td><?php echo $KHSID; ?></td>
										</tr>
										<tr>
											<td><label>PMBID</label></td>
											<td>:</td>
											<td><?php echo $PMBID; ?></td>
										</tr>
										<tr>
											<td><label>MhswID</label></td>
											<td>:</td>
											<td><?php echo $MhswID; ?></td>
										</tr>
										<tr>
											<td><label>Name</label></td>
											<td>:</td>
											<td><?php echo $Nama; ?></td>
										</tr>
										<tr>
											<td><label>Program</label></td>
											<td>:</td>
											<td>
												<?php
													$resprog = $this->m_program->PTL_select($ProgramID);
													$nama_prog = "";
													if($resprog)
													{
														$nama_prog = $ProgramID." - ".$resprog["Nama"];
													}
													echo $nama_prog;
												?>
											</td>
										</tr>
										<tr>
											<td><label>Study Program</label></td>
											<td>:</td>
											<td>
												<?php
													$resprod = $this->m_prodi->PTL_select($ProdiID);
													$nama_prod = "";
													if($resprod)
													{
														$nama_prod = $ProdiID." - ".$resprod["Nama"];
													}
													echo $nama_prod;
												?>
											</td>
										</tr>
										<tr>
											<td><label>LMS Status</label></td>
											<td>:</td>
											<td>
												<?php
													if($suspend == "N")
													{
														echo "<font color='blue'><b>GIVE ACCESS TO LMS</b></font>";
													}
													else
													{
														echo "<font color='red'><b>PENDING ACCESS TO LMS</b></font>";
													}
												?>
											</td>
										</tr>
										<tr>
											<td><label>Created By</label></td>
											<td>:</td>
											<td><?php echo $login_buat; ?></td>
										</tr>
										<tr>
											<td><label>Created Date</label></td>
											<td>:</td>
											<td><?php echo $tanggal_buat; ?></td>
										</tr>
										<tr>
											<td><label>Edited By</label></td>
											<td>:</td>
											<td><?php echo $login_edit; ?></td>
										</tr>
										<tr>
											<td><label>Edited Date</label></td>
											<td>:</td>
											<td><?php echo $tanggal_edit; ?></td>
										</tr>
										<tr>
											<td><label>Virtual Account</label></td>
											<td>:</td>
											<td>
												<form action="<?php echo site_url("students_payment/ptl_virtual_account_insert"); ?>" method="POST">
													<input type="hidden" name="KHSID" value="<?php echo $KHSID; ?>"/>
													<input type="hidden" name="MhswID" value="<?php echo $MhswID; ?>"/>
													<input type="text" name="virtual_account" value="<?php echo $virtual_account; ?>" class="form-control"/>
													<input type="submit" value="SET" id="my_button" class="btn btn-primary"/>
												</form>
											</td>
										</tr>
										<tr>
											<td colspan="3" align="center"><a href="<?php echo site_url("students_payment/ptl_register_next_semester/$KHSID"); ?>" class="btn btn-success">Register To Next Semester</a></td>
										</tr>
										<tr>
											<td colspan="3" align="center"><a href="<?php echo site_url("students_payment/ptl_correction_this_semester/$KHSID"); ?>" class="btn btn-danger">Correction This Semester</a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="box">
								<div class="box-header">
									<table class="table table-bordered table-striped">
										<tr>
											<td><h3 class="box-title">BIPOT Management</h3></td>
											<td align="right">
												<?php
													$penalty = 0;
													$min = 0;
													foreach($rowrecord as $r)
													{
														if($BIPOTID == $r->BIPOTID)
														{
															$min = $r->minimal_pembayaran;
														}
													}
													if(($cekBIPOTID == "YES") AND ($MhswID == ""))
													{
														if($detail)
														{
															$biaya2 = 0;
															$biaya_byr = 0;
															$potongan2 = 0;
															$potongan_byr = 0;
															foreach($detail as $row)
															{
																if($row->TrxID == "1")
																{
																	$biaya2 = $biaya2 + $row->Besar;
																	$biaya_byr = $biaya_byr + $row->Dibayar;
																}
																if($row->TrxID == "-1")
																{
																	$potongan2 = $potongan2 + $row->Besar;
																	$potongan_byr = $potongan_byr + $row->Dibayar;
																}
															}
															$total_bipot = $biaya2 - $potongan2 - $biaya_byr;
															if($total_bipot < 0)
															{
																$m = "-";
															}
															else
															{
																$m = "";
															}
														}
														if($rowrecord)
														{
															$h = "-7";
															$hm = $h * 60;
															$ms = $hm * 60;
															$tanggal = gmdate("Y-m-d", time()-($ms));
															foreach($rowrecord as $r)
															{
																if($BIPOTID == $r->BIPOTID)
																{
																	if($r->jatuh_tempo_akhir == "")
																	{
																		$cekjta = -1;
																		$penalty = 0;
																	}
																	else
																	{
																		$cekjta = hitungHari($r->jatuh_tempo_akhir,$tanggal);
																		$jt_a = str_replace("-","",$r->jatuh_tempo_akhir);
																		$nw_t = str_replace("-","",$tanggal);
																		$selisih_jta = 0;
																		if($jt_a < $nw_t)
																		{
																			$selisih_jta = $cekjta;
																		}
																		else
																		{
																			$cekjta = -$cekjta;
																		}
																		$penalty = $r->denda * $selisih_jta;
																	}
																}
															}
														}
														$cost = $biaya2 + $penalty;
														$pieces = $potongan2;
												?>
														<a href="<?php echo site_url("new_students/ptl_create_nim/".$KHSID."_".$PMBID."/$min/$biaya_byr/$BIPOTID/$cost/$pieces"); ?>"><h5 class="box-title">Create NIM</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td align="left">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_payment_form/$KHSID/$MhswID/$pym_TahunID"); ?>"><h5 class="box-title">Add Payment</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($rowrecord)
													{
														$h = "-7";
														$hm = $h * 60;
														$ms = $hm * 60;
														$tanggal = gmdate("Y-m-d", time()-($ms));
														foreach($rowrecord as $r)
														{
															if($BIPOTID == $r->BIPOTID)
															{
																if($r->jatuh_tempo == "")
																{
																	$cekjt = -1;
																}
																else
																{
																	$cekjt = hitungHari($r->jatuh_tempo,$tanggal);
																}
																if($r->jatuh_tempo_akhir == "")
																{
																	$cekjta = -1;
																	$penalty = 0;
																}
																else
																{
																	$cekjta = hitungHari($r->jatuh_tempo_akhir,$tanggal);
																	$jt_a = str_replace("-","",$r->jatuh_tempo_akhir);
																	$nw_t = str_replace("-","",$tanggal);
																	$selisih_jta = 0;
																	if($jt_a < $nw_t)
																	{
																		$selisih_jta = $cekjta;
																	}
																	else
																	{
																		$cekjta = -$cekjta;
																	}
																	$penalty = $r->denda * $selisih_jta;
																}
															}
														}
													}
													if($cekBIPOTID != "YES")
													{
														$biaya_fix = 0;
														$potongan_fix = 0;
														if($detail)
														{
															$biaya = 0;
															$potongan = 0;
															foreach($detail as $row)
															{
																if($row->TrxID == "1")
																{
																	$biaya = $biaya + $row->Jumlah;
																}
																if($row->TrxID == "-1")
																{
																	$potongan = $potongan + $row->Jumlah;
																}
															}
															$total_bipot = $biaya - $potongan;
															if($total_bipot < 0)
															{
																$m = "-";
															}
															else
															{
																$m = "";
															}
															$biaya_fix = $biaya + $penalty;
															$potongan_fix = $potongan;
														}
												?>
														<a href="<?php echo site_url("students_payment/ptl_bipot_create/".$MhswID."_".$BIPOTID."_".$KHSID."_".$Sesi."_".$pym_TahunID."/$biaya_fix/$potongan_fix"); ?>" onclick="return confirm('Are you sure want to process this BIPOT <?php echo $BIPOTID; ?>?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><h5 class="box-title">Process Bipot</h5></a>
												<?php
													}
													else
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_withdrawal_form/$KHSID/$MhswID/$pym_TahunID"); ?>"><h5 class="box-title">Withdrawal / Correction</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td align="left">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_pdf_invoice/$BIPOTID/$MhswID/$pym_TahunID/ESMOD%20JAKARTA"); ?>"><h5 class="box-title">Print Invoice</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_bipot_delete/$MhswID/$KHSID/$pym_TahunID/$Sesi/$BIPOTID"); ?>" onclick="return confirm('Are you sure want to delete this BIPOT <?php echo $BIPOTID; ?>?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><h5 class="box-title">Delete All Bipot</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td>
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_bipotmhs_cost_form/$MhswID/$BIPOTID/$KHSID/$pym_TahunID"); ?>"><h5 class="box-title">Add Cost</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("students_payment/ptl_bipotmhs_discounts_form/$MhswID/$BIPOTID/$KHSID/$pym_TahunID"); ?>"><h5 class="box-title">Add Discounts</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<?php
											if($rowrecord)
											{
												$h = "-7";
												$hm = $h * 60;
												$ms = $hm * 60;
												$tanggal = gmdate("Y-m-d", time()-($ms));
												foreach($rowrecord as $r)
												{
													if($BIPOTID == $r->BIPOTID)
													{
														if($r->jatuh_tempo == "")
														{
															$cekjt = -1;
														}
														else
														{
															$cekjt = hitungHari($r->jatuh_tempo,$tanggal);
														}
														if($cekjt >= 0)
														{
															$jt = "<font color='#F21729;' size='5'>Grace Period: ".tgl_singkat_eng($r->jatuh_tempo)."</font>";
														}
														else
														{
															$jt = "Grace Period: ".tgl_singkat_eng($r->jatuh_tempo);
														}
														if($r->jatuh_tempo_akhir == "")
														{
															$cekjta = -1;
															$penalty = 0;
														}
														else
														{
															$cekjta = hitungHari($r->jatuh_tempo_akhir,$tanggal);
															$jt_a = str_replace("-","",$r->jatuh_tempo_akhir);
															$nw_t = str_replace("-","",$tanggal);
															$selisih_jta = 0;
															if($jt_a < $nw_t)
															{
																$selisih_jta = $cekjta;
															}
															else
															{
																$cekjta = -$cekjta;
															}
															$penalty = $r->denda * $selisih_jta;
														}
														if($cekjta >= 0)
														{
															$jta = "<font color='#F21729;' size='5'>Due Date: ".tgl_singkat_eng($r->jatuh_tempo_akhir)."</font>";
															if($cekBIPOTID == "YES")
															{
																$pen = "<a href='".site_url("students_payment/ptl_reminder_expired/$KHSID/$MhswID")."' class='btn btn-success'>REMINDER</a>";
															}
															else
															{
																$pen = "";
															}
															$p = "Penalty: ".formatRupiah($penalty)." ($cekjta days)&nbsp;&nbsp;&nbsp;";
														}
														else
														{
															$jta = "Due Date: ".tgl_singkat_eng($r->jatuh_tempo_akhir);
															$p = "";
														}
														echo "<tr>
																<td>$jt</td>
																<td>$jta</td>
															</tr>
															<tr>
																<td>Minimum Payment: ".formatRupiah($r->minimal_pembayaran)."</td>
																<td title='This value only for reference.'>$p</td>
															</tr>
															<tr>
																<td>Notes: $r->Catatan</td>
																<td>$pen</td>
															</tr>
															<tr>
																<td colspan='2' align='center'><a href='".site_url("students_payment/ptl_reset/$KHSID")."' class='btn btn-danger'>RESET</a></td>
															</tr>";
													}
												}
											}
										?>
										<?php
											if($StatusMhswID != "A")
											{
										?>
												<tr>
													<td colspan="2"><p align='center'><a href="#" class="btn btn-warning">POSTPONE</a></p></td>
												</tr>
										<?php
											}
											if($cekBIPOTID == "YES")
											{
												if($detail)
												{
													$biaya_byr = 0;
													$potongan2 = $Potongan;// master
													// $potongan2 = 0;// master
													$potongan_byr = 0;
													foreach($detail as $row)
													{
														if($row->TrxID == "1")
														{
															$biaya_byr = $biaya_byr + $row->Dibayar;
														}
														if($row->TrxID == "-1")
														{
															$potongan2 = $potongan2 + $row->Besar;
															$potongan_byr = $potongan_byr + $row->Dibayar;
														}
													}
													$total_bipot = $Biaya - $potongan2 - $biaya_byr;
													if($total_bipot < 0)
													{
														$m = "-";
													}
													else
													{
														$m = "";
													}
										?>
													<tr>
														<td><p align='right'><h3 class="box-title">DEPOSIT</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($Deposit); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">COST</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($Biaya); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">DISCOUNTS</h3></td>
														<td><p align='right'><font size="5"><?php echo "-".formatRupiah($Potongan); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">PAYMENTS</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($Bayar); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">BALANCE</h3></td>
														<!--<td><p align='right'><font color="red" size="5"><?php echo $m.formatRupiah($total_bipot); ?></font></p></td>-->
														<<td><p align='right'><font color="red" size="5"><?php echo $m.formatRupiah($Biaya - $Potongan - $Bayar); ?></font></p></td>
													</tr>
										<?php
												}
											}
											else
											{
												if($detail)
												{
													$biaya = 0;
													$potongan = 0;
													foreach($detail as $row)
													{
														if($row->TrxID == "1")
														{
															$biaya = $biaya + $row->Jumlah;
														}
														if($row->TrxID == "-1")
														{
															$potongan = $potongan + $row->Jumlah;
														}
													}
													$total_bipot = $biaya - $potongan;
													if($total_bipot < 0)
													{
														$m = "-";
													}
													else
													{
														$m = "";
													}
													$biaya_fix = $biaya + $penalty;
													$potongan_fix = $potongan;
										?>
													<tr>
														<td><p align='right'><h3 class="box-title">DEPOSIT</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($Deposit); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">COST</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($biaya + $penalty); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">DISCOUNTS</h3></td>
														<td><p align='right'><font size="5"><?php echo "-".formatRupiah($potongan); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">BALANCE</h3></td>
														<td><p align='right'><font color="red" size="5"><?php echo $m.formatRupiah($total_bipot + $penalty); ?></font></p></td>
													</tr>
										<?php
												}
											}
										?>
									</table>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<?php
													if($cekBIPOTID == "YES")
													{
														echo "<th>#</th>
															<th>Name</th>
															<th>Total</th>
															<th>Paid</th>
															<th>Action</th>";
													}
													else
													{
														echo "<th>#</th>
															<th>Name</th>
															<th>Total</th>
															<th>NA</th>";
													}
												?>
											</tr>
										</thead>
										<tbody>
											<?php
												if($cekBIPOTID == "YES")
												{
													if($detail)
													{
														$no = 1;
														$totpot1 = 0;
														$totpot2 = 0;
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOTMhswRef = $row->BIPOTMhswRef;
																if($BIPOTMhswRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td title='BIPOTMhswID : $row->BIPOTMhswID'><b>$Nama</b>$TambahanNama</td>
																			<td><p align='right'>".formatRupiah($row->Besar)."</p></td>
																			<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																			<td>";
																		
											?>
																			<a href="<?php echo site_url("students_payment/ptl_bipotmhs_delete/$KHSID/$row->BIPOTMhswID/$BIPOTNamaID/$row->Dibayar"); ?>" title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->BIPOTMhswID." - ".$res["Nama"]; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
											<?php
																	echo "</td>
																		</tr>
																		";
																	$no++;
																	$totpot1 = $totpot1 + $row->Besar;
																	$totpot2 = $totpot2 + $row->Dibayar;
																}
															}
														}
														$totbi1 = 0;
														$totbi2 = 0;
														foreach($detail as $row)
														{
															if($row->TrxID == "1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOTMhswRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_pym_detail_ref_cek($BIPOTID,$BIPOTMhswRef,$MhswID);
																$ref_detail = "";
																$ref_jumlah2 = 0;
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$nm = $resref_detail2['Nama'];
																		}
																		$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$nm."(".formatRupiah($rd->Besar).") <a href='".site_url("students_payment/ptl_bipotmhs_delete/$KHSID/$rd->BIPOTMhswID/$BIPOTNamaID/$rd->Dibayar")."' title='Delete' class='btn btn-danger'><i class='fa fa-trash'></i></a>";
																		$ref_jumlah2 = $ref_jumlah2 + $rd->Besar;
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td title='BIPOTMhswID : $row->BIPOTMhswID'>
																			<b><a href='".site_url("students_payment/ptl_correction_cost/$row->BIPOTMhswID/$MhswID/$KHSID")."'>$Nama</a></b>
																			$TambahanNama
																			$ref_detail
																		</td>
																		<td title='Bill : ".formatRupiah($row->Besar)." ~ Discount : ".formatRupiah($ref_jumlah2)."'><p align='right'>".formatRupiah($row->Besar - $ref_jumlah2)."</p></td>
																		<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																		<td>";
																		
											?>
																			<a href="<?php echo site_url("students_payment/ptl_bipotmhs_delete/$KHSID/$row->BIPOTMhswID/$BIPOTNamaID/$row->Dibayar"); ?>" title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->BIPOTMhswID." - ".$res["Nama"]; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
											<?php
																echo "</td>
																	</tr>
																	";
																$no++;
																$totbi1 = $totbi1 + ($row->Besar - $ref_jumlah2);
																$totbi2 = $totbi2 + $row->Dibayar;
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($totbi1)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($totbi2 - $totpot2)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												}
												else
												{
													if($detail)
													{
														$no = 1;
														$jumlah_potongan = "";
														$jumlah_cost = "";
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOT2IDRef = $row->BIPOT2IDRef;
																if($BIPOT2IDRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td title='BIPOT2ID : $row->BIPOT2ID'><b>$Nama</b>$TambahanNama</td>
																			<td><p align='right'>".formatRupiah($row->Jumlah)."</p></td>
																			<td>$row->NA</td>
																		</tr>
																		";
																	$no++;
																	$jumlah_potongan = $jumlah_potongan + $row->Jumlah;
																}
															}
														}
														foreach($detail as $row)
														{
															if($row->TrxID == "1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOT2IDRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOT2IDRef);
																$ref_detail = "";
																$ref_jumlah2 = 0;
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$resref_detail2["Nama"]."(".formatRupiah($rd->Jumlah).")";
																			$ref_jumlah2 = $ref_jumlah2 + $rd->Jumlah;
																		}
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td title='BIPOT2ID : $row->BIPOT2ID'>
																			<b>$Nama</b>
																			$TambahanNama
																			$ref_detail
																		</td>
																		<td title='(".formatRupiah($row->Jumlah).") - (".formatRupiah($ref_jumlah2).")'><p align='right'>".formatRupiah($row->Jumlah - $ref_jumlah2)."</p></td>
																		<td>$row->NA</td>
																	</tr>
																	";
																$no++;
																$jumlah_cost = $jumlah_cost + ($row->Jumlah - $ref_jumlah2);
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($jumlah_cost - $jumlah_potongan)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Trx ID</th>
													<th>Type</th>
													<th>Date</th>
													<th>To Account</th>
													<th>Paid</th>
													<th>Note</th>
													<th>View</th>
													<th>Created By</th>
													<th>Posting Date</th>
													<th>Edited By</th>
													<th>Edited Date</th>
													<th>Print</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$total_bayar = 0;
													if($rowbukti)
													{
														$no = 1;
														foreach($rowbukti as $row)
														{
															$trx = "<font color='green'><b>PAID</b></font>";
															if($row->TrxID == "-1")
															{
																$trx = "<font color='blue'><b>WITHDRAWAL</b></font>";
															}
															if($row->TrxID == "-2")
															{
																$trx = "<font color='red'><b>CORRECTION</b></font>";
															}
															$cetak = $row->Cetak;
															$bktstyle = "style='background-color:#ECF0F5;border:5px;'";
															if($row->BayarMhswID == $CekBayarMhswID)
															{
																$bktstyle = "style='background-color:#F49292;border:5px;'";
															}
															echo"<tr>
																	<td $bktstyle>$no</td>
																	<td $bktstyle>$row->BayarMhswID</a></td>
																	<td $bktstyle>$trx</a></td>
																	<td $bktstyle>".tgl_singkat_eng($row->Tanggal)."</a></td>
																	<td $bktstyle>$row->RekeningID</a></td>
																	<td $bktstyle align='right'>".formatRupiah($row->Jumlah)."</td>
																	<td $bktstyle>$row->Keterangan</td>
																	<td $bktstyle>$cetak</td>
																	<td $bktstyle>$row->login_buat</td>
																	<td $bktstyle>$row->tanggal_buat</td>
																	<td $bktstyle>$row->login_edit</td>
																	<td $bktstyle>$row->tanggal_edit</td>
																	<td $bktstyle>
																		<a href='".site_url("students_payment/ptl_payment_edit/$row->BayarMhswID/$KHSID/$MhswID/$pym_TahunID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>
																		<a href='".site_url("students_payment/ptl_print_payment/$row->BayarMhswID/$KHSID/$MhswID/$pym_TahunID/ORIGINAL/ESMOD%20JAKARTA")."' class='btn btn-success'>Original</a>
																		<a href='".site_url("students_payment/ptl_print_payment/$row->BayarMhswID/$KHSID/$MhswID/$pym_TahunID/COPY/ESMOD%20JAKARTA")."' class='btn btn-warning'>Copy</a>
																	</td>
																</tr>
																";
															$no++;
															if($row->TrxID == "-1")
															{
																
															}
															else
															{
																if($row->TrxID == "-2")
																{
																	
																}
																else
																{
																	$total_bayar = $total_bayar + $row->Jumlah;
																}
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='5'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($total_bayar)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='13'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>