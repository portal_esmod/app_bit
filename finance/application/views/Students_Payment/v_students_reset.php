				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Reset
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i>Students Payment</a></li>
						<li><a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>">Master Cost and Discounts</a></li>
						<li class="active"> Reset</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Reset</h3>
								</div>
								<form role="form" action="<?php echo site_url("students_payment/ptl_reset_insert"); ?>" method="POST" />
									<div class="box-body">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>Payment data will be permanently deleted.
										</div>
										<div class="form-group">
											<label><font color="blue">SIN</font></label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control">
											<input type="hidden" name="TahunID" value="<?php echo $TahunID; ?>" class="form-control">
										</div>
										<div class="form-group">
											<label><font color="blue">Name</font></label>
											<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
										</div>
										<div class="form-group">
											<label><font color="blue">KHSID</font></label>
											<input readonly type="text" name="KHSID" value="<?php echo $KHSID; ?>" class="form-control">
										</div>
										<div class="form-group">
											<label><font color="red">Reason</font></label>
											<textarea name="reason" class="form-control" required></textarea>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to reset this payment data?\n\nTHIS ACTION CAN NOT BE RESTORED.')">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>