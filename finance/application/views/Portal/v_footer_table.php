			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 1.0
				</div>
				<strong>Copyright &copy; 2015 <a href="http://esmodjakarta.com">Esmod Jakarta</a>.</strong> All rights reserved.
			</footer>
			<aside class="control-sidebar control-sidebar-dark">
				<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
					<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
					<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="control-sidebar-home-tab">
						<h3 class="control-sidebar-heading">Recent Activity</h3>
						<ul class='control-sidebar-menu'>
							<li>
								<a href='javascript::;'>
									<i class="menu-icon fa fa-birthday-cake bg-red"></i>
									<div class="menu-info">
										<h4 class="control-sidebar-subheading">Developer Birthday</h4>
										<p>Will be 23 on April 24th</p>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<i class="menu-icon fa fa-user bg-yellow"></i>
									<div class="menu-info">
										<h4 class="control-sidebar-subheading">Susi Tenna Updated His Profile</h4>
										<p>New phone +1(800)555-1234</p>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
									<div class="menu-info">
										<h4 class="control-sidebar-subheading">Amarilla Joined Mailing List</h4>
										<p>nora@example.com</p>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<i class="menu-icon fa fa-file-code-o bg-green"></i>
									<div class="menu-info">
										<h4 class="control-sidebar-subheading">Enrollment Job 254 Executed</h4>
										<p>Execution time 5 seconds</p>
									</div>
								</a>
							</li>
						</ul>
						<h3 class="control-sidebar-heading">Tasks Progress</h3>
						<ul class='control-sidebar-menu'>
							<li>
								<a href='javascript::;'>
									<h4 class="control-sidebar-subheading">
										Task 1
										<span class="label label-danger pull-right">70%</span>
									</h4>
									<div class="progress progress-xxs">
										<div class="progress-bar progress-bar-danger" style="width: 70%"></div>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<h4 class="control-sidebar-subheading">
										Task 2
										<span class="label label-success pull-right">95%</span>
									</h4>
									<div class="progress progress-xxs">
										<div class="progress-bar progress-bar-success" style="width: 95%"></div>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<h4 class="control-sidebar-subheading">
										Task 3
										<span class="label label-waring pull-right">50%</span>
									</h4>
									<div class="progress progress-xxs">
										<div class="progress-bar progress-bar-warning" style="width: 50%"></div>
									</div>
								</a>
							</li>
							<li>
								<a href='javascript::;'>
									<h4 class="control-sidebar-subheading">
										Task 4
										<span class="label label-primary pull-right">68%</span>
									</h4>
									<div class="progress progress-xxs">
										<div class="progress-bar progress-bar-primary" style="width: 68%"></div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="tab-pane" id="control-sidebar-settings-tab">
						<form method="post">
							<h3 class="control-sidebar-heading">General Settings</h3>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Report panel usage
									<input type="checkbox" class="pull-right" checked />
								</label>
								<p>
									Some information about this general settings option
								</p>
							</div>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Allow mail redirect
									<input type="checkbox" class="pull-right" checked />
								</label>
								<p>
									Other sets of options are available
								</p>
							</div>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Expose author name in posts
									<input type="checkbox" class="pull-right" checked />
								</label>
								<p>
									Allow the user to show his name in blog posts
								</p>
							</div>
							<h3 class="control-sidebar-heading">Chat Settings</h3>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Show me as online
									<input type="checkbox" class="pull-right" checked />
								</label>
							</div>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Turn off notifications
									<input type="checkbox" class="pull-right" />
								</label>
							</div>
							<div class="form-group">
								<label class="control-sidebar-subheading">
									Delete chat history
									<a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
								</label>
							</div>
						</form>
					</div>
				</div>
			</aside>
			<div class='control-sidebar-bg'></div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src='<?php echo base_url(); ?>assets/dashboard/plugins/fastclick/fastclick.min.js'></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/dist/js/app.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/dist/js/demo.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function ()
			{
				$("#example1").dataTable();
				$("#example2").dataTable();
				$('#example3').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
			});
			
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
		</script>
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
	<script type="text/javascript">
		var $fcb = jQuery.noConflict();
		$fcb(document).ready(function() 
		{
			$fcb('.fancybox').fancybox();
		});
	</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/zebra_datepicker.js"></script>
	<script>
		var dpk = jQuery.noConflict();
		dpk('#datepicker1').Zebra_DatePicker();
		dpk('#Date1').Zebra_DatePicker({
			show_week_number: 'Wk',
			format: 'Y-m-d',
			onSelect : function (dateText, inst) {
				dpk('#formId').submit();
			}
		});
	</script>
</html>