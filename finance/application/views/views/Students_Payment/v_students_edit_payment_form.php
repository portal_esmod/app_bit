				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Add Payment
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i>Students Payment</a></li>
						<li><a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>">Master Cost and Discounts</a></li>
						<li class="active">Add Payment </li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<?php
							if($status_akhir != "")
							{
						?>
								<div class="col-md-12">
									<div class="box">
										<div class="box-body">
											<?php
												echo "<center><h4>$status_akhir</h4></center>";
											?>
										</div>
									</div>
								</div>
						<?php
							}
						?>
						<div class="col-md-5">
							<div class="box">
								<div class="box-body">
									<form action="<?php echo site_url("students_payment/ptl_payment_insert"); ?>" method="POST">
										<table class="table table-bordered table-striped">
											<tr>
												<td align="right">Paid by</td>
												<td><?php echo $MhswID." - <b>".$Nama."</b>"; ?></td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>To Account</b></font></td>
												<td>
													<select name="RekeningID" class="form-control" required>
														<option value=''>-- ACCOUNT --</option>
														<?php
															if($rowrekening)
															{
																foreach($rowrekening as $rr)
																{
																	echo "<option value='$rr->RekeningID'>$rr->RekeningID - $rr->Nama - $rr->Bank</option>";
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Paid From Bank</b></font></td>
												<td>
													<input type="text" name="Bank" class="form-control" required>
													Fill with `CASH/DEPOSIT` when paid with cash/deposit.
												</td>
											</tr>
											<tr>
												<td align="right">Reference ID</td>
												<td>
													<input type="text" name="BuktiSetoran" class="form-control">
												</td>
											</tr>
											<?php
												if($deposit != "")
												{
													$dep = $deposit;
											?>
													<tr>
														<td align="right"><font color="blue"><b>Deposit</b></font></td>
														<td align="right">
															<font color="blue"><b><?php echo formatRupiah($deposit); ?></b></font>
															<br/>
															<font color="red"><b>You must be use deposit first.</b></font>
														</td>
													</tr>
											<?php
												}
												else
												{
													$dep = 0;
												}
											?>
											<tr>
												<td align="right"><font color="red"><b>Payment Date</b></font></td>
												<td>
													<input type="text" name="Tanggal" id="datepicker1" class="form-control" required>
													<input type="hidden" name="Deposit" value="<?php echo $dep; ?>">
												</td>
											</tr>
											<tr>
												<td align="right">Note</td>
												<td>
													<textarea name="Keterangan" class="form-control"></textarea>
												</td>
											</tr>
										</table>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="box">
								<div class="box-body">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Bills</th>
													<th>Paid</th>
													<?php
														if($deposit != "")
														{
															echo "<th>Type</th>";
														}
													?>
													<th>Input</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 0;
													$no2 = 0;
													$totpot1 = 0;
													$totpot2 = 0;
													if($detail)
													{
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOTMhswRef = $row->BIPOTMhswRef;
																if($BIPOTMhswRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$BIPOTID = $row->BIPOTID;
																	$BIPOTNamaID_detail = $res['BIPOTNamaIDRef'];
																	$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
																	if(!$resref_detail)
																	{
																		if($deposit != "")
																		{
																			$coldep = "
																						<td>-</td>
																						";
																		}
																		else
																		{
																			$coldep = "";
																		}
																		$no++;
																		echo"
																			<tr class='warning'>
																				<td>$no</td>
																				<td><b>".$res['Nama']."</b>$TambahanNama</td>
																				<td><p align='right'>(".formatRupiah($row->Besar).")</p></td>
																				$coldep
																				<td><p align='right'>-</p></td>
																				<td>-</td>
																			</tr>
																			";
																		$totpot1 = $totpot1 + $row->Besar;
																		$totpot2 = $totpot2 + $row->Dibayar;
																	}
																}
															}
														}
														$totbi1 = 0;
														$totbi2 = 0;
														foreach($detail as $row)
														{
															$BIPOTNamaID = $row->BIPOTNamaID;
															$res = $this->m_master->PTL_select($BIPOTNamaID);
															if($row->TrxID == "1")
															{
																$no++;
																$no2++;
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOTMhswRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_pym_detail_ref_cek($BIPOTID,$BIPOTMhswRef,$MhswID);
																$ref_detail = "";
																$ref_jumlah = "";
																$ref_jumlah2 = "";
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$nm = $resref_detail2['Nama'];
																		}
																		$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$nm."(".formatRupiah($rd->Besar).")";
																		$ref_jumlah2 = $ref_jumlah2 + $rd->Besar;
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																$besar = $row->Besar - $ref_jumlah2;
																$readonly = "";
																if($besar == $row->Dibayar)
																{
																	$readonly = "readonly";
																}
																if($deposit != "")
																{
																	$coldep = "
																				<td>
																					<select name='type$no2'>
																						<option value='R'>Regular</option>
																						<option value='D'>Deposit</option>
																					</select>
																				</td>
																				";
																}
																else
																{
																	$coldep = "<input type='hidden' name='type$no2' value='R'>";
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td>
																			<b>".$res['Nama']."</b></b>
																			$TambahanNama
																			$ref_detail $ref_jumlah
																		</td>
																		<td><p align='right'>".formatRupiah($besar)."</p></td>
																		<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																		$coldep
																		<td>
																			<input type='hidden' name='BIPOTID$no2' value='$BIPOTID'>
																			<input type='hidden' name='bipotmhsw$no2' value='$row->BIPOTMhswID'>
																			<input type='hidden' name='bipotnama$no2' value='$row->BIPOTNamaID'>
																			<input type='hidden' name='nama$no2' value='$res[Nama]'>
																			<input type='hidden' name='besar$no2' value='$besar'>
																			<input type='hidden' name='dibayar$no2' value='$row->Dibayar'>
																			<input $readonly type='text' name='payment$no2'
																				style='text-align:right;' onmouseover='this.focus()'
																				id='inputku$no2' onkeydown='return numbersonly(this, event);'
																				onkeyup='hitung();javascript:tandaPemisahTitik(this);' autocomplete='off'>
																		</td>
																	</tr>
																	";
																$totbi1 = $totbi1 + ($row->Besar - $ref_jumlah2);
																$totbi2 = $totbi2 + $row->Dibayar;
															}
														}
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
													$total = $no2;
													$coldep2 = "";
													if($deposit != "")
													{
														$coldep2 = "<td style='background-color:#D8E5D8;border:5px;'></td>";
													}
													echo"
														<tr>
															<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
															<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi1 - $totpot1)."</p></td>
															<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi2 - $totpot2)."</p></td>
															$coldep2
															<td style='background-color:#D8E5D8;border:5px;'>
																<input type='hidden' name='KHSID' value='$KHSID'>
																<input type='hidden' name='MhswID' value='$MhswID'>
																<input type='hidden' name='TahunID' value='$TahunID'>
																<input type='hidden' name='total' value='$total'>
																<input readonly type='text' style='text-align:right;' class='hasil' value=''/>
															</td>
														</tr>
														<tr>
															<td style='background-color:#D8E5D8;border:5px;' colspan='2'></td>
															<td style='background-color:#D8E5D8;border:5px;'></td>
															<td style='background-color:#D8E5D8;border:5px;'></td>
															$coldep2
															<td style='background-color:#D8E5D8;border:5px;'>";
												?>
																<input type="submit" value="Submit" id="my_button" class="btn btn-primary" onclick="return confirm('Are you sure want to CREATE PAYMENT this Student?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
												<?php
														echo "</td>
														</tr>
														";
												?>
											</tbody>
										</table>
									</form>
									<input type="hidden" class="total" name="total" value="<?php echo (($totbi1 - $totpot1) - ($totbi2 - $totpot2)); ?>"/>
									<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.3.min.js"></script>
									<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
									<script type="text/javascript">
										var $j = jQuery.noConflict();
										function hitung()
										{
											<?php
												for($i=1;$i<=$total;$i++)
												{
											?>
													bayar<?php echo $i; ?> = bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('inputku<?php echo $i; ?>').value)));
											<?php
												}
											?>
											var total = $j(".total").val();
											jumlah = 0;
											<?php
												for($i2=1;$i2<=$total;$i2++)
												{
													if($i2 == 1)
													{
											?>
														jumlah = bayar<?php echo $i2; ?>;
														sisa = total - bayar<?php echo $i2; ?>;
											<?php
													}
													else
													{
											?>
														jumlah = jumlah - ( - bayar<?php echo $i2; ?>);
														sisa = sisa - bayar<?php echo $i2; ?>;
											<?php
													}
												}
											?>
											hasil = tandaPemisahTitik(jumlah);
											hasil_akhir = hasil.replace("-","");
											if(sisa < 0)
											{
												window.alert('TOTAL PAYMENT EXCEEDS THE BLLS. PLEASE, CHECK YOUR LAST INPUT!');
											}
											else
											{
												$j(".hasil").val(hasil_akhir);
											}
										}
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>