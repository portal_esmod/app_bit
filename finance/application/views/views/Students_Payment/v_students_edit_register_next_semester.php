				<section class="content-header">
					<h1>
						Students Payment
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-files-o"></i>Students Payment</a></li>
						<li><a href="<?php echo site_url("students_payment/ptl_edit/$KHSID"); ?>">Master Cost and Discounts</a></li>
						<li class="active">Register To Next Semester</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Students</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of students.
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students_payment/ptl_filter_regiter_tahun"); ?>" method="POST">
														<?php
															$cektahun = $this->session->userdata('pym_filter_register_tahun');
															$font = "";
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	$f = "";
																	if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																	{
																		$font = "style='background-color: #FFCD41;'";
																	}
																}
															}
														?>
														<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- ACADEMIC YEAR --</option>
															<?php
																if($rowtahun)
																{
																	foreach($rowtahun as $rt)
																	{
																		// if($rt->NA == "N")
																		// {
																			$f = "";
																			if($rt->NA == "N")
																			{
																				$f = "style='background-color: #5BB734;'";
																			}
																			echo "<option value='$rt->TahunID' $f";
																			if($cektahun == $rt->TahunID)
																			{
																				echo "selected";
																			}
																			echo ">$rt->TahunID - $rt->Nama</option>";
																		// }
																	}
																}
															?>
														</select>
														<input type="hidden" name="KHSID" value="<?php echo $KHSID; ?>">
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>KHSID</th>
													<th>Year ID</th>
													<th>Program</th>
													<th>Prodi</th>
													<th>Class</th>
													<th>Language</th>
													<th>Specialization</th>
													<th>Status</th>
													<th>Semester</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowkhs)
													{
														$no = 0;
														foreach($rowkhs as $row)
														{
															$no++;
															$TahunID = $row->TahunID;
															$resyear = $this->m_year->PTL_select($TahunID);
															$NamaTahun = "";
															if($resyear)
															{
																$NamaTahun = $resyear['Nama'];
															}
															$ProgramID = $row->ProgramID;
															$resprog = $this->m_program->PTL_select($ProgramID);
															$NamaProgram = "";
															if($resprog)
															{
																$NamaProgram = $resprog['Nama'];
															}
															$ProdiID = $row->ProdiID;
															$resprod = $this->m_prodi->PTL_select($ProdiID);
															$NamaProdi = "";
															if($resprod)
															{
																$NamaProdi = $resprod['Nama'];
															}
															$KelasID = $row->KelasID;
															$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
															$nmkelas = "";
															if($reskelas)
															{
																$nmkelas = $reskelas['Nama'];
															}
															$kelas = "";
															if($ProgramID == "INT")
															{
																$kelas = "O".$nmkelas;
															}
															else
															{
																$kelas = $row->TahunKe.$nmkelas;
															}
															$bahasa = "";
															if($row->languageID == "ENG")
															{
																$bahasa = "ENGLISH";
															}
															if($row->languageID == "FRE")
															{
																$bahasa = "FRENCH";
															}
															$SpesialisasiID = $row->SpesialisasiID;
															$spec = "";
															if($SpesialisasiID == 0)
															{
																$spec = "";
															}
															else
															{
																$resspec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																$spec = "";
																if($resspec)
																{
																	$spec = $resspec['Nama'];
																}
															}
															$StatusMhswID = $row->StatusMhswID;
															$resstatus = $this->m_status->PTL_select($StatusMhswID);
															$status = "";
															if($resstatus)
															{
																$status = $resstatus['Nama'];
															}
															echo "
																<tr>
																	<td>$no</td>
																	<td>$row->KHSID</td>
																	<td title='Year Name : $NamaTahun'>$row->TahunID</td>
																	<td title='Program Name : $NamaProgram'>$row->ProgramID</td>
																	<td title='Prodi Name : $NamaProdi'>$row->ProdiID</td>
																	<td>$kelas</td>
																	<td>$bahasa</td>
																	<td>$spec</td>
																	<td>$status</td>
																	<td>SEMESTER $row->Sesi</td>
																</tr>
																";
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='10' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
										<?php
											if($cektahun != "")
											{
										?>
												<div class="alert alert-<?php echo $notif; ?>">
													<center>
														<?php echo $pesan; ?>
													</center>
												</div>
										<?php
											}
											if(($cektahun != "") AND ($notif == "danger"))
											{
										?>
													<table class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>#</th>
																<th>KHSID</th>
																<th>Year ID</th>
																<th>Program</th>
																<th>Prodi</th>
																<th>Class</th>
																<th>Language</th>
																<th>Specialization</th>
																<th>Status</th>
																<th>Semester</th>
															</tr>
														</thead>
														<tbody>
															<?php
																$no = 1;
																$StatusMhswID = $StatusMhsw;
																$resstatus = $this->m_status->PTL_select($StatusMhswID);
																$status = "";
																if($resstatus)
																{
																	$status = $resstatus['Nama'];
																}
																$SpesialisasiID = $Spesialisasi;
																$spec = "";
																if($SpesialisasiID == 0)
																{
																	$spec = "";
																}
																else
																{
																	$resspec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																	$spec = "";
																	if($resspec)
																	{
																		$spec = $resspec['Nama'];
																	}
																}
																$bahasa = "";
																if($languageID == "ENG")
																{
																	$bahasa = "ENGLISH";
																}
																if($languageID == "FRE")
																{
																	$bahasa = "FRENCH";
																}
																echo "
																	<tr>
																		<td>$no</td>
																		<td><i>NULL</i></td>
																		<td>$tahun</td>
																		<td>";
															?>
																			<form action="<?php echo site_url("students_payment/ptl_filter_next_jur"); ?>" method="POST">
																				<select name="ProgramID" title="Filter by Program" onchange="this.form.submit()">
																					<?php
																						if($this->session->userdata('pym_next_filter_jur') == "")
																						{
																							$cekjurusan = $ProgramID;
																						}
																						else
																						{
																							$cekjurusan = $this->session->userdata('pym_next_filter_jur');
																						}
																						echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
																						echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
																						echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
																					?>
																				</select>
																				<input type="hidden" name="CekKHSID" value="<?php echo $KHSID; ?>">
																				<noscript><input type="submit" value="Submit"></noscript>
																			</form>
												<form action="<?php echo site_url("students_payment/ptl_register_next_semester_insert"); ?>" method="POST">
															<?php
																echo "</td>
																		<td>";
															?>
																			<select name="ProdiID" title="Filter by Prodi" required>
																				<option value=''>-- PRODI --</option>
																				<?php
																					if($cekjurusan == "INT")
																					{
																						if($programd1)
																						{
																							foreach($programd1 as $d1)
																							{
																								echo "<option value='$d1->ProdiID'";
																								if($ProdiID == $d1->ProdiID)
																								{
																									echo "selected";
																								}
																								echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																							}
																						}
																					}
																					if($cekjurusan == "REG")
																					{
																						if($programd3)
																						{
																							foreach($programd3 as $d3)
																							{
																								echo "<option value='$d3->ProdiID'";
																								if($ProdiID == strtoupper($d3->ProdiID))
																								{
																									echo "selected";
																								}
																								echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																							}
																						}
																					}
																					if($cekjurusan == "SC")
																					{
																						if($shortcourse)
																						{
																							foreach($shortcourse as $sc)
																							{
																								echo "<option value='$sc->KursusSingkatID'";
																								if($ProdiID == $sc->KursusSingkatID)
																								{
																									echo "selected";
																								}
																								echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																							}
																						}
																					}
																				?>
																			</select>
															<?php
																		echo "<input type='hidden' name='ProgramID' value='$cekjurusan'>
																			<input type='hidden' name='languageID' value='$languageID'>
																			<input type='hidden' name='SpesialisasiID' value='$Spesialisasi'>
																			<input type='hidden' name='StatusMhswID' value='$StatusMhsw'>
																			<input type='hidden' name='MhswID' value='$MhswID'>
																			<input type='hidden' name='MaxSKS' value='$MaxSKS'>
																			<input type='hidden' name='TahunID' value='$tahun'>
																			<input type='hidden' name='KHSID' value='$KHSID'>
																		</td>
																		<td>
																			<select name='TahunKe' required>
																				<option value=''"; if($TahunKe == ""){ echo "selected";} echo ">-- CHOOSE --</option>
																				<option value='1'"; if($TahunKe == "1"){ echo "selected";} echo ">1</option>
																				<option value='2'"; if($TahunKe == "2"){ echo "selected";} echo ">2</option>
																				<option value='3'"; if($TahunKe == "3"){ echo "selected";} echo ">3</option>
																			</select>
																		</td>
																		<td>$bahasa</td>
																		<td>$spec</td>
																		<td>$status</td>
																		<td>
																			<select name='Sesi' required>
																				<option value=''"; if($Sesi == ""){ echo "selected";} echo ">-- CHOOSE --</option>
																				<option value='1'"; if($Sesi == 1){ echo "selected";} echo ">SEMESTER 1</option>
																				<option value='2'"; if($Sesi == 2){ echo "selected";} echo ">SEMESTER 2</option>
																				<option value='3'"; if($Sesi == 3){ echo "selected";} echo ">SEMESTER 3</option>
																				<option value='4'"; if($Sesi == 4){ echo "selected";} echo ">SEMESTER 4</option>
																				<option value='5'"; if($Sesi == 5){ echo "selected";} echo ">SEMESTER 5</option>
																				<option value='6'"; if($Sesi == 6){ echo "selected";} echo ">SEMESTER 6</option>
																			</select>
																		</td>
																	</tr>
																	";
															?>
														</tbody>
													</table>
													<center>
														<input type="submit" value="Register" onclick="return confirm('Are you sure want to register this student to next semester?')" class="btn btn-info">
													</center>
												</form>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>