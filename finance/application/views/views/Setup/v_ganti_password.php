				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Change Password
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("login/ptl_ganti_password"); ?>"><i class="fa  fa-wrench"></i> Change Password</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box box-primary">
									<div class="box-header">
										<h3 class="box-title">Change Password</h3>
									</div>
									<form role="form" action="<?php echo site_url("login/ptl_update_password"); ?>" method="POST" />
										<div class="box-body">
											<div class="form-group">
												<label>Old Password</label>
												<input type="password" name="pass_lama" class="form-control" required>
											</div>
											<div class="form-group">
												<label>New Password</label>
												<input type="password" name="pass_baru1" class="form-control" required>
											</div>
											<div class="form-group">
												<label>Confirm New Password</label>
												<input type="password" name="pass_baru2" class="form-control" required>
											</div>
										</div>
										<div class="box-footer">
											<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>