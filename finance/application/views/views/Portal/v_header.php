<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Esmod Jakarta - Finance</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
		
		<?php
			// if(@$date == "USE")
			// {
		?>
				<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/default.css" type="text/css">
		<?php
			// }
		?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116304365-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-116304365-1');
		</script>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a href="<?php echo site_url("login/ptl_home"); ?>" class="logo">
					<span class="logo-mini"><b>EJ</b>F</span>
					<span class="logo-lg"><b>Esmod</b> Finance</span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!--<li class="dropdown messages-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-envelope-o"></i>
									<span class="label label-success">4</span>
								</a>
								<ul class="dropdown-menu">
									<li class="header">You have 1 messages</li>
									<li>
										<ul class="menu">
											<li>
												<a href="#">
													<div class="pull-left">
														<img src="<?php echo base_url(); ?>assets/dashboard/dist/img/avatar2.png" class="img-circle" alt="User Image"/>
													</div>
													<h4>
														Ayu
														<small><i class="fa fa-clock-o"></i> 5 mins</small>
													</h4>
													<p>Apakah pembayaran prospek saya sudah diterima?</p>
												</a>
											</li>
										</ul>
									</li>
									<li class="footer"><a href="#">See All Messages</a></li>
								</ul>
							</li>
							<li class="dropdown notifications-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-bell-o"></i>
									<span class="label label-warning">10</span>
								</a>
								<ul class="dropdown-menu">
									<li class="header">You have 10 notifications</li>
									<li>
										<ul class="menu">
											<li>
												<a href="#">
													<i class="fa fa-users text-aqua"></i> 5 new students
												</a>
											</li>
											<li>
												<a href="#">
													<i class="fa fa-warning text-yellow"></i> Don't forget to buy something
												</a>
											</li>
											<li>
												<a href="#">
													<i class="fa fa-users text-red"></i> 5 new staff joined
												</a>
											</li>
											<li>
												<a href="#">
													<i class="fa fa-shopping-cart text-green"></i> 25 purchase order of Bookstore
												</a>
											</li>
											<li>
												<a href="#">
													<i class="fa fa-user text-red"></i> You changed your username
												</a>
											</li>
										</ul>
									</li>
									<li class="footer"><a href="#">View all</a></li>
								</ul>
							</li>
							<li class="dropdown tasks-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-flag-o"></i>
									<span class="label label-danger">9</span>
								</a>
								<ul class="dropdown-menu">
									<li class="header">You have 9 tasks</li>
									<li>
										<ul class="menu">
											<li>
												<a href="#">
													<h3>
														Design some buttons
														<small class="pull-right">20%</small>
													</h3>
													<div class="progress xs">
														<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
															<span class="sr-only">20% Complete</span>
														</div>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<h3>
														Create a nice theme
														<small class="pull-right">40%</small>
													</h3>
													<div class="progress xs">
														<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
															<span class="sr-only">40% Complete</span>
														</div>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<h3>
														Some task I need to do
														<small class="pull-right">60%</small>
													</h3>
													<div class="progress xs">	
														<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
															<span class="sr-only">60% Complete</span>
														</div>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<h3>
														Make beautiful transitions
														<small class="pull-right">80%</small>
													</h3>
													<div class="progress xs">
														<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
															<span class="sr-only">80% Complete</span>
														</div>
													</div>
												</a>
											</li>
										</ul>
									</li>
									<li class="footer">
										<a href="#">View all tasks</a>
									</li>
								</ul>
							</li>-->
							<li class="dropdown user user-menu">
								<?php
									$foto = "ptl_storage/foto_umum/user.jpg";
									$foto_preview = "ptl_storage/foto_umum/user.jpg";
									$Fotoku = @$_COOKIE["foto"];
									if($Fotoku != "")
									{
										$foto = "../hris/system_storage/foto_karyawan/$Fotoku";
										$exist = file_exists_remote(base_url("$foto"));
										if($exist)
										{
											$foto = $foto;
											$source_photo = base_url("$foto");
											$info = pathinfo($source_photo);
											$foto_preview = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
											$exist1 = file_exists_remote(base_url("$foto_preview"));
											if($exist1)
											{
												$foto_preview = $foto_preview;
											}
											else
											{
												$foto_preview = $foto;
											}
										}
										else
										{
											$foto = "ptl_storage/foto_umum/user.jpg";
											$foto_preview = "ptl_storage/foto_umum/user.jpg";
										}
									}
								?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url("$foto_preview"); ?>" class="user-image" alt="User Image"/>
									<span class="hidden-xs"><?php echo $_COOKIE["nama"]; ?></span>
								</a>
								<ul class="dropdown-menu">
									<li class="user-header">
										<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
											<img class="img-polaroid img-circle" src="<?php echo base_url("$foto_preview"); ?>" width="100px" alt=""/>
										</a>
										<p>
											<?php echo $_COOKIE["nama"]." - ".$_COOKIE["akses"]; ?>
											<!--<small>Member since <?php echo $_COOKIE["awal_kontrak"]; ?></small>-->
										</p>
									</li>
									<li class="user-body">
										<div class="col-xs-4 text-center">
											<a href="<?php echo site_url("login/ptl_ganti_password"); ?>">Change Password</a>
										</div>
										<!--<div class="col-xs-4 text-center">
											<a href="<?php echo site_url("login/ptl_logout"); ?>">Status</a>
										</div>
										<div class="col-xs-4 text-center">
											<a href="<?php echo site_url("login/ptl_activity"); ?>">Activity</a>
										</div>
									</li>-->
									<li class="user-footer">
										<!--<div class="pull-left">
											<a href="<?php echo site_url("login/ptl_profil"); ?>" class="btn btn-default btn-flat">Profile</a>
										</div>-->
										<div class="pull-right">
											<a href="<?php echo site_url("login/ptl_logout"); ?>" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
							<!--<li>
								<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
							</li>-->
						</ul>
					</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<div class="user-panel">
						<div class="pull-left image">
							<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
								<img class="img-polaroid img-circle" src="<?php echo base_url("$foto_preview"); ?>" width="50px" alt=""/>
							</a>
						</div>
						<div class="pull-left info">
							<p><?php echo $_COOKIE["nama"]; ?></p>
							<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						</div>
					</div>
					<?php
						$CI =& get_instance();
						$this->load->model('m_aplikan');
						$rowpending = $CI->m_aplikan->PTL_all_semester_pending();
						$pending = count($rowpending);
						$rowpostpone = $CI->m_aplikan->PTL_all_new_postpone();
						$postpone = count($rowpostpone);
						$rowdone = $CI->m_aplikan->PTL_all_new_done();
						$done = count($rowdone);
						$total_new = $pending + $postpone + $done;
					?>
					<ul class="sidebar-menu">
						<?php $menu = $this->session->userdata('menu'); ?>
						<li class="header">MAIN NAVIGATION</li>
						<li class="<?php if($menu == "home"){ echo "active";} ?> treeview">
							<a href="<?php echo site_url("login/ptl_home"); ?>">
								<i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
							</a>
						</li>
						<li class="<?php if($menu == "form"){ echo "active";} ?> treeview">
							<a href="<?php echo site_url("payment_form"); ?>">
								<i class="fa fa-money "></i> <span>Payment Form</span></i>
							</a>
						</li>
						<li class="<?php if($menu == "new_students"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-files-o"></i>
								<span>New Students</span>
								<small class="label pull-right bg-blue"><?php /*echo $total_new;*/ ?></small>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<small class="label pull-right bg-red"><?php /*echo $pending;*/ ?></small>
								<li><a href="<?php echo site_url("new_students/ptl_pending"); ?>"><i class="fa fa-circle-o text-red"></i> Pending Payment</a></li>
								<small class="label pull-right bg-yellow"><?php /*echo $postpone;*/ ?></small>
								<li><a href="<?php echo site_url("new_students/ptl_postpone"); ?>"><i class="fa fa-circle-o text-yellow"></i> Postpone</a></li>
								<li><a href="<?php echo site_url("new_students_search"); ?>"><i class="fa fa-circle-o text-green"></i> Searching Payment</a></li>
								<small class="label pull-right bg-aqua"><?php /*echo $done;*/ ?></small>
								<li><a href="<?php echo site_url("new_students"); ?>"><i class="fa fa-circle-o text-aqua"></i> List</a></li>
							</ul>
						</li>
						<li class="<?php if($menu == "students_payment"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-files-o"></i>
								<span>Students Payment</span>
								<small class="label pull-right bg-red"></small>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<small class="label pull-right bg-red"></small>
								<li><a href="<?php echo site_url("students_payment"); ?>"><i class="fa fa-circle-o text-red"></i> List Payment</a></li>
								<li><a href="<?php echo site_url("students_payment/ptl_report"); ?>"><i class="fa fa-circle-o text-white"></i> Report</a></li>
								<li><a href="<?php echo site_url("students_payment_search"); ?>"><i class="fa fa-circle-o text-green"></i> Searching Payment</a></li>
								<li><a href="<?php echo site_url("virtual"); ?>"><i class="fa fa-circle-o text-aqua"></i> Virtual Account</a></li>
							</ul>
						</li>
						<!--<li class="<?php if($menu == "scholarship"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-files-o"></i>
								<span>Scholarship</span>
								<small class="label pull-right bg-red">11</small>
							</a>
							<ul class="treeview-menu">
								<small class="label pull-right bg-red">11</small>
								<li><a href="<?php echo site_url("scholarship/ptl_pending"); ?>"><i class="fa fa-circle-o text-red"></i> Pending Payment</a></li>
								<li><a href="<?php echo site_url("scholarship"); ?>"><i class="fa fa-circle-o text-yellow"></i> List</a></li>
								<li><a href="<?php echo site_url("scholarship/ptl_report"); ?>"><i class="fa fa-circle-o text-aqua"></i> Finance Report</a></li>
							</ul>
						</li>
						<li class="<?php if($menu == "payment_corrections"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-files-o"></i>
								<span>Payment Corrections</span>
								<small class="label pull-right bg-red">11</small>
							</a>
							<ul class="treeview-menu">
								<small class="label pull-right bg-red">11</small>
								<li><a href="<?php echo site_url("payment_corrections/ptl_pending"); ?>"><i class="fa fa-circle-o text-red"></i> Pending Corrections</a></li>
								<li><a href="<?php echo site_url("payment_corrections"); ?>"><i class="fa fa-circle-o text-yellow"></i> List</a></li>
								<li><a href="<?php echo site_url("payment_corrections/ptl_report"); ?>"><i class="fa fa-circle-o text-aqua"></i> Corrections Report</a></li>
							</ul>
						</li>
						<li class="<?php if($menu == "charts"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-pie-chart"></i>
								<span>Charts</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo site_url("charts/ptl_new_students"); ?>"><i class="fa fa-circle-o"></i> New Students</a></li>
								<li><a href="<?php echo site_url("charts"); ?>"><i class="fa fa-circle-o"></i> Students Payment</a></li>
								<li><a href="<?php echo site_url("charts/ptl_corrections_payment"); ?>"><i class="fa fa-circle-o"></i> Corrections Payment</a></li>
							</ul>
						</li>-->
						<li class="<?php if($menu == "master"){ echo "active";} ?> treeview">
							<a href="#">
								<i class="fa fa-cc-mastercard"></i>
								<span>Master Costs</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo site_url("master"); ?>"><i class="fa fa-circle-o"></i> Master Cost and Discounts</a></li>
								<li><a href="<?php echo site_url("master/ptl_bipot"); ?>"><i class="fa fa-circle-o"></i> Cost and Discounts of Student</a></li>
							</ul>
						</li>
						<li class="<?php if($menu == "bank_account"){ echo "active";} ?> treeview">
							<a href="<?php echo site_url("bank_account"); ?>">
								<i class="fa fa-cc-visa"></i> <span>Bank Account</span></i>
							</a>
						</li>
						<li class="<?php if($menu == "calendar"){ echo "active";} ?> treeview">
							<a href="<?php echo site_url("calendar"); ?>">
								<i class="fa fa-calendar-o"></i> <span>Academic Calendar</span></i>
							</a>
						</li>
						<li class="<?php if($menu == "students"){ echo "active";} ?> treeview">
							<a href="<?php echo site_url("students"); ?>">
								<i class="fa fa-user"></i> <span>Students</span></i>
							</a>
						</li>
					</ul>
				</section>
			</aside>
			<div class="content-wrapper">