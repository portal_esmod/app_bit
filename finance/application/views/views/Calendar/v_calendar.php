				<section class="content-header">
					<h1>
						Calendar
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li class="active"><i class="fa fa-files-o"></i>Calendar</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Calendar This Year</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Title</th>
												<th>Program</th>
												<th>Prodi</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$h = "-7";
												$hm = $h * 60;
												$ms = $hm * 60;
												$tanggal = gmdate("Y-m-d", time()-($ms));
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														$b1 = "";
														$b2 = "";
														if($tanggal >= (str_replace("-","",$row->tanggal_mulai)) AND ($tanggal <= str_replace("-","",$row->tanggal_selesai)))
														{
															$b1 = "<font color='red'><b>";
															$b2 = "</font></b>";
														}
														echo "<tr>
																<td>$no</td>
																<td>$b1".tgl_singkat_eng($row->tanggal_mulai)."$b2</td>
																<td>$b1".tgl_singkat_eng($row->tanggal_selesai)."$b2</td>
																<td>$b1$row->nama$b2</td>
																<td>$b1$row->ProgramID$b2</td>
																<td>$b1$row->ProdiID$b2</td>
															</tr>";
														$no++;
													}
												}
												else
												{
													echo "<tr>
															<td colspan='6' align='center'><font color='red'><b>No data available</b></font></td>
														</tr>";
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>