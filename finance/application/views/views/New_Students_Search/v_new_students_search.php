				<section class="content-header">
					<h1>
						New Students Payment Search
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students_payment_search"); ?>"><i class="fa fa-files-o"></i> New Students Payment Search</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of New Students Payment Search</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of new students payment search.
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("new_students_search/ptl_filter_tanggal"); ?>" method="POST" id="formId">
														<input type="text" name="cektanggal" value="<?php echo $today; ?>" id="Date1" class="form-control">
													</form>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>TRX ID</th>
													<th><u>PMB</u><br>NIM</th>
													<th>Photo</th>
													<th>Name</th>
													<th>Program<hr>Prodi</th>
													<th>Period ID</th>
													<th>Account ID</th>
													<th>Total</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															$PMBID = $row->PMBID;
															$res = $this->m_aplikan->PTL_select_pmb($PMBID);
															$AplikanID = "";
															$ProgramID = "";
															$ProdiID = "";
															$PMBPeriodID = "";
															$nama = "";
															$foto = "ptl_storage/foto_umum/user.jpg";
															$foto_preview = "ptl_storage/foto_umum/user.jpg";
															if($res)
															{
																$AplikanID = $res["AplikanID"];
																$ProgramID = $res["ProgramID"];
																$ProdiID = $res["ProdiID"];
																$PMBPeriodID = $res["PMBPeriodID"];
																$nama = $res["Nama"];
																if($res["foto"] != "")
																{
																	$foto = "../academic/ptl_storage/foto_mahasiswa/".$res["foto"];
																	$exist = file_exists_remote(base_url("$foto"));
																	if($exist)
																	{
																		$foto = $foto;
																		$source_photo = base_url("$foto");
																		$info = pathinfo($source_photo);
																		$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																		$exist1 = file_exists_remote(base_url("$foto_preview"));
																		if($exist1)
																		{
																			$foto_preview = $foto_preview;
																		}
																		else
																		{
																			$foto_preview = $foto;
																		}
																	}
																	else
																	{
																		$foto = "ptl_storage/foto_umum/user.jpg";
																		$foto_preview = "ptl_storage/foto_umum/user.jpg";
																	}
																}
															}
															$res3 = $this->m_program->PTL_select($ProgramID);
															$program = "";
															if($res3)
															{
																$program = $res3["Nama"];
															}
															$res4 = $this->m_prodi->PTL_select($ProdiID);
															$prodi = "";
															if($res4)
															{
																$prodi = $res4["Nama"];
															}
															$RekeningID = $row->RekeningID;
															$resrek = $this->m_rekening->PTL_select($RekeningID);
															$Bank = '';
															if($resrek)
															{
																$Bank = $resrek['Bank'];
															}
															echo "
																<tr>
																	<td>$no</td>
																	<td>$row->BayarMhswID</td>
																	<td><u>$AplikanID</u><br>$PMBID</td>
																	<td>
																		<a class='fancybox' title='$PMBID - $nama' href='".base_url("$foto")."' data-fancybox-group='gallery' >
																			<img class='img-polaroid' src='".base_url("$foto_preview")."' width='50px' alt='' />
																		</a>
																	</td>
																	<td>$nama</td>
																	<td>$program<hr/>$prodi</td>
																	<td>$PMBPeriodID</td>
																	<td>$Bank<br/>$row->RekeningID</td>
																	<td align='right'>".formatRupiah($row->Jumlah)."</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("new_students/ptl_edit/$AplikanID/$row->BayarMhswID")."' target='_blank'>Detail&nbsp;</a>
																	</td>
																</tr>
																";
															$no++;
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='12' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>