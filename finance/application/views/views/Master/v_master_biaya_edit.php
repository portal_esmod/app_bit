				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Edit Cost
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("master"); ?>"><i class="fa fa-cc-mastercard"></i> Master Cost and Discounts</a></li>
						<li class="active">Edit Cost</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Cost</h3>
								</div>
								<form role="form" action="<?php echo site_url("master/ptl_cost_update"); ?>" method="POST" />
									<div class="box-body">
										<div class="form-group">
											<label><font color="red">Sequence</font></label>
											<input type="number" name="Urutan" value="<?php echo $Urutan; ?>" class="form-control" placeholder="" required>
											<input type="hidden" name="BIPOTNamaID" value="<?php echo $BIPOTNamaID; ?>" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" placeholder="" required>
										</div>
										<div class="form-group">
											<label><font color="red">Account Id</font></label>
											<select name="RekeningID" multiple class="form-control" required>
												<?php
													if($rekening)
													{
														foreach($rekening as $r)
														{
															echo "<option value='$r->RekeningID'";
															if($RekeningID == $r->RekeningID)
															{
																echo "selected";
															}
															echo ">$r->RekeningID - $r->Bank - $r->Nama - $r->Cabang</option>";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group">
											<label><font color="red">Total</font></label>
											<input type="text" name="DefJumlah" value="<?php echo $DefJumlah; ?>" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off" required>
										</div>
										<div class="form-group">
											<label>Discount</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="KenaDenda" <?php if($KenaDenda == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="KenaDenda" <?php if($KenaDenda == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Penalty</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="DipotongBeasiswa" <?php if($DipotongBeasiswa == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="DipotongBeasiswa" <?php if($DipotongBeasiswa == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Deposit</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="Deposit" <?php if($Deposit == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="Deposit" <?php if($Deposit == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Not Active (NA)?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="NA" <?php if($NA == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="NA" <?php if($NA == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Note</label>
											<textarea name="Catatan" class="form-control" placeholder=""> <?php echo $Catatan; ?></textarea>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("master"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Submit" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>