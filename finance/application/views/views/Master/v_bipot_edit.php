				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Edit Master
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("master/ptl_bipot"); ?>"><i class="fa fa-cc-mastercard"></i>Cost and Discounts of Student</a></li>
						<li class="active">Edit Master</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Master</h3>
								</div>
								<form role="form" action="<?php echo site_url("master/ptl_bipot_update"); ?>" method="POST" />
									<div class="box-body">
										<div class="form-group">
											<label><font color="red">Year Code</font></label>
											<input type="int" name="Tahun" value="<?php echo $Tahun; ?>" class="form-control" placeholder="example: 2017/2018">
											<input type="hidden" name="BIPOTID" value="<?php echo $BIPOTID; ?>" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" placeholder="example: Intake September 2017">
										</div>
										<div class="form-group">
											<label><font color="red">Grace Period</font></label>
											<input type="text" name="jatuh_tempo" value="<?php echo $jatuh_tempo; ?>" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask >
										</div>
										<div class="form-group">
											<label><font color="red">Due Date</font></label>
											<input type="text" name="jatuh_tempo_akhir" value="<?php echo $jatuh_tempo_akhir; ?>" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask >
										</div>
										<div class="form-group">
											<label><font color="red">Minimum Payment</font></label>
											<input type="text" name="minimal_pembayaran" value="<?php echo $minimal_pembayaran; ?>" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off" required>
										</div>
										<div class="form-group">
											<label><font color="red">Penalty</font></label>
											<input type="text" name="denda" value="<?php echo $denda; ?>" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off">
											<font color="green"><b>Charged per day. Skip or not be filled if you want to fill zero.</b></font>
										</div>
										<div class="form-group">
											<label>Note</label>
											<textarea name="Catatan" class="form-control" placeholder=""> <?php echo $Catatan; ?></textarea>
										</div>
										<div class="form-group">
											<label>Default?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="Def" <?php if($Def == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="Def" <?php if($Def == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Short Semester?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="SP" <?php if($SP == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="SP" <?php if($SP == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Not Active (NA)?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="NA" <?php if($NA == "Y"){ echo "checked"; } ?> id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="NA" <?php if($NA == "N"){ echo "checked"; } ?> id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("master/ptl_bipot"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Submit" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
				<script type="text/javascript">
					$(function(){
						$("#datepicker").datepicker({
							showButtonPanel: true,
							dateFormat: 'yy-mm-dd',
							maxDate: '-16Y',
							showTime: true
						});
					});
				</script>