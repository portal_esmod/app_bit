				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Add Discounts
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("master/ptl_bipot/$BIPOTID"); ?>"><i class="fa fa-cc-mastercard"></i> Costs and Discounts of Student</a></li>
						<li class="active">Add Discounts</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Add Discounts</h3>
								</div>
									<div class="box-body">
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<form action="<?php echo site_url("master/ptl_filter_bipot_detail_discount_form"); ?>" method="POST">
												<select name="cekBIPOTNamaID" class="form-control" onchange="this.form.submit()" required>
													<?php
														$cekRekeningID = $this->session->userdata('cekRekeningID');
														if(($cekRekeningID == "NONE") OR ($cekRekeningID == ""))
														{
															$cRI = $this->session->userdata('cekDefJumlah');
														}
														else
														{
															$cRI = $RekeningID;
														}
														if(($cekDefJumlah == "NONE") OR ($cekDefJumlah == ""))
														{
															$cDJ = $this->session->userdata('cekDefJumlah');
														}
														else
														{
															$cDJ = $DefJumlah;
														}
														if($potongan)
														{
															$cekBIPOTNamaID = $this->session->userdata('cekBIPOTNamaID');
															echo "<option value=''>-- CHOOSE --</option>";
															foreach($potongan as $r)
															{
																echo "<option value='$r->BIPOTNamaID $BIPOTID'";
																if(($cekBIPOTNamaID == "NONE") OR ($cekBIPOTNamaID == ""))
																{
																	if($cekBIPOTNamaID == $r->BIPOTNamaID)
																	{
																		echo "selected";
																	}
																}
																else
																{
																	if($cekBIPOTNamaID == $r->BIPOTNamaID)
																	{
																		echo "selected";
																	}
																}
																echo ">$r->BIPOTNamaID - $r->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</div>
										<div class="form-group">
											<label>Account Id</label>
											<input readonly type="text" name="RekeningID" value="<?php echo $cRI; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Reference Total</label>
											<input readonly type="text" name="DefJumlah" value="<?php echo formatRupiah2($cDJ); ?>" style="text-align:right;" class="form-control" >
										</div>
								<form role="form" action="<?php echo site_url("master/ptl_bipot_detail_discount_insert"); ?>" method="POST" />
										<div class="form-group">
											<label>Additional Name</label>
											<input type="text" name="TambahanNama" class="form-control" >
											<input type="hidden" name="BIPOTID" value="<?php echo $BIPOTID; ?>" class="form-control" placeholder="" >
											<input type="hidden" name="BIPOTNamaID" value="<?php echo $cekBIPOTNamaID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label><font color="red">Total</font></label>
											<input type="text" name="Jumlah" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off">
											<font color="green"><b>Skip or not be filled if you want to fill zero.</b></font>
										</div>
										<div class="form-group">
											<label>Not Active (NA)?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="NA" id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="NA" id="optionsRadios2" value="N" checked>
														No
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("master/ptl_bipot/$BIPOTID"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Submit" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>