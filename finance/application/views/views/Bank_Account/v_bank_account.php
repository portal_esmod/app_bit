				<section class="content-header">
					<h1>
						Account
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("bank_account"); ?>"><i class="fa fa-cc-visa"></i> Account</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Account</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The following is a list of account.
									</div>
									<div class="table-responsive">
										<center>
											<a href="<?php echo site_url("bank_account/ptl_form"); ?>" class="btn btn-success">&nbsp;&nbsp;Add New&nbsp;&nbsp;</a>
											<br/>
											<br/>
										</center>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Account ID</th>
													<th>Name</th>
													<th>Bank Name</th>
													<th>Branch</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															echo "
																<tr>
																	<td>$no</td>
																	<td>$row->RekeningID</td>
																	<td>$row->Nama</td>
																	<td>$row->Bank</td>
																	<td>$row->Cabang</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("bank_account/ptl_edit/$row->RekeningID")."'>Edit&nbsp;</a>
																	</td>
																</tr>
																";
															$no++;
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='6' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>