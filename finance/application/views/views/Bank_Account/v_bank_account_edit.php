				<section class="content-header">
					<h1>
						Account
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("bank_account"); ?>"><i class="fa fa-cc-visa"></i> Account</a></li>
						<li class="active">Edit Data</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Data</h3>
								</div>
								<?php echo form_open_multipart('bank_account/ptl_update',array('name' => 'contoh')); ?>
									<div class="box-body">
										<div class="form-group">
											<label>Account ID</label>
											<input readonly type="text" name="RekeningID" value="<?php echo $RekeningID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Bank Name</label>
											<input type="text" name="Bank" value="<?php echo $Bank; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Branch</label>
											<input type="text" name="Cabang" value="<?php echo $Cabang; ?>" class="form-control" placeholder="" >
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("bank_account"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Update" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>