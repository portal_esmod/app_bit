				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
					$master_bipot = $this->session->userdata('master_bipot');
				?>
				<section class="content-header">
					<h1>
						Master Cost and Discounts
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("new_students/ptl_pending"); ?>"><i class="fa fa-files-o"></i>New Students</a></li>
						<li class="active">Master Cost and Discounts</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-4">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Master</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<?php
											$fstatus1 = "";
											$fstatus2 = "";
											if($this->session->userdata('new_student_status') == "NOT")
											{
												if($this->session->userdata('new_student_filter_tahun') == "")
												{
													$fstatus1 = "style='background-color: #FFCD41;'";
												}
												if($this->session->userdata('new_student_filter_sesi') == "")
												{
													$fstatus2 = "style='background-color: #FFCD41;'";
												}
											}
											if($cekBIPOTID == "YES")
											{
										?>
												<tr>
													<td><label>Payment Scheme</label></td>
													<td>:</td>
													<td>
														<?php
															$resbipot = $this->m_master->PTL_bipot_select($BIPOTID);
															if($resbipot)
															{
																echo $resbipot['BIPOTID'].' - '.$resbipot['Tahun'].' - '.$resbipot['Nama'];
															}
														?>
													</td>
												</tr>
												<?php
													if($MhswID == "")
													{
												?>
														<tr>
															<td <?php echo $fstatus1; ?>><label>Year To Register</label></td>
															<td>:</td>
															<td>
																<form action="<?php echo site_url("new_students/ptl_filter_tahun"); ?>" method="POST">
																	<?php
																		$cektahun = $this->session->userdata('new_student_filter_tahun');
																		$font = "";
																		if($rowtahun)
																		{
																			foreach($rowtahun as $rt)
																			{
																				$f = "";
																				if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																				{
																					$font = "style='background-color: #FFCD41;'";
																				}
																			}
																		}
																	?>
																	<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																		<option value='<?php echo " - $AplikanID"; ?>'>-- ACADEMIC YEAR --</option>
																		<?php
																			if($rowtahun)
																			{
																				foreach($rowtahun as $rt)
																				{
																					$f = "";
																					if($rt->NA == "N")
																					{
																						$f = "style='background-color: #5BB734;'";
																					}
																					if($rt->NA == "N")
																					{
																						echo "<option value='$rt->TahunID - $AplikanID' $f";
																						if($cektahun == $rt->TahunID)
																						{
																							echo "selected";
																						}
																						echo ">$rt->TahunID - $rt->Nama</option>";
																					}
																				}
																			}
																		?>
																	</select>
																	<noscript><input type="submit" value="Submit"></noscript>
																</form>
															</td>
														</tr>
														<tr>
															<td <?php echo $fstatus2; ?>><label>Semester To Register</label></td>
															<td>:</td>
															<td>
																<form action="<?php echo site_url("new_students/ptl_filter_sesi"); ?>" method="POST">
																	<select name="ceksesi" title="Filter by Semester" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																		<option value='<?php echo " - $AplikanID"; ?>'>-- SEMESTER --</option>
																		<?php
																			$ceksesi = $this->session->userdata('new_student_filter_sesi');
																			for($is=1;$is<=6;$is++)
																			{
																				echo "<option value='$is - $AplikanID' $f";
																				if($ceksesi == $is)
																				{
																					echo "selected";
																				}
																				echo ">$is</option>";
																			}
																		?>
																	</select>
																	<noscript><input type="submit" value="Submit"></noscript>
																</form>
															</td>
														</tr>
												<?php
													}
													else
													{
														$reskhs = $this->m_khs->PTL_select_bipot($MhswID,$BIPOTID);
														$year = "";
														$semester = "";
														$thn = "";
														if($reskhs)
														{
															$year = $reskhs["TahunID"];
															$TahunID = $reskhs["TahunID"];
															$semester = $reskhs["Sesi"];
															$resthn = $this->m_year->PTL_select($TahunID);
															if($resthn)
															{
																$thn = $resthn["Nama"];
															}
														}
												?>
														<tr>
															<td><label>Year To Register</label></td>
															<td>:</td>
															<td><?php echo $year." - ".$thn; ?></td>
														</tr>
														<tr>
															<td><label>Semester To Register</label></td>
															<td>:</td>
															<td><?php echo $semester; ?></td>
														</tr>
												<?php
													}
												?>
										<?php
											}
											else
											{
										?>
												<tr>
													<td><label>Payment Scheme</label></td>
													<td>:</td>
													<td>
														<form action="<?php echo site_url("new_students/ptl_filter_bipot"); ?>" method="POST">
															<select name="BIPOTID" class="form-control" onchange="this.form.submit()">
																<?php
																	if($rowrecord)
																	{
																		echo "<option value='0 $AplikanID'>-- CHOOSE --";
																		foreach($rowrecord as $r)
																		{
																			echo "<option value='$r->BIPOTID $AplikanID'";
																			if($master_bipot == $r->BIPOTID)
																			{
																				echo "selected";
																			}
																			echo ">$r->BIPOTID - $r->Tahun - $r->Nama</option>";
																		}
																	}
																?>
															</select>
															<noscript>
																<input type="submit" value="Submit">
															</noscript>
														</form>
													</td>
												</tr>
												<tr>
													<td <?php echo $fstatus1; ?>><label>Year To Register</label></td>
													<td>:</td>
													<td>
														<form action="<?php echo site_url("new_students/ptl_filter_tahun"); ?>" method="POST">
															<?php
																$cektahun = $this->session->userdata('new_student_filter_tahun');
																$font = "";
																if($rowtahun)
																{
																	foreach($rowtahun as $rt)
																	{
																		$f = "";
																		if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
																		{
																			$font = "style='background-color: #FFCD41;'";
																		}
																	}
																}
															?>
															<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																<option value='<?php echo " - $AplikanID"; ?>'>-- ACADEMIC YEAR --</option>
																<?php
																	if($rowtahun)
																	{
																		foreach($rowtahun as $rt)
																		{
																			$f = "";
																			if($rt->NA == "N")
																			{
																				$f = "style='background-color: #5BB734;'";
																			}
																			echo "<option value='$rt->TahunID - $AplikanID' $f";
																			if($cektahun == $rt->TahunID)
																			{
																				echo "selected";
																			}
																			echo ">$rt->TahunID - $rt->Nama</option>";
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
												</tr>
												<tr>
													<td <?php echo $fstatus2; ?>><label>Semester To Register</label></td>
													<td>:</td>
													<td>
														<form action="<?php echo site_url("new_students/ptl_filter_sesi"); ?>" method="POST">
															<select name="ceksesi" title="Filter by Semester" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
																<option value='<?php echo " - $AplikanID"; ?>'>-- SEMESTER --</option>
																<?php
																	$ceksesi = $this->session->userdata('new_student_filter_sesi');
																	for($is=1;$is<=6;$is++)
																	{
																		echo "<option value='$is - $AplikanID' $f";
																		if($ceksesi == $is)
																		{
																			echo "selected";
																		}
																		echo ">$is</option>";
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
												</tr>
										<?php
											}
										?>
										<tr>
											<td><label>Applicant ID</label></td>
											<td>:</td>
											<td><?php echo $AplikanID; ?></td>
										</tr>
										<tr>
											<td><label>PMBID</label></td>
											<td>:</td>
											<td><?php echo $PMBID; ?></td>
										</tr>
										<tr>
											<td><label>MhswID</label></td>
											<td>:</td>
											<td><?php echo $MhswID; ?></td>
										</tr>
										<tr>
											<td><label>PMB Period ID</label></td>
											<td>:</td>
											<td><?php echo $PMBPeriodID; ?></td>
										</tr>
										<tr>
											<td><label>Name</label></td>
											<td>:</td>
											<td><?php echo $Nama; ?></td>
										</tr>
										<tr>
											<td><label>Program</label></td>
											<td>:</td>
											<td><?php echo $ProgramID; ?></td>
										</tr>
										<tr>
											<td><label>Study Program</label></td>
											<td>:</td>
											<td><?php echo $ProdiID; ?></td>
										</tr>
										<tr>
											<td><label>Grade</label></td>
											<td>:</td>
											<td><?php echo $GradeNilai; ?></td>
										</tr>
										<tr>
											<td><label>Initial Status</label></td>
											<td>:</td>
											<td><?php echo $status_awal; ?></td>
										</tr>
										<tr>
											<td><label>Origin of the School</label></td>
											<td>:</td>
											<td><?php echo $AsalSekolah; ?></td>
										</tr>
										<tr>
											<td><label>Created By</label></td>
											<td>:</td>
											<td><?php echo $login_buat; ?></td>
										</tr>
										<tr>
											<td><label>Created Date</label></td>
											<td>:</td>
											<td><?php echo $tanggal_buat; ?></td>
										</tr>
										<tr>
											<td><label>Edited By</label></td>
											<td>:</td>
											<td><?php echo $login_edit; ?></td>
										</tr>
										<tr>
											<td><label>Edited Date</label></td>
											<td>:</td>
											<td><?php echo $tanggal_edit; ?></td>
										</tr>
										<tr>
											<td><label>Virtual Account</label></td>
											<td>:</td>
											<td>
												<form action="<?php echo site_url("new_students/ptl_virtual_account_insert"); ?>" method="POST">
													<input type="hidden" name="AplikanID" value="<?php echo $AplikanID; ?>"/>
													<input type="text" name="virtual_account" value="<?php echo $virtual_account; ?>" class="form-control"/>
													<input type="submit" value="SET" id="my_button" class="btn btn-primary"/>
												</form>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="box">
								<div class="box-header">
									<table class="table table-bordered table-striped">
										<tr>
											<td><h3 class="box-title">BIPOT Management</h3></td>
											<td align="right">
												<?php
													$penalty = 0;
													$min = 0;
													foreach($rowrecord as $r)
													{
														if($BIPOTID == $r->BIPOTID)
														{
															$min = $r->minimal_pembayaran;
														}
													}
													if(($cekBIPOTID == "YES") AND ($MhswID == ""))
													{
														if($detail)
														{
															$biaya2 = 0;
															$biaya_byr = 0;
															$potongan2 = 0;
															$potongan_byr = 0;
															foreach($detail as $row)
															{
																if($row->TrxID == "1")
																{
																	$biaya2 = $biaya2 + $row->Besar;
																	$biaya_byr = $biaya_byr + $row->Dibayar;
																}
																if($row->TrxID == "-1")
																{
																	$potongan2 = $potongan2 + $row->Besar;
																	$potongan_byr = $potongan_byr + $row->Dibayar;
																}
															}
															$total_bipot = $biaya2 - $potongan2 - $biaya_byr;
															if($total_bipot < 0)
															{
																$m = "-";
															}
															else
															{
																$m = "";
															}
														}
														if($rowrecord)
														{
															$h = "-7";
															$hm = $h * 60;
															$ms = $hm * 60;
															$tanggal = gmdate("Y-m-d", time()-($ms));
															foreach($rowrecord as $r)
															{
																if($BIPOTID == $r->BIPOTID)
																{
																	if($r->jatuh_tempo_akhir == "")
																	{
																		$cekjta = -1;
																		$penalty = 0;
																	}
																	else
																	{
																		$cekjta = hitungHari($r->jatuh_tempo_akhir,$tanggal);
																		$jt_a = str_replace("-","",$r->jatuh_tempo_akhir);
																		$nw_t = str_replace("-","",$tanggal);
																		$selisih_jta = 0;
																		if($jt_a < $nw_t)
																		{
																			$selisih_jta = $cekjta;
																		}
																		else
																		{
																			$cekjta = -$cekjta;
																		}
																		$penalty = $r->denda * $selisih_jta;
																	}
																}
															}
														}
														$cost = $biaya2 + $penalty;
														$pieces = $potongan2;
												?>
														<a href="<?php echo site_url("new_students/ptl_create_nim/".$AplikanID."_".$PMBID."/$min/$biaya_byr/$BIPOTID/$cost/$pieces"); ?>" onclick="return confirm('Are you sure want to create NIM this Student?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><h5 class="box-title">Create NIM</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td align="left">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_payment_form/$AplikanID"); ?>"><h5 class="box-title">Add Payment</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($cekBIPOTID != "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_bipot_create/".$AplikanID."_".$BIPOTID); ?>" onclick="return confirm('Are you sure want to process this BIPOT <?php echo $BIPOTID; ?>?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><h5 class="box-title">Process Bipot</h5></a>
													<?php
													}
													else
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_withdrawal_form/$AplikanID"); ?>"><h5 class="box-title">Withdrawal / Correction</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td align="left">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_pdf_invoice/$BIPOTID/$AplikanID/ESMOD%20JAKARTA"); ?>"><h5 class="box-title">Print Invoice</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_bipot_delete/".$AplikanID); ?>" onclick="return confirm('Are you sure want to delete this BIPOT <?php echo $master_bipot; ?>?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><h5 class="box-title">Delete All Bipot</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<tr>
											<td>
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_bipotmhs_cost_form/$PMBID/$BIPOTID"); ?>"><h5 class="box-title">Add Cost</h5></a>
												<?php
													}
												?>
											</td>
											<td align="right">
												<?php
													if($cekBIPOTID == "YES")
													{
												?>
														<a href="<?php echo site_url("new_students/ptl_bipotmhs_discounts_form/$PMBID/$BIPOTID"); ?>"><h5 class="box-title">Add Discounts</h5></a>
												<?php
													}
												?>
											</td>
										</tr>
										<?php
											if($rowrecord)
											{
												$h = "-7";
												$hm = $h * 60;
												$ms = $hm * 60;
												$tanggal = gmdate("Y-m-d", time()-($ms));
												foreach($rowrecord as $r)
												{
													if($BIPOTID == $r->BIPOTID)
													{
														if($r->jatuh_tempo == "")
														{
															$cekjt = -1;
														}
														else
														{
															$cekjt = hitungHari($r->jatuh_tempo,$tanggal);
														}
														if($cekjt >= 0)
														{
															$jt = "<font color='#F21729;' size='5'>Grace Period: $r->jatuh_tempo</font>";
														}
														else
														{
															$jt = "Grace Period: $r->jatuh_tempo";
														}
														if($r->jatuh_tempo_akhir == "")
														{
															$cekjta = -1;
															$penalty = 0;
														}
														else
														{
															$cekjta = hitungHari($r->jatuh_tempo_akhir,$tanggal);
															$jt_a = str_replace("-","",$r->jatuh_tempo_akhir);
															$nw_t = str_replace("-","",$tanggal);
															$selisih_jta = 0;
															if($jt_a < $nw_t)
															{
																$selisih_jta = $cekjta;
															}
															else
															{
																$cekjta = -$cekjta;
															}
															$penalty = $r->denda * $selisih_jta;
														}
														if($cekjta >= 0)
														{
															$jta = "<font color='#F21729;' size='5'>Due Date: $r->jatuh_tempo_akhir</font>";
															if($cekBIPOTID == "YES")
															{
																$pen = "<a href='".site_url("new_students/ptl_reminder/$AplikanID")."' class='btn btn-success'>REMINDER</a>";
															}
															else
															{
																$pen = "";
															}
															$p = "Penalty: ".formatRupiah($penalty)." ($cekjta days)&nbsp;&nbsp;&nbsp;";
														}
														else
														{
															$jta = "Due Date: $r->jatuh_tempo_akhir";
															$p = "";
														}
														echo "<tr>
																<td>$jt</td>
																<td>$jta</td>
															</tr>
															<tr>
																<td>Minimum Payment: ".formatRupiah($r->minimal_pembayaran)."</td>
																<td>$p</td>
															</tr>
															<tr>
																<td colspan='2'>Notes: $r->Catatan</td>
															</tr>";
													}
												}
											}
										?>
										<?php
											if($postpone == "Y")
											{
										?>
												<tr>
													<td colspan="2"><p align='center'><a href="#" class="btn btn-warning">POSTPONE</a></p></td>
												</tr>
										<?php
											}
										?>
										<tr>
											<td colspan="2"><font color="red"><h3 class="box-title">MARKETING NAME: <?php echo $NamaMarketing; ?></h3></font></td>
										</tr>
										<tr>
											<td colspan="2"><font color="red"><h3 class="box-title">PAYMENT FROM MARKETING: <?php echo formatRupiah($total_bayar); ?></h3></font></td>
										</tr>
										<tr>
											<td colspan="2"><font color="red"><h3 class="box-title">PROOF OF PAYMENT:</h3></font> <a href="<?php echo base_url(); ?>new_students/ptl_semester_download/<?php echo $bukti_setoran; ?>" title="<?php echo $bukti_setoran; ?>" class="btn btn-success btn-sm btn-round btn-line" ><?php $file_order = substr($bukti_setoran,0,35); echo $file_order; ?></a></td>
										</tr>
										<tr>
											<td colspan="2"><font color="red"><h3 class="box-title">NOTES: <?php echo $CatatanPresenter; ?></h3></font></td>
										</tr>
										<?php
											if($cekBIPOTID == "YES")
											{
												if($detail)
												{
													$biaya2 = 0;
													$biaya_byr = 0;
													$potongan2 = 0;
													$potongan_byr = 0;
													foreach($detail as $row)
													{
														if($row->TrxID == "1")
														{
															$biaya2 = $biaya2 + $row->Besar;
															$biaya_byr = $biaya_byr + $row->Dibayar;
														}
														if($row->TrxID == "-1")
														{
															$potongan2 = $potongan2 + $row->Besar;
															$potongan_byr = $potongan_byr + $row->Dibayar;
														}
													}
													$total_bipot = $biaya2 - $potongan2 - $biaya_byr;
													if($total_bipot < 0)
													{
														$m = "-";
													}
													else
													{
														$m = "";
													}
										?>
													<tr>
														<td><p align='right'><h3 class="box-title">COST</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($biaya2 + $penalty); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">DISCOUNTS</h3></td>
														<td><p align='right'><font size="5"><?php echo "-".formatRupiah($potongan2); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">PAYMENTS</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($biaya_byr); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">BALANCE</h3></td>
														<td><p align='right'><font color="red" size="5"><?php echo $m.formatRupiah($total_bipot + $penalty); ?></font></p></td>
													</tr>
										<?php
												}
											}
											else
											{
												if($detail)
												{
													$biaya = 0;
													$potongan = 0;
													foreach($detail as $row)
													{
														if($row->TrxID == "1")
														{
															$biaya = $biaya + $row->Jumlah;
														}
														if($row->TrxID == "-1")
														{
															$potongan = $potongan + $row->Jumlah;
														}
													}
													$total_bipot = $biaya - $potongan;
													if($total_bipot < 0)
													{
														$m = "-";
													}
													else
													{
														$m = "";
													}
										?>
													<tr>
														<td><p align='right'><h3 class="box-title">COST</h3></td>
														<td><p align='right'><font size="5"><?php echo formatRupiah($biaya + $penalty); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">DISCOUNTS</h3></td>
														<td><p align='right'><font size="5"><?php echo "-".formatRupiah($potongan); ?></font></p></td>
													</tr>
													<tr>
														<td><p align='right'><h3 class="box-title">BALANCE</h3></td>
														<td><p align='right'><font color="red" size="5"><?php echo $m.formatRupiah($total_bipot + $penalty); ?></font></p></td>
													</tr>
										<?php
												}
											}
										?>
									</table>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<?php
													if($cekBIPOTID == "YES")
													{
														echo "<th>#</th>
															<th>Name</th>
															<th>Total</th>
															<th>Paid</th>
															<th>Action</th>";
													}
													else
													{
														echo "<th>#</th>
															<th>Name</th>
															<th>Total</th>
															<th>NA</th>";
													}
												?>
											</tr>
										</thead>
										<tbody>
											<?php
												if($cekBIPOTID == "YES")
												{
													if($detail)
													{
														$no = 1;
														$totpot1 = 0;
														$totpot2 = 0;
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOTMhswRef = $row->BIPOTMhswRef;
																if($BIPOTMhswRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td title='BIPOTMhswID : $row->BIPOTMhswID'><b>".$res['Nama']."</b>$TambahanNama</td>
																			<td><p align='right'>".formatRupiah($row->Besar)."</p></td>
																			<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																			<td>";
											?>
																			<a href="<?php echo site_url("new_students/ptl_bipotmhs_delete/$AplikanID/$row->BIPOTMhswID/$BIPOTNamaID"); ?>" title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->BIPOTMhswID." - ".$res["Nama"]; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
											<?php
																	echo "</td>
																		</tr>
																		";
																	$no++;
																	$totpot1 = $totpot1 + $row->Besar;
																	$totpot2 = $totpot2 + $row->Dibayar;
																}
															}
														}
														$totbi1 = 0;
														$totbi2 = 0;
														foreach($detail as $row)
														{
															if($row->TrxID == "1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOTMhswRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_pym_detail_ref_cek_pmb($BIPOTID,$BIPOTMhswRef,$PMBID);
																$ref_detail = "";
																$ref_jumlah2 = 0;
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$nm = $resref_detail2['Nama'];
																		}
																		$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$nm."(".formatRupiah($rd->Besar).") <a href='".site_url("new_students/ptl_bipotmhs_delete/$AplikanID/$rd->BIPOTMhswID/$BIPOTNamaID/$rd->Dibayar")."' title='Delete' class='btn btn-danger'><i class='fa fa-trash'></i></a>";
																		$ref_jumlah2 = $ref_jumlah2 + $rd->Besar;
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td title='BIPOTMhswID : $row->BIPOTMhswID'>
																			<b><a href='".site_url("new_students/ptl_correction_cost/$row->BIPOTMhswID/$AplikanID")."'>$Nama</a></b>
																			$TambahanNama
																			$ref_detail
																		</td>
																		<td title='Bill : ".formatRupiah($row->Besar)." ~ Discount : ".formatRupiah($ref_jumlah2)."'><p align='right'>".formatRupiah($row->Besar - $ref_jumlah2)."</p></td>
																		<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																		<td>";
																		
											?>
																			<a href="<?php echo site_url("new_students/ptl_bipotmhs_delete/$AplikanID/$row->BIPOTMhswID/$BIPOTNamaID"); ?>" title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->BIPOTMhswID." - ".$res["Nama"]; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
											<?php
																echo "</td>
																	</tr>
																	";
																$no++;
																$totbi1 = $totbi1 + ($row->Besar - $ref_jumlah2);
																$totbi2 = $totbi2 + $row->Dibayar;
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi1 - $totpot1)."</p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi2 - $totpot2)."</p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												}
												else
												{
													if($detail)
													{
														$no = 1;
														$jumlah_potongan = "";
														$jumlah_cost = "";
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOT2IDRef = $row->BIPOT2IDRef;
																if($BIPOT2IDRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td title='BIPOT2ID : $row->BIPOT2ID'><b>$Nama</b>$TambahanNama</td>
																			<td><p align='right'>".formatRupiah($row->Jumlah)."</p></td>
																			<td>$row->NA</td>
																		</tr>
																		";
																	$no++;
																	$jumlah_potongan = $jumlah_potongan + $row->Jumlah;
																}
															}
														}
														foreach($detail as $row)
														{
															if($row->TrxID == "1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOT2IDRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOT2IDRef);
																$ref_detail = "";
																$ref_jumlah2 = 0;
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$resref_detail2["Nama"]."(".formatRupiah($rd->Jumlah).")";
																			$ref_jumlah2 = $ref_jumlah2 + $rd->Jumlah;
																		}
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td title='BIPOT2ID : $row->BIPOT2ID'>
																			<b>$Nama</b>
																			$TambahanNama
																			$ref_detail
																		</td>
																		<td title='(".formatRupiah($row->Jumlah).") - (".formatRupiah($ref_jumlah2).")'><p align='right'>".formatRupiah($row->Jumlah - $ref_jumlah2)."</p></td>
																		<td>$row->NA</td>
																	</tr>
																	";
																$no++;
																$jumlah_cost = $jumlah_cost + ($row->Jumlah - $ref_jumlah2);
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($jumlah_cost - $jumlah_potongan)."</p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Trx ID</th>
													<th>Type</th>
													<th>Date</th>
													<th>To Account</th>
													<th>Paid</th>
													<th>Note</th>
													<th>View</th>
													<th>Created By</th>
													<th>Posting Date</th>
													<th>Edited By</th>
													<th>Edited Date</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowbukti)
													{
														$no = 1;
														$total_bayar = 0;
														foreach($rowbukti as $row)
														{
															$trx = "<font color='green'><b>PAID</b></font>";
															if($row->TrxID == "-1")
															{
																$trx = "<font color='blue'><b>WITHDRAWAL</b></font>";
															}
															if($row->TrxID == "-2")
															{
																$trx = "<font color='red'><b>CORRECTION</b></font>";
															}
															$cetak = $row->Cetak;
															$bktstyle = "style='background-color:#ECF0F5;border:5px;'";
															if($row->BayarMhswID == $CekBayarMhswID)
															{
																$bktstyle = "style='background-color:#F49292;border:5px;'";
															}
															echo"
																<tr>
																	<td $bktstyle>$no</td>
																	<td $bktstyle>$row->BayarMhswID</a></td>
																	<td $bktstyle>$trx</a></td>
																	<td $bktstyle>".tgl_singkat_eng($row->Tanggal)."</a></td>
																	<td $bktstyle>$row->RekeningID</a></td>
																	<td $bktstyle align='right'>".formatRupiah($row->Jumlah)."</td>
																	<td $bktstyle>$row->Keterangan</td>
																	<td $bktstyle>$cetak</td>
																	<td $bktstyle>$row->login_buat</td>
																	<td $bktstyle>$row->tanggal_buat</td>
																	<td $bktstyle>$row->login_edit</td>
																	<td $bktstyle>$row->tanggal_edit</td>
																	<td $bktstyle>";
												?>
																		<a href="<?php echo site_url("new_students/ptl_bipotmhs_payment_delete/$AplikanID/$row->BayarMhswID"); ?>" title="Delete" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-trash"></i></a>
												<?php
																	echo "
																		<a href='".site_url("new_students/ptl_payment_edit/$row->BayarMhswID/$AplikanID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>
																		<a href='".site_url("new_students/ptl_print_payment/$row->BayarMhswID/$AplikanID/ORIGINAL/ESMOD%20JAKARTA")."' class='btn btn-success'>Original</a>
																		<a href='".site_url("new_students/ptl_print_payment/$row->BayarMhswID/$AplikanID/COPY/ESMOD%20JAKARTA")."' class='btn btn-warning'>Copy</a>
																	</td>
																</tr>
																";
															$no++;
															$total_bayar = $total_bayar + $row->Jumlah;
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='5'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($total_bayar)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='13'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>