				<style>
					div.paging {
						padding		: 2px;
						margin		: 2px;
						text-align	: center;
						font-family	: Tahoma;
						font-size	: 16px;
						font-weight	: bold;
					}
					div.paging a {
						padding				: 2px 6px;
						margin-right		: 2px;
						border				: 1px solid #DEDFDE;
						text-decoration		: none;
						color				: #dc0203;
						background-position	: bottom;
					}
					div.paging a:hover {
						background-color: #0063dc;
						border : 1px solid #fff;
						color  : #fff;
					}
					div.paging span.current {
						border : 1px solid #DEDFDE;
						padding		 : 2px 6px;
						margin-right : 2px;
						font-weight  : bold;
						color        : #FF0084;
					}
					div.paging span.disabled {
						padding      : 2px 6px;
						margin-right : 2px;
						color        : #ADAAAD;
						font-weight  : bold;
					}
					div.paging span.prevnext {    
					  font-weight : bold;
					}
					div.paging span.prevnext a {
						 border : none;
					}
					div.paging span.prevnext a:hover {
						display: block;
						border : 1px solid #fff;
						color  : #fff;
					}
				</style>
				<section class="content-header">
					<h1>
						Students
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li class="active"><i class="fa fa-user"></i>Students</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Students</h3>
								</div>
								<div class="box-body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students/ptl_filter_tahun_masuk"); ?>" method="POST">
														<select name="cektahunmasuk" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- YEAR --</option>
															<?php
																$cektahunmasuk = $this->session->userdata('students_filter_tahun_masuk');
																if($rowtahun)
																{
																	foreach($rowtahun as $rt)
																	{
																		echo "<option value='$rt->tahun'";
																		if($cektahunmasuk == $rt->tahun)
																		{
																			echo "selected";
																		}
																		echo ">$rt->tahun</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students/ptl_filter_jur"); ?>" method="POST">
														<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PROGRAM --</option>
															<?php
																$cekjurusan = $this->session->userdata('students_filter_jur');
																echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
																echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
																echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students/ptl_filter_prodi"); ?>" method="POST">
														<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PRODI --</option>
															<?php
																$cekprodi = $this->session->userdata('students_filter_prodi');
																if($cekjurusan == "INT")
																{
																	if($programd1)
																	{
																		foreach($programd1 as $d1)
																		{
																			echo "<option value='$d1->ProdiID'";
																			if($cekprodi == $d1->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "REG")
																{
																	if($programd3)
																	{
																		foreach($programd3 as $d3)
																		{
																			echo "<option value='$d3->ProdiID'";
																			if($cekprodi == strtoupper($d3->ProdiID))
																			{
																				echo "selected";
																			}
																			echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																		}
																	}
																}
																if($cekjurusan == "SC")
																{
																	if($shortcourse)
																	{
																		foreach($shortcourse as $sc)
																		{
																			echo "<option value='$sc->KursusSingkatID'";
																			if($cekprodi == $sc->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("students/ptl_filter_no"); ?>" method="POST">
														<select name="cekno" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>10 data</option>
															<?php
																$cekno = $this->session->userdata('students_filter_no');
																echo "<option value='25'"; if($cekno == '25'){ echo "selected"; } echo ">25 data</option>";
																echo "<option value='50'"; if($cekno == '50'){ echo "selected"; } echo ">50 data</option>";
																echo "<option value='100'"; if($cekno == '100'){ echo "selected"; } echo ">100 data</option>";
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
											<tr class="info">
												<td>
													<form action="<?php echo site_url("students/ptl_filter_marketing"); ?>" method="POST">
														<select name="cekmarketing" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>ONLINE REGISTRATION</option>
															<?php
																$cekmarketing = $this->session->userdata('students_filter_marketing');
																if($marketing)
																{
																	foreach($marketing as $mar)
																	{
																		echo "<option value='$mar->id_akun'";
																		if($cekmarketing == $mar->id_akun)
																		{
																			echo "selected";
																		}
																		echo ">$mar->id_akun - $mar->nama</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("students/search"); ?>" method="POST">
														<input type="text" name="cari" value="<?php echo $pencarian; ?>" class="form-control" placeholder="Search.....">
												</td>
												<td>
														<input type="submit" value="Search" id="my_button" class="btn btn-primary">
													</form>
													<?php
														if($pencarian != "")
														{
															echo "<a href='".site_url("students")."' class='btn btn-warning'>Clear</a>";
														}
													?>
												</td>
											</tr>
										</table>
										<br/>
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Photo</th>
													<th>Student ID & Name </th>
													<th>Program</th>
													<th>Father<br/>Mother</th>
													<th>Marketing</th>
													<th>Status</th>
													<th>Phone<br/>Mobile</th>
													<th>Email</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($cek)
													{
														$no = 1;
														foreach($cariproduk->result() as $row)
														{
															$foto = "ptl_storage/foto_umum/user.jpg";
															$foto_preview = "ptl_storage/foto_umum/user.jpg";
															if($row->Foto != "")
															{
																$foto = "../academic/ptl_storage/foto_mahasiswa/".$row->Foto;
																$exist = file_exists_remote(base_url("$foto"));
																if($exist)
																{
																	$foto = $foto;
																	$source_photo = base_url("$foto");
																	$info = pathinfo($source_photo);
																	$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																	$exist1 = file_exists_remote(base_url("$foto_preview"));
																	if($exist1)
																	{
																		$foto_preview = $foto_preview;
																	}
																	else
																	{
																		$foto_preview = $foto;
																	}
																}
																else
																{
																	$foto = "ptl_storage/foto_umum/user.jpg";
																	$foto_preview = "ptl_storage/foto_umum/user.jpg";
																}
															}
															$StatusMhswID = $row->StatusMhswID;
															$st = $this->m_status->PTL_select($StatusMhswID);
															$id_akun = $row->PresenterID;
															$resakses = $this->m_akun->PTL_select($id_akun);
															$NamaMarketing = '';
															if($resakses)
															{
																$word = explode(" ",$resakses['nama']);
																$NamaMarketing = @$word[0];
															}
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr>";
															}
															echo "<td>$no</td>
																	<td>
																		<a class='fancybox' title='$row->MhswID - $row->Nama' href='".base_url("$foto")."' data-fancybox-group='gallery' >
																			<img class='img-polaroid' src='".base_url("$foto_preview")."' width='50px' alt='' />
																		</a>
																	</td>
																	<td>$row->MhswID<br/>$row->Nama</td>
																	<td>$row->ProgramID<br/>$row->ProdiID</td>
																	<td>$row->NamaAyah<br/>$row->NamaIbu</td>
																	<td>$NamaMarketing</td>
																	<td>$st[Nama]</td>
																	<td>$row->Telepon<br/>$row->Handphone</td>
																	<td>$row->Email</td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("students/ptl_edit/$row->MhswID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																	</td>
																</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
										<?php
											echo $pagination;
											echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>