				<section class="content-header">
					<h1>
						Edit Student
						<small>Edit</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("students"); ?>"><i class="fa fa-user"></i>Students</a></li>
						<li class="active">Edit Student</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Edit Student</h3>
								</div>
								<?php echo form_open_multipart('students/ptl_update',array('name' => 'contoh')); ?>
									<div class="box-body">
										<?php
											$foto = "ptl_storage/foto_umum/user.jpg";
											if($Foto != "")
											{
												$foto = "../academic/ptl_storage/foto_mahasiswa/".$Foto;
												$exist = file_exists_remote(base_url("$foto"));
												if($exist)
												{
													$foto = $foto;
												}
												else
												{
													$foto = "ptl_storage/foto_umum/user.jpg";
												}
											}
										?>
										<center>
											<img src="<?php echo base_url("$foto"); ?>" width="150px"/>
										</center>
										<div class="form-group">
											<label>SIN</label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Name</label>
											<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Program</label>
											<input readonly type="text" value="<?php echo $ProgramID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Prodi</label>
											<input readonly type="text" value="<?php echo $ProdiID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Marketing</label>
											<input readonly type="text" value="<?php echo $PresenterID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Place of Birth</label>
											<input readonly type="text" name="TempatLahir" value="<?php echo $TempatLahir; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Birthday</label>
											<input readonly type="text" name="TanggalLahir" value="<?php echo $TanggalLahir; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Gender</label>
											<input readonly type="text" value="<?php echo $Kelamin; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Nationality</label>
											<input readonly type="text" name="WargaNegara" value="<?php echo $WargaNegara; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Religion</label>
											<input readonly type="text" name="Agama" value="<?php echo $Agama; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Civil Status</label>
											<input readonly type="text" value="<?php echo $StatusSipil; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea readonly name="Alamat" class="form-control" ><?php echo $Alamat; ?></textarea>
										</div>
										<div class="form-group">
											<label>RT</label>
											<input readonly type="text" name="RT" value="<?php echo $RT; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>RW</label>
											<input readonly type="text" name="RW" value="<?php echo $RW; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>City</label>
											<input readonly type="text" name="Kota" value="<?php echo $Kota; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Province</label>
											<input readonly type="text" name="Propinsi" value="<?php echo $Propinsi; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Postal Code</label>
											<input readonly type="text" name="KodePos" value="<?php echo $KodePos; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Country</label>
											<input readonly type="text" name="Negara" value="<?php echo $Negara; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input readonly type="text" name="Telepon" value="<?php echo $Telepon; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input readonly type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Email 1</label>
											<input readonly type="text" name="Email" value="<?php echo $Email; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Email 2</label>
											<input readonly type="text" name="Email2" value="<?php echo $Email2; ?>" class="form-control" >
										</div>
										<h4><font color="green"><b>Father's Data</b></font></h4>
										<div class="form-group">
											<label>Father Name</label>
											<input readonly type="text" name="NamaAyah" value="<?php echo $NamaAyah; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea readonly name="AlamatOrtu" class="form-control"><?php echo $AlamatOrtu; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input readonly type="text" name="KotaOrtu" value="<?php echo $KotaOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postal Code</label>
											<input readonly type="number" name="KodePosOrtu" value="<?php echo $KodePosOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input readonly type="number" name="TeleponOrtu" value="<?php echo $TeleponOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input readonly type="number" name="HandphoneOrtu" value="<?php echo $HandphoneOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input readonly type="mail" name="EmailOrtu" value="<?php echo $EmailOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<h4><font color="green"><b>Mother's Data</b></font></h4>
										<div class="form-group">
											<label>Mother Name</label>
											<input readonly type="text" name="NamaIbu" value="<?php echo $NamaIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input readonly type="number" name="TeleponIbu" value="<?php echo $TeleponIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input readonly type="number" name="HandphoneIbu" value="<?php echo $HandphoneIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input readonly type="mail" name="EmailIbu" value="<?php echo $EmailIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<h4><font color="green"><b>Status</b></font></h4>
										<div class="form-group">
											<label>Status</label>
											<select name="StatusMhswID" class="form-control">
												<?php
													if($rowstatus)
													{
														if($StatusMhswID != "A")
														{
															foreach($rowstatus as $rs)
															{
																if($rs->StatusMhswID != "A")
																{
																	echo "<option value='$rs->StatusMhswID'";
																	if($rs->StatusMhswID == $StatusMhswID)
																	{
																		echo "selected";
																	}
																	echo ">$rs->StatusMhswID - ".strtoupper($rs->Nama)."</option>";
																}
															}
														}
														else
														{
															foreach($rowstatus as $rs)
															{
																echo "<option value='$rs->StatusMhswID'";
																if($rs->StatusMhswID == $StatusMhswID)
																{
																	echo "selected";
																}
																echo ">$rs->StatusMhswID - ".strtoupper($rs->Nama)."</option>";
															}
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Graduation Date</label>
											<input readonly type="text" name="tanggal_lulus" value="<?php echo $tanggal_lulus; ?>" id="datepicker2" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("students"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Update" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>