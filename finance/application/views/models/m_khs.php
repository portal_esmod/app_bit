<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_khs extends CI_Model
	{
		function PTL_all_khs($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->order_by('TahunID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_insert($data_khs)
		{
			$this->db->insert('ac_khs',$data_khs);
			return;
		}
		
		function PTL_select_bipot($MhswID,$BIPOTID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('BIPOTID',$BIPOTID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_all_spesifik($cekjurusan,$cektahun,$cekprodi,$cekkelas)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekkelas != "")
			{
				$this->db->where('KelasID',$cekkelas);
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_cari_all($limit,$offset)
		{
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->order_by('MhswID','ASC');
			return $this->db->get('ac_khs',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("ac_khs");
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek_all()
		{
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_cari_total_all()
		{
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_totcostbi()
		{
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->select('sum(Biaya) AS totcost, sum(Potongan) AS totpot, sum(Bayar) AS totbi');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		// SEARCH
		
		function PTL_all_cari($cari,$limit,$offset)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			$this->db->like('a.MhswID',$cari);
			$this->db->or_like('b.Nama',$cari);
			$this->db->or_like('b.NamaAyah',$cari);
			$this->db->or_like('b.NamaIbu',$cari);
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('a.ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('a.TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('a.ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('a.KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('a.Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('a.Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.MhswID','ASC');
			$this->db->limit($limit,$offset);
			return $this->db->get();
		}
		
		function PTL_all_cari_jumlah($cari)
		{
			$data = 0;
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			$this->db->like('a.MhswID',$cari);
			$this->db->or_like('b.Nama',$cari);
			$this->db->or_like('b.NamaAyah',$cari);
			$this->db->or_like('b.NamaIbu',$cari);
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('a.ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('a.TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('a.ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('a.KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('a.Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('a.Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek($cari)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			$this->db->like('a.MhswID',$cari);
			$this->db->or_like('b.Nama',$cari);
			$this->db->or_like('b.NamaAyah',$cari);
			$this->db->or_like('b.NamaIbu',$cari);
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('a.ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('a.TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('a.ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('a.KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('a.Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('a.Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.MhswID','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_cari_total($cari)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			$this->db->like('a.MhswID',$cari);
			$this->db->or_like('b.Nama',$cari);
			$this->db->or_like('b.NamaAyah',$cari);
			$this->db->or_like('b.NamaIbu',$cari);
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('a.ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('a.TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('a.ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('a.KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('a.Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('a.Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.MhswID','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_totcostbi_all($cari)
		{
			$this->db->select('a.*,sum(a.Biaya) AS totcost, sum(a.Potongan) AS totpot,
			sum(a.Bayar) AS totbi, b.Nama');
			$this->db->from('ac_khs as a');
			$this->db->like('a.MhswID',$cari);
			$this->db->or_like('b.Nama',$cari);
			$this->db->or_like('b.NamaAyah',$cari);
			$this->db->or_like('b.NamaIbu',$cari);
			if($this->session->userdata('pym_filter_jur') != "")
			{
				$this->db->where('a.ProgramID',$this->session->userdata('pym_filter_jur'));
			}
			if(($this->session->userdata('pym_filter_jur') != "") AND ($this->session->userdata('pym_filter_tahun') != ""))
			{
				$this->db->where('a.TahunID',$this->session->userdata('pym_filter_tahun'));
			}
			if(($this->session->userdata('pym_filter_tahun') != "") AND ($this->session->userdata('pym_filter_prodi') != ""))
			{
				$this->db->where('a.ProdiID',$this->session->userdata('pym_filter_prodi'));
			}
			if(($this->session->userdata('pym_filter_prodi') != "") AND ($this->session->userdata('pym_filter_kelas') != ""))
			{
				$this->db->where('a.KelasID',$this->session->userdata('pym_filter_kelas'));
			}
			if($this->session->userdata('pym_filter_sesi') != "")
			{
				$this->db->where('a.Sesi',$this->session->userdata('pym_filter_sesi'));
			}
			if($this->session->userdata('pym_filter_payment') != "")
			{
				$this->db->where('a.Tutup',$this->session->userdata('pym_filter_payment'));
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.MhswID','ASC');
			$query = $this->db->get();
			return $query->row_array();
		}
		
		// REPORT
		
		function PTL_all_report_cari_all1()
		{
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur1'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun1'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi1'));
			$cekkelas = $this->session->userdata('pym_report_filter_kelas1');
			if(($cekkelas != "") OR ($cekkelas != "_"))
			{
				$word = explode("_",$cekkelas);
				if(@$word[0] != "")
				{
					$this->db->where('TahunKe',@$word[0]);
				}
				if(@$word[1] != "")
				{
					$this->db->where('KelasID',@$word[1]);
				}
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		function PTL_all_report_cari_all2()
		{
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur2'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun2'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi2'));
			$cekkelas = $this->session->userdata('pym_report_filter_kelas2');
			if(($cekkelas != "") OR ($cekkelas != "_"))
			{
				$word = explode("_",$cekkelas);
				if(@$word[0] != "")
				{
					$this->db->where('TahunKe',@$word[0]);
				}
				if(@$word[1] != "")
				{
					$this->db->where('KelasID',@$word[1]);
				}
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_report_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("ac_khs");
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur1'));
			$this->db->or_where('ProgramID',$this->session->userdata('pym_report_filter_jur2'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun1'));
			$this->db->or_where('TahunID',$this->session->userdata('pym_report_filter_tahun2'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi1'));
			$this->db->or_where('ProdiID',$this->session->userdata('pym_report_filter_prodi2'));
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_report_cari_cek_all()
		{
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur1'));
			$this->db->or_where('ProgramID',$this->session->userdata('pym_report_filter_jur2'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun1'));
			$this->db->or_where('TahunID',$this->session->userdata('pym_report_filter_tahun2'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi1'));
			$this->db->or_where('ProdiID',$this->session->userdata('pym_report_filter_prodi2'));
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_report_cari_total_all()
		{
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur1'));
			$this->db->or_where('ProgramID',$this->session->userdata('pym_report_filter_jur2'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun1'));
			$this->db->or_where('TahunID',$this->session->userdata('pym_report_filter_tahun2'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi1'));
			$this->db->or_where('ProdiID',$this->session->userdata('pym_report_filter_prodi2'));
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_report_totcostbi()
		{
			$this->db->where('ProgramID',$this->session->userdata('pym_report_filter_jur1'));
			$this->db->or_where('ProgramID',$this->session->userdata('pym_report_filter_jur2'));
			$this->db->where('TahunID',$this->session->userdata('pym_report_filter_tahun1'));
			$this->db->or_where('TahunID',$this->session->userdata('pym_report_filter_tahun2'));
			$this->db->where('ProdiID',$this->session->userdata('pym_report_filter_prodi1'));
			$this->db->or_where('ProdiID',$this->session->userdata('pym_report_filter_prodi2'));
			$this->db->select('sum(Biaya) AS totcost, sum(Potongan) AS totpot, sum(Bayar) AS totbi');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select($KHSID)
		{
			$this->db->where('KHSID',$KHSID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_sum_biaya()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tahun = gmdate("Y", time()-($ms));
			$q = $this->db->query("SELECT sum(Biaya) AS biaya FROM ac_khs WHERE MhswID like '$tahun%'");
			return $q->row_array();
		}
		
		function PTL_select_sum_potongan()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tahun = gmdate("Y", time()-($ms));
			$q = $this->db->query("SELECT sum(Potongan) AS potongan FROM ac_khs WHERE MhswID like '$tahun%'");
			return $q->row_array();
		}
		
		function PTL_select_sum_bayar()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tahun = gmdate("Y", time()-($ms));
			$q = $this->db->query("SELECT sum(Bayar) AS bayar FROM ac_khs WHERE MhswID like '$tahun%'");
			return $q->row_array();
		}
		
		function PTL_select_register($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_max($MhswID)
		{
			$this->db->select('max(TahunID) as tahun');
			$this->db->where('MhswID',$MhswID);
			$this->db->where('Biaya > ','0');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_max_max($MhswID)
		{
			$this->db->select('max(TahunID) as tahun');
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_update($KHSID,$data)
		{
			$this->db->where('KHSID',$KHSID);
			$this->db->update('ac_khs',$data);
		}
	}
?>