<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_status extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('StatusMhswID','ASC');
			$query = $this->db->get('ac_status_mhsw');
			return $query->result();
		}
		
		function PTL_select($StatusMhswID)
		{
			$this->db->where('StatusMhswID',$StatusMhswID);
			$query = $this->db->get('ac_status_mhsw');
			return $query->row_array();
		}
	}
?>