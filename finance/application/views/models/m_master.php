<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_master extends CI_Model
	{
		function PTL_master_biaya()
		{
			$this->db->where('TrxID','1');
			$this->db->order_by('TrxID,Urutan,Nama','DESC');
			$query = $this->db->get('fn_master');
			return $query->result();
		}
		
		function PTL_master_potongan()
		{
			$this->db->where('TrxID','-1');
			$this->db->order_by('TrxID,Urutan,Nama','DESC');
			$query = $this->db->get('fn_master');
			return $query->result();
		}
		
		function PTL_referensi()
		{
			$this->db->where('NA','N');
			$this->db->where('TrxID','1');
			// $this->db->where('KenaDenda','Y');
			// $this->db->or_where('DipotongBeasiswa','Y');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('fn_master');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('fn_master',$data);
			return;
		}
		
		function PTL_select($BIPOTNamaID)
		{
			$this->db->where('BIPOTNamaID',$BIPOTNamaID);
			$query = $this->db->get('fn_master');
			return $query->row_array();
		}
		
		function PTL_select_ref($BIPOTNamaIDRef)
		{
			$this->db->where('BIPOTNamaIDRef',$BIPOTNamaIDRef);
			$query = $this->db->get('fn_master');
			return $query->row_array();
		}
		
		function PTL_update($BIPOTNamaID,$data)
		{
			$this->db->where('BIPOTNamaID',$BIPOTNamaID);
			$this->db->update('fn_master',$data);
		}
		
		// Grup Bipot
		
		function PTL_bipot($master_program,$master_prodi)
		{
			$this->db->where('ProgramID',$master_program);
			$this->db->where('ProdiID',$master_prodi);
			$this->db->order_by('Tahun','DESC');
			$query = $this->db->get('fn_bipot');
			return $query->result();
		}
		
		function PTL_bipot_insert($data)
		{
			$this->db->insert('fn_bipot',$data);
			return;
		}
		
		function PTL_bipot_select($BIPOTID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$query = $this->db->get('fn_bipot');
			return $query->row_array();
		}
		
		function PTL_bipot_update($BIPOTID,$data)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->update('fn_bipot',$data);
		}
		
		// Bipot Detail
		function PTL_detail_ref_cek($BIPOTID,$BIPOT2IDRef)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOT2IDRef',$BIPOT2IDRef);
			$this->db->order_by('BIPOT2ID','ASC');
			$query = $this->db->get('fn_bipot_detail');
			return $query->result();
		}
		function PTL_detail_ref_master($BIPOTID,$BIPOTNamaIDRef)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOTNamaID',$BIPOTNamaIDRef);
			$query = $this->db->get('fn_bipot_detail');
			return $query->result();
		}
		
		function PTL_bipot_detail($BIPOTID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$query = $this->db->get('fn_bipot_detail');
			return $query->result();
		}
		
		function PTL_pym_bipot_detail_biaya($BIPOTID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('TrxID','1');
			$query = $this->db->get('fn_bipot_detail');
			return $query->result();
		}
		
		function PTL_pym_bipot_detail_potongan($BIPOTID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('TrxID','-1');
			$query = $this->db->get('fn_bipot_detail');
			return $query->result();
		}
		
		function PTL_bipot_detail_insert($data)
		{
			$this->db->insert('fn_bipot_detail',$data);
			return;
		}
		
		function PTL_bipot_detail_select($BIPOT2ID)
		{
			$this->db->where('BIPOT2ID',$BIPOT2ID);
			$query = $this->db->get('fn_bipot_detail');
			return $query->row_array();
		}
		
		function PTL_bipot_detail_update($BIPOT2ID,$data)
		{
			$this->db->where('BIPOT2ID',$BIPOT2ID);
			$this->db->update('fn_bipot_detail',$data);
		}
		
		function PTL_bipot_detail_delete($BIPOT2ID)
		{
			$this->db->where('BIPOT2ID',$BIPOT2ID);
			$this->db->delete('fn_bipot_detail');
		}
		
		function PTL_bipot_detail_delete_ref($BIPOTID,$BIPOT2IDRef)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOT2IDRef',$BIPOT2IDRef);
			$this->db->delete('fn_bipot_detail');
		}
		
		// Bipot Mahasiswa BIPOTMhswID
		function PTL_bipot_mhsw_insert($data)
		{
			$this->db->insert('fn_bipotmhsw',$data);
			return;
		}
		
		function PTL_pym_detail_ref_cek_aplikan_otw($MhswID,$BIPOTMhswRef,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('BIPOTMhswRef',$BIPOTMhswRef);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_pym_detail_ref_cek_aplikan($BIPOTID,$BIPOTMhswRef,$PMBID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOTMhswRef',$BIPOTMhswRef);
			$this->db->where('PMBID',$PMBID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_pym_detail_ref_cek($BIPOTID,$BIPOTMhswRef,$MhswID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOTMhswRef',$BIPOTMhswRef);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_pym_detail_ref_cek_pmb($BIPOTID,$BIPOTMhswRef,$PMBID)
		{
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('BIPOTMhswRef',$BIPOTMhswRef);
			$this->db->where('PMBID',$PMBID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_bipot_mhsw_select($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_pym_bipot_mhsw_select($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('NA','N');
			$this->db->order_by('Besar','DESC');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_bipotmhsw_select($BIPOTMhswID)
		{
			$this->db->where('BIPOTMhswID',$BIPOTMhswID);
			$query = $this->db->get('fn_bipotmhsw');
			return $query->row_array();
		}
		
		function PTL_bipotmhsw_update($BIPOTMhswID,$data)
		{
			$this->db->where('BIPOTMhswID',$BIPOTMhswID);
			$this->db->update('fn_bipotmhsw',$data);
		}
		
		function PTL_bipotmhsw_update_delete($PMBID,$data)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->update('fn_bipotmhsw',$data);
		}
		
		function PTL_correction_update($MhswID,$OldTahunID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$OldTahunID);
			$this->db->update('fn_bipotmhsw',$data);
		}
		
		function PTL_pym_bipotmhsw_update_delete($MhswID,$TahunID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->update('fn_bipotmhsw',$data);
		}
		
		function PTL_pym_bipotmhsw_delete($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->delete('fn_bipotmhsw');
		}
		
		function PTL_nspym_delete($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->delete('fn_bipotmhsw');
		}
	}
?>