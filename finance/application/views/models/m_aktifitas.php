<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aktifitas extends CI_Model
	{
		function PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman)
		{
			if($pk1 != "")
			{
				$this->db->where('pk1',$pk1);
			}
			if($pk2 != "")
			{
				$this->db->where('pk2',$pk2);
			}
			if($pk3 != "")
			{
				$this->db->where('pk3',$pk3);
			}
			if($menu != "")
			{
				$this->db->where('menu',$menu);
			}
			if($submenu != "")
			{
				$this->db->where('submenu',$submenu);
			}
			if($kode_halaman != "")
			{
				$this->db->where('kode_halaman',$kode_halaman);
			}
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('dv_aktfitas',20);
			return $query->result();
		}
		
		function PTL_insert($datalog)
		{
			$this->db->insert('dv_aktfitas',$datalog);
			return;
		}
	}
?>