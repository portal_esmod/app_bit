<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_program extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_program');
			return $query->result();
		}
		
		function PTL_all_by_urutan()
		{
			$this->db->order_by('Urutan','ASC');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_program');
			return $query->result();
		}
		
		function PTL_select($ProgramID)
		{
			$this->db->where('ProgramID',$ProgramID);
			$query = $this->db->get('ac_program');
			return $query->row_array();
		}
	}
?>