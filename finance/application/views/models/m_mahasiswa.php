<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mahasiswa extends CI_Model
	{
		function PTL_urut($kd)
		{
			$q = $this->db->query("SELECT max(MhswID) AS LAST FROM ac_mahasiswa where MhswID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_all()
		{
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_all($limit,$offset)
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			return $this->db->get('ac_mahasiswa',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("ac_mahasiswa");
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek_all()
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_total_all()
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari($cari,$limit,$offset)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			return $this->db->get('ac_mahasiswa',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah($cari)
		{
			$data = 0;
			$this->db->select('*');
			$this->db->from('ac_mahasiswa');
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek($cari)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_total($cari)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_tahun()
		{
			$q = $this->db->query("SELECT distinct(substring(MhswID,1,4)) AS tahun FROM ac_mahasiswa");
			return $q->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_mahasiswa',$data);
			return;
		}
		
		function PTL_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		function PTL_select_va($virtual_account)
		{
			$this->db->where('virtual_account',$virtual_account);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		function PTL_update($MhswID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->update('ac_mahasiswa',$data);
		}
	}
?>