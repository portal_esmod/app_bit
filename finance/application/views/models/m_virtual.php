<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class m_virtual extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('fn_virtual');
			return $query->result();
		}
		
		function PTL_import_exe($dataarray)
		{
			for($i=0;$i<count($dataarray);$i++)
			{
				if($i != 0)
				{
					$data = array(
								'id_sesi'=>$dataarray[$i]['id_sesi'],
								'tanggal_transfer'=>$dataarray[$i]['tanggal_transfer'],
								'id_virtual'=>$dataarray[$i]['id_virtual'],
								'nama'=>$dataarray[$i]['nama'],
								'jurnal'=>$dataarray[$i]['jurnal'],
								'cabang'=>$dataarray[$i]['cabang'],
								'jumlah_transfer'=>$dataarray[$i]['jumlah_transfer'],
								'dk'=>$dataarray[$i]['dk'],
								'keterangan1'=>$dataarray[$i]['keterangan1'],
								'keterangan2'=>$dataarray[$i]['keterangan2'],
								'login_buat'=>$dataarray[$i]['login_buat'],
								'tanggal_buat'=>$dataarray[$i]['tanggal_buat']
								);
					$this->db->insert('fn_virtual',$data);
				}
			}
		}
		
		function PTL_select($tanggal_transfer,$id_virtual,$nama,$jurnal,$cabang,$jumlah_transfer,$dk,$keterangan1,$keterangan2)
		{
			$this->db->where('tanggal_transfer',$tanggal_transfer);
			$this->db->where('id_virtual',$id_virtual);
			$this->db->where('nama',$nama);
			$this->db->where('jurnal',$jurnal);
			$this->db->where('cabang',$cabang);
			$this->db->where('jumlah_transfer',$jumlah_transfer);
			$this->db->where('dk',$dk);
			$this->db->where('keterangan1',$keterangan1);
			$this->db->where('keterangan2',$keterangan2);
			$query = $this->db->get('fn_virtual');
			return $query->row_array();
		}
		
		// MHSW
		function PTL_import_exe_mhsw($dataarray)
		{
			for($i=0;$i<count($dataarray);$i++)
			{
				if($i != 0)
				{
					$data = array(
								'MhswID'=>$dataarray[$i]['MhswID'],
								'Password'=>$dataarray[$i]['Password'],
								'PMBID'=>$dataarray[$i]['PMBID'],
								'TahunID'=>$dataarray[$i]['TahunID'],
								'BIPOTID'=>$dataarray[$i]['BIPOTID'],
								'Nama'=>$dataarray[$i]['Nama'],
								'StatusAwalID'=>$dataarray[$i]['StatusAwalID'],
								'StatusMhswID'=>$dataarray[$i]['StatusMhswID'],
								'ProgramID'=>$dataarray[$i]['ProgramID'],
								'ProdiID'=>$dataarray[$i]['ProdiID'],
								'KelasID'=>$dataarray[$i]['KelasID'],
								'WargaNegara'=>$dataarray[$i]['WargaNegara'],
								'Kebangsaan'=>$dataarray[$i]['Kebangsaan'],
								'Pilihan1'=>$dataarray[$i]['Pilihan1'],
								'BatasStudi'=>$dataarray[$i]['BatasStudi'],
								'KodeID'=>$dataarray[$i]['KodeID'],
								'login_buat'=>$dataarray[$i]['login_buat'],
								'tanggal_buat'=>$dataarray[$i]['tanggal_buat']
								);
					$this->db->insert('ac_mahasiswa',$data);
				}
			}
		}
		
		// MHSW
		function PTL_select_mhsw($MhswID,$PMBID,$TahunID,$BIPOTID,$Nama,$ProgramID,$ProdiID,$KelasID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('PMBID',$PMBID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->where('Nama',$Nama);
			$this->db->where('ProgramID',$ProgramID);
			$this->db->where('ProdiID',$ProdiID);
			$this->db->where('KelasID',$KelasID);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		// KHS
		function PTL_import_exe_khs($dataarray)
		{
			for($i=0;$i<count($dataarray);$i++)
			{
				if($i != 0)
				{
					$data = array(
								'MhswID'=>$dataarray[$i]['MhswID'],
								'TahunKe'=>$dataarray[$i]['TahunKe'],
								'ProgramID'=>$dataarray[$i]['ProgramID'],
								'KelasID'=>$dataarray[$i]['KelasID'],
								'WaliKelasID'=>$dataarray[$i]['WaliKelasID'],
								'WaliKelasID2'=>$dataarray[$i]['WaliKelasID2'],
								'SpesialisasiID'=>$dataarray[$i]['SpesialisasiID'],
								'TahunID'=>$dataarray[$i]['TahunID'],
								'KodeID'=>$dataarray[$i]['KodeID'],
								'ProdiID'=>$dataarray[$i]['ProdiID'],
								'StatusMhswID'=>$dataarray[$i]['StatusMhswID'],
								'Sesi'=>$dataarray[$i]['Sesi'],
								'BIPOTID'=>$dataarray[$i]['BIPOTID'],
								'MaxSKS'=>$dataarray[$i]['MaxSKS'],
								'login_buat'=>$dataarray[$i]['login_buat'],
								'tanggal_buat'=>$dataarray[$i]['tanggal_buat']
								);
					$this->db->insert('ac_khs',$data);
				}
			}
		}
		
		// KHS
		function PTL_select_khs($MhswID,$TahunKe,$ProgramID,$KelasID,$WaliKelasID,$WaliKelasID2,$SpesialisasiID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunKe',$TahunKe);
			$this->db->where('ProgramID',$ProgramID);
			$this->db->where('KelasID',$KelasID);
			$this->db->where('WaliKelasID',$WaliKelasID);
			$this->db->where('WaliKelasID2',$WaliKelasID2);
			$this->db->where('SpesialisasiID',$SpesialisasiID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_id($id)
		{
			$this->db->where('id',$id);
			$query = $this->db->get('fn_virtual');
			return $query->row_array();
		}
		
		function PTL_update($id,$data_va)
		{
			$this->db->where('id',$id);
			$this->db->update('fn_virtual',$data_va);
		}
	}
?>