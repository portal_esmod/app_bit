<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_spesialisasi extends CI_Model
	{
		function PTL_select($SpesialisasiID)
		{
			$this->db->where('SpesialisasiID',$SpesialisasiID);
			$query = $this->db->get('al_spesialisasi');
			return $query->row_array();
		}
	}
?>