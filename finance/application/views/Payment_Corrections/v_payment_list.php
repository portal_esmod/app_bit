				<section class="content-header">
					<h1>
						Payment Corrections
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("login/ptl_home"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List Payment Corrections</h3>
								</div>
								<div class="box-body">
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th><u>PMB</u><br>NIM</th>
												<th>Name</th>
												<th>Phone</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
											<tr>
												<td><u>15REG014</u><br>20133000008</td>
												<td>My Name is Students</td>
												<td>0812345678</td>
												<td>Active</td>
												<td>Edit | Delete</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<th><u>PMB</u><br>NIM</th>
												<th>Name</th>
												<th>Phone</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>