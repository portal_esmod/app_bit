<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Esmod Jakarta - Finance</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="../../index2.html"><b>Finance</b> Department</a>
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Set Email to recover your password</p>
				<form action="<?php echo site_url("login/ptl_lupa_password"); ?>" method="post">
					<div class="form-group has-feedback">
						<input type="email" name="email" class="form-control" placeholder="Email"/>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<?php
						if($_SERVER["HTTP_HOST"] != "localhost")
						{
					?>
							<div class="g-recaptcha" data-sitekey="6LeXKk4UAAAAABzBI25f2lqIDGLmfqA1u3HJGmKx"></div>
							<br/>
					<?php
						}
					?>
					<div class="row">
						<div class="col-xs-4">
							<button type="submit" class="btn btn-success btn-block btn-flat">Recover</button>
						</div>
					</div>
				</form>
				<br/>
				<a href="<?php echo site_url("login"); ?>">Back to login</a>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	</body>
</html>