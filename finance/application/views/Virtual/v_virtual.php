				<section class="content-header">
					<h1>
						Virtual Account
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("virtual"); ?>"><i class="fa fa-files-o"></i> Virtual Account</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Virtual Account</h3>
								</div>
								<div class="box-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>The Virtual Account only applies to active students.
									</div>
									<?php echo form_open_multipart('virtual/ptl_import_exe'); ?>
										<center>
											<table>
												<tr>
													<td><input name="userfile" type="file" accept=".xls" class="btn btn-warning" required/></td>
													<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
													<td><input type="submit" value="IMPORT EXCEL" id="my_button" class="btn btn-success" /></td>
												</tr>
											</table>
										</center>
									</form>
									<br/>
									<br/>
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th><p align='center'>Virtual Account ID</p></th>
												<th><p align='center'>Name</p></th>
												<th><p align='center'>Transfer Date</p></th>
												<th><p align='center'>Journal</p></th>
												<th><p align='center'>Branch</p></th>
												<th><p align='center'>Total Amount</p></th>
												<th><p align='center'>D/K</p></th>
												<th><p align='center'>Description 1</p></th>
												<th><p align='center'>Description 2</p></th>
												<th><p align='center'>Posting Date</p></th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														echo "
															<tr>
																<td>$no</td>
																<td><p align='center'><a href='".site_url("virtual/ptl_detail/$row->id/$row->id_virtual")."' title='Change Data'>$row->id_virtual</a></p></td>
																<td>$row->nama</td>
																<td><p align='center'>$row->tanggal_transfer</p></td>
																<td><p align='center'>$row->jurnal</p></td>
																<td><p align='center'>$row->cabang</p></td>
																<td><p align='center'>$row->dk</p></td>
																<td><p align='right'>".formatRupiah($row->jumlah_transfer)."</p></td>
																<td>$row->keterangan1</td>
																<td>$row->keterangan2</td>
																<td><p align='center'>$row->tanggal_buat</p></td>
															</tr>
															";
														$no++;
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>