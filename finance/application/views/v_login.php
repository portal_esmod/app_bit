<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Esmod Jakarta - Finance</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="../../index2.html"><b>Finance</b> Department</a>
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>
				<form action="<?php echo site_url("login/ptl_do_login"); ?>" method="post">
					<div class="form-group has-feedback">
						<input type="text" name="username" class="form-control" placeholder="Username"/>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<?php
						if($_SERVER["HTTP_HOST"] != "localhost")
						{
					?>
							<div class="form-group has-feedback">
								<input type="checkbox" name="remember" value="7" id="myCheck" onclick="myFunction()" class="icheckbox_square-blue">
								Remember me for 7 days.
							</div>
							<div id="text" style="display:none" class="g-recaptcha" data-sitekey="6LeXKk4UAAAAABzBI25f2lqIDGLmfqA1u3HJGmKx"></div>
							<br/>
					<?php
						}
					?>
					<div class="row">
						<div class="col-xs-8">    
							<div class="checkbox icheck">
								<label>
								</label>
							</div>
						</div>
						<div class="col-xs-4">
							<button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
						</div>
					</div>
				</form>
				<a href="<?php echo site_url("login/ptl_forgot"); ?>">Forgot Password?</a>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
		<script>
			function myFunction()
			{
				var checkBox = document.getElementById("myCheck");
				var text = document.getElementById("text");
				if (checkBox.checked == true)
				{
					text.style.display = "block";
				}
				else
				{
					text.style.display = "none";
				}
			}
		</script>
	</body>
</html>