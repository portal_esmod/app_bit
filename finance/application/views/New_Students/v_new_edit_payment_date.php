				<?php
					$mystring = $_SERVER['HTTP_USER_AGENT'];
					$findme   = 'Android';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					$ismobile = false;
					if($pos !== false)
					{
						$ismobile = true;
					}
					else
					{
					}
					$findme   = 'iPhone';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Mobile Safari';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'Blackberry';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					$findme   = 'MeeGo';
					$pos = strpos(strtolower($mystring), strtolower($findme));
					if($pos !== false)
					{
						$ismobile = true;
					}
					if($ismobile == true)
					{
						$browser = "MOBILE";
					}
					else
					{
						$browser = "DESKTOP";
					}
				?>
				<section class="content-header">
					<h1>
						Add Payment
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("new_students"); ?>"><i class="fa fa-files-o"></i>New Students</a></li>
						<li><a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>">Master Cost and Discounts</a></li>
						<li class="active">Edit Payment Date</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-5">
							<div class="box">
								<div class="box-body">
									<form action="<?php echo site_url("new_students/ptl_payment_update"); ?>" method="POST">
										<table class="table table-bordered table-striped">
											<tr>
												<td align="right">Paid by</td>
												<td><?php echo $AplikanID; ?></td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>To Account</b></font></td>
												<td><input readonly type="text" value="<?php echo $RekeningID; ?>" class="form-control"></td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Paid From Bank</b></font></td>
												<td><input readonly type="text" value="<?php echo $Bank; ?>" class="form-control"></td>
											</tr>
											<tr>
												<td align="right">Reference ID</td>
												<td>
													<input type="text" name="BuktiSetoran" value="<?php echo $BuktiSetoran; ?>" class="form-control">
												</td>
											</tr>
											<tr>
												<td align="right"><font color="red"><b>Payment Date</b></font></td>
												<td>
													<input type="text" name="Tanggal" value="<?php echo $Tanggal; ?>" id="datepicker1" class="form-control">
													<input type="hidden" name="AplikanID" value="<?php echo $AplikanID; ?>">
													<input type="hidden" name="BayarMhswID" value="<?php echo $BayarMhswID; ?>">
												</td>
											</tr>
											<tr>
												<td align="right">Note</td>
												<td>
													<textarea name="Keterangan" class="form-control"><?php echo $Keterangan; ?></textarea>
												</td>
											</tr>
											<tr>
												<td align="right"></td>
												<td>
													<a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>" class="btn btn-warning">Back</a>
													<input type="submit" value="Reset" class="btn btn-danger">
													<input type="submit" value="Save" class="btn btn-primary">
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>