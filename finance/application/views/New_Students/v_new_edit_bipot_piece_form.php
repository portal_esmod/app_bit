				<section class="content-header">
					<h1>
						Add Piece
						<small>Form</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("new_students/ptl_pending"); ?>"><i class="fa fa-files-o"></i> New Students</a></li>
						<li><a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>"> Master Cost and Pieces</a></li>
						<li class="active"> Add Piece</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Add Piece</h3>
								</div>
									<div class="box-body">
										<div class="form-group">
											<label>BIPOT ID</label>
											<input readonly type="text" name="BIPOTID" value="<?php echo $BIPOTID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Name</label>
											<form action="<?php echo site_url("new_students/ptl_filter_bipotmhs_add_piece_form"); ?>" method="POST">
												<select name="cekBIPOTNamaID" class="form-control" onchange="this.form.submit()" required>
													<?php
														$cekRekeningID = $this->session->userdata('cekRekeningID');
														if(($cekRekeningID == "NONE") OR ($cekRekeningID == ""))
														{
															$cRI = $this->session->userdata('cekDefJumlah');
														}
														else
														{
															$cRI = $RekeningID;
														}
														if(($cekDefJumlah == "NONE") OR ($cekDefJumlah == ""))
														{
															$cDJ = $this->session->userdata('cekDefJumlah');
														}
														else
														{
															$cDJ = $DefJumlah;
														}
														if($potongan)
														{
															$cekBIPOTNamaID = $this->session->userdata('cekBIPOTNamaID');
															echo "<option value=''>-- CHOOSE --</option>";
															foreach($potongan as $r)
															{
																echo "<option value='$r->BIPOTNamaID $PMBID $BIPOTID'";
																if(($cekBIPOTNamaID == "NONE") OR ($cekBIPOTNamaID == ""))
																{
																	if(@$BIPOTNamaID == $r->BIPOTNamaID)
																	{
																		echo "selected";
																	}
																}
																else
																{
																	if($cekBIPOTNamaID == $r->BIPOTNamaID)
																	{
																		echo "selected";
																	}
																}
																echo ">$r->BIPOTNamaID - $r->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</div>
										<div class="form-group">
											<label>Account Id</label>
											<input readonly type="text" name="RekeningID" value="<?php echo $cRI; ?>" class="form-control" >
										</div>
										<div class="form-group">
											<label>Reference Total</label>
											<input readonly type="text" name="DefJumlah" value="<?php echo $cDJ; ?>" class="form-control" >
										</div>
								<form role="form" action="<?php echo site_url("new_students/ptl_bipotmhs_piece_insert"); ?>" method="POST" />
										<div class="form-group">
											<label>Priority</label>
											<input type="text" name="Prioritas" class="form-control" >
											<input type="hidden" name="PMBID" value="<?php echo $PMBID; ?>" class="form-control" placeholder="" >
											<input type="hidden" name="BIPOTID" value="<?php echo $BIPOTID; ?>" class="form-control" placeholder="" >
											<input type="hidden" name="BIPOTNamaID" value="<?php echo $cekBIPOTNamaID; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Additional Name</label>
											<input type="text" name="TambahanNama" class="form-control" >
										</div>
										<div class="form-group">
											<label>Total</label>
											<input type="text" name="Jumlah" class="form-control" >
										</div>
										<div class="form-group">
											<label>How many sessions?</label>
											<input type="text" name="KaliSesi" class="form-control" >
										</div>
										<div class="form-group">
											<label>How to start session?</label>
											<input type="text" name="MulaiSesi" class="form-control" >
										</div>
										<div class="form-group">
											<label>Worn During?</label>
											<select name="SaatID" class="form-control" required>
												<option value=''>-- CHOOSE --</option>
												<option value="0" >0. Anytime</option>
												<option value="1" >1. Earlier in the session</option>
												<option value="2" >2. Middle of the session</option>
												<option value="3" >3. End of session</option>
											</select>
										</div>
										<div class="form-group">
											<label>Due Date</label>
											<input type="text" name="AkhirBayar" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask >
										</div>
										<div class="form-group">
											<label>Automatic?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="Otomatis" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="Otomatis" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per subject?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="PerMataKuliah" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="PerMataKuliah" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per subject?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="PerLab" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="PerLab" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per Remedial?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="Remedial" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="Remedial" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per Internship?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="PraktekKerja" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="PraktekKerja" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per Final Project?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="TugasAkhir" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="TugasAkhir" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Charged per SKS?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="TugasAkhir" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="TugasAkhir" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Deducted from the cost of deposits?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="KenaDeposit" id="optionsRadios1" value="Y" >
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="KenaDeposit" id="optionsRadios2" value="N" >
														No
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Not Active (NA)?</label>
											<div class="form-group">
												<div class="radio">
													<label>
														<input type="radio" name="NA" id="optionsRadios1" value="Y">
														Yes
													</label>
												</div>
												<div class="radio">
													<label>
														<input type="radio" name="NA" id="optionsRadios2" value="N">
														No
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>