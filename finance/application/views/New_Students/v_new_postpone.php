				<section class="content-header">
					<h1>
						New Students
						<small>Finance Report</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("login/ptl_home"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">New Students Finance Report</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<tr class="info">
											<td>
												<form action="<?php echo site_url("new_students/ptl_filter_postpone_pmb_period"); ?>" method="POST">
													<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PERIOD --</option>
														<?php
															if($periode)
															{
																$cekperiode = $this->session->userdata('nws_filter_postpone_period');
																foreach($periode as $per)
																{
																	echo "<option value='".$per->PMBPeriodID."'";
																	if($cekperiode == $per->PMBPeriodID)
																	{
																		echo "selected";
																	}
																	echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("new_students/ptl_filter_postpone_jur"); ?>" method="POST">
													<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PROGRAM --</option>
														<?php
															$cekjurusan = $this->session->userdata('nws_filter_postpone_jur');
															if($rowprogram)
															{
																foreach($rowprogram as $rp)
																{
																	echo "<option value='$rp->ProgramID'";
																	if($rp->ProgramID == $cekjurusan)
																	{
																		echo "selected";
																	}
																	echo ">$rp->ProgramID - $rp->Nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("new_students/ptl_filter_postpone_prodi"); ?>" method="POST">
													<select name="cekprodi" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PRODI --</option>
														<?php
															$cekprodi = $this->session->userdata('nws_filter_postpone_prodi');
															if($cekjurusan == "INT")
															{
																if($programd1)
																{
																	foreach($programd1 as $d1)
																	{
																		echo "<option value='$d1->ProdiID'";
																		if($cekprodi == $d1->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																	}
																}
															}
															if($cekjurusan == "REG")
															{
																if($programd3)
																{
																	foreach($programd3 as $d3)
																	{
																		echo "<option value='$d3->ProdiID'";
																		if($cekprodi == strtoupper($d3->ProdiID))
																		{
																			echo "selected";
																		}
																		echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																	}
																}
															}
															if($cekjurusan == "SC")
															{
																if($shortcourse)
																{
																	foreach($shortcourse as $sc)
																	{
																		echo "<option value='$sc->KursusSingkatID'";
																		if($cekprodi == $sc->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																	}
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
										</tr>
									</table>
									<br/>
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th><u>Applicant ID</u><br>PMB ID</th>
												<th>Name</th>
												<th><u>Status</u><br>Graduation</th>
												<th><u>Payment Form</u><br>Program</th>
												<th>Prodi</th>
												<th><u>Payment</u><br>Bipot</th>
												<th>From Marketing</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$totcost = 0;
												$totbi = 0;
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														$StatusAwalID = $row->status_awal;
														$res = $this->m_status_awal->PTL_select($StatusAwalID);
														$ProdiID = $row->ProdiID;
														$res2 = $this->m_prodi->PTL_select($ProdiID);
														$PMBID = $row->PMBID;
														$detail = $this->m_master->PTL_bipot_mhsw_select($PMBID);
														$biaya2 = 0;
														$biaya_byr = 0;
														$potongan2 = 0;
														$potongan_byr = 0;
														if($detail)
														{
															foreach($detail as $r)
															{
																if($r->TrxID == "1")
																{
																	$biaya2 = $biaya2 + $r->Besar;
																	$biaya_byr = $biaya_byr + $r->Dibayar;
																}
																if($r->TrxID == "-1")
																{
																	$potongan2 = $potongan2 + $r->Besar;
																	$potongan_byr = $potongan_byr + $r->Dibayar;
																}
															}
															$total_bipot = $biaya2 - $potongan2 - $biaya_byr;
															if($total_bipot < 0)
															{
																$m = "-";
															}
															else
															{
																$m = "";
															}
														}
														echo "
															<tr>
																<td>$no</td>
																<td><u>$row->AplikanID</u><br>$row->PMBID</td>
																<td>$row->Nama</td>
																<td><u>".$res['Nama']."</u><br>$row->StatusAplikanID</td>
																<td><p align='right'><u>".formatRupiah($row->Harga)."</u><br>$row->ProgramID</p></td>
																<td><u>$row->ProdiID</u><br>".$res2['Nama']."</td>
																<td><p align='right'><u>".formatRupiah($biaya_byr)."</u><br>".formatRupiah($biaya2)."</p></td>
																<td><p align='right'><u>".formatRupiah($row->total_bayar)."</u></p></td>
																<td>
																	<a href='".site_url("new_students/ptl_edit/$row->AplikanID")."' class='btn btn-info' title='Change Data'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>
															";
														$no++;
														$totcost = $totcost + $biaya2;
														$totbi = $totbi + $biaya_byr;
													}
												}
											?>
										</tbody>
									</table>
									<center>
										<h2>
											TOTAL (COST) : <?php echo formatRupiah($totcost); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											TOTAL (PAYMENT) : <?php echo formatRupiah($totbi); ?><br/><br/>
											TOTAL (REMAINING) : <?php echo formatRupiah($totcost - $totbi); ?>
										</h2>
										<br/>
										<a href="<?php echo site_url("new_students/ptl_pdf_postpone/ESMOD%20JAKARTA"); ?>" id="my_button" class="btn btn-primary">Print Report</a>
									</center>
								</div>
							</div>
						</div>
					</div>
				</section>