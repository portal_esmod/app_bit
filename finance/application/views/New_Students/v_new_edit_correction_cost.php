				<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
				<section class="content-header">
					<h1>
						Correction This Cost
						<small>Detail</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("new_students/ptl_pending"); ?>"><i class="fa fa-files-o"></i> New Students</a></li>
						<li><a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>"> Master Cost and Discounts</a></li>
						<li class="active">Correction This Cost</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-6">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Detail</h3>
								</div>
								<?php echo form_open_multipart('new_students/ptl_correction_cost_update',array('name' => 'contoh')); ?>
									<div class="box-body">
										<div class="form-group">
											<label>APL ID</label>
											<input readonly type="text" name="AplikanID" value="<?php echo $AplikanID; ?>" class="form-control" placeholder="" >
											<input type="hidden" name="BIPOTMhswID" value="<?php echo $BIPOTMhswID; ?>">
										</div>
										<div class="form-group">
											<label>Name</label>
											<input readonly type="text" value="<?php echo $Nama; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" value="<?php echo $login_buat; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" value="<?php echo $tanggal_buat; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" value="<?php echo $login_edit; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" value="<?php echo $tanggal_edit; ?>" class="form-control" placeholder="" >
										</div>
										<div class="form-group">
											<label>Cost</label>
											<input type="text" name="Besar" value="<?php echo $Besar; ?>" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off" required>
										</div>
										<div class="form-group">
											<label>Paid</label>
											<input type="text" name="Dibayar" value="<?php echo $Dibayar; ?>" style="text-align:right;"
												onmouseover="this.focus()" class="form-control"
												id="inputku" onkeydown="return numbersonly(this, event);"
												onkeyup="hitung();javascript:tandaPemisahTitik(this);" autocomplete="off" required>
										</div>
										<div class="form-group">
											<label>Reason</label>
											<textarea name="alasan" class="form-control" placeholder="Your reason" required></textarea>
										</div>
										<h3>Authorization</h3>
										<div class="form-group">
											<label>Username Supervisor</label>
											<input type="text" name="username" class="form-control" placeholder="Username other user" required>
										</div>
										<div class="form-group">
											<label>Password Supervisor</label>
											<input type="password" name="password" class="form-control" placeholder="Password other user" required>
										</div>
									</div>
									<div class="box-footer">
										<a href="<?php echo site_url("new_students/ptl_edit/$AplikanID"); ?>" class="btn btn-warning">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger">&nbsp;&nbsp;Reset&nbsp;&nbsp;</button>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="submit" value="Update" id="my_button" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>