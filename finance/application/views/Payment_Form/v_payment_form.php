				<style>
					div.paging {
						padding		: 2px;
						margin		: 2px;
						text-align	: center;
						font-family	: Tahoma;
						font-size	: 16px;
						font-weight	: bold;
					}
					div.paging a {
						padding				: 2px 6px;
						margin-right		: 2px;
						border				: 1px solid #DEDFDE;
						text-decoration		: none;
						color				: #dc0203;
						background-position	: bottom;
					}
					div.paging a:hover {
						background-color: #0063dc;
						border : 1px solid #fff;
						color  : #fff;
					}
					div.paging span.current {
						border : 1px solid #DEDFDE;
						padding		 : 2px 6px;
						margin-right : 2px;
						font-weight  : bold;
						color        : #FF0084;
					}
					div.paging span.disabled {
						padding      : 2px 6px;
						margin-right : 2px;
						color        : #ADAAAD;
						font-weight  : bold;
					}
					div.paging span.prevnext {    
					  font-weight : bold;
					}
					div.paging span.prevnext a {
						 border : none;
					}
					div.paging span.prevnext a:hover {
						display: block;
						border : 1px solid #fff;
						color  : #fff;
					}
				</style>
				<section class="content-header">
					<h1>
						Payment Form
						<small>List</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo site_url("payment_form"); ?>"><i class="fa fa-money"></i> Payment Form</a></li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">List of Payment Form</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<tr class="info">
											<td>
												<form action="<?php echo site_url("payment_form/ptl_filter_pmb_period"); ?>" method="POST">
													<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PERIOD --</option>
														<?php
															if($periode)
															{
																$cekperiode = $this->session->userdata('new_filter_period');
																foreach($periode as $per)
																{
																	echo "<option value='".$per->PMBPeriodID."'";
																	if($cekperiode == $per->PMBPeriodID)
																	{
																		echo "selected";
																	}
																	echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("payment_form/ptl_filter_jur"); ?>" method="POST">
													<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PROGRAM --</option>
														<?php
															$cekjurusan = $this->session->userdata('new_filter_jur');
															if($rowprogram)
															{
																foreach($rowprogram as $rp)
																{
																	echo "<option value='$rp->ProgramID'";
																	if($rp->ProgramID == $cekjurusan)
																	{
																		echo "selected";
																	}
																	echo ">$rp->ProgramID - $rp->Nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												<form action="<?php echo site_url("payment_form/ptl_filter_prodi"); ?>" method="POST">
													<select name="cekprodi" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
														<option value=''>-- PRODI --</option>
														<?php
															$cekprodi = $this->session->userdata('new_filter_prodi');
															if($cekjurusan == "INT")
															{
																if($programd1)
																{
																	foreach($programd1 as $d1)
																	{
																		echo "<option value='$d1->ProdiID'";
																		if($cekprodi == $d1->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																	}
																}
															}
															if($cekjurusan == "REG")
															{
																if($programd3)
																{
																	foreach($programd3 as $d3)
																	{
																		echo "<option value='$d3->ProdiID'";
																		if($cekprodi == strtoupper($d3->ProdiID))
																		{
																			echo "selected";
																		}
																		echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																	}
																}
															}
															if($cekjurusan == "SC")
															{
																if($shortcourse)
																{
																	foreach($shortcourse as $sc)
																	{
																		echo "<option value='$sc->KursusSingkatID'";
																		if($cekprodi == $sc->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																	}
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</td>
											<td>
												&nbsp;
											</td>
										</tr>
										<tr class="info">
											<td>
												&nbsp;
											</td>
											<td>
												&nbsp;
											</td>
											<td>
												<form action="<?php echo site_url("payment_form/search"); ?>" method="POST">
													<input type="text" name="cari" value="<?php echo $pencarian; ?>" class="form-control" placeholder="Search.....">
											</td>
											<td>
													<input type="submit" value="Search" id="my_button2" class="btn btn-primary">
												</form>
												<?php
													if($pencarian != "")
													{
														echo "<a href='".site_url("payment_form")."' class='btn btn-warning'>Clear</a>";
													}
												?>
											</td>
										</tr>
									</table>
									<br/>
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th><u>Applicant ID</u><br>PMB ID</th>
												<th>Name</th>
												<th><u>Status</u><br>Graduation</th>
												<th>Prodi</th>
												<th><u>Paid</u></th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($cek)
												{
													$no = 1;
													foreach($cariproduk->result() as $row)
													{
														$StatusAwalID = $row->status_awal;
														$res = $this->m_status_awal->PTL_select($StatusAwalID);
														$ProdiID = $row->ProdiID;
														$res2 = $this->m_prodi->PTL_select($ProdiID);
														echo "
															<tr>
																<td>$no</td>
																<td><u>$row->AplikanID</u><br>$row->PMBID</td>
																<td>$row->Nama</td>
																<td><u>".$res['Nama']."</u><br>$row->StatusAplikanID</td>
																<td><u>$row->ProgramID - $row->ProdiID</u><br>".@$res2['Nama']."</td>
																<td><p align='right'><u>".formatRupiah($row->Harga)."</u></p></td>
															</tr>
															";
														$no++;
													}
												}
											?>
										</tbody>
									</table>
									<br/>
									<?php
										echo $pagination;
										echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
									?>
									<center>
										<h2>TOTAL : <?php echo formatRupiah($totcost); ?></h2>
										<br/>
										<a href="<?php echo site_url("payment_form/ptl_pdf_report/ESMOD%20JAKARTA"); ?>" id="my_button" class="btn btn-primary">Print Report</a>
									</center>
								</div>
							</div>
						</div>
					</div>
				</section>