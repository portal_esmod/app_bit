<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rekening extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('fn_rekening');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$this->db->where('NA','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('fn_rekening');
			return $query->result();
		}
		
		// function PTL_urut_pmb2($kd2)
		// {
			// $q = $this->db->query("SELECT max(PMBID) AS LAST FROM al_aplikan WHERE PMBID like '$kd2%'");
			// return $q->row_array();
		// }
		
		function PTL_insert($data)
		{
			$this->db->insert('fn_rekening',$data);
			return;
		}
		
		function PTL_select($RekeningID)
		{
			$this->db->where('RekeningID',$RekeningID);
			$query = $this->db->get('fn_rekening');
			return $query->row_array();
		}
		
		function PTL_update($RekeningID,$data)
		{
			$this->db->where('RekeningID',$RekeningID);
			$this->db->update('fn_rekening',$data);
		}
	}
?>