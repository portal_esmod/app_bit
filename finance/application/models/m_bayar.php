<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_bayar extends CI_Model
	{
		function PTL_urut_bayar($kd)
		{
			$q = $this->db->query("SELECT max(BayarMhswID) AS LAST FROM fn_bayarmhsw WHERE BayarMhswID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_all_search_date($Tanggal)
		{
			$this->db->where('Tanggal',$Tanggal);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_all_select($PMBID,$BIPOTID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_all_nspym_select($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_pym_all_select($MhswID,$BIPOTID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_pym_all_select_tahun($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_all_select2($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->order_by('BayarMhsw2ID','DESC');
			$query = $this->db->get('fn_bayarmhsw2');
			return $query->result();
		}
		
		function PTL_insert($data_bayar)
		{
			$this->db->insert('fn_bayarmhsw',$data_bayar);
			return;
		}
		
		function PTL_insert2($data_bayar2)
		{
			$this->db->insert('fn_bayarmhsw2',$data_bayar2);
			return;
		}
		
		function PTL_select($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$query = $this->db->get('fn_bayarmhsw');
			return $query->row_array();
		}
		
		function PTL_select_bipot($BIPOTMhswID)
		{
			$this->db->where('BIPOTMhswID',$BIPOTMhswID);
			$query = $this->db->get('fn_bayarmhsw2');
			return $query->row_array();
		}
		
		function PTL_correction_update($MhswID,$OldTahunID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$OldTahunID);
			$this->db->update('fn_bayarmhsw',$data);
		}
		
		function PTL_update($BayarMhswID,$data_bayar)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->update('fn_bayarmhsw',$data_bayar);
		}
		
		function PTL_delete($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->delete('fn_bayarmhsw');
		}
		
		function PTL_delete2($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->delete('fn_bayarmhsw2');
		}
		
		function PTL_delete_bipot2($BIPOTMhswID)
		{
			$this->db->where('BIPOTMhswID',$BIPOTMhswID);
			$this->db->delete('fn_bayarmhsw2');
		}
		
		function PTL_nspym_delete($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->delete('fn_bayarmhsw');
		}
	}
?>