<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aplikan extends CI_Model
	{
		function PTL_all_spesifik($cekperiode,$cekjurusan,$cekprodi)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_all($limit,$offset)
		{
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			return $this->db->get('al_aplikan',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("al_aplikan");
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek_all()
		{
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_total_all()
		{
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_totcostbi()
		{
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->select('sum(Harga) AS totcost');
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		// SEARCH
		
		function PTL_all_cari($cari,$limit,$offset)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('Nama',$cari);
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			return $this->db->get('al_aplikan',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah($cari)
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("al_aplikan");
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('Nama',$cari);
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek($cari)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('Nama',$cari);
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_cari_total($cari)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('Nama',$cari);
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_totcostbi_all($cari)
		{
			$this->db->like('AplikanID',$cari);
			$this->db->or_like('Nama',$cari);
			if($this->session->userdata('new_filter_period') != "")
			{
				$this->db->where('PMBPeriodID',$this->session->userdata('new_filter_period'));
			}
			if($this->session->userdata('new_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('new_filter_jur'));
			}
			if($this->session->userdata('new_filter_prodi') != "")
			{
				$this->db->where('ProdiID',$this->session->userdata('new_filter_prodi'));
			}
			$this->db->select('sum(Harga) AS totcost');
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		// function PTL_payment_new()
		// {
			// $q = $this->db->query("SELECT sum(Harga) AS harga FROM al_aplikan WHERE SudahBayar = 'Y'");
			// return $q->row_array();
		// }
		
		// function PTL_payment_semester()
		// {
			// $q = $this->db->query("SELECT sum(total_bayar) AS total_bayar FROM al_aplikan WHERE diterima = 'Y'");
			// return $q->row_array();
		// }
		
		function PTL_all_new_done()
		{
			$this->db->where('NA','N');
			$this->db->where('MhswID !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_new_postpone()
		{
			$this->db->where('NA','N');
			$this->db->where('diterima','Y');
			$this->db->where('postpone','Y');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_semester_pending()
		{
			$this->db->where('NA','N');
			$this->db->where('MhswID','');
			$this->db->where('postpone','N');
			$this->db->where('diterima','Y');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_spesifik_new_done($cekperiode,$cekjurusan,$cekprodi)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			$this->db->where('NA','N');
			$this->db->where('MhswID !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_spesifik_new_postpone($cekperiode,$cekjurusan,$cekprodi)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			$this->db->where('NA','N');
			$this->db->where('diterima','Y');
			$this->db->where('postpone','Y');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_spesifik_semester_pending($cekperiode,$cekjurusan,$cekprodi)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			$this->db->where('NA','N');
			$this->db->where('MhswID','');
			$this->db->where('postpone','N');
			$this->db->where('diterima','Y');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_select($AplikanID)
		{
			$this->db->where('AplikanID',$AplikanID);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select_pmb($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_select_va($virtual_account)
		{
			$this->db->where('virtual_account',$virtual_account);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_update($AplikanID,$data)
		{
			$this->db->where('AplikanID',$AplikanID);
			$this->db->update('al_aplikan',$data);
		}
		
		function PTL_delete($AplikanID)
		{
			$this->db->where('AplikanID',$AplikanID);
			$this->db->delete('al_aplikan');
		}
		
		// function PTL_payment_active()
		// {
			// $q = $this->db->query("SELECT sum(total_bayar) AS total_bayar FROM al_aplikan WHERE AplikanID like '$kd%'");
			// return $q->row_array();
		// }
		
		// function PTL_all()
		// {
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_completeness()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('file_syarat !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_interview()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('ujian !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_payment()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('SudahBayar !=','N');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_postpone()
		// {
			// $this->db->where('postpone','Y');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_reguler()
		// {
			// $this->db->where('ProgramID','REG');
			// $this->db->or_where('ProgramID','INT');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_sc()
		// {
			// $this->db->where('ProgramID','SC');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_test()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('akhir_ujian !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_urut_aplikan($kd)
		// {
			// $q = $this->db->query("SELECT max(AplikanID) AS LAST FROM al_aplikan WHERE AplikanID like '$kd%'");
			// return $q->row_array();
		// }
		
		// function PTL_urut_pmb($kd)
		// {
			// $q = $this->db->query("SELECT max(PMBID) AS LAST FROM al_aplikan WHERE PMBID like '$kd%'");
			// return $q->row_array();
		// }
		
		// function PTL_urut_pmb2($kd2)
		// {
			// $q = $this->db->query("SELECT max(PMBID) AS LAST FROM al_aplikan WHERE PMBID like '$kd2%'");
			// return $q->row_array();
		// }
		
		// function PTL_insert($data)
		// {
			// $this->db->insert('al_aplikan',$data);
			// return;
		// }
		
		// function PTL_select_file($dwn)
		// {
			// $this->db->where('foto',$dwn);
			// $query = $this->db->get('al_aplikan');
			// return $query->row_array();
		// }
		
		// function PTL_select_file_bukti($dwn)
		// {
			// $this->db->where('BuktiSetoran',$dwn);
			// $query = $this->db->get('al_aplikan');
			// return $query->row_array();
		// }
		
		// function PTL_select_file_ujian($dwn)
		// {
			// $this->db->where('file_ujian',$dwn);
			// $query = $this->db->get('al_aplikan');
			// return $query->row_array();
		// }
	}
?>