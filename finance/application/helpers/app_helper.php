<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function from_session($key)
	{
		$CI =& get_instance();
		$ss = $CI->session->userdata($key);
		return $ss;
	}

	function warning($input,$goTo)
	{
		$url = site_url().'/'.$goTo;
		$output="<script> 
			alert(\"$input\");
			location = '$url';
			</script>";
		return $output;
	}
	
	function file_exists_remote($url)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		$result = curl_exec($curl);
		$ret = false;
		if ($result !== false)
		{
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			if($statusCode == 200)
			{
				$ret = true;
			}
		}
		curl_close($curl);
		return $ret;
	}
?>