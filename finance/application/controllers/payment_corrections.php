<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Payment_corrections extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function index()
		{
			$this->session->set_userdata('menu','payment_corrections');
			$this->load->view('Portal/v_header');
			$this->load->view('Payment_Corrections/v_payment_list');
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pending()
		{
			$this->session->set_userdata('menu','payment_corrections');
			$this->load->view('Portal/v_header');
			$this->load->view('Payment_Corrections/v_payment_pending');
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_report()
		{
			$this->session->set_userdata('menu','payment_corrections');
			$this->load->view('Portal/v_header');
			$this->load->view('Payment_Corrections/v_payment_report');
			$this->load->view('Portal/v_footer_table');
		}
	}
?>