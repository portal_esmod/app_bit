<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Master extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_rekening');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_master->PTL_master_biaya();
			$data['rowrecord2'] = $this->m_master->PTL_master_potongan();
			
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_master',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_cost_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			
			$data['rekening'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_master_biaya_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_discount_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			
			$data['rekening'] = $this->m_rekening->PTL_all();
			$data['ref'] = $this->m_master->PTL_referensi();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_master_potongan_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_cost_insert()
		{
			$this->authentification();
			$data = array(
						'KodeID' => 'ES01',
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'TrxID' => '1',
						'RekeningID' => $this->input->post('RekeningID'),
						'DefJumlah' => str_replace(".","",$this->input->post('DefJumlah')),
						'KenaDenda' => $this->input->post('KenaDenda'),
						'DipotongBeasiswa' => $this->input->post('DipotongBeasiswa'),
						'Deposit' => $this->input->post('Deposit'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_master->PTL_insert($data);
			echo warning("Data COST '".$this->input->post('Nama')."' successfully added.","../master");
		}
		
		function ptl_discount_insert()
		{
			$this->authentification();
			$data = array(
						'KodeID' => 'ES01',
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'TrxID' => '-1',
						'RekeningID' => $this->input->post('RekeningID'),
						'DefJumlah' => str_replace(".","",$this->input->post('DefJumlah')),
						'BIPOTNamaIDRef' => $this->input->post('BIPOTNamaIDRef'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_master->PTL_insert($data);
			echo warning("Data DISCOUNT '".$this->input->post('Nama')."' successfully added.","../master");
		}
		
		function ptl_cost_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOTNamaID = $this->uri->segment(3);
			$result = $this->m_master->PTL_select($BIPOTNamaID);
			$data['BIPOTNamaID'] = $result['BIPOTNamaID'];
			$data['Urutan'] = $result['Urutan'];
			$data['Nama'] = $result['Nama'];
			$data['RekeningID'] = $result['RekeningID'];
			$data['DefJumlah'] = formatRupiah2($result['DefJumlah']);
			$data['KenaDenda'] = $result['KenaDenda'];
			$data['DipotongBeasiswa'] = $result['DipotongBeasiswa'];
			$data['Deposit'] = $result['Deposit'];
			$data['Catatan'] = $result['Catatan'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			
			$data['rekening'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_master_biaya_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_discount_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOTNamaID = $this->uri->segment(3);
			$result = $this->m_master->PTL_select($BIPOTNamaID);
			$data['BIPOTNamaID'] = $result['BIPOTNamaID'];
			$data['Urutan'] = $result['Urutan'];
			$data['Nama'] = $result['Nama'];
			$data['RekeningID'] = $result['RekeningID'];
			$data['DefJumlah'] = formatRupiah2($result['DefJumlah']);
			$data['BIPOTNamaIDRef'] = $result['BIPOTNamaIDRef'];
			$data['Catatan'] = $result['Catatan'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			
			$data['rekening'] = $this->m_rekening->PTL_all();
			$data['ref'] = $this->m_master->PTL_referensi();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_master_potongan_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_cost_update()
		{
			$this->authentification();
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'RekeningID' => $this->input->post('RekeningID'),
						'DefJumlah' => str_replace(".","",$this->input->post('DefJumlah')),
						'KenaDenda' => $this->input->post('KenaDenda'),
						'DipotongBeasiswa' => $this->input->post('DipotongBeasiswa'),
						'Deposit' => $this->input->post('Deposit'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_master->PTL_update($BIPOTNamaID,$data);
			echo warning("Data COST with CODE '".$BIPOTNamaID."' successfully changed.","../master");
		}
		
		function ptl_discount_update()
		{
			$this->authentification();
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'RekeningID' => $this->input->post('RekeningID'),
						'DefJumlah' => str_replace(".","",$this->input->post('DefJumlah')),
						'BIPOTNamaIDRef' => $this->input->post('BIPOTNamaIDRef'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_master->PTL_update($BIPOTNamaID,$data);
			echo warning("Data DISCOUNT with CODE '".$BIPOTNamaID."' successfully changed.","../master");
		}
		
		function ptl_filter_program()
		{
			$this->authentification();
			$program = $this->input->post('program');
			if($program != "")
			{
				$this->session->set_userdata('master_program',$program);
			}
			else
			{
				$this->session->set_userdata('master_program','NONE');
				$this->session->set_userdata('master_prodi','NONE');
			}
			redirect('master/ptl_bipot');
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$prodi = $this->input->post('prodi');
			if($prodi != "")
			{
				$this->session->set_userdata('master_prodi',$prodi);
			}
			else
			{
				$this->session->set_userdata('master_prodi','NONE');
			}
			redirect('master/ptl_bipot');
		}
		
		function ptl_filter_bipot()
		{
			$this->authentification();
			$link = $this->input->post('BIPOTID');
			$word = explode("_",$link);
			$cekBIPOTID = $word[0];
			$BIPOTID = $word[1];
			if($cekBIPOTID != "")
			{
				$this->session->set_userdata('master_bipot2',$cekBIPOTID);
			}
			else
			{
				$this->session->set_userdata('master_bipot2','NONE');
			}
			redirect("master/ptl_bipot_copy_form/$BIPOTID");
		}
		
		function ptl_filter_program2()
		{
			$this->authentification();
			$link = $this->input->post('program');
			$word = explode("_",$link);
			$program = $word[0];
			$BIPOTID = $word[1];
			if($program != "")
			{
				$this->session->set_userdata('master_program2',$program);
			}
			else
			{
				$this->session->set_userdata('master_program2','NONE');
				$this->session->set_userdata('master_prodi2','NONE');
			}
			redirect("master/ptl_bipot_copy_form/$BIPOTID");
		}
		
		function ptl_filter_prodi2()
		{
			$this->authentification();
			$link = $this->input->post('prodi');
			$word = explode("_",$link);
			$prodi = $word[0];
			$BIPOTID = $word[1];
			if($prodi != "")
			{
				$this->session->set_userdata('master_prodi2',$prodi);
			}
			else
			{
				$this->session->set_userdata('master_prodi2','NONE');
			}
			redirect("master/ptl_bipot_copy_form/$BIPOTID");
		}
		
		function ptl_bipot()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOTID = $this->uri->segment(3);
			$data['detail'] = $this->m_master->PTL_bipot_detail($BIPOTID);
			$data['BIPOTID'] = $BIPOTID;
			$master_program = $this->session->userdata('master_program');
			$master_prodi = $this->session->userdata('master_prodi');
			$data['rowrecord'] = $this->m_master->PTL_bipot($master_program,$master_prodi);
			if($master_program == "SC")
			{
				$data['filter'] = $this->m_kursussingkat->PTL_all();
			}
			else
			{
				$ProgramID = $master_program;
				$data['filter'] = $this->m_prodi->PTL_all_select($ProgramID);
			}
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_bipot',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipot_copy_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$master_program = $this->session->userdata('master_program2');
			$master_prodi = $this->session->userdata('master_prodi2');
			$BIPOTID = $this->session->userdata('master_bipot2');
			$data['detail'] = $this->m_master->PTL_bipot_detail($BIPOTID);
			$data['rowrecord'] = $this->m_master->PTL_bipot($master_program,$master_prodi);
			if($master_program == "SC")
			{
				$data['filter'] = $this->m_kursussingkat->PTL_all();
			}
			if($master_program == "INT")
			{
				$data['filter'] = $this->m_prodi->PTL_all_d1();
			}
			if($master_program == "REG")
			{
				$data['filter'] = $this->m_prodi->PTL_all();
			}
			$BIPOTID = $this->uri->segment(3);
			$data['BIPOTID'] = $BIPOTID;
			$result = $this->m_master->PTL_bipot_select($BIPOTID);
			$data['Nama'] = $result['Nama'];
			$data['Tahun'] = $result['Tahun'];
			$data['rekening'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Master/v_bipot_copy_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipot_copy_insert()
		{
			$this->authentification();
			$BIPOTID = $this->input->post('new_BIPOTID');
			$link = $this->input->post('link');
			$result = $this->m_master->PTL_bipot_detail($BIPOTID);
			if($result)
			{
				foreach($result as $r)
				{
					if($r->TrxID == '1')
					{
						$data = array(
									'BIPOT2IDRef' => $r->BIPOT2IDRef,
									'BIPOTID' => $link,
									'BIPOTNamaID' => $r->BIPOTNamaID,
									'TambahanNama' => $r->TambahanNama,
									'TrxID' => $r->TrxID,
									'Prioritas' => $r->Prioritas,
									'Jumlah' => $r->Jumlah,
									'KaliSesi' => $r->KaliSesi,
									'MulaiSesi' => $r->MulaiSesi,
									'AkhirBayar' => $r->AkhirBayar,
									'Otomatis' => $r->Otomatis,
									'BesarDenda' => $r->BesarDenda,
									'JenisDenda' => $r->JenisDenda,
									'PersenDenda' => $r->PersenDenda,
									'KenaDeposit' => $r->KenaDeposit,
									'PerMataKuliah' => $r->PerMataKuliah,
									'PerSKS' => $r->PerSKS,
									'PerLab' => $r->PerLab,
									'Remedial' => $r->Remedial,
									'PraktekKerja' => $r->PraktekKerja,
									'TugasAkhir' => $r->TugasAkhir,
									'SaatID' => $r->SaatID,
									'StatusMhswID' => $r->StatusMhswID,
									'StatusPotonganID' => $r->StatusPotonganID,
									'GunakanGradeNilai' => $r->GunakanGradeNilai,
									'GradeNilai' => $r->GradeNilai,
									'GunakanGradeBeasiswa' => $r->GunakanGradeBeasiswa,
									'GradeBeasiswa' => $r->GradeBeasiswa,
									'GunakanKategoriNilai' => $r->GunakanKategoriNilai,
									'KategoriNilai' => $r->KategoriNilai,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_master->PTL_bipot_detail_insert($data);
					}
				}
				foreach($result as $r)
				{
					if($r->TrxID == '-1')
					{
						$BIPOTNamaID = $r->BIPOTNamaID;
						$resmaster = $this->m_master->PTL_select($BIPOTNamaID);
						$BIPOT2IDRef = "";
						if($resmaster)
						{
							$BIPOTNamaIDRef = $resmaster['BIPOTNamaIDRef'];
							$BIPOTID = $link;
							$rowref = $this->m_master->PTL_detail_ref_master($BIPOTID,$BIPOTNamaIDRef);
							if($rowref)
							{
								$no = 1;
								foreach($rowref as $row)
								{
									if($no == 1)
									{
										$BIPOT2IDRef = $row->BIPOT2ID;
									}
									$no++;
								}
							}
						}
						$data = array(
									'BIPOT2IDRef' => $BIPOT2IDRef,
									'BIPOTID' => $link,
									'BIPOTNamaID' => $r->BIPOTNamaID,
									'TambahanNama' => $r->TambahanNama,
									'TrxID' => $r->TrxID,
									'Prioritas' => $r->Prioritas,
									'Jumlah' => $r->Jumlah,
									'KaliSesi' => $r->KaliSesi,
									'MulaiSesi' => $r->MulaiSesi,
									'AkhirBayar' => $r->AkhirBayar,
									'Otomatis' => $r->Otomatis,
									'BesarDenda' => $r->BesarDenda,
									'JenisDenda' => $r->JenisDenda,
									'PersenDenda' => $r->PersenDenda,
									'KenaDeposit' => $r->KenaDeposit,
									'PerMataKuliah' => $r->PerMataKuliah,
									'PerSKS' => $r->PerSKS,
									'PerLab' => $r->PerLab,
									'Remedial' => $r->Remedial,
									'PraktekKerja' => $r->PraktekKerja,
									'TugasAkhir' => $r->TugasAkhir,
									'SaatID' => $r->SaatID,
									'StatusMhswID' => $r->StatusMhswID,
									'StatusPotonganID' => $r->StatusPotonganID,
									'GunakanGradeNilai' => $r->GunakanGradeNilai,
									'GradeNilai' => $r->GradeNilai,
									'GunakanGradeBeasiswa' => $r->GunakanGradeBeasiswa,
									'GradeBeasiswa' => $r->GradeBeasiswa,
									'GunakanKategoriNilai' => $r->GunakanKategoriNilai,
									'KategoriNilai' => $r->KategoriNilai,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_master->PTL_bipot_detail_insert($data);
					}
				}
				echo warning("Data BIPOTID '$BIPOTID' successfully added.","../master/ptl_bipot/$link");
			}
			else
			{
				echo warning("Data BIPOTID '$BIPOTID' not found.","../master/ptl_bipot_copy_form/$link");
			}
		}
		
		function ptl_bipot_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			
			$data['rekening'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_form',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_insert()
		{
			$this->authentification();
			$data = array(
						'KodeID' => 'ES01',
						'Nama' => $this->input->post('Nama'),
						'Tahun' => $this->input->post('Tahun'),
						'Def' => $this->input->post('Def'),
						'SP' => $this->input->post('SP'),
						'ProgramID' => $this->session->userdata('master_program'),
						'ProdiID' => $this->session->userdata('master_prodi'),
						'jatuh_tempo' => $this->input->post('jatuh_tempo'),
						'jatuh_tempo_akhir' => $this->input->post('jatuh_tempo_akhir'),
						'minimal_pembayaran' => str_replace(".","",$this->input->post('minimal_pembayaran')),
						'denda' => str_replace(".","",$this->input->post('denda')),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_buat' => $this->waktu,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
						);
			$this->m_master->PTL_bipot_insert($data);
			echo warning("Data MASTER '".$this->input->post('Nama')."' successfully added.","../master/ptl_bipot");
		}
		
		function ptl_bipot_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOTID = $this->uri->segment(3);
			$result = $this->m_master->PTL_bipot_select($BIPOTID);
			$data['BIPOTID'] = $result['BIPOTID'];
			$data['Nama'] = $result['Nama'];
			$data['Tahun'] = $result['Tahun'];
			$data['Def'] = $result['Def'];
			$data['SP'] = $result['SP'];
			$data['jatuh_tempo'] = $result['jatuh_tempo'];
			$data['jatuh_tempo_akhir'] = $result['jatuh_tempo_akhir'];
			$data['minimal_pembayaran'] = formatRupiah2($result['minimal_pembayaran']);
			$data['denda'] = formatRupiah2($result['denda']);
			$data['Catatan'] = $result['Catatan'];
			$data['NA'] = $result['NA'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_edit',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_update()
		{
			$this->authentification();
			$BIPOTID = $this->input->post('BIPOTID');
			$data = array(
						'KodeID' => 'ES01',
						'Nama' => $this->input->post('Nama'),
						'Tahun' => $this->input->post('Tahun'),
						'Def' => $this->input->post('Def'),
						'SP' => $this->input->post('SP'),
						'jatuh_tempo' => $this->input->post('jatuh_tempo'),
						'jatuh_tempo_akhir' => $this->input->post('jatuh_tempo_akhir'),
						'minimal_pembayaran' => str_replace(".","",$this->input->post('minimal_pembayaran')),
						'denda' => str_replace(".","",$this->input->post('denda')),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => $this->input->post('NA'),
						'login_edit' => $this->waktu,
						'tanggal_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
						);
			$this->m_master->PTL_bipot_update($BIPOTID,$data);
			echo warning("Data MASTER with CODE '".$BIPOTID."' successfully changed.","../master/ptl_bipot");
		}
		
		function ptl_filter_bipot_detail_discount()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID');
			$word = explode(" ",$link);
			$cekBIPOTNamaID = $word[0];
			$BIPOT2ID = $word[1];
			if($cekBIPOTNamaID != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID;
				$result2 = $this->m_master->PTL_select($BIPOTNamaID);
				$this->session->set_userdata('cekRekeningID',$result2['RekeningID']);
				$this->session->set_userdata('cekDefJumlah',$result2['DefJumlah']);
				$this->session->set_userdata('cekBIPOTNamaID',$cekBIPOTNamaID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID','NONE');
				$this->session->set_userdata('cekDefJumlah','NONE');
				$this->session->set_userdata('cekBIPOTNamaID','NONE');
			}
			redirect('master/ptl_bipot_detail_discount_edit/'.$BIPOT2ID);
		}
		
		function ptl_filter_bipot_detail_discount_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID');
			$word = explode(" ",$link);
			$cekBIPOTNamaID = $word[0];
			$BIPOTID = $word[1];
			if($cekBIPOTNamaID != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID;
				$result2 = $this->m_master->PTL_select($BIPOTNamaID);
				$this->session->set_userdata('cekRekeningID',$result2['RekeningID']);
				$this->session->set_userdata('cekDefJumlah',$result2['DefJumlah']);
				$this->session->set_userdata('cekBIPOTNamaID',$cekBIPOTNamaID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID','NONE');
				$this->session->set_userdata('cekDefJumlah','NONE');
				$this->session->set_userdata('cekBIPOTNamaID','NONE');
			}
			redirect('master/ptl_bipot_detail_discount_form/'.$BIPOTID);
		}
		
		function ptl_bipot_detail_discount_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['BIPOTID'] = $this->uri->segment(3);
			
			$data['potongan'] = $this->m_master->PTL_master_potongan();
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_detail_potongan_form',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_detail_discount_insert()
		{
			$this->authentification();
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			if(($BIPOTNamaID == "") OR ($BIPOTNamaID == "NONE"))
			{
				echo warning("Variable Name not selected.","../master/ptl_bipot_detail_discount_form/$BIPOTID");
			}
			else
			{
				$NA = $this->input->post('NA');
				if($NA == "")
				{
					$NA = "N";
				}
				$resmaster = $this->m_master->PTL_select($BIPOTNamaID);
				$BIPOT2IDRef = "";
				if($resmaster)
				{
					$BIPOTNamaIDRef = $resmaster['BIPOTNamaIDRef'];
					$rowref = $this->m_master->PTL_detail_ref_master($BIPOTID,$BIPOTNamaIDRef);
					if($rowref)
					{
						$no = 1;
						foreach($rowref as $row)
						{
							if($no == 1)
							{
								$BIPOT2IDRef = $row->BIPOT2ID;
							}
							$no++;
						}
					}
				}
				$data = array(
							'BIPOT2IDRef' => $BIPOT2IDRef,
							'BIPOTID' => $BIPOTID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TrxID' => '-1',
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Jumlah' => str_replace(".","",$this->input->post('Jumlah')),
							'NA' => $NA,
							'login_buat' => $this->waktu,
							'tanggal_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_detail_insert($data);
				echo warning("Data DISCOUNT with CODE '".$BIPOTID."' successfully added.","../master/ptl_bipot/$BIPOTID");
			}
		}
		
		function ptl_bipot_detail_discount_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOT2ID = $this->uri->segment(3);
			$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
			$data['BIPOT2ID'] = $result['BIPOT2ID'];
			$data['BIPOTID'] = $result['BIPOTID'];
			$data['BIPOTNamaID'] = $result['BIPOTNamaID'];
			$data['TambahanNama'] = $result['TambahanNama'];
			$data['Jumlah'] = formatRupiah2($result['Jumlah']);
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			
			$BIPOTNamaID = $result['BIPOTNamaID'];
			$result2 = $this->m_master->PTL_select($BIPOTNamaID);
			if($result2)
			{
				$data['RekeningID'] = $result2['RekeningID'];
				$data['DefJumlah'] = $result2['DefJumlah'];
			}
			else
			{
				$data['RekeningID'] = "";
				$data['DefJumlah'] = "";
			}
			
			$data['potongan'] = $this->m_master->PTL_master_potongan();
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_detail_potongan_edit',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_detail_discount_update()
		{
			$this->authentification();
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			if(($BIPOTNamaID == "") OR ($BIPOTNamaID == "NONE"))
			{
				echo warning("Variable Name not selected.","../master/ptl_bipot_detail_discount_edit/$BIPOT2ID");
			}
			else
			{
				$NA = $this->input->post('NA');
				if($NA == "")
				{
					$NA = "N";
				}
				$data = array(
							'BIPOTID' => $BIPOTID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Jumlah' => str_replace(".","",$this->input->post('Jumlah')),
							'NA' => $NA,
							'login_edit' => $this->waktu,
							'tanggal_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_detail_update($BIPOT2ID,$data);
				echo warning("Data DISCOUNT with CODE '".$BIPOT2ID."' successfully changed.","../master/ptl_bipot/$BIPOTID");
			}
		}
		
		function ptl_filter_bipot_detail_cost()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID2');
			$word = explode(" ",$link);
			$cekBIPOTNamaID2 = $word[0];
			$BIPOT2ID = $word[1];
			if($cekBIPOTNamaID2 != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result2 = $this->m_master->PTL_select($BIPOTNamaID);
				$this->session->set_userdata('cekRekeningID2',$result2['RekeningID']);
				$this->session->set_userdata('cekDefJumlah2',$result2['DefJumlah']);
				$this->session->set_userdata('cekBIPOTNamaID2',$cekBIPOTNamaID2);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID2','NONE');
				$this->session->set_userdata('cekDefJumlah2','NONE');
				$this->session->set_userdata('cekBIPOTNamaID2','NONE');
			}
			redirect('master/ptl_bipot_detail_cost_edit/'.$BIPOT2ID);
		}
		
		function ptl_filter_bipot_detail_cost_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID2');
			$word = explode(" ",$link);
			$cekBIPOTNamaID2 = $word[0];
			$BIPOTID = $word[1];
			if($cekBIPOTNamaID2 != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result2 = $this->m_master->PTL_select($BIPOTNamaID);
				$this->session->set_userdata('cekRekeningID2',$result2['RekeningID']);
				$this->session->set_userdata('cekDefJumlah2',$result2['DefJumlah']);
				$this->session->set_userdata('cekBIPOTNamaID2',$cekBIPOTNamaID2);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID2','NONE');
				$this->session->set_userdata('cekDefJumlah2','NONE');
				$this->session->set_userdata('cekBIPOTNamaID2','NONE');
			}
			redirect('master/ptl_bipot_detail_cost_form/'.$BIPOTID);
		}
		
		function ptl_bipot_detail_cost_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['BIPOTID'] = $this->uri->segment(3);
			
			$data['biaya'] = $this->m_master->PTL_master_biaya();
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_detail_biaya_form',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_detail_cost_insert()
		{
			$this->authentification();
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			if(($BIPOTNamaID == "") OR ($BIPOTNamaID == "NONE"))
			{
				echo warning("Variable Name not selected.","../master/ptl_bipot_detail_cost_form/$BIPOTID");
			}
			else
			{
				$NA = $this->input->post('NA');
				if($NA == "")
				{
					$NA = "N";
				}
				$data = array(
							'BIPOTID' => $BIPOTID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TrxID' => '1',
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Jumlah' => str_replace(".","",$this->input->post('Jumlah')),
							'NA' => $NA,
							'login_buat' => $this->waktu,
							'tanggal_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_detail_insert($data);
				echo warning("Data COST with CODE '".$BIPOTID."' successfully added.","../master/ptl_bipot/$BIPOTID");
			}
		}
		
		function ptl_bipot_detail_cost_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$BIPOT2ID = $this->uri->segment(3);
			$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
			$data['BIPOT2ID'] = $result['BIPOT2ID'];
			$data['BIPOTID'] = $result['BIPOTID'];
			$data['BIPOTNamaID'] = $result['BIPOTNamaID'];
			$data['TambahanNama'] = $result['TambahanNama'];
			$data['Jumlah'] = formatRupiah2($result['Jumlah']);
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			
			$BIPOTNamaID = $result['BIPOTNamaID'];
			$result2 = $this->m_master->PTL_select($BIPOTNamaID);
			$data['RekeningID'] = $result2['RekeningID'];
			$data['DefJumlah'] = $result2['DefJumlah'];
			
			$data['biaya'] = $this->m_master->PTL_master_biaya();
			$this->load->view('Portal/v_header_master');
			$this->load->view('Master/v_bipot_detail_biaya_edit',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_bipot_detail_cost_update()
		{
			$this->authentification();
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOTNamaID = $this->input->post('BIPOTNamaID');
			if(($BIPOTNamaID == "") OR ($BIPOTNamaID == "NONE"))
			{
				echo warning("Variable Name not selected.","../master/ptl_bipot_detail_cost_edit/$BIPOT2ID");
			}
			else
			{
				$NA = $this->input->post('NA');
				if($NA == "")
				{
					$NA = "N";
				}
				$data = array(
							'BIPOTID' => $BIPOTID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Jumlah' => str_replace(".","",$this->input->post('Jumlah')),
							'NA' => $this->input->post('NA'),
							'login_edit' => $this->waktu,
							'tanggal_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_detail_update($BIPOT2ID,$data);
				echo warning("Data DISCOUNT with CODE '".$BIPOT2ID."' successfully changed.","../master/ptl_bipot/$BIPOTID");
			}
		}
		
		function ptl_bipot_detail_delete()
		{
			$this->authentification();
			$BIPOTID = $this->uri->segment(3);
			$BIPOT2ID = $this->uri->segment(4);
			$BIPOT2IDRef = $BIPOT2ID;
			$this->m_master->PTL_bipot_detail_delete($BIPOT2ID);
			$this->m_master->PTL_bipot_detail_delete_ref($BIPOTID,$BIPOT2IDRef);
			echo warning("Data DISCOUNT with CODE '".$BIPOT2ID."' successfully deleted.","../master/ptl_bipot/$BIPOTID");
		}
	}
?>