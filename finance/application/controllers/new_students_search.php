<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class New_students_search extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_waktu = gmdate("YmdHis", time()-($ms));
			$this->tanggal_kirim = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_aplikan');
			$this->load->model('m_bayar');
			$this->load->model('m_khs');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_rekening');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/PTL_maintenance');
			}
		}
		
		function ptl_filter_tanggal()
		{
			$this->authentification();
			$cektanggal = $this->input->post('cektanggal');
			if($cektanggal != "")
			{
				$this->session->set_userdata('today_filter_tanggal',$cektanggal);
			}
			else
			{
				$this->session->unset_userdata('today_filter_tanggal');
			}
			redirect("new_students_search");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-d", time()-($ms));
			$cektanggal = $this->session->userdata('today_filter_tanggal');
			if($cektanggal == "")
			{
				$data['today'] = $tanggal;
				$Tanggal = $tanggal;
			}
			else
			{
				$data['today'] = $cektanggal;
				$Tanggal = $cektanggal;
			}
			$data['rowrecord'] = $this->m_bayar->PTL_all_search_date($Tanggal);
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students_Search/v_new_students_search',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>