<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class General extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('encryption');
			$this->load->library('fpdf');
			$this->load->model('m_aplikan');
			$this->load->model('m_bayar');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_master');
			$this->load->model('m_prodi');
		}
		
		function pesan()
		{
			echo "<center><h2>LINK EXPIRED</h2></center>";
		}
		
		function key_payment()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$BayarMhswID = $this->encryption->decode($this->uri->segment(3));
			$AplikanID = $this->encryption->decode($this->uri->segment(4));
			$status = "COPY";
			$res = $this->m_aplikan->PTL_select($AplikanID);
			$PMBID = $res['PMBID'];
			$resmain = $this->m_bayar->PTL_select($BayarMhswID);
			if(!$resmain)
			{
				redirect("general/pesan");
			}
			$result = $this->m_bayar->PTL_all_select2($BayarMhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$judul = "OFFICIAL RECEIPT";
			$to1 = "Sudah Terima Dari";
			$to2 = "(Received From)";
			$pay1 = "Untuk Pembayaran";
			$pay2 = "(Being Payment For)";
			$pesan = "NB : Pembayaran yang sudah ditransfer tidak dapat ditarik kembali";
			$ttd = "Yang Menerima (Received By)";
			$file = "PAYMENT_";
			if($resmain['TrxID'] == "-1")
			{
				$judul = "WITHDRAWAL";
				$to1 = "Penarikan Oleh";
				$to2 = "(Withdrawal by)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : Penarikan dengan alasan khusus";
				$ttd = "Yang Menerima (Received By)";
				$file = "WITHDRAWAL_";
			}
			if($resmain['TrxID'] == "-2")
			{
				$judul = "CORRECTIONS";
				$to1 = "Dikoreksi Untuk";
				$to2 = "(Corrected for)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Dikoreksi Oleh (Corrected By)";
				$file = "CORRECTIONS_";
			}
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(28,1,"$judul",0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,1,"NO : $BayarMhswID",0,0,"R");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY()-0.6,12.5,1.6);
			
			$this->fpdf->Ln(1.2);
			$id_akun = $AplikanID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->SetFont("helvetica","BI",20);
			$this->fpdf->Cell(26,0.7,$status,0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->Line(1,4.7,28.9,4.7);
			$this->fpdf->Line(1,4.75,28.9,4.75);
			
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $to1, "", 0, "L");
			$this->fpdf->Cell(3, 0.7, "APL ID :", "", 0, "L");
			$this->fpdf->Cell(5.5, 0.7, $AplikanID, "", 0, "C");
			$this->fpdf->Cell(2.2, 0.7, "Nama :", "", 0, "L");
			$this->fpdf->Cell(5.8, 0.7, $nama, "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $to2, "", 0, "L");
			$this->fpdf->Cell(3.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(2.2, 0.5, "(Name)", "", 0, "L");
			$this->fpdf->Cell(10.6, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$terbilang = ucwords(toTerbilang($resmain['Jumlah'])) . ' Rupiah';  
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Banyaknya", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, $terbilang, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "(The amount of)", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Check/BG/Trans./Cash", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.7, $resmain['Bank'], "", 0, "L");
			$this->fpdf->Cell(7.2, 0.7, "Tanggal Pembayaran :", "", 0, "L");
			$this->fpdf->Cell(2.8, 0.7, tgl_singkat_eng($resmain["Tanggal"]), "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "(Payment Date)", "", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $pay1, "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $pay2, "", 0, "L");
			$no = 1;
			$tot = count($result);
			$Jumlah = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$BIPOTMhswID = $r->BIPOTMhswID;
					$hasil2 = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($no == 1)
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->SetFont("Times","B",18);
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					else
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(6.5 , 0.7, "", "", 0, "L");
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					$Jumlah = $Jumlah + $r->Jumlah;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln(1.2);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5 , 1, "Jumlah / Amount", "", 0, "L");
			$this->fpdf->Cell(7 , 1, formatRupiah3($Jumlah), "LBTR", 0, "C");
			$this->fpdf->Cell(6 , 1, "", "", 0, "C");
			$this->fpdf->Cell(8 , 1, $ttd, "", 0, "C");
			$this->fpdf->Ln();
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(21.3 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(4.6 , 0.5, $pesan, "", 0, "L");
			$this->fpdf->Ln(3.6);
			$this->fpdf->Cell(20 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(7 , 0.5, "", "B", 0, "C");
			$this->fpdf->Output($file.$AplikanID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function student_key_payment()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$BayarMhswID = $this->encryption->decode($this->uri->segment(3));
			$MhswID = $this->encryption->decode($this->uri->segment(4));
			$status = "COPY";
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$PMBID = $res['PMBID'];
			$resmain = $this->m_bayar->PTL_select($BayarMhswID);
			if(!$resmain)
			{
				redirect("general/pesan");
			}
			$result = $this->m_bayar->PTL_all_select2($BayarMhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$judul = "OFFICIAL RECEIPT";
			$to1 = "Sudah Terima Dari";
			$to2 = "(Received From)";
			$pay1 = "Untuk Pembayaran";
			$pay2 = "(Being Payment For)";
			$pesan = "NB : Pembayaran yang sudah ditransfer tidak dapat ditarik kembali";
			$ttd = "Yang Menerima (Received By)";
			$file = "STUDENT_PAYMENT_";
			if($resmain['TrxID'] == "-1")
			{
				$judul = "WITHDRAWAL";
				$to1 = "Penarikan Oleh";
				$to2 = "(Withdrawal by)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : Penarikan dengan alasan khusus";
				$ttd = "Yang Menerima (Received By)";
				$file = "STUDENT_WITHDRAWAL_";
			}
			if($resmain['TrxID'] == "-2")
			{
				$judul = "CORRECTIONS";
				$to1 = "Dikoreksi Untuk";
				$to2 = "(Corrected for)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Dikoreksi Oleh (Corrected By)";
				$file = "CORRECTIONS_";
			}
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(28,1,"$judul",0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,1,"NO : $BayarMhswID",0,0,"R");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY()-0.6,12.5,1.6);
			
			$this->fpdf->Ln(1.2);
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->SetFont("helvetica","BI",20);
			$this->fpdf->Cell(26,0.7,$status,0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->Line(1,4.7,28.9,4.7);
			$this->fpdf->Line(1,4.75,28.9,4.75);
			
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $to1, "", 0, "L");
			$this->fpdf->Cell(3, 0.7, "Student ID :", "", 0, "L");
			$this->fpdf->Cell(5.5, 0.7, $MhswID, "", 0, "C");
			$this->fpdf->Cell(2.2, 0.7, "Nama :", "", 0, "L");
			$this->fpdf->Cell(5.8, 0.7, $nama, "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $to2, "", 0, "L");
			$this->fpdf->Cell(3.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(2.2, 0.5, "(Name)", "", 0, "L");
			$this->fpdf->Cell(10.6, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$terbilang = ucwords(toTerbilang($resmain['Jumlah'])) . ' Rupiah';  
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Banyaknya", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, $terbilang, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "(The amount of)", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Check/BG/Trans./Cash", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.7, $resmain['Bank'], "", 0, "L");
			$this->fpdf->Cell(7.2, 0.7, "Tanggal Pembayaran :", "", 0, "L");
			$this->fpdf->Cell(2.8, 0.7, tgl_singkat_eng($resmain["Tanggal"]), "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "(Payment Date)", "", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $pay1, "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $pay2, "", 0, "L");
			$no = 1;
			$tot = count($result);
			$Jumlah = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$BIPOTMhswID = $r->BIPOTMhswID;
					$hasil2 = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($no == 1)
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->SetFont("Times","B",18);
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					else
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(6.5 , 0.7, "", "", 0, "L");
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					$Jumlah = $Jumlah + $r->Jumlah;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln(1.2);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5 , 1, "Jumlah / Amount", "", 0, "L");
			$this->fpdf->Cell(7 , 1, formatRupiah3($Jumlah), "LBTR", 0, "C");
			$this->fpdf->Cell(6 , 1, "", "", 0, "C");
			$this->fpdf->Cell(8 , 1, $ttd, "", 0, "C");
			$this->fpdf->Ln();
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(21.3 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(4.6 , 0.5, $pesan, "", 0, "L");
			$this->fpdf->Ln(3.6);
			$this->fpdf->Cell(20 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(7 , 0.5, "", "B", 0, "C");
			$this->fpdf->Output($file.$MhswID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function student_pdf_invoice()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$BIPOTID = $this->encryption->decode($this->uri->segment(3));
			$MhswID = $this->encryption->decode($this->uri->segment(4));
			$TahunID = $this->encryption->decode($this->uri->segment(5));
			$result = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			if(!$result)
			{
				redirect("general/pesan");
			}
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"INVOICE ID $MhswID$BIPOTID",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "APL ID / PMB ID" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$id_akun." / ".$res['PMBID'] , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -1, "NAME", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -2, "PROGRAM", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1 , -2, ": ".$res['ProgramID']." - ".strtoupper($prod['Nama'])." ($res[ProdiID])", 0, "", "L");
			$this->fpdf->Ln(-0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.5 , 0.7, "#" , "T", 0, "C");
			$this->fpdf->Cell(4.5 , 0.7, "DESCRIPTION" , "T", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "AMOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "TOTAL" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "DISCOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "REMAINING" , "T", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$pot = 0;
			$bia = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "-1")
					{
						$BIPOTID = $r->BIPOTID;
						$BIPOTNamaID_detail = $hasil['BIPOTNamaIDRef'];
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						if(!$resref_detail)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							if($TambahanNama != "")
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
								$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							}
							$no++;
							$pot = $pot + $r->Besar;
						}
					}
				}
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "1")
					{
						$BIPOTNamaIDRef = $BIPOTNamaID;
						$resref = $this->m_master->PTL_select_ref($BIPOTNamaIDRef);
						$ref = "";
						if($resref)
						{
							$ref = $resref['BIPOTNamaID'];
						}
						$BIPOTID = $r->BIPOTID;
						$BIPOTNamaID_detail = $ref;
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$ref_detail = "";
						$ref_jumlah = "";
						$ref_jumlah2 = "";
						if($resref_detail)
						{
							$BIPOTNamaID = $BIPOTNamaID_detail;
							$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
							if($resref_detail2)
							{
								$ref_detail = $resref_detail2["Nama"];
								$ref_jumlah = $resref_detail2["DefJumlah"];
								$ref_jumlah2 = $resref_detail2["DefJumlah"];
							}
						}
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
						$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Dibayar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($ref_jumlah).")", "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar - $r->Dibayar - $ref_jumlah), "", 0, "R");
						if($TambahanNama != "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
						}
						$no++;
						$bia = $bia + ($r->Besar - $r->Dibayar - $ref_jumlah);
					}
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "THE TOTAL TO BE PAID", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($bia - $pot), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "NB", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Pembayaran dapat di transfer ke rekening", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "An: Yayasan PDKI-BDN-ESMOD", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "No. Rekening: 218-300-3496", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Bank: Bank BCA", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Cabang: Cabang Blok A Cipete, Jakarta", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Swift Code: CENAIDA", "", 0, "L");
			$this->fpdf->Output("STUDENT_INVOICE_".$MhswID.$BIPOTID." - ".$inv." - ".@$nama.".pdf","I");
		}
	}
?>