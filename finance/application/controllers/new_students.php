<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class New_students extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->kd_bayar = gmdate("Y", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('encryption');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_bayar');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_rekening');
			$this->load->model('m_sms');
			$this->load->model('m_status_awal');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_pmb_period()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('nws_filter_period',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('nws_filter_period','');
			}
			redirect("new_students");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('nws_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('nws_filter_jur','');
			}
			redirect("new_students");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('nws_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('nws_filter_prodi','');
			}
			redirect("new_students");
		}
		
		function ptl_filter_pending_pmb_period()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('nws_filter_pending_period',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('nws_filter_pending_period','');
			}
			redirect("new_students/ptl_pending");
		}
		
		function ptl_filter_pending_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('nws_filter_pending_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('nws_filter_pending_jur','');
			}
			redirect("new_students/ptl_pending");
		}
		
		function ptl_filter_pending_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('nws_filter_pending_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('nws_filter_pending_prodi','');
			}
			redirect("new_students/ptl_pending");
		}
		
		function ptl_filter_postpone_pmb_period()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('nws_filter_postpone_period',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('nws_filter_postpone_period','');
			}
			redirect("new_students/ptl_postpone");
		}
		
		function ptl_filter_postpone_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('nws_filter_postpone_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('nws_filter_postpone_jur','');
			}
			redirect("new_students/ptl_postpone");
		}
		
		function ptl_filter_postpone_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('nws_filter_postpone_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('nws_filter_postpone_prodi','');
			}
			redirect("new_students/ptl_postpone");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('cektahun'));
			$cektahun = $word[0];
			$AplikanID = $word[1];
			if($cektahun != "")
			{
				$this->session->set_userdata('new_student_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('new_student_filter_tahun','');
			}
			redirect("new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_filter_sesi()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('ceksesi'));
			$ceksesi = $word[0];
			$AplikanID = $word[1];
			if($ceksesi != "")
			{
				$this->session->set_userdata('new_student_filter_sesi',$ceksesi);
			}
			else
			{
				$this->session->set_userdata('new_student_filter_sesi','');
			}
			redirect("new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_filter_bipot()
		{
			$this->authentification();
			$link = $this->input->post('BIPOTID');
			$word = explode(" ",$link);
			$cekBIPOTID = $word[0];
			$AplikanID = $word[1];
			if($cekBIPOTID != "")
			{
				$this->session->set_userdata('master_bipot',$cekBIPOTID);
			}
			else
			{
				$this->session->set_userdata('master_bipot','NONE');
			}
			redirect("new_students/ptl_edit/$AplikanID");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$cekperiode = $this->session->userdata('nws_filter_period');
			$cekjurusan = $this->session->userdata('nws_filter_jur');
			$cekprodi = $this->session->userdata('nws_filter_prodi');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_spesifik_new_done($cekperiode,$cekjurusan,$cekprodi);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_list',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$AplikanID = $this->uri->segment(3);
			$data['CekBayarMhswID'] = $this->uri->segment(4);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['virtual_account'] = $result['virtual_account'];
			$data['PMBID'] = $result['PMBID'];
			$data['MhswID'] = $result['MhswID'];
			$data['PMBPeriodID'] = $result['PMBPeriodID'];
			$data['CatatanPresenter'] = $result['CatatanPresenter'];
			$data['Nama'] = $result['Nama'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['GradeNilai'] = $result['GradeNilai'];
			$data['status_awal'] = $result['status_awal'];
			$data['AsalSekolah'] = $result['AsalSekolah'];
			$data['postpone'] = $result['postpone'];
			$data['total_bayar'] = $result['total_bayar'];
			$data['total_biaya'] = $result['total_biaya'];
			$data['bukti_setoran'] = $result['bukti_setoran'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$id_akun = $result['PresenterID'];
			$resakses = $this->m_akun->PTL_select($id_akun);
			$data['NamaMarketing'] = '';
			if($resakses)
			{
				$data['NamaMarketing'] = $resakses['nama'];
			}
			$master_program = $result['ProgramID'];
			$master_prodi = $result['ProdiID'];
			$cekjurusan = $result['ProgramID'];
			if($result['ProgramID'] == "SPC")
			{
				$cekjurusan = "REG";
			}
			else
			{
				$cekjurusan = $result['ProgramID'];
			}
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord'] = $this->m_master->PTL_bipot($master_program,$master_prodi);
			
			$PMBID = $result['PMBID'];
			$mhsw = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			}
			else
			{
				$BIPOTID = $this->session->userdata('master_bipot');
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = $BIPOTID;
				$data['detail'] = $this->m_master->PTL_bipot_detail($BIPOTID);
			}
			$data['rowbukti'] = $this->m_bayar->PTL_all_select($PMBID,$BIPOTID);
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_semester_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/pembayaran/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant/ptl_semester');
			}
		}
		
		function ptl_virtual_account_insert()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$data = array(
						'virtual_account' => $this->input->post('virtual_account'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			echo warning("Virtual account number successfully saved.","../new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_payment_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$PMBID = $result['PMBID'];
			$data['PMBID'] = $result['PMBID'];
			$mhsw = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			}
			$data['rowrekening'] = $this->m_rekening->PTL_all_active();
			$data['deposit'] = "";
			$data['status_akhir'] = "";
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('New_Students/v_new_edit_payment_form',$data);
			$this->load->view('Portal/v_footer_table',$fdata);
		}
		
		function ptl_payment_insert()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$total = $this->input->post('total');
			$pesan = "";
			for($i=1;$i<=$total;$i++)
			{
				$nama = $this->input->post("nama$i");
				$besar = $this->input->post("besar$i");
				$dibayar = $this->input->post("dibayar$i");
				$payment = str_replace(".","",$this->input->post("payment$i"));
				$sisa = $besar - $dibayar;
				if($payment > $sisa)
				{
					$pesan .= "Payment for '$nama' beyond the limit.".'\n'."Balance ".formatRupiah2($sisa)." - Payment ".formatRupiah2($payment).'\n\n';
				}
				if($payment < 0)
				{
					$pesan .= "Payment for '$nama' not allowed minus value.".'\n'."Payment -".formatRupiah2($payment).'\n\n';
				}
			}
			if($pesan != "")
			{
				echo warning($pesan,"../new_students/ptl_payment_form/$AplikanID");
			}
			else
			{
				$kd = $this->kd_bayar."-";
				$result = $this->m_bayar->PTL_urut_bayar($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,5,5);
				$nextnourut = $lastnourut + 1;
				$nextid = $kd.sprintf('%05s',$nextnourut);
				$res = $this->m_aplikan->PTL_select($AplikanID);
				$Jumlah = 0;
				for($i2=1;$i2<=$total;$i2++)
				{
					$BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if(str_replace(".","",$this->input->post("payment$i2")) > 0)
					{
						$data = array(
									'Dibayar' => $resbipot['Dibayar'] + str_replace(".","",$this->input->post("payment$i2")),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						$Jumlah = $Jumlah + str_replace(".","",$this->input->post("payment$i2"));
					
						$data_bayar2 = array(
									'BayarMhswID' => $nextid,
									'BIPOTMhswID' => $BIPOTMhswID,
									'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									'Jumlah' => str_replace(".","",$this->input->post("payment$i2")),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_bayar->PTL_insert2($data_bayar2);
						$BIPOTID = $this->input->post("BIPOTID$i2");
					}
				}
				if($Jumlah == 0)
				{
					echo warning("Sorry! You do not input a nominal payment.","../new_students/ptl_payment_form/$AplikanID");
				}
				else
				{
					$data_bayar = array(
									'BayarMhswID' => $nextid,
									'BIPOTID' => $BIPOTID,
									'TahunID' => $res['PMBPeriodID'],
									'RekeningID' => $this->input->post('RekeningID'),
									'PMBID' => $res['PMBID'],
									'MhswID' => $res['MhswID'],
									'TrxID' => 1,
									'Bank' => strtoupper($this->input->post('Bank')),
									'BuktiSetoran' => $this->input->post('BuktiSetoran'),
									'Tanggal' => $this->input->post('Tanggal'),
									'Jumlah' => $Jumlah,
									'Keterangan' => $this->input->post('Keterangan'),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_bayar->PTL_insert($data_bayar);
					$key1 = $this->encryption->encode($nextid);
					$key2 = $this->encryption->encode($AplikanID);
					$key3 = $this->encryption->encode($this->waktu);
					
					$id_akun = $res['PresenterID'];
					$resakun = $this->m_akun->PTL_select($id_akun);
					$email = "";
					$email2 = "";
					if($resakun)
					{
						$email = $resakun['email'];
						$email2 = $resakun['email2'];
					}
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($res['Email']);
					$this->email->bcc("$email,$email2");
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR PAYMENT DATA HAS BEEN INPUTTED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						Dear '<b>".$res['Nama']." - ".$res['ProgramID']." - ".$res['ProdiID']."</b>', your payment data has been inputted. Please check your LMS or contact Finance Team.
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>PAYMENT $res[Nama]</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://lms.esmodjakarta.com'>LMS (Learning Management System)</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					// echo warning("Payment data successfully saved.","../new_students/ptl_edit/$AplikanID");
					if($this->email->send())
					{
						echo warning("Payment data successfully saved.","../new_students/ptl_edit/$AplikanID");
					}
					else
					{
						echo warning("Email server is not active. Payment data successfully saved.","../new_students/ptl_edit/$AplikanID");
					}
				}
			}
		}
		
		function ptl_withdrawal_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$PMBID = $result['PMBID'];
			$mhsw = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			if($mhsw)
			{
				$no = 0;
				foreach($mhsw as $m)
				{
					if($no < 1)
					{
						$BIPOTID = $m->BIPOTID;
					}
					$no++;
				}
				$data['BIPOTID'] = $BIPOTID;
				$data['cekBIPOTID'] = 'YES';
				$data['detail'] = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			}
			$data['rowrekening'] = $this->m_rekening->PTL_all_active();
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			$data['pesan'] = "Withdrawal or Corrections only available if students have SIN.";
			$this->load->view('Portal/v_header');
			$this->load->view('Students_Payment/v_students_messages',$data);
			$this->load->view('Portal/v_footer_table');
			// $this->load->view('Portal/v_header',$hdata);
			// $this->load->view('New_Students/v_new_edit_withdrawal_form',$data);
			// $this->load->view('Portal/v_footer_table',$fdata);
		}
		
		function ptl_withdrawal_insert()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$type = $this->input->post('type');
			$t = "-1";
			if($type == "C")
			{
				$t = "-2";
			}
			$total = $this->input->post('total');
			$pesan = "";
			for($i=1;$i<=$total;$i++)
			{
				$nama = $this->input->post("nama$i");
				$besar = $this->input->post("besar$i");
				$dibayar = $this->input->post("dibayar$i");
				$payment = $this->input->post("payment$i");
				$sisa = $besar - $dibayar;
				if($payment > $dibayar)
				{
					$pesan .= "Withdrawal for '$nama' beyond the limit.".'\n'."Paid ".formatRupiah2($dibayar)." - Withdrawal ".formatRupiah2($payment).'\n\n';
				}
				if($payment < 0)
				{
					$pesan .= "Withdrawal for '$nama' not allowed minus value.".'\n'."Withdrawal ".formatRupiah2($payment).'\n\n';
				}
			}
			if($pesan != "")
			{
				echo warning($pesan,"../new_students/ptl_withdrawal_form/$AplikanID");
			}
			else
			{
				$kd = $this->kd_bayar."-";
				$result = $this->m_bayar->PTL_urut_bayar($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,5,5);
				$nextnourut = $lastnourut + 1;
				$nextid = $kd.sprintf('%05s',$nextnourut);
				$res = $this->m_aplikan->PTL_select($AplikanID);
				$Jumlah = 0;
				for($i2=1;$i2<=$total;$i2++)
				{
					$BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($this->input->post("payment$i2") > 0)
					{
						$data = array(
									'Dibayar' => $resbipot['Dibayar'] - $this->input->post("payment$i2"),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						$Jumlah = $Jumlah + $this->input->post("payment$i2");
					
						$data_bayar2 = array(
									'BayarMhswID' => $nextid,
									'BIPOTMhswID' => $BIPOTMhswID,
									'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									'Jumlah' => $this->input->post("payment$i2"),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_bayar->PTL_insert2($data_bayar2);
						$BIPOTID = $this->input->post("BIPOTID$i2");
					}
				}
				if($Jumlah == 0)
				{
					echo warning("Sorry! You do not input a nominal payment.","../new_students/ptl_withdrawal_form/$AplikanID");
				}
				else
				{
					$data_bayar = array(
									'BayarMhswID' => $nextid,
									'BIPOTID' => $BIPOTID,
									'TahunID' => $res['PMBPeriodID'],
									'RekeningID' => $this->input->post('RekeningID'),
									'PMBID' => $res['PMBID'],
									'MhswID' => $res['MhswID'],
									'TrxID' => $t,
									'Bank' => strtoupper($this->input->post('Bank')),
									'BuktiSetoran' => $this->input->post('BuktiSetoran'),
									'Tanggal' => $this->input->post('Tanggal'),
									'Jumlah' => $Jumlah,
									'Keterangan' => $this->input->post('Keterangan'),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_bayar->PTL_insert($data_bayar);
					$key1 = $this->encryption->encode($nextid);
					$key2 = $this->encryption->encode($AplikanID);
					$key3 = $this->encryption->encode($this->waktu);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($res['Email']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR WITHDRAWAL DATA HAS BEEN PROCESSED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						Dear '<b>$res[Nama] - $res[ProgramID] - $res[ProdiID]</b>', your withdrawal data has been processed. Please check your LMS or contact Finance Team.
						<br/>
						<br/>
						Reason : ".$this->input->post('Keterangan')."
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>PAYMENT $res[Nama]</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://lms.esmodjakarta.com'>LMS (Learning Management System)</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Withdrawal data successfully saved.","../new_students/ptl_edit/$AplikanID");
					}
					else
					{
						echo warning("Email server is not active. Withdrawal data successfully saved.","../new_students/ptl_edit/$AplikanID");
					}
				}
			}
		}
		
		function ptl_bipot_create()
		{
			$this->authentification();
			$link = $this->uri->segment(3);
			$word = explode("_",$link);
			$AplikanID = $word[0];
			$BIPOTID = $word[1];
			$cektahun = $this->session->userdata('new_student_filter_tahun');
			$nsave = 0;
			if($BIPOTID == 0)
			{
				$this->session->set_userdata('pym_status','NOT');
				echo warning("BIPOTID has not been set.","../new_students/ptl_edit/$AplikanID");
			}
			else
			{
				$result = $this->m_aplikan->PTL_select($AplikanID);
				if(($cektahun == "") AND ($result['ProgramID'] != 'SC'))
				{
					$this->session->set_userdata('new_student_status','NOT');
					echo warning("Year ID has not been set.","../new_students/ptl_edit/$AplikanID");
				}
				else
				{
					$result2 = $this->m_master->PTL_bipot_detail($BIPOTID);
					if($result2)
					{
						foreach($result2 as $r)
						{
							$nsave++;
							$BIPOTNamaID = $r->BIPOTNamaID;
							$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
							$data = array(
										'BIPOTMhswRef' => $r->BIPOT2IDRef,
										'KodeID' => 'ES01',
										'PMBID' => $result['PMBID'],
										'TahunID' => $cektahun,
										'BIPOTID' => $BIPOTID,
										'BIPOT2ID' => $r->BIPOT2ID,
										'BIPOTNamaID' => $r->BIPOTNamaID,
										'TambahanNama' => $r->TambahanNama,
										'Nama' => $bipotnama['Nama'],
										'TrxID' => $r->TrxID,
										'Jumlah' => '1',
										'Besar' => $r->Jumlah,
										'Dibayar' => '0',
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_master->PTL_bipot_mhsw_insert($data);
						}
					}
					$data = array(
								'diterima' => 'Y',
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_aplikan->PTL_update($AplikanID,$data);
					
					$da = array(
								'id_akun' => $AplikanID,
								'nama' => $_COOKIE["nama"],
								'aktivitas' => 'PAYMENT: Your payment has been approved by finance team'
								);
					$this->m_aplikan_log->PTL_insert($da);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($result['Email']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR PAYMENT HAS BEEN APPROVED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						<b>ESMOD JAKARTA</b>: Your payment has been approved by Finance Team.
						<br/>
						<br/>
						The next step <a href='http://app.esmodjakarta.com/applicant/pembayaran.xhtml'>(STEP 6/6) FINISH</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					// echo warning("Data PAYMENT with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully approved.","../new_students/ptl_edit/$AplikanID");
					if($this->email->send())
					{
						echo warning("Data PAYMENT with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully approved.","../new_students/ptl_edit/$AplikanID");
					}
					else
					{
						echo warning("Email server is not active. Data PAYMENT THIS SEMESTER with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully approved.","../new_students/ptl_edit/$AplikanID");
					}
				}
			}
		}
		
		function ptl_bipot_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$PMBID = $result['PMBID'];
			$data = array(
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu,
						'NA' => 'Y'
						);
			$result2 = $this->m_master->PTL_bipotmhsw_update_delete($PMBID,$data);
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'PAYMENT: Your payment has been deleted by finance team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR PAYMENT HAS BEEN DELETED BY FINANCE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>FINANCE</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your payment has been deleted by Finance Team.
				<br/>
				<br/>
				The next step <a href='http://app.esmodjakarta.com/applicant/pembayaran.xhtml'>(STEP 6/6)</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			// echo warning("Data PAYMENT with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully deleted.","../new_students/ptl_edit/$AplikanID");
			if($this->email->send())
			{
				echo warning("Data PAYMENT THIS SEMESTER with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully deleted.","../new_students/ptl_edit/$AplikanID");
			}
			else
			{
				echo warning("Email server is not active. Data PAYMENT THIS SEMESTER with APPLICANT CODE '".$AplikanID."' and PMBID '".$result['PMBID']."' successfully deleted.","../new_students/ptl_edit/$AplikanID");
			}
		}
		
		function ptl_create_nim()
		{
			$this->authentification();
			$link = $this->uri->segment(3);
			$word = explode("_",$link);
			$AplikanID = $word[0];
			$min = $this->uri->segment(4);
			$biaya_byr = $this->uri->segment(5);
			$BIPOTID = $this->uri->segment(6);
			$cektahun = $this->session->userdata('new_student_filter_tahun');
			$ceksesi = $this->session->userdata('new_student_filter_sesi');
			if(($cektahun == "") OR ($ceksesi == ""))
			{
				$this->session->set_userdata('new_student_status','NOT');
				echo warning("Year ID or Semester has not been set.","../new_students/ptl_edit/$AplikanID");
			}
			else
			{
				if($biaya_byr >= $min)
				{
					$result = $this->m_aplikan->PTL_select($AplikanID);
					$ProdiID = $result['ProdiID'];
					if($result['ProgramID'] == 'SC')
					{
						$h = "-7";
						$hm = $h * 60;
						$ms = $hm * 60;
						$tahun = gmdate("Y", time()-($ms));
						$kd = $tahun.'1111';
						$resnim = $this->m_mahasiswa->PTL_urut($kd);
						$lastid = $resnim['LAST'];
						
						$lastnourut = substr($lastid,8,4);
						$nextnourut = $lastnourut + 1;
						$next = $kd.sprintf('%04s', $nextnourut);
					}
					else
					{
						$resprod = $this->m_prodi->PTL_select($ProdiID);
						if($resprod)
						{
							$h = "-7";
							$hm = $h * 60;
							$ms = $hm * 60;
							$tahun = gmdate("Y", time()-($ms));
							$w = explode("~",$resprod['FormatNIM']);
							$kd = $tahun.$w[2];
							$resnim = $this->m_mahasiswa->PTL_urut($kd);
							$lastid = $resnim['LAST'];
							
							$lastnourut = substr($lastid,8,4);
							$nextnourut = $lastnourut + 1;
							$next = $kd.sprintf('%04s', $nextnourut);
						}
						else
						{
							$id_akun = $result['PresenterID'];
							$resakun = $this->m_akun->PTL_select($id_akun);
							echo warning("Program ID not selected. Please contact Sales Ambassador : $resakun[nama].","../new_students/ptl_edit/$AplikanID");
						}
					}
					$Nama = $result['Nama'];
					$dat = array(
								'MhswID' => $next,
								'virtual_account' => $result['virtual_account'],
								'Password' => '*97E7471D8',
								'PMBID' => $result['PMBID'],
								'PresenterID' => $result['PresenterID'],
								'PMBFormJualID' => '',
								'BuktiSetoran' => $result['BuktiSetoran'],
								'TahunID' => $cektahun,
								'SemesterAwal' => $ceksesi,
								'KodeID' => 'ES01',
								'BIPOTID' => $BIPOTID,
								'Autodebet' => 'N',
								'Nama' => $result['Nama'],
								'Foto' => $result['foto'],
								'StatusAwalID' => $result['status_awal'],
								'StatusMhswID' => 'A',
								'ProgramID' => $result['ProgramID'],
								'ProdiID' => $ProdiID,
								'KelasID' => '',
								'Kelamin' => $result['Kelamin'],
								'WargaNegara' => $result['WargaNegara'],
								'Kebangsaan' => $result['Kebangsaan'],
								'TempatLahir' => $result['TempatLahir'],
								'TanggalLahir' => $result['TanggalLahir'],
								'Agama' => $result['Agama'],
								'TinggiBadan' => $result['TinggiBadan'],
								'BeratBadan' => $result['BeratBadan'],
								'Alamat' => $result['Alamat'],
								'Kota' => $result['Kota'],
								'RT' => $result['RT'],
								'RW' => $result['RW'],
								'KodePos' => $result['KodePos'],
								'Propinsi' => $result['Propinsi'],
								'Negara' => $result['Negara'],
								'Telepon' => $result['Telepon'],
								'Handphone' => $result['Handphone'],
								'Email' => $result['Email'],
								'AlamatAsal' => $result['AlamatAsal'],
								'KotaAsal' => $result['KotaAsal'],
								'RTAsal' => $result['RTAsal'],
								'RWAsal' => $result['RWAsal'],
								'KodePosAsal' => $result['KodePosAsal'],
								'PropinsiAsal' => $result['PropinsiAsal'],
								'NegaraAsal' => $result['NegaraAsal'],
								'TeleponAsal' => $result['TeleponAsal'],
								'AnakKe' => '',
								'JumlahSaudara' => '',
								'NamaAyah' => $result['NamaAyah'],
								'AgamaAyah' => $result['AgamaAyah'],
								'PendidikanAyah' => $result['PendidikanAyah'],
								'PekerjaanAyah' => $result['PekerjaanAyah'],
								'HidupAyah' => $result['HidupAyah'],
								'NamaIbu' => $result['NamaIbu'],
								'AgamaIbu' => $result['AgamaIbu'],
								'PendidikanIbu' => $result['PendidikanIbu'],
								'PekerjaanIbu' => $result['PekerjaanIbu'],
								'HidupIbu' => $result['HidupIbu'],
								'AlamatOrtu' => $result['AlamatOrtu'],
								'KotaOrtu' => $result['KotaOrtu'],
								'RTOrtu' => $result['RTOrtu'],
								'RWOrtu' => $result['RWOrtu'],
								'KodePosOrtu' => $result['KodePosOrtu'],
								'PropinsiOrtu' => $result['PropinsiOrtu'],
								'NegaraOrtu' => $result['NegaraOrtu'],
								'TeleponOrtu' => $result['TeleponOrtu'],
								'HandphoneOrtu' => $result['HandphoneOrtu'],
								'EmailOrtu' => $result['EmailOrtu'],
								'AlamatKantor' => '',
								'TeleponKantor' => '',
								'PendidikanTerakhir' => $result['PendidikanTerakhir'],
								'AsalSekolah' => $result['AsalSekolah'],
								'JenisSekolahID' => $result['JenisSekolahID'],
								'AlamatSekolah' => $result['AlamatSekolah'],
								'KotaSekolah' => $result['KotaSekolah'],
								'JurusanSekolah' => $result['JurusanSekolah'],
								'NilaiSekolah' => $result['NilaiSekolah'],
								'TahunLulus' => $result['TahunLulus'],
								'IjazahSekolah' => '',
								'AsalPT' => '',
								'MhswIDAsalPT' => '',
								'ProdiAsalPT' => '',
								'LulusAsalPT' => '',
								'TglLulusAsalPT' => '',
								'IPKAsalPT' => '',
								'Pilihan1' => $result['Pilihan1'],
								'Pilihan2' => $result['Pilihan2'],
								'Pilihan3' => $result['Pilihan3'],
								'BatasStudi' => '',
								'Harga' => $result['Harga'],
								'SudahBayar' => $result['SudahBayar'],
								'TotalBiaya' => $result['total_biaya'],
								'TotalBayar' => $result['total_bayar'],
								'Dispensasi' => '',
								'DispensasiID' => '',
								'JudulDispensasi' => '',
								'CatatanDispensasi' => '',
								'NamaBank' => '',
								'NomerRekening' => '',
								'IPK' => '',
								'TotalSKS' => '',
								'TotalSKSPindah' => '',
								'WisudaID' => '',
								'TAID' => '',
								'Predikat' => '',
								'TahunKeluar' => '',
								'CatatanKeluar' => '',
								'NoIjazah' => '',
								'TglIjazah' => '',
								'TotalSKS_' => '',
								'Catatan' => '',
								'PrestasiTambahan' => '',
								'login_buat' => $this->waktu,
								'tanggal_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
								);
					$this->m_mahasiswa->PTL_insert($dat);
					
					$Handphone = $result['Handphone'];
					if($Handphone != "")
					{
						if(substr($Handphone,0,2) == "62")
						{
							$prov = "0".substr($Handphone,2,3);
						}
						else
						{
							$prov = substr($Handphone,0,4);
						}
						if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
						{
							$data = array(
										'id_user' => $_COOKIE["id_akun"],
										'nama' => $_COOKIE["nama"],
										'aplikasi' => 'FINANCE',
										'judul_sms' => 'Notifikasi Create NIM',
										'nomor_hp' => $Handphone,
										'pesan' => "ESMOD (NO-REPLY): Your Student Identification Number is $next",
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
						}
						else
						{
							$url = "http://103.16.199.187/masking/send_post.php";
							$rows = array(
										"username" => "esmodjkt",
										"password" => "esmj@kr00t",
										"hp" => "$Handphone",
										"message" => "Your Student Identification Number is $next"
										);
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_URL, $url );
							curl_setopt($curl, CURLOPT_POST, TRUE );
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
							curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
							curl_setopt($curl, CURLOPT_HEADER, FALSE );
							curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
							curl_setopt($curl, CURLOPT_TIMEOUT, 60);
							$htm = curl_exec($curl);
							if(curl_errno($curl) !== 0)
							{
								$data = array(
											'id_user' => $_COOKIE["id_akun"],
											'nama' => $_COOKIE["nama"],
											'aplikasi' => 'FINANCE',
											'judul_sms' => 'Notifikasi Create NIM',
											'nomor_hp' => $Handphone,
											'pesan' => "ESMOD (NO-REPLY): Your Student Identification Number is $next",
											'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_buat' => $this->waktu
											);
								$this->m_sms->PTL_insert($data);
								error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
							}
							curl_close($curl);
							print_r($htm);
						}
					}
					
					$PMBID = $word[1];
					$result2 = $this->m_master->PTL_bipot_mhsw_select($PMBID);
					if($result2)
					{
						foreach($result2 as $r)
						{
							$BIPOTMhswID = $r->BIPOTMhswID;
							$data = array(
										'MhswID' => $next,
										'TahunID' => $cektahun,
										'tanggal_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'login_edit' => $this->waktu
										);
							$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						}
					}
					$rowbayar = $this->m_bayar->PTL_all_select($PMBID,$BIPOTID);
					if($rowbayar)
					{
						foreach($rowbayar as $rb)
						{
							$BayarMhswID = $rb->BayarMhswID;
							$data_bayar = array(
												'KodeID' => 'ES01',
												'TahunID' => $cektahun,
												'MhswID' => $next,
												'tanggal_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
												'login_edit' => $this->waktu
												);
							$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
						}
					}
					$data = array(
								'MhswID' => $next,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_aplikan->PTL_update($AplikanID,$data);
					
					$da = array(
								'id_akun' => $AplikanID,
								'nama' => $_COOKIE["nama"],
								'aktivitas' => 'SIN (Student Identification Number) has been created by finance team'
								);
					$this->m_aplikan_log->PTL_insert($da);
					
					if(($ceksesi == 1) OR ($ceksesi == 2))
					{
						$ses = 1;
					}
					if(($ceksesi == 3) OR ($ceksesi == 4))
					{
						$ses = 2;
					}
					if(($ceksesi == 5) OR ($ceksesi == 6))
					{
						$ses = 3;
					}
					$data_khs = array(
								'TahunID' => $cektahun,
								'KodeID' => 'ES01',
								'ProgramID' => $result['ProgramID'],
								'ProdiID' => $result['ProdiID'],
								'MhswID' => $next,
								'StatusMhswID' => 'A',
								'Sesi' => $ceksesi,
								'BIPOTID' => $this->uri->segment(6),
								'Biaya' => $this->uri->segment(7),
								'Potongan' => $this->uri->segment(8),
								'Bayar' => $this->uri->segment(5),
								'MaxSKS' => '27',
								'TahunKe' => $ses,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_khs->PTL_insert($data_khs);
					$id_akun = $result['PresenterID'];
					$resakun = $this->m_akun->PTL_select($id_akun);
					$email = "";
					$email2 = "";
					if($resakun)
					{
						$email = $resakun['email'];
						$email2 = $resakun['email2'];
					}
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($result['Email']);
					$this->email->bcc("miha@esmodjakarta.com,$email,$email2");
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('SIN HAS BEEN CREATED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						<br/>
						Dear ".strtoupper($Nama).",
						<br/>
						<br/>
						<b>ESMOD JAKARTA</b>: Your SIN (Student Identification Number) has been created by Finance Team.
						<br/>
						<br/>
						Now you have registered as a NEW STUDENT, but the subject courses are being processed by the Academic Team.
						Portal applicant http://app.esmodjakarta.com/applicant/ your previous address is no longer active.
						Access your account as a NEW STUDENT switch to http://app.esmodjakarta.com/lms address. Here are your access details:
						<br/>
						<br/>
						Username: <b>$next</b>
						<br/>
						Password: <b>0000</b>
						<br/>
						<br/>
						Login to your <a href='http://app.esmodjakarta.com/lms'>LMS (Learning Management System)</a>.
						<br/>
						<font color='red'><b>Please change your password immediately!</b></font>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Data SIN (Student Identification Number) '".$next."' has been created.","../new_students/ptl_edit/$AplikanID");
					}
					else
					{
						echo warning("Email server is not active. Data SIN (Student Identification Number) '".$next."' has been created.","../new_students/ptl_edit/$AplikanID");
					}
				}
				else
				{
					echo warning("Payment data must exceed the minimum payment.","../new_students/ptl_edit/$AplikanID");
				}
			}
		}
		
		function ptl_bipotmhs_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$BIPOTMhswID = $this->uri->segment(4);
			$BIPOTNamaID = $this->uri->segment(5);
			$Dibayar = $this->uri->segment(6);
			// $result = $this->m_khs->PTL_select($KHSID);
			// if($Dibayar > 0)
			// {
				// $Deposit = $result['Deposit'] + $Dibayar;
				// $data = array(
							// 'Deposit' => $Deposit,
							// 'Tutup' => 'N',
							// 'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							// 'tanggal_edit' => $this->waktu
							// );
				// $this->m_khs->PTL_update($KHSID,$data);
			// }
			$data = array(
						'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_hapus' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
			$res = $this->m_master->PTL_select($BIPOTNamaID);
			$nama = "";
			if($res)
			{
				$nama = $res['Nama'];
			}
			$res2 = $this->m_bayar->PTL_select_bipot($BIPOTMhswID);
			if($res2)
			{
				$BayarMhswID = $res2['BayarMhswID'];
				$res3 = $this->m_bayar->PTL_select($BayarMhswID);
				if($res3)
				{
					if($res2['Jumlah'] == $res3['Jumlah'])
					{
						$this->m_bayar->PTL_delete($BayarMhswID);
						$this->m_bayar->PTL_delete_bipot2($BIPOTMhswID);
					}
					else
					{
						$jum = $res3['Jumlah'] - $res2['Jumlah'];
						$data_bayar = array(
									'Jumlah' => $jum,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu,
									);
						$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
						$this->m_bayar->PTL_delete_bipot2($BIPOTMhswID);
					}
				}
			}
			echo warning("Data BIPOT with ID '$BIPOTMhswID - ".$nama."' successfully deleted.","../new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_filter_bipotmhs_add_cost_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID2');
			$word = explode(" ",$link);
			$cekBIPOTNamaID2 = $word[0];
			$PMBID = $word[1];
			$BIPOTID = $word[2];
			$BIPOT2ID = $word[3];
			if($cekBIPOTNamaID2 != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result1 = $this->m_master->PTL_select($BIPOTNamaID);
				$result2 = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$this->session->set_userdata('cekRekeningID2',$result1['RekeningID']);
				$this->session->set_userdata('cekDefJumlah2',$result2['Jumlah']);
				$this->session->set_userdata('cekBIPOTNamaID2',$cekBIPOTNamaID2);
				$this->session->set_userdata('cekBIPOT2ID2',$BIPOT2ID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID2','NONE');
				$this->session->set_userdata('cekDefJumlah2','NONE');
				$this->session->set_userdata('cekBIPOTNamaID2','NONE');
				$this->session->unset_userdata('cekBIPOT2ID2');
			}
			redirect("new_students/ptl_bipotmhs_cost_form/$PMBID/$BIPOTID");
		}
		
		function ptl_filter_bipotmhs_add_discounts_form()
		{
			$this->authentification();
			$link = $this->input->post('cekBIPOTNamaID');
			$word = explode(" ",$link);
			$cekBIPOTNamaID2 = $word[0];
			$PMBID = $word[1];
			$BIPOTID = $word[2];
			$BIPOT2ID = $word[3];
			if($cekBIPOTNamaID2 != "")
			{
				$BIPOTNamaID = $cekBIPOTNamaID2;
				$result1 = $this->m_master->PTL_select($BIPOTNamaID);
				$result2 = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$this->session->set_userdata('cekRekeningID',$result1['RekeningID']);
				$this->session->set_userdata('cekDefJumlah',$result2['Jumlah']);
				$this->session->set_userdata('cekBIPOTNamaID',$cekBIPOTNamaID2);
				$this->session->set_userdata('cekBIPOT2ID',$BIPOT2ID);
			}
			else
			{
				$this->session->set_userdata('cekRekeningID','NONE');
				$this->session->set_userdata('cekDefJumlah','NONE');
				$this->session->set_userdata('cekBIPOTNamaID','NONE');
				$this->session->unset_userdata('cekBIPOT2ID');
			}
			redirect("new_students/ptl_bipotmhs_discounts_form/$PMBID/$BIPOTID");
		}
		
		function ptl_bipotmhs_cost_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$data['PMBID'] = $this->uri->segment(3);
			$BIPOTID = $this->uri->segment(4);
			$data['BIPOTID'] = $this->uri->segment(4);
			$PMBID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select_pmb($PMBID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['biaya'] = $this->m_master->PTL_pym_bipot_detail_biaya($BIPOTID);
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_edit_bipot_cost_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipotmhs_discounts_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$data['PMBID'] = $this->uri->segment(3);
			$BIPOTID = $this->uri->segment(4);
			$data['BIPOTID'] = $this->uri->segment(4);
			$PMBID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select_pmb($PMBID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['potongan'] = $this->m_master->PTL_pym_bipot_detail_potongan($BIPOTID);
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_edit_bipot_discounts_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_bipotmhs_cost_insert()
		{
			$this->authentification();
			$PMBID = $this->input->post('PMBID');
			$result = $this->m_aplikan->PTL_select_pmb($PMBID);
			$AplikanID = $result['AplikanID'];
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			if($BIPOT2ID == "")
			{
				echo warning("Variable Name not selected.","../new_students/ptl_bipotmhs_cost_form/$PMBID/$BIPOTID");
			}
			else
			{
				$Jumlah = str_replace(".","",$this->input->post('Jumlah'));
				
				$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$BIPOTNamaID = $result['BIPOTNamaID'];
				$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
				
				$data = array(
							'BIPOTMhswRef' => $result['BIPOT2IDRef'],
							'KodeID' => 'ES01',
							'PMBID' => $PMBID,
							'BIPOTID' => $BIPOTID,
							'BIPOT2ID' => $BIPOT2ID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Nama' => $bipotnama['Nama'],
							'TrxID' => '1',
							'Jumlah' => '1',
							'Besar' => $Jumlah,
							'Dibayar' => '0',
							'login_buat' => $this->waktu,
							'tanggal_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"]
							);
				$this->m_master->PTL_bipot_mhsw_insert($data);
				echo warning("Data COST with CODE '".$BIPOTID."' successfully added.","../new_students/ptl_edit/$AplikanID");
			}
		}
		
		function ptl_bipotmhs_discounts_insert()
		{
			$this->authentification();
			$PMBID = $this->input->post('PMBID');
			$result = $this->m_aplikan->PTL_select_pmb($PMBID);
			$AplikanID = $result['AplikanID'];
			$BIPOTID = $this->input->post('BIPOTID');
			$BIPOT2ID = $this->input->post('BIPOT2ID');
			if($BIPOT2ID == "")
			{
				echo warning("Variable Name not selected.","../new_students/ptl_bipotmhs_discounts_form/$PMBID/$BIPOTID");
			}
			else
			{
				$Jumlah = str_replace(".","",$this->input->post('Jumlah'));
				
				$result = $this->m_master->PTL_bipot_detail_select($BIPOT2ID);
				$BIPOTNamaID = $result['BIPOTNamaID'];
				$bipotnama = $this->m_master->PTL_select($BIPOTNamaID);
				
				$data = array(
							'BIPOTMhswRef' => $result['BIPOT2IDRef'],
							'KodeID' => 'ES01',
							'PMBID' => $PMBID,
							'BIPOTID' => $BIPOTID,
							'BIPOT2ID' => $BIPOT2ID,
							'BIPOTNamaID' => $BIPOTNamaID,
							'TambahanNama' => $this->input->post('TambahanNama'),
							'Nama' => $bipotnama['Nama'],
							'TrxID' => '-1',
							'Jumlah' => '1',
							'Besar' => $Jumlah,
							'Dibayar' => '0',
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_master->PTL_bipot_mhsw_insert($data);
				echo warning("Data COST with CODE '".$BIPOTID."' successfully added.","../new_students/ptl_edit/$AplikanID");
			}
		}
		
		function ptl_bipotmhs_piece_form2()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$data['PMBID'] = $this->uri->segment(3);
			$data['BIPOTID'] = $this->uri->segment(4);
			$PMBID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select_pmb($PMBID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['potongan'] = $this->m_master->PTL_master_potongan();
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_edit_bipot_piece_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_reminder()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['Nama'] = $result['Nama'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com,ichaa@esmodjakarta.com,anita@esmodjakarta.com');
			$this->email->subject('YOUR PAYMENT THIS SEMESTER HAS BEEN EXPIRED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>FINANCE</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Dear '".$result['Nama']." - ".$result['ProgramID']." - ".$result['ProdiID']."', your payment this semester has been expired. Please check your LMS or contact Finance Team.
				<br/>
				<br/>
				For more information, please login to <a href='http://lms.esmodjakarta.com'>LMS (Learning Management System)</a>
				<br/>
				You need to login first.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("REMINDER to Applicant ID '".$AplikanID." - ".$result['Nama']." - ".$result['ProgramID']." - ".$result['ProdiID']."' successfully sent.","../new_students/ptl_edit/$AplikanID");
			}
			else
			{
				echo warning("Email server is not active. Please try again...","../new_students/ptl_edit/$AplikanID");
			}
		}
		
		function ptl_pending()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$cekperiode = $this->session->userdata('nws_filter_pending_period');
			$cekjurusan = $this->session->userdata('nws_filter_pending_jur');
			$cekprodi = $this->session->userdata('nws_filter_pending_prodi');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_spesifik_semester_pending($cekperiode,$cekjurusan,$cekprodi);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_pending',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_postpone()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$cekperiode = $this->session->userdata('nws_filter_postpone_period');
			$cekjurusan = $this->session->userdata('nws_filter_postpone_jur');
			$cekprodi = $this->session->userdata('nws_filter_postpone_prodi');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_spesifik_new_postpone($cekperiode,$cekjurusan,$cekprodi);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('New_Students/v_new_postpone',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_payment_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$BayarMhswID = $this->uri->segment(3);
			$data['BayarMhswID'] = $this->uri->segment(3);
			$data['AplikanID'] = $this->uri->segment(4);
			$result = $this->m_bayar->PTL_select($BayarMhswID);
			$data['RekeningID'] = $result['RekeningID'];
			$data['Bank'] = $result['Bank'];
			$data['BuktiSetoran'] = $result['BuktiSetoran'];
			$data['Tanggal'] = $result['Tanggal'];
			$data['Keterangan'] = $result['Keterangan'];
			$hdata['date'] = 'USE';
			$fdata['date'] = 'USE';
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('New_Students/v_new_edit_payment_date',$data);
			$this->load->view('Portal/v_footer_table',$fdata);
		}
		
		function ptl_payment_update()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$BayarMhswID = $this->input->post('BayarMhswID');
			$data_bayar = array(
							'BuktiSetoran' => $this->input->post('BuktiSetoran'),
							'Tanggal' => $this->input->post('Tanggal'),
							'Keterangan' => $this->input->post('Keterangan'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
			$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
			echo warning("Data PAYMENT DATE successfully updated.","../new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_pdf_invoice()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$BIPOTID = $this->uri->segment(3);
			$AplikanID = $this->uri->segment(4);
			$res = $this->m_aplikan->PTL_select($AplikanID);
			$PMBID = "";
			$ProgramID = "";
			$ProdiID = "";
			$nama = "";
			if($res)
			{
				$PMBID = $res['PMBID'];
				$ProgramID = $res['ProgramID'];
				$ProdiID = $res['ProdiID'];
				$nama = $res['Nama'];
			}
			$result = $this->m_master->PTL_bipot_mhsw_select($PMBID);
			$prod = $this->m_prodi->PTL_select($ProdiID);
			$namaprod = "";
			if($prod)
			{
				$namaprod = $prod['Nama'];
			}
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"INVOICE ID $AplikanID$BIPOTID",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$id_akun = $AplikanID;
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "APL ID / PMB ID" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$id_akun." / ".$PMBID , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -1, "NAME", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -2, "PROGRAM", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1 , -2, ": ".$ProgramID." - ".strtoupper($namaprod)." ($ProdiID)", 0, "", "L");
			$this->fpdf->Ln(-0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.5 , 0.7, "#" , "T", 0, "C");
			$this->fpdf->Cell(4.5 , 0.7, "DESCRIPTION" , "T", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "AMOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "TOTAL" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "DISCOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "REMAINING" , "T", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$pot = 0;
			$bia = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "-1")
					{
						$BIPOTNamaID_detail = $hasil['BIPOTNamaIDRef'];
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						if(!$resref_detail)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							if($TambahanNama != "")
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
								$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							}
							$no++;
							$pot = $pot + $r->Besar;
						}
					}
				}
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "1")
					{
						$BIPOTNamaIDRef = $BIPOTNamaID;
						$resref = $this->m_master->PTL_select_ref($BIPOTNamaIDRef);
						$ref = "";
						if($resref)
						{
							$ref = $resref['BIPOTNamaID'];
						}
						$BIPOTNamaID_detail = $ref;
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$ref_detail = "";
						$ref_jumlah = "";
						$ref_jumlah2 = "";
						if($resref_detail)
						{
							$BIPOTNamaID = $BIPOTNamaID_detail;
							$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
							if($resref_detail2)
							{
								$ref_detail = $resref_detail2["Nama"];
								$ref_jumlah = $resref_detail2["DefJumlah"];
								$ref_jumlah2 = $resref_detail2["DefJumlah"];
							}
						}
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
						$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Dibayar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($ref_jumlah).")", "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar - $r->Dibayar - $ref_jumlah), "", 0, "R");
						if($TambahanNama != "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
						}
						$no++;
						$bia = $bia + ($r->Besar - $r->Dibayar - $ref_jumlah);
					}
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "THE TOTAL TO BE PAID", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($bia - $pot), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Ln();
			$rowbukti = $this->m_bayar->PTL_all_select($PMBID,$BIPOTID);
			if($rowbukti)
			{
				$this->fpdf->SetFont("Times","",8);
				$n0 = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n0++;
					}
				}
				if($n0 > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(7.2 , 0.5, "PAYMENT HISTORY", "LBTR", 0, "C");
				}
				$this->fpdf->Ln();
				$this->fpdf->Cell(0.5 , 0.5, "#", "LBTR", 0, "C");
				$this->fpdf->Cell(1.7 , 0.5, "Date", "LBTR", 0, "C");
				$this->fpdf->Cell(2 , 0.5, "Reference", "LBTR", 0, "C");
				$this->fpdf->Cell(3 , 0.5, "Nominal", "LBTR", 0, "C");
				$n = 0;
				$total_bayar = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n++;
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $n, "LBTR", 0, "C");
						$this->fpdf->Cell(1.7 , 0.5, tgl_singkat_eng($row->Tanggal), "LBTR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $row->BayarMhswID, "LBTR", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($row->Jumlah), "LBTR", 0, "R");
						$total_bayar = $total_bayar + $row->Jumlah;
					}
				}
				if($n > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(4.2 , 0.5, "TOTAL", "LBTR", 0, "C");
					$this->fpdf->Cell(3 , 0.5, formatRupiah3($total_bayar), "LBTR", 0, "R");
				}
			}
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(4.6 , 0.5, "NB", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Pembayaran dapat di transfer ke rekening", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "An: Yayasan PDKI-BDN-ESMOD", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "No. Rekening: 218-300-3496", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Bank: Bank BCA", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Cabang: Cabang Blok A Cipete, Jakarta", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Swift Code: CENAIDA", "", 0, "L");
			$this->fpdf->Output("INVOICE_".$AplikanID.$BIPOTID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function ptl_print_payment()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$BayarMhswID = $this->uri->segment(3);
			$AplikanID = $this->uri->segment(4);
			$status = $this->uri->segment(5);
			$res = $this->m_aplikan->PTL_select($AplikanID);
			$PMBID = $res['PMBID'];
			$resmain = $this->m_bayar->PTL_select($BayarMhswID);
			$result = $this->m_bayar->PTL_all_select2($BayarMhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$tambah = $resmain['Cetak'] + 1;
			$data_bayar = array(
							'Cetak' => $tambah
							);
			$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
			
			$judul = "OFFICIAL RECEIPT";
			$to1 = "Sudah Terima Dari";
			$to2 = "(Received From)";
			$pay1 = "Untuk Pembayaran";
			$pay2 = "(Being Payment For)";
			$pesan = "NB : Pembayaran yang sudah ditransfer tidak dapat ditarik kembali";
			$file = "PAYMENT_";
			if($resmain['TrxID'] == "-1")
			{
				$judul = "WITHDRAWAL";
				$to1 = "Penarikan Oleh";
				$to2 = "(Withdrawal by)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : Penarikan dengan alasan khusus";
				$file = "WITHDRAWAL_";
			}
			if($resmain['TrxID'] == "-2")
			{
				$judul = "CORRECTIONS";
				$to1 = "Dikoreksi Untuk";
				$to2 = "(Corrected for)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Dikoreksi Oleh (Corrected By)";
				$file = "CORRECTIONS_";
			}
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(28,1,"$judul",0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,1,"NO : $BayarMhswID",0,0,"R");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY()-0.6,12.5,1.6);
			
			$this->fpdf->Ln(1.2);
			$id_akun = $AplikanID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->SetFont("helvetica","BI",20);
			$this->fpdf->Cell(26,0.7,$status,0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->Line(1,4.7,28.9,4.7);
			$this->fpdf->Line(1,4.75,28.9,4.75);
			
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $to1, "", 0, "L");
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(3, 0.7, "PMB ID :", "", 0, "L");
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(5.5, 0.7, $res['PMBID'], "", 0, "L");
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(2.2, 0.7, "Nama :", "", 0, "L");
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(5.8, 0.7, $nama, "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(6.5, 0.7, $to2, "", 0, "L");
			$this->fpdf->Cell(3, 0.7, "", "", 0, "L");
			$this->fpdf->Cell(5.5, 0.7, "", "T", 0, "L");
			$this->fpdf->Cell(2.2, 0.7, "(Name)", "", 0, "L");
			$this->fpdf->Cell(10.6, 0.7, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$terbilang = ucwords(toTerbilang($resmain['Jumlah'])) . ' Rupiah';  
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Banyaknya", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, $terbilang, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(6.5, 0.7, "(The amount of)", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Check/BG/Trans./Cash", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.7, $resmain['Bank'], "", 0, "L");
			$this->fpdf->Cell(7.2, 0.7, "Tanggal Pembayaran :", "", 0, "L");
			$this->fpdf->Cell(2.8, 0.7, tgl_singkat_eng($resmain["Tanggal"]), "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "(Payment Date)", "", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $pay1, "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(6.5, 0.7, $pay2, "", 0, "L");
			$no = 1;
			$tot = count($result);
			$Jumlah = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$BIPOTMhswID = $r->BIPOTMhswID;
					$hasil2 = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($no == 1)
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->SetFont("Times","B",18);
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					else
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(6.5 , 0.7, "", "", 0, "L");
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					$Jumlah = $Jumlah + $r->Jumlah;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln(2);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5 , 1, "Jumlah / Amount", "", 0, "L");
			$this->fpdf->Cell(7 , 1, formatRupiah3($Jumlah), "LBTR", 0, "C");
			$this->fpdf->Cell(6 , 1, "", "", 0, "C");
			$this->fpdf->Cell(8 , 1, "Yang Menerima (Received By)", "", 0, "C");
			$this->fpdf->Ln();
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(21.3 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(4.6 , 0.5, $pesan, "", 0, "L");
			$this->fpdf->Ln(3.6);
			$this->fpdf->Cell(20 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(7 , 0.5, "", "B", 0, "C");
			$this->fpdf->Output($file.$AplikanID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function ptl_pdf_pending()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$cekperiode = $this->session->userdata('nws_filter_pending_period');
			$cekjurusan = $this->session->userdata('nws_filter_pending_jur');
			$cekprodi = $this->session->userdata('nws_filter_pending_prodi');
			$result = $this->m_aplikan->PTL_all_spesifik_semester_pending($cekperiode,$cekjurusan,$cekprodi);
			$ProdiID = $cekprodi;
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"REPORT PAYMENT FORM",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "FILTER", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$cekperiode.$cekjurusan.$cekprodi, 0, "", "L");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.8 , 0.7, "#" , "LBTR", 0, "C");
			$this->fpdf->Cell(2.5 , 0.7, "PMBID" , "BTR", 0, "C");
			$this->fpdf->Cell(7.5 , 0.7, "NAME" , "BTR", 0, "C");
			$this->fpdf->Cell(5.2 , 0.7, "PROGRAM" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$pay = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$PMBID = $r->PMBID;
					$detail = $this->m_master->PTL_bipot_mhsw_select($PMBID);
					$biaya_byr = 0;
					if($detail)
					{
						foreach($detail as $row)
						{
							if($row->TrxID == "1")
							{
								$biaya_byr = $biaya_byr + $row->Dibayar;
							}
						}
					}
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "BR", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "BR", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "BR", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($biaya_byr), "BR", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "R", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "R", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "R", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($biaya_byr), "R", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					$no++;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "TOTAL PAYMENT", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($pay), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Output("REPORT_STUDENT_PAYMENT_PENDING_".$cekperiode.$cekjurusan.$cekprodi.".pdf","I");
		}
		
		function ptl_pdf_postpone()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$cekperiode = $this->session->userdata('new_filter_period');
			$cekjurusan = $this->session->userdata('new_filter_jur');
			$cekprodi = $this->session->userdata('new_filter_prodi');
			$result = $this->m_aplikan->PTL_all_spesifik_new_postpone($cekperiode,$cekjurusan,$cekprodi);
			$ProdiID = $cekprodi;
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"REPORT PAYMENT FORM",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "FILTER", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$cekperiode.$cekjurusan.$cekprodi, 0, "", "L");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.8 , 0.7, "#" , "LBTR", 0, "C");
			$this->fpdf->Cell(2.5 , 0.7, "PMBID" , "BTR", 0, "C");
			$this->fpdf->Cell(7.5 , 0.7, "NAME" , "BTR", 0, "C");
			$this->fpdf->Cell(5.2 , 0.7, "PROGRAM" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$pay = 0;
			if($result)
			{
				foreach($result as $r)
				{
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "BR", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "BR", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "BR", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->total_bayar), "BR", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "R", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "R", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "R", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->total_bayar), "R", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					$no++;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "TOTAL PAYMENT", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($pay), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Output("REPORT_STUDENT_PAYMENT_POSTPONE_".$cekperiode.$cekjurusan.$cekprodi.".pdf","I");
		}
		
		function ptl_pdf_list()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$cekperiode = $this->session->userdata('new_filter_period');
			$cekjurusan = $this->session->userdata('new_filter_jur');
			$cekprodi = $this->session->userdata('new_filter_prodi');
			$result = $this->m_aplikan->PTL_all_spesifik_new_done($cekperiode,$cekjurusan,$cekprodi);
			$ProdiID = $cekprodi;
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"REPORT PAYMENT FORM",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "FILTER", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$cekperiode.$cekjurusan.$cekprodi, 0, "", "L");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.8 , 0.7, "#" , "LBTR", 0, "C");
			$this->fpdf->Cell(2.5 , 0.7, "PMBID" , "BTR", 0, "C");
			$this->fpdf->Cell(7.5 , 0.7, "NAME" , "BTR", 0, "C");
			$this->fpdf->Cell(5.2 , 0.7, "PROGRAM" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$pay = 0;
			if($result)
			{
				foreach($result as $r)
				{
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "BR", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "BR", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "BR", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->total_bayar), "BR", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2.5 , 0.5, $r->PMBID, "R", 0, "C");
						$this->fpdf->Cell(7.5 , 0.5, $r->Nama, "R", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "R", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->total_bayar), "R", 0, "R");
						$pay = $pay + $r->total_bayar;
					}
					$no++;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "TOTAL PAYMENT", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($pay), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Output("REPORT_STUDENT_PAYMENT_LIST_".$cekperiode.$cekjurusan.$cekprodi.".pdf","I");
		}
		
		function ptl_bipotmhs_payment_delete()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$AplikanID = $this->uri->segment(3);
			$BayarMhswID = $this->uri->segment(4);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$total_bayar = 0;
			if($result)
			{
				$total_bayar = $result['total_bayar'];
			}
			$resbayar = $this->m_bayar->PTL_select($BayarMhswID);
			$Jumlah = 0;
			if($resbayar)
			{
				$Jumlah = $resbayar['Jumlah'];
			}
			$rowrecord = $this->m_bayar->PTL_all_select2($BayarMhswID);
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$BIPOTMhswID = $row->BIPOTMhswID;
					$JumlahA = $row->Jumlah;
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					$Dibayar = 0;
					if($resbipot)
					{
						$Dibayar = $resbipot['Dibayar'];
					}
					$data = array(
								'Dibayar' => $Dibayar - $JumlahA,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
				}
			}
			$this->m_bayar->PTL_delete($BayarMhswID);
			$this->m_bayar->PTL_delete2($BayarMhswID);
			$data = array(
						'total_bayar' => $total_bayar - $Jumlah,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			echo warning("Data successfully deleted.","../new_students/ptl_edit/$AplikanID");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$PMBID = $result['PMBID'];
			$this->m_master->PTL_nspym_delete($PMBID);
			$rowrecord = $this->m_bayar->PTL_all_nspym_select($PMBID);
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$BayarMhswID = $row->BayarMhswID;
					$this->m_bayar->PTL_delete2($BayarMhswID);
				}
			}
			$this->m_bayar->PTL_nspym_delete($PMBID);
			$this->m_aplikan->PTL_delete($AplikanID);
			echo warning("Data successfully deleted.","../new_students/ptl_pending");
		}
		
		function ptl_correction_cost()
		{
			$this->authentification();
			$this->session->set_userdata('menu','new_students');
			$BIPOTMhswID = $this->uri->segment(3);
			$data['BIPOTMhswID'] = $BIPOTMhswID;
			$res = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
			$data['Besar'] = formatRupiah2($res['Besar']);
			$data['Dibayar'] = formatRupiah2($res['Dibayar']);
			$data['login_buat'] = $res['login_buat'];
			$data['tanggal_buat'] = $res['tanggal_buat'];
			$data['login_edit'] = $res['login_edit'];
			$data['tanggal_edit'] = $res['tanggal_edit'];
			$AplikanID = $this->uri->segment(4);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$this->load->view('Portal/v_header_master');
			$this->load->view('New_Students/v_new_edit_correction_cost',$data);
			$this->load->view('Portal/v_footer_master');
		}
		
		function ptl_correction_cost_update()
		{
			$this->authentification();
			$BIPOTMhswID = $this->input->post('BIPOTMhswID');
			$AplikanID = $this->input->post('AplikanID');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if($_COOKIE["id_akun"] == $username)
			{
				echo warning("Sorry, you can not use your username. Please contact your superior.","../new_students/ptl_correction_cost/$BIPOTMhswID/$AplikanID");
			}
			else
			{
				$auth = $this->m_akses->PTL_select_auth($username,$password);
				if($auth)
				{
					$Besar = $this->input->post('Besar');
					$Dibayar = $this->input->post('Dibayar');
					$alasan = $this->input->post('alasan');
					$data = array(
								'Besar' => str_replace(".","",$Besar),
								'Dibayar' => str_replace(".","",$Dibayar),
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
					$datalog = array(
								'pk1' => $BIPOTMhswID,
								'pk2' => $AplikanID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => 'FINANCE',
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "NSPYM01-COR01",
								'aktifitas' => $alasan,
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("Your data has been changed.","../new_students/ptl_edit/$AplikanID");
				}
				else
				{
					$auth2 = $this->m_akses->PTL_select_user($username,$password);
					if($auth2)
					{
						echo warning("Sorry, your superior must be in the same department.","../new_students/ptl_correction_cost/$BIPOTMhswID/$AplikanID");
					}
					else
					{
						echo warning("Username or Password incorrect.","../new_students/ptl_correction_cost/$BIPOTMhswID/$AplikanID");
					}
				}
			}
		}
	}
?>