<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Bank_account extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_waktu = gmdate("YmdHis", time()-($ms));
			$this->tanggal_kirim = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_rekening');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/PTL_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','bank_account');
			$data['rowrecord'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Bank_Account/v_bank_account',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','bank_account');
			$this->load->view('Portal/v_header');
			$this->load->view('Bank_Account/v_bank_account_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'RekeningID' => $this->input->post('RekeningID'),
						'Nama' => $this->input->post('Nama'),
						'Bank' => strtoupper($this->input->post('Bank')),
						'Cabang' => $this->input->post('Cabang'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_rekening->PTL_insert($data);
			echo warning("Your data successfully added.","../bank_account");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','bank_account');
			$RekeningID = $this->uri->segment(3);
			$result = $this->m_rekening->PTL_select($RekeningID);
			$data['RekeningID'] = $result['RekeningID'];
			$data['Nama'] = $result['Nama'];
			$data['Bank'] = $result['Bank'];
			$data['Cabang'] = $result['Cabang'];
			$this->load->view('Portal/v_header');
			$this->load->view('Bank_Account/v_bank_account_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$RekeningID = $this->input->post('RekeningID');
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'Bank' => strtoupper($this->input->post('Bank')),
						'Cabang' => $this->input->post('Cabang'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_rekening->PTL_update($RekeningID,$data);
			echo warning("Your data successfully updated.","../bank_account");
		}
	}
?>