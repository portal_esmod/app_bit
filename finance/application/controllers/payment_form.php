<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Payment_form extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->sesi = gmdate("YmdHis", time()-($ms));
			$this->nim = gmdate("Yis", time()-($ms));
			$this->kd_bayar = gmdate("Y", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('encryption');
			$this->load->library('fpdf');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_bayar');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_rekening');
			$this->load->model('m_status_awal');
			$this->load->model('m_virtual');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_pmb_period()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('new_filter_period',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('new_filter_period','');
			}
			redirect("payment_form");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('new_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('new_filter_jur','');
			}
			redirect("payment_form");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('new_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('new_filter_prodi','');
			}
			redirect("payment_form");
		}
		
		function ptl_filter_no()
		{
			$this->authentification();
			$cekno = $this->input->post('cekno');
			if($cekno != "")
			{
				$this->session->set_userdata('new_filter_no',$cekno);
			}
			else
			{
				$this->session->unset_userdata('new_filter_no');
			}
			redirect("payment_form");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','form');
			if($this->session->userdata('new_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('new_filter_no');
			}
			$uri_segment = 3;
			$offset = $this->uri->segment($uri_segment);
			$data['cariproduk'] = $this->m_aplikan->PTL_all_cari_all($cekno,$offset);
			$jml = $this->m_aplikan->PTL_all_cari_jumlah_all();
			$data['cek'] = $this->m_aplikan->PTL_all_cari_cek_all();
			$data['total'] = count($this->m_aplikan->PTL_all_cari_total_all());
			$this->load->library('pagination');
			$config['base_url'] = site_url("payment_form/index");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "payment_form/index";
			
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$res = $this->m_aplikan->PTL_all_totcostbi();
			$data['totcost'] = $res['totcost'];
			$data['pencarian'] = '';
			$this->load->view('Portal/v_header');
			$this->load->view('Payment_Form/v_payment_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function search($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','form');
			if($this->session->userdata('new_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('new_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_aplikan->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_aplikan->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_aplikan->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_aplikan->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("students_payment/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students_payment/search/$cari";
			
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$res = $this->m_aplikan->PTL_all_totcostbi();
			$data['totcost'] = $res['totcost'];
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header');
			$this->load->view('Payment_Form/v_payment_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf_report()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$cekperiode = $this->session->userdata('new_filter_period');
			$cekjurusan = $this->session->userdata('new_filter_jur');
			$cekprodi = $this->session->userdata('new_filter_prodi');
			$result = $this->m_aplikan->PTL_all_spesifik($cekperiode,$cekjurusan,$cekprodi);
			$ProdiID = $cekprodi;
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"REPORT PAYMENT FORM",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "FILTER", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$cekperiode.$cekjurusan.$cekprodi, 0, "", "L");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.8 , 0.7, "#" , "LBTR", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "PMBID" , "BTR", 0, "C");
			$this->fpdf->Cell(8 , 0.7, "NAME" , "BTR", 0, "C");
			$this->fpdf->Cell(5.2 , 0.7, "PROGRAM" , "BTR", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$pay = 0;
			if($result)
			{
				foreach($result as $r)
				{
					if($no == $tot)
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LBR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->PMBID, "BR", 0, "C");
						$this->fpdf->Cell(8 , 0.5, $r->Nama, "BR", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "BR", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Harga), "BR", 0, "R");
						$pay = $pay + $r->Harga;
					}
					else
					{
						$this->fpdf->SetFont("Times","",10);
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.8 , 0.5, $no, "LR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $r->PMBID, "R", 0, "C");
						$this->fpdf->Cell(8 , 0.5, $r->Nama, "R", 0, "L");
						$this->fpdf->Cell(5.2 , 0.5, $r->ProgramID." - ".$r->ProdiID, "R", 0, "L");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Harga), "R", 0, "R");
						$pay = $pay + $r->Harga;
					}
					$no++;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "TOTAL PAYMENT", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($pay), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Output("REPORT_STUDENT_PAYMENT".$cekperiode.$cekjurusan.$cekprodi.".pdf","I");
		}
	}
?>