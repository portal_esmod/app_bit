<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class android_getData extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			// $is_login_finance = $_COOKIE["is_login_finance"];
			// if ($is_login_finance!=='logged')
			// {
				// $this->session->set_userdata('is_login_finance','notlogged');
				// redirect('login');
			// }
			// if($_COOKIE["id_akun"] == "")
			// {
				// $this->session->set_userdata('is_login_finance','notlogged');
				// redirect('login');
			// }
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->LENDRA_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/dra_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$rowrecord = $this->m_mahasiswa->LENDRA_all();
			$csv_output = "";
			$no = 1;
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					if($row->Nama != "")
					{
						if($no <= 10)
						{
							if($no == 10)
							{
								$csv_output .= "$row->MhswID";
							}
							else
							{
								$csv_output .= "$row->MhswID";
								$csv_output .= "\n";
							}
							$no++;
						}
					}
				}
			}
			print $csv_output;
			exit;
		}
	}
?>