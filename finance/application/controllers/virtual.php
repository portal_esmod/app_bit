<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Virtual extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_finance = $_COOKIE["is_login_finance"];
			if ($is_login_finance!=='logged')
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_finance','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->sesi = gmdate("YmdHis", time()-($ms));
			$this->nim = gmdate("Yis", time()-($ms));
			$this->kd_bayar = gmdate("Y", time()-($ms));
			$this->load->helper('finance');
			$this->load->library('encryption');
			$this->load->library('log');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_bayar');
			$this->load->model('m_khs');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_prodi');
			$this->load->model('m_rekening');
			$this->load->model('m_status_awal');
			$this->load->model('m_virtual');
		}
		
		function authentification()
		{
			$is_login = "is_login_finance";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE ...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$data['rowrecord'] = $this->m_virtual->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Virtual/v_virtual',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		// function ptl_import_exe()// KHS NEW
		// {
			// $this->authentification();
			// $download = $_FILES['userfile']['name'];
			// $downloadin = str_replace(' ','_',$download);
			// $config['upload_path'] = './ptl_storage/temp/';
			// $config['allowed_types'] = '*';
			// $config['max_size']	= 10000;
			// $this->load->library('upload',$config);
			// if(! $this->upload->do_upload())
			// {
				// $pesan = $this->upload->display_errors();
				// echo warning("Sorry, ".$pesan,"../virtual");
			// }
			// else
			// {
				// $data = array('error' => false);
				// $upload_data = $this->upload->data();
				// $this->load->library('excel_reader');
				// $this->excel_reader->setOutputEncoding('CP1251');
				// $file =  $upload_data['full_path'];
				// $this->excel_reader->read($file);
				// error_reporting(E_ALL ^ E_NOTICE);
				// $data = $this->excel_reader->sheets[0] ;
				// $dataarray = Array();
				// $totdup = 0;
				// for($i = 1; $i <= $data['numRows']; $i++)
				// {
					// if($data['cells'][$i][1] == '') break;
					// {
						// $MhswID = $data['cells'][$i][1];
						// $TahunKe = $data['cells'][$i][2];
						// $ProgramID = $data['cells'][$i][3];
						// $KelasID = $data['cells'][$i][4];
						// $WaliKelasID = $data['cells'][$i][5];
						// $WaliKelasID2 = $data['cells'][$i][6];
						// $SpesialisasiID = $data['cells'][$i][7];
						// $duplicate = $this->m_virtual->PTL_select_khs($MhswID,$TahunKe,$ProgramID,$KelasID,$WaliKelasID,$WaliKelasID2,$SpesialisasiID);
						// if($duplicate)
						// {
							// $totdup++;
						// }
						// else
						// {
							// $dataarray[$i-1]['MhswID'] = $data['cells'][$i][1];
							// $dataarray[$i-1]['TahunKe'] = $data['cells'][$i][2];
							// $dataarray[$i-1]['ProgramID'] = $data['cells'][$i][3];
							// $dataarray[$i-1]['KelasID'] = $data['cells'][$i][4];
							// $dataarray[$i-1]['WaliKelasID'] = $data['cells'][$i][5];
							// $dataarray[$i-1]['WaliKelasID2'] = $data['cells'][$i][6];
							// $dataarray[$i-1]['SpesialisasiID'] = $data['cells'][$i][7];
							// $dataarray[$i-1]['ProdiID'] = $data['cells'][$i][8];
							
							// if($data['cells'][$i][3] == "INT")
							// {
								// $dataarray[$i-1]['TahunID'] = 37;
								// $dataarray[$i-1]['BIPOTID'] = 118;
							// }
							// else
							// {
								// $dataarray[$i-1]['TahunID'] = 35;
								// $dataarray[$i-1]['BIPOTID'] = 121;
							// }
							// $dataarray[$i-1]['KodeID'] = "ESMOD";
							// $dataarray[$i-1]['StatusMhswID'] = "A";
							// if($data['cells'][$i][2] == 1)
							// {
								// $dataarray[$i-1]['Sesi'] = 1;
							// }
							// if($data['cells'][$i][2] == 2)
							// {
								// $dataarray[$i-1]['Sesi'] = 3;
							// }
							// if($data['cells'][$i][2] == 3)
							// {
								// $dataarray[$i-1]['Sesi'] = 5;
							// }
							// $dataarray[$i-1]['MaxSKS'] = 27;
							// $dataarray[$i-1]['login_buat'] = "ADMIN_IMPORT_NEW";
							// $dataarray[$i-1]['tanggal_buat'] = $this->waktu;
						// }
					// }
				// }
				// unlink("./ptl_storage/temp/".$downloadin);
				// $this->m_virtual->PTL_import_exe_khs($dataarray);
				// $total = $i - 2 - $totdup;
			// }
			// echo warning("Successfully import total '$total' Virtual Account to database. '$totdup' data duplicate can not entry to database.","../virtual");
		// }
		
		// function ptl_import_exe()// MHSW
		// {
			// $this->authentification();
			// $download = $_FILES['userfile']['name'];
			// $downloadin = str_replace(' ','_',$download);
			// $config['upload_path'] = './ptl_storage/temp/';
			// $config['allowed_types'] = '*';
			// $config['max_size']	= 10000;
			// $this->load->library('upload',$config);
			// if(! $this->upload->do_upload())
			// {
				// $pesan = $this->upload->display_errors();
				// echo warning("Sorry, ".$pesan,"../virtual");
			// }
			// else
			// {
				// $data = array('error' => false);
				// $upload_data = $this->upload->data();
				// $this->load->library('excel_reader');
				// $this->excel_reader->setOutputEncoding('CP1251');
				// $file =  $upload_data['full_path'];
				// $this->excel_reader->read($file);
				// error_reporting(E_ALL ^ E_NOTICE);
				// $data = $this->excel_reader->sheets[0] ;
				// $dataarray = Array();
				// $totdup = 0;
				// for($i = 1; $i <= $data['numRows']; $i++)
				// {
					// if($data['cells'][$i][1] == '') break;
					// {
						// $MhswID = $data['cells'][$i][1];
						// $PMBID = $data['cells'][$i][3];
						// $TahunID = $data['cells'][$i][4];
						// $BIPOTID = $data['cells'][$i][5];
						// $Nama = $data['cells'][$i][6];
						// $ProgramID = $data['cells'][$i][9];
						// $ProdiID = $data['cells'][$i][10];
						// $KelasID = $data['cells'][$i][11];
						// $duplicate = $this->m_virtual->PTL_select_mhsw($MhswID,$PMBID,$TahunID,$BIPOTID,$Nama,$ProgramID,$ProdiID,$KelasID);
						// if($duplicate)
						// {
							// $totdup++;
						// }
						// else
						// {
							// $dataarray[$i-1]['MhswID'] = $data['cells'][$i][1];
							// $dataarray[$i-1]['Password'] = $data['cells'][$i][2];
							// $dataarray[$i-1]['PMBID'] = $data['cells'][$i][3];
							// $dataarray[$i-1]['TahunID'] = $data['cells'][$i][4];
							// $dataarray[$i-1]['BIPOTID'] = $data['cells'][$i][5];
							// $dataarray[$i-1]['Nama'] = $data['cells'][$i][6];
							// $dataarray[$i-1]['StatusAwalID'] = $data['cells'][$i][7];
							// $dataarray[$i-1]['StatusMhswID'] = $data['cells'][$i][8];
							// $dataarray[$i-1]['ProgramID'] = $data['cells'][$i][9];
							// $dataarray[$i-1]['ProdiID'] = $data['cells'][$i][10];
							// $dataarray[$i-1]['KelasID'] = $data['cells'][$i][11];
							// $dataarray[$i-1]['WargaNegara'] = $data['cells'][$i][12];
							// $dataarray[$i-1]['Kebangsaan'] = $data['cells'][$i][13];
							// $dataarray[$i-1]['Pilihan1'] = $data['cells'][$i][14];
							// $dataarray[$i-1]['BatasStudi'] = $data['cells'][$i][15];
							
							// $dataarray[$i-1]['KodeID'] = "ESMOD";
							// $dataarray[$i-1]['login_buat'] = "ADMIN_IMPORT_NEW";
							// $dataarray[$i-1]['tanggal_buat'] = $this->waktu;
						// }
					// }
				// }
				// unlink("./ptl_storage/temp/".$downloadin);
				// $this->m_virtual->PTL_import_exe_mhsw($dataarray);
				// $total = $i - 2 - $totdup;
			// }
			// echo warning("Successfully import total '$total' Virtual Account to database. '$totdup' data duplicate can not entry to database.","../virtual");
		// }
		
		// function ptl_import_exe()// KHS EXIS
		// {
			// $this->authentification();
			// $download = $_FILES['userfile']['name'];
			// $downloadin = str_replace(' ','_',$download);
			// $config['upload_path'] = './ptl_storage/temp/';
			// $config['allowed_types'] = '*';
			// $config['max_size']	= 10000;
			// $this->load->library('upload',$config);
			// if(! $this->upload->do_upload())
			// {
				// $pesan = $this->upload->display_errors();
				// echo warning("Sorry, ".$pesan,"../virtual");
			// }
			// else
			// {
				// $data = array('error' => false);
				// $upload_data = $this->upload->data();
				// $this->load->library('excel_reader');
				// $this->excel_reader->setOutputEncoding('CP1251');
				// $file =  $upload_data['full_path'];
				// $this->excel_reader->read($file);
				// error_reporting(E_ALL ^ E_NOTICE);
				// $data = $this->excel_reader->sheets[0] ;
				// $dataarray = Array();
				// $totdup = 0;
				// for($i = 1; $i <= $data['numRows']; $i++)
				// {
					// if($data['cells'][$i][1] == '') break;
					// {
						// $MhswID = $data['cells'][$i][1];
						// $TahunKe = $data['cells'][$i][2];
						// $ProgramID = $data['cells'][$i][3];
						// $KelasID = $data['cells'][$i][4];
						// $WaliKelasID = $data['cells'][$i][5];
						// $WaliKelasID2 = $data['cells'][$i][6];
						// $SpesialisasiID = $data['cells'][$i][7];
						// $duplicate = $this->m_virtual->PTL_select_khs($MhswID,$TahunKe,$ProgramID,$KelasID,$WaliKelasID,$WaliKelasID2,$SpesialisasiID);
						// if($duplicate)
						// {
							// $totdup++;
						// }
						// else
						// {
							// $dataarray[$i-1]['MhswID'] = $data['cells'][$i][1];
							// $dataarray[$i-1]['TahunKe'] = $data['cells'][$i][2];
							// $dataarray[$i-1]['ProgramID'] = $data['cells'][$i][3];
							// $dataarray[$i-1]['KelasID'] = $data['cells'][$i][4];
							// $dataarray[$i-1]['WaliKelasID'] = $data['cells'][$i][5];
							// $dataarray[$i-1]['WaliKelasID2'] = $data['cells'][$i][6];
							// $dataarray[$i-1]['SpesialisasiID'] = $data['cells'][$i][7];
							
							// $dataarray[$i-1]['TahunID'] = 35;
							// $dataarray[$i-1]['KodeID'] = "ESMOD";
							// $dataarray[$i-1]['ProdiID'] = "COMD3";
							// $dataarray[$i-1]['StatusMhswID'] = "A";
							// if($data['cells'][$i][2] == 1)
							// {
								// $dataarray[$i-1]['Sesi'] = 1;
							// }
							// if($data['cells'][$i][2] == 2)
							// {
								// $dataarray[$i-1]['Sesi'] = 3;
							// }
							// if($data['cells'][$i][2] == 3)
							// {
								// $dataarray[$i-1]['Sesi'] = 5;
							// }
							// $dataarray[$i-1]['BIPOTID'] = 121;
							// $dataarray[$i-1]['MaxSKS'] = 27;
							// $dataarray[$i-1]['login_buat'] = "ADMIN_IMPORT_EXIS";
							// $dataarray[$i-1]['tanggal_buat'] = $this->waktu;
						// }
					// }
				// }
				// unlink("./ptl_storage/temp/".$downloadin);
				// $this->m_virtual->PTL_import_exe_khs($dataarray);
				// $total = $i - 2 - $totdup;
			// }
			// echo warning("Successfully import total '$total' Virtual Account to database. '$totdup' data duplicate can not entry to database.","../virtual");
		// }
		
		function ptl_import_exe()// DEFAULT
		{
			$this->authentification();
			$download = $_FILES['userfile']['name'];
			$downloadin = str_replace(' ','_',$download);
			$config['upload_path'] = './ptl_storage/temp/';
			$config['allowed_types'] = '*';
			$config['max_size']	= 10000;
			$this->load->library('upload',$config);
			if(! $this->upload->do_upload())
			{
				$pesan = $this->upload->display_errors();
				echo warning("Sorry, ".$pesan,"../virtual");
			}
			else
			{
				$data = array('error' => false);
				$upload_data = $this->upload->data();
				$this->load->library('excel_reader');
				$this->excel_reader->setOutputEncoding('CP1251');
				$file =  $upload_data['full_path'];
				$this->excel_reader->read($file);
				error_reporting(E_ALL ^ E_NOTICE);
				$data = $this->excel_reader->sheets[0] ;
				$dataarray = Array();
				$totdup = 0;
				for($i = 1; $i <= $data['numRows']; $i++)
				{
					if($data['cells'][$i][1] == '') break;
					{
						$tanggal_transfer = $data['cells'][$i][1];
						$id_virtual = $data['cells'][$i][2];
						$nama = $data['cells'][$i][3];
						$jurnal = $data['cells'][$i][4];
						$cabang = $data['cells'][$i][5];
						$jumlah_transfer = $data['cells'][$i][6];
						$dk = $data['cells'][$i][7];
						$keterangan1 = $data['cells'][$i][8];;
						$keterangan2 = $data['cells'][$i][9];
						$duplicate = $this->m_virtual->PTL_select($tanggal_transfer,$id_virtual,$nama,$jurnal,$cabang,$jumlah_transfer,$dk,$keterangan1,$keterangan2);
						if($duplicate)
						{
							$totdup++;
						}
						else
						{
							$dataarray[$i-1]['id_sesi'] = $this->sesi;
							$dataarray[$i-1]['tanggal_transfer'] = $data['cells'][$i][1];
							$dataarray[$i-1]['id_virtual'] = $data['cells'][$i][2];
							$dataarray[$i-1]['nama'] = $data['cells'][$i][3];
							$dataarray[$i-1]['jurnal'] = $data['cells'][$i][4];
							$dataarray[$i-1]['cabang'] = $data['cells'][$i][5];
							$dataarray[$i-1]['jumlah_transfer'] = $data['cells'][$i][6];
							$dataarray[$i-1]['dk'] = $data['cells'][$i][7];
							$dataarray[$i-1]['keterangan1'] = $data['cells'][$i][8];;
							$dataarray[$i-1]['keterangan2'] = $data['cells'][$i][9];
							$dataarray[$i-1]['login_buat'] = $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"];
							$dataarray[$i-1]['tanggal_buat'] = $this->waktu;
						}
					}
				}
				unlink("./ptl_storage/temp/".$downloadin);
				$this->m_virtual->PTL_import_exe($dataarray);
				$total = $i - 2 - $totdup;
			}
			echo warning("Successfully import total '$total' Virtual Account to database. '$totdup' data duplicate can not entry to database.","../virtual");
		}
		
		// function ptl_detail() // NEW STUDENTS
		// {
			// $this->authentification();
			// $this->session->set_userdata('menu','students_payment');
			// $id = $this->uri->segment(3);
			// $resva = $this->m_virtual->PTL_select_id($id);
			// $data['id'] = $id;
			// $data['jurnal'] = $resva['jurnal'];
			// $data['tanggal_transfer'] = $resva['tanggal_transfer'];
			// $data['keterangan1'] = $resva['keterangan1'];
			// $data['keterangan2'] = $resva['keterangan2'];
			// $data['jumlah_transfer'] = $resva['jumlah_transfer'];
			// $data['digunakan'] = $resva['digunakan'];
			// $virtual_account = $this->uri->segment(4);
			// $result = $this->m_aplikan->PTL_select_va($virtual_account);
			// $data['virtual_account'] = $virtual_account;
			// if(!$result)
			// {
				// $this->load->view('Portal/v_header');
				// $this->load->view('Virtual/v_virtual_payment_messages',$data);
				// $this->load->view('Portal/v_footer_table');
			// }
			// else
			// {
				// $data['AplikanID'] = $result['AplikanID'];
				// $data['Nama'] = $result['Nama'];
				// $PMBID = $result['PMBID'];
				// $mhsw = $this->m_master->PTL_bipot_mhsw_select($PMBID);
				// if($mhsw)
				// {
					// $no = 0;
					// foreach($mhsw as $m)
					// {
						// if($no < 1)
						// {
							// $BIPOTID = $m->BIPOTID;
						// }
						// $no++;
					// }
					// $data['BIPOTID'] = $BIPOTID;
					// $data['cekBIPOTID'] = 'YES';
					// $data['detail'] = $this->m_master->PTL_bipot_mhsw_select($PMBID);
				// }
				// $data['rowrekening'] = $this->m_rekening->PTL_all_active();
				// $this->load->view('Portal/v_header');
				// $this->load->view('Virtual/v_virtual_payment_form',$data);
				// $this->load->view('Portal/v_footer_table');
			// }
		// }
		
		function ptl_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students_payment');
			$id = $this->uri->segment(3);
			$resva = $this->m_virtual->PTL_select_id($id);
			$data['id'] = $id;
			$data['jurnal'] = $resva['jurnal'];
			$data['tanggal_transfer'] = $resva['tanggal_transfer'];
			$data['keterangan1'] = $resva['keterangan1'];
			$data['keterangan2'] = $resva['keterangan2'];
			$data['jumlah_transfer'] = $resva['jumlah_transfer'];
			$data['digunakan'] = $resva['digunakan'];
			$virtual_account = $this->uri->segment(4);
			$result = $this->m_mahasiswa->PTL_select_va($virtual_account);
			$data['virtual_account'] = $virtual_account;
			if(!$result)
			{
				$this->load->view('Portal/v_header');
				$this->load->view('Virtual/v_virtual_payment_messages',$data);
				$this->load->view('Portal/v_footer_table');
			}
			else
			{
				$data['MhswID'] = $result['MhswID'];
				$data['Nama'] = $result['Nama'];
				$MhswID = $result['MhswID'];
				$reskhs = $this->m_khs->PTL_select_max($MhswID);
				$data['KHSID'] = '';
				$data['TahunID'] = '';
				$TahunID = '';
				if($reskhs)
				{
					$data['KHSID'] = $reskhs['KHSID'];
					$data['TahunID'] = $reskhs['TahunID'];
					$TahunID = $reskhs['TahunID'];
				}
				$mhsw = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
				if($mhsw)
				{
					$no = 0;
					foreach($mhsw as $m)
					{
						if($no < 1)
						{
							$BIPOTID = $m->BIPOTID;
						}
						$no++;
					}
					$data['BIPOTID'] = $BIPOTID;
					$data['cekBIPOTID'] = 'YES';
					$data['detail'] = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
				}
				$data['rowrekening'] = $this->m_rekening->PTL_all_active();
				$this->load->view('Portal/v_header');
				$this->load->view('Virtual/v_virtual_students_payment_form',$data);
				$this->load->view('Portal/v_footer_table');
			}
		}
		
		// function ptl_payment_insert()
		// {
			// $this->authentification();
			// $id = $this->input->post('id');
			// $virtual_account = $this->input->post('virtual_account');
			// $AplikanID = $this->input->post('AplikanID');
			// $total = $this->input->post('total');
			// $pesan = "";
			// $resva = $this->m_virtual->PTL_select_id($id);
			// $tot_payment = 0;
			// for($i=1;$i<=$total;$i++)
			// {
				// $nama = $this->input->post("nama$i");
				// $besar = $this->input->post("besar$i");
				// $dibayar = $this->input->post("dibayar$i");
				// $payment = $this->input->post("payment$i");
				// $sisa = $besar - $dibayar;
				// if($payment > $sisa)
				// {
					// $pesan .= "Payment for '$nama' beyond the limit.".'\n'."Balance ".formatRupiah2($sisa)." - Payment ".formatRupiah2($payment).'\n\n';
				// }
				// if($payment < 0)
				// {
					// $pesan .= "Payment for '$nama' not allowed minus value.".'\n'."Payment -".formatRupiah2($payment).'\n\n';
				// }
				// $tot_payment = $tot_payment + $payment;
			// }
			// $va_trans = $this->input->post('jumlah_transfer') - $resva['digunakan'];
			// if($tot_payment > $va_trans)
			// {
				// echo warning("Payment exceeds the limit of Virtual Account.".'\n\n'."Transfer Via Virtual Account ".formatRupiah2($va_trans)." - Total Payment ".formatRupiah2($tot_payment),"../virtual/ptl_detail/$id/$virtual_account");
			// }
			// if($pesan != "")
			// {
				// echo warning($pesan,"../virtual/ptl_detail/$id/$virtual_account");
			// }
			// else
			// {
				// $kd = $this->kd_bayar."-";
				// $result = $this->m_bayar->PTL_urut_bayar($kd);
				// $lastid = $result['LAST'];
				
				// $lastnourut = substr($lastid,5,5);
				// $nextnourut = $lastnourut + 1;
				// $nextid = $kd.sprintf('%05s',$nextnourut);
				// $res = $this->m_aplikan->PTL_select($AplikanID);
				// $Jumlah = 0;
				// for($i2=1;$i2<=$total;$i2++)
				// {
					// $BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					// $resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					// if($this->input->post("payment$i2") > 0)
					// {
						// $data = array(
									// 'Dibayar' => $resbipot['Dibayar'] + $this->input->post("payment$i2"),
									// 'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									// 'tanggal_edit' => $this->waktu
									// );
						// $this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						// $Jumlah = $Jumlah + $this->input->post("payment$i2");
					
						// $data_bayar2 = array(
									// 'BayarMhswID' => $nextid,
									// 'BIPOTMhswID' => $BIPOTMhswID,
									// 'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									// 'Jumlah' => $this->input->post("payment$i2"),
									// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									// 'tanggal_buat' => $this->waktu
									// );
						// $this->m_bayar->PTL_insert2($data_bayar2);
						// $BIPOTID = $this->input->post("BIPOTID$i2");
					// }
				// }
				// if($Jumlah == 0)
				// {
					// echo warning("Sorry! You do not input a nominal payment.","../virtual/ptl_detail/$id/$virtual_account");
				// }
				// else
				// {
					// $data_va = array(
									// 'digunakan' => $resva['digunakan'] + $Jumlah,
									// );
					// $this->m_virtual->PTL_update($id,$data_va);
					// $data_bayar = array(
									// 'BayarMhswID' => $nextid,
									// 'BIPOTID' => $BIPOTID,
									// 'TahunID' => $res['PMBPeriodID'],
									// 'RekeningID' => $this->input->post('RekeningID'),
									// 'PMBID' => $res['PMBID'],
									// 'MhswID' => $res['MhswID'],
									// 'TrxID' => 1,
									// 'Bank' => strtoupper($this->input->post('Bank')),
									// 'BuktiSetoran' => $this->input->post('BuktiSetoran'),
									// 'Tanggal' => $this->input->post('Tanggal'),
									// 'Jumlah' => $Jumlah,
									// 'Keterangan' => $this->input->post('Keterangan'),
									// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									// 'tanggal_buat' => $this->waktu
									// );
					// $this->m_bayar->PTL_insert($data_bayar);
					// $key1 = $this->encryption->encode($nextid);
					// $key2 = $this->encryption->encode($AplikanID);
					// $key3 = $this->encryption->encode($this->waktu);
					// $this->load->library('email');
					// $config = array();
					// $config['charset'] = 'utf-8';
					// $config['useragent'] = 'Codeigniter';
					// $config['protocol']= "smtp";
					// $config['mailtype']= "html";
					// $config['smtp_host']= "mail.esmodjakarta.com";
					// $config['smtp_port']= "25";
					// $config['smtp_timeout']= "5";
					// $config['smtp_user']= "no-reply@esmodjakarta.com";
					// $config['smtp_pass']= "noreplyesmod";
					// $config['crlf']="\r\n"; 
					// $config['newline']="\r\n"; 
					// $config['wordwrap'] = TRUE;
					// $this->email->initialize($config);
					// $this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					// $this->email->to('lendra.permana@gmail.com');
					// $this->email->subject('YOUR PAYMENT DATA HAS BEEN INPUTTED BY FINANCE');
					// $this->email->message("
					// <h1>YOUR PAYMENT DATA HAS BEEN INPUTTED BY FINANCE</h1>
					// <br/>
					// <br/>
					// Dear '<b>".$res['Nama']." - ".$res['ProgramID']." - ".$res['ProdiID']."</b>', your payment data has been inputted. Please check your LMS or contact Finance Team.
					// <br/>
					// <br/>
					// <br/>
					// <br/>
					// DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>PAYMENT $res[Nama]</a>
					// <br/>
					// <br/>
					// For more information, please login to <a href='http://lms.esmodjakarta.com'>LMS (Learning Management System)</a>
					// <br/>
					// You need to login first.
					// <br/>
					// <br/>
					// <br/>
					// Thanks,
					// <br/>
					// <img src='http://sysfo.esmodjakarta.com/esmod/forgot/assets/login/img/logo.png'/ height='20px' width='120px'>
					// ");
					// if($this->email->send())
					// {
						// echo warning("Payment data successfully saved.","../virtual/ptl_detail/$id/$virtual_account");
					// }
					// else
					// {
						// echo warning("Email server is not active. Payment data successfully saved.","../virtual/ptl_detail/$id/$virtual_account");
					// }
				// }
			// }
		// }
		
		function ptl_students_payment_insert()
		{
			$this->authentification();
			$id = $this->input->post('id');
			$virtual_account = $this->input->post('virtual_account');
			$KHSID = $this->input->post('KHSID');
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$total = $this->input->post('total');
			$pesan = "";
			$resva = $this->m_virtual->PTL_select_id($id);
			$tot_payment = 0;
			for($i=1;$i<=$total;$i++)
			{
				$nama = $this->input->post("nama$i");
				$besar = $this->input->post("besar$i");
				$dibayar = $this->input->post("dibayar$i");
				$payment = $this->input->post("payment$i");
				$sisa = $besar - $dibayar;
				if($payment > $sisa)
				{
					$pesan .= "Payment for '$nama' beyond the limit.".'\n'."Balance ".formatRupiah2($sisa)." - Payment ".formatRupiah2($payment).'\n\n';
				}
				if($payment < 0)
				{
					$pesan .= "Payment for '$nama' not allowed minus value.".'\n'."Payment -".formatRupiah2($payment).'\n\n';
				}
				$tot_payment = $tot_payment + $payment;
			}
			$va_trans = $this->input->post('jumlah_transfer') - $resva['digunakan'];
			if($tot_payment > $va_trans)
			{
				echo warning("Payment exceeds the limit of Virtual Account.".'\n\n'."Transfer Via Virtual Account ".formatRupiah2($va_trans)." - Total Payment ".formatRupiah2($tot_payment),"../virtual/ptl_detail/$id/$virtual_account");
			}
			if($pesan != "")
			{
				echo warning($pesan,"../virtual/ptl_detail/$id/$virtual_account");
			}
			else
			{
				$kd = $this->kd_bayar."-";
				$result = $this->m_bayar->PTL_urut_bayar($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,5,5);
				$nextnourut = $lastnourut + 1;
				$nextid = $kd.sprintf('%05s',$nextnourut);
				$res = $this->m_mahasiswa->PTL_select($MhswID);
				$Jumlah = 0;
				for($i2=1;$i2<=$total;$i2++)
				{
					$BIPOTMhswID = $this->input->post("bipotmhsw$i2");
					$resbipot = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($this->input->post("payment$i2") > 0)
					{
						$data = array(
									'Dibayar' => $resbipot['Dibayar'] + $this->input->post("payment$i2"),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_master->PTL_bipotmhsw_update($BIPOTMhswID,$data);
						$Jumlah = $Jumlah + $this->input->post("payment$i2");
					
						$data_bayar2 = array(
									'BayarMhswID' => $nextid,
									'BIPOTMhswID' => $BIPOTMhswID,
									'BIPOTNamaID' => $this->input->post("bipotnama$i2"),
									'Jumlah' => $this->input->post("payment$i2"),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_bayar->PTL_insert2($data_bayar2);
						$BIPOTID = $this->input->post("BIPOTID$i2");
					}
				}
				if($Jumlah == 0)
				{
					echo warning("Sorry! You do not input a nominal payment.","../virtual/ptl_detail/$id/$virtual_account");
				}
				else
				{
					$pesan_akhir = "";
					$data_va = array(
									'digunakan' => $resva['digunakan'] + $Jumlah,
									);
					$this->m_virtual->PTL_update($id,$data_va);
					$data_bayar = array(
									'BayarMhswID' => $nextid,
									'BIPOTID' => $BIPOTID,
									'TahunID' => $TahunID,
									'RekeningID' => $this->input->post('RekeningID'),
									'MhswID' => $MhswID,
									'TrxID' => 1,
									'Bank' => strtoupper($this->input->post('Bank')),
									'BuktiSetoran' => $this->input->post('BuktiSetoran'),
									'Tanggal' => $this->input->post('Tanggal'),
									'Jumlah' => $Jumlah,
									'Keterangan' => $this->input->post('Keterangan'),
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_bayar->PTL_insert($data_bayar);
					$res_khs = $this->m_khs->PTL_select($KHSID);
					$Bayar = $Jumlah;
					if($res_khs)
					{
						$Bayar = $res_khs['Bayar'] + $Jumlah;
					}
					$data = array(
								'Bayar' => $Bayar,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$res_tutup = $this->m_khs->PTL_select($KHSID);
					if($res_tutup)
					{
						$hasil = $res_tutup["Biaya"] - $res_tutup["Potongan"] - $res_tutup["Bayar"];
						if($hasil == 0)
						{
							$pesan_akhir .= '\n\nPAYMENT IS COMPLETE.';
							$data = array(
										'Tutup' => 'Y',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_khs->PTL_update($KHSID,$data);
						}
						if($hasil < 0)
						{
							$pesan_akhir .= '\n\nBILL PAYMENT EXCEED.';
						}
					}
					$key1 = $this->encryption->encode($nextid);
					$key2 = $this->encryption->encode($MhswID);
					$key3 = $this->encryption->encode($this->waktu);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($res['Email']);
					$this->email->cc($res['Email2']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR PAYMENT DATA HAS BEEN INPUTTED BY FINANCE (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>FINANCE</h2></font>
						</center>
						<br/>
						Dear '<b>$res[Nama] - $res[ProgramID] - $res[ProdiID]</b>', your payment data has been inputted. Please check your PORTAL LMS or contact Finance Team.
						<br/>
						<br/>
						<b>(PAYMENT VIA VIRTUAL ACCOUNT)</b></b>
						<br/>
						<br/>
						<br/>
						<br/>
						DOWNLOAD ATTACHMENT : <a href='http://app.esmodjakarta.com/finance/general/student_key_payment/$key1/$key2/$key3/ESMOD%20JAKARTA'>VIRTUAL ACCOUNT PAYMENT $nextid</a>
						<br/>
						<br/>
						For more information, please login to <a href='http://lms.esmodjakarta.com'>PORTAL LMS (Learning Management System)</a>
						<br/>
						You need to login first.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Payment data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.".$pesan_akhir,"../virtual/ptl_detail/$id/$virtual_account");
					}
					else
					{
						echo warning("Email server is not active. Payment data with ID '$nextid', SIN '$MhswID - $res[Nama]' successfully saved.".$pesan_akhir,"../virtual/ptl_detail/$id/$virtual_account");
					}
				}
			}
		}
	}
?>