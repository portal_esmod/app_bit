<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Charts extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function index()
		{
			$this->session->set_userdata('menu','charts');
			$this->load->view('Portal/v_header');
			$this->load->view('Charts/v_students');
			$this->load->view('Portal/v_footer_charts');
		}
		
		function ptl_new_students()
		{
			$this->session->set_userdata('menu','charts');
			$this->load->view('Portal/v_header');
			$this->load->view('Charts/v_new');
			$this->load->view('Portal/v_footer_charts');
		}
		
		function ptl_corrections_payment()
		{
			$this->session->set_userdata('menu','charts');
			$this->load->view('Portal/v_header');
			$this->load->view('Charts/v_corrections');
			$this->load->view('Portal/v_footer_charts');
		}
	}
?>