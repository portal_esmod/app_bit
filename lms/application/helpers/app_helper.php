<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function from_session($key)
	{
		$CI =& get_instance();
		$ss = $CI->session->userdata($key);
		return $ss;
	}

	function warning($input,$goTo)
	{
		$url = site_url().'/'.$goTo;
		$output="<script> 
			alert(\"$input\");
			location = '$url';
			</script>";
		return $output;
	}
	
	function file_exists_remote($url)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		$result = curl_exec($curl);
		$ret = false;
		if ($result !== false)
		{
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			if($statusCode == 200)
			{
				$ret = true;
			}
		}
		curl_close($curl);
		return $ret;
	}
	
	function fsize($file)
	{
		$a = array("B", "KB", "MB", "GB","TB", "PB");
		$pos = 0;
		$size = filesize($file);
		while ($size >= 1024)
		{
			$size /= 1024;
			$pos++; 
		}
		return round($size,2)." ".$a[$pos];
	}
	
	function compress_image($source_url,$destination_url,$quality)
	{
		$info = getimagesize($source_url);
		
		if($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
		elseif($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
		elseif($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
		
		imagejpeg($image,$destination_url,$quality);
		return $destination_url;
	}
	
	function hapus_simbol($result)
	{
		$result = strtolower($result);
		$result = preg_replace('/&amp;.+?;/', '', $result);
		$result = preg_replace('/\s+/', '', $result);
		$result = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '+', $result);
		// $result = preg_replace('|-+|', '', $result);
		$result = preg_replace('/&amp;#?[a-z0-9]+;/i','',$result);
		$result = preg_replace('/[^%A-Za-z0-9 _-]/', '', $result);
		$result = str_replace('%','',$result);
		$result = trim($result, '');
		return $result;
	}
?>