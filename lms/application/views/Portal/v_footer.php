		</div>
		<footer>
			&copy; 2016 Esmod Jakarta | By : <a href="http://esmodjakarta.com/" target="_blank">Vendor</a>
		</footer>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.11.1.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.metisMenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/custom.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/zebra_datepicker.js"></script>
		<script type="text/javascript">
			$('#datepicker1').Zebra_DatePicker();
			$('#OnDate1').Zebra_DatePicker({
				show_week_number: 'Wk',
				format: 'Y-m-d',
				onSelect : function (dateText, inst) {
					$('#formId').submit();
				}
			});
		</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>