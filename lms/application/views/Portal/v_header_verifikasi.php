<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Esmod Jakarta - LMS (Learning Management System)</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap.css" rel="stylesheet"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.css" rel="stylesheet"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet"/>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/default.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/style.css" type="text/css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/shCoreDefault.css">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/XRegExp.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shCore.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shLegacy.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushJScript.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushXML.js"></script>
		
        <style>
            /****** Rating Starts *****/
            @import url(<?php echo base_url("assets/rating/font-awesome.css"); ?>);
            .rating
			{
                border: none;
                float: left;
            }
            .rating > input { display: none; }
            .rating > label:before
			{
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }
            .rating > .half:before
			{
                content: "\f089";
                position: absolute;
            }
            .rating > label
			{
                color: #ddd; 
                float: right; 
            }
            .rating > input:checked ~ label,
            .rating:not(:checked) > label:hover,
            .rating:not(:checked) > label:hover ~ label { color: #FFD700; }
            .rating > input:checked + label:hover,
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label,
            .rating > input:checked ~ label:hover ~ label { color: #FFED85; }
            /* Downloaded from http://devzone.co.in/ */
        </style>
        <script src="<?php echo base_url("assets/rating/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/rating/ca-pub-2074772727795809.js"); ?>" type="text/javascript" async=""></script>
		<script src="<?php echo base_url("assets/rating/analytics.js"); ?>" async=""></script>
		
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116286915-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-116286915-1');
		</script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a  class="navbar-brand" href="http://esmodjakarta.com/"><img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" width="155px"/></a>
				</div>
				<div class="notifications-wrapper">
					<ul class="nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-user-plus"></i>  <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="<?php echo site_url("login/ptl_logout"); ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<nav  class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php $menu = $this->session->userdata('menu');?>
						<li>
							<div class="user-img-div">
								<?php
									$foto = "ptl_storage/foto_umum/user.jpg";
									$foto_preview = "ptl_storage/foto_umum/user.jpg";
									if(@$_COOKIE["foto"] != "")
									{
										$foto = "../academic/ptl_storage/foto_mahasiswa/$_COOKIE[foto]";
										$exist = file_exists_remote(base_url("$foto"));
										if($exist)
										{
											$foto = $foto;
											$source_photo = base_url("$foto");
											$info = pathinfo($source_photo);
											$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
											$exist1 = file_exists_remote(base_url("$foto_preview"));
											if($exist1)
											{
												$foto_preview = $foto_preview;
											}
											else
											{
												$foto_preview = $foto;
											}
										}
										else
										{
											$foto = "ptl_storage/foto_umum/user.jpg";
											$foto_preview = "ptl_storage/foto_umum/user.jpg";
										}
									}
								?>
								<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
									<img class="img-polaroid" src="<?php echo base_url("$foto_preview"); ?>" width="120px" alt=""/>
								</a>
								<font color="red"><h4><?php echo $_COOKIE["nim"]; ?><br/></font><font color="white"><?php echo $_COOKIE["nama"]; ?></h4></font>
							</div>
						</li>
					</ul>
				</div>
			</nav>