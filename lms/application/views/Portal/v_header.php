<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Esmod Jakarta - LMS (Learning Management System)</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap.css" rel="stylesheet"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.css" rel="stylesheet"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet"/>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/default.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/style.css" type="text/css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/shCoreDefault.css">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/XRegExp.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shCore.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shLegacy.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushJScript.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushXML.js"></script>
		
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116286915-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-116286915-1');
		</script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a  class="navbar-brand" href="http://esmodjakarta.com/"><img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" width="155px"/></a>
				</div>
				<div class="notifications-wrapper">
					<ul class="nav">
						<li class="dropdown">
							<?php
								$MhswID = $_COOKIE["nim"];
								$CI =& get_instance();
								$CI->load->model('m_notifikasi_lms');
								$rownotif = $CI->m_notifikasi_lms->PTL_all_active_sin($MhswID);
								$rownotifnon = $CI->m_notifikasi_lms->PTL_all_active_sin_non($MhswID);
								if(count($rownotifnon) == 0)
								{
									$TotalNotif = "";
									$Gambar = "";
								}
								else
								{
									$TotalNotif = count($rownotifnon);
									$Gambar = " <img src='".base_url("assets/dashboard/img/warning.gif")."' width='20px'/>";
								}
							?>
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
								<i class="fa fa-tasks fa-fw"></i><i class="fa fa-caret-down"> <font color="red" style="font-size:20px"><?php echo $TotalNotif.$Gambar; ?></font></i>
							</a>
							<ul class="dropdown-menu dropdown-notification">
								<?php
									if($rownotif)
									{
										$noUrut = 1;
										foreach($rownotif as $rntf)
										{
											if($noUrut <= 5)
											{
												$b1 = "<font color='grey'>";
												$b2 = "</font>";
												if($rntf->tanggal_baca == "")
												{
													$b1 = "<font color='red'>";
													$b2 = "</font>";
												}
												echo "
													<li>
														<a href='".site_url("academic/attendance_detail/$rntf->JadwalID/$rntf->KRSID/$rntf->id_notifikasi_lms/notif")."'>
															<div>
																$b1<strong>$rntf->Judul</strong><p>$rntf->Pesan<br/><i>$rntf->tanggal_buat</i></p>$b2
															</div>
														</a>
													</li>
													";
												$noUrut++;
											}
										}
										echo "
											<li>
												<a href='".site_url("notification")."'>
													<div>
														<strong>Show All</strong>
													</div>
												</a>
											</li>
											";
									}
									else
									{
										echo "
											<li>
												<a href='#'>
													<div>
														<strong>No data<p>No data available</p></strong>
													</div>
												</a>
											</li>
											";
									}
								?>
								<!--<li class="divider"></li>
								<li>
									<a class="text-center" href="#">
										<strong>See Notification List + </strong>
									</a>
								</li>-->
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-user-plus"></i>  <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="<?php echo site_url("personal"); ?>"><i class="fa fa-user-plus"></i> My Profile</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo site_url("login/ptl_password"); ?>"><i class="fa fa-cog"></i> Change Password</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo site_url("login/ptl_logout"); ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<nav  class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php $menu = $this->session->userdata('menu');?>
						<li>
							<div class="user-img-div">
								<?php
									$foto = "ptl_storage/foto_umum/user.jpg";
									$foto_preview = "ptl_storage/foto_umum/user.jpg";
									if(@$_COOKIE["foto"] != "")
									{
										$foto = "../academic/ptl_storage/foto_mahasiswa/$_COOKIE[foto]";
										$exist = file_exists_remote(base_url("$foto"));
										if($exist)
										{
											$foto = $foto;
											$source_photo = base_url("$foto");
											$info = pathinfo($source_photo);
											$foto_preview = "../academic/ptl_storage/foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
											$exist1 = file_exists_remote(base_url("$foto_preview"));
											if($exist1)
											{
												$foto_preview = $foto_preview;
											}
											else
											{
												$foto_preview = $foto;
											}
										}
										else
										{
											$foto = "ptl_storage/foto_umum/user.jpg";
											$foto_preview = "ptl_storage/foto_umum/user.jpg";
										}
									}
									$cekSC = substr($_COOKIE["nim"], 4, 4);
								?>
								<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
									<img class="img-polaroid" src="<?php echo base_url("$foto_preview"); ?>" width="120px" alt=""/>
								</a>
								<font color="red"><h4><?php echo $_COOKIE["nim"]; ?><br/></font><font color="white"><?php echo $_COOKIE["nama"]; ?></h4></font>
							</div>
						</li>
						<li>
							<a class="<?php if($menu == "home"){ echo "active-menu";} ?>" href="<?php echo site_url("login/ptl_home"); ?>"><i class="fa fa-dashboard "></i>Dashboard</a>
						</li>
						<li>
							<a class="<?php if($menu == "personal"){ echo "active-menu";} ?>" href="<?php echo site_url("personal"); ?>"><i class="fa fa-venus "></i>Personal Information</a>
						</li>
						<li>
							<a class="<?php if($menu == "academic"){ echo "active-menu";} ?>" href="#"><i class="fa fa-sitemap "></i>Academic<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<?php
									if($cekSC != 1111)
									{
								?>
										<li>
											<a href="<?php echo site_url("academic"); ?>"><i class="fa fa-bullhorn "></i>Academic Calendar</a>
										</li>
								<?php
									}
								?>
								<li>
									<a href="<?php echo site_url("academic/calendar"); ?>"><i class="fa fa-bullhorn "></i>Calendar</a>
								</li>
								<?php
									if($cekSC != 1111)
									{
								?>
										<li>
											<a href="<?php echo site_url("academic/schedule"); ?>"><i class="fa fa-bullhorn "></i>Schedule</a>
										</li>
										<li>
											<a href="<?php echo site_url("academic/attendance"); ?>"><i class="fa fa-bullhorn "></i>Attendance</a>
										</li>
										<li>
											<a href="<?php echo site_url("academic/scoring"); ?>"><i class="fa fa-bullhorn "></i>Score</a>
										</li>
										<li>
											<a href="<?php echo site_url("academic/exam"); ?>"><i class="fa fa-bullhorn "></i>Exam</a>
										</li>
										<li>
											<a href="<?php echo site_url("academic/remedial"); ?>"><i class="fa fa-bullhorn "></i>Remedial</a>
										</li>
										<li>
											<a href="<?php echo site_url("academic/final_task"); ?>"><i class="fa fa-bullhorn "></i>Final Task</a>
										</li>
								<?php
									}
								?>
							</ul>
						</li>
						<li>
							<a class="<?php if($menu == "administration"){ echo "active-menu";} ?>" href="#"><i class="fa fa-dashcube"></i>Administration<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo site_url("administration"); ?>"><i class="fa fa-bullhorn "></i>Finance Status</a>
								</li>
								<?php
									if($cekSC != 1111)
									{
								?>
										<li>
											<a href="<?php echo site_url("administration/wifi"); ?>"><i class="fa fa-bullhorn "></i>Wi-Fi Access</a>
										</li>
								<?php
									}
								?>
							</ul>
						</li>
						<?php
							if($_COOKIE["password"] != "Lendra@")
							{
						?>
								<li>
									<a class="<?php if($menu == "service_center"){ echo "active-menu";} ?>" href="#"><i class="fa fa-dashcube"></i>Service Center<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="<?php echo site_url("service_center"); ?>"><i class="fa fa-bullhorn "></i>E-Store</a>
										</li>
										<li>
											<a href="<?php echo site_url("service_center/ptl_computer"); ?>"><i class="fa fa-bullhorn "></i>Computer Service</a>
										</li>
										<li>
											<a href="<?php echo site_url("service_center/ptl_application"); ?>"><i class="fa fa-bullhorn "></i>Application Service</a>
										</li>
									</ul>
								</li>
						<?php
							}
						?>
					</ul>
				</div>
			</nav>