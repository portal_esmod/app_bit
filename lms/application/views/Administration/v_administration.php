			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Administration</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Administration
								</div>
								<div class="panel-body">
									<?php
										$cekSC = substr($_COOKIE["nim"], 4, 4);
										if($cekSC != 1111)
										{
									?>
											<form action="<?php echo site_url("administration/ptl_filter_acd"); ?>" method="POST">
												<select name="cekkhs" title="Filter by Period" class="form-control" onchange="this.form.submit()">
													<option value=''>-- LAST KHS --</option>
													<?php
														$cekkhs = $this->session->userdata('adm_filter_khs');
														$thn = $tahun;
														if($rowkhs)
														{
															$nkhs = 1;
															foreach($rowkhs as $rk)
															{
																$f = "";
																$cur = "";
																if($cekkhs != "")
																{
																	if($tahun == $rk->TahunID)
																	{
																		$f = "style='background-color: #5BB734;'";
																		$cur = " - Last KHS";
																	}
																}
																echo "<option value='$rk->TahunID' $f";
																if($cekkhs == "")
																{
																	if($tahun == $rk->TahunID)
																	{
																		echo "selected";
																		$thn = $rk->TahunID;
																	}
																}
																else
																{
																	if($cekkhs == $rk->TahunID)
																	{
																		echo "selected";
																		$thn = $rk->TahunID;
																	}
																}
																$TahunID = $rk->TahunID;
																$t = $this->m_year->PTL_select($TahunID);
																echo ">SEMESTER $nkhs [$rk->TahunID - $t[Nama]$cur]</option>";
																$nkhs++;
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
											<br/>
									<?php
										}
									?>
									<h4><?php echo $ProgramID." | ".$ProdiID.$TahunKe; ?></h4>
									<?php
										$h = "-7";
										$hm = $h * 60;
										$ms = $hm * 60;
										$tanggal = gmdate("Ymd", time()-($ms));
									?>
									<div class="table-responsive">
										<center><a class="btn btn-info" href="<?php echo site_url("administration/ptl_pdf_invoice/$BIPOTID/$MhswID/$TahunID/ESMOD%20JAKARTA"); ?>"><h5 class="box-title">Print Invoice</h5></a></center>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Total</th>
													<th>Paid</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													$totpot1 = 0;
													$totpot2 = 0;
													$totbi1 = 0;
													$totbi2 = 0;
													if($detail)
													{
														foreach($detail as $row)
														{
															if($row->TrxID == "-1")
															{
																$BIPOTMhswRef = $row->BIPOTMhswRef;
																if($BIPOTMhswRef == 0)
																{
																	$TambahanNama = "";
																	if($row->TambahanNama != "")
																	{
																		$TambahanNama = "<br/>".$row->TambahanNama;
																	}
																	$BIPOTNamaID = $row->BIPOTNamaID;
																	$res = $this->m_master->PTL_select($BIPOTNamaID);
																	$Nama = "<font color='red'>No master available</font>";
																	if($res)
																	{
																		$Nama = $res['Nama'];
																	}
																	echo"
																		<tr class='warning'>
																			<td>$no</td>
																			<td title='BIPOTMhswID : $row->BIPOTMhswID'><b>".$res['Nama']."</b>$TambahanNama</td>
																			<td><p align='right'>".formatRupiah($row->Besar)."</p></td>
																			<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																		</tr>
																		";
																	$no++;
																	$totpot1 = $totpot1 + $row->Besar;
																	$totpot2 = $totpot2 + $row->Dibayar;
																}
															}
														}
														foreach($detail as $row)
														{
															if($row->TrxID == "1")
															{
																$BIPOTID = $row->BIPOTID;
																$BIPOTNamaID = $row->BIPOTNamaID;
																$res = $this->m_master->PTL_select($BIPOTNamaID);
																$Nama = "<font color='red'>No master available</font>";
																if($res)
																{
																	$Nama = $res['Nama'];
																}
																$BIPOTMhswRef = $row->BIPOT2ID;
																$resref_detail = $this->m_master->PTL_pym_detail_ref_cek($BIPOTID,$BIPOTMhswRef,$MhswID);
																$ref_detail = "";
																$ref_jumlah2 = 0;
																if($resref_detail)
																{
																	foreach($resref_detail as $rd)
																	{
																		$BIPOTNamaID = $rd->BIPOTNamaID;
																		$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
																		if($resref_detail2)
																		{
																			$nm = $resref_detail2['Nama'];
																		}
																		$ref_detail .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$nm."&nbsp;(".formatRupiah($rd->Besar).")";
																		$ref_jumlah2 = $ref_jumlah2 + $rd->Besar;
																	}
																}
																$TambahanNama = "";
																if($row->TambahanNama != "")
																{
																	$TambahanNama = "<br/>".$row->TambahanNama;
																}
																echo"
																	<tr class='success'>
																		<td>$no</td>
																		<td title='BIPOTMhswID : $row->BIPOTMhswID'>
																			<b>$Nama</b>
																			$TambahanNama
																			$ref_detail
																		</td>
																		<td title='Bill : ".formatRupiah($row->Besar)." ~ Discount : ".formatRupiah($ref_jumlah2)."'><p align='right'>".formatRupiah($row->Besar - $ref_jumlah2)."</p></td>
																		<td><p align='right'>".formatRupiah($row->Dibayar)."</p></td>
																	</tr>
																	";
																$no++;
																$totbi1 = $totbi1 + ($row->Besar - $ref_jumlah2);
																$totbi2 = $totbi2 + $row->Dibayar;
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='2'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi1 - $totpot1)."</p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'>".formatRupiah($totbi2 - $totpot2)."</p></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='5'><p align='center'><b>EMPTY</b></p></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
										<center><h3><font color="red">REMAINING : <?php echo formatRupiah(($totbi1 - $totpot1) - ($totbi2 - $totpot2)); ?></font></h3></center>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Trx ID</th>
													<th>Type</th>
													<th>Date</th>
													<th>To Account</th>
													<th>Paid</th>
													<th>Note</th>
													<th>View</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$total_bayar = 0;
													if($rowbukti)
													{
														$no = 1;
														foreach($rowbukti as $row)
														{
															$trx = "<font color='green'><b>PAID</b></font>";
															if($row->TrxID == "-1")
															{
																$trx = "<font color='blue'><b>WITHDRAWAL</b></font>";
															}
															if($row->TrxID == "-2")
															{
																$trx = "<font color='red'><b>CORRECTION</b></font>";
															}
															$cetak = $row->Cetak;
															echo"
																<tr>
																	<td style='background-color:#ECF0F5;border:5px;'>$no</td>
																	<td style='background-color:#ECF0F5;border:5px;'>$row->BayarMhswID</a></td>
																	<td style='background-color:#ECF0F5;border:5px;'>$trx</a></td>
																	<td style='background-color:#ECF0F5;border:5px;'>".tgl_singkat_eng($row->Tanggal)."</a></td>
																	<td style='background-color:#ECF0F5;border:5px;'>$row->RekeningID</a></td>
																	<td style='background-color:#ECF0F5;border:5px;' align='right'>".formatRupiah($row->Jumlah)."</td>
																	<td style='background-color:#ECF0F5;border:5px;'>$row->Keterangan</td>
																	<td style='background-color:#ECF0F5;border:5px;'>$cetak</td>
																	<td style='background-color:#ECF0F5;border:5px;'><a href='".site_url("administration/ptl_print_payment/$row->BayarMhswID/$KHSID/$MhswID/$TahunID/COPY/ESMOD%20JAKARTA")."' class='btn btn-primary'>Print</a></td>
																</tr>
																";
															$no++;
															if($row->TrxID == "-1")
															{
																
															}
															else
															{
																if($row->TrxID == "-2")
																{
																	
																}
																else
																{
																	$total_bayar = $total_bayar + $row->Jumlah;
																}
															}
														}
														echo"
															<tr>
																<td style='background-color:#D8E5D8;border:5px;' colspan='5'><p align='center'><b>TOTAL</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'><p align='right'><b>".formatRupiah($total_bayar)."</b></p></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
																<td style='background-color:#D8E5D8;border:5px;'></td>
															</tr>
															";
													}
													else
													{
														echo"
															<tr>
																<td colspan='9' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>