			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Personal Information</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail Personal Information
								</div>
								<div class="panel-body">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#home" data-toggle="tab">Personal</a></li>
										<li class=""><a href="#profile" data-toggle="tab">Address</a></li>
										<li class=""><a href="#messages" data-toggle="tab">Parents</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane fade active in" id="home">
											<table class="table">
												<tbody>
													<tr>
														<td><label>Student Identification Number (SIN)</label></td>
														<td><input disabled type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Name</label></td>
														<td><input disabled type="text" value="<?php echo $Nama; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Marketing</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($marketing)
																	{
																		foreach($marketing as $row)
																		{
																			$word = explode(" ",$row->nama);
																			echo "<option value='".$word[0]."'";
																			if($PresenterID == $word[0])
																			{
																				echo "selected";
																			}
																			echo ">".$word[0]." - $row->nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Birth Place</label></td>
														<td><input disabled type="text" value="<?php echo $TempatLahir; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Birth Date</label></td>
														<td><input disabled type="text" value="<?php echo $TanggalLahir; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Gender</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																		echo "<option value='P'"; if($Kelamin == "P") { echo "selected"; } echo ">Man</option>";
																		echo "<option value='W'"; if($Kelamin == "W") { echo "selected"; } echo ">Woman</option>";
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Citizen</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																		echo "<option value='WNI'"; if($WargaNegara == "WNI") { echo "selected"; } echo ">WNI</option>";
																		echo "<option value='WNA'"; if($WargaNegara == "WNA") { echo "selected"; } echo ">WNA</option>";
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Nationality</label></td>
														<td><input disabled type="text" value="<?php echo $Kebangsaan; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Religion</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($Agama == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Civil Status</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowsipil)
																	{
																		foreach($rowsipil as $row)
																		{
																			echo "<option value='$row->StatusSipil'";
																			if($StatusSipil == $row->StatusSipil)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Last Education</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanTerakhir == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Graduated Year</label></td>
														<td><input disabled type="number" value="<?php echo $TahunLulus; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Body Height</label></td>
														<td><input disabled type="number" value="<?php echo $TinggiBadan; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Body Weight</label></td>
														<td><input disabled type="number" value="<?php echo $BeratBadan; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Child</label></td>
														<td><input disabled type="number" value="<?php echo $AnakKe; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Number of Siblings</label></td>
														<td><input disabled type="number" value="<?php echo $JumlahSaudara; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Current Address</label></td>
														<td><textarea disabled class="form-control"><?php echo $Alamat; ?></textarea></td>
													</tr>
													<tr>
														<td><label>Current Province</label></td>
														<td><input disabled type="text" value="<?php echo $Propinsi; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Current City</label></td>
														<td><input disabled type="text" value="<?php echo $Kota; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Current Postal Code</label></td>
														<td><input disabled type="number" value="<?php echo $KodePos; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Current Country</label></td>
														<td><input disabled type="text" value="<?php echo $Negara; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input disabled type="number" value="<?php echo $Handphone; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email 1</label></td>
														<td><input disabled type="email" value="<?php echo $Email; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email 2</label></td>
														<td><input disabled type="email" value="<?php echo $Email2; ?>" class="form-control"/></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tab-pane fade" id="profile">
											<table class="table">
												<tbody>
													<tr>
														<td><label>Home Address</label></td>
														<td><textarea disabled class="form-control"><?php echo $AlamatAsal; ?></textarea></td>
													</tr>
													<tr>
														<td><label>Home RT</label></td>
														<td><input disabled type="text" value="<?php echo $RTAsal; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Home RW</label></td>
														<td><input disabled type="text" value="<?php echo $RWAsal; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Home City</label></td>
														<td><input disabled type="text" value="<?php echo $KotaAsal; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Home Postal Code</label></td>
														<td><input disabled type="number" name="KodePosAsal" value="<?php echo $KodePosAsal; ?>" class="form-control"/></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tab-pane fade" id="messages">
											<h4>Father's Data</h4>
											<table class="table">
												<tbody>
													<tr>
														<td><label>Father Name</label></td>
														<td><input disabled type="text" value="<?php echo $NamaAyah; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Religion</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($AgamaAyah == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Last Education</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanAyah == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Profession</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpekerjaan)
																	{
																		foreach($rowpekerjaan as $row)
																		{
																			echo "<option value='$row->Pekerjaan'";
																			if($PekerjaanAyah == $row->Pekerjaan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Address</label></td>
														<td><textarea disabled class="form-control"><?php echo $AlamatOrtu; ?></textarea></td>
													</tr>
													<tr>
														<td><label>City</label></td>
														<td><input disabled type="text" value="<?php echo $KotaOrtu; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label>Postal Code</label></td>
														<td><input disabled type="number" value="<?php echo $KodePosOrtu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Phone</label></td>
														<td><input disabled type="number" value="<?php echo $TeleponOrtu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input disabled type="number" value="<?php echo $HandphoneOrtu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td><input disabled type="email" value="<?php echo $EmailOrtu; ?>" class="form-control"/></td>
													</tr>
												</tbody>
											</table>
											<h4>Mother's Data</h4>
											<table class="table">
												<tbody>
													<tr>
														<td><label>Mother Name</label></td>
														<td><input disabled type="text" value="<?php echo $NamaIbu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Religion</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($AgamaIbu == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Last Education</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanIbu == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Profession</label></td>
														<td>
															<select disabled class="form-control">
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpekerjaan)
																	{
																		foreach($rowpekerjaan as $row)
																		{
																			echo "<option value='$row->Pekerjaan'";
																			if($PekerjaanIbu == $row->Pekerjaan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Phone</label></td>
														<td><input disabled type="number" value="<?php echo $TeleponIbu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input disabled type="number" value="<?php echo $HandphoneIbu; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td><input disabled type="email" value="<?php echo $EmailIbu; ?>" class="form-control"/></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tab-pane fade" id="settings">
											<h4>Settings Tab</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>