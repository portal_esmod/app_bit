			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Change Password</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Change Password
								</div>
								<div class="panel-body">
									<form action="<?php echo site_url("login/ptl_password_update"); ?>" method="POST">
										<table class="table">
											<tbody>
												<tr>
													<td><label>Old Password</label></td>
													<td><input type="password" name="pass_lama" class="form-control" required/></td>
												</tr>
												<tr>
													<td><label>New Password</label></td>
													<td><input type="password" name="pass_baru1" class="form-control" required/></td>
												</tr>
												<tr>
													<td><label>Confirm New Password</label></td>
													<td><input type="password" name="pass_baru2" class="form-control" required/></td>
												</tr>
												<tr>
													<td></td>
													<td><input type="submit" value="Save" class="btn btn-primary form-control"/></td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>