			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Attendance Detail</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Attendance Detail for <b><?php echo $NamaSubjek; ?></b>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<?php
													$foto1 = "";
													$foto_preview1 = "";
													if($NamaDosenID != "")
													{
														$foto1 = "../hris/system_storage/foto_karyawan/$FotoDosenID";
														$exist = file_exists_remote(base_url("$foto1"));
														if($exist)
														{
															$foto1 = $foto1;
															$source_photo = base_url("$foto1");
															$info = pathinfo($source_photo);
															$foto_preview1 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview1"));
															if($exist1)
															{
																$foto_preview1 = $foto_preview1;
															}
															else
															{
																$foto_preview1 = $foto1;
															}
														}
														else
														{
															$foto1 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview1 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Lecturer
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID; ?>" href="<?php echo base_url("$foto1"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview1"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID; ?>
														</td>
												<?php
													}
												?>
												<?php
													$foto2 = "";
													$foto_preview2 = "";
													if($NamaDosenID2 != "")
													{
														$foto2 = "../hris/system_storage/foto_karyawan/$FotoDosenID2";
														$exist = file_exists_remote(base_url("$foto2"));
														if($exist)
														{
															$foto2 = $foto2;
															$source_photo = base_url("$foto2");
															$info = pathinfo($source_photo);
															$foto_preview2 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview2"));
															if($exist1)
															{
																$foto_preview2 = $foto_preview2;
															}
															else
															{
																$foto_preview2 = $foto2;
															}
														}
														else
														{
															$foto2 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview2 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Assistant 1
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID2; ?>" href="<?php echo base_url("$foto2"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview2"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID2; ?>
														</td>
												<?php
													}
												?>
												<?php
													$foto3 = "";
													$foto_preview3 = "";
													if($NamaDosenID3 != "")
													{
														$foto3 = "../hris/system_storage/foto_karyawan/$FotoDosenID3";
														$exist = file_exists_remote(base_url("$foto3"));
														if($exist)
														{
															$foto3 = $foto3;
															$source_photo = base_url("$foto3");
															$info = pathinfo($source_photo);
															$foto_preview3 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview3"));
															if($exist1)
															{
																$foto_preview3 = $foto_preview3;
															}
															else
															{
																$foto_preview3 = $foto3;
															}
														}
														else
														{
															$foto3 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview3 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Assistant 2
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID3; ?>" href="<?php echo base_url("$foto3"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview3"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID3; ?>
														</td>
												<?php
													}
												?>
												<?php
													$foto4 = "";
													$foto_preview4 = "";
													if($NamaDosenID4 != "")
													{
														$foto4 = "../hris/system_storage/foto_karyawan/$FotoDosenID4";
														$exist = file_exists_remote(base_url("$foto4"));
														if($exist)
														{
															$foto4 = $foto4;
															$source_photo = base_url("$foto4");
															$info = pathinfo($source_photo);
															$foto_preview4 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview4"));
															if($exist1)
															{
																$foto_preview4 = $foto_preview4;
															}
															else
															{
																$foto_preview4 = $foto4;
															}
														}
														else
														{
															$foto4 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview4 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Assistant 3
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID4; ?>" href="<?php echo base_url("$foto4"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview4"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID4; ?>
														</td>
												<?php
													}
												?>
												<?php
													$foto5 = "";
													$foto_preview5 = "";
													if($NamaDosenID5 != "")
													{
														$foto5 = "../hris/system_storage/foto_karyawan/$FotoDosenID5";
														$exist = file_exists_remote(base_url("$foto5"));
														if($exist)
														{
															$foto5 = $foto5;
															$source_photo = base_url("$foto5");
															$info = pathinfo($source_photo);
															$foto_preview5 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview5"));
															if($exist1)
															{
																$foto_preview5 = $foto_preview5;
															}
															else
															{
																$foto_preview5 = $foto5;
															}
														}
														else
														{
															$foto5 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview5 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Assistant 4
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID5; ?>" href="<?php echo base_url("$foto5"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview5"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID5; ?>
														</td>
												<?php
													}
												?>
												<?php
													$foto6 = "";
													$foto_preview6 = "";
													if($NamaDosenID6 != "")
													{
														$foto6 = "../hris/system_storage/foto_karyawan/$FotoDosenID6";
														$exist = file_exists_remote(base_url("$foto6"));
														if($exist)
														{
															$foto6 = $foto6;
															$source_photo = base_url("$foto6");
															$info = pathinfo($source_photo);
															$foto_preview6 = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview6"));
															if($exist1)
															{
																$foto_preview6 = $foto_preview6;
															}
															else
															{
																$foto_preview6 = $foto6;
															}
														}
														else
														{
															$foto6 = "ptl_storage/foto_umum/user.jpg";
															$foto_preview6 = "ptl_storage/foto_umum/user.jpg";
														}
												?>
														<td align="center">
															Assistant 5
															<br/>
															<a class="fancybox" title="<?php echo $NamaDosenID6; ?>" href="<?php echo base_url("$foto6"); ?>" data-fancybox-group="gallery" >
																<img class="img-polaroid" src="<?php echo base_url("$foto_preview6"); ?>" width="120px" alt=""/>
															</a>
															<br/>
															<?php echo $NamaDosenID6; ?>
														</td>
												<?php
													}
												?>
											</tr>
										</table>
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<td>Download Project Sheet</td>
												<td>
													<?php
														if(substr($file_project,8) == "")
														{
															echo "<font color='red'>No file uploaded</font>";
														}
														else
														{
													?>
															<a href="<?php echo base_url("academic/ptl_download_project_sheet/$JadwalID/$file_project"); ?>" title="<?php echo $file_project; ?>" class="btn btn-info" >Download</a>
													<?php
														}
													?>
												</td>
												<td>Download Evaluation Sheet</td>
												<td>
													<?php
														if(substr($file_evaluation,8) == "")
														{
															echo "<font color='red'>No file uploaded</font>";
														}
														else
														{
													?>
															<a href="<?php echo base_url("academic/ptl_download_evaluation_sheet/$JadwalID/$file_evaluation"); ?>" title="<?php echo $file_evaluation; ?>" class="btn btn-info" >Download</a>
													<?php
														}
													?>
												</td>
											</tr>
										</table>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>ID</th>
													<th>Date</th>
													<th>Time</th>
													<th>Room</th>
													<th>Lecturer</th>
													<th>Topic</th>
													<th>File</th>
													<th colspan="2">Status</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$MhswID = $_COOKIE["nim"];
														$no = 1;
														$sebelumnya = "";
														$b1 = "";
														$b2 = "";
														foreach($rowrecord as $row)
														{
															$remove = "N";
															$DosenID = $row->DosenID;
															$dsn = $this->m_dosen->PTL_select($DosenID);
															$MKID = $row->MKID;
															$mkid = $this->m_mk->PTL_select($MKID);
															$nmmk = "";
															if($mkid)
															{
																$nmmk = "<font color='red'><b>$mkid[Nama]</b></font><br/>";
															}
															$nmdsn = "<font color='red'><b>Not set</b></font>";
															if($dsn)
															{
																$nmdsn = $dsn['Nama'];
															}
															$PresensiID = $row->PresensiID;
															$respresensi = $this->m_presensi_mahasiswa->PTL_select_presensi($PresensiID,$MhswID);
															$prsi1 = "";
															$prsi2 = "";
															if($respresensi)
															{
																if($respresensi['Multi'] == "Y")
																{
																	$JenisPresensiID = $respresensi['JenisPresensiID'];
																	if($JenisPresensiID == "P"){ $b1 = "<font color='green'>"; $b2 = "</font>"; }
																	if($JenisPresensiID == "E"){ $b1 = "<font color='purple'>"; $b2 = "</font>"; }
																	if($JenisPresensiID == "S"){ $b1 = "<font color='blue'>"; $b2 = "</font>"; }
																	if($JenisPresensiID == "A"){ $b1 = "<font color='red'>"; $b2 = "</font>"; }
																	if($JenisPresensiID == "L"){ $b1 = "<font color='yellow'>"; $b2 = "</font>"; }
																	$r1 = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
																	$JenisPresensiID = $respresensi['JenisPresensiID2'];
																	if($JenisPresensiID == "P"){ $b3 = "<font color='green'>"; $b4 = "</font>"; }
																	if($JenisPresensiID == "E"){ $b3 = "<font color='purple'>"; $b4 = "</font>"; }
																	if($JenisPresensiID == "S"){ $b3 = "<font color='blue'>"; $b4 = "</font>"; }
																	if($JenisPresensiID == "A"){ $b3 = "<font color='red'>"; $b4 = "</font>"; }
																	if($JenisPresensiID == "L"){ $b3 = "<font color='yellow'>"; $b4 = "</font>"; }
																	$r2 = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
																	$ps = "<td align='center'>$b1".@$r1["Nama"]."$b2</td>
																			<td align='center'>$b3".@$r2["Nama"]."$b4</td>";
																}
																else
																{
																	$respresensi2 = $this->m_presensi_mahasiswa->PTL_select_presensi_aktif($PresensiID,$MhswID);
																	if(!$respresensi2)
																	{
																		$ps = "<td colspan='2' align='center'><font color='red'><b>Removed</b></font></td>";
																		$remove = "Y";
																	}
																	else
																	{
																		$JenisPresensiID = $respresensi['JenisPresensiID'];
																		if($JenisPresensiID == "P"){ $b1 = "<font color='green'>"; $b2 = "</font>"; }
																		if($JenisPresensiID == "E"){ $b1 = "<font color='purple'>"; $b2 = "</font>"; }
																		if($JenisPresensiID == "S"){ $b1 = "<font color='blue'>"; $b2 = "</font>"; }
																		if($JenisPresensiID == "A"){ $b1 = "<font color='red'>"; $b2 = "</font>"; }
																		if($JenisPresensiID == "L"){ $b1 = "<font color='yellow'>"; $b2 = "</font>"; }
																		$r1 = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
																		$ps = "<td colspan='2' align='center'>$b1".@$r1["Nama"]."$b2</td>";
																	}
																}
															}
															else
															{
																$ps = "<td colspan='2' align='center'><font color='red'>No data available</font></td>";
															}
															if($remove == "N")
															{
																$resfiles = $this->m_krs2->PTL_select_spesifik($KRSID,$PresensiID);
																$files = "";
																if($resfiles)
																{
																	$files = $resfiles["files"];
																}
																echo "<tr class='info'>
																		<td>$no</td>
																		<td>$row->PresensiID</td>
																		<td>".tgl_indo($row->Tanggal)."</td>
																		<td>$row->JamMulai - $row->JamSelesai</td>
																		<td>$row->RuangID</td>
																		<td>$nmdsn</td>
																		<td>$nmmk$row->Catatan</td>
																		<td align='center'>";
																			if($files != "")
																			{
												?>
																				<a href="<?php echo base_url(); ?>academic/ptl_download_file/<?php echo $files; ?>" title="<?php echo $files; ?>" class="btn btn-danger btn-sm btn-round btn-line" ><?php $file_order = substr($files,-15); echo $file_order; ?></a>
												<?php
																			}
																			else
																			{
																				echo "<font color='red'>No file</font>";
																			}
																	echo "</td>
																		$ps
																	</tr>";
																$no++;
															}
														}
													}
													else
													{
														echo "<tr>
																<td colspan='7'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
										<a href="<?php echo site_url("academic/attendance"); ?>" class="btn btn-warning">Back</i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>