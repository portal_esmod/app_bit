			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Scoring</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Scoring
								</div>
								<div class="table-responsive panel-body">
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<?php
												if($rowrecord)
												{
													$no = 1;
													$tahun = 0;
													$kumulatif = 0;
													$kumulatifsks = 0;
													$kumulatifipk = 0;
													$ipk = 0;
													foreach($rowrecord as $row)
													{
														$MhswID = $row->MhswID;
														$r = $this->m_mahasiswa->PTL_select($MhswID);
														$kumulatif++;
														if($no == 1)
														{
															$tahun = $tahun + 1;
															$no++;
														}
														else
														{
															$tahun = $tahun;
															$no = 1;
														}
														$KelasID = $row->KelasID;
														$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
														if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
														$DosenID = $row->WaliKelasID;
														$rhr1 = $this->m_dosen->PTL_select($DosenID);
														if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
														$DosenID = $row->WaliKelasID2;
														$rhr2 = $this->m_dosen->PTL_select($DosenID);
														if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
														$SpesialisasiID = $row->SpesialisasiID;
														$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
														if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
														$b = "<font color='green' size='4'><b>";
														$thn = "";
														if($row->TahunKe == 1){ $thn = "st"; }
														if($row->TahunKe == 2){ $thn = "nd"; }
														if($row->TahunKe == 3){ $thn = "rd"; }
														if($r["ProgramID"] == "INT")
														{
															$TahunKe = "O";
														}
														else
														{
															$TahunKe = $row->TahunKe;
														}
														$ProgramID = $r["ProgramID"];
														$smt = "";
														if($row->Sesi == 1){ $smt = "st"; }
														if($row->Sesi == 2){ $smt = "nd"; }
														if($row->Sesi == 3){ $smt = "rd"; }
														if($row->Sesi == 4){ $smt = "th"; }
														if($row->Sesi == 5){ $smt = "th"; }
														if($row->Sesi == 6){ $smt = "th"; }
														echo "<tr class='success'>
																<td>$b $TahunKe$thn&nbsp;Year</td>
																<td>$b $row->Sesi$smt&nbsp;Semester</td>";
														if($row->ProgramID == "INT")
														{
															echo "<td colspan='2'>$b Class:<br/>O$kelas</td>";
														}
														else
														{
															echo "<td colspan='2'>$b Class:<br/>$TahunKe$kelas</td>";
														}
														echo "<td colspan='2'>$b Specialization:<br/>$spe</td>
															<td colspan='4'>$b Homeroom:<br/>$hr1</td>
															<td colspan='4'>$b Vice Homeroom:<br/>$hr2</td>
														</tr>";
														if($row->suspend == "Y")
														{
															echo "<tr class='success'>
																	<td colspan='14'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
																</tr>";
														}
														else
														{
															$KHSID = $row->KHSID;
															$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
															$totalbobot = 0;
															$ips = 0;
															if($rowkrs)
															{
																echo "
																	<tr>
																		<th rowspan='2' align='center'><p align='center'>#</p></th>
																		<th rowspan='2'><p align='center'>Subject</p></th>
																		<th rowspan='2'><p align='center'>Score</p></th>
																		<th colspan='4'><p align='center'>Attendance</p></th>
																		<th rowspan='2'><p align='center'>Minus/Plus</p></th>
																		<th rowspan='2'><p align='center'>Grade Value</p></th>
																		<th rowspan='2'><p align='center'>Exam Score</p></th>
																		<th rowspan='2'><p align='center'>Remedial Score</p></th>
																		<th rowspan='2'><p align='center'>Grade</p></th>
																		<th rowspan='2'><p align='center'>Credit</p></th>
																		<th rowspan='2'><p align='center'>Grade Point</p></th>
																	</tr>
																	<tr>
																		<th><p align='center'>Excuse</p></th>
																		<th><p align='center'>Sick</p></th>
																		<th><p align='center'>Absent</p></th>
																		<th><p align='center'>Late</p></th>
																	</tr>
																	";
																$ns = 1;
																$jumlahsks = 0;
																$jumlahexc = 0;
																$jumlahsic = 0;
																$jumlahabs = 0;
																$jumlahlat = 0;
																$jumlahmp = 0;
																$jumlahbobot = 0;
																foreach($rowkrs as $rkr)
																{
																	$SubjekID = $rkr->SubjekID;
																	$JadwalID = $rkr->JadwalID;
																	$rsub = $this->m_subjek->PTL_select($SubjekID);
																	$subjek = $rsub["Nama"];
																	$sks = $rsub["SKS"];
																	$jumlahsks = $jumlahsks + $rsub["SKS"];
																	$KurikulumID = $rsub["KurikulumID"];
																	$JenisMKID = $rsub["JenisMKID"];
																	$GradeNilai = $rkr->GradeNilai;
																	$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
																	$bobot = 0;
																	if($resbobot)
																	{
																		$bobot = $resbobot['Bobot'];
																	}
																	$KRSID = $rkr->KRSID;
																	$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																	if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																	$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																	if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																	$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																	if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																	$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																	if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																	$tottexc = $texc;
																	$jumlahexc = $jumlahexc + $tottexc;
																	if($tottexc == 0){ $tottexc = ""; }
																	$tottsic = $tsic;
																	$jumlahsic = $jumlahsic + $tottsic;
																	if($tottsic == 0){ $tottsic = ""; }
																	$tottabs = $tabs;
																	$jumlahabs = $jumlahabs + $tottabs;
																	if($tottabs == 0){ $tottabs = ""; }
																	$tottlat = $tlat;
																	$jumlahlat = $jumlahlat + $tottlat;
																	if($tottlat == 0){ $tottlat = ""; }
																	$TahunID = $rkr->TahunID;
																	
																	$exam = 0;
																	$ExamID = "";
																	$ExamMhswID = "";
																	$TotJum = "";
																	$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
																	if($rexmhsw1)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw1 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA1)
																			{
																				if($rexmhswA1['Nilai'] > 0)
																				{
																					$jumexam = $jumexam + $rexmhswA1['Nilai'];
																					$ExamMhswID = $rexmhswA1['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
																	if($rexmhsw2)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw2 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA2)
																			{
																				if($rexmhswA2['Nilai'] > 0)
																				{
																					$jumexam = $jumexam + $rexmhswA2['Nilai'];
																					$ExamMhswID = $rexmhswA2['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
																	if($rexmhsw3)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw3 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA3)
																			{
																				if($rexmhswA3['Nilai'] > 0)
																				{
																					$jumexam = $jumexam + $rexmhswA3['Nilai'];
																					$ExamMhswID = $rexmhswA3['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
																	if($rexmhsw4)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw4 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA4)
																			{
																				if($rexmhswA4['Nilai'] > 0)
																				{
																					$jumexam = $jumexam + $rexmhswA4['Nilai'];
																					$ExamMhswID = $rexmhswA4['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
																	if($rexmhsw5)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw5 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA5)
																			{
																				if($rexmhswA5['Nilai'] > 0)
																				{
																					$exam = $exam + $rexmhswA5['Nilai'];
																					$ExamMhswID = $rexmhswA5['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
																	if($rexmhsw6)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw6 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA6)
																			{
																				if($rexmhswA6['Nilai'] > 0)
																				{
																					$exam = $exam + $rexmhswA6['Nilai'];
																					$ExamMhswID = $rexmhswA6['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	if($exam == 0)
																	{
																		$exam = "-";
																	}
																	else
																	{
																		if($TotJum > 1)
																		{
																			$exam = number_format(($exam),2,'.','');
																			// $exam = number_format(($exam / $TotJum),2,'.','');
																		}
																	}
																	$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
																	$remedial = "-";
																	$KRSRemedialID = "";
																	if($resremedial)
																	{
																		$remedial = $resremedial['Nilai'];
																		$KRSRemedialID = $resremedial['KRSRemedialID'];
																	}
																	
																	if($rkr->gradevalue == "")
																	{
																		$mp = "";
																	}
																	else
																	{
																		$JenisPresensiID = "E";
																		$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
																		$mptottexc = $texc * $pr1;
																		$JenisPresensiID = "S";
																		$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
																		$mptottsic = $tsic * $pr2;
																		$JenisPresensiID = "A";
																		$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
																		$mptottabs = $tabs * $pr3;
																		$JenisPresensiID = "L";
																		$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
																		$mptottlat = $tlat * $pr4;
																		$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
																		if($mp == 0.00)
																		{
																			$mp = "";
																		}
																		$jumlahmp = $jumlahmp + $mp;
																	}
																	$jumlahbobot = $bobot * $sks;
																	$totalbobot = $totalbobot + $jumlahbobot;
																	$optionsub = "";
																	if($rsub["Optional"] == "Y")
																	{
																		$optionsub = "<font size='1'> (Optional)</font>";
																	}
																	$StatusSubject = "";
																	if($rkr->NA == "Y")
																	{
																		$StatusSubject = "<br/><font color='red'><b>(Drop)</b></font>";
																	}
																	echo "<tr class='info'>
																			<td>$ns</td>
																			<td title='KRS ID: $rkr->KRSID ~ Subject ID: $rkr->SubjekID'>$subjek$optionsub$StatusSubject</td>
																			<td><p align='right'>$rkr->NilaiAkhir</p></td>
																			<td><p align='center'>$tottexc</p></td>
																			<td><p align='center'>$tottsic</p></td>
																			<td><p align='center'>$tottabs</p></td>
																			<td><p align='center'>$tottlat</p></td>
																			<td><p align='right'>$mp</p></td>
																			<td><p align='right'>$rkr->gradevalue</p></td>
																			<td><p align='right'>$exam</p></td>
																			<td><p align='right'>$remedial</p></td>
																			<td><p align='center'>$rkr->GradeNilai</p></td>
																			<td><p align='center'>$sks</p></td>
																			<td><p align='right'>$jumlahbobot</p></td>
																		</tr>";
																	$rowmk = $this->m_mk->PTL_all_select($SubjekID);
																	if($rowmk)
																	{
																		foreach($rowmk as $rm)
																		{
																			if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 28) OR ($TahunID == 29))
																			{
																				$PresensiID = "";
																				$CekJenisPresensiID = "";
																				$MKID = $rm->MKID;
																				$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
																				$KRS2ID = "";
																				$Nilai = 0;
																				$AddNilai = 0;
																				if($rowkrs2)
																				{
																					$totNilai = 0;
																					$nkrs2 = 0;
																					foreach($rowkrs2 as $rkrs2)
																					{
																						if($JenisMKID == "5")
																						{
																							$PresensiID = $rkrs2->PresensiID;
																							$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
																							if($resscoring)
																							{
																								$CekJenisPresensiID = $resscoring['JenisPresensiID'];
																							}
																						}
																						$KRS2ID .= $rkrs2->KRS2ID.' - ';
																						if($rkrs2->Nilai > 0)
																						{
																							$totNilai = $totNilai + $rkrs2->Nilai;
																							$nkrs2++;
																						}
																						$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
																					}
																					if($nkrs2 > 0)
																					{
																						$Nilai = $totNilai / $nkrs2;
																					}
																				}
																				$tn = $Nilai + $AddNilai;
																				if($rowkrs2)
																				{
																					if($rm->Optional == "Y")
																					{
																						if($tn != 0)
																						{
																							echo "<tr class='info'>
																								<td>&nbsp;</td>
																								<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama (Optional)</font></td>
																								<td><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																								<td colspan='11'>&nbsp;</td>
																							</tr>";
																						}
																					}
																					else
																					{
																						if($JenisMKID == "5")
																						{
																							if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'><b>-</b></font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																							else
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																						}
																						else
																						{
																							echo "<tr class='info'>
																									<td>&nbsp;</td>
																									<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																									<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																									<td colspan='11'>&nbsp;</td>
																								</tr>";
																						}
																					}
																				}
																			}
																			else
																			{
																				$PresensiID = "";
																				$CekJenisPresensiID = "";
																				$CekMKID = $rm->MKID;
																				$rescekmk = $this->m_presensi->PTL_select_cek_mk($JadwalID,$CekMKID);
																				if($rescekmk)
																				{
																					$MKID = $rm->MKID;
																					$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
																					$KRS2ID = "";
																					$Nilai = 0;
																					$AddNilai = 0;
																					if($rowkrs2)
																					{
																						$totNilai = 0;
																						$nkrs2 = 0;
																						foreach($rowkrs2 as $rkrs2)
																						{
																							if($JenisMKID == "5")
																							{
																								$PresensiID = $rkrs2->PresensiID;
																								$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
																								if($resscoring)
																								{
																									$CekJenisPresensiID = $resscoring['JenisPresensiID'];
																								}
																							}
																							$KRS2ID .= $rkrs2->KRS2ID.' - ';
																							if($rkrs2->Nilai > 0)
																							{
																								$totNilai = $totNilai + $rkrs2->Nilai;
																								$nkrs2++;
																							}
																							$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
																						}
																						if($nkrs2 > 0)
																						{
																							$Nilai = $totNilai / $nkrs2;
																						}
																					}
																					$tn = $Nilai + $AddNilai;
																					if($rowkrs2)
																					{
																						if($rm->Optional == "Y")
																						{
																							if($tn != 0)
																							{
																								echo "<tr class='info'>
																									<td>&nbsp;</td>
																									<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama (Optional)</font></td>
																									<td><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																									<td colspan='11'>&nbsp;</td>
																								</tr>";
																							}
																						}
																						else
																						{
																							if($JenisMKID == "5")
																							{
																								if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
																								{
																									echo "<tr class='info'>
																											<td>&nbsp;</td>
																											<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																											<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'><b>-</b></font></p></td>
																											<td colspan='11'>&nbsp;</td>
																										</tr>";
																								}
																								else
																								{
																									echo "<tr class='info'>
																											<td>&nbsp;</td>
																											<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																											<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																											<td colspan='11'>&nbsp;</td>
																										</tr>";
																								}
																							}
																							else
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																	$ns++;
																}
															}
															$kumulatifsks = $kumulatifsks + $jumlahsks;
															$ips = number_format(($totalbobot / $jumlahsks), 2);
															$kumulatifipk = $kumulatifipk + $ips;
															$ipk = number_format(($kumulatifipk / $kumulatif), 2);
															echo "
																<tr>
																	<td colspan='3' align='center'>TOTAL</td>
																	<td align='right'>$jumlahexc</td>
																	<td align='right'>$jumlahsic</td>
																	<td align='right'>$jumlahabs</td>
																	<td align='right'>$jumlahlat</td>
																	<td align='right'>$jumlahmp</td>
																	<td colspan='6'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Total Grade points earned this semester</td>
																	<td>:</td>
																	<td align='right'>$totalbobot</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Number of Credits earned this semester</td>
																	<td>:</td>
																	<td align='right'>$jumlahsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Grade Point Average earned this semester</td>
																	<td>:</td>
																	<td align='right'>$ips</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Credit earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$kumulatifsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Grade Point Average earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$ipk</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>";
														}
													}
												}
												else
												{
													echo "<tr>
															<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											?>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>