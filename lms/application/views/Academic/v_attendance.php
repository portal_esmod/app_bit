			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Attendance</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Attendance
								</div>
								<div class="panel-body">
									<form action="<?php echo site_url("academic/ptl_filter_att"); ?>" method="POST">
										<select name="cekkhs" title="Filter by Period" class="form-control" onchange="this.form.submit()">
											<option value=''>-- LAST KHS --</option>
											<?php
												$cekkhs = $this->session->userdata('acd_filter_att');
												$thn = $tahun;
												if($rowkhs)
												{
													$nkhs = 1;
													foreach($rowkhs as $rk)
													{
														$f = "";
														$cur = "";
														if($cekkhs != "")
														{
															if($tahun == $rk->TahunID)
															{
																$f = "style='background-color: #5BB734;'";
																$cur = " - Last KHS";
															}
														}
														echo "<option value='$rk->TahunID' $f";
														if($cekkhs == "")
														{
															if($tahun == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														else
														{
															if($cekkhs == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														$TahunID = $rk->TahunID;
														$t = $this->m_year->PTL_select($TahunID);
														echo ">SEMESTER $nkhs [$rk->TahunID - $t[Nama]$cur]</option>";
														$nkhs++;
													}
												}
											?>
										</select>
										<noscript><input type="submit" value="Submit"></noscript>
									</form>
									<br/>
									<h4><?php echo $ProgramID." | ".$ProdiID." | ".$TahunKe; ?></h4>
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>CODE</th>
													<th>SUBJECT</th>
													<th>ROOM</th>
													<th>TEACHER</th>
													<th>CREDIT</th>
													<th>EXCUSE</th>
													<th>SICK</th>
													<th>ABSENT</th>
													<th>LATE</th>
													<th>DETAIL</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($suspend == "Y")
													{
														echo "
															<tr class='info'>
																<td colspan='11'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
															</tr>
															";
													}
													else
													{
														if($rowrecord)
														{
															$MhswID = $_COOKIE["nim"];
															$no = 1;
															$totsks = 0;
															$totexc = 0;
															$totsic = 0;
															$totabs = 0;
															$totlat = 0;
															$jumlahexc = 0;
															$jumlahsic = 0;
															$jumlahabs = 0;
															$jumlahlat = 0;
															foreach($rowrecord as $rb)
															{
																$totsks = $totsks + $rb->SKS;
																$KRSID = $rb->KRSID;
																$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																$totexc = $texc;
																$jumlahexc = $jumlahexc + $totexc;
																if($totexc == 0){ $totexc = ""; }
																$totsic = $tsic;
																$jumlahsic = $jumlahsic + $totsic;
																if($totsic == 0){ $totsic = ""; }
																$totabs = $tabs;
																$jumlahabs = $jumlahabs + $totabs;
																if($totabs == 0){ $totabs = ""; }
																$totlat = $tlat;
																$jumlahlat = $jumlahlat + $totlat;
																if($totlat == 0){ $totlat = ""; }
																$JadwalID = $rb->JadwalID;
																$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID'];
																}
																$d = $this->m_dosen->PTL_select($DosenID);
																$f1 = "";
																$f2 = "";
																$dosen = "";
																if($d)
																{
																	if($d["NA"] == "Y")
																	{
																		$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																		$f2 = "</b></font>";
																	}
																	$dosen = $d["Nama"];
																}
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID2'];
																}
																$d2 = $this->m_dosen->PTL_select($DosenID);
																$dosen2 = "";
																if($d2)
																{
																	$dosen2 = $d2["Nama"];
																}
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID3'];
																}
																$d3 = $this->m_dosen->PTL_select($DosenID);
																$dosen3 = "";
																if($d3)
																{
																	$dosen3 = $d3["Nama"];
																}
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID4'];
																}
																$d4 = $this->m_dosen->PTL_select($DosenID);
																$dosen4 = "";
																if($d4)
																{
																	$dosen4 = $d4["Nama"];
																}
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID5'];
																}
																$d5 = $this->m_dosen->PTL_select($DosenID);
																$dosen5 = "";
																if($d5)
																{
																	$dosen5 = $d5["Nama"];
																}
																$DosenID = '';
																if($resjadwal)
																{
																	$DosenID = $resjadwal['DosenID6'];
																}
																$d6 = $this->m_dosen->PTL_select($DosenID);
																$dosen6 = "";
																if($d6)
																{
																	$dosen6 = $d6["Nama"];
																}
																$resruang = $this->m_jadwal_hari->PTL_select($JadwalID);
																$ruang = "TBA";
																if($resruang)
																{
																	$ruang = $resruang['RuangID'];
																}
																if($dosen2 != "")
																{
																	$f1 = "<font color='blue'><b title='This lecturer has resigned'>";
																	$f2 = "</b></font>";
																	$dosen = "Multiple Lecturer";
																}
																echo "
																	<tr>
																		<td>$no</td>
																		<td align='center'>$rb->SubjekKode</td>
																		<td>$rb->Nama</td>
																		<td>$ruang</td>
																		<td>$f1$dosen$f2</td>
																		<td align='right'>$rb->SKS</td>
																		<td align='right'>$totexc</td>
																		<td align='right'>$totsic</td>
																		<td align='right'>$totabs</td>
																		<td align='right'>$totlat</td>
																		<td><a href='".site_url("academic/attendance_detail/$rb->JadwalID/$KRSID")."' class='btn btn-primary'><i class='fa fa-list'></i></a></td>
																	</tr>
																	";
																$no++;
															}
															if($jumlahexc == 0){ $jumlahexc = ""; }
															if($jumlahsic == 0){ $jumlahsic = ""; }
															if($jumlahabs == 0){ $jumlahabs = ""; }
															if($jumlahlat == 0){ $jumlahlat = ""; }
															echo "
																<tr>
																	<td colspan='5' align='center'><b>TOTAL</b></td>
																	<td align='right'>$totsks</td>
																	<td align='right'>$jumlahexc</td>
																	<td align='right'>$jumlahsic</td>
																	<td align='right'>$jumlahabs</td>
																	<td align='right'>$jumlahlat</td>
																	<td align='right'></td>
																</tr>
																";
														}
														else
														{
															echo "
																<tr>
																	<td colspan='11' align='center'><font color='red'><b>No data available</b></font></td>
																</tr>
																";
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>