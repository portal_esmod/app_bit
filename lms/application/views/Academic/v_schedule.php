			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Schedule</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Schedule
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<a class="alert-link">Notes: </a>If your schedule does not appear, please contact the finance department.
									</div>
									<form action="<?php echo site_url("academic/ptl_filter_scd"); ?>" method="POST">
										<select name="cekkhs" title="Filter by Period" class="form-control" onchange="this.form.submit()">
											<option value=''>-- LAST KHS --</option>
											<?php
												$cekkhs = $this->session->userdata('acd_filter_scd');
												$thn = $tahun;
												if($rowkhs)
												{
													$nkhs = 1;
													foreach($rowkhs as $rk)
													{
														$f = "";
														$cur = "";
														if($cekkhs != "")
														{
															if($tahun == $rk->TahunID)
															{
																$f = "style='background-color: #5BB734;'";
																$cur = " - Last KHS";
															}
														}
														echo "<option value='$rk->TahunID' $f";
														if($cekkhs == "")
														{
															if($tahun == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														else
														{
															if($cekkhs == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														$TahunID = $rk->TahunID;
														$t = $this->m_year->PTL_select($TahunID);
														echo ">SEMESTER $nkhs [$rk->TahunID - $t[Nama]$cur]</option>";
														$nkhs++;
													}
												}
											?>
										</select>
										<noscript><input type="submit" value="Submit"></noscript>
									</form>
									<br/>
									<table>
										<tr>
											<td width="10%">Select Date:</td>
											<td width="20%">
												<form action="<?php echo site_url("academic/ptl_filter_tanggal"); ?>" method="POST" id="formId">
													<?php
														$cetanggal = $this->session->userdata('lms_schedule_filter_tanggal');
														if($cetanggal == "")
														{
															$h = "-7";
															$hm = $h * 60;
															$ms = $hm * 60;
															$tahun = gmdate("Y-m-d", time()-($ms));
															$cetanggal = $tahun;
														}
													?>
													<input type="text" name="cetanggal" value="<?php echo $cetanggal; ?>" id="OnDate1" placeholder="yyyy" class="form-control">
												</form>
											</td>
											<td>&nbsp;</td>
											<td><a href="<?php echo site_url("academic/ptl_filter_unset_tanggal"); ?>" class="btn btn-primary">Clear Date</a></td>
										</tr>
									</table>
									<br/>
									<div class="table-responsive">
										<table class="table">
											<?php
												if($suspend == "Y")
												{
													echo "
														<tr class='info'>
															<td colspan='6'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
														</tr>
														";
												}
												else
												{
													$TahunID = $tahunacd;
													if($rowrecord)
													{
														$nkrs = 1;
														foreach($rowrecord as $row)
														{
															if(($row->HariID > 0) AND ($row->HariID < 6))
															{
																if($row->HariID == 1){ $Tanggal = $D1; }
																if($row->HariID == 2){ $Tanggal = $D2; }
																if($row->HariID == 3){ $Tanggal = $D3; }
																if($row->HariID == 4){ $Tanggal = $D4; }
																if($row->HariID == 5){ $Tanggal = $D5; }
																echo "<tr class='success'>
																		<th colspan='6'>".strtoupper($row->Nama_en)." <font color='red'><b>(".tgl_singkat_eng($Tanggal).")</b></font></th>
																	</tr>
																	<tr class='success'>
																		<th>SUBJECT</b></th>
																		<th>TEACHER</th>
																		<th>CODE</th>
																		<th>CREDIT</th>
																		<th>ROOM</th>
																		<th>TIME</th>
																	</tr>";
																$rowjadwal = $this->m_jadwal->PTL_all_jadwal_harian_update($Tanggal,$ProgramID,$MhswID,$TahunID);
																$no = 0;
																if($rowjadwal)
																{
																	foreach($rowjadwal as $rj)
																	{
																		$PresensiID = $rj->PresensiID;
																		$cekpresensi = $this->m_presensi_mahasiswa->PTL_all_cek_presensi_aktif($PresensiID);
																		$respresensi = $this->m_presensi_mahasiswa->PTL_select_presensi_aktif($PresensiID,$MhswID);
																		if(($cekpresensi) AND ($respresensi))
																		{
																			$no++;
																			$DosenID = $rj->DosenID;
																			$resdosen = $this->m_dosen->PTL_select($DosenID);
																			$NamaDosen = "<font color='red'><b>Not set</b></font>";
																			if($resdosen)
																			{
																				$NamaDosen = $resdosen['Nama'];
																			}
																			echo "<tr class='success'>
																					<td title='PresensiID: $PresensiID'>$rj->_subjek<br/>".tgl_singkat_eng($rj->TglMulai)." -> ".tgl_singkat_eng($rj->TglSelesai)."</td>
																					<td>$NamaDosen</td>
																					<td>$rj->SubjekKode</td>
																					<td>$rj->SKS</td>
																					<td>$rj->RuangID</td>
																					<td>".substr($rj->JamMulai,0,5)." - ".substr($rj->JamSelesai,0,5)."</td>
																				</tr>";
																		}
																		else
																		{
																			if(!$cekpresensi)
																			{
																				$no++;
																				$DosenID = $rj->DosenID;
																				$resdosen = $this->m_dosen->PTL_select($DosenID);
																				$NamaDosen = "<font color='red'><b>Not set</b></font>";
																				if($resdosen)
																				{
																					$NamaDosen = $resdosen['Nama'];
																				}
																				echo "<tr class='success'>
																						<td title='PresensiID: $PresensiID'>$rj->_subjek<br/>".tgl_singkat_eng($rj->TglMulai)." -> ".tgl_singkat_eng($rj->TglSelesai)."</td>
																						<td>$NamaDosen</td>
																						<td>$rj->SubjekKode</td>
																						<td>$rj->SKS</td>
																						<td>$rj->RuangID</td>
																						<td>".substr($rj->JamMulai,0,5)." - ".substr($rj->JamSelesai,0,5)."</td>
																					</tr>";
																			}
																		}
																	}
																	echo "<tr>
																			<th colspan='6'></th>
																		</tr>";
																}
																if($no <= 0)
																{
																	echo "<tr class='success'>
																			<th colspan='6'><font color='red'><b>No Schedule</b></font></th>
																		</tr>
																		<tr>
																			<th colspan='6'></th>
																		</tr>";
																}
															}
														}
													}
												}
											?>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>