			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Exam</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Exam
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<form action="<?php echo site_url("academic/ptl_filter_exam_tahun"); ?>" method="POST">
											<select name="cekkhs" title="Filter by Period" class="form-control" onchange="this.form.submit()">
												<option value=''>-- LAST KHS --</option>
												<?php
													$cekkhs = $this->session->userdata('acd_filter_exam_tahun');
													$thn = $tahun;
													if($rowkhs)
													{
														$nkhs = 1;
														foreach($rowkhs as $rk)
														{
															$f = "";
															$cur = "";
															if($cekkhs != "")
															{
																if($tahun == $rk->TahunID)
																{
																	$f = "style='background-color: #5BB734;'";
																	$cur = " - Last KHS";
																}
															}
															echo "<option value='$rk->TahunID' $f";
															if($cekkhs == "")
															{
																if($tahun == $rk->TahunID)
																{
																	echo "selected";
																	$thn = $rk->TahunID;
																}
															}
															else
															{
																if($cekkhs == $rk->TahunID)
																{
																	echo "selected";
																	$thn = $rk->TahunID;
																}
															}
															$TahunID = $rk->TahunID;
															$t = $this->m_year->PTL_select($TahunID);
															echo ">SEMESTER $nkhs [$rk->TahunID - $t[Nama]$cur]</option>";
															$nkhs++;
														}
													}
												?>
											</select>
											<noscript><input type="submit" value="Submit"></noscript>
										</form>
										<br/>
										<table class="table table-striped table-bordered table-hover">
											<?php
												if($suspend == "Y")
												{
													echo "
														<tr class='info'>
															<td colspan='3'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
														</tr>
														";
												}
												else
												{
													$r = $this->m_mahasiswa->PTL_select($MhswID);
													if($r)
													{
											?>
														<tr>
															<?php
																if($r["Foto"] == "")
																{
																	$foto = "ptl_storage/foto_umum/user.jpg";
																}
																else
																{
																	$foto = "../academic/ptl_storage/foto_mahasiswa/".$r["Foto"];
																	$exist = file_exists_remote(base_url("$foto"));
																	if($exist)
																	{
																		$foto = "../academic/ptl_storage/foto_mahasiswa/".$r["Foto"];
																	}
																	else
																	{
																		$foto = "ptl_storage/foto_umum/user.jpg";
																	}
																}
															?>
															<td rowspan="5"><p align="center"><img src="<?php echo base_url("$foto"); ?>" width="200px"/></p></td>
															<td>Name</td>
															<td>:</td>
															<td><?php echo $r["Nama"]; ?></td>
														</tr>
														<tr>
															<td>Birth Place / Date</td>
															<td>:</td>
															<td><?php echo $r["TempatLahir"].", ".tgl_indo($r["TanggalLahir"]); ?></td>
														</tr>
														<tr>
															<td>Program</td>
															<td>:</td>
															<td>
																<?php
																	if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
																	if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
																	if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
																?>
															</td>
														</tr>
														<tr>
															<td>Program Study</td>
															<td>:</td>
															<td>
																<?php
																	$ProdiID =  $r["ProdiID"];
																	if($r["ProgramID"] == "SC")
																	{
																		$KursusSingkatID = $ProdiID;
																		$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
																	}
																	else
																	{
																		$p = $this->m_prodi->PTL_select($ProdiID);
																	}
																	echo $ProdiID." - ".$p["Nama"];
																?>
															</td>
														</tr>
														<tr>
															<td>Status</td>
															<td>:</td>
															<td>
																<?php
																	$StatusMhswID = $r["StatusMhswID"];
																	$s = $this->m_status->PTL_select($StatusMhswID);
																	echo $s["Nama"];
																?>
															</td>
														</tr>
											<?php
													}
												}
											?>
										</table>
										<?php
											if($suspend == "Y")
											{
												
											}
											else
											{
										?>
												<table class="table table-striped table-bordered table-hover">
													<thead>
														<tr>
															<th>CODE</th>
															<th>SUBJECT</th>
															<th>ROOM</th>
															<th>DATE</th>
															<th>TIME</th>
															<th>SUPERVISOR</th>
														</tr>
													</thead>
													<tbody>
														<?php
															$status = 0;
															if($rowrecord)
															{
																foreach($rowrecord as $row)
																{
																	$KRSID = $row->KRSID;
																	$TahunID = $row->TahunID;
																	$SubjekID = $row->SubjekID;
																	$resexam = $this->m_exam->PTL_all_select_exam_card($TahunID,$SubjekID);
																	$exam = "";
																	if($resexam)
																	{
																		foreach($resexam as $re)
																		{
																			if(($re->TahunID == $TahunID) OR ($re->TahunID2 == $TahunID))
																			{
																				if(($re->SubjekID == $SubjekID) OR ($re->SubjekID12 == $SubjekID) OR ($re->SubjekID2 == $SubjekID))
																				{
																					$ExamID = $re->ExamID;
																					$reskursi = $this->m_exam_kursi->PTL_select($ExamID,$MhswID);
																					$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																					$resdosen = $this->m_exam_dosen->PTL_select($ExamID);
																					$DosenID = "";
																					$dosen = "";
																					if($resdosen)
																					{
																						$DosenID = $resdosen["DosenID"];
																						$resdosen2 = $this->m_dosen->PTL_select($DosenID);
																						$dosen = $resdosen2["Nama"];
																					}
																					if($reskursi)
																					{
																						$data = array(
																									'ExamID' => $re->ExamID
																									);
																						$this->m_krs->PTL_update($KRSID,$data);
																						echo "
																							<tr>
																								<td title='ExamID : $re->ExamID ~ KRSID : $KRSID'>$ressubjek[SubjekKode]</td>
																								<td title='SubjekID : $row->SubjekID'>$ressubjek[Nama]</td>
																								<td>$re->RuangID</td>
																								<td>".tgl_singkat_eng($re->Tanggal)."</td>
																								<td>$re->JamMulai - $re->JamSelesai</td>
																								<td>$dosen</td>
																							</tr>
																							";
																						$status = 1;
																					}
																				}
																			}
																		}
																	}
																}
															}
															if($status == 0)
															{
																echo "<tr>
																		<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																	</tr>";
															}
														?>
													</tbody>
												</table>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>