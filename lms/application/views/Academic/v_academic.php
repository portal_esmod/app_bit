			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Academic Calendar</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Academic Calendar
								</div>
								<div class="panel-body">
									<form action="<?php echo site_url("academic/ptl_filter_acd"); ?>" method="POST">
										<select name="cekkhs" title="Filter by Period" class="form-control" onchange="this.form.submit()">
											<option value=''>-- LAST KHS --</option>
											<?php
												$cekkhs = $this->session->userdata('acd_filter_acd');
												$thn = $tahun;
												if($rowkhs)
												{
													$nkhs = 1;
													foreach($rowkhs as $rk)
													{
														$f = "";
														$cur = "";
														if($cekkhs != "")
														{
															if($tahun == $rk->TahunID)
															{
																$f = "style='background-color: #5BB734;'";
																$cur = " - Last KHS";
															}
														}
														echo "<option value='$rk->TahunID' $f";
														if($cekkhs == "")
														{
															if($tahun == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														else
														{
															if($cekkhs == $rk->TahunID)
															{
																echo "selected";
																$thn = $rk->TahunID;
															}
														}
														$TahunID = $rk->TahunID;
														$t = $this->m_year->PTL_select($TahunID);
														echo ">SEMESTER $nkhs [$rk->TahunID - $t[Nama]$cur]</option>";
														$nkhs++;
													}
												}
											?>
										</select>
										<noscript><input type="submit" value="Submit"></noscript>
									</form>
									<br/>
									<h4><?php echo $ProgramID." | ".$ProdiID." | ".$TahunKe; ?></h4>
									<?php
										$h = "-7";
										$hm = $h * 60;
										$ms = $hm * 60;
										$tanggal = gmdate("Ymd", time()-($ms));
									?>
									<div class="table-responsive">
										<table class="table">
											<tbody>
												<?php
													if($suspend == "Y")
													{
														echo "
															<tr class='info'>
																<td colspan='3'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
															</tr>
															";
													}
													else
													{
														if($ProgramID != "")
														{
															$b1 = "";
															$b2 = "";
															if($tanggal >= (str_replace("-","",$Payment)) AND ($tanggal <= str_replace("-","",$PaymentEnd)))
															{
																$b1 = "<font color='red'><b>";
																$b2 = "</font></b>";
															}
												?>
															<tr class="info">
																<td><?php echo $b1; ?>Payment Process<?php echo $b2; ?></td>
																<td>:</td>
																<td>
																	<?php
																		if($Payment == "0000-00-00")
																		{
																			echo "<font color='red'>Not set</font>";
																		}
																		else
																		{
																			$t2 = "";
																			if($PaymentEnd != "")
																			{
																				$t2 = " until ".tgl_singkat_eng($PaymentEnd);
																			}
																			echo $b1.tgl_singkat_eng($Payment).$t2.$b2;
																		}
																	?>
																</td>
															</tr>
															<tr class="info">
																<?php
																	$b1 = "";
																	$b2 = "";
																	if($tanggal >= (str_replace("-","",$Enrollment)) AND ($tanggal <= str_replace("-","",$EnrollmentEnd)))
																	{
																		$b1 = "<font color='red'><b>";
																		$b2 = "</font></b>";
																	}
																?>
																<td><?php echo $b1; ?>Enrollment Process<?php echo $b2; ?></td>
																<td>:</td>
																<td>
																	<?php
																		if($Enrollment == "0000-00-00")
																		{
																			echo "<font color='red'>Not set</font>";
																		}
																		else
																		{
																			$t2 = "";
																			if($EnrollmentEnd != "")
																			{
																				$t2 = " until ".tgl_singkat_eng($EnrollmentEnd);
																			}
																			echo $b1.tgl_singkat_eng($Enrollment).$t2.$b2;
																		}
																	?>
																</td>
															</tr>
															<tr class="info">
																<?php
																	$b1 = "";
																	$b2 = "";
																	if($tanggal >= (str_replace("-","",$Specialization)) AND ($tanggal <= str_replace("-","",$SpecializationEnd)))
																	{
																		$b1 = "<font color='red'><b>";
																		$b2 = "</font></b>";
																	}
																?>
																<td><?php echo $b1; ?>Specialization Process<?php echo $b2; ?></td>
																<td>:</td>
																<td>
																	<?php
																		if($Specialization == "0000-00-00")
																		{
																			echo "<font color='red'>Not set</font>";
																		}
																		else
																		{
																			$t2 = "";
																			if($SpecializationEnd != "")
																			{
																				$t2 = " until ".tgl_singkat_eng($SpecializationEnd);
																			}
																			echo $b1.tgl_singkat_eng($Specialization).$t2.$b2;
																		}
																	?>
																</td>
															</tr>
															<tr class="info">
																<?php
																	$b1 = "";
																	$b2 = "";
																	if($tanggal >= (str_replace("-","",$Postpone)) AND ($tanggal <= str_replace("-","",$PostponeEnd)))
																	{
																		$b1 = "<font color='red'><b>";
																		$b2 = "</font></b>";
																	}
																?>
																<td><?php echo $b1; ?>Postponed<?php echo $b2; ?></td>
																<td>:</td>
																<td>
																	<?php
																		if($Postpone == "0000-00-00")
																		{
																			echo "<font color='red'>Not set</font>";
																		}
																		else
																		{
																			$t2 = "";
																			if($PostponeEnd != "")
																			{
																				$t2 = " until ".tgl_singkat_eng($PostponeEnd);
																			}
																			echo $b1.tgl_singkat_eng($Postpone).$t2.$b2;
																		}
																	?>
																</td>
															</tr>
															<tr class="info">
																<?php
																	$b1 = "";
																	$b2 = "";
																	if($tanggal >= (str_replace("-","",$Academic)) AND ($tanggal <= str_replace("-","",$AcademicEnd)))
																	{
																		$b1 = "<font color='red'><b>";
																		$b2 = "</font></b>";
																	}
																?>
																<td><?php echo $b1; ?>Academic Process<?php echo $b2; ?></td>
																<td>:</td>
																<td>
																	<?php
																		if($Academic == "0000-00-00")
																		{
																			echo "<font color='red'>Not set</font>";
																		}
																		else
																		{
																			$t2 = "";
																			if($AcademicEnd != "")
																			{
																				$t2 = " until ".tgl_singkat_eng($AcademicEnd);
																			}
																			echo $b1.tgl_singkat_eng($Academic).$t2.$b2;
																		}
																	?>
																</td>
															</tr>
												<?php
														}
													}
												?>
												<?php
													if($suspend == "Y")
													{
													}
													else
													{
														if($rowrecord)
														{
															foreach($rowrecord as $row)
															{
																$b1 = "";
																$b2 = "";
																if($tanggal >= (str_replace("-","",$row->tanggal_mulai)) AND ($tanggal <= str_replace("-","",$row->tanggal_selesai)))
																{
																	$b1 = "<font color='red'><b>";
																	$b2 = "</font></b>";
																}
																$t2 = "";
																if($row->tanggal_selesai != "")
																{
																	$t2 = " until ".tgl_singkat_eng($row->tanggal_selesai);
																}
																echo "
																	<tr class='info'>
																		<td>$b1$row->nama$b2</td>
																		<td>:</td>
																		<td>$b1".tgl_singkat_eng($row->tanggal_mulai).$t2."$b2</td>
																	</tr>
																	";
															}
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>