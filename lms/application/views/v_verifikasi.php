<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Esmod Jakarta - Academic</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<link href="<?php echo base_url(); ?>assets/login/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/login/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/login/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/login/css/style.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/login/css/main-style.css" rel="stylesheet" />
	</head>
	<body class="body-Login-back">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 text-center logo-margin ">
					<img src="<?php echo base_url(); ?>assets/login/img/logo.png" alt="" width="350px"/>
				</div>
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">                  
						<div class="panel-heading">
							<h3 class="panel-title">Validate your OTP code</h3>
						</div>
						<div class="panel-body">
							<form role="form" action="<?php echo site_url("login/ptl_verification_sms_check"); ?>" method="POST">
								<fieldset>
									<div class="form-group">
										<input type="text" name="kode_sms" class="form-control" placeholder="ESMOD OTP" autofocus required>
										<input type="hidden" name="kode_verifikasi" value="<?php echo $kode_verifikasi; ?>">
									</div>
									<input type="submit" class="btn btn-lg btn-success btn-block" value="Confirm">
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/login/plugins/jquery-1.10.2.js"></script>
		<script src="<?php echo base_url(); ?>assets/login/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/login/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script>
			function myFunction()
			{
				var checkBox = document.getElementById("myCheck");
				var text = document.getElementById("text");
				if (checkBox.checked == true)
				{
					text.style.display = "block";
				}
				else
				{
					text.style.display = "none";
				}
			}
		</script>
	</body>
</html>