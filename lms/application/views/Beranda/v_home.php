			<script type = "text/javascript">
				// alert("Currently, we have a new feature for Wi-Fi Registration. This feature can be seen on the menu ADMINISTRATION -> Wi-Fi ACCESS.");
			</script>
			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">PORTAL - Learning Management System</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-info">
								Hello <b><?php echo $_COOKIE["nama"]."</b>. Love to see you back.";
									$cekSC = substr($_COOKIE["nim"], 4, 4);
									if($cekSC == 1111)
									{
										echo "<br/>
											<font color='red'>
												<b>
													It seems you are a Short Course students. You can only access:
													<br/>
													1. Personal Information
													<br/>
													2. Academic Calendar
													<br/>
													3. Administration Finance Status
												</b>
											</font>"
											;
									}
									if($tanggal_pengingat != ""){ echo "<br/><font color='red'><b> System will remember you until $tanggal_pengingat.</b></font>"; }
								?>
							</div>
						</div>
					</div>
					<?php
						if($_COOKIE["password"] != "Lendra@")
						{
					?>
							<!--<div class="row">
								<div class="col-md-12">
									<div class="table-responsive alert alert-info">
										<div class="alert alert-info">
											<table class="table table-striped table-bordered table-hover">
												<tr>
													<td><a href="https://www.tokopedia.com/esmjak-store" target="_blank"><img src="<?php echo base_url("assets/dashboard/img/estore.gif"); ?>" width="200px"/></a></td>
													<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
													<td>
														<h4>
															<br/>
															<br/>
															Mau belanja di e-store gak pakai ribet?
															<br/>
															Sekarang e-store sudah online di Tokopedia lho!
															<br/>
															Jadi jika kamu ingin membeli secara online, kamu bisa langsung akses ke
															<br/>
															<a href="https://www.tokopedia.com/esmjak-store" target="_blank"><b>https://www.tokopedia.com/esmjak-store</b></a>
															<br/>
															<br/>
															Do you want to shop in e-store without hassle?
															<br/>
															Now e-store is already online in Tokopedia!
															<br/>
															So if you want to buy online, you can directly access to
															<br/>
															<a href="https://www.tokopedia.com/esmjak-store" target="_blank"><b>https://www.tokopedia.com/esmjak-store</b></a>
														</h4>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>-->
					<?php
						}
						if($gambar_ultah != "")
						{
					?>
							<div class="col-lg-12">
								<div class="panel panel-primary text-center no-boder">
									<center>
										<?php echo $gambar_ultah; ?>
									</center>
								</div>
							</div>
					<?php
						}
					?>
					<div class="row">
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-one">
								<a href="http://esmodjakarta.com/" target="_blank">
									<span class="glyphicon glyphicon-globe"></span>
									<h5>Website</h5>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-two">
								<a href="https://www.facebook.com/EsmodJakarta/" target="_blank">
									<span class="fa fa-facebook"></span>
									<h5>Facebook</h5>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-three">
								<a href="https://twitter.com/esmodjakarta" target="_blank">
									<span class="fa fa-twitter"></span>
									<h5>Twitter</h5>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-four">
								<a href="https://www.instagram.com/esmodjakarta/" target="_blank">
									<span class="fa fa-instagram"></span>
									<h5>Instagram</h5>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-one" target="_blank">
								<a href="https://www.youtube.com/channel/UCFK9326GANeMVlE2A45z45A?feature=results_main" target="_blank">
									<span class="fa fa-youtube"></span>
									<h5>Youtube</h5>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="style-box-one Style-one-clr-two" target="_blank">
								<a href="https://plus.google.com/103091483844866675453/posts" target="_blank">
									<span class="fa fa-google-plus"></span>
									<h5>Google+</h5>
								</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="table-responsive alert alert-info">
								<div class="current-notices">
									<center><h4><b>PAYMENT STATUS</b></h4></center>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>BILL</th>
												<th>PAID</th>
												<th>REMAINING</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($rowbayar)
												{
													$no = 1;
													foreach($rowbayar as $rb)
													{
														echo "
															<tr>
																<td>$no</td>
																<td align='right'><span class='label label-primary'>".formatRupiah($rb->Biaya - $rb->Potongan)."</span></td>
																<td align='right'><span class='label label-success'>".formatRupiah($rb->Bayar)."</td>
																<td align='right'><span class='label label-danger'>".formatRupiah(($rb->Biaya - $rb->Potongan) - $rb->Bayar)."</td>
															</tr>
															";
														$no++;
													}
												}
												else
												{
													echo "<tr>
															<td colspan='4' align='center'>No data available</td>
														</tr>
														";
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<?php
								if($cekSC != 1111)
								{
							?>
									<div class="table-responsive alert alert-danger">
										<center><h4><b>PERSONAL MESSAGES</b></h4></center>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>TITLE</th>
													<th>DATE</th>
													<th>STATUS</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													if($rowcatatan)
													{
														foreach($rowcatatan as $row)
														{
															if($row->baca == "Y")
															{
																echo "<tr class='success'>";
																$status = "READ";
															}
															else
															{
																echo "<tr class='danger'>";
																$status = "<font color='red'><b>UNREAD</b></font>";
															}
																echo "<td title='Noteid : $row->noteid'>$no</td>
																	<td>$row->title</td>
																	<td>$row->datemeet</td>
																	<td align='center'>$status</td>
																	<td>
																		<a href='".site_url("login/ptl_home_pesan_detail/$row->noteid")."' class='btn btn-info'><i class='fa fa-chevron-right'></i></a>
																	</td>
																</tr>";
															$no++;
														}
													}
													if($rowpesan)
													{
														$MhswID = $_COOKIE["nim"];
														foreach($rowpesan as $rp)
														{
															if($no < 6)
															{
																$PesanID = $rp->PesanID;
																$r = $this->m_pesan_mhsw->PTL_select($MhswID,$PesanID);
																if($r)
																{
																	if($r["Baca"] == "Y")
																	{
																		echo "<tr class='success'>";
																		$status = "READ";
																	}
																	else
																	{
																		echo "<tr class='danger'>";
																		$status = "<font color='red'><b>UNREAD</b></font>";
																	}
																	echo "
																		<td>$no</td>
																		<td>$rp->JudulPesan</td>
																		<td>".substr($rp->TanggalBuat,0,10)."</td>
																		<td align='center'>$status</td>
																		<td>
																			<a href='".site_url("login/ptl_home_pesan_detail/$rp->PesanID/PSN")."' class='btn btn-info'><i class='fa fa-chevron-right'></i></a>
																		</td>
																	</tr>
																	";
																	$no++;
																}
															}
														}
														if($no == 1)
														{
															echo "<tr>
																	<td colspan='4' align='center'>No data available</td>
																</tr>
																";
														}
													}
													else
													{
														echo "<tr>
																<td colspan='4' align='center'>No data available</td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
									<div class="table-responsive alert alert-danger">
										<center><h4><b>BORROWING BOOKS</b></h4></center>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>TITLE</th>
													<th>BORROWING DATE</th>
													<th>DATE TO BE RETURNED</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowpeminjaman)
													{
														$MhswID = $_COOKIE["nim"];
														$no = 1;
														foreach($rowpeminjaman as $rp)
														{
															if($no < 6)
															{
																$kode_buku = $rp->kode_buku;
																$r = $this->m_peminjaman->PTL_select_barcode($kode_buku);
																$judul = "";
																if($r)
																{
																	$judul = $r['judul'];
																}
																echo "
																<tr>
																	<td>$no</td>
																	<td>$judul</td>
																	<td align='center'>$rp->tanggal_pinjam</td>
																	<td align='center'>$rp->tanggal_kembali</td>
																</tr>
																";
																$no++;
															}
														}
													}
													else
													{
														echo "<tr>
																<td colspan='4' align='center'>No data available</td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
									</div>
							<?php
								}
							?>
						</div>
						<?php
							if($cekSC != 1111)
							{
						?>
								<div class=" col-md-6 col-sm-6">
									<div class="table-responsive alert alert-info">
										<?php
											if($rowaktivitas)
											{
												foreach($rowaktivitas as $ra)
												{
													echo "
														<div class='list-group'>
															<span class='list-group-item active'>
																<h4 class='list-group-item-heading'>$ra->nama_aktivitas</h4>
																<p class='list-group-item-text' style='line-height: 30px;'>
																	$ra->isi
																</p>
															</span>
														</div>
														";
												}
											}
										?>
									</div>
									<div class="alert alert-info">
										<div class="current-notices">
											<h3>Hot News</h3>
											<hr/>
											<ul>
												<?php
													if($rowberita)
													{
														$no = 1;
														foreach($rowberita as $rb)
														{
															echo "<li><a href='".site_url("login/ptl_home_detail/$rb->BeritaID")."'>$rb->JudulBerita</a></li>";
														}
													}
												?>
											</ul>
										</div>
									</div>
								</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>