			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Rating for Teacher</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Question
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>The data you enter will be evaluating the performance of teachers.
										</div>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><b>Subject Code</b></th>
													<th><b>Subject ID</b></th>
													<th><b>Year ID</b></th>
													<th><b>Name</b></th>
													<th><b>Start Date</b></th>
													<th><b>End Date</b></th>
													<th><b>Session</b></th>
												</tr>
											</thead>
											<tbody>
												<?php
													$MhswID = $_COOKIE["nim"];
													$JumlahDosen = 0;
													$JadwalID = "";
													$SubjekID = "";
													if($rowrecord2)
													{
														$n = 1;
														foreach($rowrecord2 as $r)
														{
															$JID = $r->JadwalID;
															$SID = $r->SubjekID;
															$resgruprating = $this->m_rating_grup->PTL_select_spesifik($MhswID,$TahunID,$JID,$SID);
															if(!$resgruprating)
															{
																if($n == 1)
																{
																	$JadwalID = $r->JadwalID;
																	$SubjekID = $r->SubjekID;
																	$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
																	$TglMulai = "";
																	$TglSelesai = "";
																	$Kehadiran = "";
																	if($resjadwal)
																	{
																		$TglMulai = $resjadwal['TglMulai'];
																		$TglSelesai = $resjadwal['TglSelesai'];
																		$Kehadiran = $resjadwal['Kehadiran'];
																	}
																	$respertemuan = $this->m_presensi->PTL_all_select($JadwalID);
																	$JumlahPertemuan = count($respertemuan);
																	echo "
																		<tr>
																			<td title='$r->KHSID'><b>$r->SubjekKode</b></td>
																			<td><b><font color='red'>$r->SubjekID</b></td>
																			<td><b><font color='red'>$r->TahunID</b></td>
																			<td><b><font color='red'>$r->Nama</b></td>
																			<td><b><font color='red'>".tgl_singkat_eng($TglMulai)."</b></td>
																			<td><b><font color='red'>".tgl_singkat_eng($TglSelesai)."</b></td>
																			<td><b><font color='red'>$JumlahPertemuan</b></td>
																		<tr>
																		";
																	if($resjadwal)
																	{
																		$DosenID1 = $resjadwal['DosenID'];
																		$DosenID = $resjadwal['DosenID'];
																		$resakun1 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto1 = "";
																		$foto_preview1 = "";
																		$Photo1 = "";
																		if($resakun1)
																		{
																			$foto1 = "../hris/ptl_storage/foto_karyawan/$resakun1[foto]";
																			$exist = file_exists_remote(base_url("$foto1"));
																			if($exist)
																			{
																				$foto1 = $foto1;
																				$source_photo = base_url("$foto1");
																				$info = pathinfo($source_photo);
																				$foto_preview1 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview1"));
																				if($exist1)
																				{
																					$foto_preview1 = $foto_preview1;
																				}
																				else
																				{
																					$foto_preview1 = $foto1;
																				}
																			}
																			else
																			{
																				$foto1 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview1 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo1 = "<a class='fancybox' title='$resakun1[nama]' href='".base_url("$foto1")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview1")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen1 = $resakun1['nama'];
																			$JumlahDosen++;
																		}
																		$DosenID2 = $resjadwal['DosenID2'];
																		$DosenID = $resjadwal['DosenID2'];
																		$resakun2 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto2 = "";
																		$foto_preview2 = "";
																		$Photo2 = "";
																		if($resakun2)
																		{
																			$foto2 = "../hris/ptl_storage/foto_karyawan/$resakun2[foto]";
																			$exist = file_exists_remote(base_url("$foto2"));
																			if($exist)
																			{
																				$foto2 = $foto2;
																				$source_photo = base_url("$foto2");
																				$info = pathinfo($source_photo);
																				$foto_preview2 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview2"));
																				if($exist1)
																				{
																					$foto_preview2 = $foto_preview2;
																				}
																				else
																				{
																					$foto_preview2 = $foto2;
																				}
																			}
																			else
																			{
																				$foto2 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview2 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo2 = "<a class='fancybox' title='$resakun2[nama]' href='".base_url("$foto2")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview2")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen2 = $resakun2['nama'];
																			$JumlahDosen++;
																		}
																		$DosenID3 = $resjadwal['DosenID3'];
																		$DosenID = $resjadwal['DosenID3'];
																		$resakun3 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto3 = "";
																		$foto_preview3 = "";
																		$Photo3 = "";
																		if($resakun3)
																		{
																			$foto3 = "../hris/ptl_storage/foto_karyawan/$resakun3[foto]";
																			$exist = file_exists_remote(base_url("$foto3"));
																			if($exist)
																			{
																				$foto3 = $foto3;
																				$source_photo = base_url("$foto3");
																				$info = pathinfo($source_photo);
																				$foto_preview3 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview3"));
																				if($exist1)
																				{
																					$foto_preview3 = $foto_preview3;
																				}
																				else
																				{
																					$foto_preview3 = $foto3;
																				}
																			}
																			else
																			{
																				$foto3 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview3 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo3 = "<a class='fancybox' title='$resakun3[nama]' href='".base_url("$foto3")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview3")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen3 = $resakun3['nama'];
																			$JumlahDosen++;
																		}
																		$DosenID4 = $resjadwal['DosenID4'];
																		$DosenID = $resjadwal['DosenID4'];
																		$resakun4 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto4 = "";
																		$foto_preview4 = "";
																		$Photo4 = "";
																		if($resakun4)
																		{
																			$foto4 = "../hris/ptl_storage/foto_karyawan/$resakun4[foto]";
																			$exist = file_exists_remote(base_url("$foto4"));
																			if($exist)
																			{
																				$foto4 = $foto4;
																				$source_photo = base_url("$foto4");
																				$info = pathinfo($source_photo);
																				$foto_preview4 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview4"));
																				if($exist1)
																				{
																					$foto_preview4 = $foto_preview4;
																				}
																				else
																				{
																					$foto_preview4 = $foto4;
																				}
																			}
																			else
																			{
																				$foto4 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview4 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo4 = "<a class='fancybox' title='$resakun4[nama]' href='".base_url("$foto4")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview4")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen4 = $resakun4['nama'];
																			$JumlahDosen++;
																		}
																		$DosenID5 = $resjadwal['DosenID5'];
																		$DosenID = $resjadwal['DosenID5'];
																		$resakun5 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto5 = "";
																		$foto_preview5 = "";
																		$Photo5 = "";
																		if($resakun5)
																		{
																			$foto5 = "../hris/ptl_storage/foto_karyawan/$resakun5[foto]";
																			$exist = file_exists_remote(base_url("$foto5"));
																			if($exist)
																			{
																				$foto5 = $foto5;
																				$source_photo = base_url("$foto5");
																				$info = pathinfo($source_photo);
																				$foto_preview5 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview5"));
																				if($exist1)
																				{
																					$foto_preview5 = $foto_preview5;
																				}
																				else
																				{
																					$foto_preview5 = $foto5;
																				}
																			}
																			else
																			{
																				$foto5 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview5 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo5 = "<a class='fancybox' title='$resakun5[nama]' href='".base_url("$foto5")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview5")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen5 = $resakun5['nama'];
																			$JumlahDosen++;
																		}
																		$DosenID6 = $resjadwal['DosenID6'];
																		$DosenID = $resjadwal['DosenID6'];
																		$resakun6 = $this->m_akun->PTL_select_dosen($DosenID);
																		$foto6 = "";
																		$foto_preview6 = "";
																		$Photo6 = "";
																		if($resakun6)
																		{
																			$foto6 = "../hris/ptl_storage/foto_karyawan/$resakun6[foto]";
																			$exist = file_exists_remote(base_url("$foto6"));
																			if($exist)
																			{
																				$foto6 = $foto6;
																				$source_photo = base_url("$foto6");
																				$info = pathinfo($source_photo);
																				$foto_preview6 = "../hris/ptl_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
																				$exist1 = file_exists_remote(base_url("$foto_preview6"));
																				if($exist1)
																				{
																					$foto_preview6 = $foto_preview6;
																				}
																				else
																				{
																					$foto_preview6 = $foto6;
																				}
																			}
																			else
																			{
																				$foto6 = "ptl_storage/foto_umum/user.jpg";
																				$foto_preview6 = "ptl_storage/foto_umum/user.jpg";
																			}
																			$Photo6 = "<a class='fancybox' title='$resakun6[nama]' href='".base_url("$foto6")."' data-fancybox-group='gallery' >
																						<img class='img-polaroid' src='".base_url("$foto_preview6")."' width='120px' alt=''/>
																					</a>";
																			$NamaDosen6 = $resakun6['nama'];
																			$JumlahDosen++;
																		}
																	}
																	$n++;
																}
															}
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='7'><center><font color='red'><b>NO SUBJECT AVAILABLE</b></font></center></td>
															</tr>
															";
													}
												?>
											</tbody>
										</table>
										<table class="table table-striped table-bordered table-hover">
											<tbody>
												<?php
													if($rowrecord2)
													{
														if($Photo1 != "")
														{
															$A1 = "A";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A1.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A1.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo1<br/>$DosenID1 - $NamaDosen1</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID1;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A1$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A1$no' name='rating$A1$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck10/>
																					<label class='full' for='star53$A1$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A1$no' name='rating$A1$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck9/>
																					<label class='half' for='star4half3$A1$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A1$no' name='rating$A1$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck8/>
																					<label class='full' for='star43$A1$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A1$no' name='rating$A1$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck7/>
																					<label class='half' for='star3half3$A1$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A1$no' name='rating$A1$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck6/>
																					<label class='full' for='star33$A1$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A1$no' name='rating$A1$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck5/>
																					<label class='half' for='star2half3$A1$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A1$no' name='rating$A1$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck4/>
																					<label class='full' for='star23$A1$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A1$no' name='rating$A1$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck3/>
																					<label class='half' for='star1half3$A1$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A1$no' name='rating$A1$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck2/>
																					<label class='full' for='star13$A1$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A1$no' name='rating$A1$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID1' $Ck1/>
																					<label class='half' for='starhalf3$A1$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A1$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
														if($Photo2 != "")
														{
															$A2 = "B";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A2.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A2.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo2<br/>$DosenID2 - $NamaDosen2</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID2;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A2$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A2$no' name='rating$A2$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck10/>
																					<label class='full' for='star53$A2$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A2$no' name='rating$A2$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck9/>
																					<label class='half' for='star4half3$A2$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A2$no' name='rating$A2$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck8/>
																					<label class='full' for='star43$A2$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A2$no' name='rating$A2$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck7/>
																					<label class='half' for='star3half3$A2$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A2$no' name='rating$A2$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck6/>
																					<label class='full' for='star33$A2$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A2$no' name='rating$A2$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck5/>
																					<label class='half' for='star2half3$A2$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A2$no' name='rating$A2$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck4/>
																					<label class='full' for='star23$A2$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A2$no' name='rating$A2$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck3/>
																					<label class='half' for='star1half3$A2$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A2$no' name='rating$A2$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck2/>
																					<label class='full' for='star13$A2$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A2$no' name='rating$A2$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID2' $Ck1/>
																					<label class='half' for='starhalf3$A2$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A2$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
														if($Photo3 != "")
														{
															$A3 = "C";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A3.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A3.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo3<br/>$DosenID3 - $NamaDosen3</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID3;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A3$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A3$no' name='rating$A3$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck10/>
																					<label class='full' for='star53$A3$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A3$no' name='rating$A3$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck9/>
																					<label class='half' for='star4half3$A3$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A3$no' name='rating$A3$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck8/>
																					<label class='full' for='star43$A3$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A3$no' name='rating$A3$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck7/>
																					<label class='half' for='star3half3$A3$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A3$no' name='rating$A3$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck6/>
																					<label class='full' for='star33$A3$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A3$no' name='rating$A3$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck5/>
																					<label class='half' for='star2half3$A3$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A3$no' name='rating$A3$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck4/>
																					<label class='full' for='star23$A3$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A3$no' name='rating$A3$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck3/>
																					<label class='half' for='star1half3$A3$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A3$no' name='rating$A3$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck2/>
																					<label class='full' for='star13$A3$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A3$no' name='rating$A3$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID3' $Ck1/>
																					<label class='half' for='starhalf3$A3$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A3$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
														if($Photo4 != "")
														{
															$A4 = "D";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A4.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A4.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo4<br/>$DosenID4 - $NamaDosen4</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID4;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A4$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A4$no' name='rating$A4$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck10/>
																					<label class='full' for='star53$A4$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A4$no' name='rating$A4$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck9/>
																					<label class='half' for='star4half3$A4$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A4$no' name='rating$A4$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck8/>
																					<label class='full' for='star43$A4$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A4$no' name='rating$A4$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck7/>
																					<label class='half' for='star3half3$A4$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A4$no' name='rating$A4$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck6/>
																					<label class='full' for='star33$A4$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A4$no' name='rating$A4$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck5/>
																					<label class='half' for='star2half3$A4$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A4$no' name='rating$A4$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck4/>
																					<label class='full' for='star23$A4$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A4$no' name='rating$A4$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck3/>
																					<label class='half' for='star1half3$A4$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A4$no' name='rating$A4$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck2/>
																					<label class='full' for='star13$A4$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A4$no' name='rating$A4$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID4' $Ck1/>
																					<label class='half' for='starhalf3$A4$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A4$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
														if($Photo5 != "")
														{
															$A5 = "E";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A5.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A5.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo5<br/>$DosenID5 - $NamaDosen5</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID5;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A5$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A5$no' name='rating$A5$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck10/>
																					<label class='full' for='star53$A5$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A5$no' name='rating$A5$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck9/>
																					<label class='half' for='star4half3$A5$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A5$no' name='rating$A5$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck8/>
																					<label class='full' for='star43$A5$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A5$no' name='rating$A5$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck7/>
																					<label class='half' for='star3half3$A5$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A5$no' name='rating$A5$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck6/>
																					<label class='full' for='star33$A5$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A5$no' name='rating$A5$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck5/>
																					<label class='half' for='star2half3$A5$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A5$no' name='rating$A5$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck4/>
																					<label class='full' for='star23$A5$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A5$no' name='rating$A5$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck3/>
																					<label class='half' for='star1half3$A5$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A5$no' name='rating$A5$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck2/>
																					<label class='full' for='star13$A5$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A5$no' name='rating$A5$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID5' $Ck1/>
																					<label class='half' for='starhalf3$A5$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A5$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
														if($Photo6 != "")
														{
															$A6 = "F";
												?>
															<script>
																<?php
																	if($rowrecord)
																	{
																		$n = 1;
																		foreach($rowrecord as $row)
																		{
																?>
																			$(document).ready(function ()
																			{
																				$("#demo<?php echo $A6.$n; ?> .stars").click(function ()
																				{
																					$.post('<?php echo site_url("login/ptl_rating"); ?>',{rate:$(this).val()},function(d)
																					{
																						// if(d>0)
																						// {
																							// alert('You already rated');
																						// }else{
																							// alert('Thanks For Rating');
																						// }
																					});
																					var label = $("label[for='" + $(this).attr('id') + "']");
																					$("#feedback<?php echo $A6.$n; ?>").text(label.attr('title'));
																					$(this).attr('id',"checked");
																				});
																			});
																<?php
																			$n++;
																		}
																	}
																?>
															</script>
												<?php
															if($rowrecord)
															{
																echo "
																	<tr>
																		<th colspan='4'><p align='center'><b>$Photo6<br/>$DosenID6 - $NamaDosen6</b></p></th>
																	</tr>
																	<tr>
																		<th><b>#</b></th>
																		<th><b>Question</b></th>
																		<th><b>Give Star</b></th>
																		<th><b>Description</b></th>
																	</tr>
																	";
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$DosenID = $DosenID6;
																	$id_rating_question = $row->id_rating_question;
																	$resAnswer = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
																	$Ck1 = "";
																	$Ck2 = "";
																	$Ck3 = "";
																	$Ck4 = "";
																	$Ck5 = "";
																	$Ck6 = "";
																	$Ck7 = "";
																	$Ck8 = "";
																	$Ck9 = "";
																	$Ck10 = "";
																	if($resAnswer)
																	{
																		if($resAnswer['rate'] == '0.5'){ $Ck1 = "checked"; }
																		if($resAnswer['rate'] == '1.0'){ $Ck2 = "checked"; }
																		if($resAnswer['rate'] == '1.5'){ $Ck3 = "checked"; }
																		if($resAnswer['rate'] == '2.0'){ $Ck4 = "checked"; }
																		if($resAnswer['rate'] == '2.5'){ $Ck5 = "checked"; }
																		if($resAnswer['rate'] == '3.0'){ $Ck6 = "checked"; }
																		if($resAnswer['rate'] == '3.5'){ $Ck7 = "checked"; }
																		if($resAnswer['rate'] == '4.0'){ $Ck8 = "checked"; }
																		if($resAnswer['rate'] == '4.5'){ $Ck9 = "checked"; }
																		if($resAnswer['rate'] == '5.0'){ $Ck10 = "checked"; }
																	}
																	echo "
																		<tr>
																			<td><b>$no</b></td>
																			<td><b><font color='red'>$row->pertanyaan</b></td>
																			<td>
																				<fieldset id='demo$A6$no' class='rating'>
																					<input class='stars' type='radio' id='star53$A6$no' name='rating$A6$no' value='5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck10/>
																					<label class='full' for='star53$A6$no' title='Excellent - 5 stars'></label>
																					<input class='stars' type='radio' id='star4half3$A6$no' name='rating$A6$no' value='4.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck9/>
																					<label class='half' for='star4half3$A6$no' title='Very Good - 4.5 stars'></label>
																					<input class='stars' type='radio' id='star43$A6$no' name='rating$A6$no' value='4-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck8/>
																					<label class='full' for='star43$A6$no' title='Pretty good - 4 stars'></label>
																					<input class='stars' type='radio' id='star3half3$A6$no' name='rating$A6$no' value='3.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck7/>
																					<label class='half' for='star3half3$A6$no' title='Above Average - 3.5 stars'></label>
																					<input class='stars' type='radio' id='star33$A6$no' name='rating$A6$no' value='3-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck6/>
																					<label class='full' for='star33$A6$no' title='Average - 3 stars'></label>
																					<input class='stars' type='radio' id='star2half3$A6$no' name='rating$A6$no' value='2.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck5/>
																					<label class='half' for='star2half3$A6$no' title='Below Average - 2.5 stars'></label>
																					<input class='stars' type='radio' id='star23$A6$no' name='rating$A6$no' value='2-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck4/>
																					<label class='full' for='star23$A6$no' title='Fairly Bad - 2 stars'></label>
																					<input class='stars' type='radio' id='star1half3$A6$no' name='rating$A6$no' value='1.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck3/>
																					<label class='half' for='star1half3$A6$no' title='Bad - 1.5 stars'></label>
																					<input class='stars' type='radio' id='star13$A6$no' name='rating$A6$no' value='1-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck2/>
																					<label class='full' for='star13$A6$no' title='Very Bad - 1 star'></label>
																					<input class='stars' type='radio' id='starhalf3$A6$no' name='rating$A6$no' value='0.5-$row->id_rating_question-$row->id_rating_question_grup-$TahunID-$KHSID-$ProgramID-$ProdiID-$TahunKe-$KelasID-$Sesi-$JadwalID-$SubjekID-$DosenID6' $Ck1/>
																					<label class='half' for='starhalf3$A6$no' title='Poor - 0.5 stars'></label>
																				</fieldset>
																			</td>
																			<td><div id='feedback$A6$no'></div></td>
																		</tr>
																		";
																	$no++;
																}
															}
															else
															{
																echo "
																	<tr>
																		<td colspan='3'><center><font color='red'><b>NO QUESTION AVAILABLE. PLEASE CONTACT ACADEMIC COUNSELOR.</b></font></center></td>
																	</tr>
																	";
															}
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='3'><center><font color='red'><b>NO SUBJECT AVAILABLE</b></font></center></td>
															</tr>
															";
													}
													$JumlahSoal = count($rowrecord);
												?>
											</tbody>
										</table>
										<?php
											if($rowrecord)
											{
										?>
												<center>
													<a href="<?php echo site_url("login/ptl_rating_grup/$TahunID/$KHSID/$JadwalID/$SubjekID/$ProgramID/$ProdiID/$TahunKe/$Sesi/$KelasID/$JumlahDosen/$JumlahSoal/$id_rating_question_grup"); ?>" class="btn btn-primary">Save</a>
												</center>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>