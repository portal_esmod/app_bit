			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Personal Information</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Profile Photo
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php echo form_open_multipart('login/ptl_photo',array('name' => 'contoh','class' => 'form-horizontal')); ?>
											<table class="table">
												<tbody>
													<tr>
														<td><label><font color="green">Student Identification Number (SIN)</font></label></td>
														<td>
															<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
															<input type="hidden" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/>
															<input type="hidden" name="Email" value="<?php echo $Email; ?>" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">PHOTO</font></label></td>
														<td>
															<input type="file" name="userfile" class="form-control" accept="image/*" required/>
															<font color="blue">Maximum file 2MB</font>
														</td>
													</tr>
													<tr>
														<td></td>
														<td>
															<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to set your profile photo?')">SAVE</button>
														</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td colspan="2">If you have problem with PORTAL LMS, please email to IT Developer : <a href="mailto:lendra@esmodjakarta.com">lendra@esmodjakarta.com</a></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>