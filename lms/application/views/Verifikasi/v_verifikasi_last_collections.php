			<script>
				function uploadFile()
				{
					var file = document.getElementById("fileku").files[0];
					var formdata = new FormData();
					formdata.append("userfile", file);
					var ajax = new XMLHttpRequest();
					ajax.upload.addEventListener("progress", progressHandler, false);
					ajax.open("POST", "<?php echo base_url(); ?>index.php/login/ptl_upload_last_collections", true);
					ajax.send(formdata);
				}
				
				function progressHandler(event)
				{
					var percent = (event.loaded / event.total) * 100;
					document.getElementById("progressBar").value = Math.round(percent);
					document.getElementById("status").innerHTML = Math.round(percent)+"% has been uploaded";
					document.getElementById("total").innerHTML = "Has been uploaded "+event.loaded+" bytes from "+event.total;
					if(percent == 100)
					{
						document.getElementById("result").innerHTML = "<br/><br/><a href='<?php echo site_url("login/ptl_home"); ?>' class='btn btn-primary'>SAVE</a>";
						document.getElementById('hidebutton').style.display = "none";
					}
				}
			</script>
			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Last Collections</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail Last Collections
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<font color="red"><b>It looks like you have graduated from ESMOD Jakarta. You are required to upload your last collections to LMS. File include your profile picture and your collections. Please make it in one file (winrar-.rar or winzip-.zip). You only have one chance.</b></font>
										</div>
										<form id="upload_form" enctype="multipart/form-data">
											<table class="table">
												<tbody>
													<tr>
														<td><label><font color="green">Student Identification Number (SIN)</font></label></td>
														<td><input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="green">Name</font></label></td>
														<td><input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="green">KHSID</font></label></td>
														<td><input readonly type="text" name="KHSID" value="<?php echo $KHSID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="red">UPLOAD YOUR LAST COLLECTIONS</font></label></td>
														<td>
															<input type="file" name="userfile" id="fileku" class="form-control" required>
															<progress id="progressBar" class="form-control" value="0" max="100"></progress>
															<h3 id="status"></h3>
															<p id="total"></p>
														</td>
													</tr>
													<tr>
														<td></td>
														<td>
															<input type="button" value="Upload File" id="hidebutton" class="btn btn-warning" onclick="uploadFile(); return confirm('Are you sure want to upload this FILE?\n\nTHIS ACTION CAN NOT BE RESTORED.');">
														</td>
													</tr>
													<tr>
														<td></td>
														<td><span id="result"></span></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>