			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Personal Information</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail Personal Information
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<font color="red"><b>Please upload your Identity File (KTP, KITAS, PASPORT), Parents Identity File (KTP, KITAS, PASPORT) and Last Diploma File (Ijazah SMA / S1 / Other). Files will be checked by Academic Team.</b></font>
										</div>
										<?php echo form_open_multipart('login/ptl_verification_file',array('name' => 'contoh','class' => 'form-horizontal')); ?>
											<table class="table">
												<tbody>
													<tr>
														<td><label><font color="green">Student Identification Number (SIN)</font></label></td>
														<td><input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="red">Identity File</font></label></td>
														<td>
															<input type="file" name="gambar1">
															<font color="blue">If you want to upload some files, please make .rar / .zip file.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Parents Identity File</font></label></td>
														<td>
															<input type="file" name="gambar2">
															<font color="blue">If you want to upload some files, please make .rar / .zip file.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Last Diploma File</font></label></td>
														<td>
															<input type="file" name="gambar3">
															<font color="blue">If you want to upload some files, please make .rar / .zip file.</font>
														</td>
													</tr>
													<tr>
														<td></td>
														<td>
															<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to upload your FILE?')">SAVE</button>
														</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>