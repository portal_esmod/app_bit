			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Mobile Phone Verification</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Mobile Phone Verification
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>The data you enter must be valid. A verification code will be sent to your mobile phone to ensure that phone is valid.
										</div>
										<?php
											if($handphone_verifikasi_kode == "")
											{
										?>
												<?php echo form_open_multipart('verification/ptl_auth_update',array('name' => 'contoh','class' => 'form-horizontal')); ?>
													<table class="table">
														<tbody>
															<tr>
																<td><label><font color="red">Mobile Phone</font></label></td>
																<td>
																	<input type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control" required/>
																	<input type="hidden" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
																	<input type="hidden" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/>
																	<font color="blue">ESMOD verification code will sent to this number.</font>
																</td>
															</tr>
															<tr>
																<td></td>
																<td>
																	<button type="reset" class="btn btn-danger">RESET</button>
																	<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to save your data?\nMake sure that the data you entered is valid.\n\nTHIS ACTION CAN NOT BE RESTORED.')">SAVE</button>
																</td>
															</tr>
														</tbody>
													</table>
												</form>
										<?php
											}
											else
											{
												$hp = substr($Handphone,0,3)."xxxxxxxx".substr($Handphone,-1);
										?>
												<?php echo form_open_multipart('verification/ptl_auth_verifikasi',array('name' => 'contoh','class' => 'form-horizontal')); ?>
													<table class="table">
														<tbody>
															<tr>
																<td><label><font color="red">ESMOD Verification Code</font></label></td>
																<td>
																	<input type="number" name="otp" class="form-control" required/>
																	<input type="hidden" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
																	<input type="hidden" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/>
																	<input type="hidden" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control"/>
																	<input type="hidden" name="Email" value="<?php echo $Email; ?>" class="form-control"/>
																	<input type="hidden" name="handphone_verifikasi_kode" value="<?php echo $handphone_verifikasi_kode; ?>" class="form-control"/>
																	<font color="" size=2>
																		Verification code was sent to <i><b><?php echo $hp; ?></b></i>. <a href="<?php echo site_url("verification/ptl_auth_reset/$MhswID"); ?>">Edit</a>
																		<br/>
																		<br/>
																		Have a problem? Please email to : lendra@esmodjakarta.com
																	</font>
																</td>
															</tr>
															<tr>
																<td></td>
																<td>
																	<button type="reset" class="btn btn-danger">RESET</button>
																	<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to save your data?\nMake sure that the data you entered is valid.\n\nTHIS ACTION CAN NOT BE RESTORED.')">SAVE</button>
																</td>
															</tr>
														</tbody>
													</table>
												</form>
										<?php
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>