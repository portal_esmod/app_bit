			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Personal Information</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail Personal Information
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>The data you enter must be valid. You only have one chance. A verification code will be sent to your email to ensure that email is valid.
										</div>
										<?php echo form_open_multipart('login/authentification_profile',array('name' => 'contoh','class' => 'form-horizontal')); ?>
											<table class="table">
												<tbody>
													<tr>
														<td><label><font color="green">Student Identification Number (SIN)</font></label></td>
														<td><input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="red">Name</font></label></td>
														<td><input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required/>
															<font color="blue">According KTP/Passport/KITAS.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Marketing</font></label></td>
														<td>
															<select name="PresenterID" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($marketing)
																	{
																		foreach($marketing as $row)
																		{
																			$word = explode(" ",$row->nama);
																			echo "<option value='".$word[0]."'";
																			if($PresenterID == $word[0])
																			{
																				echo "selected";
																			}
																			echo ">".$word[0]." - $row->nama</option>";
																		}
																	}
																?>
															</select>
															<font color="blue">Marketing name related to you when you register in ESMOD.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Place of Birth</font></label></td>
														<td><input type="text" name="TempatLahir" value="<?php echo $TempatLahir; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Date of Birth</font></label></td>
														<td><input type="text" name="TanggalLahir" value="<?php echo $TanggalLahir; ?>" id="datepicker1" placeholder="yyyy-mm-dd" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Gender</font></label></td>
														<td>
															<select name="Kelamin" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<option value="P" <?php if($Kelamin == "P") { echo "selected"; } ?>>Man</option>
																<option value="W" <?php if($Kelamin == "W") { echo "selected"; } ?>>Woman</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Citizen</font></label></td>
														<td>
															<select name="WargaNegara" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<option value="WNI" <?php if($WargaNegara == "WNI") { echo "selected"; } ?>>WNI</option>
																<option value="WNA" <?php if($WargaNegara == "WNA") { echo "selected"; } ?>>WNA</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Nationality</font></label></td>
														<td><input type="text" name="Kebangsaan" value="<?php echo $Kebangsaan; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Religion</font></label></td>
														<td>
															<select name="Agama" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($Agama == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Civil Status</font></label></td>
														<td>
															<select name="StatusSipil" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowsipil)
																	{
																		foreach($rowsipil as $row)
																		{
																			echo "<option value='$row->StatusSipil'";
																			if($StatusSipil == $row->StatusSipil)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Last Education</font></label></td>
														<td>
															<select name="PendidikanTerakhir" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanTerakhir == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Graduated Year</font></label></td>
														<td>
															<input type="number" name="TahunLulus" value="<?php echo $TahunLulus; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Body Height</font></label></td>
														<td>
															<input type="number" name="TinggiBadan" value="<?php echo $TinggiBadan; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Body Weight</font></label></td>
														<td>
															<input type="number" name="BeratBadan" value="<?php echo $BeratBadan; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Child</font></label></td>
														<td>
															<input type="number" name="AnakKe" value="<?php echo $AnakKe; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Number of Siblings</font></label></td>
														<td>
															<input type="number" name="JumlahSaudara" value="<?php echo $JumlahSaudara; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Current Address</font></label></td>
														<td><textarea name="Alamat" class="form-control" required><?php echo $Alamat; ?></textarea></td>
													</tr>
													<tr>
														<td><label><font color="red">RT</font></label></td>
														<td>
															<input type="number" name="RT" value="<?php echo $RT; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">RW</font></label></td>
														<td>
															<input type="number" name="RW" value="<?php echo $RW; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Current City</font></label></td>
														<td><input type="text" name="Kota" value="<?php echo $Kota; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Current Province</font></label></td>
														<td><input type="text" name="Propinsi" value="<?php echo $Propinsi; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label>Current Postal Code</label></td>
														<td>
															<input type="number" name="KodePos" value="<?php echo $KodePos; ?>" class="form-control"/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Current Country</font></label></td>
														<td><input type="text" name="Negara" value="<?php echo $Negara; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Mobile Phone</font></label></td>
														<td>
															<input type="number" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Email 1</font></label></td>
														<td>
															<input type="email" name="Email" value="<?php echo $Email; ?>" class="form-control" required/>
															<font color="blue">This email will receive notifications.</font>
														</td>
													</tr>
													<tr>
														<td><label>Email 2</label></td>
														<td>
															<input type="email" name="Email2" value="<?php echo $Email2; ?>" class="form-control"/>
															<font color="blue">This email will receive backup notifications.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Home Address</font></label></td>
														<td>
															<textarea name="AlamatAsal" class="form-control" required><?php echo $AlamatAsal; ?></textarea>
															<font color="blue">Where do you come from.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Home RT</font></label></td>
														<td>
															<input type="number" name="RTAsal" value="<?php echo $RTAsal; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Home RW</font></label></td>
														<td>
															<input type="number" name="RWAsal" value="<?php echo $RWAsal; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Home City</font></label></td>
														<td>
															<input type="text" name="KotaAsal" value="<?php echo $KotaAsal; ?>" class="form-control" required/>
															<font color="blue">Your hometown.</font>
														</td>
													</tr>
													<tr>
														<td><label>Home Postal Code</label></td>
														<td>
															<input type="number" name="KodePosAsal" value="<?php echo $KodePosAsal; ?>" class="form-control"/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><h4><font color="green"><b>Father's Data</b></font></h4></td>
														<td></td>
													</tr>
													<tr>
														<td><label><font color="red">Father Name</font></label></td>
														<td><input type="text" name="NamaAyah" value="<?php echo $NamaAyah; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Religion</font></label></td>
														<td>
															<select name="AgamaAyah" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($AgamaAyah == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Last Education</font></label></td>
														<td>
															<select name="PendidikanAyah" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanAyah == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Profession</font></label></td>
														<td>
															<select name="PekerjaanAyah" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpekerjaan)
																	{
																		foreach($rowpekerjaan as $row)
																		{
																			echo "<option value='$row->Pekerjaan'";
																			if($PekerjaanAyah == $row->Pekerjaan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Address</font></label></td>
														<td><textarea name="AlamatOrtu" class="form-control" required><?php echo $AlamatOrtu; ?></textarea></td>
													</tr>
													<tr>
														<td><label><font color="red">City</font></label></td>
														<td><input type="text" name="KotaOrtu" value="<?php echo $KotaOrtu; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label>Postal Code</label></td>
														<td>
															<input type="number" name="KodePosOrtu" value="<?php echo $KodePosOrtu; ?>" class="form-control"/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label>Phone</label></td>
														<td>
															<input type="number" name="TeleponOrtu" value="<?php echo $TeleponOrtu; ?>" class="form-control"/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Mobile Phone</font></label></td>
														<td>
															<input type="number" name="HandphoneOrtu" value="<?php echo $HandphoneOrtu; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td>
															<input type="email" name="EmailOrtu" value="<?php echo $EmailOrtu; ?>" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td><h4><font color="green"><b>Mother's Data</b></font></h4></td>
														<td></td>
													</tr>
													<tr>
														<td><label><font color="red">Mother Name</font></label></td>
														<td><input type="text" name="NamaIbu" value="<?php echo $NamaIbu; ?>" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label><font color="red">Religion</font></label></td>
														<td>
															<select name="AgamaIbu" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowagama)
																	{
																		foreach($rowagama as $row)
																		{
																			echo "<option value='$row->Agama'";
																			if($AgamaIbu == $row->Agama)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Last Education</font></label></td>
														<td>
															<select name="PendidikanIbu" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpendidikan)
																	{
																		foreach($rowpendidikan as $row)
																		{
																			echo "<option value='$row->Pendidikan'";
																			if($PendidikanIbu == $row->Pendidikan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Profession</font></label></td>
														<td>
															<select name="PekerjaanIbu" class="form-control" required>
																<option value="">-- CHOOSE -- </option>
																<?php
																	if($rowpekerjaan)
																	{
																		foreach($rowpekerjaan as $row)
																		{
																			echo "<option value='$row->Pekerjaan'";
																			if($PekerjaanIbu == $row->Pekerjaan)
																			{
																				echo "selected";
																			}
																			echo ">$row->Nama</option>";
																		}
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Phone</label></td>
														<td>
															<input type="number" name="TeleponIbu" value="<?php echo $TeleponIbu; ?>" class="form-control"/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label><font color="red">Mobile Phone</font></label></td>
														<td>
															<input type="number" name="HandphoneIbu" value="<?php echo $HandphoneIbu; ?>" class="form-control" required/>
															<font color="blue">Numbers only.</font>
														</td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td>
															<input type="email" name="EmailIbu" value="<?php echo $EmailIbu; ?>" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td></td>
														<td><button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to save your data?\nMake sure that the data you entered is valid.\n\nTHIS ACTION CAN NOT BE RESTORED.')">SAVE</button></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>