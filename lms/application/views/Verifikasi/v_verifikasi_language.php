			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Personal Information</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail Personal Information
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<font color="red"><b>The following is a list of language specialization. You have to choose one. Specialization in this language will you follow until you graduated from ESMOD Jakarta. You can not make a change after this.</b></font>
										</div>
										<?php echo form_open_multipart('login/authentification_language',array('name' => 'contoh','class' => 'form-horizontal')); ?>
											<table class="table">
												<tbody>
													<tr>
														<td><label><font color="green">Student Identification Number (SIN)</font></label></td>
														<td><input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="green">KHSID</font></label></td>
														<td><input readonly type="text" name="KHSID" value="<?php echo $KHSID; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label><font color="red">SUBJECT SPECIALIZATION</font></label></td>
														<td>
															<select name="languageID" class="form-control" required>
																<option value="">-- CHOOSE --</option>
																<option value="ENG">ENGLISH</option>
																<option value="FRE">FRENCH</option>
															</select>
														</td>
													</tr>
													<tr>
														<td></td>
														<td>
															<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to set your LANGUAGE SPECIALIZATION?')">SAVE</button>
														</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>