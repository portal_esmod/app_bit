			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Application Service</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Application Service
								</div>
								<div class="panel-body">
									<div class="tab-content">
										<div class="row">
											<div class="col-md-12">
												<div class="alert alert-info">
													<b><a href="#">Description:</a></b> This menu is provided if you have your own business / parents business / family business and would like to published online.
													Please fill out the form below.
													Furthermore, the IT Team will give you reference the right design and website for your business.
													<br/>
													<font color="red"><b>
														If you want to create your website like Zalora, Zoya and etc. Please contact IT team.
													</b></font>
												</div>
											</div>
										</div>
										<form action="<?php echo site_url("service_center/ptl_update"); ?>" method="POST">
											<table class="table">
												<tbody>
													<tr>
														<td><label>SIN</label></td>
														<td>
															<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
															<input type="hidden" name="type_pesanan" value="APPLICATION" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td><label>Name</label></td>
														<td><input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td><input readonly type="email" name="Email" value="<?php echo $Email; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input readonly type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Application Type</label></td>
														<td>
															<select name="tipe_aplikasi" class="form-control" required>
																<option value="">-- CHOOSE --</option>
																<option value="WEBSITE">WEBSITE</option>
																<option value="ONLINE STORE">ONLINE STORE</option>
																<option value="OTHERS">OTHERS</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Descriptions</label></td>
														<td><textarea name="keterangan" placeholder="ex: I want to develop my website like Zalora." class="form-control" required></textarea></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td><button class="btn btn-info">&nbsp;&nbsp;Save&nbsp;&nbsp;</button></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>