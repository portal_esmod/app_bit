			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">E-Store Upload</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									E-Store Upload
								</div>
								<div class="panel-body">
									<div class="tab-content">
										<div class="row">
											<div class="col-md-12">
												<div class="alert alert-info">
													<b><a href="#">Description:</a></b> If you have a product that you want to sell or have your own business or family business you want to sell.
													Please fill out the form below.
													Furthermore, Estore Team will check the quality of the products that have been submitted.
													If the product passes the quality control, then the product will appear in the official website of ESMOD Jakarta.
												</div>
											</div>
										</div>
										<form action="<?php echo site_url("service_center/ptl_update_photo"); ?>" method="POST" enctype="multipart/form-data">
											<table class="table">
												<tbody>
													<tr>
														<td><label>SIN</label></td>
														<td>
															<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
															<input type="hidden" name="type_pesanan" value="ESTORE" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td><label>Name</label></td>
														<td><input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td><input readonly type="email" name="Email" value="<?php echo $Email; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input readonly type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Category</label></td>
														<td><input type="text" name="spesialisasi" placeholder="ex: menswear, womenswear" class="form-control" required/></td>
													</tr>
													<tr>
														<td><label>Upload Your Product</label></td>
														<td><input type="file" name="userfile" accept="image/*" required/></td>
													</tr>
													<tr>
														<td><label>Descriptions</label></td>
														<td><textarea name="keterangan" placeholder="ex: material cotton, size X/M/L, etc" class="form-control" required></textarea></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td><button class="btn btn-info">&nbsp;&nbsp;Save&nbsp;&nbsp;</button></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>