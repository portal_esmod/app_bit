			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Computer Service</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Computer Service
								</div>
								<div class="panel-body">
									<div class="tab-content">
										<div class="row">
											<div class="col-md-12">
												<div class="alert alert-info">
													<b><a href="#">Description:</a></b> This menu is reserved for those of you who want to fix or repair your computer used for work.
													Please fill out the form below to complete the data we need.
													Furthermore IT Team will help you to provide the right solution for your computer problems.
													<br/>
													<font color="red"><b>
														If you want to install Adobe Photoshop, Adobe illustrator, Microsoft Office, reinstall your Operating System and etc. Please contact IT team.
													</b></font>
												</div>
											</div>
										</div>
										<form action="<?php echo site_url("service_center/ptl_update"); ?>" method="POST">
											<table class="table">
												<tbody>
													<tr>
														<td><label>SIN</label></td>
														<td>
															<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control"/>
															<input type="hidden" name="type_pesanan" value="SERVICE" class="form-control"/>
														</td>
													</tr>
													<tr>
														<td><label>Name</label></td>
														<td><input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Email</label></td>
														<td><input readonly type="email" name="Email" value="<?php echo $Email; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Mobile Phone</label></td>
														<td><input readonly type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control"/></td>
													</tr>
													<tr>
														<td><label>Computer Model</label></td>
														<td>
															<select name="computer_model" class="form-control" required>
																<option value="">-- CHOOSE --</option>
																<option value="NOTEBOOK">NOTEBOOK</option>
																<option value="MACBOOK">MACBOOK</option>
																<option value="DESKTOP PC">DESKTOP PC</option>
																<option value="iMAC">iMAC</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Type</label></td>
														<td>
															<select name="computer_model" class="form-control" required>
																<option value="">-- CHOOSE --</option>
																<option value="HARDWARE">HARDWARE</option>
																<option value="SOFTWARE">SOFTWARE</option>
																<option value="OTHERS">OTHERS</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><label>Description of Problems</label></td>
														<td><textarea name="keterangan" placeholder="ex: My laptop not working properly." class="form-control" required></textarea></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td><button class="btn btn-info">&nbsp;&nbsp;Save&nbsp;&nbsp;</button></td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>