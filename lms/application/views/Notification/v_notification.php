			<div id="page-wrapper" class="page-wrapper-cls">
				<div id="page-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="page-head-line">Notifications</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Notifications
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>Title</th>
													<th>Messages</th>
													<th>Date</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$MhswID = $_COOKIE["nim"];
														$no = 1;
														$sebelumnya = "";
														$b1 = "";
														$b2 = "";
														foreach($rowrecord as $row)
														{
															if($row->tanggal_baca == "")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
															echo "<td>$no</td>
																	<td>$row->Judul</td>
																	<td>$row->Pesan</td>
																	<td>".tgl_indo($row->tanggal_buat)."</td>
																	<td align='center'>
																		<a href='".site_url("academic/attendance_detail/$row->JadwalID/$row->KRSID/$row->id_notifikasi_lms/notif")."'>Show Detail</a>
																	</td>
																</tr>";
															$no++;
														}
													}
													else
													{
														echo "<tr>
																<td colspan='5'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>