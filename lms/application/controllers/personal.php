<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Personal extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms!=='logged')
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_akses');
			$this->load->model('m_agama');
			$this->load->model('m_dosen');
			$this->load->model('m_hari');
			$this->load->model('m_jadwal');
			$this->load->model('m_jadwal_hari');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_pekerjaan');
			$this->load->model('m_pendidikan');
			$this->load->model('m_prodi');
			$this->load->model('m_sipil');
			$this->load->model('m_subjek');
			$this->load->model('m_status');
			$this->load->model('m_year');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED. Please contact IT Team.','../login/ptl_suspend');
			}
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','personal');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $result['MhswID'];
			$data['Nama'] = $result['Nama'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['TempatLahir'] = $result['TempatLahir'];
			$data['TanggalLahir'] = $result['TanggalLahir'];
			$data['Kelamin'] = $result['Kelamin'];
			$data['WargaNegara'] = $result['WargaNegara'];
			$data['Kebangsaan'] = $result['Kebangsaan'];
			$data['Agama'] = $result['Agama'];
			$data['StatusSipil'] = $result['StatusSipil'];
			$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
			$data['TahunLulus'] = $result['TahunLulus'];
			$data['TinggiBadan'] = $result['TinggiBadan'];
			$data['BeratBadan'] = $result['BeratBadan'];
			$data['AnakKe'] = $result['AnakKe'];
			$data['JumlahSaudara'] = $result['JumlahSaudara'];
			$data['Alamat'] = $result['Alamat'];
			$data['RT'] = $result['RT'];
			$data['RW'] = $result['RW'];
			$data['Propinsi'] = $result['Propinsi'];
			$data['Kota'] = $result['Kota'];
			$data['KodePos'] = $result['KodePos'];
			$data['Negara'] = $result['Negara'];
			$data['Handphone'] = $result['Handphone'];
			$data['Email'] = $result['Email'];
			$data['Email2'] = $result['Email2'];
			
			$data['AlamatAsal'] = $result['AlamatAsal'];
			$data['RTAsal'] = $result['RTAsal'];
			$data['RWAsal'] = $result['RWAsal'];
			$data['KotaAsal'] = $result['KotaAsal'];
			$data['KodePosAsal'] = $result['KodePosAsal'];
			
			$data['NamaAyah'] = $result['NamaAyah'];
			$data['AgamaAyah'] = $result['AgamaAyah'];
			$data['PendidikanAyah'] = $result['PendidikanAyah'];
			$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
			$data['AlamatOrtu'] = $result['AlamatOrtu'];
			$data['KotaOrtu'] = $result['KotaOrtu'];
			$data['KodePosOrtu'] = $result['KodePosOrtu'];
			$data['TeleponOrtu'] = $result['TeleponOrtu'];
			$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
			$data['EmailOrtu'] = $result['EmailOrtu'];
			$data['NamaIbu'] = $result['NamaIbu'];
			$data['AgamaIbu'] = $result['AgamaIbu'];
			$data['PendidikanIbu'] = $result['PendidikanIbu'];
			$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
			$data['TeleponIbu'] = $result['TeleponIbu'];
			$data['HandphoneIbu'] = $result['HandphoneIbu'];
			$data['EmailIbu'] = $result['EmailIbu'];
			
			$data['marketing'] = $this->m_akses->PTL_all_marketing();
			$data['rowagama'] = $this->m_agama->PTL_all_active();
			$data['rowsipil'] = $this->m_sipil->PTL_all_active();
			$data['rowpendidikan'] = $this->m_pendidikan->PTL_all_active();
			$data['rowpekerjaan'] = $this->m_pekerjaan->PTL_all_active();
			$this->load->view('Portal/v_header');
			$this->load->view('Personal/v_personal',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_set_all()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			for($n=1;$n<=$total;$n++)
			{
				$KHSID = $this->input->post("KHSID$n");
				$data = array(
							'WaliKelasID' => $this->input->post("WaliKelasID$n"),
							'WaliKelasID2' => $this->input->post("WaliKelasID2$n"),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
			}
			echo warning("Your data successfully updated.","../homeroom");
		}
	}
?>