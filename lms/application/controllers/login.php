<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Login extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_verifikasi = gmdate("dis", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('auth');
			$this->load->library('log');
			$this->load->library('upload');
			$this->load->model('m_agama');
			$this->load->model('m_akses');
			$this->load->model('m_aktivitas');
			$this->load->model('m_akun');
			$this->load->model('m_berita');
			$this->load->model('m_catatan');
			$this->load->model('m_catatan2');
			$this->load->model('m_jadwal');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_log');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_mail');
			$this->load->model('m_maintenance');
			$this->load->model('m_pekerjaan');
			$this->load->model('m_pendidikan');
			$this->load->model('m_peminjaman');
			$this->load->model('m_pesan');
			$this->load->model('m_pesan_mhsw');
			$this->load->model('m_presensi');
			$this->load->model('m_rating');
			$this->load->model('m_rating_grup');
			$this->load->model('m_rating_grup_all');
			$this->load->model('m_rating_question');
			$this->load->model('m_sipil');
			$this->load->model('m_sms');
			$this->load->model('m_subjek');
			$this->load->model('m_ultah');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function suspended()
		{
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED.','../login/ptl_suspend');
			}
		}
		
		function index()
		{
			$rowrecord = $this->m_mahasiswa->PTL_all_birthday_today();
			$JmlUltah = 0;
			$Nama = "";
			$Email = "";
			if($_SERVER["HTTP_HOST"] != "localhost")
			{
				if($rowrecord)
				{
					foreach($rowrecord as $row)
					{
						$MhswID = $row->MhswID;
						$result = $this->m_ultah->PTL_select_today($MhswID);
						if($result)
						{
							
						}
						else
						{
							$Nama .= " ".$row->Nama.",";
							$word = explode(" ",$row->Nama);
							$NamaSingkat = strtoupper($word[0]);
							$Handphone = $row->Handphone;
							$Email .= $row->Email.",";
							$JmlUltah++;
							if(substr($Handphone,0,2) == "62")
							{
								$prov = "0".substr($Handphone,2,3);
							}
							else
							{
								$prov = substr($Handphone,0,4);
							}
							if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
							{
								$data = array(
											'id_user' => $row->MhswID,
											'nama' => $row->Nama,
											'aplikasi' => 'LMS',
											'judul_sms' => 'Ucapan Ulang Tahun',
											'nomor_hp' => $Handphone,
											'pesan' => "ESMOD (NO-REPLY): HAPPY BIRTHDAY $NamaSingkat! Hopefully there is a lot of love today. Face your future with enthusiasm and do your best.",
											'login_buat' => 'SYSTEM',
											'tanggal_buat' => $this->waktu
											);
								$this->m_sms->PTL_insert($data);
							}
							else
							{
								$url = "http://103.16.199.187/masking/send_post.php";
								$rows = array(
											"username" => "esmodjkt",
											"password" => "esmj@kr00t",
											"hp" => "$Handphone",
											"message" => "HAPPY BIRTHDAY $NamaSingkat! Hopefully there is a lot of love today. Face your future with enthusiasm and do your best."
											);
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_URL, $url );
								curl_setopt($curl, CURLOPT_POST, TRUE );
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
								curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
								curl_setopt($curl, CURLOPT_HEADER, FALSE );
								curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
								curl_setopt($curl, CURLOPT_TIMEOUT, 60);
								$htm = curl_exec($curl);
								if(curl_errno($curl) !== 0)
								{
									$data = array(
												'id_user' => $row->MhswID,
												'nama' => $row->Nama,
												'aplikasi' => 'HRIS',
												'judul_sms' => 'Ucapan Ulang Tahun',
												'nomor_hp' => $Handphone,
												'pesan' => "ESMOD (NO-REPLY): HAPPY BIRTHDAY $NamaSingkat! Hopefully there is a lot of love today. Face your future with enthusiasm and do your best.",
												'login_buat' => 'SYSTEM',
												'tanggal_buat' => $this->waktu
												);
									$this->m_sms->PTL_insert($data);
									error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
								}
								curl_close($curl);
								print_r($htm);
							}
							$ip_client = $this->log->getIpAdress();
							$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$os_client = $this->log->getOs();
							$browser_client = $this->log->getBrowser();
							$perangkat_client = $this->log->getPerangkat();
							$data = array(
										'id_akun' => $row->MhswID,
										'nama' => $row->Nama,
										'tanggal_lahir' => $row->TanggalLahir,
										'pesan' => "HAPPY BIRTHDAY $NamaSingkat! Hopefully there is a lot of love today. Face your future with enthusiasm and do your best.",
										'tanggal_kirim' => $this->waktu,
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client
										);
							$this->m_ultah->PTL_insert($data);
						}
					}
				}
			}
			if($JmlUltah > 0)
			{
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$Email");
				$this->email->bcc('lendra.permana@gmail.com,wprasasti@esmodjakarta.com');
				$this->email->subject('HAPPY BIRTHDAY! (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					<br/>
					Dear<b>".strtoupper($Nama)."</b>
					<br/>
					<br/>
					<center>
						<img src='http://app.esmodjakarta.com/banner/birthday.gif' style='max-width:700px; width:100%'/>
					</center>
					<br/>
					<br/>
					HAPPY BIRTHDAY!
					<br/>
					<br/>
					Hopefully there is a lot of love today. Face your future with enthusiasm and do your best.
					<br/>
					<br/>
					Thanks,
					<br/>
					<br/>
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				$this->email->send();
			}
			
			@$is_login_lms = $this->session->userdata('is_login_lms');
			if($is_login_lms == "notlogged")
			{
				$this->session->unset_userdata('is_login_lms');
				setcookie("is_login_lms", "", time()+1);
				setcookie("is_login_lms", "", time()+1, "/");
				echo warning('You are not logged in.','../login');
			}
			@$cis_login_app = $_COOKIE["is_login_app"];
			@$cis_login_lms = $_COOKIE["is_login_lms"];
			if(($cis_login_app == "logged") AND ($cis_login_lms == "logged"))
			{
				redirect('login/ptl_home');
			}
			else
			{
				if($cis_login_lms == "logged")
				{
					echo warning('Sorry, it looks like you are already logged ...','../login/ptl_home');
				}
			}
			$this->load->view('v_login');
		}
		
		function ptl_do_login()
		{
			$this->authentification();
			$remember = $this->input->post('remember');
			if($remember == "30")
			{
				$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
				$recaptcha = $this->input->post('g-recaptcha-response');
				$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
				$response = @file_get_contents($api_url);
				$datacaptcha = json_decode($response, true);
				if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
				{
					$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
					if ($this->form_validation->run() == FALSE)
					{
						$this->index();
					}
					else
					{
						$login = array(
									'username' => $this->input->post('username'),
									'password' => $this->input->post('password')
									);
						$return = $this->auth->do_login_remember($login);
						if($return)
						{
							$h = "-7";
							$hm = $h * 60;
							$ms = $hm * 60;
							$tanggal = gmdate("Y-m-d", time()-($ms));
							$jam = gmdate("H:i:s", time()-($ms));
							$word = explode("-",$tanggal);
							$dataTime = mktime(0,0,0,$word[1],$word[2]+7,$word[0]);
							$tanggal_pengingat = date("Y-m-d",$dataTime)." ".$jam;
							$MhswID = $this->input->post('username');
							$data = array(
										'tanggal_pengingat' => $tanggal_pengingat,
										'verifikasi_login' => 'N'
										);
							$this->m_mahasiswa->PTL_update($MhswID,$data);
							$result = $this->m_mahasiswa->PTL_select($MhswID);
							$NamaAkun = "Unknown ($MhswID)";
							$Email = "";
							$Handphone = "";
							$suspend = "";
							if($result)
							{
								$NamaAkun = $result['Nama'];
								$Email = $result['Email'];
								$Handphone = $result['Handphone'];
								$suspend = $result['suspend'];
							}
							if($suspend == "Y")
							{
								echo warning('Sorry! Your account was SUSPENDED.','../login/ptl_suspend');
							}
							else
							{
								$h = "-7";
								$hm = $h * 60; 
								$ms = $hm * 60;
								$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
								$ip_client = $this->log->getIpAdress();
								$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
								$os_client = $this->log->getOs();
								$browser_client = $this->log->getBrowser();
								$perangkat_client = $this->log->getPerangkat();
								
								$this->load->library('email');
								$config = array();
								$config['charset'] = 'utf-8';
								$config['useragent'] = 'Codeigniter';
								$config['protocol']= "smtp";
								$config['mailtype']= "html";
								$config['smtp_host']= "mail.esmodjakarta.com";
								$config['smtp_port']= "25";
								$config['smtp_timeout']= "5";
								$config['smtp_user']= "no-reply@esmodjakarta.com";
								$config['smtp_pass']= "noreplyesmod";
								$config['crlf']="\r\n"; 
								$config['newline']="\r\n"; 
								$config['wordwrap'] = TRUE;
								$this->email->initialize($config);
								$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
								$this->email->to("$Email");
								$this->email->bcc('lendra.permana@gmail.com');
								$this->email->subject('LMS - REMEMBER LOGGED IN FOR 30 DAYS (NO REPLY)');
								$this->email->message("
									<center>
										<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
										<font color='red'><h2>LMS</h2></font>
									</center>
									<br/>
									Dear <b>".strtoupper($NamaAkun)."</b>,
									<br/>
									<br/>
									This email want to inform you that you have logged in for the next 30 days in LMS Application.
									<br/>
									Contact your administrator immediately if you do not perform this action.
									<br/>
									<br/>
									<br/>
									Thanks,
									<br/>
									<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
									<center>
										<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
										Ip: $ip_client
										<br/>
										Hostname: $hostname_client
										<br/>
										OS: $os_client
										<br/>
										Browser: $browser_client
										<br/>
										Devices: $perangkat_client
									</center>
								");
								if($this->email->send())
								{
									echo warning('Welcome '.from_session('nama').'\n'.
											'Login Now  :  '.$waktu,'../login/ptl_home');
								}
								else
								{
									echo warning('Welcome '.from_session('nama').'\n'.
											'Login Now  :  '.$waktu.' Code: ESNA.','../login/ptl_home');
								}
							}
						}
						else
						{
							echo warning('Sorry, username or password you entered is incorrect ...','../login');
						}
					}
				}
				else
				{
					echo warning('Please complete the form and captcha.','../login');
				}
			}
			else
			{
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$login = array(
								'username' => $this->input->post('username'),
								'password' => $this->input->post('password')
								);
					$return = $this->auth->do_login($login);
					if($return)
					{
						$MhswID = $this->input->post('username');
						$data = array(
									'tanggal_pengingat' => '',
									'verifikasi_login' => 'N'
									);
						$this->m_mahasiswa->PTL_update($MhswID,$data);
						$result = $this->m_mahasiswa->PTL_select($MhswID);
						$suspend = "";
						if($result)
						{
							$suspend = $result['suspend'];
						}
						if($suspend == "Y")
						{
							echo warning('Sorry! Your account was SUSPENDED.','../login/ptl_suspend');
						}
						else
						{
							$h = "-7";
							$hm = $h * 60; 
							$ms = $hm * 60;
							$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
							echo warning('Welcome '.from_session('nama').'\n'.
									'Login Now  :  '.$waktu,'../login/ptl_home');
						}
					}
					else
					{
						echo warning('Sorry, username or password you entered is incorrect ...','../login');
					}
				}
			}
		}
		
		function ptl_suspend()
		{
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data["alasan_suspend"] = $result["alasan_suspend"];
			$this->load->view('v_suspend',$data);
		}
		
		function ptl_forgot()
		{
			$this->load->view('v_forgot');
		}
		
		function ptl_lupa_password()
		{
			$this->authentification();
			$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
			$recaptcha = $this->input->post('g-recaptcha-response');
			$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
			$response = @file_get_contents($api_url);
			$datacaptcha = json_decode($response, true);
			if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
			{
				$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
				if($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$h = "-7";
					$hm = $h * 60; 
					$ms = $hm * 60;
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
					$tanggal = gmdate("Y-m-d", time()-($ms));
					$jam = gmdate("H:i:s", time()-($ms));
					$tgl = gmdate("d M Y", time()-($ms));
					$email = $this->input->post('email');
					$return = $this->m_mahasiswa->PTL_select_email($email);
					if($return)
					{
						$datadir = array(
									// 'id_akun' => $return['id_akun'],
									// 'id_akses' => $return['id_akses'],
									// 'id_cabang' => $return['id_cabang'],
									// 'nama' => $return['nama'],
									// 'divisi' => $return['divisi'],
									// 'jabatan' => $return['jabatan'],
									// 'akses' => $return['akses'],
									'aplikasi' => 'LMS',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD'
									);
						$this->m_log->PTL_input($datadir);
						
						$datacat = array(
										'jenis' => 'MAINTENANCE',
										'aplikasi' => 'LMS',
										// 'dari' => $return['nama'],
										// 'dr_divisi' => $return['divisi'],
										// 'dr_jabatan' => $return['jabatan'],
										'pada' => 'LENDRA PERMANA',
										'pd_divisi' => 'IT',
										'pd_jabatan' => 'DEVELOPER',
										'masalah' => 'Forgot Password',
										'solusi' => 'Reset Password in Database',
										'tanggal_posting' => $waktu,
										'tanggal_done' => $waktu,
										'status' => 'DONE'
										);
						$this->m_catatan->PTL_input($datacat);
						
						$username = $return["MhswID"];
						$data = gmdate("is", time()-($ms));
						$pass = gmdate("is", time()-($ms));
						$this->m_akses->PTL_update_password($username,$data);
						
						$MhswID = $return['MhswID'];
						$Nama = "";
						$resakun = $this->m_mahasiswa->PTL_select($MhswID);
						$handphone = "";
						if($resakun)
						{
							$Nama = $resakun['Nama'];
							$handphone = $resakun['handphone'];
						}
						if($handphone != "")
						{
							if(substr($handphone,0,2) == "62")
							{
								$prov = "0".substr($handphone,2,3);
							}
							else
							{
								$prov = substr($handphone,0,4);
							}
							if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
							{
								$data = array(
											'id_user' => $MhswID,
											'nama' => $Nama,
											'aplikasi' => 'LMS',
											'judul_sms' => 'Pengiriman Untuk Lupa Password',
											'nomor_hp' => $handphone,
											'pesan' => "ESMOD (NO-REPLY): Dear $Nama. Your new password is $pass",
											'login_buat' => $MhswID."_".$Nama,
											'tanggal_buat' => $this->waktu
											);
								$this->m_sms->PTL_insert($data);
							}
							else
							{
								$url = "http://103.16.199.187/masking/send_post.php";
								$rows = array(
											"username" => "esmodjkt",
											"password" => "esmj@kr00t",
											"hp" => "$handphone",
											"message" => "Dear $Nama. Your new password is $pass"
											);
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_URL, $url );
								curl_setopt($curl, CURLOPT_POST, TRUE );
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
								curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
								curl_setopt($curl, CURLOPT_HEADER, FALSE );
								curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
								curl_setopt($curl, CURLOPT_TIMEOUT, 60);
								$htm = curl_exec($curl);
								if(curl_errno($curl) !== 0)
								{
									$data = array(
												'id_user' => $MhswID,
												'nama' => $Nama,
												'aplikasi' => 'LMS',
												'judul_sms' => 'Pengiriman Untuk Lupa Password',
												'nomor_hp' => $handphone,
												'pesan' => "ESMOD (NO-REPLY): Dear $Nama. Your new password is $pass",
												'login_buat' => $MhswID."_".$Nama,
												'tanggal_buat' => $this->waktu
												);
									$this->m_sms->PTL_insert($data);
									error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
								}
								curl_close($curl);
								print_r($htm);
							}
						}
						
						$this->load->library('email');
						$config = array();
						$config['charset'] = 'utf-8';
						$config['useragent'] = 'Codeigniter';
						$config['protocol']= "smtp";
						$config['mailtype']= "html";
						$config['smtp_host']= "mail.esmodjakarta.com";
						$config['smtp_port']= "25";
						$config['smtp_timeout']= "5";
						$config['smtp_user']= "no-reply@esmodjakarta.com";
						$config['smtp_pass']= "noreplyesmod";
						$config['crlf']="\r\n"; 
						$config['newline']="\r\n"; 
						$config['wordwrap'] = TRUE;
						$this->email->initialize($config);
						$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
						$this->email->to($return['Email']);
						$this->email->cc($return['Email2']);
						$this->email->bcc('lendra.permana@gmail.com');
						$this->email->subject('FORGOT PASSWORD (NO REPLY)');
						$this->email->message("
							<center>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
								<font color='red'><h2>LMS</h2></font>
							</center>
							<br/>
							Hi ".strtoupper($return['Nama'])."!
							<br/>
							<br/>
							On this $tgl at $jam, you made a request to reset your password.
							<br/>
							<br/>
							This is your new access
							<br/>
							URL: http://app.esmodjakarta.com/lms
							<br/>
							SIN: <b>$username</b>
							<br/>
							Your new password: <b>$data</b>
							<br/>
							<br/>
							<br/>
							Thanks,
							<br/>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
							<center>
								<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
								Ip: $ip_client
								<br/>
								Hostname: $hostname_client
								<br/>
								OS: $os_client
								<br/>
								Browser: $browser_client
								<br/>
								Devices: $perangkat_client
							</center>
						");
						if($this->email->send())
						{
							echo warning('Verification will be sent to your email:\n\n'.$return['Email'],'../login');
						}
						else
						{
							echo warning('Email server is not active. Please try again ...','../login');
						}
					}
					else
					{
						$datadir = array(
									'in' => $email,
									'aplikasi' => 'LMS',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD FAILED'
									);
						$this->m_log->PTL_input($datadir);
						echo warning('Sorry, your Username or Email you entered is incorrect ...','../login/ptl_forgot');
					}
				}
			}
			else
			{
				echo warning('Please complete the form and captcha.','../login/ptl_forgot');
			}
		}
		
		function ptl_password()
		{
			$this->authentification();
			$this->suspended();
			$this->session->set_userdata('menu','home');
			$this->load->view('Portal/v_header');
			$this->load->view('Personal/v_password');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_password_update()
		{
			$this->authentification();
			$this->suspended();
			$username = $_COOKIE["nim"];
			$MhswID = $_COOKIE["nim"];
			$return = $this->m_mahasiswa->PTL_select($MhswID);
			$dataku = $this->input->post('pass_lama');
			
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$jam = gmdate("H:i:s", time()-($ms));
			$tgl = gmdate("d M Y", time()-($ms));
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($return['Email']);
			$this->email->cc($return['Email2']);
			$this->email->bcc('lendra.permana@gmail.com');
			
			$result1 = $this->m_akses->PTL_password_lama($username,$dataku);
			if($result1)
			{
				$data = $this->input->post('pass_baru1');
				$data1 = $this->input->post('pass_baru2');
				if($data == $data1)
				{
					$this->m_akses->PTL_update_password($username,$data);
					$this->email->subject('PASSWORD CHANGED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>LMS</h2></font>
						</center>
						<br/>
						Hi ".strtoupper($return['Nama'])."!
						<br/>
						<br/>
						On this $tgl at $jam, you make a change your password. This email is to notify you that the password for the your PORTAL LMS account has been changed.
						<br/>
						If you did not make this change, please contact IT Developer by e-mail at lendra@esmodjakarta.com.
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Password Successfully Changed.","../login/ptl_home");
					}
					else
					{
						echo warning("Email server is not active. Password Successfully Changed.","../login/ptl_home");
					}
				}
				else
				{
					echo warning("Confirm password is not the same. Please repeat.","../login/ptl_password");
				}
			}
			else
			{
				echo warning("Old password is incorrect. Please repeat.","../login/ptl_password");
			}
		}
		
		function ptl_logout()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$MhswID = $_COOKIE["nim"];
			$data = array(
						'tanggal_pengingat' => '',
						'verifikasi_login' => 'N'
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			$this->auth->logout();
			echo warning('You have successfully logged out ...'.'\n'.
						'Logout Now :  '.$waktu,'../login');
		}
		
		function ptl_home()
		{
			$this->authentification();
			$this->suspended();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$MhswID = $_COOKIE["nim"];
			$nis = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$tanggal_pengingat = $result['tanggal_pengingat'];
			if(($result['tanggal_pengingat'] != "") AND ($result['verifikasi_login'] == "N"))
			{
				$Handphone = "";
				if($result)
				{
					$Handphone = $result['Handphone'];
				}
				if($Handphone != "")
				{
					if(substr($Handphone,0,2) == "62")
					{
						$prov = "0".substr($Handphone,2,3);
					}
					else
					{
						$prov = substr($Handphone,0,4);
					}
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $_COOKIE["nim"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => 'LMS',
									'judul_sms' => 'OTP untuk login 30 hari',
									'nomor_hp' => $Handphone,
									'pesan' => "ESMOD (NO-REPLY): LMS App Login for 30 days. Your OTP $this->kode_verifikasi",
									'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$Handphone",
									"message" => "LMS App Login for 30 days. Your OTP $this->kode_verifikasi"
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $_COOKIE["nim"],
										'nama' => $_COOKIE["nama"],
										'aplikasi' => 'LMS',
										'judul_sms' => 'OTP untuk login 30 hari',
										'nomor_hp' => $Handphone,
										'pesan' => "ESMOD (NO-REPLY): LMS App Login for 30 days. Your OTP $this->kode_verifikasi",
										'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
				}
				$data = array(
							'kode_verifikasi' => $this->kode_verifikasi
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				echo warning('OTP was sent.','../login/ptl_verification_sms');
			}
			else
			{
				if($result['verifikasi_kode'] == "")
				{
					$this->session->set_userdata('menu','');
					$data['MhswID'] = $result['MhswID'];
					$data['Nama'] = $result['Nama'];
					$data['PresenterID'] = $result['PresenterID'];
					$data['TempatLahir'] = $result['TempatLahir'];
					$data['TanggalLahir'] = $result['TanggalLahir'];
					$data['Kelamin'] = $result['Kelamin'];
					$data['WargaNegara'] = $result['WargaNegara'];
					$data['Kebangsaan'] = $result['Kebangsaan'];
					$data['Agama'] = $result['Agama'];
					$data['StatusSipil'] = $result['StatusSipil'];
					$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
					$data['TahunLulus'] = $result['TahunLulus'];
					$data['TinggiBadan'] = $result['TinggiBadan'];
					$data['BeratBadan'] = $result['BeratBadan'];
					$data['AnakKe'] = $result['AnakKe'];
					$data['JumlahSaudara'] = $result['JumlahSaudara'];
					$data['Alamat'] = $result['Alamat'];
					$data['RT'] = $result['RT'];
					$data['RW'] = $result['RW'];
					$data['Kota'] = $result['Kota'];
					$data['Propinsi'] = $result['Propinsi'];
					$data['KodePos'] = $result['KodePos'];
					$data['Negara'] = $result['Negara'];
					$data['Handphone'] = $result['Handphone'];
					$data['Email'] = $result['Email'];
					$data['Email2'] = $result['Email2'];
					
					$data['AlamatAsal'] = $result['AlamatAsal'];
					$data['RTAsal'] = $result['RTAsal'];
					$data['RWAsal'] = $result['RWAsal'];
					$data['KotaAsal'] = $result['KotaAsal'];
					$data['KodePosAsal'] = $result['KodePosAsal'];
					
					$data['NamaAyah'] = $result['NamaAyah'];
					$data['AgamaAyah'] = $result['AgamaAyah'];
					$data['PendidikanAyah'] = $result['PendidikanAyah'];
					$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
					$data['AlamatOrtu'] = $result['AlamatOrtu'];
					$data['KotaOrtu'] = $result['KotaOrtu'];
					$data['KodePosOrtu'] = $result['KodePosOrtu'];
					$data['TeleponOrtu'] = $result['TeleponOrtu'];
					$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
					$data['EmailOrtu'] = $result['EmailOrtu'];
					
					$data['NamaIbu'] = $result['NamaIbu'];
					$data['AgamaIbu'] = $result['AgamaIbu'];
					$data['PendidikanIbu'] = $result['PendidikanIbu'];
					$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
					$data['TeleponIbu'] = $result['TeleponIbu'];
					$data['HandphoneIbu'] = $result['HandphoneIbu'];
					$data['EmailIbu'] = $result['EmailIbu'];
					
					$data['marketing'] = $this->m_akses->PTL_all_marketing();
					$data['rowagama'] = $this->m_agama->PTL_all_active();
					$data['rowsipil'] = $this->m_sipil->PTL_all_active();
					$data['rowpendidikan'] = $this->m_pendidikan->PTL_all_active();
					$data['rowpekerjaan'] = $this->m_pekerjaan->PTL_all_active();
					$this->load->view('Portal/v_header_verifikasi');
					$this->load->view('Verifikasi/v_verifikasi_profil',$data);
					$this->load->view('Portal/v_footer');
				}
				else
				{
					if($result['verifikasi'] == "N")
					{
						$this->session->set_userdata('menu','');
						$data['MhswID'] = $result['MhswID'];
						$data['Email'] = $result['Email'];
						$this->load->view('Portal/v_header_verifikasi');
						$this->load->view('Verifikasi/v_verifikasi_kode',$data);
						$this->load->view('Portal/v_footer');
					}
					else
					{
						if(($result['ProgramID'] == "SC") AND ($result['status_foto'] == "N"))
						{
							$this->session->set_userdata('menu','');
							$data['MhswID'] = $result['MhswID'];
							$data['Nama'] = $result['Nama'];
							$data['Email'] = $result['Email'];
							$this->load->view('Portal/v_header_verifikasi');
							$this->load->view('Verifikasi/v_verifikasi_foto',$data);
							$this->load->view('Portal/v_footer');
						}
						else
						{
							$reskhs = $this->m_khs->PTL_select_max($MhswID);
							$TahunID = "";
							$KHSID = "";
							$ProgramID = "";
							$ProdiID = "";
							$KelasID = "";
							$Sesi = "";
							$languageID = "";
							$TahunKe = "";
							if($reskhs)
							{
								$TahunID = $reskhs['tahun'];
								$reskhs2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
								if($reskhs2)
								{
									$KHSID = $reskhs2['KHSID'];
									$ProgramID = $reskhs2['ProgramID'];
									$ProdiID = $reskhs2['ProdiID'];
									$KelasID = $reskhs2['KelasID'];
									$Sesi = $reskhs2['Sesi'];
									$languageID = $reskhs2['languageID'];
									$TahunKe = $reskhs2['TahunKe'];
								}
							}
							$h = "-7";
							$hm = $h * 60;
							$ms = $hm * 60;
							$restahun = $this->m_year->PTL_select($TahunID);
							$id_rating_question_grup = "";
							$Questionnaire = "0000-00-00";
							$QuestionnaireEnd = "0000-00-00";
							if($restahun)
							{
								$id_rating_question_grup = $restahun['id_rating_question_grup'];
								$Questionnaire = str_replace("-","",$restahun['Questionnaire']);
								$QuestionnaireEnd = str_replace("-","",$restahun['QuestionnaireEnd']);
							}
							$RatingDate = gmdate("Ymd", time()-($ms));
							$reskrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
							$TotalKRS = count($reskrs);
							$resrating = $this->m_rating_grup->PTL_all_spesifik($id_rating_question_grup,$MhswID,$KHSID);
							$TotalRating = count($resrating);
							if(($RatingDate >= $Questionnaire) AND ($RatingDate <= $QuestionnaireEnd) AND ($TotalRating < $TotalKRS))
							{
								$data['id_rating_question_grup'] = $id_rating_question_grup;
								$data['TahunID'] = $TahunID;
								$data['KHSID'] = $KHSID;
								$data['ProgramID'] = $ProgramID;
								$data['ProdiID'] = $ProdiID;
								$data['TahunKe'] = $TahunKe;
								$data['Sesi'] = $Sesi;
								$data['KelasID'] = $KelasID;
								$data['rowrecord'] = $this->m_rating_question->PTL_all_spesifik($id_rating_question_grup);
								$data['rowrecord2'] = $reskrs;
								$this->load->view('Portal/v_header_verifikasi');
								$this->load->view('Verifikasi/v_verifikasi_rating',$data);
								$this->load->view('Portal/v_footer');
							}
							else
							{
								$resakun = $this->m_mahasiswa->PTL_select($MhswID);
								if($resakun['handphone_verifikasi'] == "N")
								{
									if($_SERVER["HTTP_HOST"] == "localhost")
									{
										header("Location: http://localhost/app/lms/verification/ptl_auth/$MhswID");
									}
									else
									{
										header("Location: http://app.esmodjakarta.com/lms/verification/ptl_auth/$MhswID");
									}
								}
								else
								{
									$ultah = gmdate("m-d", time()-($ms));
									$tanggal_lahir = substr($resakun['TanggalLahir'],5,5);
									$data['gambar_ultah'] = '';
									if($tanggal_lahir == $ultah)
									{
										$data['gambar_ultah'] = "<img src='".base_url("ptl_storage/foto_umum/ultah.gif")."' class='img-responsive' alt='Cinque Terre' width='1100'>";
									}
									if($resakun['status_identitas'] == "N")
									{
										$data['MhswID'] = $_COOKIE["nim"];
										$this->load->view('Portal/v_header_verifikasi');
										$this->load->view('Verifikasi/v_verifikasi_file',$data);
										$this->load->view('Portal/v_footer');
									}
									else
									{
										if(($ProdiID != "IFB3") AND ($ProgramID != "SC"))
										{
											if(($Sesi == 1) AND ($languageID == ""))
											{
												$data['KHSID'] = $KHSID;
												$data['MhswID'] = $result['MhswID'];
												$data['TahunID'] = $TahunID;
												$this->load->view('Portal/v_header_verifikasi');
												$this->load->view('Verifikasi/v_verifikasi_language',$data);
												$this->load->view('Portal/v_footer');
											}
											else
											{
												// New Feature
												// $TahunMasuk = (gmdate("Y", time()-($ms))) - 3;
												// if((substr($result['MhswID'],0,4) <= $TahunMasuk) AND ($result['LastCollections'] == "") AND ($Sesi > 2))
												// {
													// $data['KHSID'] = $KHSID;
													// $data['MhswID'] = $result['MhswID'];
													// $data['Nama'] = $result['Nama'];
													// $data['TahunID'] = $TahunID;
													// $this->load->view('Portal/v_header_verifikasi');
													// $this->load->view('Verifikasi/v_verifikasi_last_collections',$data);
													// $this->load->view('Portal/v_footer');
												// }
												// else
												// {
													$this->session->set_userdata('menu','home');
													$data['tanggal_pengingat'] = $tanggal_pengingat;
													$data['rowbayar'] = $this->m_khs->PTL_all_select($MhswID);
													$data['rowpesan'] = $this->m_pesan->PTL_all();
													$data['rowcatatan'] = $this->m_catatan2->PTL_all_select($MhswID);
													$data['rowberita'] = $this->m_berita->PTL_all();
													$data['rowpeminjaman'] = $this->m_peminjaman->PTL_all_select_nis($nis);
													$data['rowaktivitas'] = $this->m_aktivitas->PTL_all();
													$this->load->view('Portal/v_header');
													$this->load->view('Beranda/v_home',$data);
													$this->load->view('Portal/v_footer');
												// }
											}
										}
										else
										{
											$this->session->set_userdata('menu','home');
											$data['tanggal_pengingat'] = $tanggal_pengingat;
											$data['rowbayar'] = $this->m_khs->PTL_all_select($MhswID);
											$data['rowpesan'] = $this->m_pesan->PTL_all();
											$data['rowcatatan'] = $this->m_catatan2->PTL_all_select($MhswID);
											$data['rowberita'] = $this->m_berita->PTL_all();
											$data['rowpeminjaman'] = $this->m_peminjaman->PTL_all_select_nis($nis);
											$data['rowaktivitas'] = $this->m_aktivitas->PTL_all();
											$this->load->view('Portal/v_header');
											$this->load->view('Beranda/v_home',$data);
											$this->load->view('Portal/v_footer');
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		function ptl_verification_file()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$MhswID = $this->input->post('MhswID');
			$pesan = "";
			$error = array();
			$gambar = array();
			$no = 1;
			foreach($_FILES as $field_name => $file)
			{
				$h = "-7";
				$hm = $h * 60;
				$ms = $hm * 60;
				$storage = gmdate("Y-m", time()-($ms));
				$tgl = gmdate("ymdHis", time()-($ms));
				$download = $file['name'];
				$downloadin = $tgl."_".$MhswID."_".str_replace(' ','_',$download);
				if($no == 1)
				{
					$uploadFile = '../lms/ptl_storage/file_identitas/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 2)
				{
					$uploadFile = '../lms/ptl_storage/file_identitas/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 3)
				{
					$uploadFile = '../lms/ptl_storage/file_identitas/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				$config['upload_path'] = $uploadFile;
				$config['allowed_types'] = '*';
				$config['max_size'] = '1024000000';// 10240
				$config['remove_spaces'] = true;
				$config['overwrite'] = false;
				$config['file_name'] = $downloadin;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload($field_name))
				{
					$error[] = $this->upload->display_errors();
					$gambar[] = '';
					if($no == 1)
					{
						$pesan .= "Identity File not uploaded. ";
					}
					if($no == 2)
					{
						$pesan .= "Parents Identity File not uploaded. ";
					}
					if($no == 3)
					{
						$pesan .= "Last Diploma File not uploaded. ";
					}
				}
				else
				{ 
					$gambar0 = $this->upload->data();
					$gambar[] = $gambar0['file_name'];
				}
				$no++;
			}
			if($pesan != "")
			{
				echo warning("$pesan","../login/ptl_home");
			}
			else
			{
				$this->m_mahasiswa->PTL_insert_file($MhswID,$gambar,$gambar,$gambar);
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				$EmailMhsw1 = $resmhsw["Email"];
				$EmailMhsw2 = $resmhsw["Email2"];
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$EmailMhsw1,$EmailMhsw2");
				$this->email->bcc("lendra.permana@gmail.com,miha@esmodjakarta.com");
				// $this->email->bcc("lendra.permana@gmail.com");
				$this->email->subject('IDENTITY FILE SUCCESSFULLY UPLOADED (NO REPLY)');
				$message = "
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<h1>IDENTITY FILE SUCCESSFULLY UPLOADED</h1>
					<br/>
					<br/>
					Dear ".$_COOKIE["nim"]." - ".strtoupper($resmhsw['Nama']).",
					<br/>
					<br/>
					Your identity file successfully uploaded.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				";
				$this->email->message($message);
				if($this->email->send())
				{
					echo warning("Your identity file successfully uploaded.","../login/ptl_home");
				}
				else
				{
					$data = array(
								'aplikasi' => 'LMS',
								'from' => 'no-reply@esmodjakarta.com',
								'to' => "$EmailMhsw1,$EmailMhsw2",
								'subject' => 'IDENTITY FILE SUCCESSFULLY UPLOADED',
								'message' => $message,
								'id_akun' => $MhswID,
								'nama' => strtoupper($resmhsw['Nama']),
								'login_buat' => strtoupper($resmhsw['Nama']),
								'tanggal_buat' => $this->waktu
								);
					$this->m_mail->PTL_insert($data);
					echo warning("Email server is not active. Your identity file successfully uploaded.","../login/ptl_home");
				}
			}
		}
		
		function ptl_verification_sms()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);;
			$data['kode_verifikasi'] = $result['kode_verifikasi'];
			$this->load->view('v_verifikasi',$data);
		}
		
		function ptl_verification_sms_check()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$kode_sms = $this->input->post('kode_sms');
			$kode_verifikasi = $this->input->post('kode_verifikasi');
			if($kode_sms == $kode_verifikasi)
			{
				$MhswID = $_COOKIE["nim"];
				$data = array(
							'verifikasi_login' => 'Y'
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				echo warning('Validation Success!','../login/ptl_home');
			}
			else
			{
				echo warning('Invalid OTP!','../login/ptl_verification_sms');
			}
		}
		
		// New Feature
		// function ptl_upload_last_collections()
		// {
			// $this->authentification();
			// $h = "-7";
			// $hm = $h * 60;
			// $ms = $hm * 60;
			// $tgl = gmdate("ymdHis", time()-($ms));
			// $admin_dp = 'ptl_storage/file_last_collections/';
			// if(!is_dir($admin_dp) )
			// {
				// mkdir($admin_dp, DIR_WRITE_MODE);
			// }
			// $download = $_FILES['userfile']['name'];
			// $downloadin = "LAST_COLLECTIONS_".$_COOKIE["nim"]."_".str_replace(' ','_',$_COOKIE["nama"])."_".$tgl."_".str_replace(' ','_',$download);
			
			// $dwn = $downloadin;
			// $config['upload_path'] = $admin_dp;
			// $config['allowed_types'] = '*';
			// $config['max_size'] = '102400';
			// $config['remove_spaces'] = true;
			// $config['overwrite'] = false;
			// $config['file_name'] = $downloadin;
			// $this->upload->initialize($config);
			// $this->load->library('upload', $config);
			// if(!$this->upload->do_upload())
			// {
				// $error = $this->upload->display_errors();
				// $MhswID = $_COOKIE["nim"];
				// $data = array(
							// 'LastCollections' => $error,
							// 'LastCollections_tanggal' => $this->waktu
							// );
				// $this->m_mahasiswa->PTL_update($MhswID,$data);
			// }
			// else
			// {
				// $MhswID = $_COOKIE["nim"];
				// $data = array(
							// 'LastCollections' => $dwn,
							// 'LastCollections_tanggal' => $this->waktu
							// );
				// $this->m_mahasiswa->PTL_update($MhswID,$data);
				// echo warning("Your file successfully uploaded.","../login/ptl_home");
			// }
		// }
		
		function authentification_profile()
		{
			$this->authentification();
			$pesan = "";
			if($this->input->post('Nama') == ""){ $pesan .= "Name not filled. "; }
			if($this->input->post('PresenterID') == ""){ $pesan .= "Marketing not filled. "; }
			if($this->input->post('TempatLahir') == ""){ $pesan .= "Birth Place not filled. "; }
			if($this->input->post('TanggalLahir') == ""){ $pesan .= "Birth Date not filled. "; }
			if($this->input->post('Kelamin') == ""){ $pesan .= "Gender not filled. "; }
			if($this->input->post('WargaNegara') == ""){ $pesan .= "Citizen not filled. "; }
			if($this->input->post('Kebangsaan') == ""){ $pesan .= "Nationality not filled. "; }
			if($this->input->post('Agama') == ""){ $pesan .= "Religion not filled. "; }
			if($this->input->post('StatusSipil') == ""){ $pesan .= "Civil Status not filled. "; }
			if($this->input->post('PendidikanTerakhir') == ""){ $pesan .= "Last Education not filled. "; }
			if($this->input->post('TahunLulus') == ""){ $pesan .= "Graduated Year not filled. "; }
			if($this->input->post('TinggiBadan') == ""){ $pesan .= "Body Height not filled. "; }
			if($this->input->post('BeratBadan') == ""){ $pesan .= "Body Weight not filled. "; }
			if($this->input->post('AnakKe') == ""){ $pesan .= "Child not filled. "; }
			if($this->input->post('JumlahSaudara') == ""){ $pesan .= "Number of Siblings not filled. "; }
			if($this->input->post('Alamat') == ""){ $pesan .= "Current Address not filled. "; }
			if($this->input->post('RT') == ""){ $pesan .= "RT not filled. "; }
			if($this->input->post('RW') == ""){ $pesan .= "RW not filled. "; }
			if($this->input->post('Kota') == ""){ $pesan .= "Current City not filled. "; }
			if($this->input->post('Propinsi') == ""){ $pesan .= "Current Province not filled. "; }
			if($this->input->post('Negara') == ""){ $pesan .= "Current Country not filled. "; }
			if($this->input->post('Handphone') == ""){ $pesan .= "Mobile Phone not filled. "; }
			if($this->input->post('Email') == ""){ $pesan .= "Email 1 not filled. "; }
			if($this->input->post('AlamatAsal') == ""){ $pesan .= "Home Address not filled. "; }
			if($this->input->post('RTAsal') == ""){ $pesan .= "Home RT not filled. "; }
			if($this->input->post('RWAsal') == ""){ $pesan .= "Home RW not filled. "; }
			if($this->input->post('KotaAsal') == ""){ $pesan .= "Home City not filled. "; }
			if($this->input->post('NamaAyah') == ""){ $pesan .= "Father Name not filled. "; }
			if($this->input->post('AgamaAyah') == ""){ $pesan .= "Father Religion not filled. "; }
			if($this->input->post('PendidikanAyah') == ""){ $pesan .= "Father Last Education not filled. "; }
			if($this->input->post('PekerjaanAyah') == ""){ $pesan .= "Father Profession not filled. "; }
			if($this->input->post('AlamatOrtu') == ""){ $pesan .= "Father Address not filled. "; }
			if($this->input->post('KotaOrtu') == ""){ $pesan .= "Father City not filled. "; }
			if($this->input->post('HandphoneOrtu') == ""){ $pesan .= "Father Mobile Phone not filled. "; }
			if($this->input->post('NamaIbu') == ""){ $pesan .= "Mother Name not filled. "; }
			if($this->input->post('AgamaIbu') == ""){ $pesan .= "Mother Religion not filled. "; }
			if($this->input->post('PendidikanIbu') == ""){ $pesan .= "Mother Last Education not filled. "; }
			if($this->input->post('PekerjaanIbu') == ""){ $pesan .= "Mother Profession not filled. "; }
			if($this->input->post('HandphoneIbu') == ""){ $pesan .= "Mother Mobile Phone not filled. "; }
			
			$Email = $this->input->post('Email');
			$Email2 = $this->input->post('Email2');
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			$this->email->bcc("lendra.permana@gmail.com");
			if($pesan == "")
			{
				$h = "-7";
				$hm = $h * 60;
				$ms = $hm * 60;
				$kode = "LEN".gmdate("is", time()-($ms));
				$MhswID = $this->input->post('MhswID');
				$Nama = strtoupper($this->input->post('Nama'));
				$data = array(
							'Nama' => strtoupper($this->input->post('Nama')),
							'PresenterID' => $this->input->post('PresenterID'),
							'TempatLahir' => strtoupper($this->input->post('TempatLahir')),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'WargaNegara' => $this->input->post('WargaNegara'),
							'Kebangsaan' => strtoupper($this->input->post('Kebangsaan')),
							'Agama' => $this->input->post('Agama'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'PendidikanTerakhir' => $this->input->post('PendidikanTerakhir'),
							'TahunLulus' => $this->input->post('TahunLulus'),
							'TinggiBadan' => $this->input->post('TinggiBadan'),
							'BeratBadan' => $this->input->post('BeratBadan'),
							'AnakKe' => $this->input->post('AnakKe'),
							'JumlahSaudara' => $this->input->post('JumlahSaudara'),
							'Alamat' => $this->input->post('Alamat'),
							'RT' => $this->input->post('RT'),
							'RW' => $this->input->post('RW'),
							'Propinsi' => strtoupper($this->input->post('Propinsi')),
							'Kota' => strtoupper($this->input->post('Kota')),
							'KodePos' => $this->input->post('KodePos'),
							'Negara' => strtoupper($this->input->post('Negara')),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => strtolower($this->input->post('Email')),
							'Email2' => strtolower($this->input->post('Email2')),
							'AlamatAsal' => $this->input->post('AlamatAsal'),
							'RTAsal' => $this->input->post('RTAsal'),
							'RWAsal' => $this->input->post('RWAsal'),
							'KotaAsal' => strtoupper($this->input->post('KotaAsal')),
							'KodePosAsal' => $this->input->post('KodePosAsal'),
							'NamaAyah' => strtoupper($this->input->post('NamaAyah')),
							'AgamaAyah' => $this->input->post('AgamaAyah'),
							'PendidikanAyah' => $this->input->post('PendidikanAyah'),
							'PekerjaanAyah' => $this->input->post('PekerjaanAyah'),
							'AlamatOrtu' => $this->input->post('AlamatOrtu'),
							'KotaOrtu' => strtoupper($this->input->post('KotaOrtu')),
							'KodePosOrtu' => $this->input->post('KodePosOrtu'),
							'TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'EmailOrtu' => strtolower($this->input->post('EmailOrtu')),
							'NamaIbu' => strtoupper($this->input->post('NamaIbu')),
							'AgamaIbu' => $this->input->post('AgamaIbu'),
							'PendidikanIbu' => $this->input->post('PendidikanIbu'),
							'PekerjaanIbu' => $this->input->post('PekerjaanIbu'),
							'TeleponIbu' => $this->input->post('TeleponIbu'),
							'HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'EmailIbu' => strtolower($this->input->post('EmailIbu')),
							'verifikasi_kode' => $kode,
							'login_verifikasi' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_verifikasi' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				$this->email->subject('VERIFICATION CODE (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					Dear $MhswID - $Nama,
					<br/>
					<br/>
					<b>ESMOD JAKARTA</b>: Your VERIFICATION CODE is <center><h2><font color='green'>$kode</font></h2></center>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("VERIFICATION CODE has been sent to your email '".$Email."'","../login/ptl_home");
				}
				else
				{
					echo warning('Email server is not active. Data saved...','../login/ptl_home');
				}
			}
			else
			{
				$MhswID = $this->input->post('MhswID');
				$Nama = strtoupper($this->input->post('Nama'));
				$data = array(
							'Nama' => strtoupper($this->input->post('Nama')),
							'PresenterID' => $this->input->post('PresenterID'),
							'TempatLahir' => strtoupper($this->input->post('TempatLahir')),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'WargaNegara' => $this->input->post('WargaNegara'),
							'Kebangsaan' => strtoupper($this->input->post('Kebangsaan')),
							'Agama' => $this->input->post('Agama'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'PendidikanTerakhir' => $this->input->post('PendidikanTerakhir'),
							'TahunLulus' => $this->input->post('TahunLulus'),
							'TinggiBadan' => $this->input->post('TinggiBadan'),
							'BeratBadan' => $this->input->post('BeratBadan'),
							'AnakKe' => $this->input->post('AnakKe'),
							'JumlahSaudara' => $this->input->post('JumlahSaudara'),
							'Alamat' => $this->input->post('Alamat'),
							'RT' => $this->input->post('RT'),
							'RW' => $this->input->post('RW'),
							'Propinsi' => strtoupper($this->input->post('Propinsi')),
							'Kota' => strtoupper($this->input->post('Kota')),
							'KodePos' => $this->input->post('KodePos'),
							'Negara' => strtoupper($this->input->post('Negara')),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => strtolower($this->input->post('Email')),
							'Email2' => strtolower($this->input->post('Email2')),
							'AlamatAsal' => $this->input->post('AlamatAsal'),
							'RTAsal' => $this->input->post('RTAsal'),
							'RWAsal' => $this->input->post('RWAsal'),
							'KotaAsal' => strtoupper($this->input->post('KotaAsal')),
							'KodePosAsal' => $this->input->post('KodePosAsal'),
							'NamaAyah' => strtoupper($this->input->post('NamaAyah')),
							'AgamaAyah' => $this->input->post('AgamaAyah'),
							'PendidikanAyah' => $this->input->post('PendidikanAyah'),
							'PekerjaanAyah' => $this->input->post('PekerjaanAyah'),
							'AlamatOrtu' => $this->input->post('AlamatOrtu'),
							'KotaOrtu' => strtoupper($this->input->post('KotaOrtu')),
							'KodePosOrtu' => $this->input->post('KodePosOrtu'),
							'TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'EmailOrtu' => strtolower($this->input->post('EmailOrtu')),
							'NamaIbu' => strtoupper($this->input->post('NamaIbu')),
							'AgamaIbu' => $this->input->post('AgamaIbu'),
							'PendidikanIbu' => $this->input->post('PendidikanIbu'),
							'PekerjaanIbu' => $this->input->post('PekerjaanIbu'),
							'TeleponIbu' => $this->input->post('TeleponIbu'),
							'HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'EmailIbu' => strtolower($this->input->post('EmailIbu')),
							'login_verifikasi' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_verifikasi' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				$this->email->subject('PROFILE VERIFICATION NOT COMPLETE (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					Dear $MhswID - $Nama,
					<br/>
					<br/>
					<b>ESMOD JAKARTA</b>: Your PROFILE VERIFICATION IS NOT COMPLETE. Please complete your profile and try again.
					<br/>
					<br/>
					Note: $pesan
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("PROFILE VERIFICATION NOT COMPLETE.","../login/ptl_home");
				}
				else
				{
					echo warning('Email server is not active. Data saved, but not complete...','../login/ptl_home');
				}
			}
		}
		
		function ptl_resend()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$Nama = $result['Nama'];
			$kode = $result['verifikasi_kode'];
			$Email = $result['Email'];
			$Email2 = $result['Email2'];
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			$this->email->bcc("lendra.permana@gmail.com");
			$this->email->subject('RESEND VERIFICATION CODE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>LMS</h2></font>
				</center>
				<br/>
				Dear $MhswID - $Nama,
				<br/>
				<br/>
				<b>ESMOD JAKARTA</b>: Your VERIFICATION CODE is <center><h2><font color='green'>$kode</font></h2></center>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("VERIFICATION CODE has been sent to your email '".$Email."'","../login/ptl_home");
			}
			else
			{
				echo warning('Email server is not active. Data saved...','../login/ptl_home');
			}
		}
		
		function authentification_code()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$verifikasi_kode = $this->input->post('verifikasi_kode');
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['verifikasi_kode'] == $verifikasi_kode)
			{
				$data = array(
							'verifikasi' => 'Y',
							'login_verifikasi' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_verifikasi' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				$MhswID = $result['MhswID'];
				$Nama = $result['Nama'];
				$Email = $result['Email'];
				$Email2 = $result['Email2'];
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$Email");
				$this->email->cc("$Email2");
				$this->email->bcc("lendra.permana@gmail.com,miha@esmodjakarta.com");
				$this->email->subject('PROFILE VERIFICATION (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					<b>ESMOD JAKARTA</b>: Your data has been updated.
					<br/>
					<center><h2><font color='green'>$MhswID - $Nama</font></h2></center>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("The code is successfully verified.","../login/ptl_home");
				}
				else
				{
					echo warning('Email server is not active. The code is successfully verified...','../login/ptl_home');
				}
			}
			else
			{
				echo warning("Verification code is incorrect.","../login/ptl_home");
			}
		}
		
		function ptl_photo()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$MhswID = $this->input->post('MhswID');
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$admin_dp = '../academic/ptl_storage/foto_mahasiswa';
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$info_dwn = pathinfo($download);
			$asal_url = str_replace(' ','_',strtolower($download));
			$url = hapus_simbol($asal_url);
			$downloadin = "FOTO_".$MhswID."_".str_replace(' ','_',$this->input->post('Nama'))."_".$tgl."_".$url.".".@$info_dwn["extension"];
			
			$dwn = $downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '20480';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->upload->initialize($config);
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$pesan = $this->upload->display_errors();
				echo warning("Your photo not set. Please upload your photo.","../login/ptl_home");
			}
			else
			{
				$source_photo = "$admin_dp/$downloadin";
				$ukuran =  fsize($source_photo);
				$word = explode(" ",$ukuran);
				$pesan_file = "";
				$ukuran_akhir = "";
				if(($word[1] == "KB") OR ($word[1] == "MB"))
				{
					$pesan_file .= " File memenuhi syarat upload kurang dari 10 MB.";
					if(($word[1] == "KB") AND ($word[0] > 128))
					{
						$info = pathinfo($source_photo);
						$pesan_file.= " File perlu di kompresi karena lebih dari 512 KB.";
						$destination_photo = $source_photo."_compress.".$info["extension"];
						compress_image($source_photo,$destination_photo,30);
						$ukuran_akhir = " Ukuran akhir ".fsize($destination_photo).". File ukuran sebelumnya berhasil diarsip.";
						// unlink("./$source_photo");
						$info2 = pathinfo($destination_photo);
						$dwn = $info2["filename"].".".$info2["extension"];
					}
					else
					{
						if(($word[1] == "MB") AND ($word[0] > 1))
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " File perlu di kompresi karena lebih dari 1 MB.";
							$destination_photo = $source_photo."_compress.".$info["extension"];
							compress_image($source_photo,$destination_photo,30);
							$ukuran_akhir = " Ukuran akhir ".fsize($destination_photo).". File ukuran sebelumnya berhasil diarsip.";
							// unlink("./$source_photo");
							$info2 = pathinfo($destination_photo);
							$dwn = $info2["filename"].".".$info2["extension"];
						}
						else
						{
							$pesan_file .= " File tidak perlu di kompresi.";
						}
					}
					$status_file = "Y";
				}
				else
				{
					if($word[1] == "B")
					{
						$pesan_file .=  " File terlalu kecil. $ukuran";
					}
					else
					{
						$pesan_file .=  " File terlalu besar.";
					}
					$status_file = "N";
				}
				if($status_file == "N")
				{
					echo warning("Your photo not updated.$pesan_file$ukuran_akhir","../login/ptl_home");
				}
				else
				{
					setcookie("foto", $dwn, time()+28800, "/");
					$data = array(
								'Foto' => $dwn,
								'status_foto' => 'Y',
								'login_edit' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_mahasiswa->PTL_update($MhswID,$data);
					$result = $this->m_mahasiswa->PTL_select($MhswID);
					$Nama = $result['Nama'];
					$Email = $result['Email'];
					$Email2 = $result['Email2'];
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to("$Email");
					$this->email->cc("$Email2");
					$this->email->bcc("lendra.permana@gmail.com");
					$this->email->subject('PROFILE PHOTO UPDATED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>LMS</h2></font>
						</center>
						<br/>
						Dear $MhswID - $Nama,
						<br/>
						<br/>
						<b>ESMOD JAKARTA</b>: Your PHOTO was updated.
						<br/>
						<br/>
						<center>
							<img src='http://app.esmodjakarta.com/academic/ptl_storage/foto_mahasiswa/$dwn' style='max-width:600px; width:100%'/>
						</center>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Your photo successfully updated.$pesan_file$ukuran_akhir","../login/ptl_home");
					}
					else
					{
						echo warning("Your photo successfully updated.$pesan_file$ukuran_akhir. Email server is not active. Data saved...","../login/ptl_home");
					}
				}
			}
		}
		
		function authentification_language()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$KHSID = $this->input->post('KHSID');
			$languageID = $this->input->post('languageID');
			if($languageID == "")
			{
				echo warning("You must choose LANGUAGE SPECIALIZATION.","../login/ptl_home");
			}
			else
			{
				$result = $this->m_mahasiswa->PTL_select($MhswID);
				$data = array(
							'languageID' => $languageID
							);
				$this->m_khs->PTL_update($KHSID,$data);
				if($languageID == "ENG")
				{
					$ChooseLang = "ENGLISH";
					$lang = "FRENCH";
				}
				if($languageID == "FRE")
				{
					$ChooseLang = "FRENCH";
					$lang = "ENGLISH";
				}
				$rowrecord = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
				if($rowrecord)
				{
					foreach($rowrecord as $row)
					{
						$SubjekID = $row->SubjekID;
						$resub = $this->m_subjek->PTL_select($SubjekID);
						if($resub)
						{
							if(strpos(strtoupper($resub['Nama']),$lang) !== false)
							{
								$KRSID = $row->KRSID;
								$this->m_krs->PTL_delete_krs($KRSID);
							}
						}
					}
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$Nama = $result['Nama'];
				$Email = $result['Email'];
				$Email2 = $result['Email2'];
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$Email");
				$this->email->cc("$Email2");
				$this->email->bcc("lendra.permana@gmail.com,miha@esmodjakarta.com");
				$this->email->subject('LANGUAGE VERIFICATION (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					<b>ESMOD JAKARTA</b>: Your data has been updated.
					<br/>
					<center><h2><font color='green'>$MhswID - $Nama</font></h2></center>
					<br/>
					<br/>
					<center><h2><font color='blue'>LANGUAGE SPECIALIZATION - $ChooseLang</font></h2></center>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("The LANGUAGE SPECIALIZATION is successfully verified.","../login/ptl_home");
				}
				else
				{
					echo warning('Email server is not active. Data LANGUAGE SPECIALIZATION saved...','../login/ptl_home');
				}
			}
		}
		
		function ptl_home_detail()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$this->authentification();
			$this->suspended();
			$this->session->set_userdata('menu','home');
			$BeritaID = $this->uri->segment(3);
			$result = $this->m_berita->PTL_select($BeritaID);
			$data['BeritaID'] = $result['BeritaID'];
			$data['JudulBerita'] = $result['JudulBerita'];
			$data['IsiBerita'] = $result['IsiBerita'];
			$data['UploadFile'] = $result['UploadFile'];
			$this->load->view('Portal/v_header');
			$this->load->view('Beranda/v_home_detail',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_home_pesan_detail()
		{
			$this->authentification();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms != 'logged') 
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','home');
			$psn = $this->uri->segment(4);
			if($psn == "PSN")
			{
				$MhswID = $_COOKIE["nim"];
				$PesanID = $this->uri->segment(3);
				$da = array(
							'Bacda' => 'Y'
							);
				$this->m_pesan_mhsw->PTL_update2($MhswID,$PesanID,$da);
				$result = $this->m_pesan->PTL_select($PesanID);
				$data['JudulBerita'] = $result['JudulPesan'];
				$data['IsiBerita'] = $result['IsiPesan'];
				$data['UploadFile'] = '';
			}
			else
			{
				$noteid = $this->uri->segment(3);
				$da = array(
							'baca' => 'Y'
							);
				$this->m_catatan2->PTL_update2($noteid,$da);
				$result = $this->m_catatan2->PTL_select($noteid);
				$data['JudulBerita'] = $result['title'];
				$data['IsiBerita'] = $result['comment'];
				$data['datemeet'] = "<br/><br/>Meeting Date : ".$result['datemeet'];
				$data['UploadFile'] = '';
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Beranda/v_home_detail',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_download()
		{
			$this->authentification();
			$this->suspended();
			$filelink = $this->uri->segment(3);
			$BeritaID = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$exist = file_exists_remote(base_url("../academic/ptl_storage/berita/".$direktori."/".$file));
				if($exist)
				{
					$data = file_get_contents("../academic/ptl_storage/berita/".$direktori."/".$file);
					force_download($file,$data);
				}
				else
				{
					echo warning('File does not exist in directory...',"../login/ptl_home_detail/$BeritaID");
				}
			}
			else
			{
				echo warning('File does not exist...',"../login/ptl_home_detail/$BeritaID");
			}
		}
		
		function ptl_rating()
		{
			$this->authentification();
			$word = explode("-",$this->input->post('rate'));
			$rate = $word[0];
			$id_rating_question = $word[1];
			$id_rating_question_grup = $word[2];
			$TahunID = $word[3];
			$KHSID = $word[4];
			$ProgramID = $word[5];
			$ProdiID = $word[6];
			$TahunKe = $word[7];
			$KelasID = $word[8];
			$Sesi = $word[9];
			$JadwalID = $word[10];
			$SubjekID = $word[11];
			$DosenID = $word[12];
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_rating->PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID);
			if($result)
			{
				$id_rating = $result['id_rating'];
				$data = array(
							'rate' => $rate,
							'login_edit' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_rating->PTL_update($id_rating,$data);
			}
			else
			{
				$kd = "RTG".$_COOKIE["nim"];
				$result = $this->m_rating->PTL_urut($kd);
				$lastid = $result['LAST'];
				$lastnourut = substr($lastid,15,5);
				$nextnourut = $lastnourut + 1;
				$id_rating = $kd.sprintf('%05s',$nextnourut);
				
				$kd2 = "GRT".$_COOKIE["nim"];
				$result2 = $this->m_rating_grup->PTL_urut($kd2);
				$lastid2 = $result2['LAST'];
				$lastnourut2 = substr($lastid2,15,5);
				$nextnourut2 = $lastnourut2 + 1;
				$id_rating_grup = $kd2.sprintf('%05s',$nextnourut2);
				$data = array(
							'id_rating' => $id_rating,
							'id_rating_grup' => $id_rating_grup,
							'id_rating_question' => $id_rating_question,
							'id_rating_question_grup' => $id_rating_question_grup,
							'MhswID' => $MhswID,
							'TahunID' => $TahunID,
							'KHSID' => $KHSID,
							'ProgramID' => $ProgramID,
							'ProdiID' => $ProdiID,
							'TahunKe' => $TahunKe,
							'Sesi' => $Sesi,
							'KelasID' => $KelasID,
							'JadwalID' => $JadwalID,
							'SubjekID' => $SubjekID,
							'DosenID' => $DosenID,
							'rate' => $rate,
							'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_rating->PTL_insert($data);
			}
		}
		
		function ptl_rating_grup()
		{
			$this->authentification();
			$MhswID = $_COOKIE["nim"];
			$TahunID = $this->uri->segment(3);
			$KHSID = $this->uri->segment(4);
			$JadwalID = $this->uri->segment(5);
			$SubjekID = $this->uri->segment(6);
			$ProgramID = $this->uri->segment(7);
			$ProdiID = $this->uri->segment(8);
			$TahunKe = $this->uri->segment(9);
			$Sesi = $this->uri->segment(10);
			$KelasID = $this->uri->segment(11);
			$JumlahDosen = $this->uri->segment(12);
			$JumlahSoal = $this->uri->segment(13);
			$id_rating_question_grup = $this->uri->segment(14);
			$res = $this->m_rating->PTL_all_spesifik($MhswID,$TahunID,$KHSID,$JadwalID,$SubjekID);
			$TotalAnswer = count($res);
			$TotalQuestion = $JumlahDosen * $JumlahSoal;
			$kurang = $TotalQuestion - $TotalAnswer;
			if($TotalQuestion == $TotalAnswer)
			{
				$kd2 = "GRT".$_COOKIE["nim"];
				$result2 = $this->m_rating_grup->PTL_urut($kd2);
				$lastid2 = $result2['LAST'];
				$lastnourut2 = substr($lastid2,15,5);
				$nextnourut2 = $lastnourut2 + 1;
				$id_rating_grup = $kd2.sprintf('%05s',$nextnourut2);
				
				$kd3 = "GGR".$_COOKIE["nim"];
				$result3 = $this->m_rating_grup_all->PTL_urut($kd3);
				$lastid3 = $result3['LAST'];
				$lastnourut3 = substr($lastid3,15,5);
				$nextnourut3 = $lastnourut3 + 1;
				$id_rating_grup_all = $kd3.sprintf('%05s',$nextnourut3);
				$data = array(
							'id_rating_grup' => $id_rating_grup,
							'id_rating_grup_all' => $id_rating_grup_all,
							'id_rating_question_grup' => $id_rating_question_grup,
							'MhswID' => $MhswID,
							'TahunID' => $TahunID,
							'KHSID' => $KHSID,
							'ProgramID' => $ProgramID,
							'ProdiID' => $ProdiID,
							'TahunKe' => $TahunKe,
							'Sesi' => $Sesi,
							'KelasID' => $KelasID,
							'JadwalID' => $JadwalID,
							'SubjekID' => $SubjekID,
							'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_rating_grup->PTL_insert($data);
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$NamaSub = "";
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$reskrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
				$TotalKRS = count($reskrs);
				$resrating = $this->m_rating_grup->PTL_all_spesifik($id_rating_question_grup,$MhswID,$KHSID);
				$TotalRating = count($resrating);
				if($TotalRating == $TotalKRS)
				{
					$data = array(
								'id_rating_grup_all' => $id_rating_grup_all,
								'id_rating_question_grup' => $id_rating_question_grup,
								'MhswID' => $MhswID,
								'TahunID' => $TahunID,
								'KHSID' => $KHSID,
								'ProgramID' => $ProgramID,
								'ProdiID' => $ProdiID,
								'TahunKe' => $TahunKe,
								'Sesi' => $Sesi,
								'KelasID' => $KelasID,
								'JadwalID' => $JadwalID,
								'SubjekID' => $SubjekID,
								'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_rating_grup_all->PTL_insert($data);
					$result = $this->m_mahasiswa->PTL_select($MhswID);
					$MhswID = $result['MhswID'];
					$Nama = $result['Nama'];
					$Email = $result['Email'];
					$Email2 = $result['Email2'];
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to("$Email");
					$this->email->cc("$Email2");
					$this->email->bcc("lendra.permana@gmail.com,miha@esmodjakarta.com");
					$this->email->subject('RATING WAS COMPLETED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>LMS</h2></font>
						</center>
						<br/>
						<b>ESMOD JAKARTA</b>: Your rating for this semester was completed.
						<br/>
						<center><h2><font color='green'>$MhswID - $Nama</font></h2></center>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Rating for '$NamaSub' successfully saved. Rating for this semester was completed.","../login/ptl_home");
					}
					else
					{
						echo warning("Email server is not active. Rating for '$NamaSub' successfully saved. Rating for this semester was completed.","../login/ptl_home");
					}
				}
				else
				{
					echo warning("Rating for '$NamaSub' successfully saved.","../login/ptl_home");
				}
			}
			else
			{
				echo warning("You do not complete answers. $kurang question not answer.","../login/ptl_home");
			}
		}
		
		function ptl_maintenance()
		{
			$this->load->view('v_maintenance');
		}
		
		// Coba
		// function coba()
		// {
			// $rowrecord = $this->m_rating_grup->coba();
			// if($rowrecord)
			// {
				// foreach($rowrecord as $row)
				// {
					// // echo $row->MhswID."<br/>";
					// $kd3 = "GGR".$row->MhswID;
					// $result3 = $this->m_rating_grup_all->PTL_urut($kd3);
					// $lastid3 = $result3['LAST'];
					// $lastnourut3 = substr($lastid3,15,5);
					// $nextnourut3 = $lastnourut3 + 1;
					// $id_rating_grup_all = $kd3.sprintf('%05s',$nextnourut3);
					
					// $MhswID = $row->MhswID;
					// $rowrec = $this->m_rating_grup->coba_ok($MhswID);
					// if($rowrec)
					// {
						// foreach($rowrec as $r)
						// {
							// echo "&nbsp;&nbsp;&nbsp;&nbsp;".$id_rating_grup_all."<br/>";
							// $da = array(
										// 'id_rating_grup_all' => $id_rating_grup_all
										// );
							// $this->m_rating_grup->update($MhswID,$da);
						// }
					// }
					// $data = array(
								// 'id_rating_grup_all' => $id_rating_grup_all,
								// 'MhswID' => $row->MhswID,
								// 'TahunID' => $row->TahunID,
								// 'KHSID' => $row->KHSID,
								// 'ProgramID' => $row->ProgramID,
								// 'ProdiID' => $row->ProdiID,
								// 'TahunKe' => $row->TahunKe,
								// 'Sesi' => $row->Sesi,
								// 'KelasID' => $row->KelasID,
								// 'JadwalID' => $row->JadwalID,
								// 'SubjekID' => $row->SubjekID,
								// 'login_buat' => $row->login_buat,
								// 'tanggal_buat' => $row->tanggal_buat
								// );
					// // print_r($data);
					// // echo "<br/><br/><br/>";
					// $this->m_rating_grup_all->PTL_insert($data);
				// }
			// }
		// }
	}
?>