<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Notification extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms!=='logged')
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_notifikasi_lms');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED. Please contact IT Team.','../login/ptl_suspend');
			}
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','');
			$MhswID = $_COOKIE["nim"];
			$data['rowrecord'] = $this->m_notifikasi_lms->PTL_all_active($MhswID);
			$this->load->view('Portal/v_header');
			$this->load->view('Notification/v_notification',$data);
			$this->load->view('Portal/v_footer');
		}
	}
?>