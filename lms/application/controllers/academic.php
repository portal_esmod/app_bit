<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Academic extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms!=='logged')
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('tanggal');
			$this->load->model('m_akun');
			$this->load->model('m_dosen');
			$this->load->model('m_exam');
			$this->load->model('m_exam_dosen');
			$this->load->model('m_exam_kursi');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_hari');
			$this->load->model('m_jadwal');
			$this->load->model('m_jadwal_hari');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kalender');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_krs2');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_nilai');
			$this->load->model('m_nilai3');
			$this->load->model('m_notifikasi_lms');
			$this->load->model('m_mk');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_remedial_krs');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_subjek');
			$this->load->model('m_status');
			$this->load->model('m_year');
			$this->load->model('m_year_detail');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED. Please contact IT Team.','../login/ptl_suspend');
			}
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_acd()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			if($cekkhs != "")
			{
				$this->session->set_userdata('acd_filter_acd',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('acd_filter_acd');
			}
			redirect("academic");
		}
		
		function ptl_filter_scd()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			if($cekkhs != "")
			{
				$this->session->set_userdata('acd_filter_scd',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('acd_filter_scd');
			}
			redirect("academic/schedule");
		}
		
		function ptl_filter_att()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			if($cekkhs != "")
			{
				$this->session->set_userdata('acd_filter_att',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('acd_filter_att');
			}
			redirect("academic/attendance");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$MhswID = $_COOKIE["nim"];
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$res = $this->m_khs->PTL_select_max($MhswID);
			$data['tahun'] = $res['tahun'];
			if($this->session->userdata('acd_filter_acd') == "")
			{
				$TahunID = $res['tahun'];
			}
			else
			{
				$TahunID = $this->session->userdata('acd_filter_acd');
			}
			$result = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			if($result)
			{
				$ProgramID = $result['ProgramID'];
				$ProdiID = $result['ProdiID'];
				$KelasID = $result['KelasID'];
				$data['suspend'] = $result['suspend'];
			}
			else
			{
				$ProgramID = '';
				$ProdiID = '';
				$KelasID = '';
				$data['suspend'] = '';
			}
			$result2 = $this->m_program->PTL_select($ProgramID);
			if($result2)
			{
				$data['ProgramID'] = $result['ProgramID'].' - '.$result2['Nama'];
			}
			else
			{
				$data['ProgramID'] = '';
			}
			$result3 = $this->m_prodi->PTL_select($ProdiID);
			if($result3)
			{
				$data['ProdiID'] = $result['ProdiID'].' - '.$result3['Nama'];
			}
			else
			{
				$data['ProdiID'] = '';
			}
			$result4 = $this->m_kelas->PTL_select_kelas($KelasID);
			if($result4)
			{
				if($ProgramID == "INT")
				{
					$data['TahunKe'] = 'O'.$result4['Nama'];
				}
				else
				{
					$data['TahunKe'] = $result['TahunKe'].$result4['Nama'];
				}
			}
			else
			{
				$data['TahunKe'] = '';
			}
			$result5 = $this->m_year->PTL_select($TahunID);
			if($result5)
			{
				$data['Enrollment'] = $result5['Enrollment'];
				$data['EnrollmentEnd'] = $result5['EnrollmentEnd'];
				$data['Specialization'] = $result5['Specialization'];
				$data['SpecializationEnd'] = $result5['SpecializationEnd'];
				$data['Postpone'] = $result5['Postpone'];
				$data['PostponeEnd'] = $result5['PostponeEnd'];
				$data['Academic'] = $result5['Academic'];
				$data['AcademicEnd'] = $result5['AcademicEnd'];
				$data['Payment'] = $result5['Payment'];
				$data['PaymentEnd'] = $result5['PaymentEnd'];
			}
			else
			{
				$data['Enrollment'] = '';
				$data['EnrollmentEnd'] = '';
				$data['Specialization'] = '';
				$data['SpecializationEnd'] = '';
				$data['Postpone'] = '';
				$data['PostponeEnd'] = '';
				$data['Academic'] = '';
				$data['AcademicEnd'] = '';
				$data['Payment'] = '';
				$data['PaymentEnd'] = '';
			}
			$data['rowrecord'] = $this->m_year_detail->PTL_all_spesifik($TahunID,$ProdiID);
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_academic',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function calendar()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$data['rowrecord'] = $this->m_kalender->PTL_all_tahun_ini();
			$this->load->view('Portal/v_header_kalender');
			$this->load->view('Academic/v_calendar');
			$this->load->view('Portal/v_footer_kalender',$data);
		}
		
		function ptl_filter_tanggal()
		{
			$this->authentification();
			$cetanggal = $this->input->post('cetanggal');
			if($cetanggal != "")
			{
				$this->session->set_userdata('lms_schedule_filter_tanggal',$cetanggal);
			}
			else
			{
				$this->session->unset_userdata('lms_schedule_filter_tanggal');
			}
			redirect("academic/schedule");
		}
		
		function ptl_filter_unset_tanggal()
		{
			$this->authentification();
			$this->session->unset_userdata('lms_schedule_filter_tanggal');
			redirect("academic/schedule");
		}
		
		function schedule()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$MhswID = $_COOKIE["nim"];
			$data['MhswID'] = $MhswID;
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$result2 = $this->m_khs->PTL_select_max($MhswID);
			$data['tahun'] = $result2['tahun'];
			$cekkhs = $this->session->userdata('acd_filter_scd');
			if($cekkhs == "")
			{
				$TahunID = $result2['tahun'];
			}
			else
			{
				$TahunID = $cekkhs;
			}
			$result = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$data['tahunacd'] = $TahunID;
			if($result)
			{
				$data['TahunID'] = $result['TahunID'];
				$data['ProgramID'] = $result['ProgramID'];
				$data['KelasID'] = $result['KelasID'];
				$data['Sesi'] = $result['Sesi'];
				$data['suspend'] = $result['suspend'];
			}
			else
			{
				$data['TahunID'] = '';
				$data['ProgramID'] = '';
				$data['KelasID'] = '';
				$data['Sesi'] = '';
				$data['suspend'] = '';
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cetanggal = $this->session->userdata('lms_schedule_filter_tanggal');
			if($cetanggal == "")
			{
				$tanggal = gmdate("Y-m-d", time()-($ms));
				$hari = gmdate("D", time()-($ms));
			}
			else
			{
				$tanggal = $cetanggal;
				$hari = date('D', strtotime($tanggal));
			}
			if($hari == 'Mon')
			{
				$TanggalAwal = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+3 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+4 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+4 days', strtotime($tanggal)));
			}
			if($hari == 'Tue')
			{
				$TanggalAwal = date('Y-m-d', strtotime('-1 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('-1 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+3 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+3 days', strtotime($tanggal)));
			}
			if($hari == 'Wed')
			{
				$TanggalAwal = date('Y-m-d', strtotime('-2 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('-2 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('-1 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
			}
			if($hari == 'Thu')
			{
				$TanggalAwal = date('Y-m-d', strtotime('-3 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('-3 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('-2 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('-1 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
			}
			if($hari == 'Fri')
			{
				$TanggalAwal = date('Y-m-d', strtotime('-4 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('-4 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('-3 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('-2 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('-1 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+0 days', strtotime($tanggal)));
			}
			if($hari == 'Sat')
			{
				$TanggalAwal = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('+3 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('+4 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+5 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+6 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+6 days', strtotime($tanggal)));
			}
			if($hari == 'Sun')
			{
				$TanggalAwal = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$data['D1'] = date('Y-m-d', strtotime('+1 days', strtotime($tanggal)));
				$data['D2'] = date('Y-m-d', strtotime('+2 days', strtotime($tanggal)));
				$data['D3'] = date('Y-m-d', strtotime('+3 days', strtotime($tanggal)));
				$data['D4'] = date('Y-m-d', strtotime('+4 days', strtotime($tanggal)));
				$data['D5'] = date('Y-m-d', strtotime('+5 days', strtotime($tanggal)));
				$TanggalAkhir = date('Y-m-d', strtotime('+5 days', strtotime($tanggal)));
			}
			
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_schedule',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$MhswID = $_COOKIE["nim"];
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$result2 = $this->m_khs->PTL_select_max($MhswID);
			$data['tahun'] = $result2['tahun'];
			$cekkhs = $this->session->userdata('acd_filter_att');
			if($cekkhs == "")
			{
				$TahunID = $result2['tahun'];
			}
			else
			{
				$TahunID = $cekkhs;
			}
			$result = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			if($result)
			{
				$KHSID = $result['KHSID'];
				$ProgramID = $result['ProgramID'];
				$ProdiID = $result['ProdiID'];
				$KelasID = $result['KelasID'];
				$data['suspend'] = $result['suspend'];
			}
			else
			{
				$KHSID = '';
				$ProgramID = '';
				$ProdiID = '';
				$KelasID = '';
				$data['suspend'] = '';
			}
			$res2 = $this->m_program->PTL_select($ProgramID);
			$data['ProgramID'] = $result['ProgramID'].' - '.$res2['Nama'];
			$res3 = $this->m_prodi->PTL_select($ProdiID);
			$data['ProdiID'] = $result['ProdiID'].' - '.$res3['Nama'];
			$res4 = $this->m_kelas->PTL_select_kelas($KelasID);
			if($ProgramID == "INT")
			{
				if($res4)
				{
					$data['TahunKe'] = 'O'.$res4['Nama'];
				}
				else
				{
					$data['TahunKe'] = "O<font color='red'> Class not set</font>";
				}
			}
			else
			{
				if($res4)
				{
					$data['TahunKe'] = $result['TahunKe'].$res4['Nama'];
				}
				else
				{
					$data['TahunKe'] = $result['TahunKe']."<font color='red'> Class not set</font>";
				}
			}
			$data['rowrecord'] = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_attendance',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_download_file()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/file_student/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo "File does not exist...";
			}
		}
		
		function attendance_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$JadwalID = $this->uri->segment(3);
			$KRSID = $this->uri->segment(4);
			$id_notifikasi_lms = $this->uri->segment(5);
			$notif = $this->uri->segment(6);
			if($notif != "")
			{
				$datanotif = array(
								'login_baca' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
								'tanggal_baca' => $this->waktu
								);
				$this->m_notifikasi_lms->PTL_update($id_notifikasi_lms,$datanotif);
			}
			$data['KRSID'] = $KRSID;
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$SubjekID = $result['SubjekID'];
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$data['NamaSubjek'] = "";
			if($ressubjek)
			{
				$data['NamaSubjek'] = $ressubjek['Nama'];
			}
			$data['file_project'] = $result['file_project'];
			$data['file_evaluation'] = $result['file_evaluation'];
			$data['NamaDosenID'] = '';
			$data['NamaDosenID2'] = '';
			$data['NamaDosenID3'] = '';
			$data['NamaDosenID4'] = '';
			$data['NamaDosenID5'] = '';
			$data['NamaDosenID6'] = '';
			if($result)
			{
				$DosenID = $result["DosenID"];
				$res1 = $this->m_akun->PTL_select_dosen($DosenID);
				$DosenID = $result["DosenID2"];
				$res2 = $this->m_akun->PTL_select_dosen($DosenID);
				$DosenID = $result["DosenID3"];
				$res3 = $this->m_akun->PTL_select_dosen($DosenID);
				$DosenID = $result["DosenID4"];
				$res4 = $this->m_akun->PTL_select_dosen($DosenID);
				$DosenID = $result["DosenID5"];
				$res5 = $this->m_akun->PTL_select_dosen($DosenID);
				$DosenID = $result["DosenID6"];
				$res6 = $this->m_akun->PTL_select_dosen($DosenID);
				if($res1)
				{
					$data['NamaDosenID'] = $res1['nama'];
					$data['FotoDosenID'] = $res1['foto'];
				}
				if($res2)
				{
					$data['NamaDosenID2'] = $res2['nama'];
					$data['FotoDosenID2'] = $res2['foto'];
				}
				if($res3)
				{
					$data['NamaDosenID3'] = $res3['nama'];
					$data['FotoDosenID3'] = $res3['foto'];
				}
				if($res4)
				{
					$data['NamaDosenID4'] = $res4['nama'];
					$data['FotoDosenID4'] = $res4['foto'];
				}
				if($res5)
				{
					$data['NamaDosenID5'] = $res5['nama'];
					$data['FotoDosenID5'] = $res5['foto'];
				}
				if($res6)
				{
					$data['NamaDosenID6'] = $res6['nama'];
					$data['FotoDosenID6'] = $res6['foto'];
				}
			}
			$data['rowrecord'] = $this->m_presensi->PTL_all_select($JadwalID);
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_attendance_detail',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function scoring()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$MhswID = $_COOKIE["nim"];
			$rowrecord = $this->m_khs->PTL_all_select($MhswID);
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$ProgramID = $row->ProgramID;
					$MhswID = $row->MhswID;
					$KHSID = $row->KHSID;
					$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
					if($rowkrs)
					{
						foreach($rowkrs as $rkr)
						{
							$TahunID = $rkr->TahunID;
							$JadwalID = $rkr->JadwalID;
							$SubjekID = $rkr->SubjekID;
							$KRSID = $rkr->KRSID;
							$rowsub = $this->m_subjek->PTL_select($SubjekID);
							$KurikulumID = "";
							$JenisMKID = "";
							if($rowsub)
							{
								$KurikulumID = $rowsub['KurikulumID'];
								$JenisMKID = $rowsub['JenisMKID'];
							}
							$rowmk = $this->m_mk->PTL_all_select($SubjekID);
							if($rowmk)
							{
								$NilAkhir = 0;
								$NilBagi = 0;
								foreach($rowmk as $rm)
								{
									if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 28) OR ($TahunID == 29))
									{
										if($JenisMKID == "5")
										{
											$PresensiID = "";
											$CekJenisPresensiID = "";
										}
										$MKID = $rm->MKID;
										$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
										$Nilai = 0;
										$AddNilai = 0;
										if($rowkrs2)
										{
											$totNilai = 0;
											$nkrs2 = 0;
											foreach($rowkrs2 as $rkrs2)
											{
												if($JenisMKID == "5")
												{
													$PresensiID = $rkrs2->PresensiID;
													$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
													if($resscoring)
													{
														$CekJenisPresensiID = $resscoring['JenisPresensiID'];
													}
												}
												if($rkrs2->Nilai > 0)
												{
													$totNilai = $totNilai + $rkrs2->Nilai;
													$nkrs2++;
												}
												$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
											}
											if($nkrs2 > 0)
											{
												$Nilai = $totNilai / $nkrs2;
											}
										}
										$tn = $Nilai + $AddNilai;
										if($rowkrs2)
										{
											if($rm->Optional == "Y")
											{
												if($tn != 0)
												{
													$NilAkhir = $NilAkhir + $tn;
													$NilBagi++;
												}
											}
											else
											{
												$NilAkhir = $NilAkhir + $tn;
												if($JenisMKID == "5")
												{
													if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
													{
														
													}
													else
													{
														$NilBagi++;
													}
												}
												else
												{
													$NilBagi++;
												}
											}
										}
									}
									else
									{
										if($JenisMKID == "5")
										{
											$PresensiID = "";
											$CekJenisPresensiID = "";
										}
										$CekMKID = $rm->MKID;
										$rescekmk = $this->m_presensi->PTL_select_cek_mk($JadwalID,$CekMKID);
										if($rescekmk)
										{
											$MKID = $rm->MKID;
											$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
											$Nilai = 0;
											$AddNilai = 0;
											if($rowkrs2)
											{
												$totNilai = 0;
												$nkrs2 = 0;
												foreach($rowkrs2 as $rkrs2)
												{
													if($JenisMKID == "5")
													{
														$PresensiID = $rkrs2->PresensiID;
														$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
														if($resscoring)
														{
															$CekJenisPresensiID = $resscoring['JenisPresensiID'];
														}
													}
													if($rkrs2->Nilai > 0)
													{
														$totNilai = $totNilai + $rkrs2->Nilai;
														$nkrs2++;
													}
													$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
												}
												if($nkrs2 > 0)
												{
													$Nilai = $totNilai / $nkrs2;
												}
											}
											$tn = $Nilai + $AddNilai;
											if($rowkrs2)
											{
												if($rm->Optional == "Y")
												{
													if($tn != 0)
													{
														$NilAkhir = $NilAkhir + $tn;
														$NilBagi++;
													}
												}
												else
												{
													$NilAkhir = $NilAkhir + $tn;
													if($JenisMKID == "5")
													{
														if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
														{
															
														}
														else
														{
															$NilBagi++;
														}
													}
													else
													{
														$NilBagi++;
													}
												}
											}
										}
									}
								}
								$mp = 0;
								if($NilBagi != 0)
								{
									if($JenisMKID == "5")
									{
										$rowscore = $this->m_presensi_mahasiswa->PTL_all_sum($KRSID,$MhswID);
										if($rowscore)
										{
											$JMP = 0;
											$TMP = 0;
											foreach($rowscore as $rs)
											{
												$JMP = $JMP + $rs->Score;
												$TMP++;
											}
											if($JMP > 0)
											{
												$mp = ($JMP / $TMP);
											}
										}
									}
									else
									{
										$JenisMKID = 4;
										$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
										if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
										$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
										if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
										$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
										if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
										$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
										if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
										$JenisPresensiID = "E";
										$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
										$tottexc = $texc * $pr1;
										$JenisPresensiID = "S";
										$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
										$tottsic = $tsic * $pr2;
										$JenisPresensiID = "A";
										$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
										$tottabs = $tabs * $pr3;
										$JenisPresensiID = "L";
										$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
										$tottlat = $tlat * $pr4;
										$mp = $tottexc + $tottsic + $tottabs + $tottlat;
									}
									$exam = 0;
									$ExamID = "";
									$TotJum = "";
									$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
									if($rexmhsw1)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw1 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA1)
											{
												if($rexmhswA1['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA1['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
									if($rexmhsw2)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw2 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA2)
											{
												if($rexmhswA2['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA2['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
									if($rexmhsw3)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw3 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA3)
											{
												if($rexmhswA3['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA3['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
									if($rexmhsw4)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw4 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA4)
											{
												if($rexmhswA4['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA4['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
									if($rexmhsw5)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw5 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA5)
											{
												if($rexmhswA5['Nilai'] > 0)
												{
													$exam = $exam + $rexmhswA5['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
									if($rexmhsw6)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw6 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA6)
											{
												if($rexmhswA6['Nilai'] > 0)
												{
													$exam = $exam + $rexmhswA6['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									if($TotJum > 1)
									{
										$exam = $exam;
										// $exam = $exam / $TotJum;
									}
									if($exam > 0)
									{
										if($JenisMKID == "5")
										{
											$gradevalueAkhir = (($NilAkhir / $NilBagi) + $exam) / 2;
										}
										else
										{
											$gradevalueAkhir = ((($NilAkhir / $NilBagi) + $mp) + $exam) / 2;
										}
									}
									else
									{
										if($JenisMKID == "5")
										{
											$gradevalueAkhir = ($NilAkhir / $NilBagi);
										}
										else
										{
											$gradevalueAkhir = ($NilAkhir / $NilBagi) + $mp;
										}
									}
									$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
									$remedial = 0;
									if($resremedial)
									{
										$remedial = $resremedial['Nilai'];
									}
									if($remedial > 0)
									{
										$gradevalueAkhir = ($gradevalueAkhir + $remedial) / 2;
									}
									$NilaiAkhir = ($NilAkhir / $NilBagi);
									if($JenisMKID == "5")
									{
										$gradevalue = ($NilAkhir / $NilBagi);
									}
									else
									{
										$gradevalue = ($NilAkhir / $NilBagi) + $mp;
									}
									$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
									$GradeNilai = "";
									if($rownilai)
									{
										foreach($rownilai as $rn)
										{
											if((number_format($gradevalueAkhir,2) >= $rn->NilaiMin) AND (number_format($gradevalueAkhir,2) <= $rn->NilaiMax))
											{
												$GradeNilai = $rn->Nama;
											}
										}
									}
									$data_krs = array(
												'NilaiAkhir' => $NilaiAkhir,
												// 'gradevalue' => $gradevalue,
												'gradevalue' => $gradevalueAkhir,
												'GradeNilai' => $GradeNilai
												);
									$this->m_krs->PTL_update_evaluation($KRSID,$data_krs);
								}
								else
								{
									if($NilBagi == 0)
									{
										$data_krs = array(
													'NilaiAkhir' => 0,
													'gradevalue' => '',
													'GradeNilai' => ''
													);
										$this->m_krs->PTL_update_evaluation($KRSID,$data_krs);
									}
								}
							}
						}
					}
				}
			}
			$data['rowrecord'] = $this->m_khs->PTL_all_select($MhswID);
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_scoring',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_filter_exam_tahun()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			if($cekkhs != "")
			{
				$this->session->set_userdata('acd_filter_exam_tahun',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('acd_filter_exam_tahun');
			}
			redirect("academic/exam");
		}
		
		function exam()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$MhswID = $_COOKIE["nim"];
			$data['MhswID'] = $MhswID;
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$result2 = $this->m_khs->PTL_select_max($MhswID);
			$data['tahun'] = $result2['tahun'];
			$cekkhs = $this->session->userdata('acd_filter_exam_tahun');
			if($cekkhs == "")
			{
				$TahunID = $result2['tahun'];
			}
			else
			{
				$TahunID = $cekkhs;
			}
			$result = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			if($result)
			{
				$data['suspend'] = $result['suspend'];
			}
			else
			{
				$data['suspend'] = '';
			}
			$data['rowrecord'] = $this->m_krs->PTL_all_select_exam_card_date($MhswID,$TahunID);
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_exam',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function remedial()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_remedial');
			$this->load->view('Portal/v_footer');
		}
		
		function final_task()
		{
			$this->authentification();
			$this->session->set_userdata('menu','academic');
			$this->load->view('Portal/v_header');
			$this->load->view('Academic/v_final_task');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_download_project_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/project_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../academic/attendance_detail/$JadwalID");
			}
		}
		
		function ptl_download_evaluation_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/evaluation_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../academic/attendance_detail/$JadwalID");
			}
		}
	}
?>