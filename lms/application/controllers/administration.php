<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Administration extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms!=='logged')
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_bayar');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_year');
			$this->load->model('m_year_detail');
			$this->load->model('m_wifi');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED. Please contact IT Team.','../login/ptl_suspend');
			}
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_acd()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			if($cekkhs != "")
			{
				$this->session->set_userdata('adm_filter_khs',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('adm_filter_khs');
			}
			redirect("administration");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','administration');
			$MhswID = $_COOKIE["nim"];
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$restahun = $this->m_khs->PTL_select_max($MhswID);
			$TahunID = $restahun['tahun'];
			$res = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$data['MhswID'] = $MhswID;
			$data['KHSID'] = "";
			$data['TahunID'] = "";
			$data['tahun'] = "";
			$data['BIPOTID'] = "";
			$BIPOTID = "";
			if($res)
			{
				$data['KHSID'] = $res['KHSID'];
				$data['TahunID'] = $res['TahunID'];
				$data['tahun'] = $restahun['tahun'];
				$data['BIPOTID'] = $res['BIPOTID'];
				$BIPOTID = $res['BIPOTID'];
			}
			if($this->session->userdata('adm_filter_khs') == "")
			{
				$TahunID = $restahun['tahun'];
			}
			else
			{
				$TahunID = $this->session->userdata('adm_filter_khs');
			}
			$result = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			if($result)
			{	
				$ProgramID = $result['ProgramID'];
				$ProdiID = $result['ProdiID'];
				$KelasID = $result['KelasID'];
			}
			else
			{
				$ProgramID = '';
				$ProdiID = '';
				$KelasID = '';
			}
			$result2 = $this->m_program->PTL_select($ProgramID);
			if($result2)
			{
				$data['ProgramID'] = $result['ProgramID'].' - '.$result2['Nama'];
			}
			else
			{
				$data['ProgramID'] = '';
			}
			if($ProgramID == "SC")
			{
				$KursusSingkatID = $ProdiID;
				$result3 = $this->m_kursussingkat->PTL_select($KursusSingkatID);
				if($result3)
				{
					$data['ProdiID'] = $result['ProdiID'].' - '.$result3['Nama'];
				}
				else
				{
					$data['ProdiID'] = '';
				}
			}
			else
			{
				$result3 = $this->m_prodi->PTL_select($ProdiID);
				if($result3)
				{
					$data['ProdiID'] = $result['ProdiID'].' - '.$result3['Nama'];
				}
				else
				{
					$data['ProdiID'] = '';
				}
			}
			$result4 = $this->m_kelas->PTL_select_kelas($KelasID);
			if($result4)
			{
				if($ProgramID == "INT")
				{
					$data['TahunKe'] = ' | O'.$result4['Nama'];
				}
				else
				{
					$data['TahunKe'] = ' | '.$result['TahunKe'].$result4['Nama'];
				}
			}
			else
			{
				$data['TahunKe'] = '';
			}
			$data['rowrecord'] = $this->m_year_detail->PTL_all_spesifik($TahunID,$ProdiID);
			$data['detail'] = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			$data['rowbukti'] = $this->m_bayar->PTL_pym_all_select($MhswID,$TahunID);
			$this->load->view('Portal/v_header');
			$this->load->view('Administration/v_administration',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_pdf_invoice()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			date_default_timezone_set('Asia/Jakarta');
			$BIPOTID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$result = $this->m_master->PTL_pym_bipot_mhsw_select($MhswID,$TahunID);
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			if($res['ProgramID'] == 'SC')
			{
				$KursusSingkatID = $res['ProdiID'];
				$prod = $this->m_kursussingkat->PTL_select($KursusSingkatID);
			}
			else
			{
				$ProdiID = $res['ProdiID'];
				$prod = $this->m_prodi->PTL_select($ProdiID);
			}
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(19,0.7,"INVOICE ID $MhswID$BIPOTID",0,0,"C");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-19,$this->fpdf->getY(),5.5,0.6);
			
			$this->fpdf->Ln();
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(2,0.3,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(2,0.3,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->SetFont("helvetica","",16);
			$this->fpdf->Cell(15,0.5,"FINANCE",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 1, "REF" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , 0, "APL ID / PMB ID" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , 0, ": ".$id_akun." / ".$res['PMBID'] , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -1, "NAME", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(2 , -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(4.5 , -2, "PROGRAM", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1 , -2, ": ".$res['ProgramID']." - ".strtoupper($prod['Nama'])." ($res[ProdiID])", 0, "", "L");
			$this->fpdf->Ln(-0.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(0.5 , 0.7, "#" , "T", 0, "C");
			$this->fpdf->Cell(4.5 , 0.7, "DESCRIPTION" , "T", 0, "C");
			$this->fpdf->Cell(2 , 0.7, "AMOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "TOTAL" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "PAID" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "DISCOUNT" , "T", 0, "C");
			$this->fpdf->Cell(3 , 0.7, "REMAINING" , "T", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$pot = 0;
			$bia = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "-1")
					{
						$BIPOTNamaID_detail = $hasil['BIPOTNamaIDRef'];
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						if(!$resref_detail)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "-", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($r->Besar).")", "", 0, "R");
							if($TambahanNama != "")
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
								$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
								$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							}
							$no++;
							$pot = $pot + $r->Besar;
						}
					}
				}
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$this->fpdf->SetFont("Times","",10);
					if($r->TrxID == "1")
					{
						$BIPOTNamaIDRef = $BIPOTNamaID;
						$resref = $this->m_master->PTL_select_ref($BIPOTNamaIDRef);
						$ref = "";
						if($resref)
						{
							$ref = $resref['BIPOTNamaID'];
						}
						$BIPOTNamaID_detail = $ref;
						$resref_detail = $this->m_master->PTL_detail_ref_cek($BIPOTID,$BIPOTNamaID_detail);
						$ref_detail = "";
						$ref_jumlah = "";
						$ref_jumlah2 = "";
						if($resref_detail)
						{
							$BIPOTNamaID = $BIPOTNamaID_detail;
							$resref_detail2 = $this->m_master->PTL_select($BIPOTNamaID);
							if($resref_detail2)
							{
								$ref_detail = $resref_detail2["Nama"];
								$ref_jumlah = $resref_detail2["DefJumlah"];
								$ref_jumlah2 = $resref_detail2["DefJumlah"];
							}
						}
						$TambahanNama = "";
						if($r->TambahanNama != "")
						{
							$TambahanNama = $r->TambahanNama;
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $no, "", 0, "C");
						$this->fpdf->Cell(4.5 , 0.5, $hasil['Nama'], "", 0, "L");
						$this->fpdf->Cell(2 , 0.5, $r->Jumlah." X", "", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Dibayar), "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, "(".formatRupiah3($ref_jumlah).")", "", 0, "R");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($r->Besar - $r->Dibayar - $ref_jumlah), "", 0, "R");
						if($TambahanNama != "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(4.5 , 0.5, $TambahanNama, "", 0, "L");
							$this->fpdf->Cell(2 , 0.5, "", "", 0, "C");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
							$this->fpdf->Cell(3 , 0.5, "", "", 0, "R");
						}
						$no++;
						$bia = $bia + ($r->Besar - $r->Dibayar - $ref_jumlah);
					}
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(4.5 , 0.3, "", "", 0, "L");
			$this->fpdf->Cell(2 , 0.3, "", "", 0, "C");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Cell(3 , 0.3, "", "", 0, "R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(13 , 1, "THE TOTAL TO BE PAID", "LBT", 0, "L");
			$this->fpdf->Cell(6 , 1, formatRupiah3($bia - $pot), "BTR", 0, "R");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(2);
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta Selatan, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Finance,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(13 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.5, "SUSY TENNA", "", 0, "C");
			$this->fpdf->Ln();
			$rowbukti = $this->m_bayar->PTL_pym_all_select($MhswID,$BIPOTID);
			if($rowbukti)
			{
				$this->fpdf->SetFont("Times","",8);
				$n0 = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n0++;
					}
				}
				if($n0 > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(7.2 , 0.5, "PAYMENT HISTORY", "LBTR", 0, "C");
				}
				$this->fpdf->Ln();
				$this->fpdf->Cell(0.5 , 0.5, "#", "LBTR", 0, "C");
				$this->fpdf->Cell(1.7 , 0.5, "Date", "LBTR", 0, "C");
				$this->fpdf->Cell(2 , 0.5, "Reference", "LBTR", 0, "C");
				$this->fpdf->Cell(3 , 0.5, "Nominal", "LBTR", 0, "C");
				$n = 0;
				$total_bayar = 0;
				foreach($rowbukti as $row)
				{
					if($row->TrxID == "1")
					{
						$n++;
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5 , 0.5, $n, "LBTR", 0, "C");
						$this->fpdf->Cell(1.7 , 0.5, tgl_singkat_eng($row->Tanggal), "LBTR", 0, "C");
						$this->fpdf->Cell(2 , 0.5, $row->BayarMhswID, "LBTR", 0, "C");
						$this->fpdf->Cell(3 , 0.5, formatRupiah3($row->Jumlah), "LBTR", 0, "R");
						$total_bayar = $total_bayar + $row->Jumlah;
					}
				}
				if($n > 0)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(4.2 , 0.5, "TOTAL", "LBTR", 0, "C");
					$this->fpdf->Cell(3 , 0.5, formatRupiah3($total_bayar), "LBTR", 0, "R");
				}
			}
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(4.6 , 0.5, "NB", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Pembayaran dapat di transfer ke rekening", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "An: Yayasan PDKI-BDN-ESMOD", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "No. Rekening: 218-300-3496", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Bank: Bank BCA", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Cabang: Cabang Blok A Cipete, Jakarta", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(4.6 , 0.5, "Swift Code: CENAIDA", "", 0, "L");
			$this->fpdf->Output("STUDENT_INVOICE_".$MhswID.$BIPOTID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function ptl_print_payment()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$BayarMhswID = $this->uri->segment(3);
			$KHSID = $this->uri->segment(4);
			$MhswID = $this->uri->segment(5);
			$TahunID = $this->uri->segment(6);
			$status = $this->uri->segment(7);
			$resmain = $this->m_bayar->PTL_select($BayarMhswID);
			$result = $this->m_bayar->PTL_all_select2($BayarMhswID);
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$ProdiID = $res['ProdiID'];
			$prod = $this->m_prodi->PTL_select($ProdiID);
			
			$tambah = $resmain['Cetak'] + 1;
			$data_bayar = array(
							'Cetak' => $tambah
							);
			$this->m_bayar->PTL_update($BayarMhswID,$data_bayar);
			
			$judul = "OFFICIAL RECEIPT";
			$to1 = "Sudah Terima Dari";
			$to2 = "(Received From)";
			$pay1 = "Untuk Pembayaran";
			$pay2 = "(Being Payment For)";
			$pesan = "NB : Pembayaran yang sudah ditransfer tidak dapat ditarik kembali";
			$ttd = "Yang Menerima (Received By)";
			$file = "STUDENT_PAYMENT_";
			if($resmain['TrxID'] == "-1")
			{
				$judul = "WITHDRAWAL";
				$to1 = "Penarikan Oleh";
				$to2 = "(Withdrawal by)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : Penarikan dengan alasan khusus";
				$ttd = "Yang Menerima (Received By)";
				$file = "STUDENT_WITHDRAWAL_";
			}
			if($resmain['TrxID'] == "-2")
			{
				$judul = "CORRECTIONS";
				$to1 = "Dikoreksi Untuk";
				$to2 = "(Corrected for)";
				$pay1 = "Dari Pembayaran";
				$pay2 = "(From Payment)";
				$pesan = "NB : $resmain[Cetak]";
				$ttd = "Dikoreksi Oleh (Corrected By)";
				$file = "STUDENT_CORRECTIONS_";
			}
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(28,1,"$judul",0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,1,"NO : $BayarMhswID",0,0,"R");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()-28,$this->fpdf->getY()-0.6,12.5,1.6);
			
			$this->fpdf->Ln(1.2);
			$id_akun = $MhswID;
			$nama = $res['Nama'];
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
			$this->fpdf->SetFont("helvetica","BI",20);
			$this->fpdf->Cell(26,0.7,$status,0,0,"R");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",20);
			$this->fpdf->Cell(2,0.7,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
			$this->fpdf->Line(1,4.7,28.9,4.7);
			$this->fpdf->Line(1,4.75,28.9,4.75);
			
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $to1, "", 0, "L");
			$this->fpdf->Cell(3, 0.7, "Student ID :", "", 0, "L");
			$this->fpdf->Cell(5.5, 0.7, $MhswID, "", 0, "C");
			$this->fpdf->Cell(2.2, 0.7, "Nama :", "", 0, "L");
			$this->fpdf->Cell(5.8, 0.7, $nama, "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $to2, "", 0, "L");
			$this->fpdf->Cell(3.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(2.2, 0.5, "(Name)", "", 0, "L");
			$this->fpdf->Cell(10.6, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$terbilang = ucwords(toTerbilang($resmain['Jumlah'])) . ' Rupiah';  
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Banyaknya", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, $terbilang, "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "(The amount of)", "", 0, "L");
			$this->fpdf->Cell(21.3, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, "Check/BG/Trans./Cash", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.7, $resmain['Bank'], "", 0, "L");
			$this->fpdf->Cell(7.2, 0.7, "Tanggal Pembayaran :", "", 0, "L");
			$this->fpdf->Cell(2.8, 0.7, tgl_singkat_eng($resmain["Tanggal"]), "B", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, "", "", 0, "L");
			$this->fpdf->Cell(8.5, 0.5, "", "T", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "(Payment Date)", "", 0, "L");
			$this->fpdf->Cell(6.4, 0.5, "", "T", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5, 0.7, $pay1, "", 0, "L");
			$this->fpdf->Cell(21.3, 0.7, "", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",14);
			$this->fpdf->Cell(6.5, 0.5, $pay2, "", 0, "L");
			$no = 1;
			$tot = count($result);
			$Jumlah = 0;
			if($result)
			{
				foreach($result as $r)
				{
					$BIPOTNamaID = $r->BIPOTNamaID;
					$hasil = $this->m_master->PTL_select($BIPOTNamaID);
					$BIPOTMhswID = $r->BIPOTMhswID;
					$hasil2 = $this->m_master->PTL_bipotmhsw_select($BIPOTMhswID);
					if($no == 1)
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->SetFont("Times","B",18);
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					else
					{
						$tambahan = "";
						if($hasil2['TambahanNama'] != "")
						{
							$tambahan = " (".$hasil2['TambahanNama'].")";
						}
						$this->fpdf->Ln();
						$this->fpdf->Cell(6.5 , 0.7, "", "", 0, "L");
						$this->fpdf->Cell(14 , 0.7, $hasil['Nama'].$tambahan, "", 0, "L");
						$this->fpdf->Cell(1 , 0.7, ": Rp", "", 0, "R");
						$this->fpdf->Cell(4 , 0.7, formatRupiah2($r->Jumlah), "", 0, "R");
						$no++;
					}
					$Jumlah = $Jumlah + $r->Jumlah;
				}
			}
			else
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(19 , 1, "EMPTY DATA", "LBR", 0, "C");
			}
			$this->fpdf->Ln(1.2);
			$this->fpdf->SetFont("Times","B",18);
			$this->fpdf->Cell(6.5 , 1, "Jumlah / Amount", "", 0, "L");
			$this->fpdf->Cell(7 , 1, formatRupiah3($Jumlah), "LBTR", 0, "C");
			$this->fpdf->Cell(6 , 1, "", "", 0, "C");
			$this->fpdf->Cell(8 , 1, $ttd, "", 0, "C");
			$this->fpdf->Ln();
			$dd = gmdate("d", time()-($ms));
			$mm = gmdate("m", time()-($ms));
			$yy = gmdate("Y", time()-($ms));
			$inv = gmdate("YmdHis", time()-($ms));
			$this->fpdf->Cell(21.3 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(4.6 , 0.4, "Jakarta, $dd ".bulan_singkat($mm)." $yy", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","B",16);
			$this->fpdf->Cell(4.6 , 0.5, $pesan, "", 0, "L");
			$this->fpdf->Ln(3.6);
			$this->fpdf->Cell(20 , 0.4, "", "", 0, "C");
			$this->fpdf->Cell(7 , 0.5, "", "B", 0, "C");
			$this->fpdf->Output($file.$MhswID." - ".$inv." - ".@$nama.".pdf","I");
		}
		
		function wifi()
		{
			$this->authentification();
			$this->session->set_userdata('menu','administration');
			$MhswID = $_COOKIE["nim"];
			$data['MhswID'] = $MhswID;
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = '';
			if($resmhsw)
			{
				$data['Nama'] = $resmhsw['Nama'];
			}
			
			$ip = $_SERVER['REMOTE_ADDR'];
			$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$_PERINTAH = "arp -a $ip";
			ob_start();
			system($_PERINTAH);
			$_HASIL = ob_get_contents();
			ob_clean();
			$_PECAH = strstr($_HASIL, $ip);
			$_PECAH_STRING = explode($ip, str_replace(" ", "", $_PECAH));
			$_HASIL = substr(@$_PECAH_STRING[1], 0, 17);
			$browser = $_SERVER['HTTP_USER_AGENT'];
			$ip_server = @$REMOTE_ADDR;
			
			$data['hostname'] = $hostname;
			$data['mac_address'] = $_HASIL;
			$data['ip_address'] = $ip;
			$data['browser'] = $browser;
			$data['ip_server'] = $ip_server;
			$data['hostname_server'] = getenv('COMPUTERNAME');
			$data['url_server'] = $_SERVER['HTTP_HOST'];
			$this->load->view('Portal/v_header');
			$this->load->view('Administration/v_administration_wifi',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function wifi_auth()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$Nama = strtoupper($this->input->post('Nama'));
			$data = array(
						'kode_akun' => $MhswID,
						'nama_akun' => $Nama,
						'access_name' => $this->input->post('access_name'),
						'hostname' => $this->input->post('hostname'),
						'mac_address' => $this->input->post('mac_address'),
						'ip_address' => $this->input->post('ip_address'),
						'ip_server' => $this->input->post('ip_server'),
						'hostname_server' => $this->input->post('hostname_server'),
						'url_server' => $this->input->post('url_server'),
						'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_wifi->PTL_insert($data);
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$Email = $resmhsw['Email'];
			$Email2 = $resmhsw['Email2'];
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			$this->email->bcc("lendra.permana@gmail.com,wprasasti@esmodjakarta.com");
			$this->email->subject('WIFI REGISTRATION (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>LMS</h2></font>
				</center>
				<br/>
				Dear $MhswID - $Nama,
				<br/>
				<br/>
				<b>ESMOD JAKARTA</b>: Your WIFI REGISTRATION will processed by IT Team. Please wait!
				<br/>
				<br/>
				<br/>
				<br/>
				<center>
					<table>
						<tr>
							<th colspan='3' align='center'>DATA</th>
						</tr>
						<tr>
							<td>Access Name</td>
							<td>:</td>
							<td>".$this->input->post('access_name')."</td>
						</tr>
						<tr>
							<td>Device Name</td>
							<td>:</td>
							<td>".$this->input->post('hostname')."</td>
						</tr>
						<tr>
							<td>Mac Address</td>
							<td>:</td>
							<td>".$this->input->post('mac_address')."</td>
						</tr>
						<tr>
							<td>IP Address</td>
							<td>:</td>
							<td>".$this->input->post('ip_address')."</td>
						</tr>
						<tr>
							<td>IP Server</td>
							<td>:</td>
							<td>".$this->input->post('ip_server')."</td>
						</tr>
						<tr>
							<td>Hostname Server</td>
							<td>:</td>
							<td>".$this->input->post('hostname_server')."</td>
						</tr>
						<tr>
							<td>URL Server</td>
							<td>:</td>
							<td>".$this->input->post('url_server')."</td>
						</tr>
					</table>
				</center>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				echo warning("WIFI REGISTRATION has been sent to ESMOD IT Team.","../administration/wifi");
			}
			else
			{
				echo warning('Email server is not active. Data saved...','../administration/wifi');
			}
		}
	}
?>