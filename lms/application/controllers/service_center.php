<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Service_center extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_lms = $_COOKIE["is_login_lms"];
			if ($is_login_lms!=='logged')
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			if($_COOKIE["nim"] == "")
			{
				$this->session->set_userdata('is_login_lms','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->library('upload');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_service');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result['suspend'] == "Y")
			{
				echo warning('Sorry! Your account was SUSPENDED. Please contact IT Team.','../login/ptl_suspend');
			}
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','service_center');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $result['MhswID'];
			$data['Nama'] = $result['Nama'];
			$data['Email'] = $result['Email'];
			$data['Handphone'] = $result['Handphone'];
			$this->load->view('Portal/v_header');
			$this->load->view('Service/v_estore',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_computer()
		{
			$this->authentification();
			$this->session->set_userdata('menu','service_center');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $result['MhswID'];
			$data['Nama'] = $result['Nama'];
			$data['Email'] = $result['Email'];
			$data['Handphone'] = $result['Handphone'];
			$this->load->view('Portal/v_header');
			$this->load->view('Service/v_service',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_application()
		{
			$this->authentification();
			$this->session->set_userdata('menu','service_center');
			$MhswID = $_COOKIE["nim"];
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['MhswID'] = $result['MhswID'];
			$data['Nama'] = $result['Nama'];
			$data['Email'] = $result['Email'];
			$data['Handphone'] = $result['Handphone'];
			$this->load->view('Portal/v_header');
			$this->load->view('Service/v_aplikasi',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$type_pesanan = $this->input->post('type_pesanan');
			if($type_pesanan == "SERVICE")
			{
				$data = array(
							'MhswID' => $this->input->post('MhswID'),
							'Nama' => $this->input->post('Nama'),
							'Email' => $this->input->post('Email'),
							'Handphone' => $this->input->post('Handphone'),
							'type_pesanan' => $type_pesanan,
							'computer_model' => $this->input->post('computer_model'),
							'keterangan' => $this->input->post('keterangan'),
							'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_buat' => $this->waktu
							);
			}
			if($type_pesanan == "APPLICATION")
			{
				$data = array(
							'MhswID' => $this->input->post('MhswID'),
							'Nama' => $this->input->post('Nama'),
							'Email' => $this->input->post('Email'),
							'Handphone' => $this->input->post('Handphone'),
							'type_pesanan' => $type_pesanan,
							'tipe_aplikasi' => $this->input->post('tipe_aplikasi'),
							'keterangan' => $this->input->post('keterangan'),
							'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_buat' => $this->waktu
							);
			}
			$this->m_service->PTL_insert($data);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to('wishnuprasasti@gmail.com');
			$this->email->cc('lendra.permana@gmail.com');
			$this->email->subject('NEW ORDER FOR BUSINESS (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>LMS</h2></font>
				</center>
				<br/>
				Dear Tim,
				<br/>
				<br/>
				Berikut permintaan baru dari mahasiswa untuk $type_pesanan.
				<br/>
				<br/>
				<table>
					<tr>
						<td>SIN</td>
						<td>:</td>
						<td>".$this->input->post('MhswID')."</td>
					</tr>
					<tr>
						<td>NAME</td>
						<td>:</td>
						<td>".$this->input->post('Nama')."</td>
					</tr>
					<tr>
						<td>EMAIL</td>
						<td>:</td>
						<td>".$this->input->post('Email')."</td>
					</tr>
					<tr>
						<td>MOBILE PHONE</td>
						<td>:</td>
						<td>".$this->input->post('Handphone')."</td>
					</tr>
					<tr>
						<td>TYPE</td>
						<td>:</td>
						<td>$type_pesanan</td>
					</tr>
					<tr>
						<td>DESCRIPTIONS</td>
						<td>:</td>
						<td>".$this->input->post('keterangan')."</td>
					</tr>
				</table>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($this->email->send())
			{
				if($type_pesanan == "SERVICE")
				{
					echo warning("Your data successfully updated.","../service_center/PTL_computer");
				}
				else
				{
					echo warning("Your data successfully updated.","../service_center/PTL_application");
				}
			}
			else
			{
				if($type_pesanan == "SERVICE")
				{
					echo warning("Email server is not active. Your data successfully updated.","../service_center/PTL_computer");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully updated.","../service_center/PTL_application");
				}
			}
		}
		
		function ptl_update_photo()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$nama_akun = str_replace(' ','_',strtoupper($this->input->post('nama')));
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$admin_dp = '../lms/ptl_storage/foto_produk';
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$info_dwn = pathinfo($download);
			$asal_url = str_replace(' ','_',strtolower($download));
			$url = hapus_simbol($asal_url);
			$downloadin = $tgl."_".$MhswID."_".$nama_akun."_".$url.".".@$info_dwn["extension"];
			
			$dwn = $downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '2048000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->upload->initialize($config);
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$pesan = $this->upload->display_errors();
				echo warning("Data other than the image has been updated.","../service_center");
			}
			else
			{
				$source_photo = "$admin_dp/$downloadin";
				$ukuran =  fsize($source_photo);
				$word = explode(" ",$ukuran);
				$pesan_file = "";
				$ukuran_akhir = "";
				if(($word[1] == "KB") OR ($word[1] == "MB"))
				{
					$pesan_file .= " File memenuhi syarat upload kurang dari 10 MB.";
					if(($word[1] == "KB") AND ($word[0] > 128))
					{
						$info = pathinfo($source_photo);
						$pesan_file.= " File perlu di kompresi karena lebih dari 512 KB.";
						$destination_photo = $source_photo."_compress.".$info["extension"];
						compress_image($source_photo,$destination_photo,30);
						$ukuran_akhir =  " Ukuran akhir ".fsize($destination_photo).". File ukuran sebelumnya berhasil diarsip.";
						// unlink("./$source_photo");
						$info2 = pathinfo($destination_photo);
						$dwn = $info2["filename"].".".$info2["extension"];
					}
					else
					{
						if(($word[1] == "MB") AND ($word[0] > 1))
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " File perlu di kompresi karena lebih dari 1 MB.";
							$destination_photo = $source_photo."_compress.".$info["extension"];
							compress_image($source_photo,$destination_photo,30);
							$ukuran_akhir = " Ukuran akhir ".fsize($destination_photo).". File ukuran sebelumnya berhasil diarsip.";
							// unlink("./$source_photo");
							$info2 = pathinfo($destination_photo);
							$dwn = $info2["filename"].".".$info2["extension"];
						}
						else
						{
							$pesan_file .= " File tidak perlu di kompresi.";
						}
					}
				}
				else
				{
					if($word[1] == "B")
					{
						$pesan_file .=  " File terlalu kecil. $ukuran";
					}
					else
					{
						$pesan_file .=  " File terlalu besar.";
					}
				}
				$type_pesanan = $this->input->post('type_pesanan');
				$data = array(
							'MhswID' => $this->input->post('MhswID'),
							'Nama' => $this->input->post('Nama'),
							'Email' => $this->input->post('Email'),
							'Handphone' => $this->input->post('Handphone'),
							'type_pesanan' => $type_pesanan,
							'spesialisasi' => $this->input->post('spesialisasi'),
							'foto_produk' => $dwn,
							'keterangan' => $this->input->post('keterangan'),
							'login_buat' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_service->PTL_insert($data);
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to('wishnuprasasti@gmail.com');
				$this->email->cc('lendra.permana@gmail.com');
				$this->email->subject('NEW ORDER FOR BUSINESS (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>LMS</h2></font>
					</center>
					<br/>
					Dear Tim,
					<br/>
					<br/>
					Berikut permintaan baru dari mahasiswa untuk $type_pesanan.
					<br/>
					<br/>
					<table>
						<tr>
							<td>SIN</td>
							<td>:</td>
							<td>".$this->input->post('MhswID')."</td>
						</tr>
						<tr>
							<td>NAME</td>
							<td>:</td>
							<td>".$this->input->post('Nama')."</td>
						</tr>
						<tr>
							<td>EMAIL</td>
							<td>:</td>
							<td>".$this->input->post('Email')."</td>
						</tr>
						<tr>
							<td>MOBILE PHONE</td>
							<td>:</td>
							<td>".$this->input->post('Handphone')."</td>
						</tr>
						<tr>
							<td>TYPE</td>
							<td>:</td>
							<td>$type_pesanan</td>
						</tr>
						<tr>
							<td>DESCRIPTIONS</td>
							<td>:</td>
							<td>".$this->input->post('keterangan')."</td>
						</tr>
					</table>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($this->email->send())
				{
					echo warning("Your data successfully added.$pesan_file$ukuran_akhir","../service_center");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully added.$pesan_file$ukuran_akhir","../service_center");
				}
			}
		}
	}
?>