<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Verification extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->kode = gmdate("dis", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_sms');
		}
		
		function authentification()
		{
			$is_login = "is_login_lms";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/dra_maintenance');
			}
		}
		
		function index()
		{
			redirect("login");
		}
		
		function ptl_auth()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			if($MhswID == "")
			{
				echo "<center><h2><b>Sorry! You don't have access.</b></h2></center>";
				redirect("login");
			}
			else
			{
				if($_SERVER["HTTP_HOST"] == "localhost")
				{
					$data['link_logout'] = "http://localhost/app/lms/login/ptl_logout";
				}
				else
				{
					$data['link_logout'] = "http://app.esmodjakarta.com/lms/login/ptl_logout";
				}
				$result = $this->m_mahasiswa->PTL_select($MhswID);
				$data['MhswID'] = $result['MhswID'];
				$data['Nama'] = $result['Nama'];
				$data['Handphone'] = $result['Handphone'];
				$data['Email'] = $result['Email'];
				$data['handphone_verifikasi_kode'] = $result['handphone_verifikasi_kode'];
				$this->load->view('Portal/v_header_verifikasi');
				$this->load->view('Verifikasi/v_verifikasi_phone',$data);
				$this->load->view('Portal/v_footer');
			}
		}
		
		function ptl_auth_update()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$Nama = $this->input->post('Nama');
			$Handphone = $this->input->post('Handphone');
			$data = array(
						'Handphone' => $Handphone,
						'handphone_verifikasi_kode' => $this->kode,
						'login_edit' => $MhswID."_".$Nama,
						'tanggal_edit' => $this->waktu
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			if(substr($Handphone,0,2) == "62")
			{
				$prov = "0".substr($Handphone,2,3);
			}
			else
			{
				$prov = substr($Handphone,0,4);
			}
			if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
			{
				$data = array(
							'id_user' => $MhswID,
							'nama' => $Nama,
							'aplikasi' => 'LMS',
							'judul_sms' => 'Verifikasi HP',
							'nomor_hp' => $Handphone,
							'pesan' => "ESMOD (NO-REPLY): Never share this OTP number to anyone. ESMOD verification code $this->kode",
							'login_buat' => $MhswID."_".$Nama,
							'tanggal_buat' => $this->waktu
							);
				$this->m_sms->PTL_insert($data);
			}
			else
			{
				$url = "http://103.16.199.187/masking/send_post.php";
				$rows = array(
							"username" => "esmodjkt",
							"password" => "esmj@kr00t",
							"hp" => "$Handphone",
							"message" => "Never share this OTP number to anyone. ESMOD verification code $this->kode"
							);
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url );
				curl_setopt($curl, CURLOPT_POST, TRUE );
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
				curl_setopt($curl, CURLOPT_HEADER, FALSE );
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
				curl_setopt($curl, CURLOPT_TIMEOUT, 60);
				$htm = curl_exec($curl);
				if(curl_errno($curl) !== 0)
				{
					$data = array(
								'id_user' => $MhswID,
								'nama' => $Nama,
								'aplikasi' => 'LMS',
								'judul_sms' => 'Verifikasi HP',
								'nomor_hp' => $Handphone,
								'pesan' => "ESMOD (NO-REPLY): Never share this OTP number to anyone. ESMOD verification code $this->kode",
								'login_buat' => $MhswID."_".$Nama,
								'tanggal_buat' => $this->waktu
								);
					$this->m_sms->PTL_insert($data);
					error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
				}
				curl_close($curl);
				print_r($htm);
			}
			echo warning("OTP was sent to $Handphone","../verification/ptl_auth/$MhswID");
		}
		
		function ptl_auth_reset()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			$data = array(
						'handphone_verifikasi_kode' => '',
						'login_edit' => $_COOKIE["nim"]."_".$_COOKIE["nama"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			redirect("../lms/verification/ptl_auth/$MhswID");
		}
		
		function ptl_auth_verifikasi()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$Nama = $this->input->post('Nama');
			$otp = $this->input->post('otp');
			$handphone_verifikasi_kode = $this->input->post('handphone_verifikasi_kode');
			if($otp == $handphone_verifikasi_kode)
			{
				$Handphone = $this->input->post('Handphone');
				$hp = substr($Handphone,0,3)."xxxxxxxx".substr($Handphone,-1);
				if($hp == "")
				{
					echo warning("Invalid Mobile Phone.","../verification/ptl_auth/$MhswID");
				}
				else
				{
					if(substr($Handphone,0,2) == "62")
					{
						$prov = "0".substr($Handphone,2,3);
					}
					else
					{
						$prov = substr($Handphone,0,4);
					}
					$provider = "";
					$setprovider = "";
					if($prov == "0811"){ $provider = "<font color='red'>Telkomsel - Kartu Halo</font>"; $setprovider = "Telkomsel - Kartu Halo"; }
					if($prov == "0812"){ $provider = "<font color='red'>Telkomsel - Kartu Halo / simPATI</font>"; $setprovider = "Telkomsel - Kartu Halo / simPATI"; }
					if($prov == "0813"){ $provider = "<font color='red'>Telkomsel - Kartu Halo / simPATI</font>"; $setprovider = "Telkomsel - Kartu Halo / simPATI"; }
					if($prov == "0821"){ $provider = "<font color='red'>Telkomsel - simPATI</font>"; $setprovider = "Telkomsel - simPATI"; }
					if($prov == "0822"){ $provider = "<font color='red'>Telkomsel - simPATI / Kartu Facebook</font>"; $setprovider = "Telkomsel - simPATI / Kartu Facebook"; }
					if($prov == "0823"){ $provider = "<font color='red'>Telkomsel - Kartu AS</font>"; $setprovider = "Telkomsel - Kartu AS"; }
					if($prov == "0852"){ $provider = "<font color='red'>Telkomsel - Kartu AS</font>"; $setprovider = "Telkomsel - Kartu AS"; }
					if($prov == "0853"){ $provider = "<font color='red'>Telkomsel - Kartu AS</font>"; $setprovider = "Telkomsel - Kartu AS"; }
					if($prov == "0851"){ $provider = "<font color='red'>Telkomsel - Kartu AS Flexi</font>"; $setprovider = "Telkomsel - Kartu AS Flexi"; }
					
					if($prov == "0855"){ $provider = "<font color='blue'>Indosat - Matrix</font>"; $setprovider = "Indosat - Matrix"; }
					if($prov == "0856"){ $provider = "<font color='blue'>Indosat - IM3</font>"; $setprovider = "Indosat - IM3"; }
					if($prov == "0857"){ $provider = "<font color='blue'>Indosat - IM3</font>"; $setprovider = "Indosat - IM3"; }
					if($prov == "0858"){ $provider = "<font color='blue'>Indosat - Mentari</font>"; $setprovider = "Indosat - Mentari"; }
					if($prov == "0814"){ $provider = "<font color='blue'>Indosat - Broadband Indosat M2</font>"; $setprovider = "Indosat - Broadband Indosat M2"; }
					if($prov == "0815"){ $provider = "<font color='blue'>Indosat - Matrix / Mentari</font>"; $setprovider = "Indosat - Matrix / Mentari"; }
					if($prov == "0816"){ $provider = "<font color='blue'>Indosat - Matrix / Mentari</font>"; $setprovider = "Indosat - Matrix / Mentari"; }
					
					if($prov == "0817"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					if($prov == "0818"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					if($prov == "0819"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					if($prov == "0859"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					if($prov == "0877"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					if($prov == "0878"){ $provider = "<font color='green'>XL Axiata - Pra bayar dan Explor</font>"; $setprovider = "XL Axiata - Pra bayar dan Explor"; }
					
					if($prov == "0838"){ $provider = "<font color='green'>Lippo Telecom - AXIS</font>"; $setprovider = "Lippo Telecom - AXIS"; }
					
					if($prov == "0896"){ $provider = "<font color='purple'>3 (Three) - Pra bayar dan Pasca bayar</font>"; $setprovider = "3 (Three) - Pra bayar dan Pasca bayar"; }
					if($prov == "0897"){ $provider = "<font color='purple'>3 (Three) - Pra bayar dan Pasca bayar</font>"; $setprovider = "3 (Three) - Pra bayar dan Pasca bayar"; }
					if($prov == "0898"){ $provider = "<font color='purple'>3 (Three) - Pra bayar dan Pasca bayar</font>"; $setprovider = "3 (Three) - Pra bayar dan Pasca bayar"; }
					if($prov == "0899"){ $provider = "<font color='purple'>3 (Three) - Pra bayar dan Pasca bayar</font>"; $setprovider = "3 (Three) - Pra bayar dan Pasca bayar"; }
					
					if($prov == "0881"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0882"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0883"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0884"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0885"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0886"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					if($prov == "0887"){ $provider = "<font color='yellow'>SMART Telecom - Pra bayar dan Pasca bayar</font>"; $setprovider = "SMART Telecom - Pra bayar dan Pasca bayar"; }
					
					if($prov == "0888"){ $provider = "<font color='grey'>Fren (Mobile-8) - Fren / Mobi</font>"; $setprovider = "Fren (Mobile-8) - Fren / Mobi"; }
					if($prov == "0889"){ $provider = "<font color='grey'>Fren (Mobile-8) - Mobi</font>"; $setprovider = "Fren (Mobile-8) - Fren"; }
					
					if($prov == "0828"){ $provider = "<font color='pink'>Ceria (Sampurna Telecom) - Pra bayar dan Pasca bayar</font>"; $setprovider = "Ceria (Sampurna Telecom) - Pra bayar dan Pasca bayar"; }
					
					if($prov == "0868"){ $provider = "<font color='grey'>BYRU SATELIT (Pasifik Satelit Nusantara)</font>"; $setprovider = "BYRU SATELIT (Pasifik Satelit Nusantara)"; }
					$data = array(
								'handphone_provider' => $setprovider,
								'handphone_verifikasi' => 'Y',
								'login_buat' => $MhswID."_".$Nama,
								'tanggal_edit' => $this->waktu
								);
					$this->m_mahasiswa->PTL_update($MhswID,$data);
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $MhswID,
									'nama' => $Nama,
									'aplikasi' => 'LMS',
									'judul_sms' => 'Berhasil Verifikasi HP',
									'nomor_hp' => $Handphone,
									'pesan' => "ESMOD (NO-REPLY): Your number $hp has been verified.",
									'login_buat' => $MhswID."_".$Nama,
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$Handphone",
									"message" => "Your number $hp has been verified."
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $MhswID,
										'nama' => $Nama,
										'aplikasi' => 'LMS',
										'judul_sms' => 'Berhasil Verifikasi HP',
										'nomor_hp' => $Handphone,
										'pesan' => "ESMOD (NO-REPLY): Your number $hp has been verified.",
										'login_buat' => $MhswID."_".$Nama,
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to(strtolower($this->input->post('Email')));
					$this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('LMS - MOBILE PHONE HAS BEEN UPDATED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>LMS</h2></font>
						</center>
						<br/>
						<br/>
						Dear ".strtoupper($this->input->post('Nama')).",
						<br/>
						<br/>
						Your mobile phone <i><b>$hp</b></i> ($provider) in LMS application has been updated.
						<br/>
						<br/>
						Thanks,
						<br/>
						<br/>
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($this->email->send())
					{
						echo warning("Data has been updated.","../login/ptl_home");
					}
					else
					{
						echo warning("Email server is not active. Data has been updated.","../login/ptl_home");
					}
				}
			}
			else
			{
				echo warning("Invalid OTP.","../verification/ptl_auth/$MhswID");
			}
		}
	}
?>