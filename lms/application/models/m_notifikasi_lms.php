<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_notifikasi_lms extends CI_Model
	{
		function PTL_all_active($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_notifikasi_lms');
			return $query->result();
		}
		
		function PTL_all_active_sin($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_notifikasi_lms');
			return $query->result();
		}
		
		function PTL_all_active_sin_non($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('tanggal_baca','');
			$this->db->where('NA','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_notifikasi_lms');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_notifikasi_lms',$data);
			return;
		}
		
		function PTL_update($id_notifikasi_lms,$datanotif)
		{
			$this->db->where('id_notifikasi_lms',$id_notifikasi_lms);
			$this->db->update('ac_notifikasi_lms',$datanotif);
		}
	}
?>