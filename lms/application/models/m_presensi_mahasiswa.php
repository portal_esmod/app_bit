<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_presensi_mahasiswa extends CI_Model
	{
		function PTL_all_cek_presensi_aktif($PresensiID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_excuse($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','E');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_sick($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','S');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_absent($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','A');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_late($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','L');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_sum($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_select_presensi($PresensiID,$MhswID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->row_array();
		}
		
		function PTL_select_scoring($PresensiID,$MhswID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('Removed','N');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->row_array();
		}
		
		function PTL_select_presensi_aktif($PresensiID,$MhswID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('Removed','N');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->row_array();
		}
	}
?>