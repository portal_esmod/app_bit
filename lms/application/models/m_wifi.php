<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_wifi extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_wifi');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('dv_wifi',$data);
			return;
		}
		
		function PTL_select($id_akses)
		{
			$this->db->where('id_akses',$id_akses);
			$query = $this->db->get('dv_wifi');
			return $query->row_array();
		}
		
		function PTL_update($id_akses,$data)
		{
			$this->db->where('id_akses',$id_akses);
			$this->db->update('dv_wifi',$data);
		}
	}
?>