<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mk extends CI_Model
	{
		function PTL_all_select($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('NA','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_mk');
			return $query->result();
		}
		
		function PTL_select($MKID)
		{
			$this->db->where('MKID',$MKID);
			$query = $this->db->get('ac_mk');
			return $query->row_array();
		}
	}
?>