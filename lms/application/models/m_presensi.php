<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_presensi extends CI_Model
	{
		function PTL_all_select($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_select_cek_mk($JadwalID,$CekMKID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('MKID',$CekMKID);
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
	}
?>