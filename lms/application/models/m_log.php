<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_log extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('id_log','DESC');
			$query = $this->db->get('dv_log');
			return $query->result();
		}
		
		function transaksi_grafik_logout($i)
		{
			$h = "-7";
			$hm = $h * 60; 
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-", time()-($ms));
			$harian = $tanggal.sprintf('%02s',$i);
			$q = $this->db->query("SELECT COUNT(status) AS logout FROM dv_log WHERE tanggal like '$harian%' AND status = 'LOGOUT'");
			return $q->row_array();
		}
		
		function transaksi_grafik_login($i)
		{
			$h = "-7";
			$hm = $h * 60; 
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-", time()-($ms));
			$harian = $tanggal.sprintf('%02s',$i);
			$q = $this->db->query("SELECT COUNT(status) AS login FROM dv_log WHERE tanggal like '$harian%' AND status = 'LOGIN'");
			return $q->row_array();
		}
		
		function PTL_input($datadir)
		{
			$this->db->insert('dv_log',$datadir);
			return;
		}
		
		function PTL_login()
		{
			$this->db->where('aplikasi','ADMISSION');
			$this->db->where('status','LOGIN');
			$this->db->order_by('id_log','DESC');
			$query = $this->db->get('dv_log',10);
			return $query->result();
		}
		
		function PTL_logout()
		{
			$this->db->where('aplikasi','ADMISSION');
			$this->db->where('status','LOGOUT');
			$this->db->order_by('id_log','DESC');
			$query = $this->db->get('dv_log',10);
			return $query->result();
		}
	}
?>