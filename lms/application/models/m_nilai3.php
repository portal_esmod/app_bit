<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_nilai3 extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->where('NA','N');
			// $query = $this->db->get('ac_nilai3');
			// return $query->result();
		// }
		
		function PTL_all_spesifik($cekgroup)
		{
			$this->db->where('JenisMKID',$cekgroup);
			$query = $this->db->get('ac_nilai3');
			return $query->result();
		}
		
		function PTL_select_attendance($JenisMKID,$JenisPresensiID)
		{
			$this->db->where('JenisMKID',$JenisMKID);
			$this->db->where('JenisPresensiID',$JenisPresensiID);
			$query = $this->db->get('ac_nilai3');
			return $query->row_array();
		}
		
		function PTL_update($Nilai3ID,$data)
		{
			$this->db->where('Nilai3ID',$Nilai3ID);
			$this->db->update('ac_nilai3',$data);
		}
	}
?>