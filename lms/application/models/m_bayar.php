<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_bayar extends CI_Model
	{
		// function PTL_urut_bayar($kd)
		// {
			// $q = $this->db->query("SELECT max(BayarMhswID) AS LAST FROM fn_bayarmhsw WHERE BayarMhswID like '$kd%'");
			// return $q->row_array();
		// }
		
		// function PTL_all_select($PMBID,$BIPOTID)
		// {
			// $this->db->where('PMBID',$PMBID);
			// $this->db->where('BIPOTID',$BIPOTID);
			// $this->db->order_by('BayarMhswID','DESC');
			// $query = $this->db->get('fn_bayarmhsw');
			// return $query->result();
		// }
		
		function PTL_pym_all_select($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_all_select2($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->order_by('BayarMhsw2ID','DESC');
			$query = $this->db->get('fn_bayarmhsw2');
			return $query->result();
		}
		
		// function PTL_insert($data_bayar)
		// {
			// $this->db->insert('fn_bayarmhsw',$data_bayar);
			// return;
		// }
		
		// function PTL_insert2($data_bayar2)
		// {
			// $this->db->insert('fn_bayarmhsw2',$data_bayar2);
			// return;
		// }
		
		function PTL_select($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$query = $this->db->get('fn_bayarmhsw');
			return $query->row_array();
		}
		
		function PTL_update($BayarMhswID,$data_bayar)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->update('fn_bayarmhsw',$data_bayar);
		}
	}
?>