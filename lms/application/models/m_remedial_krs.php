<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_remedial_krs extends CI_Model
	{	
		function PTL_insert($data)
		{
			$this->db->insert('ac_krs_remedial',$data);
			return;
		}
		
		function PTL_select_evaluasi($SubjekID,$KRSID,$MhswID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_select($JadwalRemedialID,$MhswID)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_select_kursi($JadwalRemedialID,$NomorKursi)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->where('NomorKursi',$NomorKursi);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		// function PTL_update($TahunID,$data)
		// {
			// $this->db->where('TahunID',$TahunID);
			// $this->db->update('ac_tahun',$data);
		// }
		
		function PTL_delete($KRSRemedialID)
		{
			$this->db->where('KRSRemedialID',$KRSRemedialID);
			$this->db->delete('ac_krs_remedial');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_krs_remedial');
		}
	}
?>