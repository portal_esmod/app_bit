<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_berita extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_berita',10);
			return $query->result();
		}
		
		function PTL_select($BeritaID)
		{
			$this->db->where('BeritaID',$BeritaID);
			$query = $this->db->get('ac_berita');
			return $query->row_array();
		}
	}
?>