<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jadwal_hari extends CI_Model
	{
		function PTL_select($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$query = $this->db->get('ac_jadwal_hari');
			return $query->row_array();
		}
		
		function PTL_select_spesifik($JadwalID,$HariID)
		{
			if($JadwalID != "")
			{
				$this->db->where('JadwalID',$JadwalID);
			}
			if($HariID != "")
			{
				$this->db->where('HariID',$HariID);
			}
			$query = $this->db->get('ac_jadwal_hari');
			return $query->row_array();
		}
	}
?>