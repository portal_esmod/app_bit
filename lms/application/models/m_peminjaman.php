<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	class M_peminjaman extends CI_Model
	{
		function PTL_all_select_nis($nis)
		{
			$this->db->where('nis',$nis);
			$this->db->where('status','N');
			$query = $this->db->get('lb_transaksi');
			return $query->result();
		}
		
		function PTL_select_barcode($kode_buku)
		{
			$this->db->where('kode_buku',$kode_buku);
			$query = $this->db->get('lb_buku');
			return $query->row_array();
		}
	}
?>