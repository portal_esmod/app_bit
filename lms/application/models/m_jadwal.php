<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jadwal extends CI_Model
	{
		function PTL_all_spesifik($ProgramID,$TahunID,$KelasID)
		{
			if($ProgramID != "")
			{
				$this->db->where('ProgramID',$ProgramID);
			}
			if($TahunID != "")
			{
				$this->db->where('TahunID',$TahunID);
			}
			if($KelasID != "")
			{
				$this->db->where('KelasID',$KelasID);
			}
			$this->db->order_by('TahunID','DESC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($ceksubjek != "")
			{
				$this->db->where('SubjekID',$ceksubjek);
			}
			if($Tahun != "")
			{
				$this->db->where('TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->where('KelasID',$Kelas);
			}
			$this->db->order_by('SubjekID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring_total($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($Tahun != "")
			{
				$this->db->where('TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->where('KelasID',$Kelas);
			}
			if($SubjekID != "")
			{
				$this->db->where('SubjekID',$SubjekID);
			}
			$this->db->order_by('SubjekID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_jadwal_harian_update($Tanggal,$ProgramID,$MhswID,$TahunID)
		{
			$this->db->select('
				a.PresensiID, a.Pertemuan, a.DosenID, a.Tanggal, a.JamMulai, a.JamSelesai, a.RuangID, a.MKID,
				b.TglMulai, b.TglSelesai,
				c.KRSID,
				d.Nama as _subjek, d.SubjekKode, d.SKS');
			$this->db->from('ac_presensi AS a');
			$this->db->where('a.Tanggal',$Tanggal);
			$this->db->where('a.NA','N');
			$this->db->where('b.ProgramID',$ProgramID);
			$this->db->where('c.MhswID',$MhswID);
			$this->db->where('c.TahunID',$TahunID);
			$this->db->join('ac_jadwal AS b', 'b.JadwalID = a.JadwalID', 'INNER');
			$this->db->join('ac_krs AS c', 'c.JadwalID = a.JadwalID', 'INNER');
			$this->db->join('ac_subjek AS d', 'd.SubjekID = b.SubjekID', 'INNER');
			$this->db->join('ac_khs AS e', 'e.KHSID = c.KHSID', 'INNER');
			$this->db->order_by('a.JamMulai','ASC');
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return FALSE;
			}
		}
		
		function PTL_subjek($cekjurusan,$cektahun,$Tahun,$Kelas)
		{
			$j = "";
			$t = "";
			$th = "";
			$kls = "";
			if($cekjurusan != "")
			{
				$j = "AND ac_jadwal.ProgramID = '$cekjurusan'";
			}
			if($cektahun != "")
			{
				$t = "AND ac_jadwal.TahunID = '$cektahun'";
			}
			if($Tahun != "")
			{
				$th = "AND ac_jadwal.TahunKe = '$Tahun'";
			}
			if($Kelas != "")
			{
				$kls = "AND ac_jadwal.KelasID = '$Kelas'";
			}
			$q = $this->db->query("
				SELECT distinct(ac_jadwal.SubjekID) AS subjek, ac_subjek.Nama as nama
				FROM ac_subjek
				LEFT JOIN ac_jadwal ON ac_jadwal.SubjekID = ac_subjek.SubjekID
				WHERE ac_jadwal.NA = 'N' $j $t $th $kls ORDER BY ac_subjek.Nama ASC");
			return $q->result();
		}
		
		function PTL_select($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$query = $this->db->get('ac_jadwal');
			return $query->row_array();
		}
	}
?>