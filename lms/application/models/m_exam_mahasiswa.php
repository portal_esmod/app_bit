<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_exam_mahasiswa extends CI_Model
	{
		function PTL_nilai_evaluasi($ExamID,$MhswID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_exam_mhsw');
			return $query->row_array();
		}
		
		function PTL_all_evaluation($ExamID,$MhswID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_exam_mhsw');
			return $query->row_array();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_exam_mhsw',$data);
			return;
		}
		
		function PTL_update($ExamID,$MhswID,$data)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->where('MhswID',$MhswID);
			$this->db->update('ac_exam_mhsw',$data);
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_exam_mhsw');
		}
	}
?>