<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pesan_mhsw extends CI_Model
	{
		function PTL_select($MhswID,$PesanID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('PesanID',$PesanID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_pesan_mhsw');
			return $query->row_array();
		}
		
		function PTL_update2($MhswID,$PesanID,$da)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('PesanID',$PesanID);
			$this->db->update('ac_pesan_mhsw',$da);
		}
	}
?>