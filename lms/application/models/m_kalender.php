<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kalender extends CI_Model
	{
		function PTL_all_tahun_ini()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tahun = gmdate("Y", time()-($ms));
			$this->db->like('tanggal_mulai',$tahun);
			$this->db->where('na','N');
			$query = $this->db->get('ac_kalender');
			return $query->result();
		}
	}
?>