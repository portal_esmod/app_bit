<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kursussingkat extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_kursussingkat');
			return $query->result();
		}
		
		function PTL_select($KursusSingkatID)
		{
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$query = $this->db->get('ac_kursussingkat');
			return $query->row_array();
		}
	}
?>