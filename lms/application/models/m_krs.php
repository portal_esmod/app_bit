<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_krs extends CI_Model
	{
		function PTL_select_spesifik($MhswID,$KHSID)
		{
			$this->db->select('a.*, b.SubjekID, b.SubjekKode , b.Nama, b.SKS');
			$this->db->from('ac_krs AS a');
			if($MhswID != "")
			{
				$this->db->where('a.MhswID',$MhswID);
			}
			if($KHSID != "")
			{
				$this->db->where('a.KHSID',$KHSID);
			}
			$this->db->join('ac_subjek AS b', 'b.SubjekID = a.SubjekID', 'INNER');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return FALSE;
			}
		}
		
		function PTL_all_spesifik_scoring($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs');
			return $query->result();
		}
		
		function PTL_all_select_exam_card_date($MhswID,$TahunID)
		{
			$this->db->select('a.*, b.Tanggal, b.JamMulai');
			$this->db->from('ac_krs as a');
			$this->db->where('a.MhswID',$MhswID);
			$this->db->where('a.TahunID',$TahunID);
			$this->db->join('ac_exam as b','b.ExamID=a.ExamID');
			$this->db->order_by('b.Tanggal,b.JamMulai','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_update($KRSID,$data)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->update('ac_krs',$data);
		}
		
		function PTL_update_evaluation($KRSID,$data_krs)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->update('ac_krs',$data_krs);
		}
		
		function PTL_delete_krs($KRSID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->delete('ac_krs');
		}
	}
?>