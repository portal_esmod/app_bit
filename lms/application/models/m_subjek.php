<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_subjek extends CI_Model
	{
		function lookup_subjek($keyword)
		{
			$this->db->where('NA','N');
			$this->db->like('Nama',$keyword);
			$query = $this->db->get('ac_subjek',10);
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->group_by('Nama');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_subjek');
			return $query->result();
		}
		
		function PTL_select($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_select_subjek($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_select_subjek_nama($Nama)
		{
			$this->db->where('Nama',$Nama);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
	}
?>