<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rating extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_urut($kd)
		{
			$q = $this->db->query("SELECT max(id_rating) AS LAST FROM fb_rating where id_rating like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_all_spesifik($MhswID,$TahunID,$KHSID,$JadwalID,$SubjekID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('KHSID',$KHSID);
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('fb_rating',$data);
			return;
		}
		
		function PTL_select_spesifik($MhswID,$id_rating_question,$TahunID,$JadwalID,$SubjekID,$DosenID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('id_rating_question',$id_rating_question);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('DosenID',$DosenID);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating');
			return $query->row_array();
		}
		
		function PTL_update($id_rating,$data)
		{
			$this->db->where('id_rating',$id_rating);
			$this->db->update('fb_rating',$data);
		}
	}
?>