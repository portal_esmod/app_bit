<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mahasiswa extends CI_Model
	{
		function lookup($keyword)
		{
			$this->db->select('*')->from('ac_mahasiswa');
			$this->db->where('NA','N');
			$this->db->like('MhswID',$keyword,'after');
			$this->db->or_like('Nama',$keyword,'after');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_birthday_today()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$LastSIN = ((gmdate("Y", time()-($ms))) - 2)."00000000";
			$birthday = gmdate("-m-d", time()-($ms));
			$this->db->like('TanggalLahir',$birthday);
			$this->db->where('MhswID >=',$LastSIN);
			$this->db->where('StatusMhswID','A');
			$this->db->where('na','N');
			$this->db->order_by('nama','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif($cekjurusan,$cekprodi,$cektahun)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_insert_file($MhswID,$gambar,$gambar,$gambar)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$storage = gmdate("Y-m", time()-($ms));
			$folder = $storage.'_';
			$dt = array(
						'file_identitas'=>$folder.@$gambar[0],
						'status_identitas'=>'Y',
						'file_identitas_ortu'=>$folder.@$gambar[1],
						'status_identitas_ortu'=>'Y',
						'file_ijazah'=>$folder.@$gambar[2],
						'status_ijazah'=>'Y'
						);
			$this->db->where('MhswID',$MhswID);
			return $this->db->update('ac_mahasiswa',$dt);
		}
		
		function PTL_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		function PTL_select_email($email)
		{
			$this->db->where('Email',$email);
			$this->db->or_where('Email2',$email);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		function PTL_update($MhswID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->update('ac_mahasiswa',$data);
		}
	}
?>