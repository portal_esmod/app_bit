<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_ultah extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_ultah',$data);
			return;
		}
		
		function PTL_select_today($MhswID)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tanggal_kirim = gmdate("Y-m-d", time()-($ms));
			$this->db->where('id_akun',$MhswID);
			$this->db->like('tanggal_kirim',$tanggal_kirim);
			$query = $this->db->get('ac_ultah');
			return $query->row_array();
		}
	}
?>