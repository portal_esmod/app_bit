<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kelas extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('SpesialisasiID','0');
			$this->db->where('NA','N');
			$query = $this->db->get('al_kelas');
			return $query->result();
		}
		
		function PTL_select()
		{
			$cekkelas = $this->session->userdata('year_filter_kelas');
			if($cekkelas != "")
			{
				$this->db->where('KelasID',$cekkelas);
			}
			$this->db->where('SpesialisasiID','0');
			$this->db->where('NA','N');
			$query = $this->db->get('al_kelas');
			return $query->result();
		}
		
		function PTL_select_kelas($KelasID)
		{
			$this->db->where('KelasID',$KelasID);
			$query = $this->db->get('al_kelas');
			return $query->row_array();
		}
	}
?>