<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_nilai extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->where('NA','N');
			// $query = $this->db->get('ac_nilai');
			// return $query->result();
		// }
		
		function PTL_all_evaluation($KurikulumID)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->order_by('Nama','DESC');
			$query = $this->db->get('ac_nilai');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekkurikulum)
		{
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_nilai');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_nilai',$data);
			return;
		}
		
		function PTL_select($NilaiID)
		{
			$this->db->where('NilaiID',$NilaiID);
			$query = $this->db->get('ac_nilai');
			return $query->row_array();
		}
		
		function PTL_select_bobot($KurikulumID,$GradeNilai)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->where('Nama',$GradeNilai);
			$query = $this->db->get('ac_nilai');
			return $query->row_array();
		}
		
		function PTL_select_kode_nilai($Nama,$cekkurikulum)
		{
			$this->db->where('Nama',$Nama);
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_nilai');
			return $query->row_array();
		}
		
		function PTL_update($NilaiID,$data)
		{
			$this->db->where('NilaiID',$NilaiID);
			$this->db->update('ac_nilai',$data);
		}
	}
?>