<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_catatan2 extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_catatan');
			return $query->result();
		}
		
		function PTL_all_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('na','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_catatan');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_catatan',$data);
			return;
		}
		
		function PTL_select($noteid)
		{
			$this->db->where('noteid',$noteid);
			$query = $this->db->get('ac_catatan');
			return $query->row_array();
		}
		
		function PTL_update2($noteid,$da)
		{
			$this->db->where('noteid',$noteid);
			$this->db->update('ac_catatan',$da);
		}
	}
?>