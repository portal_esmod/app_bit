<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pesan extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->order_by('TanggalBuat','DESC');
			$query = $this->db->get('ac_pesan');
			return $query->result();
		}
		
		function PTL_select($PesanID)
		{
			$this->db->where('PesanID',$PesanID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_pesan');
			return $query->row_array();
		}
	}
?>