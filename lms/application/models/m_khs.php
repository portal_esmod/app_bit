<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_khs extends CI_Model
	{
		function PTL_all_spesifik($cekjurusan,$cektahun,$cekprodi,$cekkelas)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekkelas != "")
			{
				$this->db->where('KelasID',$cekkelas);
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_spc($cekjurusan,$cekprodi,$cekspc,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekspc != "")
			{
				$this->db->where('SpesialisasiID',$cekspc);
			}
			if($cekke != "")
			{
				$this->db->where('TahunKe',$cekke);
			}
			$this->db->group_by('MhswID','ASC');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_no_spc($cekjurusan,$cekprodi,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekke != "")
			{
				$this->db->where('TahunKe',$cekke);
			}
			$this->db->where('SpesialisasiID','0');
			$this->db->group_by('MhswID','ASC');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			// $this->db->where('NA','N');
			$this->db->order_by('Sesi','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_select_drop($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_select_att($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_max($MhswID)
		{
			$this->db->select('max(TahunID) as tahun');
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_tahun($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_update($KHSID,$data)
		{
			$this->db->where('KHSID',$KHSID);
			$this->db->update('ac_khs',$data);
		}
	}
?>