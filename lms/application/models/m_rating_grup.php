<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rating_grup extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_urut($kd2)
		{
			$q = $this->db->query("SELECT max(id_rating_grup) AS LAST FROM fb_rating_grup where id_rating_grup like '$kd2%'");
			return $q->row_array();
		}
		
		function PTL_all_spesifik($id_rating_question_grup,$MhswID,$KHSID)
		{
			$this->db->where('id_rating_question_grup',$id_rating_question_grup);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('KHSID',$KHSID);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating_grup');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('fb_rating_grup',$data);
			return;
		}
		
		function PTL_select_spesifik($MhswID,$TahunID,$JID,$SID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('JadwalID',$JID);
			$this->db->where('SubjekID',$SID);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating_grup');
			return $query->row_array();
		}
		
		// Coba
		// function coba()
		// {
			// $q = $this->db->query("SELECT * FROM fb_rating_grup group by MhswID ORDER BY tanggal_buat ASC");
			// return $q->result();
		// }
		
		// function coba_ok($MhswID)
		// {
			// $this->db->where('MhswID',$MhswID);
			// $this->db->where('na','N');
			// $query = $this->db->get('fb_rating_grup');
			// return $query->result();
		// }
		
		// function update($MhswID,$da)
		// {
			// $this->db->where('MhswID',$MhswID);
			// $this->db->update('fb_rating_grup',$da);
		// }
	}
?>