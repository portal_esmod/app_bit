<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_year_detail extends CI_Model
	{
		function PTL_all_spesifik($TahunID,$ProdiID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('ProdiID',$ProdiID);
			$this->db->where('na','N');
			$this->db->order_by('tanggal_mulai','ASC');
			$query = $this->db->get('ac_tahun_detail');
			return $query->result();
		}
	}
?>