<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_program extends CI_Model
	{
		function PTL_select($ProgramID)
		{
			$this->db->where('ProgramID',$ProgramID);
			$query = $this->db->get('ac_program');
			return $query->row_array();
		}
	}
?>