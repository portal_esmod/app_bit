<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_identity extends CI_Model
	{
		function LENDRA_all()
		{
			$query = $this->db->get('ac_identitas');
			return $query->result();
		}
		
		function LENDRA_insert($data)
		{
			$this->db->insert('ac_identitas',$data);
			return;
		}
		
		function LENDRA_select($TahunID)
		{
			$this->db->where('TahunID',$TahunID);
			$query = $this->db->get('ac_identitas');
			return $query->row_array();
		}
		
		function LENDRA_update($TahunID,$data)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->update('ac_tahun',$data);
		}
	}
?>