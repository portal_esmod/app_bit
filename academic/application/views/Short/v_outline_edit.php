		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("short"); ?>">Short Course</a>
							>>
							<a href="<?php echo site_url("short/ptl_outline_form/$KursusSingkatID"); ?>">Add Outline</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("short/ptl_outline_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<input type="hidden" name="OutlineID" value="<?php echo $OutlineID; ?>">
											<input type="hidden" name="KursusSingkatID" value="<?php echo $KursusSingkatID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Descriptions</label>
											<textarea name="Keterangan" class="form-control"><?php echo $Keterangan; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("short"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>