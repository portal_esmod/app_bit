		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("short"); ?>">Short Course</a>
							>>
							<a href="<?php echo site_url("short/ptl_form"); ?>">Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("short/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Short Course Program ID</label>
											<input type="text" name="KursusSingkatID" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Format Student ID</label>
											<input type="text" name="FormatNIM" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Duration</label>
											<input type="int" name="Waktu" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Fee</label>
											<input type="text" name="Biaya" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("short"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>