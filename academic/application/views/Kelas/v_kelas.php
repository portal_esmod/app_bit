		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Class Before Enroll</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("kelas"); ?>">Class Before Enroll (CLASS02)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>This is the second step before set homeroom teacher. <a href="<?php echo site_url("tutorial/ptl_detail/CLASS02"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Students in this Class
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_jur1"); ?>" method="POST">
												<select name="cekjurusan1" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan1 = $this->session->userdata('class_filter_jur1');
														echo "<option value='REG'"; if($cekjurusan1 == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan1 == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan1 == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_prodi1"); ?>" method="POST">
												<select name="cekprodi1" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi1 = $this->session->userdata('class_filter_prodi1');
														if($cekjurusan1 != "")
														{
															if($rowprodi1)
															{
																if($cekjurusan1 == "SC")
																{
																	foreach($rowprodi1 as $rp)
																	{
																		echo "<option value='$rp->KursusSingkatID'";
																		if($cekprodi1 == $rp->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->KursusSingkatID - $rp->Nama</option>";
																	}
																}
																else
																{
																	foreach($rowprodi1 as $rp)
																	{
																		echo "<option value='$rp->ProdiID'";
																		if($cekprodi1 == $rp->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->ProdiID - $rp->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_status1"); ?>" method="POST">
												<select name="cekstatus1" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- STATUS --</option>
													<?php
														$cekstatus1 = $this->session->userdata('class_filter_status1');
														if($cekjurusan1 != "")
														{
															if($rowstatus)
															{
																foreach($rowstatus as $status)
																{
																	echo "<option value='$status->StatusMhswID'";
																	if($cekstatus1 == $status->StatusMhswID)
																	{
																		echo "selected";
																	}
																	echo ">$status->StatusMhswID - $status->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_ke1"); ?>" method="POST">
												<select name="cekke1" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- SEMESTER --</option>
													<?php
														$cekke1 = $this->session->userdata('class_filter_ke1');
														if($cekjurusan1 != "")
														{
															echo "<option value='1'"; if($cekke1 == '1'){ echo "selected"; } echo ">SEMESTER 1</option>";
															echo "<option value='2'"; if($cekke1 == '2'){ echo "selected"; } echo ">SEMESTER 2</option>";
															echo "<option value='3'"; if($cekke1 == '3'){ echo "selected"; } echo ">SEMESTER 3</option>";
															echo "<option value='4'"; if($cekke1 == '4'){ echo "selected"; } echo ">SEMESTER 4</option>";
															echo "<option value='5'"; if($cekke1 == '5'){ echo "selected"; } echo ">SEMESTER 5</option>";
															echo "<option value='6'"; if($cekke1 == '6'){ echo "selected"; } echo ">SEMESTER 6</option>";
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_class"); ?>" method="POST">
												<select name="cekclass" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- CLASS --</option>
													<?php
														$cekclass = $this->session->userdata('class_filter_class');
														if($cekjurusan1 != "")
														{
															$add = "";
															if($cekjurusan1 == "INT")
															{
																$add = "O";
															}
															if($rowkelas)
															{
																foreach($rowkelas as $rk)
																{
																	if($cekjurusan1 == $rk->ProgramID)
																	{
																		echo "<option value='$rk->KelasID'";
																		if($cekclass == $rk->KelasID)
																		{
																			echo "selected";
																		}
																		echo ">CLASS $add$rk->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_tahun1"); ?>" method="POST">
												<?php
													$cektahun1 = $this->session->userdata('class_filter_tahun1');
													$font = "";
													if($rowtahun1)
													{
														foreach($rowtahun1 as $rt)
														{
															$f = "";
															if(($cektahun1 == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun1" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan1 != "")
														{
															if($rowtahun1)
															{
																foreach($rowtahun1 as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun1 == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
								</table>
								<form role="form" action="<?php echo site_url("kelas/ptl_unset"); ?>" method="POST">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>#</th>
												<th>
													<?php
														$cek = $this->session->userdata('class_set_all1');
														if($cek == "")
														{
															echo "<a href='".site_url("kelas/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
														}
														else
														{
															echo "<a href='".site_url("kelas/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
														}
													?>
												</th>
												<th>Student Name & ID</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$n = 1;
												if($cekprodi1 != "")
												{
													if($rowrecord1)
													{
														foreach($rowrecord1 as $row)
														{
															if(($row->KelasID != 0) AND ($row->KelasID != ""))
															{
																$MhswID = $row->MhswID;
																$m = $this->m_mahasiswa->PTL_select($MhswID);
																$cekstatus1 = $this->session->userdata('class_filter_status1');
																if($cekstatus1 == "")
																{
																	$StatusMhswID = "";
																	$Nama = "<font color='red' title='No data in table `ac_mahasiswa`'>No data</font>";
																	if($m)
																	{
																		$StatusMhswID = $m["StatusMhswID"];
																		$Nama = $m["Nama"];
																	}
																	if(@$m["StatusMhswID"] == "A")
																	{
																		echo "<tr>";
																	}
																	else
																	{
																		echo "<tr class='danger'>";
																	}
																	echo "<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$n</td>
																		<td>";
																	$keuangan = $row->Potongan + $row->Bayar;
																	if($keuangan >= 0)
																	{
																		if(@$m["StatusMhswID"] == "A")
																		{
																			if($cek == "")
																			{
																				echo "<input type='checkbox' name='cek$n' value='$row->KHSID'>";
																			}
																			else
																			{
																				echo "<input type='checkbox' name='cek$n' value='$row->KHSID' checked>";
																			}
																		}
																		else
																		{
																			$s = $this->m_status->PTL_select($StatusMhswID);
																			$nama = "";
																			if($s)
																			{
																				$nama = $s["Nama"];
																			}
																			echo "<input type='hidden' name='cek$n' value=''>";
																			echo $nama;
																		}
																	}
																	else
																	{
																		echo "<font color='red'><b>Upaid the financial</b></font>";
																	}
																	$Finance = "";
																	if($row->suspend == "Y")
																	{
																		$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																	}
																	echo "</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a> - <a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a>$Finance</td>
																		</tr>";
																	$n++;
																}
																else
																{
																	if($cekstatus1 == @$m["StatusMhswID"])
																	{
																		$Nama = "<font color='red' title='No data in table `ac_mahasiswa`'>No data</font>";
																		if($m)
																		{
																			$StatusMhswID = $m["StatusMhswID"];
																			$Nama = $m["Nama"];
																		}
																		if(@$m["StatusMhswID"] == "A")
																		{
																			echo "<tr>";
																		}
																		else
																		{
																			echo "<tr class='danger'>";
																		}
																		echo "<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$n</td>
																			<td>";
																		$keuangan = $row->Potongan + $row->Bayar;
																		if($keuangan >= 0)
																		{
																			if(@$m["StatusMhswID"] == "A")
																			{
																				if($cek == "")
																				{
																					echo "<input type='checkbox' name='cek$n' value='$row->KHSID'>";
																				}
																				else
																				{
																					echo "<input type='checkbox' name='cek$n' value='$row->KHSID' checked>";
																				}
																			}
																			else
																			{
																				$s = $this->m_status->PTL_select($StatusMhswID);
																				$nama = "";
																				if($s)
																				{
																					$nama = $s["Nama"];
																				}
																				echo "<input type='hidden' name='cek$n' value=''>";
																				echo $nama;
																			}
																		}
																		else
																		{
																			echo "<font color='red'><b>Upaid the financial</b></font>";
																		}
																		$Finance = "";
																		if($row->suspend == "Y")
																		{
																			$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																		}
																		echo "</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a> - <a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a>$Finance</td>
																		</tr>";
																		$n++;
																	}
																}
															}
														}
													}
												}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th>#</th>
												<th>
													<?php
														$cek = $this->session->userdata('class_set_all1');
														if($cek == "")
														{
															echo "<a href='".site_url("kelas/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
														}
														else
														{
															echo "<a href='".site_url("kelas/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
														}
													?>
												</th>
												<th>
													<?php
														echo "<input type='hidden' name='total1' value='$n'>";
													?>
													<input type="submit" value="Unset Allocated" id="my_button" class="btn btn-primary" onclick="return confirm('Are you sure want to process this data?\n\nTHIS ACTION MAY TAKE A LONG TIME.')">
												</th>
											</tr>
										</tfoot>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Students don't have Class
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_jur2"); ?>" method="POST">
												<select name="cekjurusan2" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan2 = $this->session->userdata('class_filter_jur2');
														echo "<option value='REG'"; if($cekjurusan2 == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan2 == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan2 == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_prodi2"); ?>" method="POST">
												<select name="cekprodi2" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi2 = $this->session->userdata('class_filter_prodi2');
														if($cekjurusan2 != "")
														{
															if($rowprodi2)
															{
																if($cekjurusan2 == "SC")
																{
																	foreach($rowprodi2 as $rp)
																	{
																		echo "<option value='$rp->KursusSingkatID'";
																		if($cekprodi2 == $rp->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->KursusSingkatID - $rp->Nama</option>";
																	}
																}
																else
																{
																	foreach($rowprodi2 as $rp)
																	{
																		echo "<option value='$rp->ProdiID'";
																		if($cekprodi2 == $rp->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->ProdiID - $rp->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_status2"); ?>" method="POST">
												<select name="cekstatus2" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- STATUS --</option>
													<?php
														$cekstatus2 = $this->session->userdata('class_filter_status2');
														if($cekjurusan1 != "")
														{
															if($rowstatus)
															{
																foreach($rowstatus as $status)
																{
																	echo "<option value='$status->StatusMhswID'";
																	if($cekstatus2 == $status->StatusMhswID)
																	{
																		echo "selected";
																	}
																	echo ">$status->StatusMhswID - $status->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_ke2"); ?>" method="POST">
												<select name="cekke2" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- SEMESTER --</option>
													<?php
														$cekke2 = $this->session->userdata('class_filter_ke2');
														if($cekjurusan2 != "")
														{
															echo "<option value='1'"; if($cekke2 == '1'){ echo "selected"; } echo ">SEMESTER 1</option>";
															echo "<option value='2'"; if($cekke2 == '2'){ echo "selected"; } echo ">SEMESTER 2</option>";
															echo "<option value='3'"; if($cekke2 == '3'){ echo "selected"; } echo ">SEMESTER 3</option>";
															echo "<option value='4'"; if($cekke2 == '4'){ echo "selected"; } echo ">SEMESTER 4</option>";
															echo "<option value='5'"; if($cekke2 == '5'){ echo "selected"; } echo ">SEMESTER 5</option>";
															echo "<option value='6'"; if($cekke2 == '6'){ echo "selected"; } echo ">SEMESTER 6</option>";
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("kelas/ptl_filter_tahun2"); ?>" method="POST">
												<?php
													$cektahun2 = $this->session->userdata('class_filter_tahun2');
													$font = "";
													if($rowtahun2)
													{
														foreach($rowtahun2 as $rt)
														{
															$f = "";
															if(($cektahun2 == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun2" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan2 != "")
														{
															if($rowtahun2)
															{
																foreach($rowtahun2 as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun2 == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
								</table>
								<form role="form" action="<?php echo site_url("kelas/ptl_set"); ?>" method="POST">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example2">
										<thead>
											<tr>
												<th>#</th>
												<th>
													<?php
														$cek2 = $this->session->userdata('class_set_all2');
														if($cek2 == "")
														{
															echo "<a href='".site_url("kelas/ptl_set_check_all2")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
														}
														else
														{
															echo "<a href='".site_url("kelas/ptl_set_uncheck_all2")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
														}
													?>
												</th>
												<th>Student Name & ID</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$n2 = 1;
												if($cekprodi2 != "")
												{
													if($rowrecord2)
													{
														foreach($rowrecord2 as $row2)
														{
															if(($row2->KelasID == 0) OR ($row2->KelasID == ""))
															{
																$MhswID = $row2->MhswID;
																$m = $this->m_mahasiswa->PTL_select($MhswID);
																$cekstatus2 = $this->session->userdata('class_filter_status2');
																if($cekstatus2 == "")
																{
																	$StatusMhswID = "";
																	$Nama = "<font color='red' title='No data in table `ac_mahasiswa`'>No data</font>";
																	if($m)
																	{
																		$StatusMhswID = $m["StatusMhswID"];
																		$Nama = $m["Nama"];
																	}
																	if($StatusMhswID == "A")
																	{
																		echo "<tr>";
																	}
																	else
																	{
																		echo "<tr class='danger'>";
																	}
																	echo "<td title='KHSID : $row2->KHSID ~ Created by : $row2->login_buat ~ Created Date : $row2->tanggal_buat ~ Edited by : $row2->login_edit ~ Edited Date : $row2->tanggal_edit'>$n2</td>
																		<td>";
																	$keuangan = $row2->Potongan + $row2->Bayar;
																	if($keuangan >= 0)
																	{
																		if($StatusMhswID == "A")
																		{
																			if($cek2 == "")
																			{
																				echo "<input type='checkbox' name='cek$n2' value='$row2->KHSID'>";
																			}
																			else
																			{
																				echo "<input type='checkbox' name='cek$n2' value='$row2->KHSID' checked>";
																			}
																		}
																		else
																		{
																			$s = $this->m_status->PTL_select($StatusMhswID);
																			$nama = "";
																			if($s)
																			{
																				$nama = $s["Nama"];
																			}
																			echo "<input type='hidden' name='cek$n2' value=''>";
																			echo $nama;
																		}
																	}
																	else
																	{
																		echo "<font color='red'><b>Upaid the financial</b></font>";
																	}
																	$Finance = "";
																	if($row2->suspend == "Y")
																	{
																		$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																	}
																	echo "</td>
																		<td><a href='".site_url("evaluation/ptl_cari/$row2->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row2->MhswID</b></a> - <a href='".site_url("students/ptl_edit/$row2->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a>$Finance</td>
																	</tr>";
																	$n2++;
																}
																else
																{
																	if($cekstatus2 == @$m["StatusMhswID"])
																	{
																		$Nama = "<font color='red' title='No data in table `ac_mahasiswa`'>No data</font>";
																		if($m)
																		{
																			$StatusMhswID = $m["StatusMhswID"];
																			$Nama = $m["Nama"];
																		}
																		if(@$m["StatusMhswID"] == "A")
																		{
																			echo "<tr>";
																		}
																		else
																		{
																			echo "<tr class='danger'>";
																		}
																		echo "<td title='KHSID : $row2->KHSID ~ Created by : $row2->login_buat ~ Created Date : $row2->tanggal_buat ~ Edited by : $row2->login_edit ~ Edited Date : $row2->tanggal_edit'>$n2</td>
																			<td>";
																		$keuangan = $row2->Potongan + $row2->Bayar;
																		if($keuangan >= 0)
																		{
																			if(@$m["StatusMhswID"] == "A")
																			{
																				if($cek2 == "")
																				{
																					echo "<input type='checkbox' name='cek$n2' value='$row2->KHSID'>";
																				}
																				else
																				{
																					echo "<input type='checkbox' name='cek$n2' value='$row2->KHSID' checked>";
																				}
																			}
																			else
																			{
																				$s = $this->m_status->PTL_select($StatusMhswID);
																				$nama = "";
																				if($s)
																				{
																					$nama = $s["Nama"];
																				}
																				echo "<input type='hidden' name='cek$n2' value=''>";
																				echo $nama;
																			}
																		}
																		else
																		{
																			echo "<font color='red'><b>Upaid the financial</b></font>";
																		}
																		$Finance = "";
																		if($row2->suspend == "Y")
																		{
																			$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																		}
																		echo "</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row2->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row2->MhswID</b></a> - <a href='".site_url("students/ptl_edit/$row2->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a>$Finance</td>
																		</tr>";
																		$n2++;
																	}
																}
															}
														}
													}
												}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th>#</th>
												<th>
													<?php
														$cek2 = $this->session->userdata('class_set_all2');
														if($cek2 == "")
														{
															echo "<a href='".site_url("kelas/ptl_set_check_all2")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
														}
														else
														{
															echo "<a href='".site_url("kelas/ptl_set_uncheck_all2")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
														}
													?>
												</th>
												<th>
													<?php
														echo "<input type='hidden' name='total2' value='$n2'>";
													?>
													<input type="submit" value="Set Allocated" id="my_button2" class="btn btn-primary" onclick="return confirm('Are you sure want to process this data?\n\nTHIS ACTION MAY TAKE A LONG TIME.')">
												</th>
											</tr>
										</tfoot>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>