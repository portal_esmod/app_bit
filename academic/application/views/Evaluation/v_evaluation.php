			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Evaluation</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("evaluation"); ?>">Student Evaluation (EVALU02)</a>
                        </div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This page is used to view the student evaluation. <a href="<?php echo site_url("tutorial/ptl_detail/EVAL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<?php
									$mhsw = $this->session->userdata('eval_filter_mahasiswa');
								?>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("evaluation/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('eval_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("evaluation/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('eval_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("evaluation/ptl_filter_status"); ?>" method="POST">
												<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- STATUS --</option>
													<?php
														$cekstatus = $this->session->userdata('eval_filter_status');
														if($status)
														{
															foreach($status as $row)
															{
																echo "<option value='$row->StatusMhswID'";
																if($cekstatus == $row->StatusMhswID)
																{
																	echo "selected";
																}
																echo ">$row->StatusMhswID - $row->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<form role="form" id="student" action="<?php echo site_url("evaluation/ptl_cari"); ?>" method="POST">
											<td colspan="3">
												<input type="text" name="cari" value="<?php echo $mhsw; ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
										</form>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													$foto = "foto_umum/user.jpg";
													if($r["Foto"] != "")
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto = $foto_preview;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="6"><p align="center"><img src="<?php echo base_url("ptl_storage/$foto"); ?>" width="200px"/></p></td>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td>
													<?php
														$tgl = tgl_singkat_eng($r["TanggalLahir"]);
														if($r["TanggalLahir"] == "NULL")
														{
															$tgl = "";
														}
														echo $r["TempatLahir"].", ".$tgl;
													?>
												</td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td>
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td>
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td>
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Class</td>
												<td>:</td>
												<td>
													<?php
														$reskhs = $this->m_khs->PTL_all_select($MhswID);
														$kelas1 = "";
														$kelas2 = "";
														$kelas3 = "";
														$spec = "";
														if($reskhs)
														{
															foreach($reskhs as $rk)
															{
																$KelasID = $rk->KelasID;
																$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
																if($reskelas)
																{
																	if($rk->TahunKe == 1)
																	{
																		if($rk->ProgramID == "INT")
																		{
																			$kelas1 = "O".$reskelas["Nama"];
																		}
																		else
																		{
																			$kelas1 = $rk->TahunKe.$reskelas["Nama"];
																		}
																	}
																	if($rk->TahunKe == 2)
																	{
																		$kelas2 = " > ".$rk->TahunKe.$reskelas["Nama"];
																	}
																	if($rk->TahunKe == 3)
																	{
																		$kelas3 = " > ".$rk->TahunKe.$reskelas["Nama"];
																	}
																}
															}
														}
														$kelas = $kelas1.$kelas2.$kelas3;
														echo $kelas;
													?>
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<center>
									<a href="<?php echo site_url("transcript"); ?>" class="btn btn-info">Go To Transcript</a>
									<br/>
									<br/>
								</center>
								<table class="table table-striped table-bordered table-hover">
									<tbody>
										<tr>
											<?php
												if($mhsw != "")
												{
													if($rowrecord)
													{
														$no = 1;
														$tahun = 0;
														$kumulatif = 0;
														$kumulatifsks = 0;
														$kumulatifipk = 0;
														$ipk = 0;
														foreach($rowrecord as $row)
														{
															$kumulatif++;
															if($no == 1)
															{
																$tahun = $tahun + 1;
																$no++;
															}
															else
															{
																$tahun = $tahun;
																$no = 1;
															}
															$KelasID = $row->KelasID;
															$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
															if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
															$DosenID = $row->WaliKelasID;
															$rhr1 = $this->m_dosen->PTL_select($DosenID);
															if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
															$DosenID = $row->WaliKelasID2;
															$rhr2 = $this->m_dosen->PTL_select($DosenID);
															if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
															$SpesialisasiID = $row->SpesialisasiID;
															$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
															if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
															$b = "<font color='green' size='4'><b>";
															$thn = "";
															if($row->TahunKe == 1){ $thn = "st"; }
															if($row->TahunKe == 2){ $thn = "nd"; }
															if($row->TahunKe == 3){ $thn = "rd"; }
															if($r["ProgramID"] == "INT")
															{
																$TahunKe = "O";
															}
															else
															{
																$TahunKe = $row->TahunKe;
															}
															$ProgramID = $r["ProgramID"];
															$smt = "";
															if($row->Sesi == 1){ $smt = "st"; }
															if($row->Sesi == 2){ $smt = "nd"; }
															if($row->Sesi == 3){ $smt = "rd"; }
															if($row->Sesi == 4){ $smt = "th"; }
															if($row->Sesi == 5){ $smt = "th"; }
															if($row->Sesi == 6){ $smt = "th"; }
															if($row->suspend == "Y")
															{
																echo "<tr class='success'>
																		<td colspan='14'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
																	</tr>";
															}
															echo "<tr class='success'>
																	<td>$b $TahunKe$thn&nbsp;Year</td>
																	<td>$b $row->Sesi$smt&nbsp;Semester</td>
																	<td colspan='2'>$b Class:<br/>$TahunKe$kelas</td>
																	<td colspan='2'>$b Specialization:<br/>$spe</td>
																	<td colspan='4'>$b Homeroom:<br/>$hr1</td>
																	<td colspan='4'>$b Vice Homeroom:<br/>$hr2</td>
																</tr>";
															$MhswID = $row->MhswID;
															$KHSID = $row->KHSID;
															$TahunID = $row->TahunID;
															$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
															$totalbobot = 0;
															$ips = 0;
															$jumlahsks = 0;
															$jumlahexc = 0;
															$jumlahsic = 0;
															$jumlahabs = 0;
															$jumlahlat = 0;
															$jumlahmp = 0;
															$jumlahbobot = 0;
															if($rowkrs)
															{
																echo "
																	<tr>
																		<th rowspan='2' align='center'><p align='center'>#</p></th>
																		<th rowspan='2'><p align='center'>Subject</p></th>
																		<th rowspan='2'><p align='center'>Score</p></th>
																		<th colspan='4'><p align='center'>Attendance</p></th>
																		<th rowspan='2'><p align='center'>Minus/Plus</p></th>
																		<th rowspan='2'><p align='center'>Grade Value</p></th>
																		<th rowspan='2'><p align='center'>Exam Score</p></th>
																		<th rowspan='2'><p align='center'>Remedial Score</p></th>
																		<th rowspan='2'><p align='center'>Grade</p></th>
																		<th rowspan='2'><p align='center'>Credit</p></th>
																		<th rowspan='2'><p align='center'>Grade Point</p></th>
																	</tr>
																	<tr>
																		<th><p align='center'>Excuse</p></th>
																		<th><p align='center'>Sick</p></th>
																		<th><p align='center'>Absent</p></th>
																		<th><p align='center'>Late</p></th>
																	</tr>
																	";
																$ns = 1;
																foreach($rowkrs as $rkr)
																{
																	$TahunID = $rkr->TahunID;
																	$JadwalID = $rkr->JadwalID;
																	$SubjekID = $rkr->SubjekID;
																	$rsub = $this->m_subjek->PTL_select($SubjekID);
																	$subjek = $rsub["Nama"];
																	$sks = $rsub["SKS"];
																	$jumlahsks = $jumlahsks + $rsub["SKS"];
																	$KurikulumID = $rsub["KurikulumID"];
																	$JenisMKID = $rsub["JenisMKID"];
																	$GradeNilai = $rkr->GradeNilai;
																	$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
																	$bobot = 0;
																	if($resbobot)
																	{
																		$bobot = $resbobot['Bobot'];
																	}
																	$KRSID = $rkr->KRSID;
																	$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																	if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																	$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																	if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																	$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																	if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																	$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																	if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																	$tottexc = $texc;
																	$jumlahexc = $jumlahexc + $tottexc;
																	if($tottexc == 0){ $tottexc = ""; }
																	$tottsic = $tsic;
																	$jumlahsic = $jumlahsic + $tottsic;
																	if($tottsic == 0){ $tottsic = ""; }
																	$tottabs = $tabs;
																	$jumlahabs = $jumlahabs + $tottabs;
																	if($tottabs == 0){ $tottabs = ""; }
																	$tottlat = $tlat;
																	$jumlahlat = $jumlahlat + $tottlat;
																	if($tottlat == 0){ $tottlat = ""; }
																	$TahunID = $rkr->TahunID;
																	
																	$exam = 0;
																	$ExamID = "";
																	$LinkExamID = "";
																	$KodeExamID = "";
																	$RuangExamID = "";
																	$ExamMhswID = "";
																	$LinkExamMhswID = "";
																	$TotJum = 0;
																	$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
																	if($rexmhsw1)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw1 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA1)
																			{
																				if($rexmhswA1['Nilai'] > 0)
																				{
																					$LinkExamID .= 'A'.$row->ExamID;
																					$KodeExamID = $row->ExamID;
																					$RuangExamID = $row->RuangID;
																					$jumexam = $jumexam + $rexmhswA1['Nilai'];
																					$ExamMhswID = $rexmhswA1['ExamMhswID'];
																					$LinkExamMhswID .= 'A'.$rexmhswA1['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
																	if($rexmhsw2)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw2 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA2)
																			{
																				if($rexmhswA2['Nilai'] > 0)
																				{
																					$LinkExamID .= 'B'.$row->ExamID;
																					if($KodeExamID == "")
																					{
																						$KodeExamID = $row->ExamID;
																					}
																					if($RuangExamID == "")
																					{
																						$RuangExamID = $row->RuangID;
																					}
																					$jumexam = $jumexam + $rexmhswA2['Nilai'];
																					$ExamMhswID = $rexmhswA2['ExamMhswID'];
																					$LinkExamMhswID .= 'B'.$rexmhswA2['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
																	if($rexmhsw3)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw3 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA3)
																			{
																				if($rexmhswA3['Nilai'] > 0)
																				{
																					$LinkExamID .= 'C'.$row->ExamID;
																					if($KodeExamID == "")
																					{
																						$KodeExamID = $row->ExamID;
																					}
																					if($RuangExamID == "")
																					{
																						$RuangExamID = $row->RuangID;
																					}
																					$jumexam = $jumexam + $rexmhswA3['Nilai'];
																					$ExamMhswID = $rexmhswA3['ExamMhswID'];
																					$LinkExamMhswID .= 'C'.$rexmhswA3['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
																	if($rexmhsw4)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw4 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA4)
																			{
																				if($rexmhswA4['Nilai'] > 0)
																				{
																					$LinkExamID .= 'D'.$row->ExamID;
																					if($KodeExamID == "")
																					{
																						$KodeExamID = $row->ExamID;
																					}
																					if($RuangExamID == "")
																					{
																						$RuangExamID = $row->RuangID;
																					}
																					$jumexam = $jumexam + $rexmhswA4['Nilai'];
																					$ExamMhswID = $rexmhswA4['ExamMhswID'];
																					$LinkExamMhswID .= 'D'.$rexmhswA4['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
																	if($rexmhsw5)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw5 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA5)
																			{
																				if($rexmhswA5['Nilai'] > 0)
																				{
																					$LinkExamID .= 'E'.$row->ExamID;
																					if($KodeExamID == "")
																					{
																						$KodeExamID = $row->ExamID;
																					}
																					if($RuangExamID == "")
																					{
																						$RuangExamID = $row->RuangID;
																					}
																					$exam = $exam + $rexmhswA5['Nilai'];
																					$ExamMhswID = $rexmhswA5['ExamMhswID'];
																					$LinkExamMhswID .= 'E'.$rexmhswA5['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
																	if($rexmhsw6)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw6 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA6)
																			{
																				if($rexmhswA6['Nilai'] > 0)
																				{
																					$LinkExamID .= 'F'.$row->ExamID;
																					if($KodeExamID == "")
																					{
																						$KodeExamID = $row->ExamID;
																					}
																					if($RuangExamID == "")
																					{
																						$RuangExamID = $row->RuangID;
																					}
																					$exam = $exam + $rexmhswA6['Nilai'];
																					$ExamMhswID = $rexmhswA6['ExamMhswID'];
																					$LinkExamMhswID .= 'F'.$rexmhswA6['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	if($exam == 0)
																	{
																		$exam = "-";
																	}
																	else
																	{
																		if($TotJum > 1)
																		{
																			$exam = number_format(($exam),2,'.','');
																			// $exam = number_format(($exam / $TotJum),2,'.','');
																		}
																	}
																	$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
																	$remedial = "-";
																	$KRSRemedialID = "";
																	if($resremedial)
																	{
																		$remedial = $resremedial['Nilai'];
																		$KRSRemedialID = $resremedial['KRSRemedialID'];
																	}
																	$Trackingmp = "";
																	if($rkr->gradevalue == "")
																	{
																		$mp = "";
																	}
																	else
																	{
																		$JenisPresensiID = "E";
																		$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
																		$mptottexc = $texc * $pr1;
																		$JenisPresensiID = "S";
																		$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
																		$mptottsic = $tsic * $pr2;
																		$JenisPresensiID = "A";
																		$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
																		$mptottabs = $tabs * $pr3;
																		$JenisPresensiID = "L";
																		$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																		$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
																		$mptottlat = $tlat * $pr4;
																		if(($JenisMKID == "5") OR ($JenisMKID == "25")) // MKID Special
																		{
																			$mp = "";
																		}
																		else
																		{
																			$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
																		}
																		$Trackingmp .= "JenisMKID : $JenisMKID ~ $mptottexc + $mptottsic + $mptottabs + $mptottlat";
																		if($mp == 0.00)
																		{
																			$mp = "";
																		}
																		$jumlahmp = $jumlahmp + $mp;
																	}
																	$jumlahbobot = $bobot * $sks;
																	$totalbobot = $totalbobot + $jumlahbobot;
																	$optionsub = "";
																	if($rsub["Optional"] == "Y")
																	{
																		$optionsub = "<font size='1'> (Optional)</font>";
																	}
																	$resjenismk = $this->m_jenis_mk->PTL_select($JenisMKID);
																	$NamaMK = "";
																	$CheckMK = "<br/><font color='red'><b>MKID</b></font>";
																	if($resjenismk)
																	{
																		$NamaMK = $resjenismk["Nama"];
																		$CheckMK = "";
																	}
																	$StatusSubject = "";
																	if($rkr->NA == "Y")
																	{
																		$StatusSubject = "<br/><font color='red'><b>(Drop)</b></font>";
																	}
																	echo "<tr class='info'>
																			<td title='TahunID: $TahunID ~ KHSID: $KHSID'>$ns</td>
																			<td title='KRS ID: $rkr->KRSID ~ Subject ID: $rkr->SubjekID ~ JadwalID ID: $rkr->JadwalID ~ JenisMKID: $JenisMKID - $NamaMK'><a href='".site_url("attendance/ptl_list/$rkr->JadwalID")."' title='Go to Attendance' target='_blank'>$subjek$optionsub</a>$CheckMK<a href='".site_url("drop/ptl_search/".$MhswID."_-_".str_replace(" ","_",$r["Nama"]))."' title='Go to Student Subject Management' target='_blank'>$StatusSubject</a></td>
																			<td><p align='right'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID")."' title='Go to Scoring' target='_blank'>$rkr->NilaiAkhir</a></p></td>
																			<td><p align='center'>$tottexc</p></td>
																			<td><p align='center'>$tottsic</p></td>
																			<td><p align='center'>$tottabs</p></td>
																			<td><p align='center'>$tottlat</p></td>
																			<td title='$Trackingmp'><p align='right'>$mp</p></td>
																			<td><p align='right'>$rkr->gradevalue</p></td>
																			<td title='ExamID : $LinkExamID ~ ExamMhswID : $LinkExamMhswID'><p align='right'><a href='".site_url("exam/ptl_scoring/$KodeExamID/$RuangExamID")."' title='Go to Exam Score' target='_blank'>$exam</a></p></td>
																			<td title='KRSRemedialID : $KRSRemedialID'><p align='right'>$remedial</p></td>
																			<td><p align='center'>$rkr->GradeNilai</p></td>
																			<td><p align='center'>$sks</p></td>
																			<td><p align='right'>$jumlahbobot</p></td>
																		</tr>";
																	$rowmk = $this->m_mk->PTL_all_select($SubjekID);
																	if($rowmk)
																	{
																		foreach($rowmk as $rm)
																		{
																			if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 21) OR ($TahunID == 22) OR ($TahunID == 28) OR ($TahunID == 29) OR ($TahunID == 30) OR ($TahunID == 31))
																			{
																				$PresensiID = "";
																				$CekJenisPresensiID = "";
																				$MKID = $rm->MKID;
																				$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
																				$KRS2ID = "";
																				$Nilai = 0;
																				$AddNilai = 0;
																				if($rowkrs2)
																				{
																					$totNilai = 0;
																					$nkrs2 = 0;
																					foreach($rowkrs2 as $rkrs2)
																					{
																						if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																						{
																							$PresensiID = $rkrs2->PresensiID;
																							$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
																							if($resscoring)
																							{
																								$CekJenisPresensiID = $resscoring['JenisPresensiID'];
																							}
																						}
																						$KRS2ID .= $rkrs2->KRS2ID.' - ';
																						if($rkrs2->Nilai > 0)
																						{
																							$totNilai = $totNilai + $rkrs2->Nilai;
																							$nkrs2++;
																						}
																						$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
																					}
																					if($nkrs2 == 0)
																					{
																						$Nilai = 0;
																					}
																					else
																					{
																						if($nkrs2 < 0)
																						{
																							$Nilai = 0;
																						}
																						else
																						{
																							$Nilai = $totNilai / $nkrs2;
																						}
																					}
																				}
																				$tn = $Nilai + $AddNilai;
																				if($rowkrs2)
																				{
																					if($rm->Optional == "Y")
																					{
																						if($tn != 0)
																						{
																							echo "<tr class='info'>
																								<td>&nbsp;</td>
																								<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama (Optional)</a></font></td>
																								<td title='KRS2ID: $KRS2ID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																								<td colspan='11'>&nbsp;</td>
																							</tr>";
																						}
																					}
																					else
																					{
																						if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																						{
																							if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'><b>-</b></font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																							else
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																						}
																						else
																						{
																							echo "<tr class='info'>
																									<td>&nbsp;</td>
																									<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																									<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																									<td colspan='11'>&nbsp;</td>
																								</tr>";
																						}
																					}
																				}
																			}
																			else
																			{
																				$PresensiID = "";
																				$CekJenisPresensiID = "";
																				$CekMKID = $rm->MKID;
																				$rescekmk = $this->m_presensi->PTL_select_cek_mk_scoring($JadwalID,$CekMKID);
																				if($rescekmk)
																				{
																					$MKID = $rm->MKID;
																					$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
																					$KRS2ID = "";
																					$Nilai = 0;
																					$AddNilai = 0;
																					if($rowkrs2)
																					{
																						$totNilai = 0;
																						$nkrs2 = 0;
																						foreach($rowkrs2 as $rkrs2)
																						{
																							if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																							{
																								$PresensiID = $rkrs2->PresensiID;
																								$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
																								if($resscoring)
																								{
																									$CekJenisPresensiID = $resscoring['JenisPresensiID'];
																								}
																							}
																							$KRS2ID .= $rkrs2->KRS2ID.' - ';
																							if($rkrs2->Nilai > 0)
																							{
																								$totNilai = $totNilai + $rkrs2->Nilai;
																								$nkrs2++;
																							}
																							$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
																						}
																						if($nkrs2 > 0)
																						{
																							$Nilai = $totNilai / $nkrs2;
																						}
																					}
																					$tn = $Nilai + $AddNilai;
																					if($rowkrs2)
																					{
																						if($rm->Optional == "Y")
																						{
																							if($tn != 0)
																							{
																								echo "<tr class='info'>
																									<td>&nbsp;</td>
																									<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama (Optional)</a></font></td>
																									<td title='KRS2ID: $KRS2ID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																									<td colspan='11'>&nbsp;</td>
																								</tr>";
																							}
																						}
																						else
																						{
																							if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																							{
																								if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
																								{
																									echo "<tr class='info'>
																											<td>&nbsp;</td>
																											<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																											<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'><b>-</b></font></p></td>
																											<td colspan='11'>&nbsp;</td>
																										</tr>";
																								}
																								else
																								{
																									echo "<tr class='info'>
																											<td>&nbsp;</td>
																											<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																											<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																											<td colspan='11'>&nbsp;</td>
																										</tr>";
																								}
																							}
																							else
																							{
																								echo "<tr class='info'>
																										<td>&nbsp;</td>
																										<td title='MKID: $rm->MKID'><font size='1'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID/$rm->MKID")."' title='Go to Project' target='_blank'>$rm->Nama</a></font></td>
																										<td title='KRS2ID: $KRS2ID ~ JenisPresensiID: $CekJenisPresensiID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																										<td colspan='11'>&nbsp;</td>
																									</tr>";
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																	$ns++;
																}
															}
															$kumulatifsks = $kumulatifsks + $jumlahsks;
															if($totalbobot > 0)
															{
																$ips = number_format(($totalbobot / $jumlahsks), 2);
															}
															else
															{
																$ips = 0;
															}
															$kumulatifipk = $kumulatifipk + $ips;
															$ipk = number_format(($kumulatifipk / $kumulatif), 2);
															$data = array(
																		'IPS' => $ips,
																		'TotalSKS' => $jumlahsks,
																		'IP' => $ipk,
																		'SKS' => $kumulatifsks
																		);
															$this->m_khs->PTL_update($KHSID,$data);
															$resmax = $this->m_khs->PTL_select_drop($MhswID);
															$LastYear = $resmax['tahun'];
															if($LastYear == $TahunID)
															{
																$data = array(
																		'IPK' => $ipk,
																		'TotalSKS' => $kumulatifsks
																		);
																$resmax = $this->m_mahasiswa->PTL_update($MhswID,$data);
															}
															echo "
																<tr>
																	<td colspan='3' align='center'>TOTAL</td>
																	<td align='right'>$jumlahexc</td>
																	<td align='right'>$jumlahsic</td>
																	<td align='right'>$jumlahabs</td>
																	<td align='right'>$jumlahlat</td>
																	<td align='right'>$jumlahmp</td>
																	<td colspan='6'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Total Grade points earned this semester</td>
																	<td>:</td>
																	<td align='right'>$totalbobot</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Number of Credits earned this semester</td>
																	<td>:</td>
																	<td align='right'>$jumlahsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Grade Point Average earned this semester</td>
																	<td>:</td>
																	<td align='right'>$ips</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Credit earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$kumulatifsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Grade Point Average earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$ipk</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>";
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												}
											?>
										</tr>
									</tbody>
								</table>
								<center>
									<?php
										if($mhsw != "")
										{
											echo "<a href='".site_url("evaluation/ptl_pdf/ESMOD%20JAKARTA")."' class='btn btn-info'>&nbsp;Download PDF</a>
												<br/>
												<br/>";
										}
										// <a href='".site_url("evaluation/ptl_excel")."' class='btn btn-success'>Download Excel</a>
									?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>