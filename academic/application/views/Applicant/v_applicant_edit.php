		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Prospects Detail</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("interview"); ?>">Interview</a>
							>>
							<a href="<?php echo site_url("interview/ptl_applicant_edit/$AplikanID"); ?>">Prospects Detail</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php echo form_open_multipart('prospects/ptl_update',array('name' => 'contoh','class' => 'form-horizontal')); ?>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Applicant ID</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $AplikanID; ?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Marketing <?php echo $PresenterID; ?></label>
										<div class="col-lg-8">
											<?php
												if($marketing)
												{
													if($PresenterID == "")
													{
														$or = "ONLINE REGISTRATION";
													}
													foreach($marketing as $row)
													{
														$word = explode(" ",$row->nama);
														if($PresenterID == $word[0])
														{
															$or = "$word[0] - $row->nama";
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $or; ?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Status</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $StatusProspekID; ?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">PMB Period</label>
										<div class="col-lg-8">
											<?php
												if($pmbperiod)
												{
													foreach($pmbperiod as $rowpmb)
													{
														if($PMBPeriodID == $rowpmb->PMBPeriodID)
														{
															$pmb = "$rowpmb->PMBPeriodID - $rowpmb->Nama";
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $pmb; ?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Note</label>
										<div class="col-lg-8">
											<textarea readonly class="form-control"><?php echo $CatatanPresenter; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Photo</label>
										<div class="col-lg-8">
											<div class="fileupload fileupload-new" data-provides="fileupload">
												<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
													<?php
														if($foto == "-_-")
														{
													?>
															<img src="<?php echo base_url(); ?>../applicant/ptl_storage/admisi/aplikan/user.png" alt="" />
													<?php
														}
														else
														{
															$dir = substr($foto,0,7);
															$file = substr($foto,8);
													?>
															<img src="<?php echo base_url(); ?>../applicant/ptl_storage/admisi/aplikan/<?php echo $dir."/".$file; ?>" alt="" />
													<?php
														}
													?>
												</div>
											</div>
										</div>
									</div>
									<br/>
									<br/>
									<br/>
									<br/>
									<br/>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Name</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Nama; ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Gender</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="Kelamin" value="P" <?php if($Kelamin == "P"){echo "checked";} ?>> MALE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input disabled type="radio" name="Kelamin" value="W" <?php if($Kelamin == "W"){echo "checked";} ?>> FEMALE
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Type of Blood</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $GolonganDarah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Status</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="WargaNegara" value="WNI" onClick="pilihanmu()" <?php if($WargaNegara == "WNI"){echo "checked";} ?>> WNI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input disabled type="radio" name="WargaNegara" value="WNA" onClick="pilihanmu()" <?php if($WargaNegara == "WNA"){echo "checked";} ?>> WNA
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Country</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Kebangsaan; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Place of Birth</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $TempatLahir; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Date of Birth</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $TanggalLahir; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Age</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $umur; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Religion</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Agama; ?>" id="Agama" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Height</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $TinggiBadan; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Weight</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $BeratBadan; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Address</label>
										<div class="col-lg-8">
											<textarea readonly class="form-control"><?php echo $Alamat; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">City</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Kota; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">RT</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $RT; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">RW</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $RW; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Postal Code</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $KodePos; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Province</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Propinsi; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Country of Address</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Negara; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Phone</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Telepon; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Mobile Phone</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $Handphone; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Email</label>
										<div class="col-lg-8">
											<input readonly type="email" value="<?php echo $Email; ?>" class="form-control">
										</div>
									</div>
									<legend>Last Education</legend>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Last School</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $PendidikanTerakhir; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Name of Last School</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $AsalSekolah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Specialize</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $JurusanSekolah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Graduation Year</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $TahunLulus; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Final Exam Scores</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $NilaiSekolah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Already Working?</label>
										<div class="col-lg-8">
											<input disabled type="checkbox" value="Y" <?php if($SudahBekerja == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<legend>Parents Data</legend>
									<hr>
									<h4>FATHER</h4>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Name</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $NamaAyah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Religion</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $AgamaAyah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Last Education</label>
										<div class="col-lg-8">
											<?php
												$PendAyah = "";
												if($ortu)
												{
													foreach($ortu as $rowortu)
													{
														if($PendidikanAyah == strtoupper($rowortu->Nama))
														{
															$PendAyah = strtoupper($rowortu->Nama);
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $PendAyah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Job</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $PekerjaanAyah; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Address</label>
										<div class="col-lg-8">
											<textarea readonly class="form-control"><?php echo $AlamatOrtu; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">City</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $KotaOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">RT</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $RTOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">RW</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $RWOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Postal Code</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $KodePosOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Province</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $PropinsiOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Country of Address</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $NegaraOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Phone</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $TeleponOrtu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Mobile Phone</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $HandphoneOrtu; ?>" class="form-control">
										</div>
									</div>
									<h4>MOTHER</h4>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Name</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $NamaIbu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Religion</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $AgamaIbu; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Last Education</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo strtoupper($PendidikanIbu); ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Job</label>
										<div class="col-lg-8">
											<input readonly type="text" value="<?php echo $PekerjaanIbu; ?>" class="form-control">
										</div>
									</div>
									<legend>Elective Courses</legend>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Three Years Program (D3)</label>
										<div class="col-lg-8">
											<?php
												$nmd3 = "";
												if($d3)
												{
													foreach($d3 as $rowd3)
													{
														if($Pilihan1 == $rowd3->ProdiID)
														{
															$nmd3 = $rowd3->Nama;
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $nmd3; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">One Year Program (D1)</label>
										<div class="col-lg-8">
											<?php
												$nmd1 = "";
												if($d1)
												{
													foreach($d1 as $rowd1)
													{
														if($Pilihan2 == $rowd1->ProdiID)
														{
															$nmd1 = $rowd1->Nama;
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $nmd1; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Short Course</label>
										<div class="col-lg-8">
											<?php
												$nmsc = "";
												if($kursussingkat)
												{
													foreach($kursussingkat as $rowks)
													{
														if($Pilihan3 == $rowks->KursusSingkatID)
														{
															$nmsc = $rowks->Nama;
														}
													}
												}
											?>
											<input readonly type="text" value="<?php echo $nmsc; ?>" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">On the initiative of whom Mr/s come to ESMOD Jakarta?</label>
										<div class="col-lg-8">
											<textarea readonly class="form-control"><?php echo $Catatan; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Resources</label>
										<div class="col-lg-8">
											<?php
												if($sumberinfo)
												{
													$no = 0;
													foreach($sumberinfo as $rowsi)
													{
														$no++;
														echo "<input disabled type='checkbox' name='SumberInformasi$no' value='$rowsi->InfoID'";
														if(strpos("__".$SumberInformasi,"_".$rowsi->InfoID."_"))
														{
															echo "checked";
														}
														echo ">$rowsi->Nama<br>";
													}
												}
											?>
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">1. Already Register?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="NA" value="Y" <?php if($NA == "Y"){echo "checked";} ?>> No
											<input disabled type="radio" name="NA" value="N" <?php if($NA == "N"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">2. Already Payment Form?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="SudahBayar" value="N" <?php if($SudahBayar == "N"){echo "checked";} ?>> No
											<input disabled type="radio" name="SudahBayar" value="Y" <?php if($SudahBayar == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">3. Already Test?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="LulusUjian" value="N" <?php if($LulusUjian == "N"){echo "checked";} ?>> No
											<input disabled type="radio" name="LulusUjian" value="Y" <?php if($LulusUjian == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">4. Already Completeness File?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="SyaratLengkap" value="N" <?php if($SyaratLengkap == "N"){echo "checked";} ?>> No
											<input disabled type="radio" name="SyaratLengkap" value="Y" <?php if($SyaratLengkap == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">5. Already Payment 1st Semester?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="diterima" value="N" <?php if($diterima == "N"){echo "checked";} ?>> No
											<input disabled type="radio" name="diterima" value="Y" <?php if($diterima == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Postpone?</label>
										<div class="col-lg-8">
											<input disabled type="radio" name="postpone" value="N" <?php if($postpone == "N"){echo "checked";} ?>> No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input disabled type="radio" name="postpone" value="Y" <?php if($postpone == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<hr>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4"></label>
										<div class="col-lg-8">
											<a href="<?php echo site_url("interview"); ?>" class="btn btn-warning">BACK</a>
										</div>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>