		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("account"); ?>">Account</a>
							>>
							<a href="<?php echo site_url("account/ptl_edit/$RekeningID"); ?>">Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("account/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="RekeningID" value="<?php echo $RekeningID; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Bank</label>
											<input type="text" name="Bank" value="<?php echo $Bank; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Location</label>
											<input type="text" name="Cabang" value="<?php echo $Cabang; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Def</label>
											<input type="checkbox" name="Def" value="Y" <?php if($Def == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("account"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>