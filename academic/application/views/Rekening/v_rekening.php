		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Institutional Account</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("account"); ?>">Institutional Account</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of institutional account.
								</div>
								<center><a href="<?php echo site_url("account/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Bank</th>
											<th>Location</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>
																<td>$no</td>
																<td>$row->RekeningID</td>
																<td>$row->Nama</td>
																<td>$row->Bank</td>
																<td>$row->Cabang</td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("account/ptl_edit/$row->RekeningID")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													else
													{
														echo "<tr>
																<td>$no</td>
																<td>$row->RekeningID</td>
																<td>$row->Nama</td>
																<td>$row->Bank</td>
																<td>$row->Cabang</td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("account/ptl_edit/$row->RekeningID")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>