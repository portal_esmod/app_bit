		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Publication</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("publication"); ?>">Publication (PUBLI01)</a>
                        </div>
                        <div class="panel-body">
								<div class="table-responsive"><div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of news for students. <a href="<?php echo site_url("tutorial/ptl_detail/PUBL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<center><a href="<?php echo site_url("publication/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Title</th>
											<th>File</th>
											<th>Created Date</th>
											<th>Last Updated</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td>$no</td>
															<td>$row->BeritaID</td>
															<td>$row->JudulBerita</td>";
															if($row->UploadFile == "")
															{
																echo "<td>No File</td>";
															}
															else
															{
																echo "<td><a href='".site_url("publication/ptl_download/$row->UploadFile")."' class='btn btn-default'>".substr($row->UploadFile,8)."</a></td>";
															}	
														echo "<td>$row->tanggal_buat</td>
															<td>$row->tanggal_edit</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("publication/ptl_edit/$row->BeritaID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>