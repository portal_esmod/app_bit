		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("publication"); ?>">Program</a>
							>>
							<a href="<?php echo site_url("publication/ptl_form"); ?>">Add New (PUBLI02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php echo form_open_multipart('publication/ptl_insert'); ?>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="JudulBerita" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Content</label>
											<script src="<?php echo base_url(); ?>assets/cekeditor/ckeditor.js"></script>
											<textarea name="IsiBerita" class="ckeditor" cols="80000" id="editor1" rows="10000" class="form-control" required></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>File</label>
											<input name="userfile" type="file" class="btn btn-warning" accept="*" />
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("publication"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>