		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Change Password</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("login/ptl_password"); ?>">Change Password</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("login/ptl_password_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Old Password</label>
											<input type="password" name="pass_lama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>New Password</label>
											<input type="password" name="pass_baru1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Confirm New Password</label>
											<input type="password" name="pass_baru2" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("login/ptl_home"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>