			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Dashboard</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-info">
							<i class="fa fa-folder-open"></i><b>&nbsp;Hello ! </b>Welcome Back <b><?php echo $_COOKIE["id_akun"]."_".$_COOKIE["nama"]; ?></b>. <?php echo $last_login.$last_logout.$total_hour; if($tanggal_pengingat != ""){ echo "<br/><font color='red'><b> System will remember you until $tanggal_pengingat.</b></font>"; } ?>
						</div>
					</div>
				</div>
				<div class="row">
					<?php
						if($gambar_ultah != "")
						{
					?>
							<div class="col-lg-12">
								<div class="panel panel-primary text-center no-boder">
									<center>
										<?php echo $gambar_ultah; ?>
									</center>
								</div>
							</div>
					<?php
						}
						if(($rowrecord) AND ($DosenID != ""))
						{
					?>
							<div class="col-lg-12">
								<div class="panel panel-primary text-center no-boder">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<?php
												$perangkat_client = $this->log->getPerangkat();
												$TotalRate = 0;
												$pembagi = 0;
												foreach($rowrecord as $row)
												{
													$id_rating_question = $row->id_rating_question;
													$resAnswer = $this->m_rating->PTL_select_sum_dosen($id_rating_question,$DosenID);
													$RowAnswer = $this->m_rating->PTL_all_spesifik($id_rating_question,$DosenID);
													if($RowAnswer)
													{
														$TotAnswer = count($RowAnswer);
														$TotalRate = $TotalRate + ($resAnswer['TotalRate'] / $TotAnswer);
														$pembagi++;
													}
												}
												$HasilRate = 0;
												if($pembagi > 0)
												{
													$HasilRate = number_format(($TotalRate / $pembagi),2);
												}
												$no = 1;
												if($perangkat_client == 'Desktop')
												{
													echo "
														<tr>
															<th colspan='5'><p align='center'><b>$_COOKIE[id_akun] - $_COOKIE[nama] ($DosenID)</b></p></th>
														</tr>
														<tr>
															<th colspan='5'><p align='center'><b>$namarating</b></p></th>
														</tr>
														<tr>
															<th><b>#</b></th>
															<th><p align='center'><b>Criteria</b></p></th>
															<th width='15%'><p align='center'><b>Rating</b></p></th>
															<th><p align='center'><b>Value</b></p></th>
															<th rowspan='9'><p align='center' valign='bottom'><font size='10'><b><br/><br/>$HasilRate</b></font></p></th>
														</tr>
														";
													foreach($rowrecord as $row)
													{
														$id_rating_question = $row->id_rating_question;
														$resAnswer = $this->m_rating->PTL_select_sum_dosen($id_rating_question,$DosenID);
														$RowAnswer = $this->m_rating->PTL_all_spesifik($id_rating_question,$DosenID);
														$TotalRate = 0;
														if($RowAnswer)
														{
															$TotAnswer = count($RowAnswer);
															$TotalRate = number_format(($resAnswer['TotalRate'] / $TotAnswer),2);
														}
														$Ck1 = "";
														$Ck2 = "";
														$Ck3 = "";
														$Ck4 = "";
														$Ck5 = "";
														$Ck6 = "";
														$Ck7 = "";
														$Ck8 = "";
														$Ck9 = "";
														$Ck10 = "";
														if($resAnswer)
														{
															if(($TotalRate >= '0.5') AND ($TotalRate < '1.0')){ $Ck1 = "checked"; }
															if(($TotalRate >= '1.0') AND ($TotalRate < '1.5')){ $Ck2 = "checked"; }
															if(($TotalRate >= '1.5') AND ($TotalRate < '2.0')){ $Ck3 = "checked"; }
															if(($TotalRate >= '2.0') AND ($TotalRate < '2.5')){ $Ck4 = "checked"; }
															if(($TotalRate >= '2.5') AND ($TotalRate < '3.0')){ $Ck5 = "checked"; }
															if(($TotalRate >= '3.0') AND ($TotalRate < '3.5')){ $Ck6 = "checked"; }
															if(($TotalRate >= '3.5') AND ($TotalRate < '4.0')){ $Ck7 = "checked"; }
															if(($TotalRate >= '4.0') AND ($TotalRate < '4.5')){ $Ck8 = "checked"; }
															if(($TotalRate >= '4.5') AND ($TotalRate < '5.0')){ $Ck9 = "checked"; }
															if($TotalRate >= '5.0'){ $Ck10 = "checked"; }
														}
														echo "
															<tr>
																<td><b>$no.</b></td>
																<td align='left'><b><font color='red'>&nbsp;$row->pertanyaan</b></td>
																<td>
																	<fieldset id='demo$no' class='rating' disabled>
																		<input class='stars' type='radio' id='star53$no' name='rating$no' $Ck10/>
																		<label class='full' for='star53$no' title='Excellent - 5 stars'></label>
																		<input class='stars' type='radio' id='star4half3$no' name='rating$no' $Ck9/>
																		<label class='half' for='star4half3$no' title='Very Good - 4.5 stars'></label>
																		<input class='stars' type='radio' id='star43$no' name='rating$no' $Ck8/>
																		<label class='full' for='star43$no' title='Pretty good - 4 stars'></label>
																		<input class='stars' type='radio' id='star3half3$no' name='rating$no' $Ck7/>
																		<label class='half' for='star3half3$no' title='Above Average - 3.5 stars'></label>
																		<input class='stars' type='radio' id='star33$no' name='rating$no' $Ck6/>
																		<label class='full' for='star33$no' title='Average - 3 stars'></label>
																		<input class='stars' type='radio' id='star2half3$no' name='rating$no' $Ck5/>
																		<label class='half' for='star2half3$no' title='Below Average - 2.5 stars'></label>
																		<input class='stars' type='radio' id='star23$no' name='rating$no' $Ck4/>
																		<label class='full' for='star23$no' title='Fairly Bad - 2 stars'></label>
																		<input class='stars' type='radio' id='star1half3$no' name='rating$no' $Ck3/>
																		<label class='half' for='star1half3$no' title='Bad - 1.5 stars'></label>
																		<input class='stars' type='radio' id='star13$no' name='rating$no' $Ck2/>
																		<label class='full' for='star13$no' title='Very Bad - 1 star'></label>
																		<input class='stars' type='radio' id='starhalf3$no' name='rating$no' $Ck1/>
																		<label class='half' for='starhalf3$no' title='Poor - 0.5 stars'></label>
																	</fieldset>
																</td>
																<td align='left'><b><font color='green'>$TotalRate</b></td>
															</tr>
															";
														$no++;
													}
												}
												else
												{
													$Ck1 = "";
													$Ck2 = "";
													$Ck3 = "";
													$Ck4 = "";
													$Ck5 = "";
													$Ck6 = "";
													$Ck7 = "";
													$Ck8 = "";
													$Ck9 = "";
													$Ck10 = "";
													if($resAnswer)
													{
														if(($HasilRate >= '0.5') AND ($HasilRate < '1.0')){ $Ck1 = "checked"; }
														if(($HasilRate == '1.0') AND ($HasilRate < '1.5')){ $Ck2 = "checked"; }
														if(($HasilRate == '1.5') AND ($HasilRate < '2.0')){ $Ck3 = "checked"; }
														if(($HasilRate == '2.0') AND ($HasilRate < '2.5')){ $Ck4 = "checked"; }
														if(($HasilRate == '2.5') AND ($HasilRate < '3.0')){ $Ck5 = "checked"; }
														if(($HasilRate == '3.0') AND ($HasilRate < '3.5')){ $Ck6 = "checked"; }
														if(($HasilRate == '3.5') AND ($HasilRate < '4.0')){ $Ck7 = "checked"; }
														if(($HasilRate == '4.0') AND ($HasilRate < '4.5')){ $Ck8 = "checked"; }
														if(($HasilRate == '4.5') AND ($HasilRate < '5.0')){ $Ck9 = "checked"; }
														if($HasilRate == '5.0'){ $Ck10 = "checked"; }
													}
													echo "
														<tr>
															<th><p align='center'><b>$_COOKIE[id_akun] - $_COOKIE[nama] ($DosenID)</b></p></th>
														</tr>
														<tr>
															<th><p align='center'><b>Rating</b></p></th>
														</tr>
														<tr>
															<td align='center'>
																<fieldset id='demo$no' class='rating' disabled>
																	<input class='stars' type='radio' id='star53$no' name='rating$no' $Ck10/>
																	<label class='full' for='star53$no' title='Excellent - 5 stars'></label>
																	<input class='stars' type='radio' id='star4half3$no' name='rating$no' $Ck9/>
																	<label class='half' for='star4half3$no' title='Very Good - 4.5 stars'></label>
																	<input class='stars' type='radio' id='star43$no' name='rating$no' $Ck8/>
																	<label class='full' for='star43$no' title='Pretty good - 4 stars'></label>
																	<input class='stars' type='radio' id='star3half3$no' name='rating$no' $Ck7/>
																	<label class='half' for='star3half3$no' title='Above Average - 3.5 stars'></label>
																	<input class='stars' type='radio' id='star33$no' name='rating$no' $Ck6/>
																	<label class='full' for='star33$no' title='Average - 3 stars'></label>
																	<input class='stars' type='radio' id='star2half3$no' name='rating$no' $Ck5/>
																	<label class='half' for='star2half3$no' title='Below Average - 2.5 stars'></label>
																	<input class='stars' type='radio' id='star23$no' name='rating$no' $Ck4/>
																	<label class='full' for='star23$no' title='Fairly Bad - 2 stars'></label>
																	<input class='stars' type='radio' id='star1half3$no' name='rating$no' $Ck3/>
																	<label class='half' for='star1half3$no' title='Bad - 1.5 stars'></label>
																	<input class='stars' type='radio' id='star13$no' name='rating$no' $Ck2/>
																	<label class='full' for='star13$no' title='Very Bad - 1 star'></label>
																	<input class='stars' type='radio' id='starhalf3$no' name='rating$no' $Ck1/>
																	<label class='half' for='starhalf3$no' title='Poor - 0.5 stars'></label>
																</fieldset>
																&nbsp;
																&nbsp;
																&nbsp;
																&nbsp;
																<font size='10'><b>$HasilRate</b></font>
															</td>
														</tr>
														";
												}
											?>
										</table>
									</div>
								</div>
							</div>
					<?php
						}
						if(($rowpresensi) OR ($rowexam))
						{
					?>
							<div class="col-lg-12">
								<div class="panel panel-primary text-center no-boder">
									<div class="table-responsive">
										<h3><font color="red">List of projects you should be set the score.</font></h3>
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<th>#</th>
												<th>Attendance ID</th>
												<th>Subject</th>
												<th>Project</th>
												<th>Topic</th>
												<th>Date</th>
												<th>Start Time</th>
												<th>End TIme</th>
												<th>Actions</th>
											</tr>
											<?php
												$_COOKIE["id_akun"];
												$no = 1;
												if($rowpresensi)
												{
													foreach($rowpresensi as $rp)
													{
														$MKID = $rp->MKID;
														$JadwalID = $rp->JadwalID;
														$rowjadwal = $this->m_presensi->PTL_all_lecturer_expired_related($JadwalID,$MKID);
														$TotalNilai = 0;
														if($rowjadwal)
														{
															foreach($rowjadwal as $rj)
															{
																$PresensiID = $rj->PresensiID;
																$rowkrs = $this->m_krs2->PTL_all_lecturer_expired($PresensiID,$MKID);
																if($rowkrs)
																{
																	foreach($rowkrs as $rk)
																	{
																		$TotalNilai = $TotalNilai + $rk->AdditionalNilai;
																	}
																}
															}
														}
														if($TotalNilai == 0)
														{
															$PresensiID = $rp->PresensiID;
															$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
															$SubjekID = $resjadwal['SubjekID'];
															$ressubjek = $this->m_subjek->PTL_select($SubjekID);
															$resmk = $this->m_mk->PTL_select($MKID);
															$NamaMk = "<font color='red'><b>not set</b></font>";
															if($resmk)
															{
																$NamaMk = $resmk['Nama'];
															}
															$Catatan = "<font color='red'><b>not set</b></font>";
															if($rp->Catatan != "")
															{
																$Catatan = $rp->Catatan;
															}
															echo "<tr>
																	<td>$no</td>
																	<td>$PresensiID</td>
																	<td align='left' title='JadwalID : $JadwalID'>$ressubjek[Nama]</td>
																	<td align='left' title='MKID : $MKID'>$NamaMk</td>
																	<td align='left'>$Catatan</td>
																	<td>".tgl_singkat_eng($rp->Tanggal)."</td>
																	<td>".substr($rp->JamMulai,0,5)."</td>
																	<td>".substr($rp->JamSelesai,0,5)."</td>";
																	$rowattendance1 = $this->m_krs2->PTL_all_lecturer_expired_att($PresensiID);
																	if($rowattendance1)
																	{
																		$rowattendance2 = $this->m_presensi_mahasiswa->PTL_all_lecturer_expired($PresensiID);
																		if($rowattendance2)
																		{
																			$datanotif = array(
																							'id_akun' => $atasan,
																							'nama' => $NamaAtasan,
																							'noteid' => "",
																							'title' => "$_COOKIE[nama] has not filled out the student attendance. Subject $ressubjek[Nama], date ".tgl_singkat_eng($rp->Tanggal).", start time ".substr($rp->JamMulai,0,5).", end time ".substr($rp->JamSelesai,0,5).".",
																							'nama_buat' => $_COOKIE["nama"],
																							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																							'tanggal_buat' => $this->waktu
																							);
																			$this->m_notifikasi->PTL_insert($datanotif);
																			echo "<td><a href='".site_url("attendance/ptl_list_attendance/$PresensiID/$resjadwal[TahunID]/$MKID")."' target='_blank' class='btn btn-info'>Set Attendance</a></td>";
																		}
																		else
																		{
																			$datanotif = array(
																							'id_akun' => $atasan,
																							'nama' => $NamaAtasan,
																							'noteid' => "",
																							'title' => "$_COOKIE[nama] has not filled out the student score.. Subject $ressubjek[Nama], date ".tgl_singkat_eng($rp->Tanggal).", start time ".substr($rp->JamMulai,0,5).", end time ".substr($rp->JamSelesai,0,5).".",
																							'nama_buat' => $_COOKIE["nama"],
																							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																							'tanggal_buat' => $this->waktu
																							);
																			$this->m_notifikasi->PTL_insert($datanotif);
																			echo "<td><a href='".site_url("scoring/ptl_list/$JadwalID/$MKID")."' target='_blank' class='btn btn-success'>Set Score</a></td>";
																		}
																	}
																	else
																	{
																		$datanotif = array(
																							'id_akun' => $atasan,
																							'nama' => $NamaAtasan,
																							'noteid' => "",
																							'title' => "$_COOKIE[nama] has not filled out the student attendance. Subject $ressubjek[Nama], date ".tgl_singkat_eng($rp->Tanggal).", start time ".substr($rp->JamMulai,0,5).", end time ".substr($rp->JamSelesai,0,5).".",
																							'nama_buat' => $_COOKIE["nama"],
																							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																							'tanggal_buat' => $this->waktu
																							);
																			$this->m_notifikasi->PTL_insert($datanotif);
																		echo "<td><a href='".site_url("attendance/ptl_list_attendance/$PresensiID/$resjadwal[TahunID]/$MKID")."' target='_blank' class='btn btn-info'>Set Attendance</a></td>";
																	}
															echo "</tr>
																";
															$no++;
														}
													}
												}
												else
												{
													echo "
														<tr>
															<td colspan='9'><font color='green'><b>Completed</b></font></td>
														</tr>
														";
												}
										?>
										</table>
										<h3><font color="red">List of exam you should be set the score.</font></h3>
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<th>#</th>
												<th>Attendance ID</th>
												<th>Subject</th>
												<th>Date</th>
												<th>Start Time</th>
												<th>End TIme</th>
												<th>Actions</th>
											</tr>
										<?php
												if($rowexam)
												{
													foreach($rowexam as $re)
													{
														$ExamID = $re->ExamID;
														$rowexammhsw = $this->m_exam_mahasiswa->PTL_all_lecturer_expired($ExamID);
														$TotNilai = 0;
														if($rowexammhsw)
														{
															foreach($rowexammhsw as $remhsw)
															{
																$TotNilai = $TotNilai + $remhsw->Nilai;
															}
														}
														if($TotNilai == 0)
														{
															$NamaSubjek = "";
															$SubjekID = $re->SubjekID;
															$ressubjek = $this->m_subjek->PTL_select($SubjekID);
															if($ressubjek)
															{
																$NamaSubjek .= $ressubjek['Nama'].'<br/>';
															}
															$SubjekID = $re->SubjekID12;
															$ressubjek = $this->m_subjek->PTL_select($SubjekID);
															if($ressubjek)
															{
																$NamaSubjek .= $ressubjek['Nama'].'<br/>';
															}
															$SubjekID = $re->SubjekID2;
															$ressubjek = $this->m_subjek->PTL_select($SubjekID);
															if($ressubjek)
															{
																$NamaSubjek .= $ressubjek['Nama'].'<br/>';
															}
															echo "<tr>
																	<td>$no</td>
																	<td>$re->ExamID</td>
																	<td align='left'>$NamaSubjek</td>
																	<td>".tgl_singkat_eng($re->Tanggal)."</td>
																	<td>".substr($re->JamMulai,0,5)."</td>
																	<td>".substr($re->JamSelesai,0,5)."</td>
																	<td><a href='".site_url("exam/ptl_scoring/$ExamID/$re->RuangID")."' target='_blank' class='btn btn-success'>Set Score</a></td>
																</tr>
																";
															$no++;
														}
													}
												}
												else
												{
													echo "
														<tr>
															<td colspan='7'><font color='green'><b>Completed</b></font></td>
														</tr>
														";
												}
											?>
										</table>
									</div>
								</div>
							</div>
					<?php
						}
					?>
					<div class="col-lg-12">
						<div class="panel panel-primary text-center no-boder">
							<div class="table-responsive">
								<h3><font color="red">If you encounter any problems related to this kind of thing, you can contact the person in charge:</font></h3>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<th>ITEMS</th>
										<th>PIC</th>
										<th>&nbsp;</th>
										<th>ITEMS</th>
										<th>PIC</th>
									</tr>
									<tr>
										<td align="left">Academic Year</td>
										<td align="left">Anisa Fajar</td>
										<td>&nbsp;</td>
										<td align="left">Academic Calendar</td>
										<td align="left">Anisa Fajar</td>
									</tr>
									<tr>
										<td align="left">Class</td>
										<td align="left">Miha and Rossy</td>
										<td>&nbsp;</td>
										<td align="left">Language</td>
										<td align="left">Rossy</td>
									</tr>
									<tr>
										<td align="left">Specialization</td>
										<td align="left">Rossy</td>
										<td>&nbsp;</td>
										<td align="left">Homeroom</td>
										<td align="left">Rossy</td>
									</tr>
									<tr>
										<td align="left">Schedule</td>
										<td align="left">Patrice and Anisa Fajar</td>
										<td>&nbsp;</td>
										<td align="left">Attendance</td>
										<td align="left">All Teacher and Rossy</td>
									</tr>
									<tr>
										<td align="left">Enrollment</td>
										<td align="left">Rossy</td>
										<td>&nbsp;</td>
										<td align="left">Student Subject Management</td>
										<td align="left">Miha</td>
									</tr>
									<tr>
										<td align="left">Scoring</td>
										<td align="left">All Teacher and Coordinator</td>
										<td>&nbsp;</td>
										<td align="left">Exam</td>
										<td align="left">All Teacher, Miha and Rossy</td>
									</tr>
									<tr>
										<td align="left">Remedial</td>
										<td align="left">All Teacher, Miha and Rossy</td>
										<td>&nbsp;</td>
										<td align="left">Student Evaluation</td>
										<td align="left">Miha and Rossy</td>
									</tr>
									<tr>
										<td align="left">Score for Applicant</td>
										<td align="left">Patrice</td>
										<td>&nbsp;</td>
										<td align="left">Master Table</td>
										<td align="left">Patrice</td>
									</tr>
									<tr>
										<td align="left">Lecturer Data</td>
										<td align="left">Anisa Fajar</td>
										<td>&nbsp;</td>
										<td align="left">Campus and Room</td>
										<td align="left">Anisa Fajar</td>
									</tr>
									<tr>
										<td align="left">Curriculum</td>
										<td align="left">Patrice</td>
										<td>&nbsp;</td>
										<td align="left">Create New, Update and Delete Specialization</td>
										<td align="left">Anisa Fajar</td>
									</tr>
									<tr>
										<td align="left">Create New, Update and Delete Subject</td>
										<td align="left">Patrice</td>
										<td>&nbsp;</td>
										<td align="left">Create New, Update and Delete Project</td>
										<td align="left">Your Coordinator</td>
									</tr>
								</table>
								<h4><font color="red">For system error like this </font></h4><?php echo $error; ?>
								<h4><font color="red">Please contact Lendra Permana as IT Developer.</font></h4>
								<h4><font color="red">If you have problem with your internet connection or Wi-Fi Register. Please contact Wishnu Prasasti as IT Network.</font></h4>
								<h3><font color="red"><b>We hope that you contact the right person.</b></font></h3>
								<br/>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="panel panel-primary text-center no-boder">
							<div class="panel-body yellow">
								<i class="fa fa-bar-chart-o fa-3x"></i>
								<h3><?php echo count($active); ?></h3>
							</div>
							<div class="panel-footer">
								<span class="panel-eyecandy-title">Students Active
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="panel panel-primary text-center no-boder">
							<div class="panel-body blue">
								<i class="fa fa-bar-chart-o fa-3x"></i>
								<h3><?php echo count($leave); ?></h3>
							</div>
							<div class="panel-footer">
								<span class="panel-eyecandy-title">Students Leave
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="panel panel-primary text-center no-boder">
							<div class="panel-body red">
								<i class="fa fa-bar-chart-o fa-3x"></i>
								<h3><?php echo count($drop_out); ?></h3>
							</div>
							<div class="panel-footer">
								<span class="panel-eyecandy-title">Students Drop-Out
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="panel panel-primary text-center no-boder">
							<div class="panel-body green">
								<i class="fa fa-bar-chart-o fa-3x"></i>
								<h3><?php echo count($graduated); ?></h3>
							</div>
							<div class="panel-footer">
								<span class="panel-eyecandy-title">Students Graduated
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<center>Last Login</center>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>#</th>
												<th>Account ID</th>
												<th>Name</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($rowrecord1)
												{
													$n = 1;
													foreach($rowrecord1 as $row)
													{
														if(strpos($row->aplikasi,"ac") !== false)
														{
															if($n < 21)
															{
																echo "<tr>
																		<td>$n</td>
																		<td>$row->id_akun</td>
																		<td title='$row->akses'>$row->nama</td>
																		<td>$row->last_login</td>
																	</tr>";
																$n++;
															}
														}
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<center>Last Logout</center>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>#</th>
												<th>Account ID</th>
												<th>Name</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($rowrecord2)
												{
													$n = 1;
													foreach($rowrecord2 as $row)
													{
														if(strpos($row->aplikasi,"ac") !== false)
														{
															if($n < 21)
															{
																echo "<tr>
																		<td>$n</td>
																		<td>$row->id_akun</td>
																		<td title='$row->akses'>$row->nama</td>
																		<td>$row->last_logout</td>
																	</tr>";
																$n++;
															}
														}
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>