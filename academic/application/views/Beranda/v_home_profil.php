		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Profile</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("login/ptl_profile"); ?>">Profile</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php echo form_open_multipart('login/ptl_profile_update',array('name' => 'contoh')); ?>
									<div class="col-lg-6">
										<center>
											<?php
												if($foto != "")
												{
													$foto = "../hris/ptl_storage/foto_karyawan/$foto";
													$exist = file_exists_remote(base_url("$foto"));
													if($exist)
													{
														$foto = $foto;
													}
													else
													{
														$foto = "ptl_storage/foto_umum/user.jpg";
													}
												}
												else
												{
													$foto = "ptl_storage/foto_umum/user.jpg";
												}
											?>
											<div class="form-group">
												<img src="<?php echo base_url("$foto"); ?>" alt="" width="200px">
												<br/>
												<br/>
												<input disabled type="file" name="userfile" accept="image/*" class="btn btn-primary">
												<p class="help-block"></p>
											</div>
										</center>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input readonly type="text" name="id_akun" value="<?php echo $id_akun; ?>" class="form-control">
											<input type="hidden" name="foto_lama" value="<?php echo $foto_lama; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input readonly type="text" name="nama" value="<?php echo $nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Place of Birth</label>
											<input type="text" name="tempat" value="<?php echo $tempat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of Birth</label>
											<input readonly type="text" name="tanggal_lahir" value="<?php echo $tanggal_lahir; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Nationality</label>
											<input type="text" name="kebangsaan" value="<?php echo $kebangsaan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Civil Status</label>
											<input type="text" name="status_sipil" value="<?php echo $status_sipil; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Religion</label>
											<input type="text" name="agama" value="<?php echo $agama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="alamat_domisili" class="form-control"><?php echo $alamat_domisili; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="kota" value="<?php echo $kota; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postal Code</label>
											<input type="text" name="kode_pos" value="<?php echo $kode_pos; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="telepon" value="<?php echo $telepon; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input type="text" name="handphone" value="<?php echo $handphone; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Work E-Mail</label>
											<input type="email" name="email" value="<?php echo $email; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Private Mail</label>
											<input type="email" name="email2" value="<?php echo $email2; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created by</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date Created</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("login/ptl_home"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>