		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students"); ?>">Students</a>
							>>
							<a href="<?php echo site_url("students/ptl_edit/$MhswID"); ?>">Detail</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<p class="help-block"></p>
									</div>
								</div>
								<?php echo form_open_multipart('students/ptl_update',array('name' => 'contoh')); ?>
									<div class="col-lg-6">
										<center>
											<?php
												if($Foto == "")
												{
													$foto = "foto_umum/user.jpg";
												}
												else
												{
													$foto = "foto_mahasiswa/$Foto";
													$exist = file_exists_remote(base_url("ptl_storage/$foto"));
													if($exist)
													{
														$foto = $foto;
													}
													else
													{
														$foto = "foto_umum/user.jpg";
													}
												}
											?>
											<div class="form-group">
												<img src="<?php echo base_url("ptl_storage/$foto"); ?>" alt="" width="200px">
												<br/>
												<br/>
												<input type="file" name="userfile" accept="image/*" class="btn btn-primary">
												<p class="help-block"></p>
											</div>
											<br/>
											<br/>
											<div class="table-responsive">
												<div class="alert alert-info">
													<a class="alert-link">Notes: </a>List of Private Notes from Academic Staff and Teachers.
												</div>
												<a href="<?php echo site_url("students/ptl_note_form/$MhswID"); ?>" class="btn btn-primary">Add New</a>
												<br/>
												<br/>
												<table class="table table-striped table-bordered table-hover" id="dataTables-example">
													<thead>
														<tr>
															<th>#</th>
															<th>Title</th>
															<th>Posting</th>
															<th>Updated</th>
															<th>Status</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php
															if($rowcatatan)
															{
																$no = 1;
																foreach($rowcatatan as $row)
																{
																	if($row->baca == "Y")
																	{
																		echo "<tr class='success'>";
																		$status = "READ";
																	}
																	else
																	{
																		echo "<tr class='danger'>";
																		$status = "<font color='red'><b>UNREAD</b></font>";
																	}
																		echo "<td title='Noteid : $row->noteid'>$no</td>
																			<td>$row->title</td>
																			<td>$row->tanggal_buat</td>
																			<td>$row->tanggal_edit</td>
																			<td>$status</td>
																			<td>
																				<a href='".site_url("students/ptl_note_edit/$MhswID/$row->noteid")."' class='btn btn-info'><i class='fa fa-pencil'></i></a>
																				<a href='".site_url("students/ptl_note_delete/$MhswID/$row->noteid")."' class='btn btn-danger'><i class='fa fa-times'></i></a>
																			</td>
																		</tr>";
																	$no++;
																}
															}
															else
															{
																echo "</tr>
																		<td colspan='6' align='center'><font color='red'><b>No data available</b></font></td>
																	</tr>";
															}
														?>
													</tbody>
												</table>
											</div>
										</center>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Student Identification Number (SIN)</label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control">
											<input type="hidden" name="foto_lama" value="<?php echo $foto_lama; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Certificate ID</label>
											<input type="text" name="certificate_id" value="<?php echo $certificate_id; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Identity File</label>
											<br/>
											<a href="<?php echo base_url("students/ptl_download_identity/$file_identitas/$MhswID"); ?>"><?php echo $file_identitas; ?></a></td>
											<p class="help-block"><font color="green">This file can only be uploaded in LMS.</font></p>
										</div>
										<div class="form-group">
											<label>Parent Identity File</label>
											<br/>
											<a href="<?php echo base_url("students/ptl_download_parents_identity/$file_identitas_ortu/$MhswID"); ?>"><?php echo $file_identitas_ortu; ?></a></td>
											<p class="help-block"><font color="green">This file can only be uploaded in LMS.</font></p>
										</div>
										<div class="form-group">
											<label>Last Diploma File</label>
											<br/>
											<a href="<?php echo base_url("students/ptl_download_last_diploma/$file_ijazah/$MhswID"); ?>"><?php echo $file_ijazah; ?></a></td>
											<p class="help-block"><font color="green">This file can only be uploaded in LMS.</font></p>
										</div>
										<div class="form-group">
											<label>Marketing</label>
											<select name="PresenterID" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($marketing)
													{
														foreach($marketing as $row)
														{
															$word = explode(" ",$row->nama);
															echo "<option value='".$word[0]."'";
															if($PresenterID == $word[0])
															{
																echo "selected";
															}
															echo ">".$word[0]." - $row->nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Place of Birth</label>
											<input type="text" name="TempatLahir" value="<?php echo $TempatLahir; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of Birth</label>
											<input type="text" name="TanggalLahir" value="<?php echo $TanggalLahir; ?>" id="datepicker1" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Gender</label>
											<select name="Kelamin" class="form-control" required>
												<option value="P" <?php if($Kelamin == "P"){ echo "selected"; } ?>>MALE</option>
												<option value="W" <?php if($Kelamin == "W"){ echo "selected"; } ?>>FEMALE</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Citizen</label>
											<select name="WargaNegara" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<option value="WNI" <?php if($WargaNegara == "WNI") { echo "selected"; } ?>>WNI</option>
												<option value="WNA" <?php if($WargaNegara == "WNA") { echo "selected"; } ?>>WNA</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Nationality</label>
											<input type="text" name="Kebangsaan" value="<?php echo $Kebangsaan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Religion</label>
											<select name="Agama" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowagama)
													{
														foreach($rowagama as $row)
														{
															echo "<option value='$row->Agama'";
															if($Agama == $row->Agama)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Civil Status</label>
											<select name="StatusSipil" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowsipil)
													{
														foreach($rowsipil as $row)
														{
															echo "<option value='$row->StatusSipil'";
															if($StatusSipil == $row->StatusSipil)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Last Education</label>
											<select name="PendidikanTerakhir" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowpendidikan)
													{
														foreach($rowpendidikan as $row)
														{
															echo "<option value='$row->Pendidikan'";
															if($PendidikanTerakhir == $row->Pendidikan)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Graduated Year</label>
											<input type="text" name="TahunLulus" value="<?php echo $TahunLulus; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Body Height</label>
											<input type="text" name="TinggiBadan" value="<?php echo $TinggiBadan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Body Weight</label>
											<input type="text" name="BeratBadan" value="<?php echo $BeratBadan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Child</label>
											<input type="text" name="AnakKe" value="<?php echo $AnakKe; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Number of Siblings</label>
											<input type="text" name="JumlahSaudara" value="<?php echo $JumlahSaudara; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Current Address</label>
											<textarea name="Alamat" class="form-control"><?php echo $Alamat; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>RT</label>
											<input type="text" name="RT" value="<?php echo $RT; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>RW</label>
											<input type="text" name="RW" value="<?php echo $RW; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Current City</label>
											<input type="text" name="Kota" value="<?php echo $Kota; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Current Province</label>
											<input type="text" name="Propinsi" value="<?php echo $Propinsi; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Current Postal Code</label>
											<input type="text" name="KodePos" value="<?php echo $KodePos; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Current Country</label>
											<input type="text" name="Negara" value="<?php echo $Negara; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input type="number" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email 1</label>
											<input type="mail" name="Email" value="<?php echo $Email; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email 2</label>
											<input type="mail" name="Email2" value="<?php echo $Email2; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Home Address</label>
											<textarea name="AlamatAsal" class="form-control"><?php echo $AlamatAsal; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Home RT</label>
											<input type="text" name="RTAsal" value="<?php echo $RTAsal; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Home RW</label>
											<input type="text" name="RWAsal" value="<?php echo $RWAsal; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Home City</label>
											<input type="text" name="KotaAsal" value="<?php echo $KotaAsal; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Home Postal Code</label>
											<input type="number" name="KodePosAsal" value="<?php echo $KodePosAsal; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<h4><font color="green"><b>Father's Data</b></font></h4>
										<div class="form-group">
											<label>Father Name</label>
											<input type="text" name="NamaAyah" value="<?php echo $NamaAyah; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Religion</label>
											<select name="AgamaAyah" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowagama)
													{
														foreach($rowagama as $row)
														{
															echo "<option value='$row->Agama'";
															if($AgamaAyah == $row->Agama)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Last Education</label>
											<select name="PendidikanAyah" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowpendidikan)
													{
														foreach($rowpendidikan as $row)
														{
															echo "<option value='$row->Pendidikan'";
															if($PendidikanAyah == $row->Pendidikan)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Profession</label>
											<select name="PekerjaanAyah" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowpekerjaan)
													{
														foreach($rowpekerjaan as $row)
														{
															echo "<option value='$row->Pekerjaan'";
															if($PekerjaanAyah == $row->Pekerjaan)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="AlamatOrtu" class="form-control"><?php echo $AlamatOrtu; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="KotaOrtu" value="<?php echo $KotaOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postal Code</label>
											<input type="number" name="KodePosOrtu" value="<?php echo $KodePosOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="TeleponOrtu" value="<?php echo $TeleponOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input type="number" name="HandphoneOrtu" value="<?php echo $HandphoneOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="mail" name="EmailOrtu" value="<?php echo $EmailOrtu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<h4><font color="green"><b>Mother's Data</b></font></h4>
										<div class="form-group">
											<label>Mother Name</label>
											<input type="text" name="NamaIbu" value="<?php echo $NamaIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Religion</label>
											<select name="AgamaIbu" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowagama)
													{
														foreach($rowagama as $row)
														{
															echo "<option value='$row->Agama'";
															if($AgamaIbu == $row->Agama)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Last Education</label>
											<select name="PendidikanIbu" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowpendidikan)
													{
														foreach($rowpendidikan as $row)
														{
															echo "<option value='$row->Pendidikan'";
															if($PendidikanIbu == $row->Pendidikan)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Profession</label>
											<select name="PekerjaanIbu" class="form-control">
												<option value="">-- CHOOSE -- </option>
												<?php
													if($rowpekerjaan)
													{
														foreach($rowpekerjaan as $row)
														{
															echo "<option value='$row->Pekerjaan'";
															if($PekerjaanIbu == $row->Pekerjaan)
															{
																echo "selected";
															}
															echo ">".strtoupper($row->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="TeleponIbu" value="<?php echo $TeleponIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Mobile Phone</label>
											<input type="number" name="HandphoneIbu" value="<?php echo $HandphoneIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="mail" name="EmailIbu" value="<?php echo $EmailIbu; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<h4><font color="green"><b>Status</b></font></h4>
										<div class="form-group">
											<label>Status</label>
											<select name="StatusMhswID" class="form-control">
												<?php
													if($rowstatus)
													{
														foreach($rowstatus as $rs)
														{
															echo "<option value='$rs->StatusMhswID'";
															if($rs->StatusMhswID == $StatusMhswID)
															{
																echo "selected";
															}
															echo ">$rs->StatusMhswID - ".strtoupper($rs->Nama)."</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Graduation Date</label>
											<input type="text" name="tanggal_lulus" value="<?php echo $tanggal_lulus; ?>" id="datepicker2" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
											<?php
												if($this->session->userdata('students_set_tanggal_lulus') != "")
												{
													if($cektanggal_lulus == "YES")
													{
														echo "<font color='red'><b>Needs to be saved (if status is graduated)</b></font>";
													}
												}
											?>
										</div>
									</div>
									<center>
										<a href="<?php echo site_url("students"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<button type="submit" class="btn btn-primary">Update</button>
										<br/>
										<br/>
										Profile Verification <?php echo $verifikasi; ?>
										<br/>
										Verification Code <?php echo $verifikasi_kode; ?>
										<br/>
										<br/>
										<a href="<?php echo site_url("students/ptl_not_complete/$MhswID"); ?>" class="btn btn-success">Warning to complete the data</a>
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>