		<style>
			div.paging {
				padding		: 2px;
				margin		: 2px;
				text-align	: center;
				font-family	: Tahoma;
				font-size	: 16px;
				font-weight	: bold;
			}
			div.paging a {
				padding				: 2px 6px;
				margin-right		: 2px;
				border				: 1px solid #DEDFDE;
				text-decoration		: none;
				color				: #dc0203;
				background-position	: bottom;
			}
			div.paging a:hover {
				background-color: #0063dc;
				border : 1px solid #fff;
				color  : #fff;
			}
			div.paging span.current {
				border : 1px solid #DEDFDE;
				padding		 : 2px 6px;
				margin-right : 2px;
				font-weight  : bold;
				color        : #FF0084;
			}
			div.paging span.disabled {
				padding      : 2px 6px;
				margin-right : 2px;
				color        : #ADAAAD;
				font-weight  : bold;
			}
			div.paging span.prevnext {    
			  font-weight : bold;
			}
			div.paging span.prevnext a {
				 border : none;
			}
			div.paging span.prevnext a:hover {
				display: block;
				border : 1px solid #fff;
				color  : #fff;
			}
		</style>
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Students</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students"); ?>">Students</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>List of Students.
								</div>
								<table class="table table-bordered table-striped">
									<tr class="info">
										<td>
											<form action="<?php echo site_url("students/ptl_filter_tahun_masuk"); ?>" method="POST">
												<select name="cektahunmasuk" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- YEAR --</option>
													<?php
														$cektahunmasuk = $this->session->userdata('students_filter_tahun_masuk');
														if($rowtahun)
														{
															foreach($rowtahun as $rt)
															{
																echo "<option value='$rt->tahun'";
																if($cektahunmasuk == $rt->tahun)
																{
																	echo "selected";
																}
																echo ">$rt->tahun</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('students_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students/ptl_filter_prodi"); ?>" method="POST">
												<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi = $this->session->userdata('students_filter_prodi');
														if($cekjurusan == "INT")
														{
															if($programd1)
															{
																foreach($programd1 as $d1)
																{
																	echo "<option value='$d1->ProdiID'";
																	if($cekprodi == $d1->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "REG")
														{
															if($programd3)
															{
																foreach($programd3 as $d3)
																{
																	echo "<option value='$d3->ProdiID'";
																	if($cekprodi == strtoupper($d3->ProdiID))
																	{
																		echo "selected";
																	}
																	echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "SC")
														{
															if($shortcourse)
															{
																foreach($shortcourse as $sc)
																{
																	echo "<option value='$sc->KursusSingkatID'";
																	if($cekprodi == $sc->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students/ptl_filter_no"); ?>" method="POST">
												<select name="cekno" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>10 data</option>
													<?php
														$cekno = $this->session->userdata('students_filter_no');
														echo "<option value='25'"; if($cekno == '25'){ echo "selected"; } echo ">25 data</option>";
														echo "<option value='50'"; if($cekno == '50'){ echo "selected"; } echo ">50 data</option>";
														echo "<option value='100'"; if($cekno == '100'){ echo "selected"; } echo ">100 data</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr class="info">
										<td>
											<form action="<?php echo site_url("students/ptl_filter_marketing"); ?>" method="POST">
												<select name="cekmarketing" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ONLINE REGISTRATION --</option>
													<?php
														$cekmarketing = $this->session->userdata('students_filter_marketing');
														if($marketing)
														{
															foreach($marketing as $row)
															{
																if($row->na == "N")
																{
																	$word = explode(" ",$row->nama);
																	echo "<option value='".$word[0]."'";
																	if($cekmarketing == $word[0])
																	{
																		echo "selected";
																	}
																	echo ">".$word[0]." - $row->nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students/ptl_filter_status"); ?>" method="POST">
												<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- STATUS --</option>
													<?php
														$cekstatus = $this->session->userdata('students_filter_status');
														if($status)
														{
															foreach($status as $row)
															{
																echo "<option value='$row->StatusMhswID'";
																if($cekstatus == $row->StatusMhswID)
																{
																	echo "selected";
																}
																echo ">$row->StatusMhswID - $row->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students/search"); ?>" method="POST">
												<input type="text" name="cari" value="<?php echo $pencarian; ?>" class="form-control" placeholder="Search.....">
										</td>
										<td>
												<button type="submit" class="btn btn-primary">Search</button>
											</form>
											<?php
												if($pencarian != "")
												{
													echo "<a href='".site_url("students")."' class='btn btn-warning'>Clear</a>";
												}
											?>
										</td>
									</tr>
								</table>
								<?php
									if($cektahunmasuk != "")
									{
										echo "<center>$totverif data is verified, $totverifnot data is not verified, $totverifsent data waiting for confirmation from $totverifall data.</center>";
									}
								?>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Photo</th>
											<th>Student ID & Name</th>
											<th>Program</th>
											<th>Status<br/>Certificate ID</th>
											<th>Phone<br/>Mobile</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($cek)
											{
												$no = 1;
												foreach($cariproduk->result() as $row)
												{
													$foto = "foto_umum/user.jpg";
													$foto_preview = "foto_umum/user.jpg";
													if($row->Foto != "")
													{
														$foto = "foto_mahasiswa/".$row->Foto;
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
														}
													}
													$StatusMhswID = $row->StatusMhswID;
													$st = $this->m_status->PTL_select($StatusMhswID);
													$StatusMhsw = "";
													if($st)
													{
														$StatusMhsw = $st["Nama"];
													}
													$CID = "<font color='red'>No CID</font>";
													if($row->certificate_id != "")
													{
														$CID = $row->certificate_id;
													}
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td>$no</td>
															<td>
																<a class='fancybox' title='$row->MhswID - $row->Nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																	<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																</a>
															</td>
															<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/>$row->Nama</td>
															<td>$row->ProgramID<br/>$row->ProdiID</td>
															<td>$StatusMhsw<br/>$CID</td>
															<td>$row->Telepon<br/>$row->Handphone</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("students/ptl_edit/$row->MhswID")."' title='Edit'>
																	<i class='fa fa-list'></i>
																</a>
																<a class='btn btn-success' href='".site_url("students/ptl_certificate/$row->MhswID/ESMOD%20JAKARTA")."' target='_blank' title='Download Certificate'>
																	<i class='fa fa-certificate'></i>
																</a>";
										?>
																<a class="btn btn-danger" href="<?php echo site_url("students/ptl_confirm_delete/$row->MhswID"); ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->MhswID." - ".$row->Nama; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
																	<i class="fa fa-times"></i>
																</a>
										<?php
														echo "</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
								<?php
									echo $pagination;
									echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>