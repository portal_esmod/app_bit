		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Note</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students"); ?>">Students</a>
							>>
							<a href="<?php echo site_url("students/ptl_edit/$MhswID"); ?>">Detail</a>
							>>
							<a href="<?php echo site_url("students/ptl_note_form/$MhswID"); ?>">Add Note</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("students/ptl_note_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>SIN</label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="title" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Notes</label>
											<textarea name="comment" class="form-control"></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Meeting Date</label>
											<input type="text" name="datemeet" id="datepicker1" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("students/ptl_edit/$MhswID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>