		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Subject</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("drop"); ?>">Drop</a>
							>>
							<a href="<?php echo site_url("drop/ptl_subject_form"); ?>">Add Subject (SSMAN06)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">							
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This subject will automatically enroll.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Student ID</td>
										<td>:</td>
										<td><?php echo $MhswID; ?></td>
										<td>Program Study</td>
										<td>:</td>
										<td><?php echo $ProdiID." - ".$namaProdiID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("drop/ptl_subject_insert"); ?>" method="POST">
										<div class="col-lg-12">
											<div class="form-group">
												<label>Subjects</label>
												<input type="text" name="SubjekID" id="SubjekDrop" class="form-control" required>
												<input type="hidden" name="KHSID" value="<?php echo $KHSID; ?>">
												<input type="hidden" name="TahunID" value="<?php echo $TahunID; ?>">
												<input type="hidden" name="MhswID" value="<?php echo $MhswID; ?>">
												<p class="help-block"></p>
												<span id="hasil-cari"></span>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("drop"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>