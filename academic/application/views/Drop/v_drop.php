			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Subject Management</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("drop"); ?>">Student Subject Management (SSMAN02)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>This page is used if the student wants to remove or add the subject. <a href="<?php echo site_url("tutorial/ptl_detail/SSMJ001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Students Subject Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<form role="form" id="student" action="<?php echo site_url("drop/ptl_cari"); ?>" method="POST">
											<td colspan="4">
												<input type="text" name="cari" value="<?php echo $mhs; ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
										</form>
									</tr>
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													$foto = "foto_umum/user.jpg";
													$foto_preview = "foto_umum/user.jpg";
													if($r["Foto"] != "")
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="10">
													<p align="center">
														<a class="fancybox" title="<?php echo "$MhswID - $r[Nama]"; ?>" href="<?php echo base_url("ptl_storage/$foto"); ?>" data-fancybox-group="gallery">
															<img class="img-polaroid" src="<?php echo base_url("ptl_storage/$foto_preview"); ?>" width="300px" alt="" />
														</a>
													</p>
												</td>
												<td>Name</td>
												<td>:</td>
												<td colspan="2"><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td colspan="2">
													<?php
														$tgl = "";
														if($r["TanggalLahir"] != "NULL")
														{
															$tgl = tgl_singkat_eng($r["TanggalLahir"]);
														}
														echo $r["TempatLahir"].", ".$tgl;
													?>
												</td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td colspan="2">
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td colspan="2">
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>KHS ID</td>
												<td>:</td>
												<td colspan="2">
													<?php
														$cekkhs = $this->session->userdata('drop_filter_khs');
														if($cekkhs == "")
														{
															$TahunID = $tahun;
															$res = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
															if($res)
															{
																echo $res["KHSID"];
															}
															$font = "";
														}
														else
														{
															$font = "style='background-color: #FFCD41;'";
															echo " [".$khs2."]";
														}
													?>
												</td>
											</tr>
											<tr>
												<td>Year ID</td>
												<td>:</td>
												<td colspan="2">
													<form action="<?php echo site_url("drop/ptl_filter_khs"); ?>" method="POST">
														<select name="cekkhs" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- LAST KHS --</option>
															<?php
																$thn = $tahun;
																if($rowkhs)
																{
																	foreach($rowkhs as $rk)
																	{
																		$f = "";
																		$cur = "";
																		if($cekkhs != "")
																		{
																			if($tahun == $rk->TahunID)
																			{
																				$f = "style='background-color: #5BB734;'";
																				$cur = " - Last KHS";
																			}
																		}
																		echo "<option value='$rk->TahunID' $f";
																		if($cekkhs == "")
																		{
																			if($tahun == $rk->TahunID)
																			{
																				echo "selected";
																				$thn = $rk->TahunID;
																				$this->session->set_userdata('drop_filter_khs',$thn);
																			}
																		}
																		else
																		{
																			if($cekkhs == $rk->TahunID)
																			{
																				echo "selected";
																				$thn = $rk->TahunID;
																				$this->session->set_userdata('drop_filter_khs',$thn);
																			}
																		}
																		$TahunID = $rk->TahunID;
																		$t = $this->m_year->PTL_select($TahunID);
																		echo ">SEMESTER $rk->Sesi [$rk->TahunID - $t[Nama]$cur]</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td colspan="2">
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Class</td>
												<td>:</td>
												<td colspan="2"><?php echo $kelas; ?></td>
											</tr>
											<tr>
												<td>Specialization</td>
												<td>:</td>
												<td colspan="2"><?php echo $Spesialisasi; ?></td>
											</tr>
											<tr>
												<td>Language</td>
												<td>:</td>
												<td colspan="2"><?php echo $languageID; ?></td>
											</tr>
									<?php
										}
									?>
								</table>
								<?php
									if($r)
									{
								?>
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<td>
													<form action="<?php echo site_url("drop/ptl_change_program"); ?>" method="POST">
														<input type="hidden" name="MhswID" value="<?php echo $MhswID; ?>">
														<select name="prodi" title="Filter by Period" class="form-control round-form" required>
															<option value=''>-- CHANGE TO --</option>
															<?php
																if($rowprodi1)
																{
																	foreach($rowprodi1 as $r1)
																	{
																		if($ProdiID != $r1->ProdiID)
																		{
																			echo "<option value='REG_$r1->ProdiID'>REG - $r1->ProdiID - $r1->Nama</option>";
																		}
																	}
																}
																if($rowprodi2)
																{
																	foreach($rowprodi2 as $r2)
																	{
																		if($ProdiID != $r2->ProdiID)
																		{
																			echo "<option value='INT_$r2->ProdiID'>INT - $r2->ProdiID - $r2->Nama</option>";
																		}
																	}
																}
																// if($rowprodi3)
																// {
																	// foreach($rowprodi3 as $r3)
																	// {
																		// echo "<option value='SC_$r3->KursusSingkatID'>SC - $r3->KursusSingkatID - $r3->Nama</option>";
																	// }
																// }
															?>
														</select>
												</td>
												<td>
													&nbsp;&nbsp;:&nbsp;&nbsp;
												</td>
												<td>
														<input type="submit" value="Change Program Study" class="btn btn-success" onclick="return confirm('Are you sure want to CHANGE PRODI this student named <?php echo strtoupper($r["Nama"]); ?>?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
													</form>
												</td>
											</tr>
										</table>
										<br/>
										<center><a href="<?php echo site_url("drop/ptl_subject_form/$MhswID/$khs2/$thn"); ?>" class="btn btn-info">Add Subject</a></center>
										<br/>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>ID</th>
													<th>SUBJECT TAKEN</th>
													<th>CREDIT</th>
													<th>EXCUSE</th>
													<th>SICK</th>
													<th>ABSENT</th>
													<th>LATE</th>
													<th>GRADE</th>
													<th>GRADE VALUE</th>
													<th>CREATED BY</th>
													<th>EDITED BY</th>
													<th>ACTIONS</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<?php
														if($rowrecord)
														{
															$no = 1;
															$totsks = 0;
															$totexc = 0;
															$totsic = 0;
															$totabs = 0;
															$totlat = 0;
															$jumlahexc = 0;
															$jumlahsic = 0;
															$jumlahabs = 0;
															$jumlahlat = 0;
															foreach($rowrecord as $row)
															{
																$KRSID = $row->KRSID;
																$JadwalID = $row->JadwalID;
																$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																$totexc = $texc;
																$jumlahexc = $jumlahexc + $totexc;
																if($totexc == 0){ $totexc = ""; }
																$totsic = $tsic;
																$jumlahsic = $jumlahsic + $totsic;
																if($totsic == 0){ $totsic = ""; }
																$totabs = $tabs;
																$jumlahabs = $jumlahabs + $totabs;
																if($totabs == 0){ $totabs = ""; }
																$totlat = $tlat;
																$jumlahlat = $jumlahlat + $totlat;
																if($totlat == 0){ $totlat = ""; }
																
																$SubjekID = $row->SubjekID;
																$s = $this->m_subjek->PTL_select($SubjekID);
																$subjek = "";
																$SKS = "";
																if($s)
																{
																	$subjek = $s["Nama"];
																	$SKS = $s["SKS"];
																	$totsks = $totsks + $s["SKS"];
																}
																if($row->NA == "Y")
																{
																	echo "<tr class='danger'>";
																}
																else
																{
																	echo "<tr class='info'>";
																}
																	$wordA1 = explode("_",@$row->login_buat);
																	$wordA2 = explode(" ",@$wordA1[1]);
																	$lb = @$wordA2[0];
																	$wordB1 = explode("_",@$row->login_edit);
																	$wordB2 = explode(" ",@$wordB1[1]);
																	$le = @$wordB2[0];
																	echo "<td>$no</td>
																		<td title='SubjekID : $SubjekID ~ JadwalID : $JadwalID'>$row->KRSID</td>
																		<td title='Go To Attendance'>
																			<a href='".site_url("attendance/ptl_list/$JadwalID")."' target='_blank'>
																				$subjek
																			</a>
																		</td>
																		<td align='right'>$SKS</td>
																		<td align='right'>$totexc</td>
																		<td align='right'>$totsic</td>
																		<td align='right'>$totabs</td>
																		<td align='right'>$totlat</td>
																		<td align='center'>$row->GradeNilai</td>
																		<td align='center'>$row->NilaiAkhir</td>
																		<td title='Created Date : $row->tanggal_buat'>$lb</td>
																		<td title='Edited Date : $row->tanggal_edit'>$le</td>
																		<td>";
																		if($row->NA == "Y")
																		{
													?>
																			<a class="btn btn-success" href="<?php echo site_url("drop/ptl_enabled/$row->KRSID");?>" onclick="return confirm('Are you sure to active of this data <?php echo $subjek; ?>?')" title="Enable this subject">
																				<i class="fa fa-check"></i>
																			</a>
																			<a class="btn btn-danger" href="<?php echo site_url("drop/ptl_delete/$row->KRSID");?>" onclick="return confirm('Are you sure to DELETE PERMANENTLY of this data <?php echo $subjek; ?>?')" title="Delete Permanently">
																				<i class="fa fa-times"></i>
																			</a>
													<?php
																		}
																		else
																		{
													?>
																			<a class="btn btn-warning" href="<?php echo site_url("drop/ptl_disabled/$row->KRSID"); ?>" onclick="return confirm('Are you sure to delete of this data <?php echo $subjek; ?>?')" title="Drop this subject">
																				<i class="fa fa-times"></i>
																			</a>
													<?php
																		}
																	echo "</td>
																	</tr>";
																$no++;
															}
														}
														else
														{
															echo "<tr>
																	<td colspan='13'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																</tr>";
														}
													?>
												</tr>
											</tbody>
										</table>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>