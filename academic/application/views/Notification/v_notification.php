		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Notification</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("notification"); ?>">Notification</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of notification.
								</div>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>From</th>
											<th>Message</th>
											<th>Created Date</th>
											<th>Last Seen</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->tanggal_baca == "")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td>$no</td>
															<td>$row->nama_buat</td>
															<td>$row->title</td>
															<td>$row->tanggal_buat</td>
															<td>$row->tanggal_baca</td>
															<td><a class='btn btn-info' href='".site_url("notification/ptl_detail/$row->id_notifikasi/$row->noteid")."'>READ</a></td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>