		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <a href="<?php echo site_url("notification"); ?>">Notification</a>
							>>
							<a href="<?php echo site_url("notification/ptl_detail/$id_notifikasi/$noteid"); ?>">Detail</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="#" method="POST">
									<div class="col-lg-6">
										<?php
											if($MhswID != "")
											{
										?>
												<div class="form-group">
													<label>SIN</label>
													<input readonly type="text" value="<?php echo $MhswID; ?>" class="form-control">
													<p class="help-block"></p>
												</div>
										<?php
											}
											if($Nama != "")
											{
										?>
												<div class="form-group">
													<label>Name</label>
													<input readonly type="text" value="<?php echo $Nama; ?>" class="form-control">
													<p class="help-block"></p>
												</div>
										<?php
											}
										?>
										<div class="form-group">
											<label>Title</label>
											<input readonly type="text" value="<?php echo $title; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Notes</label>
											<textarea readonly class="form-control"><?php echo $comment; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>From</label>
											<input readonly type="text" value="<?php echo $nama_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<?php
											if($NamaSubjek != "")
											{
										?>
												<div class="form-group">
													<label>Subject</label>
													<input readonly type="text" value="<?php echo $NamaSubjek; ?>" class="form-control">
													<p class="help-block"></p>
												</div>
										<?php
											}
										?>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("notification"); ?>" class="btn btn-warning">Back</a>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>