		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Project Score</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project_score"); ?>">Project Score</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<form action="<?php echo site_url("project_score/ptl_filter_kur"); ?>" method="POST">
									<select name="cekkurikulum" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- CURRICULUM --</option>
										<?php
											$cekkurikulum = $this->session->userdata('ps_filter_kur');
											if($rowkurikulum)
											{
												foreach($rowkurikulum as $rk)
												{
													// if($rk->NA == "N")
													// {
														echo "<option value='$rk->KurikulumID'";
														if($cekkurikulum == $rk->KurikulumID)
														{
															echo "selected";
														}
														echo ">$rk->KurikulumKode - $rk->Nama</option>";
													// }
												}
											}
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<?php
									if($cekkurikulum != "")
									{
								?>
										<br/>
										<center>
											<a href="<?php echo site_url("project_score/ptl_form"); ?>" class="btn btn-primary">Add New</a>
											<a href="<?php echo site_url("project_score/ptl_copy"); ?>" class="btn btn-warning"><i class="fa fa-copy"> Copy</i></a>
										</center>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th>#</th>
													<th>Grade</th>
													<th>Minimal Score</th>
													<th>Pass?</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr>";
															}
																echo "<td title='Nilai2ID : $row->Nilai2ID'>$no</td>
																	<td align='center'>$row->Nama</td>
																	<td align='right'>$row->NilaiMin</td>
																	<td align='center'><img src='".base_url("assets/dashboard/img/$row->Lulus.gif")."'></td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("project_score/ptl_edit/$row->Nilai2ID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																	</td>
																</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>