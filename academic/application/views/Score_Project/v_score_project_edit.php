		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Project Score</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project_score"); ?>">Project Score</a>
							>>
							<a href="<?php echo site_url("project_score/ptl_edit/$Nilai2ID"); ?>">Edit Project Score</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("project_score/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Grade</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required>
											<input type="hidden" name="Nilai2ID" value="<?php echo $Nilai2ID; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Minimal Score</label>
											<input type="number" name="NilaiMin" value="<?php echo $NilaiMin; ?>" step="0.01" min="0" max="100" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Pass?</label>
											<input type="checkbox" name="Lulus" value="Y" <?php if($Lulus == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("subject_score"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>