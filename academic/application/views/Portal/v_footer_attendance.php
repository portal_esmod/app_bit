		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-1.10.2.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/scripts/siminta.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/morris/morris.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/scripts/dashboard-demo.js"></script>
		<script type="text/javascript">
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
			
			function tandaJam(b)
			{
				var _minus = false;
				if (b<0) _minus = true;
				b = b.toString();
				b=b.replace(".","");
				b=b.replace("-","");
				c = "";
				panjang = b.length;
				j = 0;
				if(panjang < 5)
				{
					for (i = panjang; i > 0; i--)
					{
						j = j + 1;
						if (((j % 2) == 1) && (j != 1))
						{
							c = b.substr(i-1,1) + ":" + c;
						}
						else
						{
							c = b.substr(i-1,1) + c;
						}
					}
				}
				if (_minus) c = ":" + c ;
				return c;
			}
			
			function angkaSaja(ini, e)
			{
				if (e.keyCode>=49)
				{
					if(e.keyCode<=57)
					{
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
						ini.value = tandaJam(b);
						return false;
					}
					else
					{
						if(e.keyCode<=105)
						{
							if(e.keyCode>=96)
							{
								a = ini.value.toString().replace(".","");
								b = a.replace(/[^\d]/g,"");
								b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
								ini.value = tandaJam(b);
								return false;
							}
							else {return false;}
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					if (e.keyCode==48)
					{
						a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
						b = a.replace(/[^\d]/g,"");
						if (parseFloat(b)!=0)
						{
							ini.value = tandaJam(b);
							return false;
						}
						else
						{
							return false;
						}
					}
					else
					{
						if (e.keyCode==95)
						{
							a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
							b = a.replace(/[^\d]/g,"");
							if (parseFloat(b)!=0)
							{
								ini.value = tandaJam(b);
								return false;
							}
							else 
							{
								return false;
							}
						}
						else
						{
							if (e.keyCode==8 || e.keycode==46)
							{
								a = ini.value.replace(".","");
								b = a.replace(/[^\d]/g,"");
								b = b.substr(0,b.length -1);
								if (tandaJam(b)!="")
								{
									ini.value = tandaJam(b);
								}
								else
								{
									ini.value = "";
								}
								return false;
							}
							else
							{
								if (e.keyCode==9)
								{
									return true;
								}
								else
								{
									if (e.keyCode==17)
									{
										return true;
									}
									else
									{
										return false;
									}
								}
							}
						}
					}
				}
			}
		</script>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
		<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
		<?php $url = base_url()."assets/auto/loading.gif"; ?>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
		<style>
			/* Autocomplete
			----------------------------------*/
			.ui-autocomplete { position: absolute; cursor: default; }
			.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

			/* workarounds */
			* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

			/* Menu
			----------------------------------*/
			.ui-menu {
				list-style:none;
				padding: 2px;
				margin: 0;
				display:block;
			}
			.ui-menu .ui-menu {
				margin-top: -3px;
			}
			.ui-menu .ui-menu-item {
				margin:0;
				padding: 0;
				zoom: 1;
				float: left;
				clear: left;
				width: 100%;
				font-size:80%;
			}
			.ui-menu .ui-menu-item a {
				text-decoration:none;
				display:block;
				padding:.2em .4em;
				line-height:1.5;
				zoom:1;
			}
			.ui-menu .ui-menu-item a.ui-state-hover,
			.ui-menu .ui-menu-item a.ui-state-active {
				font-weight: normal;
				margin: -1px;
			}
		</style>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/umur/jquery-ui.css"/>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/zebra_datepicker.js"></script>
		<script type="text/javascript">
			$('#datepicker1').Zebra_DatePicker();
			$('#datepicker2').Zebra_DatePicker();
			$('#datepicker3').Zebra_DatePicker();
			$('#datepicker4').Zebra_DatePicker();
			$('#datepicker5').Zebra_DatePicker();
			$('#datepicker6').Zebra_DatePicker();
			$('#datepicker7').Zebra_DatePicker();
			$('#datepicker8').Zebra_DatePicker();
			$('#datepicker9').Zebra_DatePicker();
			$('#datepicker10').Zebra_DatePicker();
			$('#datepicker11').Zebra_DatePicker();
			$('#datepicker12').Zebra_DatePicker();
			$('#datepicker13').Zebra_DatePicker();
			$('#datepicker14').Zebra_DatePicker();
			$('#datepicker15').Zebra_DatePicker();
			$('#datepicker16').Zebra_DatePicker();
			$('#datepicker17').Zebra_DatePicker();
			$('#datepicker18').Zebra_DatePicker();
			$('#datepicker19').Zebra_DatePicker();
			$('#datepicker20').Zebra_DatePicker();
			$('#datepicker21').Zebra_DatePicker();
			$('#datepicker22').Zebra_DatePicker();
			$('#datepicker23').Zebra_DatePicker();
			$('#datepicker24').Zebra_DatePicker();
			// Special Condition
			<?php
				$CI =& get_instance();
				$CI->load->helper('tanggal');
				$CI->load->model('m_kalender');
				$rowkalender = $CI->m_kalender->PTL_all_tahun_ini();
			?>
			$('#datepickerSC1').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				pair: $('#datepickerSC1-End')
			});
			$('#datepickerSC2').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				pair: $('#datepickerSC2-End')
			});
			$('#datepickerSC3').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				pair: $('#datepickerSC3-End')
			});
			$('#datepickerSC4').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				pair: $('#datepickerSC4-End')
			});
			$('#datepickerSC5').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				pair: $('#datepickerSC5-End')
			});
			$('#datepickerSC1-End').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				direction: 1
			});
			$('#datepickerSC2-End').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				direction: 1
			});
			$('#datepickerSC3-End').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				direction: 1
			});
			$('#datepickerSC4-End').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				direction: 1
			});
			$('#datepickerSC5-End').Zebra_DatePicker({
				<?php
					if(@$rowkalender)
					{
				?>
						disabled_dates: [
				<?php
							$tot = count(@$rowkalender);
							$n = 0;
							foreach(@$rowkalender as $rk)
							{
								$n++;
								$word = explode("-",$rk->tanggal_mulai);
								if($tot == $n)
								{
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
				<?php
								}
								else
								{
				?>
									'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
				<?php
									if($rk->tanggal_selesai != "")
									{
										$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
										for($i=1;$i<=$max_date;$i++)
										{
											$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
											$date = date("Y-m-d",$data);
											$word2 = explode("-",$date);
				?>
											'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
				<?php
										}
									}
								}
							}
				?>
						],
				<?php
					}
				?>
				show_week_number: 'Wk',
				direction: 1
			});
			// Custom
			$('#dpSC1').Zebra_DatePicker({
				show_week_number: 'Wk',
				direction: ['<?php echo @$TglMulai; ?>', '<?php echo @$TglSelesai; ?>'],
				format: 'Y-m-d'
			});
			$('#dpSCRange1').Zebra_DatePicker({
				show_week_number: 'Wk',
				format: 'Y-m-d'
			});
			$('#dpSCRange2').Zebra_DatePicker({
				show_week_number: 'Wk',
				format: 'Y-m-d'
			});
			// Schedule
			var $scd = jQuery.noConflict();
			$scd(this).ready( function()
			{
				$scd("#Subjek1").autocomplete(
				{
					minLength: 1,
					source: 
					function(req, add)
					{
						$scd.ajax(
						{
							url: "<?php echo base_url(); ?>index.php/schedule/lookup_subjek_detail",
							dataType: 'json',
							type: 'POST',
							data: req,
							success:
							function(data)
							{
								if(data.response =="true")
								{
									add(data.message);
								}
							},
						});
					},
					select: 
						function(event, ui)
						{
							$scd("#result").append
							(
								"<li>"+ ui.item.value + "</li>"
							);
							$scd("#hasil-cari").append
							(
								"<b>"+ ui.item.prodi + "</b>"
							);
						},
				});
			});
			$scd(this).ready( function()
			{
				$scd("#Dosen1").autocomplete(
				{
					minLength: 1,
					source: 
					function(req, add)
					{
						$scd.ajax(
						{
							url: "<?php echo base_url(); ?>index.php/schedule/lookup_dosen",
							dataType: 'json',
							type: 'POST',
							data: req,
							success:
							function(data)
							{
								if(data.response =="true")
								{
									add(data.message);
								}
							},
						});
					},
					select: 
						function(event, ui)
						{
							$scd("#result").append
							(
								"<li>"+ ui.item.value + "</li>"
							);
						},
				});
			});
			$scd(this).ready( function()
			{
				$scd("#Dosen2").autocomplete(
				{
					minLength: 1,
					source: 
					function(req, add)
					{
						$scd.ajax(
						{
							url: "<?php echo base_url(); ?>index.php/schedule/lookup_dosen",
							dataType: 'json',
							type: 'POST',
							data: req,
							success:
							function(data)
							{
								if(data.response =="true")
								{
									add(data.message);
								}
							},
						});
					},
					select: 
						function(event, ui)
						{
							$scd("#result").append
							(
								"<li>"+ ui.item.value + "</li>"
							);
						},
				});
			});
			$scd(this).ready( function()
			{
				$scd("#Dosen3").autocomplete(
				{
					minLength: 1,
					source: 
					function(req, add)
					{
						$scd.ajax(
						{
							url: "<?php echo base_url(); ?>index.php/exam/lookup_dosen",
							dataType: 'json',
							type: 'POST',
							data: req,
							success:
							function(data)
							{
								if(data.response =="true")
								{
									add(data.message);
								}
							},
						});
					},
					select: 
						function(event, ui)
						{
							$scd("#result").append
							(
								"<li>"+ ui.item.value + "</li>"
							);
						},
				});
			});
		</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/timepicker/js/jquery-1.11.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/timepicker/js/mtimepicker.js"></script>
		<script type="text/javascript">
			var $j = jQuery.noConflict();
			$j(document).ready( function(){
				<?php
					if($this->session->userdata('aksi') == "edit")
					{
				?>
						$j('#timepickeredit1').mTimePicker().mTimePicker();
						$j('#timepickeredit2').mTimePicker().mTimePicker();
				<?php
					}
					else
					{
				?>
						$j('#timepicker1').mTimePicker().mTimePicker('setTime','09:00');
						$j('#timepicker2').mTimePicker().mTimePicker('setTime','11:00');
				<?php
					}
				?>
			});
		</script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>