		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-1.10.2.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/scripts/siminta.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/morris/morris.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/scripts/dashboard-demo.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/prettify/prettify.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/js/jquery-ui-1.9.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/js/fullcalendar.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/calendar/js/custom.js"></script>
		<script type="text/javascript">
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
			
			jQuery(document).ready(function()
			{
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				
				var calendar = jQuery('#calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					buttonText: {
						prev: '&laquo;',
						next: '&raquo;',
						prevYear: '&nbsp;&lt;&lt;&nbsp;',
						nextYear: '&nbsp;&gt;&gt;&nbsp;',
						today: 'today',
						month: 'month',
						week: 'week',
						day: 'day'
					},
					selectable: true,
					selectHelper: true,
					/* select: function(start, end, allDay) {
						var title = prompt('Event Title:');
						if (title) {
							calendar.fullCalendar('renderEvent',
								{
									title: title,
									start: start,
									end: end,
									allDay: allDay
								},
								true // make the event "stick"
							);
						}
						calendar.fullCalendar('unselect');
					},*/
					editable: true,
					events: [
						<?php
							if($rowrecord)
							{
								$no = 0;
								$tot = count($rowrecord);
								foreach($rowrecord as $row)
								{
									$no++;
									$tanggal_mulai = $row->tanggal_mulai;
									$wordA = explode("-",$tanggal_mulai);
									$tanggal_selesai = $row->tanggal_selesai;
									$wordB = explode("-",$tanggal_selesai);
									
									$jam_mulai = $row->jam_mulai;
									$worA = explode(":",$jam_mulai);
									$jam_selesai = $row->jam_selesai;
									$worB = explode(":",$jam_selesai);
									
									$tidak_kuliah = $row->tidak_kuliah;
									if($tot == $no)
									{
						?>
										{
											title: '<?php echo str_replace("'"," ",$row->keterangan); ?>',
						<?php
											if($tanggal_mulai != "")
											{
												if($jam_mulai != "")
												{
						?>
													start: new Date(<?php echo $wordA[0]; ?>, <?php echo $wordA[1]-1; ?>, <?php echo $wordA[2]; ?>, <?php echo intval($worA[0]); ?>, <?php echo intval($worA[1]); ?>),
						<?php
												}
												else
												{
						?>
													start: new Date(<?php echo $wordA[0]; ?>, <?php echo $wordA[1]-1; ?>, <?php echo $wordA[2]; ?>),
						<?php
												}
											}
											if($tanggal_selesai != "")
											{
												if($jam_selesai != "")
												{
						?>
													end: new Date(<?php echo $wordB[0]; ?>, <?php echo $wordB[1]-1; ?>, <?php echo $wordB[2]; ?>, <?php echo intval($worB[0]); ?>, <?php echo intval($worB[1]); ?>),
						<?php
												}
												else
												{
						?>
													end: new Date(<?php echo $wordB[0]; ?>, <?php echo $wordB[1]-1; ?>, <?php echo $wordB[2]; ?>),
						<?php
												}
											}
											if($jam_mulai != "")
											{
												if($tidak_kuliah == "N")
												{
						?>
													allDay: false,
						<?php
												}
											}
						?>
											url: '<?php echo site_url("calendar"); ?>'
										}
						<?php
									}
									else
									{
						?>
										{
											title: '<?php echo str_replace("'"," ",$row->keterangan); ?>',
						<?php
											if($tanggal_mulai != "")
											{
												if($jam_mulai != "")
												{
						?>
													start: new Date(<?php echo $wordA[0]; ?>, <?php echo $wordA[1]-1; ?>, <?php echo $wordA[2]; ?>, <?php echo intval($worA[0]); ?>, <?php echo intval($worA[1]); ?>),
						<?php
												}
												else
												{
						?>
													start: new Date(<?php echo $wordA[0]; ?>, <?php echo $wordA[1]-1; ?>, <?php echo $wordA[2]; ?>),
						<?php
												}
											}
											if($tanggal_selesai != "")
											{
												if($jam_selesai != "")
												{
						?>
													end: new Date(<?php echo $wordB[0]; ?>, <?php echo $wordB[1]-1; ?>, <?php echo $wordB[2]; ?>, <?php echo intval($worB[0]); ?>, <?php echo intval($worB[1]); ?>),
						<?php
												}
												else
												{
						?>
													end: new Date(<?php echo $wordB[0]; ?>, <?php echo $wordB[1]-1; ?>, <?php echo $wordB[2]; ?>),
						<?php
												}
											}
											if($jam_mulai != "")
											{
												if($tidak_kuliah == "N")
												{
						?>
													allDay: false,
						<?php
												}
											}
						?>
											url: '<?php echo site_url("calendar"); ?>'
										},
						<?php
									}
								}
							}
						?>
						/*{
							title: 'All Day Event',
							start: new Date(y, m, 1)
						},
						{
							title: 'Long Event',
							start: new Date(y, m, 4),
							end: new Date(y, m, d-2)
						},
						{
							title: 'Meeting',
							start: new Date(y, m, d, 14, 30),
							allDay: false
						},
						{
							title: 'Lunch',
							start: new Date(y, m, d, 12, 0),
							end: new Date(y, m, d, 14, 0),
							allDay: false
						},
						{
							title: 'Birthday Party',
							start: new Date(y, m, d+1, 19, 0),
							end: new Date(y, m, d+1, 22, 30),
							allDay: false
						},
						{
							title: 'Click for Google',
							start: new Date(y, m, 28),
							end: new Date(y, m, 29),
							url: 'http://google.com/'
						}*/
					]
				});
				
			});
		</script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>