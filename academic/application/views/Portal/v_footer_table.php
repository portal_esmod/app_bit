		</div>
		<?php
			if($this->session->userdata("sch_Kehadiran") == "")
			{
		?>
				<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-1.10.2.js"></script>
		<?php
			}
		?>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/scripts/siminta.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/dataTables/jquery.dataTables.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/dataTables/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			$('#my_button').on('click', function(){
				$('#my_button').attr("disabled",true);
				$('#my_button').attr("value","Processing...");
				this.form.submit();
            });
			
			$('#my_button2').on('click', function(){
				$('#my_button2').attr("disabled",true);
				$('#my_button2').attr("value","Processing...");
				this.form.submit();
            });
			
			$(document).ready(function()
			{
				$('#checkt-all').change(function()
				{
					$("input:checkbox.kategori-check").prop('checked',this.checked);
					if(this.checked == true)
					{
					   // melakukan suatu tindakan jika checked all aktif
					}
					else
					{
					   // melakukan suatu tindakan jika checked all tidak aktif
					}
				  });
			});
			
			function tandaJam(b)
			{
				var _minus = false;
				if (b<0) _minus = true;
				b = b.toString();
				b=b.replace(".","");
				b=b.replace("-","");
				c = "";
				panjang = b.length;
				j = 0;
				if(panjang < 5)
				{
					for (i = panjang; i > 0; i--)
					{
						j = j + 1;
						if (((j % 2) == 1) && (j != 1))
						{
							c = b.substr(i-1,1) + ":" + c;
						}
						else
						{
							c = b.substr(i-1,1) + c;
						}
					}
				}
				if (_minus) c = ":" + c ;
				return c;
			}
			
			function angkaSaja(ini, e)
			{
				if (e.keyCode>=49)
				{
					if(e.keyCode<=57)
					{
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
						ini.value = tandaJam(b);
						return false;
					}
					else
					{
						if(e.keyCode<=105)
						{
							if(e.keyCode>=96)
							{
								a = ini.value.toString().replace(".","");
								b = a.replace(/[^\d]/g,"");
								b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
								ini.value = tandaJam(b);
								return false;
							}
							else {return false;}
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					if (e.keyCode==48)
					{
						a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
						b = a.replace(/[^\d]/g,"");
						if (parseFloat(b)!=0)
						{
							ini.value = tandaJam(b);
							return false;
						}
						else
						{
							return false;
						}
					}
					else
					{
						if (e.keyCode==95)
						{
							a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
							b = a.replace(/[^\d]/g,"");
							if (parseFloat(b)!=0)
							{
								ini.value = tandaJam(b);
								return false;
							}
							else 
							{
								return false;
							}
						}
						else
						{
							if (e.keyCode==8 || e.keycode==46)
							{
								a = ini.value.replace(".","");
								b = a.replace(/[^\d]/g,"");
								b = b.substr(0,b.length -1);
								if (tandaJam(b)!="")
								{
									ini.value = tandaJam(b);
								}
								else
								{
									ini.value = "";
								}
								return false;
							}
							else
							{
								if (e.keyCode==9)
								{
									return true;
								}
								else
								{
									if (e.keyCode==17)
									{
										return true;
									}
									else
									{
										return false;
									}
								}
							}
						}
					}
				}
			}
			$(document).ready(function () {
				$('#dataTables-example').dataTable();
				$('#dataTables-example2').dataTable();
			});
		</script>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/jquery-ui.css" type="text/css" media="all" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/auto/ui.theme.css" type="text/	css" media="all" />
		<script src="<?php echo base_url(); ?>assets/auto/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/auto/jquery-ui.min.js" type="text/javascript"></script>
		<?php $url = base_url()."assets/auto/loading.gif"; ?>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/auto/my.js"></script>
		<style>
	    	/* Autocomplete
			----------------------------------*/
			.ui-autocomplete { position: absolute; cursor: default; }
			.ui-autocomplete-loading { background: white url('<?php echo $url; ?>') right center no-repeat; }*/

			/* workarounds */
			* html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */

			/* Menu
			----------------------------------*/
			.ui-menu {
				list-style:none;
				padding: 2px;
				margin: 0;
				display:block;
			}
			.ui-menu .ui-menu {
				margin-top: -3px;
			}
			.ui-menu .ui-menu-item {
				margin:0;
				padding: 0;
				zoom: 1;
				float: left;
				clear: left;
				width: 100%;
				font-size:80%;
			}
			.ui-menu .ui-menu-item a {
				text-decoration:none;
				display:block;
				padding:.2em .4em;
				line-height:1.5;
				zoom:1;
			}
			.ui-menu .ui-menu-item a.ui-state-hover,
			.ui-menu .ui-menu-item a.ui-state-active {
				font-weight: normal;
				margin: -1px;
			}
	    </style>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/zebra_datepicker.js"></script>
		<script type="text/javascript">
			$('#datepicker1').Zebra_DatePicker();
			$('#Date1').Zebra_DatePicker({
				show_week_number: 'Wk',
				format: 'Y-m-d',
				onSelect : function (dateText, inst) {
					$('#formId').submit();
				}
			});
			<?php
				$CI =& get_instance();
				$CI->load->helper('tanggal');
				$CI->load->model('m_kalender');
				$rowkalender = $CI->m_kalender->PTL_all_tahun_ini();
				$rk = $this->session->userdata("sch_Kehadiran");
				if($rk != "")
				{
					$vrk = $rk;
					$TglMulai = $this->session->userdata('sch_TglMulai');
					$TglSelesai = $this->session->userdata('sch_TglSelesai');
					$tm = tgl_singkat_eng($TglMulai);
					$ts = tgl_singkat_eng($TglSelesai);
					$hl = date('D', strtotime($TglMulai));
					$hr = date('D', strtotime($TglSelesai));
				}
				else
				{
					if(@$RencanaKehadiran != "")
					{
						$vrk = $RencanaKehadiran;
						$tm = tgl_singkat_eng($TglMulai);
						$ts = tgl_singkat_eng($TglSelesai);
						$hl = date('D', strtotime($TglMulai));
						$hr = date('D', strtotime($TglSelesai));
					}
					else
					{
						$vrk = "";
					}
				}
				if($vrk != "")
				{
					for($i=1;$i<=$vrk;$i++)
					{
			?>
						$('#dpSC<?php echo $i; ?>').Zebra_DatePicker({
							<?php
								if(@$rowkalender)
								{
							?>
									disabled_dates: [
							<?php
										$tot = count(@$rowkalender);
										$n = 0;
										foreach(@$rowkalender as $rk)
										{
											$n++;
											$word = explode("-",$rk->tanggal_mulai);
											if($tot == $n)
											{
												if($rk->tanggal_selesai != "")
												{
													$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
													for($i2=1;$i2<=$max_date;$i2++)
													{
														$data = mktime(0,0,0,$word[1],$word[2]+$i2,$word[0]);
														$date = date("Y-m-d",$data);
														$word2 = explode("-",$date);
							?>
														'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
							<?php
													}
												}
							?>
												'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>'
							<?php
											}
											else
											{
							?>
												'<?php echo @$word[2]; ?> <?php echo @$word[1]; ?> <?php echo @$word[0]; ?>',
							<?php
												if($rk->tanggal_selesai != "")
												{
													$max_date = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
													for($i3=1;$i3<=$max_date;$i3++)
													{
														$data = mktime(0,0,0,$word[1],$word[2]+$i3,$word[0]);
														$date = date("Y-m-d",$data);
														$word2 = explode("-",$date);
							?>
														'<?php echo @$word2[2]; ?> <?php echo @$word2[1]; ?> <?php echo @$word2[0]; ?>',
							<?php
													}
												}
											}
										}
							?>
									],
							<?php
								}
							?>
							show_week_number: 'Wk',
							direction: ['<?php echo $hl.", ".$tm; ?>', '<?php echo $hr.", ".$ts; ?>'],
							format: 'D, d M Y',
							onSelect : function cek_date()
										{
											var attPresensiID = $("input[name='attPresensiID<?php echo $i; ?>']").val();
											var attTanggal = $("input[name='attTanggal<?php echo $i; ?>']").val();
											var attJamMulai = $("input[name='attJamMulai<?php echo $i; ?>']").val();
											var attJamSelesai = $("input[name='attJamSelesai<?php echo $i; ?>']").val();
											var attRuangID = $("select[name='attRuangID<?php echo $i; ?>']").val();
											var check_result_attTanggal = $('.check_result_attTanggal<?php echo $i; ?>');
											var check_result_attJamMulai = $('.check_result_attJamMulai<?php echo $i; ?>');
											var check_result_attJamSelesai = $('.check_result_attJamSelesai<?php echo $i; ?>');
											var check_result_attRuangID = $('.check_result_attRuangID<?php echo $i; ?>');
											$.post("<?php echo site_url("schedule/ptl_edit_day_attendance_cek_bentrok"); ?>", { attPresensiID: attPresensiID, attTanggal: attTanggal, attJamMulai: attJamMulai, attJamSelesai: attJamSelesai, attRuangID: attRuangID }, function (result)
											{
												if(result == 1)
												{
													check_result_attTanggal.html('');
													check_result_attJamMulai.html('');
													check_result_attJamSelesai.html('');
													check_result_attRuangID.html('');
												}
												else
												{
													check_result_attTanggal.html("<font color='red'><b>Schedule conflict</b></font> <a href='<?php echo site_url("attendance/ptl_list_edit"); ?>/"+result+"' target='_blank'>click here to show detail!</a>");
													check_result_attJamMulai.html('');
													check_result_attJamSelesai.html('');
													check_result_attRuangID.html('');
												}
											});
										}
						});
						$(document).ready(function()
						{
							var jum_karakter_minimal = 2;
							var karakter_minimal_error = "Characters are less than 3";
							var pesan_cek = '';
							var check_result_attTanggal = $('.check_result_attTanggal<?php echo $i; ?>');
							var check_result_attJamMulai = $('.check_result_attJamMulai<?php echo $i; ?>');
							var check_result_attJamSelesai = $('.check_result_attJamSelesai<?php echo $i; ?>');
							var check_result_attRuangID = $('.check_result_attRuangID<?php echo $i; ?>');
							
							var check_JamMulai = $('#check_JamMulai<?php echo $i; ?>');
							check_JamMulai.on('keyup', function()
							{
								if(check_JamMulai.val().length <= jum_karakter_minimal)
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html(karakter_minimal_error);
									check_result_attJamSelesai.html('');
									check_result_attRuangID.html('');
								}
								else
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html(pesan_cek);
									check_result_attJamSelesai.html('');
									check_result_attRuangID.html('');
									cek_JamMulai();
								}
							});
							
							function cek_JamMulai()
							{
								var attPresensiID = $("input[name='attPresensiID<?php echo $i; ?>']").val();
								var attTanggal = $("input[name='attTanggal<?php echo $i; ?>']").val();
								var attJamMulai = $("input[name='attJamMulai<?php echo $i; ?>']").val();
								var attJamSelesai = $("input[name='attJamSelesai<?php echo $i; ?>']").val();
								var attRuangID = $("select[name='attRuangID<?php echo $i; ?>']").val();
								$.post("<?php echo site_url("schedule/ptl_edit_day_attendance_cek_bentrok"); ?>", { attPresensiID: attPresensiID, attTanggal: attTanggal, attJamMulai: attJamMulai, attJamSelesai: attJamSelesai, attRuangID: attRuangID }, function (result)
								{
									if(result == 1)
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html('');
										check_result_attJamSelesai.html('');
										check_result_attRuangID.html('');
									}
									else
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html("<font color='red'><b>Schedule conflict</b></font> <a href='<?php echo site_url("attendance/ptl_list_edit"); ?>/"+result+"' target='_blank'>click here to show detail!</a>");
										check_result_attJamSelesai.html('');
										check_result_attRuangID.html('');
									}
								});
							}
							
							var check_JamSelesai = $('#check_JamSelesai<?php echo $i; ?>');
							check_JamSelesai.on('keyup', function()
							{
								if(check_JamSelesai.val().length <= jum_karakter_minimal)
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html('');
									check_result_attJamSelesai.html(karakter_minimal_error);
									check_result_attRuangID.html('');
								}
								else
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html('');
									check_result_attJamSelesai.html(pesan_cek);
									check_result_attRuangID.html('');
									cek_JamSelesai();
								}
							});
							
							function cek_JamSelesai()
							{
								var attPresensiID = $("input[name='attPresensiID<?php echo $i; ?>']").val();
								var attTanggal = $("input[name='attTanggal<?php echo $i; ?>']").val();
								var attJamMulai = $("input[name='attJamMulai<?php echo $i; ?>']").val();
								var attJamSelesai = $("input[name='attJamSelesai<?php echo $i; ?>']").val();
								var attRuangID = $("select[name='attRuangID<?php echo $i; ?>']").val();
								$.post("<?php echo site_url("schedule/ptl_edit_day_attendance_cek_bentrok"); ?>", { attPresensiID: attPresensiID, attTanggal: attTanggal, attJamMulai: attJamMulai, attJamSelesai: attJamSelesai, attRuangID: attRuangID }, function (result)
								{
									if(result == 1)
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html('');
										check_result_attJamSelesai.html('');
										check_result_attRuangID.html('');
									}
									else
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html('');
										check_result_attJamSelesai.html("<font color='red'><b>Schedule conflict</b></font> <a href='<?php echo site_url("attendance/ptl_list_edit"); ?>/"+result+"' target='_blank'>click here to show detail!</a>");
										check_result_attRuangID.html('');
									}
								});
							}
							
							var check_RuangID = $('#check_RuangID<?php echo $i; ?>');
							check_RuangID.on('click', function()
							{
								if(check_RuangID.val().length <= jum_karakter_minimal)
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html('');
									check_result_attJamSelesai.html('');
									check_result_attRuangID.html(karakter_minimal_error);
								}
								else
								{
									check_result_attTanggal.html('');
									check_result_attJamMulai.html('');
									check_result_attJamSelesai.html('');
									check_result_attRuangID.html(pesan_cek);
									cek_RuangID();
								}
							});
							
							function cek_RuangID()
							{
								var attPresensiID = $("input[name='attPresensiID<?php echo $i; ?>']").val();
								var attTanggal = $("input[name='attTanggal<?php echo $i; ?>']").val();
								var attJamMulai = $("input[name='attJamMulai<?php echo $i; ?>']").val();
								var attJamSelesai = $("input[name='attJamSelesai<?php echo $i; ?>']").val();
								var attRuangID = $("select[name='attRuangID<?php echo $i; ?>']").val();
								$.post("<?php echo site_url("schedule/ptl_edit_day_attendance_cek_bentrok"); ?>", { attPresensiID: attPresensiID, attTanggal: attTanggal, attJamMulai: attJamMulai, attJamSelesai: attJamSelesai, attRuangID: attRuangID }, function (result)
								{
									if(result == 1)
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html('');
										check_result_attJamSelesai.html('');
										check_result_attRuangID.html('');
									}
									else
									{
										check_result_attTanggal.html('');
										check_result_attJamMulai.html('');
										check_result_attJamSelesai.html('');
										check_result_attRuangID.html("<font color='red'><b>Schedule conflict</b></font> <a href='<?php echo site_url("attendance/ptl_list_edit"); ?>/"+result+"' target='_blank'>click here to show detail!</a>");
									}
								});
							}
						});
			<?php
					}
				}
			?>
		</script>
	    <script type="text/javascript">
			var f=jQuery.noConflict();
			f(this).ready( function()
			{
				f("#<?php echo @$id; ?>").autocomplete(
				{
					minLength: 1,
					source: 
					function(req, add)
					{
						f.ajax(
						{
							url: "<?php echo site_url(@$controller); ?>",
							dataType: 'json',
							type: 'POST',
							data: req,
							success:    
							function(data)
							{
								if(data.response =="true")
								{
									add(data.message);
								}
							},
						});
					},
					select: 
						function(event, ui)
						{
							// f("#result").append
							// (
								// "<li>"+ ui.item.value + "</li>"
							// );
							f(this).val(ui.item.value);
							f('#student').submit();
						},
				});
			});
	    </script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/fancy_box/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">
			var $fcb = jQuery.noConflict();
			$fcb(document).ready(function() 
			{
				$fcb('.fancybox').fancybox();
			});
		</script>
	</body>
</html>