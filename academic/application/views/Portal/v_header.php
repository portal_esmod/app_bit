<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Esmod Jakarta - Academic</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/css/main-style.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/default.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/style.css" type="text/css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/css/shCoreDefault.css">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/XRegExp.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shCore.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shLegacy.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushJScript.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/datepicker/javascript/shBrushXML.js"></script>
		<script type="text/javascript">
			SyntaxHighlighter.defaults['toolbar'] = false;
			SyntaxHighlighter.all();
		</script>
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/timepicker/css/mtimepicker.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/timepicker/css/styles.css" />
		
        <style>
            /****** Rating Starts *****/
            @import url(<?php echo base_url("assets/rating/font-awesome.css"); ?>);
            .rating
			{
                border: none;
                float: left;
            }
            .rating > input { display: none; }
            .rating > label:before
			{
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }
            .rating > .half:before
			{
                content: "\f089";
                position: absolute;
            }
            .rating > label
			{
                color: #ddd; 
                float: right; 
            }
            .rating > input:checked ~ label,
            .rating:not(:checked) > label:hover,
            .rating:not(:checked) > label:hover ~ label { color: #FFD700; }
            .rating > input:checked + label:hover,
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label,
            .rating > input:checked ~ label:hover ~ label { color: #FFED85; }
            /* Downloaded from http://devzone.co.in/ */
        </style>
        <script src="<?php echo base_url("assets/rating/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/rating/ca-pub-2074772727795809.js"); ?>" type="text/javascript" async=""></script>
		<script src="<?php echo base_url("assets/rating/analytics.js"); ?>" async=""></script>
		
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116213135-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-116213135-1');
		</script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
				<noscript>
					<center>
						<h4><font color="red"><b>Warning!</b></font></h4>
						<p><font color="red">You need to have <a href="http://www.enable-javascript.com/id/" target="_blank">JavaScript</a> enabled to use this site.</font></p>
					</center>
				</noscript>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://esmodjakarta.com/">
						<img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" alt="" />
					</a>
				</div>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<?php
							$CI =& get_instance();
							$this->load->model('m_notifikasi');
							$id_akun = $_COOKIE["id_akun"];
							$rownotif = $CI->m_notifikasi->PTL_all_select_unread($id_akun);
						?>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<span class="top-label label label-danger"><?php if(count($rownotif) != 0){ echo count($rownotif); }; ?></span><i class="fa fa-envelope fa-3x"></i>
						</a>
						<ul class="dropdown-menu dropdown-messages">
							<?php
								$nn = 0;
								if($rownotif)
								{
									foreach($rownotif as $rn)
									{
										if($nn < 5)
										{
											echo "
												<li>
													<a href='".base_url("notification/ptl_detail/$rn->id_notifikasi/$rn->noteid")."'>
														<div>
															<strong><span class='label label-danger'>$rn->nama_buat</span></strong>
															<span class='pull-right text-muted'>
																<em>$rn->tanggal_buat</em>
															</span>
														</div>
														<div>$rn->title</div>
													</a>
												</li>
												";
											$nn++;
										}
									}
								}
								else
								{
									echo "
										<li>
											<center><div>No Notification</div></center>
										</li>
										";
								}
							?>
							<li class="divider"></li>
							<li>
								<a class="text-center" href="<?php echo site_url("notification"); ?>">
									<strong>Read All Messages</strong>
									<i class="fa fa-angle-right"></i>
								</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-user fa-3x"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li><a href="<?php echo site_url("login/ptl_profile"); ?>"><i class="fa fa-user fa-fw"></i>User Profile</a></li>
							<li><a href="<?php echo site_url("login/ptl_password"); ?>"><i class="fa fa-gear fa-fw"></i>Change Password</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo site_url("login/ptl_logout"); ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="side-menu">
						<li>
							<div class="user-section">
								<div class="user-section-inner">
									<?php
										$foto = "ptl_storage/foto_umum/user.jpg";
										$foto_preview = "ptl_storage/foto_umum/user.jpg";
										$Fotoku = @$_COOKIE["foto"];
										if($Fotoku != "")
										{
											$foto = "../hris/system_storage/foto_karyawan/$Fotoku";
											$exist = file_exists_remote(base_url("$foto"));
											if($exist)
											{
												$foto = $foto;
												$source_photo = base_url("$foto");
												$info = pathinfo($source_photo);
												$foto_preview = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
												$exist1 = file_exists_remote(base_url("$foto_preview"));
												if($exist1)
												{
													$foto_preview = $foto_preview;
												}
												else
												{
													$foto_preview = $foto;
												}
											}
											else
											{
												$foto = "ptl_storage/foto_umum/user.jpg";
												$foto_preview = "ptl_storage/foto_umum/user.jpg";
											}
										}
									?>
									<a class="fancybox" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery" >
										<img class="img-polaroid" src="<?php echo base_url("$foto_preview"); ?>" width="150" height="150" alt=""/>
									</a>
								</div>
								<div class="user-info">
									<div><strong><?php $word = explode(" ",$_COOKIE["nama"]); echo $word[0]; ?></strong></div>
									<div class="user-text-online">
										<span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
									</div>
								</div>
							</div>
						</li>
						<?php $menu = $this->session->userdata('menu');?>
						<li class="<?php if($menu == "home"){ echo "selected";} ?>">
							<a href="<?php echo site_url("login/ptl_home"); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
						</li>
						<?php
							if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "year"){ echo "selected";} ?>">
									<a href="<?php echo site_url("year"); ?>"><i class="fa fa-dashboard fa-fw"></i> Academic Year</a>
								</li>
						<?php
							}
						?>
						<li class="<?php if($menu == "calendar"){ echo "selected";} ?>">
							<a href="<?php echo site_url("calendar"); ?>"><i class="fa fa-dashboard fa-fw"></i> Academic Calendar</a>
						</li>
						<li class="<?php if($menu == "lecturer_calendar"){ echo "selected";} ?>">
							<a href="<?php echo site_url("lecturer_calendar"); ?>"><i class="fa fa-dashboard fa-fw"></i> Lecturer Calendar</a>
						</li>
						<?php
							if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "kelas"){ echo "selected";} ?>">
									<a href="#"><i class="fa fa-dashboard fa-fw"></i> Class<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="<?php echo site_url("kelas"); ?>"> Before Enroll</a>
										</li>
										<li>
											<a href="<?php echo site_url("kelas/ptl_after"); ?>"> After Enroll</a>
										</li>
									</ul>
								</li>
								<li class="<?php if($menu == "language"){ echo "selected";} ?>">
									<a href="<?php echo site_url("language"); ?>"><i class="fa fa-dashboard fa-fw"></i> Language</a>
								</li>
								<li class="<?php if($menu == "spesialisasi"){ echo "selected";} ?>">
									<a href="<?php echo site_url("spesialisasi"); ?>"><i class="fa fa-dashboard fa-fw"></i> Specialization</a>
								</li>
						<?php
							}
							if((($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "homeroom"){ echo "selected";} ?>">
									<a href="<?php echo site_url("homeroom"); ?>"><i class="fa fa-dashboard fa-fw"></i> Homeroom Teacher</a>
								</li>
								<li class="<?php if($menu == "schedule"){ echo "selected";} ?>">
									<a href="<?php echo site_url("schedule"); ?>"><i class="fa fa-dashboard fa-fw"></i> Schedule</a>
								</li>
								<li class="<?php if($menu == "attendance"){ echo "selected";} ?>">
									<a href="<?php echo site_url("attendance"); ?>"><i class="fa fa-dashboard fa-fw"></i> Attendance</a>
								</li>
						<?php
							}
							if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "enrollment"){ echo "selected";} ?>">
									<a href="<?php echo site_url("enrollment"); ?>"><i class="fa fa-dashboard fa-fw"></i> Enrollment</a>
								</li>
								<li class="<?php if($menu == "drop"){ echo "selected";} ?>">
									<a href="<?php echo site_url("drop"); ?>"><i class="fa fa-dashboard fa-fw"></i> Student Subject Management</a>
								</li>
						<?php
							}
							if((($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "scoring"){ echo "selected";} ?>">
									<a href="<?php echo site_url("scoring"); ?>"><i class="fa fa-dashboard fa-fw"></i> Scoring</a>
								</li>
								<li class="<?php if($menu == "students_participation"){ echo "selected";} ?>">
									<a href="<?php echo site_url("students_participation"); ?>"><i class="fa fa-dashboard fa-fw"></i> Students Participation</a>
								</li>
								<li class="<?php if($menu == "exam"){ echo "selected";} ?>">
									<a href="#"><i class="fa fa-dashboard fa-fw"></i> Exam & Short Semester<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="<?php echo site_url("exam"); ?>"> Exam</a>
										</li>
										<li>
											<a href="<?php echo site_url("remedial"); ?>"> Short Semester</a>
										</li>
									</ul>
								</li>
								<!--<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Management OJT</a>
								</li>
								<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Participant OJT</a>
								</li>
								<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Probation Period</a>
								</li>
								<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Setup Award</a>
								</li>
								<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Jury Session</a>
								</li>
								<li class="<?php if($menu == "home"){ echo "selected";} ?>">
									<a href="<?php echo site_url("login"); ?>"><i class="fa fa-dashboard fa-fw"></i>Jury Day</a>
								</li>
								<li class="<?php if($menu == "evaluation"){ echo "selected";} ?>">
									<a href="<?php echo site_url("evaluation"); ?>"><i class="fa fa-dashboard fa-fw"></i>Student Evaluation</a>
								</li>-->
						<?php
							}
						?>
						<li class="<?php if($menu == "evaluation"){ echo "selected";} ?>">
							<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Student Evaluation<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo site_url("attendance_problem"); ?>"> Attendance Problem</a>
								</li>
								<li>
									<a href="<?php echo site_url("evaluation"); ?>"> Evaluation</a>
								</li>
								<li>
									<a href="<?php echo site_url("transcript"); ?>"> Transcript</a>
								</li>
								<li>
									<a href="<?php echo site_url("rank"); ?>"> Student Performance Report</a>
								</li>
							</ul>
						</li>
						<?php
							if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
							{
						?>
								<li class="<?php if($menu == "publication"){ echo "selected";} ?>">
									<a href="<?php echo site_url("publication"); ?>"><i class="fa fa-dashboard fa-fw"></i> Publication</a>
								</li>
						<?php
							}
						?>
						<li class="<?php if($menu == "additional"){ echo "selected";} ?>">
							<a href="#"><i class="fa fa-wrench fa-fw"></i> Additional<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo site_url("today"); ?>"> Schedule</a>
								</li>
								<li>
									<a href="<?php echo site_url("room_available"); ?>"> Room Available</a>
								</li>
								<?php
									if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
									{
								?>
										<li>
											<a href="<?php echo site_url("interview"); ?>"> Interview From Admission</a>
										</li>
										<li>
											<a href="<?php echo site_url("pmbreq"); ?>"> Form Requirement for Admission</a>
										</li>
										<li>
											<a href="<?php echo site_url("inter"); ?>"> Standard Interview for Admission</a>
										</li>
										<li>
											<a href="<?php echo site_url("pending_new"); ?>"> Pending Payment (New)</a>
										</li>
										<li>
											<a href="<?php echo site_url("pending_old"); ?>"> Pending Payment (Current)</a>
										</li>
								<?php
									}
								?>
							</ul>
						</li>
							<li class="<?php if($menu == "master"){ echo "selected";} ?>">
								<a href="#"><i class="fa fa-wrench fa-fw"></i> Master Data<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<?php
										if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
										{
									?>
											<li>
												<a href="<?php echo site_url("institutional"); ?>"> Institutional Identity</a>
											</li>
											<li>
												<a href="<?php echo site_url("official"); ?>"> Official</a>
											</li>
											<li>
												<a href="<?php echo site_url("account"); ?>"> Institutional Account</a>
											</li>
											<li>
												<a href="<?php echo site_url("degree"); ?>"> Degree</a>
											</li>
											<li>
												<a href="<?php echo site_url("program"); ?>"> Program</a>
											</li>
											<li>
												<a href="<?php echo site_url("regular"); ?>"> Regular</a>
											</li>
											<li>
												<a href="<?php echo site_url("short"); ?>"> Short Course</a>
											</li>
											<li>
												<a href="<?php echo site_url("students"); ?>"> Students</a>
											</li>
									<?php
										}
									?>
											<li>
												<a href="<?php echo site_url("lecturer"); ?>"> Lecturer</a>
											</li>
									<?php
										if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
										{
									?>
											<li>
												<a href="<?php echo site_url("class_list"); ?>"> Class</a>
											</li>
											<li>
												<a href="<?php echo site_url("status"); ?>"> Status</a>
											</li>
											<li>
												<a href="<?php echo site_url("room"); ?>"> Campus & Room</a>
											</li>
									<?php
										}
									?>
								</ul>
							</li>
						<?php
							if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC")) OR (($_COOKIE["divisi"] == "PRESIDENT") OR (stristr($_COOKIE["akses"],"COORDINATOR"))))
							{
						?>
								<li class="<?php if($menu == "system"){ echo "selected";} ?>">
									<a href="#"><i class="fa fa-wrench fa-fw"></i> Academic System<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="<?php echo site_url("curriculum"); ?>"> Curriculum</a>
										</li>
										<li>
											<a href="<?php echo site_url("type"); ?>"> Subject Type</a>
										</li>
										<li>
											<a href="<?php echo site_url("group"); ?>"> Subject Grup</a>
										</li>
										<li>
											<a href="<?php echo site_url("specialization"); ?>"> Specialization</a>
										</li>
										<li>
											<a href="<?php echo site_url("subject"); ?>"> Subject</a>
										</li>
										<li>
											<a href="<?php echo site_url("project"); ?>"> Project</a>
										</li>
										<li>
											<a href="<?php echo site_url("subject_score"); ?>"> Subject Score</a>
										</li>
										<li>
											<a href="<?php echo site_url("project_score"); ?>"> Project Score</a>
										</li>
										<li>
											<a href="<?php echo site_url("attendance_score"); ?>"> Attendance Score</a>
										</li>
										<li>
											<a href="<?php echo site_url("predicate"); ?>"> Predicate</a>
										</li>
									</ul>
								</li>
						<?php
							}
						?>
					</ul>
				</div>
			</nav>