			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Exam Card</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("exam"); ?>">Exam</a>
							>>
							<a href="<?php echo site_url("exam/ptl_card"); ?>">Exam Card (EXAM20)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Evaluation Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("exam/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('exam_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("exam/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('exam_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	$f = "";
																	if($rt->NA == "N")
																	{
																		$f = "style='background-color: #5BB734;'";
																	}
																	echo "<option value='$rt->TahunID' $f";
																	if($cektahun == $rt->TahunID)
																	{
																		echo "selected";
																	}
																	echo ">$rt->TahunID - $rt->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<form role="form" id="student" action="<?php echo site_url("exam/ptl_cari"); ?>" method="POST">
											<td colspan="4">
												<input type="text" name="cari" value="<?php echo $this->session->userdata('exam_filter_mahasiswa'); ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
										</form>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													if($r["Foto"] == "")
													{
														$foto = "foto_umum/user.jpg";
													}
													else
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = "foto_mahasiswa/".$r["Foto"];
														}
														else
														{
															$foto = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="5"><p align="center"><img src="<?php echo base_url("ptl_storage/$foto"); ?>" width="200px"/></p></td>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td><?php echo $r["TempatLahir"].", ".tgl_indo($r["TanggalLahir"]); ?></td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td>
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td>
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td>
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
													?>
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<center>
									<?php
										if($this->session->userdata('exam_filter_sort_date') == "Y")
										{
											echo "<a href='".site_url("exam/ptl_filter_exam_pass_undate")."' class='btn btn-success'>Sort by Name</a>";
										}
										else
										{
											echo "<a href='".site_url("exam/ptl_filter_exam_pass_date")."' class='btn btn-success'>Sort by Date</a>";
										}
									?>
								</center>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>CODE</th>
											<th>SUBJECT</th>
											<th>ROOM</th>
											<th>DATE</th>
											<th>TIME</th>
											<th>SUPERVISOR</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$status = 0;
											if($rowrecord)
											{
												foreach($rowrecord as $row)
												{
													$KRSID = $row->KRSID;
													$TahunID = $row->TahunID;
													$SubjekID = $row->SubjekID;
													$resexam = $this->m_exam->PTL_all_select_exam_card($TahunID,$SubjekID);
													$exam = "";
													if($resexam)
													{
														foreach($resexam as $re)
														{
															if(($re->TahunID == $TahunID) OR ($re->TahunID2 == $TahunID))
															{
																if(($re->SubjekID == $SubjekID) OR ($re->SubjekID12 == $SubjekID) OR ($re->SubjekID2 == $SubjekID))
																{
																	$ExamID = $re->ExamID;
																	$reskursi = $this->m_exam_kursi->PTL_select($ExamID,$MhswID);
																	$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																	$resdosen = $this->m_exam_dosen->PTL_select($ExamID);
																	$DosenID = "";
																	$dosen = "";
																	if($resdosen)
																	{
																		$DosenID = $resdosen["DosenID"];
																		$resdosen2 = $this->m_dosen->PTL_select($DosenID);
																		$dosen = $resdosen2["Nama"];
																	}
																	if($reskursi)
																	{
																		$data = array(
																					'ExamID' => $re->ExamID
																					);
																		$this->m_krs->PTL_update($KRSID,$data);
																		echo "
																			<tr>
																				<td title='ExamID : $re->ExamID ~ KRSID : $KRSID'>$ressubjek[SubjekKode]</td>
																				<td title='SubjekID : $row->SubjekID'>$ressubjek[Nama]</td>
																				<td>$re->RuangID</td>
																				<td>".tgl_singkat_eng($re->Tanggal)."</td>
																				<td>$re->JamMulai - $re->JamSelesai</td>
																				<td>$dosen</td>
																			</tr>
																			";
																		$status = 1;
																	}
																}
															}
														}
													}
												}
											}
											if($status == 0)
											{
												echo "<tr>
														<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
								<center>
									<?php
										if($status == 1)
										{
											echo "<a href='".site_url("exam/ptl_pdf_exam_pass")."' class='btn btn-info'>Print Exam Pass</a>";
										}
									?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>