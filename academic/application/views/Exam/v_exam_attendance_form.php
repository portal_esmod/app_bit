		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Set Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("exam"); ?>">Exam</a>
							>>
							<a href="<?php echo site_url("exam/ptl_attendance/$ExamID/$RuangID"); ?>">Set Attendance (EXAM14)</a>
                        </div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Academic Year Regular</th>
										<th>:</th>
										<th>
											<?php
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Academic Year Intensive</th>
										<th>:</th>
										<th>
											<?php
												$TahunID = $TahunID2;
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Regular 1</th>
										<th>:</th>
										<th>
											<?php
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Subject Regular 2</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID12;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID12." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Intensive</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID2;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID2." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Lecturer</th>
										<th>:</th>
										<th>
											<?php
												$e = $this->m_exam_dosen->PTL_select($ExamID);
												$DosenID = "";
												$dosen = "-";
												if($e)
												{
													$DosenID = $e["DosenID"];
													$d = $this->m_dosen->PTL_select($DosenID);
													$dosen = "<font color='red'><b>No Supervisor</b></font>";
													if($d)
													{
														$dosen = $DosenID." - ".$d["Nama"];
													}
												}
												echo $dosen;
											?>
										</th>
									</tr>
								</thead>
							</table>
						</div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Students List</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<center><a href="<?php echo site_url("exam/ptl_scoring/$ExamID/$RuangID"); ?>" class="btn btn-success">Go to Scoring</a></center>
								<br/>
								<form action="<?php echo site_url("exam/ptl_attendance_insert"); ?>" method="POST">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name & ID</th>
												<th>Attendance</th>
											</tr>
										</thead>
										<tbody>
											<?php
												for($i=1;$i<=$kapasitas;$i++)
												{
													$NomorKursi = $i;
													$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
													if($rkursi)
													{
														$MhswID = $rkursi["MhswID"];
														$mhsw = " - ".$rkursi["MhswID"];
														$r = $this->m_mahasiswa->PTL_select($MhswID);
														$resexam = $this->m_exam_mahasiswa->PTL_all_evaluation($ExamID,$MhswID);
														$ExamMhswID = "";
														$Presensi = "";
														$f = "";
														if($resexam)
														{
															if($resexam["Presensi"] == "Y") { $f = "style='background-color: #D9EDF7;'"; }
															if($resexam["Presensi"] == "E") { $f = "style='background-color: yellow;'"; }
															if($resexam["Presensi"] == "N") { $f = "style='background-color: #F2DEDE;'"; }
															$ExamMhswID = $resexam["ExamMhswID"];
															$Presensi = $resexam["Presensi"];
														}
														echo "
															<tr class='success'>
																<td title='ExamMhswID : $ExamMhswID'>$NomorKursi</td>
																<td><a href='".site_url("evaluation/ptl_cari/$MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$MhswID</b></a> - <a href='".site_url("students/ptl_edit/$MhswID")."' title='Go to Personal Information' target='_blank'>$r[Nama]</a></td>
																<td>
																	<input type='hidden' name='ExamID$i' value='$ExamID'>
																	<input type='hidden' name='MhswID$i' value='$MhswID'>
																	<select name='Presensi$i' title='Set Presence' $f class='form-control round-form' required>
																		<option value=''>-- CHOOSE --</option>
																		<option value='Y' style='background-color: #D9EDF7;'";
																		if($Presensi == "Y")
																		{
																			echo "selected";
																		}
																		echo ">Present</option>
																		<option value='E' style='background-color: yellow;'";
																		if($Presensi == "E")
																		{
																			echo "selected";
																		}
																		echo ">Excuse</option>
																		<option value='N' style='background-color: #F2DEDE;'";
																		if($Presensi == "N")
																		{
																			echo "selected";
																		}
																		echo ">Absent</option>
																	</select>
																</td>
															</tr>";
													}
												}
											?>
										</tbody>
									</table>
									<center>
										<?php
											echo "
												<input type='hidden' name='exam' value='$ExamID'>
												<input type='hidden' name='ruang' value='$RuangID'>
												<input type='hidden' name='total' value='$i'>
												<a href='".site_url("exam")."' class='btn btn-warning'>Back</a>
												<input type='submit' value='Save Attendance' class='btn btn-info'>";
										?>
									</center>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Mapping Room <?php echo $RuangID; ?></center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<?php
										$klm = 1;
										for($i=1;$i<=$kapasitas;$i++)
										{
											$NomorKursi = $i;
											$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
											if($rkursi)
											{
												$MhswID = $rkursi["MhswID"];
												$mhsw = " - ".$rkursi["MhswID"];
												$r = $this->m_mahasiswa->PTL_select($MhswID);
												$nama = "";
												$namaMhsw = "";
												$foto = "foto_umum/user.jpg";
												$foto_preview = "foto_umum/user.jpg";
												if($r)
												{
													if($r["Foto"] != "")
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
														}
													}
													$word = explode(" ",$r["Nama"]);
													$kata1 = @$word[0]." ".@$word[1];
													$kata2 = "<span class='label label-info'>".@$word[2]." ".@$word[3]."</span>";
													if(@$word[2] == "")
													{
														$kata2 = "";
													}
													$namaMhsw = $r["Nama"];
													$nama = "
															<span class='label label-info'>".$kata1."</span>
															$kata2
															";
												}
												$img = "<a class='fancybox' title='$MhswID - $namaMhsw' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
															<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='80px' alt='' />
														</a>";
											}
											else
											{
												$mhsw = "";
												$nama = "";
												$img = "<img src='".base_url("assets/dashboard/img/kursi.jpg")."' width='80px'/>";
											}
											if($klm == 4)
											{
												$klm = 1;
												echo "<td><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
													</tr>";
											}
											else
											{
												if($klm == 1)
												{
													echo "
														<tr>
															<td><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
														";
												}
												else
												{
													echo "<td><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>";
												}
												$klm++;
											}
										}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>