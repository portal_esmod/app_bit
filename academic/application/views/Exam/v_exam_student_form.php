		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Allocation</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("exam"); ?>">Exam</a>
							>>
							<a href="<?php echo site_url("exam/ptl_form/$ExamID"); ?>">Add New Schedule</a>
							>>
							<a href="<?php echo site_url("exam/ptl_student_form/$ExamID"); ?>">Student Allocation (EXAM06)</a>
                        </div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Academic Year Regular</th>
										<th>:</th>
										<th>
											<?php
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Academic Year Intensive</th>
										<th>:</th>
										<th>
											<?php
												$TahunID = $TahunID2;
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Regular 1</th>
										<th>:</th>
										<th>
											<?php
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Subject Regular 2</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID12;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID12." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Intensive</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID2;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID2." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Lecturer</th>
										<th>:</th>
										<th>
											<?php
												$e = $this->m_exam_dosen->PTL_select($ExamID);
												$DosenID = "";
												$dosen = "-";
												if($e)
												{
													$DosenID = $e["DosenID"];
													$d = $this->m_dosen->PTL_select($DosenID);
													$dosen = "<font color='red'><b>No Supervisor</b></font>";
													if($d)
													{
														$dosen = $DosenID." - ".$d["Nama"];
													}
												}
												echo $dosen;
											?>
										</th>
									</tr>
								</thead>
							</table>
						</div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Students List</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Student Name & ID</th>
											<th>Sequence</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if(($row->TahunID == $AddTahunID) OR ($row->TahunID == $AddTahunID2))
													{
														if(($row->SubjekID == $AddSubjekID) OR ($row->SubjekID == $AddSubjekID12) OR ($row->SubjekID == $AddSubjekID2))
														{
															$MhswID = $row->MhswID;
															$rmhs = $this->m_mahasiswa->PTL_select($MhswID);
															$mhsw = "";
															if($rmhs)
															{
																$mhsw = $rmhs["Nama"];
															}
															$rexam = $this->m_exam_kursi->PTL_select($ExamID,$MhswID);
															if($rexam)
															{
																$cls = "class='success'";
																$inp = "<p align='center'>".$rexam["NomorKursi"]."</p>";
																$ExamKursiID = $rexam["ExamKursiID"];
															}
															else
															{
																$cls = "";
																$inp = "<input type='number' min='1' max='$kapasitas' name='NomorKursi' style='width:50px;' required>";
																$ExamKursiID = "";
															}
															echo "
																<tr $cls>
																	<td title='SubjekID : $row->SubjekID'>$no</td>
																	<td><a href='".site_url("evaluation/ptl_cari/$MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$MhswID</b></a> - <a href='".site_url("students/ptl_edit/$MhswID")."' title='Go to Personal Information' target='_blank'>$mhsw</a></td>
																		<form action='".site_url("exam/ptl_student_form_set/$ExamID")."' method='POST'>
																			<td>
																				$inp
																				<input type='hidden' name='MhswID' value='$MhswID'>
																				<input type='hidden' name='ExamID' value='$ExamID'>
																				<input type='hidden' name='SubjekID' value='$row->SubjekID'>
																				<input type='hidden' name='kapasitas' value='$kapasitas'>
																			</td>
																			<td>";
																			if($cls == "")
																			{
																				echo "<input type='submit' value='+' class='btn btn-success btn-circle'>";
																			}
																			else
																			{
																				echo "<a href='".site_url("exam/ptl_student_form_delete/$ExamKursiID/$ExamID")."' class='btn btn-danger btn-circle'>-</i>";
																			}
																		echo "</td>
																		</form>";
															echo "</tr>
																";
															$no++;
														}
													}
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Mapping Room <?php echo $RuangID; ?></center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<?php
										$klm = 1;
										for($i=1;$i<=$kapasitas;$i++)
										{
											$NomorKursi = $i;
											$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
											if($rkursi)
											{
												$ExamKursiID = $rkursi["ExamKursiID"];
												$MhswID = $rkursi["MhswID"];
												$mhsw = " - ".$rkursi["MhswID"];
												$r = $this->m_mahasiswa->PTL_select($MhswID);
												$foto = "foto_umum/user.jpg";
												$foto_preview = "foto_umum/user.jpg";
												$nama = "";
												if($r)
												{
													if($r["Foto"] != "")
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
														}
													}
													$word = explode(" ",$r["Nama"]);
													$kata1 = @$word[0]." ".@$word[1];
													$kata2 = "<span class='label label-info'>".@$word[2]." ".@$word[3]."</span>";
													if(@$word[2] == "")
													{
														$kata2 = "";
													}
													$nama = "
															<span class='label label-info'>".$kata1."</span>
															$kata2
															";
												}
												$img = "
													<a class='fancybox' title='$r[MhswID] - $r[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
														<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='80px' alt='' />
													</a>
													";
											}
											else
											{
												$ExamKursiID = "";
												$mhsw = "";
												$nama = "";
												$img = "<img src='".base_url("assets/dashboard/img/kursi.jpg")."' width='80px'/>";
											}
											if($klm == 4)
											{
												$klm = 1;
												echo "<td title='ExamKursiID: $ExamKursiID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
													</tr>";
											}
											else
											{
												if($klm == 1)
												{
													echo "
														<tr>
															<td title='ExamKursiID: $ExamKursiID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
														";
												}
												else
												{
													echo "<td title='ExamKursiID: $ExamKursiID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>";
												}
												$klm++;
											}
										}
									?>
								</table>
								<center>
									<a href="<?php echo site_url("exam/ptl_form/$ExamID"); ?>" class="btn btn-warning">Back</a>
									<a href="<?php echo site_url("exam"); ?>" class="btn btn-success">Done</a>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>