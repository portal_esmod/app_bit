		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Attendance Score</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<?php
								if($link == "SCORING")
								{
							?>
									<a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>">Scoring List</a>
									>>
									<a href="<?php echo site_url("attendance_score/ptl_edit/$Nilai3ID/SCORING/$JadwalID"); ?>">Edit Attendance Score</a>
							<?php
								}
								else
								{
									if($link == "ATTENDANCE")
									{
							?>
										<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
										>>
										<a href="<?php echo site_url("attendance_score/ptl_edit/$Nilai3ID/ATTENDANCE/$JadwalID"); ?>">Edit Attendance Score</a>
							<?php
									}
									else
									{
							?>
										<a href="<?php echo site_url("attendance_score"); ?>">Attendance Score</a>
										>>
										<a href="<?php echo site_url("attendance_score/ptl_edit/$Nilai3ID"); ?>">Edit Attendance Score</a>
							<?php
									}
								}
							?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("attendance_score/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Attendance ID</label>
											<input readonly type="text" name="JenisPresensiID" value="<?php echo $JenisPresensiID; ?>" class="form-control" required>
											<input type="hidden" name="Nilai3ID" value="<?php echo $Nilai3ID; ?>" class="form-control" required>
											<input type="hidden" name="link" value="<?php echo $link; ?>" class="form-control" required>
											<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Score</label>
											<input type="number" name="Score" value="<?php echo $Score; ?>" step="0.01" min="0" max="<?php echo $MaxScore; ?>" title="Maximum Score <?php echo $MaxScore; ?>" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<?php
												if($link == "SCORING")
												{
											?>
													<a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>" class="btn btn-warning">Back</a>
											<?php
												}
												else
												{
													if($link == "ATTENDANCE")
													{
											?>
														<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-warning">Back</a>
											<?php
													}
													else
													{
											?>
														<a href="<?php echo site_url("subject_score"); ?>" class="btn btn-warning">Back</a>
											<?php
													}
												}
											?>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>