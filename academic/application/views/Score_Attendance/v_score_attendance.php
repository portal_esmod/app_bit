		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance Score</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance_score"); ?>">Attendance Score</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("attendance_score/ptl_filter_kur"); ?>" method="POST">
												<select name="cekkurikulum" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- CURRICULUM --</option>
													<?php
														$cekkurikulum = $this->session->userdata('att_scr_filter_kur');
														if($rowkurikulum)
														{
															foreach($rowkurikulum as $rk)
															{
																// if($rk->NA == "N")
																// {
																	echo "<option value='$rk->KurikulumID'";
																	if($cekkurikulum == $rk->KurikulumID)
																	{
																		echo "selected";
																	}
																	echo ">$rk->KurikulumKode - $rk->Nama</option>";
																// }
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td>
											<form action="<?php echo site_url("attendance_score/ptl_filter_type"); ?>" method="POST">
												<select name="cektype" title="Filter by Type of Subjects" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- TYPE --</option>
													<?php
														$cektype = $this->session->userdata('att_scr_filter_type');
														if($rowtype)
														{
															foreach($rowtype as $rt)
															{
																// if($rk->NA == "N")
																// {
																	echo "<option value='$rt->JenisMKID'";
																	if($cektype == $rt->JenisMKID)
																	{
																		echo "selected";
																	}
																	echo ">$rt->Nama</option>";
																// }
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<br/>
								<center>
									<a href="<?php echo site_url("attendance_score/ptl_copy"); ?>" class="btn btn-warning"><i class="fa fa-copy"> Copy</i></a>
								</center>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Attendance ID</th>
											<th>Attendance Status</th>
											<th>Score</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 0;
												foreach($rowrecord as $row)
												{
													$JenisPresensiID = $row->JenisPresensiID;
													$res = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
													$nama = "";
													if($res)
													{
														$nama = $res["Nama"];
													}
													$no++;
													echo "<tr>
															<td align='center' title='Nilai3ID : $row->Nilai3ID'>$row->JenisPresensiID</td>
															<td>$nama</td>
															<td align='right'>$row->Score</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("attendance_score/ptl_edit/$row->Nilai3ID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
												}
											}
											else
											{
												echo "<tr>
														<td colspan='4' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>