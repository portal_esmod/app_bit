		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Academic Year</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("year"); ?>">Academic Year (ACAYE01)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the first step before determining class of students. Only one academic year may be active. <a href="<?php echo site_url("tutorial/ptl_detail/ACAYE01"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<form action="<?php echo site_url("year/ptl_filter_jur"); ?>" method="POST">
									<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- PROGRAM --</option>
										<?php
											$cekjurusan = $this->session->userdata('year_filter_jur');
											echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
											echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
											echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<br/>
								<?php
									if($cekjurusan != "")
									{
								?>
										<center><a href="<?php echo site_url("year/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th>#</th>
													<th>Code</th>
													<th>Year Name</th>
													<th>Academic</th>
													<th>Enrollment</th>
													<th>Specialization</th>
													<th>Payment</th>
													<th>Participant</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															$TahunID = $row->TahunID;
															$res = $this->m_khs->PTL_all_total($TahunID);
															$participant = count($res);
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>
																		<td>$no</td>
																		<td>$row->TahunID</td>
																		<td>$row->Nama</td>
																		<td>
																			$row->Academic
																			<br/>
																			$row->AcademicEnd
																		</td>
																		<td>
																			$row->Enrollment
																			<br/>
																			$row->EnrollmentEnd
																		</td>
																		<td>
																			$row->Specialization
																			<br/>
																			$row->SpecializationEnd
																		</td>
																		<td>
																			$row->Payment
																			<br/>
																			$row->PaymentEnd
																		</td>
																		<td align='right'><a href='".site_url("year/ptl_participant/$row->TahunID")."' target='_blank'>$participant</a></td>
																		<td class='center'>
																			<a class='btn btn-info' href='".site_url("year/ptl_edit/$row->TahunID")."'>
																				<i class='fa fa-list'></i>
																			</a>
																			<a class='btn btn-success' href='".site_url("year/ptl_enabled/$row->TahunID")."'>
																				<i class='fa fa-check'></i>
																			</a>";
												?>
																			<a class="btn btn-warning" href="<?php echo site_url("year/ptl_copy/$row->TahunID"); ?>" onclick="return confirm('Are you sure to make a copy of this year (<?php echo $row->TahunID." - ".$row->Nama; ?>)?')">
																				<i class="fa fa-copy"></i>
																			</a>
												<?php
																		if((stristr($_COOKIE["akses"],"MANAGER")) OR (stristr($_COOKIE["akses"],"PRESIDENT")) OR (stristr($_COOKIE["akses"],"COUNSELOR")))
																		{
																			echo "<a class='btn btn-success' href='".site_url("year/ptl_admin_enabled/$row->TahunID")."'>
																					<i class='fa fa-star'></i>
																				</a>";
																		}
																echo "</td>
																	</tr>";
															}
															else
															{
																echo "<tr>
																		<td>$no</td>
																		<td>$row->TahunID</td>
																		<td>$row->Nama</td>
																		<td>
																			$row->Academic
																			<br/>
																			$row->AcademicEnd
																		</td>
																		<td>
																			$row->Enrollment
																			<br/>
																			$row->EnrollmentEnd
																		</td>
																		<td>
																			$row->Specialization
																			<br/>
																			$row->SpecializationEnd
																		</td>
																		<td>
																			$row->Payment
																			<br/>
																			$row->PaymentEnd
																		</td>
																		<td align='right'><a href='".site_url("year/ptl_participant/$row->TahunID")."' target='_blank'>$participant</a></td>
																		<td class='center'>
																			<a class='btn btn-info' href='".site_url("year/ptl_edit/$row->TahunID")."'>
																				<i class='fa fa-list'></i>
																			</a>
																			<a class='btn btn-danger' href='".site_url("year/ptl_disabled/$row->TahunID")."'>
																				<i class='fa fa-times'></i>
																			</a>";
												?>
																			<a class="btn btn-warning" href="<?php echo site_url("year/ptl_copy/$row->TahunID"); ?>" onclick="return confirm('Are you sure to make a copy of this year (<?php echo $row->TahunID." - ".$row->Nama; ?>)?')">
																				<i class="fa fa-copy"></i>
																			</a>
												<?php
																		if((stristr($_COOKIE["akses"],"MANAGER")) OR (stristr($_COOKIE["akses"],"PRESIDENT")) OR (stristr($_COOKIE["akses"],"COUNSELOR")))
																		{
																			echo "<a class='btn btn-danger' href='".site_url("year/ptl_admin_disabled/$row->TahunID")."'>
																					<i class='fa fa-star'></i>
																				</a>";
																		}
																echo "</td>
																	</tr>";
															}
															$no++;
														}
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>