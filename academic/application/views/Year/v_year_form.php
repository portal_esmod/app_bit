		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Academic Year</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("year"); ?>">Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_form"); ?>">Add New Academic Year (ACAYE03)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php
									$jur = $this->session->userdata('year_filter_jur');
									if($jur == "REG"){ $j = "REG - Regular"; $d = "Three Years Program"; $jen = "D3"; }
									if($jur == "INT"){ $j = "INT - Intensive"; $d = "One Years Program"; $jen = "D1"; }
									if($jur == "SC"){ $j = "SC - Short Course"; $d = "Short Course Program"; $jen = "SC"; }
								?>
								<form role="form" action="<?php echo site_url("year/ptl_insert"); ?>" method="POST">
									<div class="col-lg-12">
										<div class="alert alert-info">
											<a class="alert-link">Notes:
											<br/>
											<a class="alert-link"><font color="blue">Blue</font>: </a>Auto-filled.
											<a class="alert-link"><font color="red">Red</font>: </a>Must be filled.
											<a class="alert-link">Black: </a>Optional. <a href="<?php echo site_url("tutorial/ptl_detail/ACYR002"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="blue">Program</font></label>
											<input readonly type="text" value="<?php echo $j; ?>" class="form-control">
											<input type="hidden" name="Jenjang2ID" value="<?php echo $jen; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Year Code</font></label>
											<input type="text" name="kode_tahun" value="<?php echo $kode_tahun; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Curriculum</font></label>
											<select name="KurikulumID" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowkurikulum)
													{
														foreach($rowkurikulum as $row)
														{
															echo "<option value='$row->KurikulumID'>$row->KurikulumID - $row->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization Process</label>
											<input type="text" name="Specialization" id="datepickerSC2" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Payment Process</label>
											<input type="text" name="Payment" id="datepickerSC5" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Enrollment Process</label>
											<input type="text" name="Enrollment" id="datepickerSC1" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postponed</label>
											<input type="text" name="Postpone" id="datepickerSC3" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Academic Process</label>
											<input type="text" name="Academic" id="datepickerSC4" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Questionnaire Process</label>
											<input type="text" name="Questionnaire" id="datepickerSC6" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Evaluation Assessment</label>
											<input type="text" name="Evaluation" id="datepicker6" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Submission</label>
											<input type="text" name="Submission" id="datepicker7" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Job Training</label>
											<input type="text" name="JobTraining" id="datepicker8" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Session</label>
											<input type="text" name="JurySession" id="datepicker9" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Day</label>
											<input type="text" name="JuryDay" id="datepicker10" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Exam</label>
											<input type="text" name="Exam" id="datepicker11" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Remedial</label>
											<input type="text" name="Remedial" id="datepicker12" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>-->
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="blue">Degree</font></label>
											<input readonly type="text" value="<?php echo $d; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Semester</font></label>
											<select name="Nama" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="ODD">ODD</option>
												<option value="EVEN">EVEN</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Rating Code</label>
											<select name="id_rating_question_grup" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowrating)
													{
														foreach($rowrating as $row)
														{
															echo "<option value='$row->id_rating_question_grup'>$row->id_rating_question_grup - $row->nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization End</label>
											<input type="text" name="SpecializationEnd" id="datepickerSC2-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Payment End</label>
											<input type="text" name="PaymentEnd" id="datepickerSC5-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Enrollment End</label>
											<input type="text" name="EnrollmentEnd" id="datepickerSC1-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postponed End</label>
											<input type="text" name="PostponeEnd" id="datepickerSC3-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Academic End</label>
											<input type="text" name="AcademicEnd" id="datepickerSC4-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Questionnaire End</label>
											<input type="text" name="QuestionnaireEnd" id="datepickerSC6-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Evaluation End</label>
											<input type="text" name="EvaluationEnd" id="datepicker18" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Submission End</label>
											<input type="text" name="SubmissionEnd" id="datepicker19" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Job Training End</label>
											<input type="text" name="JobTrainingEnd" id="datepicker20" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Session End</label>
											<input type="text" name="JurySessionEnd" id="datepicker21" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Day End</label>
											<input type="text" name="JuryDayEnd" id="datepicker22" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Exam End</label>
											<input type="text" name="ExamEnd" id="datepicker23" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Remedial End</label>
											<input type="text" name="RemedialEnd" id="datepicker24" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>-->
									</div>
									<center>
										<a href="<?php echo site_url("year"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<input type="submit" value="Save and Next" id="my_button" class="btn btn-primary">
									</center>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>