		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Details Academic Year</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
							<a href="<?php echo site_url("year"); ?>">Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_edit/$TahunID"); ?>">Add New Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_detail_form/$TahunID/$ProgramID"); ?>">Details Academic Year (ACAYE05)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-5">
					<div class="panel panel-default">
						<div class="panel-heading">
							Program
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Name</th>
											<th>Details</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											if($this->session->userdata('year_filter_jur') == "SC")
											{
												if($rowrecord)
												{
													foreach($rowrecord as $row)
													{
														if($ProdiID == $row->KursusSingkatID)
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr class='info'>";
														}
															echo "<td>$no</td>
																<td>$row->KursusSingkatID</td>
																<td>$row->Nama</td>
																<td><a href='".site_url("year/ptl_detail_form/$TahunID/$ProgramID/$row->KursusSingkatID")."' class='btn btn-warning btn-circle'>>></a></td>
															</tr>";
														$no++;
													}
												}
												else
												{
													echo "<tr>
															<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											}
											else
											{
												if($rowrecord)
												{
													foreach($rowrecord as $row)
													{
														if($ProdiID == $row->ProdiID)
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr class='info'>";
														}
															echo "<td>$no</td>
																<td>$row->ProdiID</td>
																<td>$row->Nama</td>
																<td><a href='".site_url("year/ptl_detail_form/$TahunID/$ProgramID/$row->ProdiID")."' class='btn btn-warning btn-circle'>>></a></td>
															</tr>";
														$no++;
													}
												}
												else
												{
													echo "<tr>
															<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											}
										?>
									</tbody>
								</table>
								<center>
									<a href="<?php echo site_url("year"); ?>" id="my_button" onclick="return confirm('Are you sure to finish this action?')" class="btn btn-success">Finish</a>
								</center>
							</div>
						</div>
					</div>
				</div>
				<?php
					if($ProdiID != "")
					{
				?>
						<div class="col-lg-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									Schedule Details
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php
											if($id != "")
											{
										?>
												<form role="form" action="<?php echo site_url("year/ptl_detail_update"); ?>" method="POST">
										<?php
											}
											else
											{
										?>
												<form role="form" action="<?php echo site_url("year/ptl_detail_insert"); ?>" method="POST">
										<?php
											}
										?>
											<div class="form-group">
												<label><font color="red">Name</font></label>
												<input type="text" name="nama" value="<?php echo $nama; ?>" class="form-control" required>
												<input type="hidden" name="id" value="<?php echo $id; ?>">
												<input type="hidden" name="TahunID" value="<?php echo $TahunID; ?>">
												<input type="hidden" name="ProgramID" value="<?php echo $ProgramID; ?>">
												<input type="hidden" name="ProdiID" value="<?php echo $ProdiID; ?>">
												<p class="help-block"></p>
											</div>
											<table class="table">
												<tr>
													<td>
														<div class="form-group">
															<label><font color="red">Start Date</font></label>
															<input type="text" name="tanggal_mulai" value="<?php echo $tanggal_mulai; ?>" id="datepickerSC1" class="form-control" required>
															<p class="help-block"></p>
														</div>
													</td>
													<td>
														<div class="form-group">
															<label><font color="red">End Date</font></label>
															<input type="text" name="tanggal_selesai" value="<?php echo $tanggal_selesai; ?>" id="datepickerSC1-End" class="form-control" required>
															<p class="help-block"></p>
														</div>
													</td>
												</tr>
												<tr>
													<td colspan="2" align="center">
														<?php
															if($id != "")
															{
														?>
																<button type="submit" class="btn btn-success">Update</button>
														<?php
															}
															else
															{
														?>
																<button type="submit" class="btn btn-primary">Save</button>
														<?php
															}
														?>
													</td>
												</tr>
											</table>
										</form>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Start Date</th>
													<th>End Date</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord2)
													{
														$no = 1;
														foreach($rowrecord2 as $row)
														{
															if($id == $row->id)
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
															echo "<td title='id : $row->id'>$no</td>
																	<td title='Created By : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited By : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$row->nama</td>
																	<td>$row->tanggal_mulai</td>
																	<td>$row->tanggal_selesai</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("year/ptl_detail_form/$TahunID/$ProgramID/$ProdiID/$row->id")."'>
																			<i class='fa fa-list'></i>
																		</a>";
												?>
																		<a class="btn btn-danger" href="<?php echo site_url("year/ptl_detail_delete/$TahunID/$ProgramID/$ProdiID/$row->id"); ?>" onclick="return confirm('Are you sure to delete this data?')">
																			<i class="fa fa-times"></i>
																		</a>
												<?php
																echo "</td>
																</tr>";
															$no++;
														}
													}
													else
													{
														echo "<tr>
																<td colspan='5'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>	
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
				<?php
					}
				?>
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>