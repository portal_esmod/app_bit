		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Participants</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("year"); ?>">Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_participant/$TahunID"); ?>">Participants (ACAYE02)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Students in this Academic Year <b><?php echo $TahunID; ?></b>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Status</th>
											<th>Student Name & ID</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$n = 1;
											if($rowrecord)
											{
												foreach($rowrecord as $row)
												{
													$MhswID = $row->MhswID;
													$m = $this->m_mahasiswa->PTL_select($MhswID);
													$StatusMhswID = "";
													$Nama = "<font color='red' title='No data in table `ac_mahasiswa`'>No data</font>";
													if($m)
													{
														$StatusMhswID = $m["StatusMhswID"];
														$Nama = $m["Nama"];
													}
													if(@$m["StatusMhswID"] == "A")
													{
														echo "<tr>";
													}
													else
													{
														echo "<tr class='danger'>";
													}
													echo "<td title='$row->KHSID'>$n</td>
														<td>";
													$keuangan = $row->Potongan + $row->Bayar;
													if($keuangan > 0)
													{
														$s = $this->m_status->PTL_select($StatusMhswID);
														$nama = "";
														if($s)
														{
															$nama = $s["Nama"];
														}
														echo $nama;
													}
													else
													{
														echo "<font color='red'><b>Upaid the financial</b></font>";
													}
													if($row->ProgramID == 'SC')
													{
														echo "</td>
																<td><b>$row->MhswID</b> - <a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a></td>
															</tr>";
													}
													else
													{
														echo "</td>
																<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a> - <a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$Nama</a></td>
															</tr>";
													}
													$n++;
												}
											}
										?>
									</tbody>
									<tfoot>
										<tr>
											<th>#</th>
											<th>Status</th>
											<th>Student Name & ID</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>