		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Calendar</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("year"); ?>">Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_edit/$TahunID"); ?>">Edit</a>
							>>
							<a href="<?php echo site_url("year/ptl_calendar"); ?>">Calendar</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("year/ptl_update"); ?>" method="POST">
										<iframe src="https://calendar.google.com/calendar/embed?title=Esmod%20Calendar&amp;height=600&amp;wkst=2&amp;bgcolor=%23ccffff&amp;src=c4g2ta2s2n7s3u5502500bavsc%40group.calendar.google.com&amp;color=%23A32929&amp;src=en.indonesian%23holiday%40group.v.calendar.google.com&amp;color=%235B123B&amp;ctz=Asia%2FJakarta" style="border:solid 1px #777" width="800" height="600" frameborder="0" scrolling="no"></iframe>
									<div class="col-lg-6">
										
									</div>
									<center>
										<a href="<?php echo site_url("year/ptl_edit/$TahunID"); ?>" class="btn btn-warning">Back</a>
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>