		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Academic Year</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("year"); ?>">Academic Year</a>
							>>
							<a href="<?php echo site_url("year/ptl_edit/$TahunID"); ?>">Edit Academic Year (ACAYE09)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php
									$jur = $ProgramID;
									if($jur == "REG"){ $j = "REG - Regular"; $d = "Three Years Program"; $jen = "D3"; }
									if($jur == "INT"){ $j = "INT - Intensive"; $d = "One Years Program"; $jen = "D1"; }
									if($jur == "SC"){ $j = "SC - Short Course"; $d = "Short Course Program"; $jen = "SC"; }
								?>
								<div class="col-lg-12">
									<div class="form-group">
										<!--<a href="<?php echo site_url("year/ptl_calendar/$TahunID"); ?>" target="_blank" class="btn btn-success">Go To Calendar</a>-->
										<p class="help-block"></p>
									</div>
								</div>
								<form role="form" action="<?php echo site_url("year/ptl_update"); ?>" method="POST">
									<div class="col-lg-12">
										<div class="alert alert-info">
											<a class="alert-link">Notes:
											<br/>
											<a class="alert-link"><font color="blue">Blue</font>: </a>Auto-filled.
											<a class="alert-link"><font color="red">Red</font>: </a>Must be filled.
											<a class="alert-link">Black: </a>Optional. <a href="<?php echo site_url("tutorial/ptl_detail/ACYR003"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="blue">Degree</font></label>
											<input readonly type="text" value="<?php echo $j; ?>" class="form-control">
											<input type="hidden" name="TahunID" value="<?php echo $TahunID; ?>" class="form-control">
											<input type="hidden" name="Jenjang2ID" value="<?php echo $jen; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Year Code</font></label>
											<input type="text" name="kode_tahun" value="<?php echo $Nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Curriculum</font></label>
											<select name="KurikulumID" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowkurikulum)
													{
														foreach($rowkurikulum as $row)
														{
															echo "<option value='$row->KurikulumID'";
															if($row->KurikulumID == $KurikulumID)
															{
																echo "selected";
															}
															echo ">$row->KurikulumID - $row->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization Process</label>
											<input type="text" name="Specialization" value="<?php echo $Specialization; ?>" id="datepickerSC2" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Payment Process</label>
											<input type="text" name="Payment" value="<?php echo $Payment; ?>" id="datepickerSC5" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Enrollment Process</label>
											<input type="text" name="Enrollment" value="<?php echo $Enrollment; ?>" id="datepickerSC1" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postponed</label>
											<input type="text" name="Postpone" value="<?php echo $Postpone; ?>" id="datepickerSC3" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Academic Process</label>
											<input type="text" name="Academic" value="<?php echo $Academic; ?>" id="datepickerSC4" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Questionnaire Process</label>
											<input type="text" name="Questionnaire" value="<?php echo $Questionnaire; ?>" id="datepickerSC6" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Evaluation Assessment</label>
											<input type="text" name="Evaluation" value="<?php echo $Evaluation; ?>" id="datepicker6" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Submission</label>
											<input type="text" name="Submission" value="<?php echo $Submission; ?>" id="datepicker7" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Job Training</label>
											<input type="text" name="JobTraining" value="<?php echo $JobTraining; ?>" id="datepicker8" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Session</label>
											<input type="text" name="JurySession" value="<?php echo $JurySession; ?>" id="datepicker9" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Day</label>
											<input type="text" name="JuryDay" value="<?php echo $JuryDay; ?>" id="datepicker10" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Exam</label>
											<input type="text" name="Exam" value="<?php echo $Exam; ?>" id="datepicker11" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Remedial</label>
											<input type="text" name="Remedial" value="<?php echo $Remedial; ?>" id="datepicker12" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="blue">Degree</font></label>
											<input readonly type="text" value="<?php echo $d; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Semester</font></label>
											<select name="Nama" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="ODD" <?php if(@$word1[2] == "ODD"){ echo "selected"; } ?>>ODD</option>
												<option value="EVEN" <?php if(@$word1[2] == "EVEN"){ echo "selected"; } ?>>EVEN</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Rating Code</label>
											<select name="id_rating_question_grup" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowrating)
													{
														foreach($rowrating as $row)
														{
															echo "<option value='$row->id_rating_question_grup'";
															if($row->id_rating_question_grup == $id_rating_question_grup)
															{
																echo "selected";
															}
															echo ">$row->id_rating_question_grup - $row->nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization End</label>
											<input type="text" name="SpecializationEnd" value="<?php echo $SpecializationEnd; ?>" id="datepickerSC2-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Payment End</label>
											<input type="text" name="PaymentEnd" value="<?php echo $PaymentEnd; ?>" id="datepickerSC5-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Enrollment End</label>
											<input type="text" name="EnrollmentEnd" value="<?php echo $EnrollmentEnd; ?>" id="datepickerSC1-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Postponed End</label>
											<input type="text" name="PostponeEnd" value="<?php echo $PostponeEnd; ?>" id="datepickerSC3-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Academic End</label>
											<input type="text" name="AcademicEnd" value="<?php echo $AcademicEnd; ?>" id="datepickerSC4-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Questionnaire End</label>
											<input type="text" name="QuestionnaireEnd" value="<?php echo $QuestionnaireEnd; ?>" id="datepickerSC6-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Evaluation End</label>
											<input type="text" name="EvaluationEnd" value="<?php echo $EvaluationEnd; ?>" id="datepicker18" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Submission End</label>
											<input type="text" name="SubmissionEnd" value="<?php echo $SubmissionEnd; ?>" id="datepicker19" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Job Training End</label>
											<input type="text" name="JobTrainingEnd" value="<?php echo $JobTrainingEnd; ?>" id="datepicker20" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Session End</label>
											<input type="text" name="JurySessionEnd" value="<?php echo $JurySessionEnd; ?>" id="datepicker21" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Jury Day End</label>
											<input type="text" name="JuryDayEnd" value="<?php echo $JuryDayEnd; ?>" id="datepicker22" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Exam End</label>
											<input type="text" name="ExamEnd" value="<?php echo $ExamEnd; ?>" id="datepicker23" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Remedial End</label>
											<input type="text" name="RemedialEnd" value="<?php echo $RemedialEnd; ?>" id="datepicker24" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
									</div>
									<center>
										<a href="<?php echo site_url("year"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<input type="submit" value="Save and Next" id="my_button" class="btn btn-primary">
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>