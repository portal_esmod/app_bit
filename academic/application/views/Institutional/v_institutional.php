		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Institutional Identity</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("institutional"); ?>">Institutional Identity</a>
                        </div>
                        <div class="panel-body">
								<div class="table-responsive"><div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of institutional identity.
								</div>
								<center><a href="<?php echo site_url("institutional/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Website</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>
																<td>$no</td>
																<td>$row->Kode</td>
																<td>$row->Nama_IN</td>
																<td><a href='$row->Website' target='_blank'>$row->Website</a></td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("institutional/ptl_edit/$row->Kode")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													else
													{
														echo "<tr>
																<td>$no</td>
																<td>$row->Kode</td>
																<td>$row->Nama_IN</td>
																<td><a href='$row->Website' target='_blank'>$row->Website</a></td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("institutional/ptl_edit/$row->Kode")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>