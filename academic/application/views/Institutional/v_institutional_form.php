		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("institutional"); ?>">Institutional Identity</a>
							>>
							<a href="<?php echo site_url("institutional/ptl_form"); ?>">Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("institutional/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="Kode" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Institutional Code</label>
											<input type="text" name="KodeInstitusi" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name (Indonesian)</label>
											<input type="text" name="Nama_IN" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name (English)</label>
											<input type="text" name="Nama_EN" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Institution / Departement</label>
											<input type="text" name="Yayasan" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Start Date</label>
											<input type="text" name="TglMulai" id="datepicker1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<input type="text" name="Alamat1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="Kota" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Post Code</label>
											<input type="int" name="KodePos" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Telephone</label>
											<input type="int" name="Telepon" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Fax</label>
											<input type="int" name="Fax" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="email" name="Email" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Website</label>
											<input type="text" name="Website" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Akta Number</label>
											<input type="text" name="NoAkta" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date Of Akta</label>
											<input type="text" name="TglAkta" id="datepicker2" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Validation Number</label>
											<input type="text" name="NoSah" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Validation Date</label>
											<input type="text" name="TglSah" id="datepicker3" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Logo</label>
											<input type="text" name="Logo" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("institutional"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>