		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lecturer Calendar</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("calendar"); ?>">Lecturer Calendar (ACALC01)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a lecturer calendar. <a href="<?php echo site_url("tutorial/ptl_detail/ACALC01"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("lecturer_calendar/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('lc_filter_jur');
														$add = "";
														if($cekjurusan == "INT")
														{
															$add = "O";
														}
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("lecturer_calendar/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('lc_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("lecturer_calendar/ptl_filter_dosen"); ?>" method="POST">
												<select name="cekdosen" title="Filter by Lecturer" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- TEACHERS --</option>
													<?php
														$cekdosen = $this->session->userdata('lc_filter_dosen');
														if($cekjurusan != "")
														{
															if($rowdosen)
															{
																foreach($rowdosen as $dsn)
																{
																	if($dsn->NA == "N")
																	{
																		echo "<option value='$dsn->Login'";
																		if($cekdosen == $dsn->Login)
																		{
																			echo "selected";
																		}
																		echo ">$dsn->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<br/>
								<?php
									if($cektahun != "")
									{
								?>
										<div id='calendar'></div>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>