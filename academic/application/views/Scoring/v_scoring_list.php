		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Scoring List</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("scoring"); ?>">Scoring</a>
							>>
							<a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>">Scoring List (SCORI04)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Subject Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Schedule ID</td>
										<td>:</td>
										<td><?php echo $JadwalID; ?></td>
									</tr>
									<tr>
										<td>Academic Years</td>
										<td>:</td>
										<td>
											<?php
												$t = $this->m_year->PTL_select($TahunID);
												echo $TahunID." - ".$t["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Program</td>
										<td>:</td>
										<td>
											<?php
												$add = "";
												if($ProgramID == "REG")
												{
													echo $ProgramID." - REGULAR";
												}
												if($ProgramID == "INT")
												{
													echo $ProgramID." - INTENSIVE";
													$add = "O";
												}
												if($ProgramID == "SC")
												{
													echo $ProgramID." - SHORT COURSE";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												$kelas = "";
												if($KelasID == 0)
												{
													$grup1 = explode(".",$KelasIDGabungan);
													$kelas1 = explode("^",@$grup1[0]);
													$kelas2 = explode("^",@$grup1[1]);
													$KelasID = @$kelas1[1];
													$k1 = $this->m_kelas->PTL_select_kelas($KelasID);
													$KelasID = @$kelas2[1];
													$k2 = $this->m_kelas->PTL_select_kelas($KelasID);
													$kls = @$kelas1[0].@$k1["Nama"]."/".@$kelas2[0].@$k2["Nama"];
													if($kls == "/")
													{
														echo "<font color='red'><b>Not set</b></font>";
													}
													else
													{
														echo $kls;
														$kelas = $kls;
													}
												}
												else
												{
													$k = $this->m_kelas->PTL_select_kelas($KelasID);
													if($add == "")
													{
														$add = $TahunKe;
													}
													echo $add.$k["Nama"];
													$kelas = $add.$k["Nama"];
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td title="<?php echo $SubjekID; ?>">
											<?php
												$s = $this->m_subjek->PTL_select($SubjekID);
												$JenisMKID = $s["JenisMKID"];
												$resmk = $this->m_jenis_mk->PTL_select($JenisMKID);
												$namamk = "";
												if($resmk)
												{
													$namamk = strtoupper($resmk['Nama']);
												}
												echo $s["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Type</td>
										<td>:</td>
										<td><?php echo $namamk; ?></td>
									</tr>
									<tr>
										<td>Teacher</td>
										<td>:</td>
										<td>
											<?php
												$d = $this->m_dosen->PTL_select($DosenID);
												$f1 = "";
												$f2 = "";
												if($d)
												{
													if($d["NA"] == "Y")
													{
														echo "<font color='red'><b title='This lecturer has resigned'>
															$d[Nama]
															</b></font>";
													}
													else
													{
														echo $d["Nama"];
													}
												}
												else
												{
													echo "";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td>
											<?php
												echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai);
											?>
										</td>
									</tr>
									<tr>
										<td>Total Standard Attendance</td>
										<td>:</td>
										<td><?php echo $RencanaKehadiran." session"; ?></td>
									</tr>
									<tr>
										<td>Maximum Absent</td>
										<td>:</td>
										<td><?php echo $MaxAbsen." session"; ?></td>
									</tr>
									<tr>
										<td>Created By</td>
										<td>:</td>
										<td><?php echo $login_buat; ?></td>
									</tr>
									<tr>
										<td>Created Date</td>
										<td>:</td>
										<td><?php echo $tanggal_buat; ?></td>
									</tr>
									<tr>
										<td>Edited By</td>
										<td>:</td>
										<td><?php echo $login_edit; ?></td>
									</tr>
									<tr>
										<td>Edited Date</td>
										<td>:</td>
										<td><?php echo $tanggal_edit; ?></td>
									</tr>
									<tr>
										<td>Short Link</td>
										<td>:</td>
										<td><a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-success">Attendance</a></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Notes</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>You can leave notes on student. <font color="red"><b>Notes can only be inputted in Attendance.</b></font>
								</div>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Student Name & ID</th>
											<th>Title</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowcatatan)
											{
												$n = 1;
												foreach($rowcatatan as $row)
												{
													$MhswID = $row->MhswID;
													$m = $this->m_mahasiswa->PTL_select($MhswID);
													$nama = "";
													if($m)
													{
														$nama = $m['Nama'];
													}
													echo "<tr>
															<td title='noteid : $row->noteid ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$n</td>
															<td>$row->MhswID - $nama</td>
															<td>$row->comment</td>
														</tr>";
													$n++;
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				if($rownilai != "")
				{
			?>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<center>Attendance Score</center>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>You can edit Attendance Score. This page appear only for EXTRA CURRICULAR.
										</div>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>Attendance ID</th>
													<th>Attendance Status</th>
													<th>Score</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rownilai)
													{
														$no = 0;
														foreach($rownilai as $row)
														{
															$JenisPresensiID = $row->JenisPresensiID;
															$res = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
															$nama = "";
															if($res)
															{
																$nama = $res["Nama"];
															}
															$no++;
															echo "<tr>
																	<td align='center' title='Nilai3ID : $row->Nilai3ID'>$row->JenisPresensiID</td>
																	<td>$nama</td>
																	<td align='right'>$row->Score</td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("attendance_score/ptl_edit/$row->Nilai3ID/SCORING/$JadwalID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																	</td>
																</tr>";
														}
													}
													else
													{
														echo "<tr>
																<td colspan='4' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php
				}
			?>
			<div class="row">
				<div class="col-lg-5">
					<div class="panel panel-default">
						<div class="panel-heading">Projects</div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The project will only appear if it has been set on attendance.
								</div>
								<center>
									<?php
										$sequence = $this->session->userdata('score_filter_sequence');
										if($sequence == "")
										{
									?>
											<a href="<?php echo site_url("scoring/ptl_filter_sequence/$JadwalID"); ?>" class="btn btn-info">Sort By Sequence</a>
									<?php
										}
										else
										{
									?>
											<a href="<?php echo site_url("scoring/ptl_filter_name/$JadwalID"); ?>" class="btn btn-info">Sort By Name</a>
									<?php
										}
									?>
								</center>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Name</th>
											<th>Participants</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$namamk = "";
											if($rowmk)
											{
												$no = 1;
												foreach($rowmk as $row)
												{
													$CekMKID = $row->MKID;
													$rescekmk = $this->m_presensi->PTL_select_cek_mk_scoring($JadwalID,$CekMKID);
													if($rescekmk)
													{
														if($MKID == $CekMKID)
														{
															echo "<tr class='danger'>";
															$namamk = $row->Nama;
														}
														else
														{
															echo "<tr class='info'>";
														}
															echo "<td>$no</td>
																<td>$row->MKID</td>
																<td>$row->Nama</td>
																<td><a href='".site_url("scoring/ptl_list/$JadwalID/$row->MKID")."' class='btn btn-warning btn-circle'>>></a></td>
															</tr>";
														$no++;
													}
												}
											}
											else
											{
												echo "<tr>
														<td colspan='5'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-7">
					<div class="panel panel-default">
						<div class="panel-heading">
							List of Students
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<?php
									if($MKID == "")
									{
								?>
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>The following is the score of attendance.
										</div>
								<?php
									}
									else
									{
								?>
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>The following is the score of project <font color="green"><b><?php echo $namamk; ?></b></font>.
											<br/>
											<a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>" class="btn btn-primary">Clear Project Selection</a>
										</div>
								<?php
									}
									if($statusmk == "NO")
									{
										echo "<center><font color='red'><h3>This project has not been set at Attendance.</h3></font></center>";
									}
									else
									{
										if($MKID == "")
										{
											echo "<h4><b><p align='center'><font color='red'>You have to choose the project to be assessed.</font></p></b></h4>";
										}
										else
										{
								?>
											<form role="form" action="<?php echo site_url("scoring/ptl_list_scoring_set"); ?>" method="POST">
												<table class="table">
													<thead>
														<tr>
															<th>#</th>
															<th colspan="2">Student Name & ID</th>
															<th>Attendance Score</th>
															<th>Assignment Score</th>
															<th>Score</th>
														</tr>
													</thead>
													<tbody>
														<?php
															$tot = 0;
															$no = 1;
															$sebelumnya = "";
															$status_show = "";
															if($rowrecord)
															{
																foreach($rowrecord as $row)
																{
																	$KRSID = $row->KRSID;
																	$krs2 = $this->m_krs2->PTL_select_scoring_not_null($KRSID,$MKID);
																	if($krs2)
																	{
																		if($krs2['show'] == "Y")
																		{
																			$status_show = "Y";
																		}
																		$MhswID = $row->MhswID;
																		$r = $this->m_mahasiswa->PTL_select($MhswID);
																		$foto = "foto_umum/user.jpg";
																		$foto_preview = "foto_umum/user.jpg";
																		$nama = "";
																		if($r)
																		{
																			if($r["Foto"] != "")
																			{
																				$foto = "foto_mahasiswa/".$r["Foto"];
																				$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																				if($exist)
																				{
																					$foto = $foto;
																					$source_photo = base_url("ptl_storage/$foto");
																					$info = pathinfo($source_photo);
																					$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																					$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																					if($exist1)
																					{
																						$foto_preview = $foto_preview;
																					}
																					else
																					{
																						$foto_preview = $foto;
																					}
																				}
																				else
																				{
																					$foto = "foto_umum/user.jpg";
																					$foto_preview = "foto_umum/user.jpg";
																				}
																			}
																			$nama = $r["Nama"];
																		}
																		$TahunID = $row->TahunID;
																		$k = $this->m_khs->PTL_select_att($MhswID,$TahunID);
																		$StatusMhswID = "";
																		$suspend = "";
																		if($k)
																		{
																			$StatusMhswID = $k["StatusMhswID"];
																			$suspend = $k["suspend"];
																		}
																		$s = $this->m_status->PTL_select($StatusMhswID);
																		if($StatusMhswID == "A")
																		{
																			$status = "<font color='green'><b>$s[Nama]</b></font>";
																		}
																		else
																		{
																			$status = "<font color='red'><b>$s[Nama]</b></font>";
																		}
																		$rowkrs2 = $this->m_krs2->PTL_all_select_scoring($KRSID,$MKID);
																		$Nilai = 0;
																		$week = "";
																		$totNilai = 0;
																		$nkrs2 = 0;
																		if($rowkrs2)
																		{
																			foreach($rowkrs2 as $rkrs2)
																			{
																				$PresensiID = $rkrs2->PresensiID;
																				$resp = $this->m_presensi->PTL_select($PresensiID);
																				if($resp)
																				{
																					if($resp['NA'] == "N")
																					{
																						$totNilai = $totNilai + $rkrs2->Nilai;
																						$week .= "Week $resp[Pertemuan] ($PresensiID): ".number_format($rkrs2->Nilai,2,'.','')." ~ ";
																						$nkrs2++;
																					}
																				}
																			}
																			if($nkrs2 == 0)
																			{
																				$Nilai = 0;
																			}
																			else
																			{
																				if($nkrs2 < 0)
																				{
																					$Nilai = 0;
																				}
																				else
																				{
																					if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																					{
																						$Nilai = $totNilai / $nkrs2; // Pakai rata - rata
																					}
																					else
																					{
																						$Nilai = $totNilai;
																					}
																				}
																			}
																		}
																		$week .= " Average : ".number_format($Nilai,2,'.','');
																		if($krs2["AdditionalNilai"] == 0)
																		{
																			$tn = $Nilai;
																		}
																		else
																		{
																			$tn = $Nilai + $krs2["AdditionalNilai"];
																		}
																		$MhswID = $row->MhswID;
																		$PresensiID = $krs2['PresensiID'];
																		$respresensi = $this->m_presensi_mahasiswa->PTL_select_att($JadwalID,$PresensiID,$MhswID);
																		$totalpresensi = "";
																		$NRemove = "N";
																		$JenisPresensiID = "";
																		if($respresensi)
																		{
																			$JenisPresensiID = $respresensi['JenisPresensiID'];
																			if(($respresensi['JenisPresensiID'] == "") AND ($respresensi['Removed'] == "N") AND ($respresensi['NA'] == "N"))
																			{
																				$totalpresensi = 0;
																			}
																			if($respresensi['Removed'] == "Y")
																			{
																				$NRemove = 'Y';
																			}
																		}
																		if(($nkrs2 == 1) AND ($NRemove == "Y"))
																		{
																			
																		}
																		else
																		{
																			if($StatusMhswID == "A")
																			{
																				$Finance = "";
																				if($suspend == "Y")
																				{
																					$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																				}
																				echo "<tr class='info'>
																						<td title='KRSID : $row->KRSID ~ KRS2ID : $krs2[KRS2ID] ~ PresensiID:  $PresensiID'>$no</td>
																						<td>
																							<a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																								<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																							</a>
																						</td>
																						<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>";
																				$NoteAtt = "";
																				if(($nkrs2 == 1) AND ($JenisPresensiID == ""))
																				{
																					$NoteAtt = "<br/><font color='red'>Attendance not set</font>";
																				}
																				if($rownilai != "")
																				{
																					if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																					{
																						if(($JenisPresensiID == "E") OR ($JenisPresensiID == "S"))
																						{
																							echo "<td title='$week'>$JenisPresensiID : -</td>";
																						}
																						else
																						{
																							echo "<td title='$week'>$JenisPresensiID : ".number_format($Nilai,2,'.','')."</td>";
																						}
																					}
																					else
																					{
																						echo "<td title='$week'>$JenisPresensiID : ".number_format($Nilai,2,'.','')."</td>";
																					}
																				}
																				else
																				{
																					echo "<td title='$week'>".number_format($Nilai,2,'.','')."</td>";
																				}
																					echo "<td>";
																							if(($nkrs2 == 1) AND ($JenisPresensiID == ""))
																							{
																								echo "<font color='red'>Attendance not set</font>";
																								echo "<input type='hidden' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																							}
																							else
																							{
																								if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																								{
																									if(($JenisPresensiID == "E") OR ($JenisPresensiID == "S"))
																									{
																										echo "-<input type='hidden' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																									}
																									else
																									{
																										echo "<input type='number' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																									}
																								}
																								else
																								{
																									echo "<input type='number' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																								}
																							}
																					echo "<input type='hidden' name='KRS2ID$no' value='$krs2[KRS2ID]'>
																							<input type='hidden' name='JenisPresensiID$no' value='$totalpresensi'>
																							<input type='hidden' name='PresensiID' value='$PresensiID'>
																							<input type='hidden' name='CekPresensiID$no' value='$respresensi[PresensiID]'>
																						</td>
																						<td>".number_format($tn,2,'.','')."</td>
																					</tr>";
																				$MKID = $krs2['MKID'];
																				$no++;
																				$tot++;
																			}
																		}
																	}
																	else
																	{
																		$krs2 = $this->m_krs2->PTL_select_scoring($KRSID,$MKID);
																		if($krs2)
																		{
																			if($krs2['show'] == "Y")
																			{
																				$status_show = "Y";
																			}
																			$MhswID = $row->MhswID;
																			$r = $this->m_mahasiswa->PTL_select($MhswID);
																			$foto = "foto_umum/user.jpg";
																			$foto_preview = "foto_umum/user.jpg";
																			$nama = "";
																			if($r)
																			{
																				if($r["Foto"] != "")
																				{
																					$foto = "foto_mahasiswa/".$r["Foto"];
																					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																					if($exist)
																					{
																						$foto = $foto;
																						$source_photo = base_url("ptl_storage/$foto");
																						$info = pathinfo($source_photo);
																						$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																						$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																						if($exist1)
																						{
																							$foto_preview = $foto_preview;
																						}
																						else
																						{
																							$foto_preview = $foto;
																						}
																					}
																					else
																					{
																						$foto = "foto_umum/user.jpg";
																						$foto_preview = "foto_umum/user.jpg";
																					}
																				}
																				$nama = $r["Nama"];
																			}
																			$TahunID = $row->TahunID;
																			$k = $this->m_khs->PTL_select_att($MhswID,$TahunID);
																			$StatusMhswID = "";
																			$suspend = "";
																			if($k)
																			{
																				$StatusMhswID = $k["StatusMhswID"];
																				$suspend = $k["suspend"];
																			}
																			$s = $this->m_status->PTL_select($StatusMhswID);
																			if($StatusMhswID == "A")
																			{
																				$status = "<font color='green'><b>$s[Nama]</b></font>";
																			}
																			else
																			{
																				$status = "<font color='red'><b>$s[Nama]</b></font>";
																			}
																			$rowkrs2 = $this->m_krs2->PTL_all_select_scoring($KRSID,$MKID);
																			$Nilai = 0;
																			$week = "";
																			$totNilai = 0;
																			$nkrs2 = 0;
																			if($rowkrs2)
																			{
																				foreach($rowkrs2 as $rkrs2)
																				{
																					$PresensiID = $rkrs2->PresensiID;
																					$resp = $this->m_presensi->PTL_select($PresensiID);
																					if($resp)
																					{
																						if($resp['NA'] == "N")
																						{
																							$totNilai = $totNilai + $rkrs2->Nilai;
																							$week .= "Week $resp[Pertemuan] ($PresensiID): ".number_format($rkrs2->Nilai,2,'.','')." ~ ";
																							$nkrs2++;
																						}
																					}
																				}
																				if($nkrs2 == 0)
																				{
																					$Nilai = 0;
																				}
																				else
																				{
																					if($nkrs2 < 0)
																					{
																						$Nilai = 0;
																					}
																					else
																					{
																						if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																						{
																							$Nilai = $totNilai / $nkrs2; // Pakai rata - rata
																						}
																						else
																						{
																							$Nilai = $totNilai;
																						}
																					}
																				}
																			}
																			$week .= " Average : ".number_format($Nilai,2,'.','');
																			if($krs2["AdditionalNilai"] == 0)
																			{
																				$tn = $Nilai;
																			}
																			else
																			{
																				$tn = $Nilai + $krs2["AdditionalNilai"];
																			}
																			$MhswID = $row->MhswID;
																			$PresensiID = $krs2['PresensiID'];
																			$respresensi = $this->m_presensi_mahasiswa->PTL_select_att($JadwalID,$PresensiID,$MhswID);
																			$totalpresensi = "";
																			$NRemove = 'N';
																			if($respresensi)
																			{
																				$JenisPresensiID = $respresensi['JenisPresensiID'];
																				if(($respresensi['JenisPresensiID'] == "") AND ($respresensi['Removed'] == "N") AND ($respresensi['NA'] == "N"))
																				{
																					$totalpresensi = 0;
																				}
																				if($respresensi['Removed'] == "Y")
																				{
																					$NRemove = 'Y';
																				}
																			}
																			if(($nkrs2 <= 1) AND ($NRemove == "Y"))
																			{
																				
																			}
																			else
																			{
																				if($StatusMhswID == "A")
																				{
																					$Finance = "";
																					if($suspend == "Y")
																					{
																						$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																					}
																					echo "<tr class='info'>
																							<td title='KRSID : $row->KRSID ~ KRS2ID : $krs2[KRS2ID] ~ PresensiID:  $PresensiID'>$no</td>
																							<td>
																								<a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																									<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																								</a>
																							</td>
																							<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>";
																					if($rownilai != "")
																					{
																						if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																						{
																							if(($JenisPresensiID == "E") OR ($JenisPresensiID == "S"))
																							{
																								echo "<td title='$week'>$JenisPresensiID : -</td>";
																							}
																							else
																							{
																								echo "<td title='$week'>$JenisPresensiID : ".number_format($Nilai,2,'.','')."</td>";
																							}
																						}
																						else
																						{
																							echo "<td title='$week'>$JenisPresensiID : ".number_format($Nilai,2,'.','')."</td>";
																						}
																					}
																					else
																					{
																						echo "<td title='$week'>".number_format($Nilai,2,'.','')."</td>";
																					}
																						echo "<td>";
																								if(($nkrs2 == 1) AND ($JenisPresensiID == ""))
																								{
																									echo "<font color='red'>Attendance not set</font>";
																									echo "<input type='hidden' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																								}
																								else
																								{
																									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																									{
																										if(($JenisPresensiID == "E") OR ($JenisPresensiID == "S"))
																										{
																											echo "-<input type='hidden' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																										}
																										else
																										{
																											echo "<input type='number' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																										}
																									}
																									else
																									{
																										echo "<input type='number' step='0.01' min='-20' max='20' name='AdditionalNilai$no' value='$krs2[AdditionalNilai]' style='width:70px;text-align:right;'>";
																									}
																								}
																						echo "<input type='hidden' name='KRS2ID$no' value='$krs2[KRS2ID]'>
																								<input type='hidden' name='JenisPresensiID$no' value='$totalpresensi'>
																								<input type='hidden' name='PresensiID' value='$PresensiID'>
																								<input type='hidden' name='CekPresensiID$no' value='$respresensi[PresensiID]'>
																							</td>
																							<td>";
																								if(($JenisMKID == "5") OR ($JenisMKID == "25"))
																								{
																									if(($JenisPresensiID == "E") OR ($JenisPresensiID == "S"))
																									{
																										echo "-";
																									}
																									else
																									{
																										echo number_format($tn,2,'.','');
																									}
																								}
																								else
																								{
																									echo number_format($tn,2,'.','');
																								}
																						echo "</td>
																						</tr>";
																					$MKID = $krs2['MKID'];
																					$no++;
																					$tot++;
																				}
																			}
																		}
																	}
																}
																echo "<input type='hidden' name='TahunID' value='$TahunID'>";
																echo "<input type='hidden' name='MKID' value='$MKID'>";
																echo "<input type='hidden' name='JadwalID' value='$JadwalID'>";
																echo "<input type='hidden' name='total' value='".($no -1)."'>";
																if($tot == 0)
																{
																	if($MKID == "")
																	{
																		echo "<tr>
																				<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																			</tr>
																			<tr>
																				<td colspan='6'><h4><b><p align='center'><font color='red'><h3>This subject has no additional attendance. Please select a project on the left side.</h3></font></p></b></h4></td>
																			</tr>";
																	}
																	else
																	{
																		echo "<tr>
																				<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																			</tr>
																			<tr>
																				<td colspan='6'><h4><b><p align='center'><font color='red'><h3>If you want to input score for each project in system, pelase update the attendance for the project first.</h3></font></p></b></h4></td>
																			</tr>";
																	}
																}
															}
															else
															{
																echo "<tr>
																		<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																	</tr>";
																$tot = 0;
															}
														?>
													</tbody>
												</table>
												<?php
													if($tot > 0)
													{
												?>
														<center>
															<b>Finalize?</b>
															<input type="checkbox" name="show" value="Y" class="form-control" <?php if($status_show == "Y"){ echo "checked"; } ?>>
															<br/>
															<a href="<?php echo site_url("scoring"); ?>" class="btn btn-warning">Back</a>
															<input type="submit" value="Save" id="my_button" class="btn btn-primary">
														</center>
												<?php
													}
												?>
											</form>
								<?php
										}
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>