		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Interview</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("interview"); ?>">Interview</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is a list of students who will interview. <a href="<?php echo site_url("tutorial/ptl_detail/INTV001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_periode"); ?>" method="POST">
													<select name="cekperiode" title="Filter by Period" class="form-control" onchange="this.form.submit()">
														<option value=''>-- PERIOD --</option>
														<?php
															if($periode)
															{
																$cekperiode = $this->session->userdata('int_filter_periode');
																foreach($periode as $per)
																{
																	echo "<option value='".$per->PMBPeriodID."'";
																	if($cekperiode == $per->PMBPeriodID)
																	{
																		echo "selected";
																	}
																	echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_marketing"); ?>" method="POST">
													<select name="cekmarketing" title="Filter by Marketing Staff" class="form-control" onchange="this.form.submit()">
														<option value=''>-- MARKETING --</option>
														<?php
															if($marketing)
															{
																$cekmarketing = $this->session->userdata('int_filter_marketing');
																echo "<option value='OLREG'";
																if($cekmarketing == "OLREG")
																{
																	echo "selected";
																}
																echo ">OLREG - ONLINE REGISTRATION</option>";
																foreach($marketing as $mar)
																{
																	$word = explode(" ",$mar->nama);
																	echo "<option value='".$word[0]."'";
																	if($cekmarketing == $word[0])
																	{
																		echo "selected";
																	}
																	echo ">".$word[0]." - $mar->nama</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_type"); ?>" method="POST">
													<select name="cektype" title="Filter by Type" class="form-control" onchange="this.form.submit()">
														<option value=''>-- TYPE --</option>
														<?php
															$cektype = $this->session->userdata('int_filter_type');
															echo "<option value='HOT'"; if($cektype == "HOT") { echo "selected"; } echo ">HOT - Potensial</option>";
															echo "<option value='USUAL'"; if($cektype == "USUAL") { echo "selected"; } echo ">USUAL - Biasa</option>";
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
										</tr>
										<tr>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_status"); ?>" method="POST">
													<select name="cekstatus" title="Filter by Status" class="form-control" onchange="this.form.submit()">
														<option value=''>-- STATUS --</option>
														<?php
															$cekstatus = $this->session->userdata('int_filter_status');
															echo "<option value='Y'"; if($cekstatus == "Y") { echo "selected"; } echo ">STEP DOWN</option>";
															echo "<option value='N'"; if($cekstatus == "N") { echo "selected"; } echo ">NOT STEP DOWN</option>";
															echo "<option value='P'"; if($cekstatus == "P") { echo "selected"; } echo ">POSTPONE</option>";
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_program"); ?>" method="POST">
													<select name="cekpogram" title="Filter by Program" class="form-control" onchange="this.form.submit()">
														<option value=''>-- PROGRAM --</option>
														<?php
															$cekpogram = $this->session->userdata('int_filter_program');
															echo "<option value='INT'"; if($cekpogram == "INT") { echo "selected"; } echo ">INT - INTENSIVE</option>";
															if($programd1)
															{
																foreach($programd1 as $d1)
																{
																	echo "<option value='$d1->ProdiID'";
																	if($cekpogram == $d1->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																}
															}
															echo "<option value='REG'"; if($cekpogram == "REG") { echo "selected"; } echo ">REG - REGULAR</option>";
															if($programd3)
															{
																foreach($programd3 as $d3)
																{
																	echo "<option value='$d3->ProdiID'";
																	if($cekpogram == strtoupper($d3->ProdiID))
																	{
																		echo "selected";
																	}
																	echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																}
															}
															echo "<option value='SC'"; if($cekpogram == "SC") { echo "selected"; } echo ">SC - SHORT COURSE</option>";
															if($shortcourse)
															{
																foreach($shortcourse as $sc)
																{
																	echo "<option value='$sc->KursusSingkatID'";
																	if($cekpogram == $sc->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
											<th><p valign="middle" align="center">
												<form action="<?php echo site_url("interview/ptl_filter_int_tahun"); ?>" method="POST">
													<select name="cektahun" title="Filter by Year" class="form-control" onchange="this.form.submit()">
														<option value=''>-- YEAR --</option>
														<?php
															if($tahun)
															{
																$cektahun = $this->session->userdata('int_filter_tahun');
																foreach($tahun as $thn)
																{
																	echo "<option value='$thn->tahun'";
																	if($cektahun == $thn->tahun)
																	{
																		echo "selected";
																	}
																	echo ">$thn->tahun</option>";
																}
															}
														?>
													</select>
													<noscript><input type="submit" value="Submit"></noscript>
												</form>
											</p></th>
										</tr>
									</thead>
								</table>
								<br/>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>NO</th>
											<th>PMB ID</th>
											<th>STAGE</th>
											<th>NAME</th>
											<th>ANSWER TEST</th>
											<th>SCORE</th>
											<th>GRADE</th>
											<th>INTERVIEW</th>
											<th>TEST DATE</th>
											<th>ACTION</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->LulusUjian == "N")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														if($row->LulusUjian == "I")
														{
															echo "<tr class='warning'>";
														}
														else
														{
															echo "<tr class='success'>";
														}
													}
														echo "<td>$no</td>
															<td><p align='center'>$row->PMBID</p></td>
															<td>$row->StatusAplikanID</td>
															<td>$row->Nama</td>
															<td>
															<p align='center'>";
														$h = "-7";
														$hm = $h * 60;
														$ms = $hm * 60;
														if(($row->akhir_ujian <= gmdate("m/d/Y H:i", time()-($ms))) AND ($row->ujian == ""))
														{
															echo "<a href='".site_url("interview/ptl_test_time/$row->AplikanID")."' target='_blank' class='btn btn-danger btn-sm btn-round btn-line' >EXPIRED, GIVE MORE TIME?</a>";
														}
														else
														{
															if($row->ujian == '1')
															{
																echo "<a href='".site_url("interview/ptl_test_download/$row->file_ujian1")."' title='$row->file_ujian1' class='btn btn-danger' >".substr($row->file_ujian1,8,13)."</a>";
															}
															else
															{
																echo "<a href='".site_url("interview/ptl_test_view/$row->AplikanID")."' class='btn btn-danger' >VIEW RESULT</a>";
															}
														}
														echo "</p>
															<td><p align='center'>";
																// if(($row->LulusUjian == "N") OR ($row->LulusUjian == "I"))
																if($row->NilaiUjian <= '0')
																{
																	echo "<form method='POST' action='".site_url("interview/ptl_test_score")."'>
																			<input type='hidden' name='AplikanID' value='$row->AplikanID' />
																			<input type='number' name='NilaiUjian' step='0.1' min='1' max='20' title='Range (0 - 20)' value='$row->NilaiUjian' style='width:40px;text-align:right;' />
																			<input type='submit' value='SET' class='btn btn-success'/>
																		</form>";
																}
																else
																{
																	echo $row->NilaiUjian;
																}
															echo "</p>
															</td>
															<td><p align='center'>$row->GradeNilai</p></td>
															<td>
																<p align='center'>";
																if($row->hasil_interview == "N")
																{
																	echo "<a href='".base_url()."interview/ptl_edit/$row->AplikanID' target='_blank' title='Edit this Prospect Interview' class='btn btn-danger'>$row->hasil_interview</a>";
																}
																else
																{
																	echo "<a href='".base_url()."interview/ptl_edit/$row->AplikanID' target='_blank' title='Edit this Prospect Interview' class='btn btn-success'>$row->hasil_interview</a>";
																}
															echo "</p>
															</td>
															<td><p align='center'>$row->TanggalUjian</p></td>
															<td>
																<a href='".site_url("interview/ptl_applicant_edit/$row->AplikanID")."' title='Detail this Prospect' class='btn btn-primary'><i class='fa fa-list'></i></a>
															";
										?>
																<a href="<?php echo site_url("interview/ptl_test_delete/$row->AplikanID"); ?>" onclick="return confirm('Are you sure to remove TEST with APPLICANT ID <?php echo $row->PMBID." - ".$row->Nama; ?>?')" title="Delete this Prospect Test" class="btn btn-danger"><i class="fa fa-times"></i></a>
										<?php
														echo "</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>