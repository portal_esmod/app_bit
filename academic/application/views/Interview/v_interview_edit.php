		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Set Interview</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("interview"); ?>">Interview</a>
							>>
                            <a href="<?php echo site_url("interview/ptl_edit/$AplikanID"); ?>">Set Interview</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php
									$jur = $this->session->userdata('year_filter_jur');
									if($jur == "REG"){ $j = "REG - Regular"; $d = "Three Years Program"; $jen = "D3"; }
									if($jur == "INT"){ $j = "INT - Intensive"; $d = "One Years Program"; $jen = "D1"; }
									if($jur == "SC"){ $j = "SC - Short Course"; $d = "Short Course Program"; $jen = "SC"; }
								?>
								<?php echo form_open_multipart('interview/ptl_update',array('class' => 'form-horizontal')); ?>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Applicant ID</label>
										<div class="col-lg-8">
											<input readonly type="text" name="AplikanID" value="<?php echo $AplikanID; ?>" placeholder="" class="form-control" />
											<input type="hidden" name="Email" value="<?php echo $Email; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Name</label>
										<div class="col-lg-8">
											<input readonly type="text" name="Nama" value="<?php echo $Nama; ?>" placeholder="" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Question</label>
										<div class="col-lg-8">
											<?php
												if($wawancara)
												{
													foreach($wawancara as $row)
													{
														echo $row->pertanyaan." (".$row->standard.")<br/>";
													}
												}
											?>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Note Interview</label>
										<div class="col-lg-8">
											<textarea name="nilai_interview" col="3" class="form-control"><?php echo $nilai_interview; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4">Pass?</label>
										<div class="col-lg-8">
											<input type="checkbox" name="hasil_interview" value="Y" <?php if($hasil_interview == "Y"){echo "checked";} ?>> Yes
										</div>
									</div>
									<div class="form-group">
										<label for="text1" class="control-label col-lg-4"></label>
										<div class="col-lg-8">
											<button type="reset" class="btn btn-danger">RESET</button>
											<button type="submit" class="btn btn-primary">SAVE</button>
										</div>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>