		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pending Payment (Current Students)</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("pending_old"); ?>">Pending Payment (Current Students)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is pending payment for current students. <font color="red"><b>For more information, please contact Finance Team.</b></font>
								</div>
								<table class="table table-bordered table-striped">
									<tr class="info">
										<td>
											<form action="<?php echo site_url("pending_old/ptl_filter_pending_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('academic_filter_pending_old_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("pending_old/ptl_filter_pending_prodi"); ?>" method="POST">
												<select name="cekprodi" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi = $this->session->userdata('academic_filter_pending_old_prodi');
														if($cekjurusan == "INT")
														{
															if($programd1)
															{
																foreach($programd1 as $d1)
																{
																	echo "<option value='$d1->ProdiID'";
																	if($cekprodi == $d1->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "REG")
														{
															if($programd3)
															{
																foreach($programd3 as $d3)
																{
																	echo "<option value='$d3->ProdiID'";
																	if($cekprodi == strtoupper($d3->ProdiID))
																	{
																		echo "selected";
																	}
																	echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "SC")
														{
															if($shortcourse)
															{
																foreach($shortcourse as $sc)
																{
																	echo "<option value='$sc->KursusSingkatID'";
																	if($cekprodi == $sc->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("pending_old/ptl_filter_pending_tahun"); ?>" method="POST">
												<select name="cektahun" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														$cektahun = $this->session->userdata('academic_filter_pending_old_tahun');
														$GanjilGenap = '';
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	echo "<option value='$rt->TahunID'";
																	if($cektahun == $rt->TahunID)
																	{
																		echo "selected";
																		$GanjilGenap = $rt->Nama;
																	}
																	echo ">$rt->TahunID - $rt->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("pending_old/ptl_filter_pending_tahun_aktif"); ?>" method="POST">
												<select name="cektahunaktif" title="Filter by SIN Code" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- SIN CODE --</option>
													<?php
														$cektahunaktif = $this->session->userdata('academic_filter_pending_old_tahun_aktif');
														if($cekjurusan != "")
														{
															if($tahunaktif)
															{
																foreach($tahunaktif as $rt)
																{
																	echo "<option value='$rt->tahun'";
																	if($cektahunaktif == $rt->tahun)
																	{
																		echo "selected";
																	}
																	echo ">$rt->tahun</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>SIN</th>
											<th>Name</th>
											<th>Bills</th>
											<th>Discounts</th>
											<th>Paid</th>
											<th>Remaining</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($cektahunaktif != "")
											{
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														$MhswID = $row->MhswID;
														$cekmahasiswa = $this->m_khs->PTL_select_cektahun($MhswID);
														if($cekmahasiswa)
														{
															
														}
														else
														{
															$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
															echo "
																<tr>
																	<td>$no</td>
																	<td>$row->MhswID</td>
																	<td>$resmhsw[Nama]</td>
																	<td><div style='text-align:right;'>".formatRupiah($row->Biaya)."</div></td>
																	<td><div style='text-align:right;'>".formatRupiah($row->Potongan)."</div></td>
																	<td><div style='text-align:right;'>".formatRupiah($row->Bayar)."</div></td>
																	<td><div style='text-align:right;'>".formatRupiah($row->Biaya - $row->Potongan - $row->Bayar)."</div></td>
																</tr>
																";
															$no++;
														}
													}
												}
												else
												{
													echo "<tr>
															<td colspan='7' align='center'><font color='red'><b>No data available</b></font></td>
														</tr>";
												}
											}
											else
											{
												echo "<tr>
														<td colspan='7' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>