		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <a href="<?php echo site_url("notif"); ?>">Notif</a>
							>>
							<a href="<?php echo site_url("notif/ptl_detail/$kodelog"); ?>">Detail</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("notif/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>From</label>
											<input readonly type="text" name="dari" value="<?php echo $dari; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>To</label>
											<input readonly type="text" name="untuk" value="<?php echo $untuk; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Message</label>
											<textarea readonly name="aktivitas" class="form-control"><?php echo $aktivitas; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date</label>
											<input readonly type="text" name="tanggal" value="<?php echo $tanggal; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Status</label>
											<input readonly type="text" name="status" value="<?php echo $status; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("notif"); ?>" class="btn btn-warning">Back</a>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>