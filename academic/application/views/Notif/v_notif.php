		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Notification</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("notif"); ?>">Notification</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-danger">
									 <a class="alert-link">Notes: </a>The following is a list of notification.
								</div>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>From</th>
											<th>To</th>
											<th>Message</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->presenter != "")
													{
														$word1 = explode("_",$row->dari);
														$dari = $word1[1];
														$word2 = explode("_",$row->untuk);
														$untuk = $word2[1];
														$kata = substr($row->aktivitas,0,20)."...";
														if($row->status == "UNREAD")
														{
															echo "<tr class='danger'>
																	<td>$no</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("notif/ptl_detail/$row->kodelog")."'>
																			<i class='fa fa-pencil'></i>
																		</a>
																		$dari
																	</td>
																	<td>$untuk</td>
																	<td>$kata</td>
																	<td>$row->tanggal</td>
																</tr>";
														}
														else
														{
															echo "<tr>
																	<td>$no</td>
																	<td>
																		<a class='btn btn-info' href='".site_url("notif/ptl_detail/$row->kodelog")."'>
																			<i class='fa fa-pencil'></i>
																		</a>
																		$dari
																	</td>
																	<td>$untuk</td>
																	<td>$kata</td>
																	<td>$row->tanggal</td>
																</tr>";
														}
														$no++;
													}
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>