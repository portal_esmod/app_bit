		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Performance Report</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("rank"); ?>">Student Performance Report (EVSPR02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All student attendance problem will show in this page. <a href="<?php echo site_url("tutorial/ptl_detail/ENRL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table>
									<tr>
										<!--<td>
											<form action="<?php echo site_url("rank/ptl_filter_gpa"); ?>" method="POST">
												<select name="cekgpa" title="Filter by GPA/Attendance" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- SCORE --</option>
													<?php
														$cekgpa = $this->session->userdata('spr_filter_gpa');
														echo "<option value='GPA'"; if($cekgpa == 'GPA'){ echo "selected"; } echo ">GPA</option>";
														echo "<option value='ATTENDANCE'"; if($cekgpa == 'ATTENDANCE'){ echo "selected"; } echo ">ATTENDANCE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>-->
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('spr_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_prodi"); ?>" method="POST">
												<select name="cekprodi" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi = $this->session->userdata('spr_filter_prodi');
														if($cekjurusan != "")
														{
															if($rowprodi)
															{
																if($cekjurusan == "SC")
																{
																	foreach($rowprodi as $rp)
																	{
																		echo "<option value='$rp->KursusSingkatID'";
																		if($cekprodi == $rp->KursusSingkatID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->KursusSingkatID - $rp->Nama</option>";
																	}
																}
																else
																{
																	foreach($rowprodi as $rp)
																	{
																		echo "<option value='$rp->ProdiID'";
																		if($cekprodi == $rp->ProdiID)
																		{
																			echo "selected";
																		}
																		echo ">$rp->ProdiID - $rp->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_tahun"); ?>" method="POST">
												<select name="cektahun" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														$cektahun = $this->session->userdata('spr_filter_tahun');
														$GanjilGenap = '';
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	echo "<option value='$rt->TahunID'";
																	if($cektahun == $rt->TahunID)
																	{
																		echo "selected";
																		$GanjilGenap = $rt->Nama;
																	}
																	echo ">$rt->TahunID - $rt->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<!--<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_tingkat"); ?>" method="POST">
												<select name="cektingkat" title="Filter by Level" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- LEVEL --</option>
													<?php
														// $cektingkat = $this->session->userdata('spr_filter_tingkat');
														// if($cektahun != "")
														// {
															// echo "<option value='1'"; if($cektingkat == 1){ echo "selected"; } echo ">1</option>";
															// echo "<option value='2'"; if($cektingkat == 2){ echo "selected"; } echo ">2</option>";
															// echo "<option value='3'"; if($cektingkat == 3){ echo "selected"; } echo ">3</option>";
														// }
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>-->
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_kelas"); ?>" method="POST">
												<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
													<option value="_">-- CLASS --</option>
													<?php
														if($cekjurusan == "REG")
														{
															$init = "";
														}
														if($cekjurusan == "INT")
														{
															$init = "O";
														}
														$ni = "";
														$cekkelas = $this->session->userdata('spr_filter_kelas');
														if($cekjurusan != "")
														{
															if($rowkelas)
															{
																for($i=1;$i<=3;$i++)
																{
																	foreach($rowkelas as $kls)
																	{
																		if($init == "")
																		{
																			$ni = $i;
																		}
																		else
																		{
																			$ni = "";
																		}
																		if($kls->ProgramID == $cekjurusan)
																		{
																			echo "<option value='".$i."_".$kls->KelasID."'";
																			if($cekkelas == $i."_".$kls->KelasID)
																			{
																				echo "selected";
																			}
																			echo ">$init$ni$kls->Nama</option>";
																		}
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("rank/ptl_filter_subjek"); ?>" method="POST">
												<select name="ceksubjek" title="Filter by Subjects" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- SUBJECTS --</option>
													<?php
														$ceksubjek = $this->session->userdata('spr_filter_subjek');
														if($cekkelas != "")
														{
															if($rowrecord)
															{
																$no = 1;
																foreach($rowrecord as $rsub)
																{
																	if($no == 1)
																	{
																		$MhswID = $rsub->MhswID;
																		$KHSID = $rsub->KHSID;
																		$reskrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
																		if($reskrs)
																		{
																			foreach($reskrs as $rs)
																			{
																				$SubjekID = $rs->SubjekID;
																				$ressub = $this->m_subjek->PTL_select($SubjekID);
																				echo "<option value='$rs->SubjekID'";
																				if($rs->SubjekID == $ceksubjek)
																				{
																					echo "selected";
																				}
																				echo ">$rs->Nama</option>";
																			}
																		}
																		$no++;
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<?php
									if(($cekjurusan != "") AND ($cekprodi != "") AND ($cektahun != "") AND ($cekkelas != ""))
									{
								?>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>Photo</th>
													<th>Student Name & ID</th>
													<th>Program<br/>Program&nbsp;Studi</th>
													<th>Language ID</th>
													<th>Status</th>
													<th>Class</th>
													<!--<th>Total Absent</th>-->
													<th>Score</th>
													<?php
														if($ceksubjek == "")
														{
															echo "<th>CGPA</th>";
														}
													?>
												</tr>
											</thead>
											<tbody>
												<?php
													$no = 1;
													if($rowrecord)
													{
														foreach($rowrecord as $row)
														{
															$MhswID = $row->MhswID;
															$KHSID = $row->KHSID;
															$reskrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
															if($reskrs)
															{
																$cls = "class='success'";
															}
															else
															{
																$cls = "class='danger'";
															}
															$jumlahKRS = 0;
															$KelasID = $row->KelasID;
															$r = $this->m_mahasiswa->PTL_select($MhswID);
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
															$nama = "";
															if($r)
															{
																if($r["Foto"] != "")
																{
																	$foto = "foto_mahasiswa/".$r["Foto"];
																	$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																	if($exist)
																	{
																		$foto = $foto;
																		$source_photo = base_url("ptl_storage/$foto");
																		$info = pathinfo($source_photo);
																		$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																		$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																		if($exist1)
																		{
																			$foto_preview = $foto_preview;
																		}
																		else
																		{
																			$foto_preview = $foto;
																		}
																	}
																	else
																	{
																		$foto = "foto_umum/user.jpg";
																		$foto_preview = "foto_umum/user.jpg";
																	}
																}
																$nama = $r["Nama"];
															}
															$ProdiID = $row->ProdiID;
															if($row->ProgramID == "SC")
															{
																$KursusSingkatID = $ProdiID;
																$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
															}
															else
															{
																$p = $this->m_prodi->PTL_select($ProdiID);
															}
															$StatusMhswID = $row->StatusMhswID;
															$s = $this->m_status->PTL_select($StatusMhswID);
															$k = $this->m_kelas->PTL_select_kelas($KelasID);
															$kelas = "";
															if($k)
															{
																$kelas = $k["Nama"];
															}
															$resstatus = $this->m_status->PTL_select($StatusMhswID);
															if($resstatus["StatusMhswID"] == "A")
															{
																$status = "<font color='green'><b>$resstatus[Nama]</b></font>";
															}
															else
															{
																$status = "<font color='red'><b>$resstatus[Nama]</b></font>";
															}
															$thn = $row->TahunKe;
															if($cekjurusan == "INT")
															{
																$thn = "O";
															}
															$languageID = "<font color='red'><b>Not set</b></font>";
															if($row->languageID != "")
															{
																$languageID = $row->languageID;
															}
															echo "<tr $cls>
																	<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$no</td>
																	<td>
																		&nbsp;&nbsp;
																		<a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																			<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																		</a>
																	</td>
																	<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)</td>
																	<td>$row->ProgramID<br/>$p[Nama]</td>
																	<td>$languageID</td>
																	<td>$s[Nama]</td>
																	<td>$thn$kelas</td>
																	<td align='right'><font size='6px' color='red'><b>$row->IPS</b></font></td>";
																	if($ceksubjek == "")
																	{
																		echo "<td align='right'><font size='6px' color='red'><b>$row->IP</b></font></td>";
																	}
															echo "</tr>";
															$no++;
															// $MhswID = $row->MhswID;
															// $KHSID = $row->KHSID;
															// $reskrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
															// $TotalAbsent = 0;
															// if($reskrs)
															// {
																// foreach($reskrs as $r)
																// {
																	// $KRSID = $r->KRSID;
																	// $rabs = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																	// if($rabs) { $tabs = count($rabs); } else { $tabs = 0; }
																	// $TotalAbsent = $TotalAbsent + $tabs;
																// }
																// $cls = "class='success'";
															// }
															// else
															// {
																// $cls = "class='danger'";
															// }
															// if($TotalAbsent > 4)
															// {
																// $jumlahKRS = 0;
																// $KelasID = $row->KelasID;
																// $r = $this->m_mahasiswa->PTL_select($MhswID);
																// $foto = "foto_umum/user.jpg";
																// $foto_preview = "foto_umum/user.jpg";
																// $nama = "";
																// if($r)
																// {
																	// if($r["Foto"] != "")
																	// {
																		// $foto = "foto_mahasiswa/".$r["Foto"];
																		// $exist = file_exists_remote(base_url("ptl_storage/$foto"));
																		// if($exist)
																		// {
																			// $foto = $foto;
																			// $source_photo = base_url("ptl_storage/$foto");
																			// $info = pathinfo($source_photo);
																			// $foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																			// $exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																			// if($exist1)
																			// {
																				// $foto_preview = $foto_preview;
																			// }
																			// else
																			// {
																				// $foto_preview = $foto;
																			// }
																		// }
																		// else
																		// {
																			// $foto = "foto_umum/user.jpg";
																			// $foto_preview = "foto_umum/user.jpg";
																		// }
																	// }
																	// $nama = $r["Nama"];
																// }
																// $ProdiID = $row->ProdiID;
																// if($row->ProgramID == "SC")
																// {
																	// $KursusSingkatID = $ProdiID;
																	// $p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
																// }
																// else
																// {
																	// $p = $this->m_prodi->PTL_select($ProdiID);
																// }
																// $StatusMhswID = $row->StatusMhswID;
																// $s = $this->m_status->PTL_select($StatusMhswID);
																// $k = $this->m_kelas->PTL_select_kelas($KelasID);
																// $kelas = "";
																// if($k)
																// {
																	// $kelas = $k["Nama"];
																// }
																// $resstatus = $this->m_status->PTL_select($StatusMhswID);
																// if($resstatus["StatusMhswID"] == "A")
																// {
																	// $status = "<font color='green'><b>$resstatus[Nama]</b></font>";
																// }
																// else
																// {
																	// $status = "<font color='red'><b>$resstatus[Nama]</b></font>";
																// }
																// $thn = $row->TahunKe;
																// if($cekjurusan == "INT")
																// {
																	// $thn = "O";
																// }
																// $languageID = "<font color='red'><b>Not set</b></font>";
																// if($row->languageID != "")
																// {
																	// $languageID = $row->languageID;
																// }
																// echo "<tr $cls>
																		// <td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$no</td>
																		// <td>
																			// &nbsp;&nbsp;
																			// <a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																				// <img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																			// </a>
																		// </td>
																		// <td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)</td>
																		// <td>$row->ProgramID<br/>$p[Nama]</td>
																		// <td>$languageID</td>
																		// <td>$s[Nama]</td>
																		// <td>$thn$kelas</td>
																		// <td align='right'><font size='6px' color='red'><b>$TotalAbsent</b></font></td>
																	// </tr>";
																// $no++;
															// }
														}
													}
													if($no == 1)
													{
														echo "<tr>
																<td colspan='9'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>