		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subject Score</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject_score"); ?>">Subject Score</a>
							>>
							<a href="<?php echo site_url("subject_score/ptl_form"); ?>">Add New Subject Score</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("subject_score/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Grade</label>
											<input type="text" name="Nama" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Value</label>
											<input type="number" name="Bobot" step="0.01" min="0" max="4" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Minimal Score</label>
											<input type="number" name="NilaiMin" step="0.01" min="0" max="100" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Maximum Score</label>
											<input type="number" name="NilaiMax" step="0.01" min="0" max="100" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Appreciation</label>
											<input type="text" name="Deskripsi" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Remedial?</label>
											<input type="checkbox" name="Remedial" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Pass?</label>
											<input type="checkbox" name="Lulus" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("subject_score"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>