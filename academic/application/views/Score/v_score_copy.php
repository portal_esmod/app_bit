		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Copy Subjects Score</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject_score"); ?>">Subject Score</a>
							>>
							<a href="<?php echo site_url("subject_score/ptl_copy"); ?>">Copy Subjects Score</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>You can copy the score of inter-subjects score.
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							The subject score will be copied
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<form action="<?php echo site_url("subject_score/ptl_filter_kur1"); ?>" method="POST">
									<select name="cekkurikulum1" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- CURRICULUM --</option>
										<?php
											$cekkurikulum1 = $this->session->userdata('subject_score_filter_kur1');
											if($rowkurikulum)
											{
												foreach($rowkurikulum as $rk)
												{
													echo "<option value='$rk->KurikulumID'";
													if($cekkurikulum1 == $rk->KurikulumID)
													{
														echo "selected";
													}
													echo ">$rk->KurikulumKode - $rk->Nama</option>";
												}
											}
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<br/>
								<?php
									if($cekkurikulum1)
									{
								?>
										<form role="form" action="<?php echo site_url("subject_score/ptl_set_copy"); ?>" method="POST">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>
															<?php
																$cek = $this->session->userdata('subject_score_set_all1');
																if($cek == "")
																{
																	echo "<a href='".site_url("subject_score/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
																}
																else
																{
																	echo "<a href='".site_url("subject_score/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
																}
															?>
														</th>
														<th>Subjects Name</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$n = 0;
														if($rowrecord1)
														{
															foreach($rowrecord1 as $row)
															{
																$n++;
																echo "<tr>
																		<td title='NilaiID : $row->NilaiID'>$n</td>
																		<td>";
																		if($cek == "")
																		{
																			echo "<input type='checkbox' name='cek$n' value='$row->NilaiID'>";
																		}
																		else
																		{
																			echo "<input type='checkbox' name='cek$n' value='$row->NilaiID' checked>";
																		}
																echo "</td>
																		<td>$row->Nama ($row->Bobot - $row->Deskripsi)</td>
																	</tr>";
															}
														}
														else
														{
															echo "<tr>
																	<td colspan='3' align='center'><font color='red'><b>No data available</b></font></td>
																</tr>";
														}
													?>
												</tbody>
												<tfoot>
													<tr>
														<th>#</th>
														<th>
															<?php
																$cek = $this->session->userdata('subject_score_set_all1');
																if($cek == "")
																{
																	echo "<a href='".site_url("subject_score/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
																}
																else
																{
																	echo "<a href='".site_url("subject_score/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
																}
															?>
														</th>
														<th>
															<?php
																echo "<input type='hidden' name='total1' value='$n'>";
																echo "<button type='submit' class='btn btn-primary'>Copy Allocated</button>";
															?>
														</th>
													</tr>
												</tfoot>
											</table>
										</form>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							The copy object
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<form action="<?php echo site_url("subject_score/ptl_filter_kur2"); ?>" method="POST">
									<select name="cekkurikulum2" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- CURRICULUM --</option>
										<?php
											$cekkurikulum2 = $this->session->userdata('subject_score_filter_kur2');
											if($rowkurikulum)
											{
												foreach($rowkurikulum as $rk)
												{
													if($cekkurikulum1 != $rk->KurikulumID)
													{
														echo "<option value='$rk->KurikulumID'";
														if($cekkurikulum2 == $rk->KurikulumID)
														{
															echo "selected";
														}
														echo ">$rk->KurikulumKode - $rk->Nama</option>";
													}
												}
											}
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<br/>
								<?php
									if($cekkurikulum2 != "")
									{
								?>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>Subjects Name</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$n2 = 1;
													if($rowrecord2)
													{
														foreach($rowrecord2 as $row)
														{
															echo "<tr>
																	<td title='NilaiID : $row->NilaiID'>$n2</td>
																	<td>$row->Nama ($row->Bobot - $row->Deskripsi)</td>
																</tr>";
															$n2++;
														}
													}
													else
													{
														echo "<tr>
																<td colspan='2' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>";
													}
												?>
											</tbody>
											<tfoot>
												<tr>
													<th>#</th>
													<th>Subjects Name</th>
												</tr>
											</tfoot>
										</table>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>