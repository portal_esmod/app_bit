<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}
	if(count($matches)>1)
	{
		$version = $matches[1];
		switch(true)
		{
			case ($version<=12):
			echo "<script>
					alert('sedang dalam pengembangan, buka di browser lain');
					window.close();
				</script>";
			exit;
			default:
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Esmod Jakarta - Academic</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dashboard/img/favicon.ico"/>
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/dashboard/css/main-style.css" rel="stylesheet" />
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body class="body-Login-back">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 text-center logo-margin ">
					<img src="<?php echo base_url(); ?>assets/dashboard/img/logo.png" alt="" width="350px"/>
				</div>
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">                  
						<div class="panel-heading">
							<h3 class="panel-title">Please Sign</h3>
						</div>
						<div class="panel-body">
							<noscript>
								<h4><font color="red"><b>Warning!</b></font></h4>
								<p><font color="red">You need to have <a href="http://www.enable-javascript.com/id/" target="_blank">JavaScript</a> enabled to use this site.</font></p>
							</noscript>
							<center><font color="blue"><b>Best view and performance in Google Chrome</b></font></center>
							<br/>
							<form role="form" action="<?php echo site_url("login/ptl_do_login"); ?>" method="POST">
								<fieldset>
									<div class="form-group">
										<input type="text" name="username" class="form-control" placeholder="Username" autofocus required>
									</div>
									<div class="form-group">
										<input type="password" name="password" class="form-control" placeholder="Password" required>
									</div>
									<?php
										if($_SERVER["HTTP_HOST"] != "localhost")
										{
									?>
											<div class="form-group">
												<input type="checkbox" name="remember" value="7" id="myCheck" onclick="myFunction()">
												Remember me for 7 days.
											</div>
											<div id="text" style="display:none" class="g-recaptcha" data-sitekey="6LeXKk4UAAAAABzBI25f2lqIDGLmfqA1u3HJGmKx"></div>
											<br/>
									<?php
										}
									?>
									<a href="<?php echo site_url("login/ptl_forgot"); ?>">Forgot Password?</a>
									<br/>
									<br/>
									<input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery-1.10.2.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script>
			function myFunction()
			{
				var checkBox = document.getElementById("myCheck");
				var text = document.getElementById("text");
				if (checkBox.checked == true)
				{
					text.style.display = "block";
				}
				else
				{
					text.style.display = "none";
				}
			}
		</script>
	</body>
</html>