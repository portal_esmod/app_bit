		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Subject</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject"); ?>">Subjects</a>
							>>
							<a href="<?php echo site_url("subject/ptl_form/$KurikulumID"); ?>">Add New Subject</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("subject/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Subject Code</label>
											<input type="text" name="SubjekKode" class="form-control">
											<input type="hidden" name="KurikulumID" value="<?php echo $KurikulumID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Credit</font></label>
											<input type="number" name="SKS" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Semester</font></label>
											<select name="Sesi" class="form-control">
												<option value="">-- CHOOSE --</option>
												<option value="1">SEMESTER 1</option>
												<option value="2">SEMESTER 2</option>
												<option value="3">SEMESTER 3</option>
												<option value="4">SEMESTER 4</option>
												<option value="5">SEMESTER 5</option>
												<option value="6">SEMESTER 6</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Subject Type</font></label>
											<select name="JenisMKID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowjenis)
													{
														foreach($rowjenis as $rj)
														{
															echo "<option value='$rj->JenisMKID'>$rj->Singkatan - $rj->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject Group</label>
											<select name="SubjekGroupID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowgrup)
													{
														foreach($rowgrup as $rg)
														{
															echo "<option value='$rg->SubjekGroupID'>$rg->Kode - $rg->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Program Studi</label>
											</div>
											<?php
												$no = 0;
												if($rowd3)
												{
													foreach($rowd3 as $rd3)
													{
														$no++;
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd3->ProdiID'>&nbsp;$rd3->ProdiID - $rd3->Nama</div>";
													}
												}
												if($rowd1)
												{
													foreach($rowd1 as $rd1)
													{
														$no++;
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd1->ProdiID'>&nbsp;$rd1->ProdiID - $rd1->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="total" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Optional?</label>
											</div>
											<input type="checkbox" name="Optional" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("subject"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>