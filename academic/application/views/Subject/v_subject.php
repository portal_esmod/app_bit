		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subjects</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject"); ?>">Subjects</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is a list of subject.
								</div>
								<form action="<?php echo site_url("subject/ptl_filter_kur"); ?>" method="POST">
									<select name="cekkurikulum" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- CURRICULUM --</option>
										<?php
											$cekkurikulum = $this->session->userdata('subject_filter_kur');
											if($rowkurikulum)
											{
												foreach($rowkurikulum as $rk)
												{
													// if($rk->NA == "N")
													// {
														echo "<option value='$rk->KurikulumID'";
														if($cekkurikulum == $rk->KurikulumID)
														{
															echo "selected";
														}
														echo ">$rk->KurikulumKode - $rk->Nama</option>";
													// }
												}
											}
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<br/>
								<?php
									if($cekkurikulum != "")
									{
								?>
										<center>
											<a href="<?php echo site_url("subject/ptl_form/$cekkurikulum"); ?>" class="btn btn-primary">Add New</a>
											<a href="<?php echo site_url("subject/ptl_copy"); ?>" class="btn btn-warning"><i class="fa fa-copy"> Copy</i></a>
										</center>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th>#</th>
													<th>Code</th>
													<th>Name</th>
													<th>Credit</th>
													<th>Semester</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
																echo "<td title='SubjekID : $row->SubjekID'>$no</td>
																	<td>$row->SubjekKode</td>
																	<td>$row->Nama</td>
																	<td align='right'>$row->SKS</td>
																	<td align='right'>$row->Sesi</td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("subject/ptl_edit/$row->SubjekID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																		<a class='btn btn-success' href='".site_url("subject/ptl_percentage_form/$row->SubjekID")."'>
																			<i class='fa fa-plus'></i>
																		</a>
																	</td>
																</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>