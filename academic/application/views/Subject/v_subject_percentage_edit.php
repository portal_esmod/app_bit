		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Percentage</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject"); ?>">Subjects</a>
							>>
							<a href="<?php echo site_url("subject/ptl_percentage_form/$SubjekID/$id_subjek_percentage"); ?>">Edit Percentage</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Subject Code</th>
											<th>:</th>
											<th><?php echo $SubjekKode; ?></th>
											<th>Subject Name</th>
											<th>:</th>
											<th><?php echo $Nama; ?></th>
										</tr>
										<tr>
											<th>SKS</th>
											<th>:</th>
											<th><?php echo $SKS; ?></th>
											<th>Session</th>
											<th>:</th>
											<th><?php echo $Sesi; ?></th>
										</tr>
										<tr>
											<th>Program ID</th>
											<th>:</th>
											<th><?php echo $ProdiID; ?></th>
											<th>Optional</th>
											<th>:</th>
											<th><?php echo $Optional; ?></th>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Percentage</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											$total = 0;
											if($rowrecord)
											{
												foreach($rowrecord as $row)
												{
													if($row->na == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr class='info'>";
													}
														echo "<td title='id_subjek_percentage : $row->id_subjek_percentage'>$no</td>
															<td>$row->nama</td>
															<td align='right'>$row->persentase%</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("subject/ptl_percentage_form/$row->SubjekID/$row->id_subjek_percentage")."'>
																	<i class='fa fa-list'></i>
																</a>
																<a class='btn btn-danger' href='".site_url("subject/ptl_percentage_delete/$row->id_subjek_percentage/$row->SubjekID")."'>
																	X
																</a>
															</td>
														</tr>";
													$total = $total + $row->persentase;
													$no++;
												}
											}
											else
											{
												echo "<tr>
														<td colspan='4' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
							<center>
								<h3>Total <?php echo $total; ?>%</h3>
							</center>
							<form role="form" action="<?php echo site_url("subject/ptl_percentage_update"); ?>" method="POST">
								<div class="col-lg-6">
									<div class="form-group">
										<label><font color="red">Name</font></label>
										<input type="text" name="nama" value="<?php echo $nama; ?>" class="form-control" required>
										<input type="hidden" name="id_subjek_percentage" value="<?php echo $id_subjek_percentage; ?>">
										<input type="hidden" name="SubjekID" value="<?php echo $SubjekID; ?>">
										<input type="hidden" name="Total" value="<?php echo (100 - $total); ?>">
										<p class="help-block"></p>
									</div>
									<div class="form-group">
										<label><font color="red">Percentage</font></label>
										<input type="number" name="persentase" value="<?php echo $persentase; ?>" step="0" min="0" max="<?php echo (100 - $total); ?>" class="form-control" required>
										<p class="help-block"></p>
									</div>
									<center>
										<a href="<?php echo site_url("subject"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<button type="submit" class="btn btn-primary">Save</button>
									</center>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>