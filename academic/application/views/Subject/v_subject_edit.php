		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Subject</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("subject"); ?>">Subjects</a>
							>>
							<a href="<?php echo site_url("subject/ptl_edit/$SubjekID"); ?>">Edit Subject</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("subject/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="blue">Subject Code</font></label>
											<input type="text" name="SubjekKode" value="<?php echo $SubjekKode; ?>" class="form-control" required>
											<input type="hidden" name="SubjekID" value="<?php echo $SubjekID; ?>">
											<input type="hidden" name="KurikulumID" value="<?php echo $KurikulumID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Credit</font></label>
											<input type="number" name="SKS" value="<?php echo $SKS; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Semester</font></label>
											<select name="Sesi" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<option value="1" <?php if($Sesi == 1){ echo "selected"; } ?>>SEMESTER 1</option>
												<option value="2" <?php if($Sesi == 2){ echo "selected"; } ?>>SEMESTER 2</option>
												<option value="3" <?php if($Sesi == 3){ echo "selected"; } ?>>SEMESTER 3</option>
												<option value="4" <?php if($Sesi == 4){ echo "selected"; } ?>>SEMESTER 4</option>
												<option value="5" <?php if($Sesi == 5){ echo "selected"; } ?>>SEMESTER 5</option>
												<option value="6" <?php if($Sesi == 6){ echo "selected"; } ?>>SEMESTER 6</option>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Subject Type</font></label>
											<select name="JenisMKID" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowjenis)
													{
														foreach($rowjenis as $rj)
														{
															echo "<option value='$rj->JenisMKID'";
															if($JenisMKID == $rj->JenisMKID)
															{
																echo "selected";
															}
															echo ">$rj->Singkatan - $rj->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject Group</label>
											<select name="SubjekGroupID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowgrup)
													{
														foreach($rowgrup as $rg)
														{
															echo "<option value='$rg->SubjekGroupID'";
															if($SubjekGroupID == $rg->SubjekGroupID)
															{
																echo "selected";
															}
															echo ">$rg->Kode - $rg->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Program Studi</label>
											</div>
											<?php
												$no = 0;
												$word = explode(".",$ProdiID);
												$td3 = count($rowd3);
												if($rowd3)
												{
													foreach($rowd3 as $rd3)
													{
														$no++;
														$cd3 = "";
														for($d3=0;$d3<$td3;$d3++)
														{
															if(@$word[$d3] == $rd3->ProdiID)
															{
																$cd3 = "checked";
															}
														}
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd3->ProdiID' $cd3>&nbsp;$rd3->ProdiID - $rd3->Nama</div>";
													}
												}
												$td1 = count($rowd1);
												if($rowd1)
												{
													foreach($rowd1 as $rd1)
													{
														$no++;
														$cd1 = "";
														for($d1=0;$d1<$td1;$d1++)
														{
															if(@$word[$d1] == $rd1->ProdiID)
															{
																$cd1 = "checked";
															}
														}
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd1->ProdiID' $cd1>&nbsp;$rd1->ProdiID - $rd1->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="total" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="col-lg-6">
											<br/>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Optional?</label>
											</div>
											<input type="checkbox" name="Optional" value="Y" <?php if($Optional == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Not Active?</label>
											</div>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("subject"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>