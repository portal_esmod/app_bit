		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Class</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("class_list"); ?>">Class</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of class.
								</div>
								<form action="<?php echo site_url("class_list/ptl_filter_jur"); ?>" method="POST">
									<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- PROGRAM --</option>
										<?php
											$cekjurusan = $this->session->userdata('class_filter_jur');
											echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
											echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
											echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
										?>
									</select>
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<br/>
								<?php
									if($cekjurusan != "")
									{
								?>
										<center><a href="<?php echo site_url("class_list/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Specialization</th>
													<th>Maximun Capacity</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															$SpesialisasiID = $row->SpesialisasiID;
															$res = $this->m_spesialisasi->PTL_select($SpesialisasiID);
															if($res)
															{
																$spec = $res["Nama"];
															}
															else
															{
																$spec = "";
															}
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>
																		<td title='KelasID : $row->KelasID'>$no</td>
																		<td>$row->Nama</td>
																		<td>$spec</td>
																		<td align='right'>$row->KapasitasMaksimum</td>
																		<td class='center'>
																			<a class='btn btn-info' href='".site_url("class_list/ptl_edit/$row->KelasID")."'>
																				<i class='fa fa-list'></i>
																			</a>
																		</td>
																	</tr>";
															}
															else
															{
																echo "<tr>
																		<td title='KelasID : $row->KelasID'>$no</td>
																		<td>$row->Nama</td>
																		<td>$spec</td>
																		<td align='right'>$row->KapasitasMaksimum</td>
																		<td class='center'>
																			<a class='btn btn-info' href='".site_url("class_list/ptl_edit/$row->KelasID")."'>
																				<i class='fa fa-list'></i>
																			</a>
																		</td>
																	</tr>";
															}
															$no++;
														}
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>