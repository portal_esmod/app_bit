		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Class</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("class_list"); ?>">Class</a>
							>>
							<a href="<?php echo site_url("class_list/ptl_form"); ?>">Add New Class</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>ATTENTION! You can not delete the data after you input because the data you input will be the primary key.
								</div>
								<form role="form" action="<?php echo site_url("class_list/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization</label>
											<select name="SpesialisasiID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowrecord)
													{
														foreach($rowrecord as $row)
														{
															echo "<option value='$row->SpesialisasiID'>$row->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Capacity</label>
											<input type="text" name="KapasitasMaksimum" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("class_list"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>