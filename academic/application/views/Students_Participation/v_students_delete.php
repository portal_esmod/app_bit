		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Delete Student</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students"); ?>">Students</a>
							>>
							<a href="<?php echo site_url("students/ptl_confirm_delete/$MhswID"); ?>">Delete Student</a>
                        </div>
                        <div class="panel-body">
							<div class="alert alert-info">
								 <a class="alert-link">Notes: </a>This action will erase all data of students permanently. You can not restore this data at a later date including Administrators or IT Team. By doing this you have been sure to delete the data, so that the data on the student's other departments will also be deleted. The system will sends an email to all relevant departments.
								 <br/>
								 ("patrice@esmodjakarta.com, anita@esmodjakarta.com, rossy@esmodjakarta.com, anisa@esmodjakarta.com, miha@esmodjakarta.com, wprasasti@esmodjakarta.com, ichaa@esmodjakarta.com, stenna@esmodjakarta.com, theresia@esmodjakarta.com, lendra@esmodjakarta.com")
							</div>
							<form role="form" action="<?php echo site_url("students/ptl_delete"); ?>" method="POST">
								<div class="col-lg-6">
									<div class="form-group">
										<label>SIN</label>
										<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control">
										<p class="help-block"></p>
									</div>
									<div class="form-group">
										<label>Reason</label>
										<textarea name="reason" class="form-control" required></textarea>
										<p class="help-block"></p>
									</div>
									<center>
										<a href="<?php echo site_url("students"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure want to delete this data (<?php echo $MhswID." - ".$Nama; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')">Delete</button>
									</center>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>