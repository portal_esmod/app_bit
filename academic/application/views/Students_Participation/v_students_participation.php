		<style>
			div.paging {
				padding		: 2px;
				margin		: 2px;
				text-align	: center;
				font-family	: Tahoma;
				font-size	: 16px;
				font-weight	: bold;
			}
			div.paging a {
				padding				: 2px 6px;
				margin-right		: 2px;
				border				: 1px solid #DEDFDE;
				text-decoration		: none;
				color				: #dc0203;
				background-position	: bottom;
			}
			div.paging a:hover {
				background-color: #0063dc;
				border : 1px solid #fff;
				color  : #fff;
			}
			div.paging span.current {
				border : 1px solid #DEDFDE;
				padding		 : 2px 6px;
				margin-right : 2px;
				font-weight  : bold;
				color        : #FF0084;
			}
			div.paging span.disabled {
				padding      : 2px 6px;
				margin-right : 2px;
				color        : #ADAAAD;
				font-weight  : bold;
			}
			div.paging span.prevnext {    
			  font-weight : bold;
			}
			div.paging span.prevnext a {
				 border : none;
			}
			div.paging span.prevnext a:hover {
				display: block;
				border : 1px solid #fff;
				color  : #fff;
			}
		</style>
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Students Participation</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students_participation"); ?>">Students Participation</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>List of Students Participation.
								</div>
								<table class="table table-bordered table-striped">
									<tr class="info">
										<td>
											<form action="<?php echo site_url("students_participation/ptl_filter_tahun_masuk"); ?>" method="POST">
												<select name="cektahunmasuk" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- YEAR --</option>
													<?php
														$cektahunmasuk = $this->session->userdata('students_filter_tahun_masuk');
														if($rowtahun)
														{
															foreach($rowtahun as $rt)
															{
																echo "<option value='$rt->tahun'";
																if($cektahunmasuk == $rt->tahun)
																{
																	echo "selected";
																}
																echo ">$rt->tahun</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students_participation/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('students_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students_participation/ptl_filter_prodi"); ?>" method="POST">
												<select name="cekprodi" title="Filter by Prodi" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi = $this->session->userdata('students_filter_prodi');
														if($cekjurusan == "INT")
														{
															if($programd1)
															{
																foreach($programd1 as $d1)
																{
																	echo "<option value='$d1->ProdiID'";
																	if($cekprodi == $d1->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "REG")
														{
															if($programd3)
															{
																foreach($programd3 as $d3)
																{
																	echo "<option value='$d3->ProdiID'";
																	if($cekprodi == strtoupper($d3->ProdiID))
																	{
																		echo "selected";
																	}
																	echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "SC")
														{
															if($shortcourse)
															{
																foreach($shortcourse as $sc)
																{
																	echo "<option value='$sc->KursusSingkatID'";
																	if($cekprodi == $sc->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("students_participation/ptl_filter_no"); ?>" method="POST">
												<select name="cekno" title="Filter by Number" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>10 data</option>
													<?php
														$cekno = $this->session->userdata('students_filter_no');
														echo "<option value='25'"; if($cekno == '25'){ echo "selected"; } echo ">25 data</option>";
														echo "<option value='50'"; if($cekno == '50'){ echo "selected"; } echo ">50 data</option>";
														echo "<option value='100'"; if($cekno == '100'){ echo "selected"; } echo ">100 data</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<?php
									if($cektahunmasuk != "")
									{
										echo "<center>$totverif data is verified, $totverifnot data is not verified, $totverifsent data waiting for confirmation from $totverifall data.</center>";
									}
								?>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Photo</th>
											<th>Student ID & Name</th>
											<th>Program</th>
											<th>Activity</th>
											<th>Score</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											echo "<tr>
													<td colspan='7' align='center'><font color='red'><b>No data available</b></font></td>
												</tr>";
										?>
									</tbody>
								</table>
								<?php
									// echo $pagination;
									// echo "<center><font color='red'><h3><b>TOTAL : $total</b></h3></font></center>";
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>