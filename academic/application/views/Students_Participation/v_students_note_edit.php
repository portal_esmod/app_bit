		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Note</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("students"); ?>">Students</a>
							>>
							<a href="<?php echo site_url("students/ptl_edit/$MhswID"); ?>">Detail</a>
							>>
							<a href="<?php echo site_url("students/ptl_note_edit/$MhswID/$noteid"); ?>">Edit Note</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("students/ptl_note_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>SIN</label>
											<input readonly type="text" name="MhswID" value="<?php echo $MhswID; ?>" class="form-control">
											<input type="hidden" name="noteid" value="<?php echo $noteid; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="title" value="<?php echo $title; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Notes</label>
											<textarea name="comment" class="form-control"><?php echo $comment; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Meeting Date</label>
											<input type="text" name="datemeet" value="<?php echo $datemeet; ?>" id="datepicker1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject</label>
											<input readonly type="text" name="NamaSubjek" value="<?php echo $NamaSubjek; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("students/ptl_edit/$MhswID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>