		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <a href="<?php echo site_url("inter"); ?>">Standard Interview for Admission</a>
							>>
							<a href="<?php echo site_url("inter/ptl_edit/$WawancaraID"); ?>">Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("inter/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Sequence</label>
											<input type="number" name="urutan" value="<?php echo $urutan; ?>" class="form-control" required>
											<input type="hidden" name="WawancaraID" value="<?php echo $WawancaraID; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Question</label>
											<input type="text" name="pertanyaan" value="<?php echo $pertanyaan; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Standard Answer</label>
											<textarea name="standard" class="form-control" required><?php echo $standard; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("inter"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>