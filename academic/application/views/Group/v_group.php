		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subject Group</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("group"); ?>">Subject Group</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<center><a href="<?php echo site_url("group/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
														echo "<td title='SubjekGroupID : $row->SubjekGroupID'>$no</td>
															<td>$row->Kode</td>
															<td>$row->Nama</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("group/ptl_edit/$row->SubjekGroupID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>