		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Predicate</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("predicate"); ?>">Predicate</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of predicate.
								</div>
								<center><a href="<?php echo site_url("predicate/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Grade</th>
											<th>IPK (CGPA) Min</th>
											<th>IPK (CGPA) Max</th>
											<th>Predicate</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
														echo "<td title='PredikatID : $row->PredikatID'>$no</td>
															<td align='center'>$row->Grade</td>
															<td align='right'>$row->IPKMin</td>
															<td align='right'>$row->IPKMax</td>
															<td>$row->NamaEn</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("predicate/ptl_edit/$row->PredikatID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>