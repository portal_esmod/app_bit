		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Predicate</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("predicate"); ?>">Predicate</a>
							>>
							<a href="<?php echo site_url("predicate/ptl_form"); ?>">Add New Predicate</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("predicate/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="NamaEn" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Grade</label>
											<input type="text" name="Grade" maxlength="1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>IPK (CGPA) Min</label>
											<input type="number" name="IPKMin" step="0.01" min="0" max="4" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>IPK (CGPA) Max</label>
											<input type="number" name="IPKMax" step="0.01" min="0" max="4" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("predicate"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>