		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Predicate</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <a href="<?php echo site_url("predicate"); ?>">Predicate</a>
							>>
							<a href="<?php echo site_url("predicate/ptl_edit/$PredikatID"); ?>">Edit Predicate</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("predicate/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="NamaEn" value="<?php echo $NamaEn; ?>" class="form-control">
											<input type="hidden" name="PredikatID" value="<?php echo $PredikatID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Grade</label>
											<input type="text" name="Grade" maxlength="1" value="<?php echo $Grade; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>IPK (CGPA) Min</label>
											<input type="number" name="IPKMin" value="<?php echo $IPKMin; ?>" step="0.01" min="0" max="4" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>IPK (CGPA) Max</label>
											<input type="number" name="IPKMax" value="<?php echo $IPKMax; ?>" step="0.01" min="0" max="4" style="text-align:right;" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("predicate"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>