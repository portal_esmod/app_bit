		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_edit/$JadwalID/$total"); ?>">Edit</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_edit_day_detail/$JadwalID/$total"); ?>">Edit Day</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_edit_day_attendance/$JadwalID/$total"); ?>">Edit Attendance (SCHED19)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												if($KelasID == 0)
												{
													$kelas = "";
													$wordgab = explode(".",$KelasIDGabungan);
													for($i=0;$i<30;$i++)
													{
														$wg = explode("^",@$wordgab[$i]);
														$KelasID = @$wg[1];
														$res = $this->m_kelas->PTL_select_kelas($KelasID);
														if($res)
														{
															$kls = $res["Nama"];
														}
														else
														{
															$kls = "";
														}
														if($i == 0)
														{
															$kelas .= @$wg[0].$kls;
														}
														else
														{
															if($wg[0] != "")
															{
																$kelas .= ", ".@$wg[0].$kls;
															}
														}
													}
													echo "<font color='red'><b>MERGE</b></font> ($kelas)";
												}
												else
												{
													echo $TahunKe.$NamaKelas;
												}
											?>
										</td>
										<td>Date</td>
										<td>:</td>
										<td><?php
												echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai);
											?>
										</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td><?php echo $NamaSubjek; ?></td>
										<td>Teacher</td>
										<td>:</td>
										<td><?php echo $NamaDosen; ?></td>
									</tr>
									<tr>
										<td>Total Day</td>
										<td>:</td>
										<td><?php echo $RencanaKehadiran." session / ".$total." days in week = ".ceil($RencanaKehadiran/$total)." weeks"; ?></td>
										<td>Id Attendance</td>
										<td>:</td>
										<td><?php echo $JadwalID; ?></td>
									</tr>
								</table>
								<center><h3>Choose on what day this subject should be scheduled</h3></center>
								<form role="form" action="<?php echo site_url("schedule/ptl_edit_day_attendance_save"); ?>" method="POST">
									<table class="table">
										<tbody>
											<?php
												$totatt = $RencanaKehadiran;
												echo "<tr class='success'>
														<td align='center'><b>#</b></td>
														<td align='center'><b>Date</b></td>
														<td align='center' colspan='2'><b>Time</b></td>
														<td align='center'><b>Room</b></td>
														<td align='center'><b>Teacher</b></td>
														<td align='center'><b>Notes</b></td>
													</tr>";
												$max_date = hitungHari($TglSelesai,$TglMulai) + 1;
												$nta = 0;
												$no = 1;
												$word = explode("-",$TglMulai);
												for($i=0;$i<$max_date;$i++)
												{
													$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
													$date = date("Y-m-d",$data);
													$nama = date("l",$data);
													for($ti=1;$ti<=$total;$ti++)
													{
														$rowjadwalhari = $this->m_jadwal_hari->PTL_all_jadwal($JadwalID);
														if($rowjadwalhari)
														{
															$nrhr = 1;
															foreach($rowjadwalhari as $row)
															{
																if($ti == $nrhr)
																{
																	$HariID = $row->HariID;
																}
																$nrhr++;
															}
														}
														$reshari = $this->m_hari->PTL_select($HariID);
														if($reshari["Nama_en"] == $nama)
														{
															if($nta < $totatt)
															{
																$PresensiID = "";
																$rowhari = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																$Pertemuan = $no;
																$resatt = $this->m_presensi->PTL_select_attendance($JadwalID,$Pertemuan);
																if($resatt)
																{
																	$PresensiID = $resatt['PresensiID'];
																	$JamMulai = substr($resatt['JamMulai'],0,5);
																	$JamSelesai = substr($resatt['JamSelesai'],0,5);
																	if($apply == "APPLY")
																	{
																		$RuangID = $rowhari["RuangID"];
																	}
																	else
																	{
																		$RuangID = $resatt['RuangID'];
																	}
																	$PresensiID = $resatt['PresensiID'];
																	$dosenid = $resatt["DosenID"];
																}
																else
																{
																	$JamMulai = substr($rowhari["JamMulai"],0,5);
																	$JamSelesai = substr($rowhari["JamSelesai"],0,5);
																	$RuangID = $rowhari["RuangID"];
																	$PresensiID = "";
																	$dosenid = "";
																}
																if(@$resatt['Tanggal'] != "")
																{
																	$word = explode("-",@$resatt['Tanggal']);
																	$data = mktime(0,0,0,$word[1],$word[2],$word[0]);
																	$date = date("Y-m-d",$data);
																	$nama = date("l",$data);
																}
																if(@$resatt['JamMulai'] != "")
																{
																	if($apply == "APPLY")
																	{
																		$JamMulai = substr($rowhari["JamMulai"],0,5);
																	}
																	else
																	{
																		$JamMulai = substr(@$resatt['JamMulai'],0,5);
																	}
																}
																if(@$resatt['JamSelesai'] != "")
																{
																	if($apply == "APPLY")
																	{
																		$JamSelesai = substr($rowhari["JamSelesai"],0,5);
																	}
																	else
																	{
																		$JamSelesai = substr(@$resatt['JamSelesai'],0,5);
																	}
																}
																echo "
																	<tr>
																		<td title='PresensiID : $PresensiID'>$no</td>
																		<td><input type='text' name='attTanggal$no' id='dpSC$no' value='".substr($nama,0,3).", ".tgl_singkat_eng($date)."' class='form-control' required>
																			<input type='hidden' name='no$no' value='$no'>
																			<input type='hidden' name='attPresensiID$no' value='$PresensiID'>
																			<p class='check_result_attTanggal$no'></p>
																		</td>
																		<td>
																			<input type='text' name='attJamMulai$no' value='$JamMulai' id='check_JamMulai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																			<p class='check_result_attJamMulai$no'></p>
																		</td>
																		<td>
																			<input type='text' name='attJamSelesai$no' value='$JamSelesai' id='check_JamSelesai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																			<p class='check_result_attJamSelesai$no'></p>
																		</td>
																		<td>
																			<select name='attRuangID$no' title='' id='check_RuangID$no' class='form-control round-form' required>";
																				if($rowruang)
																				{
																					foreach($rowruang as $rr)
																					{
																						echo "<option value='$rr->RuangID'";
																						if($RuangID == $rr->RuangID)
																						{
																							echo "selected";
																						}
																						echo ">$rr->RuangID - $rr->Nama</option>";
																					}
																				}
																			echo "</select>
																			<p class='check_result_attRuangID$no'></p>
																		</td>
																		<td>
																			<select name='attDosenID$no' title='' class='form-control round-form' required>
																				<option value='$DosenID'";
																				if($dosenid == $DosenID)
																				{
																					echo "selected";
																				}
																				echo ">$DosenID - $NamaDosen</option>";
																				if($DosenID2 != "")
																				{
																					echo "<option value='$DosenID2'";
																					if($dosenid == $DosenID2)
																					{
																						echo "selected";
																					}
																					echo ">$DosenID2 - $NamaDosen2</option>";
																				}
																				if($DosenID3 != "")
																				{
																					echo "<option value='$DosenID3'";
																					if($dosenid == $DosenID3)
																					{
																						echo "selected";
																					}
																					echo ">$DosenID3 - $NamaDosen3</option>";
																				}
																				if($DosenID4 != "")
																				{
																					echo "<option value='$DosenID4'";
																					if($dosenid == $DosenID4)
																					{
																						echo "selected";
																					}
																					echo ">$DosenID4 - $NamaDosen4</option>";
																				}
																				if($DosenID5 != "")
																				{
																					echo "<option value='$DosenID5'";
																					if($dosenid == $DosenID5)
																					{
																						echo "selected";
																					}
																					echo ">$DosenID5 - $NamaDosen5</option>";
																				}
																				if($DosenID6 != "")
																				{
																					echo "<option value='$DosenID6'";
																					if($dosenid == $DosenID6)
																					{
																						echo "selected";
																					}
																					echo ">$DosenID6 - $NamaDosen6</option>";
																				}
																		echo "</select>
																		</td>
																		<td><input type='text' name='Catatan$no' value='$resatt[Catatan]' class='form-control' onmouseover='this.focus()'></td>
																	</tr>
																	";
																$nta++;
																$no++;
															}
														}
													}
												}
												$sisa = $RencanaKehadiran - ($no - 1);
												$peringatan = "";
												if($sisa > 0)
												{
													$peringatan = "style='background-color:#ED9C28;' title='Out of range.'";
												}
												for($ii=1;$ii<=$sisa;$ii++)
												{
													$Pertemuan = $no;
													$resatt = $this->m_presensi->PTL_select_attendance($JadwalID,$Pertemuan);
													if($resatt)
													{
														$PresensiID = $resatt['PresensiID'];
														$JamMulai = substr($resatt['JamMulai'],0,5);
														$JamSelesai = substr($resatt['JamSelesai'],0,5);
														$RuangID = $resatt['RuangID'];
														$PresensiID = $resatt['PresensiID'];
														$dosenid = $resatt["DosenID"];
														$catatan = $resatt["Catatan"];
													}
													else
													{
														$JamMulai = substr($rowhari["JamMulai"],0,5);
														$JamSelesai = substr($rowhari["JamSelesai"],0,5);
														$RuangID = $rowhari["RuangID"];
														$PresensiID = "";
														$dosenid = "";
														$catatan = "";
													}
													if(@$resatt['Tanggal'] != "")
													{
														$word = explode("-",@$resatt['Tanggal']);
														$data = mktime(0,0,0,$word[1],$word[2],$word[0]);
														$date = date("Y-m-d",$data);
														$nama = date("l",$data);
														$tanggal = substr($nama,0,3).", ".tgl_singkat_eng($date);
													}
													else
													{
														$tanggal = "";
													}
													if(@$resatt['JamMulai'] != "")
													{
														$JamMulai = substr(@$resatt['JamMulai'],0,5);
													}
													else
													{
														$JamMulai = "00:00";
													}
													if(@$resatt['JamSelesai'] != "")
													{
														$JamSelesai = substr(@$resatt['JamSelesai'],0,5);
													}
													else
													{
														$JamSelesai = "00:00";
													}
													echo "
														<tr>
															<td title='PresensiID : $PresensiID'>$no</td>
															<td><input $peringatan type='text' name='attTanggal$no' id='dpSC$no' value='$tanggal' class='form-control' required>
																<input type='hidden' name='no$no' value='$no'>
																<input type='hidden' name='attPresensiID$no' value='$PresensiID'>
																<p class='check_result_attTanggal$no'></p>
															</td>
															<td>
																<input $peringatan type='text' name='attJamMulai$no' value='$JamMulai' id='check_JamMulai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																<p class='check_result_attJamMulai$no'></p>
															</td>
															<td>
																<input $peringatan type='text' name='attJamSelesai$no' value='$JamSelesai' id='check_JamSelesai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																<p class='check_result_attJamSelesai$no'></p>
															</td>
															<td>
																<select $peringatan name='attRuangID$no' title='' id='check_RuangID$no' class='form-control round-form' required>";
																	if($rowruang)
																	{
																		foreach($rowruang as $rr)
																		{
																			echo "<option value='$rr->RuangID'";
																			if($RuangID == $rr->RuangID)
																			{
																				echo "selected";
																			}
																			echo ">$rr->RuangID - $rr->Nama</option>";
																		}
																	}
																echo "</select>
																<p class='check_result_attRuangID$no'></p>
															</td>
															<td>
																<select $peringatan name='attDosenID$no' title='' class='form-control round-form' required>";
																	if($dosenid == $DosenID)
																	{
																		echo "selected";
																	}
																	echo ">$DosenID - $NamaDosen</option>";
																	if($DosenID2 != "")
																	{
																		echo "<option value='$DosenID2'";
																		if($dosenid == $DosenID2)
																		{
																			echo "selected";
																		}
																		echo ">$DosenID2 - $NamaDosen2</option>";
																	}
																	if($DosenID3 != "")
																	{
																		echo "<option value='$DosenID3'";
																		if($dosenid == $DosenID3)
																		{
																			echo "selected";
																		}
																		echo ">$DosenID3 - $NamaDosen3</option>";
																	}
																	if($DosenID4 != "")
																	{
																		echo "<option value='$DosenID4'";
																		if($dosenid == $DosenID4)
																		{
																			echo "selected";
																		}
																		echo ">$DosenID4 - $NamaDosen4</option>";
																	}
																	if($DosenID5 != "")
																	{
																		echo "<option value='$DosenID5'";
																		if($dosenid == $DosenID5)
																		{
																			echo "selected";
																		}
																		echo ">$DosenID5 - $NamaDosen5</option>";
																	}
																	if($DosenID6 != "")
																	{
																		echo "<option value='$DosenID6'";
																		if($dosenid == $DosenID6)
																		{
																			echo "selected";
																		}
																		echo ">$DosenID6 - $NamaDosen6</option>";
																	}
															echo "</select>
															</td>
															<td><input $peringatan type='text' name='Catatan$no' value='$catatan' class='form-control' onmouseover='this.focus()'></td>
														</tr>
														";
													$no++;
												}
												echo "<input type='hidden' name='ProgramID' value='$ProgramID'>";
												echo "<input type='hidden' name='TahunID' value='$TahunID'>";
												echo "<input type='hidden' name='attJadwalID' value='$JadwalID'>";
												echo "<input type='hidden' name='total' value='".($no - 1)."'>";
											?>
										</tbody>
									</table>
									<center>
										<a href="<?php echo site_url("schedule/ptl_edit_day_detail/$JadwalID"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<input type="submit" value="Finish" id="my_button" class="btn btn-primary">
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>