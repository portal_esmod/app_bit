		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Day</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form/$ProgramID/$TahunID/$JadwalID/$total_all"); ?>">Add New</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form_day_detail/$JadwalID/$total_all"); ?>">Add Day (SCHED05)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												if($KelasID == 0)
												{
													$kelas = "";
													$wordgab = explode(".",$KelasIDGabungan);
													for($i=0;$i<30;$i++)
													{
														$wg = explode("^",@$wordgab[$i]);
														$KelasID = @$wg[1];
														$res = $this->m_kelas->PTL_select_kelas($KelasID);
														if($res)
														{
															$kls = $res["Nama"];
														}
														else
														{
															$kls = "";
														}
														if($i == 0)
														{
															$kelas .= @$wg[0].$kls;
														}
														else
														{
															if($wg[0] != "")
															{
																$kelas .= ", ".@$wg[0].$kls;
															}
														}
													}
													echo "<font color='red'><b>MERGE</b></font> ($kelas)";
												}
												else
												{
													$res = $this->m_kelas->PTL_select_kelas($KelasID);
													$NamaKelas = "";
													if($res)
													{
														$NamaKelas = $res["Nama"];
													}
													echo $TahunKe.$NamaKelas;
												}
											?>
										</td>
										<td>Date</td>
										<td>:</td>
										<td><?php echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai); ?></td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td>
											<?php
												$ressub = $this->m_subjek->PTL_select($SubjekID);
												$NamaSubjek = "";
												if($ressub)
												{
													$NamaSubjek = $ressub["Nama"];
												}
												echo $NamaSubjek;
											?>
										</td>
										<td>Teacher</td>
										<td>:</td>
										<td>
											<?php
												$resdos = $this->m_dosen->PTL_select($DosenID);
												$NamaDosen = "";
												if($resdos)
												{
													$NamaDosen = $resdos["Nama"];
												}
												echo $NamaDosen;
											?>
										</td>
									</tr>
								</table>
								<center><h3>Choose on what day this subject should be scheduled</h3></center>
								<?php
									if($total_all == "")
									{
								?>
										<form role="form" action="<?php echo site_url("schedule/ptl_form_day_save"); ?>" method="POST">
								<?php
									}
									else
									{
								?>
										<form role="form" action="<?php echo site_url("schedule/ptl_form_day_save_update"); ?>" method="POST">
								<?php
									}
								?>
									<table class="table">
										<tbody>
											<?php
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														if($row->HariID > 0)
														{
															echo "<tr class='success'>
																	<td colspan='6' align='center'><b>$row->Nama_en</b></td>
																	<td><a href='".site_url("schedule/ptl_form_day_insert/$JadwalID/$row->HariID")."' class='btn btn-success btn-circle'><i class='fa fa-plus'></i></a></td>
																</tr>";
															$HariID = $row->HariID;
															$rowhari = $this->m_jadwal_hari->PTL_all_select_hari($JadwalID,$HariID);
															$rowruang = $this->m_ruang->PTL_all();
															foreach($rowhari as $hr)
															{
																echo "<tr>
																		<td title='JadwalHariID : $hr->JadwalHariID'>Class Allocation<td>
																		<td>";
																		if($rowruang)
																		{
																			echo "<select name='RuangID$no' title='' class='form-control round-form' required>
																			<option value=''>-- CHOOSE --</option>";
																			if($rowruang)
																			{
																				foreach($rowruang as $rr)
																				{
																					echo "<option value='$rr->RuangID'";
																					if($hr->RuangID == $rr->RuangID)
																					{
																						echo "selected";
																					}
																					echo ">$rr->RuangID - $rr->Nama</option>";
																				}
																			}
																			echo "</select>";
																		}
																	echo "</td>
																		<td>
																			<input type='text' name='JamMulai$no' value='".substr($hr->JamMulai,0,5)."' class='form-control' id='time1picker$no' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																			<input type='hidden' name='JadwalHariID$no' value='$hr->JadwalHariID'>
																			<input type='hidden' name='HariID$no' value='$HariID'>
																		</td>
																		<td>--></td>
																		<td><input type='text' name='JamSelesai$no' value='".substr($hr->JamSelesai,0,5)."' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required></td>
																		<td><a href='".site_url("schedule/ptl_form_day_delete/$JadwalID/$hr->JadwalHariID")."' class='btn btn-danger btn-circle'><i class='fa fa-minus'></i></a></td>
																	</tr>
																	";
																	$no++;
															}
														}
													}
													$total = $no - 1;
													echo "<tr>
															<td>
																<input type='hidden' name='JadwalID' value='$JadwalID'>
																<input type='hidden' name='total' value='$total'>
															</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														";
												}
												else
												{
													echo "<tr>
															<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											?>
										</tbody>
									</table>
									<center>
										<a href="<?php echo site_url("schedule/ptl_form/$ProgramID/$TahunID/$JadwalID/$total_all"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<?php
											if($total_all == "")
											{
										?>
												<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										<?php
											}
											else
											{
												echo "<input type='submit' value='Update' id='my_button2' class='btn btn-success'>
													<a href='".site_url("schedule/ptl_form_day_attendance/$JadwalID/$total_all")."' class='btn btn-primary'>Next</a>";
											}
										?>
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>