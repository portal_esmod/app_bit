		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Schedule</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule (SCHED02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the fifth step fot setup schedule. <font color="red"><b>If you add a new schedule to the newly subject, then you should enroll each related student to the new subject. </b></font><a href="<?php echo site_url("tutorial/ptl_detail/SCDL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("schedule/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('sch_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("schedule/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('sch_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if(($cekjurusan != "") AND ($cektahun != ""))
											{
												if(!$rowpending)
												{
										?>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<form action="<?php echo site_url("schedule/ptl_filter_dosen"); ?>" method="POST">
															<select name="cekdosen" title="Filter by Subject" class="form-control round-form" onchange="this.form.submit()">
																<option value=''>-- TEACHERS --</option>
																<?php
																	$cekdosen = $this->session->userdata('sch_filter_dosen');
																	if($cekjurusan != "")
																	{
																		if($rowdosen)
																		{
																			foreach($rowdosen as $dsn)
																			{
																				if($dsn->NA == "N")
																				{
																					echo "<option value='$dsn->Login'";
																					if($cekdosen == $dsn->Login)
																					{
																						echo "selected";
																					}
																					echo ">$dsn->Nama</option>";
																				}
																			}
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<form action="<?php echo site_url("schedule/ptl_filter_kelas"); ?>" method="POST">
															<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
																<option value="_">-- CLASS --</option>
																<?php
																	if($cekjurusan == "REG")
																	{
																		$init = "";
																	}
																	if($cekjurusan == "INT")
																	{
																		$init = "O";
																	}
																	$ni = "";
																	$cekkelas = $this->session->userdata('sch_filter_kelas');
																	if($cekjurusan != "")
																	{
																		if($rowkelas)
																		{
																			for($i=1;$i<=3;$i++)
																			{
																				foreach($rowkelas as $kls)
																				{
																					if($init == "")
																					{
																						$ni = $i;
																					}
																					else
																					{
																						$ni = "";
																					}
																					if($kls->ProgramID == $cekjurusan)
																					{
																						echo "<option value='".$i."_".$kls->KelasID."'";
																						if($cekkelas == $i."_".$kls->KelasID)
																						{
																							echo "selected";
																						}
																						echo ">$init$ni$kls->Nama</option>";
																					}
																				}
																			}
																		}
																		echo "<option value='MERGE'";
																		if($cekkelas == "MERGE")
																		{
																			echo "selected";
																		}
																		echo ">*** MERGE CLASS ***</option>";
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<form action="<?php echo site_url("schedule/ptl_filter_type"); ?>" method="POST">
															<select name="cektype" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<?php
																	$cektype = $this->session->userdata('sch_filter_type');
																	echo "<option value=''"; if($cektype == ''){ echo "selected"; } echo ">ALL SCHEDULE</option>";
																	echo "<option value='VAL'"; if($cektype == 'VAL'){ echo "selected"; } echo ">SCHEDULE FILLED</option>";
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<a href="<?php echo site_url("schedule/ptl_pdf/ESMOD%20JAKARTA"); ?>" class="btn btn-info">Print Schedule</a>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<a href="<?php echo site_url("schedule/ptl_form/$cekjurusan/$cektahun"); ?>" class="btn btn-info">Add Schedule</a>
													</td>
										<?php
												}
											}
										?>
									</tr>
								</table>
								<?php
									if($rowpending)
									{
										echo "<br/><center><font color='red'><h4><b>You have a schedule that delayed. Please finish it or delete it.</b></h4></font></center>
											<table class='table'>
												<thead>
													<tr>
														<th>Schedule ID</th>
														<th>Subject</th>
														<th>Date</th>
														<th>Lecturer</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
											";
										foreach($rowpending as $rp)
										{
											$SubjekID = $rp->SubjekID;
											$ressub = $this->m_subjek->PTL_select($SubjekID);
											$NamaSub = "";
											if($ressub)
											{
												$NamaSub = $ressub['Nama'];
											}
											$DosenID = $rp->DosenID;
											$resdos = $this->m_dosen->PTL_select($DosenID);
											$NamaDosen = "";
											if($resdos)
											{
												$NamaDosen = $resdos['Nama'];
											}
											echo "<tr class='info'>
													<td>$rp->JadwalID</td>
													<td>$NamaSub</td>
													<td>
														".tgl_singkat_eng($rp->TglMulai)."
														-
														".tgl_singkat_eng($rp->TglSelesai)."
													</td>
													<td>$NamaDosen</td>
													<td>";
								?>
														<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete_all/$rp->JadwalID"); ?>" title="<?php echo "JadwalID : $rp->JadwalID ~ SubjekID : $SubjekID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $rp->JadwalID; ?>)?')">
															Delete
														</a>
								<?php
												echo "<a class='btn btn-success' href='".site_url("schedule/ptl_edit/$rp->JadwalID")."' title='JadwalID : $rp->JadwalID ~ SubjekID : $SubjekID'>
															Continue
														</a></td>
											</tr>";
										}
									}
									else
									{
										if(($cekjurusan != "") AND ($cektahun != ""))
										{
								?>
											<table class="table">
												<thead>
													<tr>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php
														if($rowrecord)
														{
															foreach($rowrecord as $row)
															{
																if($row->HariID > 0)
																{
																	echo "<tr class='success'>
																			<td><b>$row->Nama_en</b></td>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																		<tr class='success'>
																			<td><b>Class</b></td>
																			<td><b>Subject</b></td>
																			<td><b>Specialization</b></td>
																			<td><b>Teacher</b></td>
																			<td><b>Class Allocation</b></td>
																			<td><b>Time</b></td>
																			<td><b>Action</b></td>
																		</tr>";
																	$row1 = $this->m_kelas->PTL_select();
																	if($row1)
																	{
																		foreach($row1 as $r1)
																		{
																			if($r1->ProgramID == $cekjurusan)
																			{
																				$ProgramID = $cekjurusan;
																				$TahunID = $cektahun;
																				$KelasID = $r1->KelasID;
																				$row2 = $this->m_jadwal->PTL_all_spesifik($ProgramID,$TahunID,$KelasID);
																				$js = count($row2);
																				$min = 0;
																				if($row2)
																				{
																					foreach($row2 as $r2)
																					{
																						$JadwalID = $r2->JadwalID;
																						$HariID = $row->HariID;
																						$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																						if(!$cls)
																						{
																							$min++;
																						}
																					}
																				}
																				$rowspan = $js - $min;
																				$jumruang = 0;
																				$jum = 0;
																				if($row2)
																				{
																					foreach($row2 as $r2)
																					{
																						$JadwalID = $r2->JadwalID;
																						$HariID = $row->HariID;
																						$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																						if($cls)
																						{
																							$jumruang++;
																							$jum++;
																						}
																					}
																				}
																				$cektype = $this->session->userdata('sch_filter_type');
																				if($cektype == "")
																				{
																					$jumruang = $jumruang + 1;
																				}
																				if($this->session->userdata('sch_filter_kelas') == "")
																				{
																					if($jumruang > 0)
																					{
																						echo "<tr class='info'>";
																						if($rowspan == 0)
																						{
																							echo "<td><font color='red'><b>$r1->Nama</b></font></td>
																								</tr>";// rowspan='$rowspan'
																						}
																						if($row2)
																						{
																							$rn = 1;
																							foreach($row2 as $r2)
																							{
																								if($cekdosen == "")
																								{
																									$SubjekID = $r2->SubjekID;
																									$rs2 = $this->m_subjek->PTL_select($SubjekID);
																									$DosenID = $r2->DosenID;
																									$dsn = $this->m_dosen->PTL_select($DosenID);
																									if($dsn)
																									{
																										$nmdsn = $dsn['Nama'];
																									}
																									else
																									{
																										$nmdsn = "";
																									}
																									$JadwalID = $r2->JadwalID;
																									$HariID = $row->HariID;
																									$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																									if($cls)
																									{
																										if($cls['RuangID'] == "")
																										{
																											$ruang = "<font color='red'><b>Not Set</b></font>";
																										}
																										else
																										{
																											$ruang = $cls['RuangID'];
																										}
																										$mulai = substr($cls['JamMulai'],0,5);
																										$selesai = substr($cls['JamSelesai'],0,5);
																									}
																									else
																									{
																										$ruang = "";
																										$mulai = "";
																										$selesai = "";
																									}
																									if($cls)
																									{
																										if($cekjurusan == "REG")
																										{
																											$init = $r2->TahunKe;
																										}
																										if($cekjurusan == "INT")
																										{
																											$init = "O";
																										}
																										if($rn == 1)
																										{
																											echo "<td><font color='red'><b>$init$r1->Nama</b></font></td>
																												<td>
																													$rs2[Nama]
																													<br/>
																													".tgl_singkat_eng($r2->TglMulai)."
																													-
																													".tgl_singkat_eng($r2->TglSelesai)."
																												</td>
																												<td></td>
																												<td>$nmdsn</td>
																												<td>$ruang</td>
																												<td>$mulai -  $selesai</td>
																												<td>
																													<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																														<i class='fa fa-list'></i>
																													</a>";
														?>
																														<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																															<i class="fa fa-times"></i>
																														</a>
														<?php
																											echo "</td>
																											</tr>";
																										}
																										else
																										{
																											echo "<tr class='info'>
																													<td><font color='red'><b>$init$r1->Nama</b></font></td>
																													<td>
																														$rs2[Nama]
																														<br/>
																														".tgl_singkat_eng($r2->TglMulai)."
																														-
																														".tgl_singkat_eng($r2->TglSelesai)."
																													</td>
																													<td></td>
																													<td>$nmdsn</td>
																													<td>$ruang</td>
																													<td>$mulai -  $selesai</td>
																													<td>
																														<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																															<i class='fa fa-list'></i>
																														</a>";
														?>
																														<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																															<i class="fa fa-times"></i>
																														</a>
														<?php
																												echo "</td>
																											</tr>";
																										}
																										$rn++;
																									}
																								}
																								else
																								{
																									if($r2->DosenID == $cekdosen)
																									{
																										$SubjekID = $r2->SubjekID;
																										$rs2 = $this->m_subjek->PTL_select($SubjekID);
																										$DosenID = $r2->DosenID;
																										$dsn = $this->m_dosen->PTL_select($DosenID);
																										if($dsn)
																										{
																											$nmdsn = $dsn['Nama'];
																										}
																										else
																										{
																											$nmdsn = "";
																										}
																										$JadwalID = $r2->JadwalID;
																										$HariID = $row->HariID;
																										$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																										if($cls)
																										{
																											if($cls['RuangID'] == "")
																											{
																												$ruang = "<font color='red'><b>Not Set</b></font>";
																											}
																											else
																											{
																												$ruang = $cls['RuangID'];
																											}
																											$mulai = substr($cls['JamMulai'],0,5);
																											$selesai = substr($cls['JamSelesai'],0,5);
																										}
																										else
																										{
																											$ruang = "";
																											$mulai = "";
																											$selesai = "";
																										}
																										if($cls)
																										{
																											if($cekjurusan == "REG")
																											{
																												$init = $r2->TahunKe;
																											}
																											if($cekjurusan == "INT")
																											{
																												$init = "O";
																											}
																											if($rn == 1)
																											{
																												echo "<td><font color='red'><b>$init$r1->Nama</b></font></td>
																													<td>
																														$rs2[Nama]
																														<br/>
																														".tgl_singkat_eng($r2->TglMulai)."
																														-
																														".tgl_singkat_eng($r2->TglSelesai)."
																													</td>
																													<td></td>
																													<td>$nmdsn</td>
																													<td>$ruang</td>
																													<td>$mulai -  $selesai</td>
																													<td>
																														<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																															<i class='fa fa-list'></i>
																														</a>";
															?>
																															<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																<i class="fa fa-times"></i>
																															</a>
															<?php
																												echo "</td>
																												</tr>";
																											}
																											else
																											{
																												echo "<tr class='info'>
																														<td><font color='red'><b>$init$r1->Nama</b></font></td>
																														<td>
																															$rs2[Nama]
																															<br/>
																															".tgl_singkat_eng($r2->TglMulai)."
																															-
																															".tgl_singkat_eng($r2->TglSelesai)."
																														</td>
																														<td></td>
																														<td>$nmdsn</td>
																														<td>$ruang</td>
																														<td>$mulai -  $selesai</td>
																														<td>
																															<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																																<i class='fa fa-list'></i>
																															</a>";
															?>
																															<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																<i class="fa fa-times"></i>
																															</a>
															<?php
																													echo "</td>
																												</tr>";
																											}
																											$rn++;
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																				else
																				{
																					$ck = "";
																					if($cekkelas != "")
																					{
																						$ck = $cekkelas;
																					}
																					$word = explode("_",$ck);
																					$MatchTahun = @$word[0];
																					$MatchKelas = @$word[1];
																					if($KelasID == $MatchKelas)
																					{
																						if($jumruang > 0)
																						{
																							echo "<tr class='info'>";
																							if($rowspan == 0)
																							{
																								echo "<td><font color='red'><b>$r1->Nama</b></font></td>
																									</tr>";// rowspan='$rowspan'
																							}
																							if($row2)
																							{
																								$rn = 1;
																								foreach($row2 as $r2)
																								{
																									if($r2->TahunKe == $MatchTahun)
																									{
																										if($cekdosen == "")
																										{
																											$SubjekID = $r2->SubjekID;
																											$rs2 = $this->m_subjek->PTL_select($SubjekID);
																											$DosenID = $r2->DosenID;
																											$dsn = $this->m_dosen->PTL_select($DosenID);
																											if($dsn)
																											{
																												$nmdsn = $dsn['Nama'];
																											}
																											else
																											{
																												$nmdsn = "";
																											}
																											$JadwalID = $r2->JadwalID;
																											$HariID = $row->HariID;
																											$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																											if($cls)
																											{
																												if($cls['RuangID'] == "")
																												{
																													$ruang = "<font color='red'><b>Not Set</b></font>";
																												}
																												else
																												{
																													$ruang = $cls['RuangID'];
																												}
																												$mulai = substr($cls['JamMulai'],0,5);
																												$selesai = substr($cls['JamSelesai'],0,5);
																											}
																											else
																											{
																												$ruang = "";
																												$mulai = "";
																												$selesai = "";
																											}
																											if($cls)
																											{
																												if($cekjurusan == "REG")
																												{
																													$init = $r2->TahunKe;
																												}
																												if($cekjurusan == "INT")
																												{
																													$init = "O";
																												}
																												if($rn == 1)
																												{
																													echo "<td><font color='red'><b>$init$r1->Nama</b></font></td>
																														<td>
																															$rs2[Nama]
																															<br/>
																															".tgl_singkat_eng($r2->TglMulai)."
																															-
																															".tgl_singkat_eng($r2->TglSelesai)."
																														</td>
																														<td></td>
																														<td>$nmdsn</td>
																														<td>$ruang</td>
																														<td>$mulai-$selesai</td>
																														<td>
																															<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																																<i class='fa fa-list'></i>
																															</a>";
															?>
																															<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																<i class="fa fa-times"></i>
																															</a>
															<?php
																														echo "</td>
																													</tr>";
																												}
																												else
																												{
																													echo "<tr class='info'>
																															<td><font color='red'><b>$init$r1->Nama</b></font></td>
																															<td>
																																$rs2[Nama]
																																<br/>
																																".tgl_singkat_eng($r2->TglMulai)."
																																-
																																".tgl_singkat_eng($r2->TglSelesai)."
																															</td>
																															<td></td>
																															<td>$nmdsn</td>
																															<td>$ruang</td>
																															<td>$mulai-$selesai</td>
																															<td>
																																<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																																	<i class='fa fa-list'></i>
																																</a>";
															?>
																																<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																	<i class="fa fa-times"></i>
																																</a>
															<?php
																														echo "</td>
																													</tr>";
																												}
																												$rn++;
																											}
																										}
																										else
																										{
																											if($r2->DosenID == $cekdosen)
																											{
																												$SubjekID = $r2->SubjekID;
																												$rs2 = $this->m_subjek->PTL_select($SubjekID);
																												$DosenID = $r2->DosenID;
																												$dsn = $this->m_dosen->PTL_select($DosenID);
																												if($dsn)
																												{
																													$nmdsn = $dsn['Nama'];
																												}
																												else
																												{
																													$nmdsn = "";
																												}
																												$JadwalID = $r2->JadwalID;
																												$HariID = $row->HariID;
																												$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																												if($cls)
																												{
																													if($cls['RuangID'] == "")
																													{
																														$ruang = "<font color='red'><b>Not Set</b></font>";
																													}
																													else
																													{
																														$ruang = $cls['RuangID'];
																													}
																													$mulai = substr($cls['JamMulai'],0,5);
																													$selesai = substr($cls['JamSelesai'],0,5);
																												}
																												else
																												{
																													$ruang = "";
																													$mulai = "";
																													$selesai = "";
																												}
																												if($cls)
																												{
																													if($cekjurusan == "REG")
																													{
																														$init = $r2->TahunKe;
																													}
																													if($cekjurusan == "INT")
																													{
																														$init = "O";
																													}
																													if($rn == 1)
																													{
																														echo "<td><font color='red'><b>$init$r1->Nama</b></font></td>
																															<td>
																																$rs2[Nama]
																																<br/>
																																".tgl_singkat_eng($r2->TglMulai)."
																																-
																																".tgl_singkat_eng($r2->TglSelesai)."
																															</td>
																															<td></td>
																															<td>$nmdsn</td>
																															<td>$ruang</td>
																															<td>$mulai-$selesai</td>
																															<td>
																																<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																																	<i class='fa fa-list'></i>
																																</a>";
																?>
																																<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																	<i class="fa fa-times"></i>
																																</a>
																<?php
																															echo "</td>
																														</tr>";
																													}
																													else
																													{
																														echo "<tr class='info'>
																																<td><font color='red'><b>$init$r1->Nama</b></font></td>
																																<td>
																																	$rs2[Nama]
																																	<br/>
																																	".tgl_singkat_eng($r2->TglMulai)."
																																	-
																																	".tgl_singkat_eng($r2->TglSelesai)."
																																</td>
																																<td></td>
																																<td>$nmdsn</td>
																																<td>$ruang</td>
																																<td>$mulai-$selesai</td>
																																<td>
																																	<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																																		<i class='fa fa-list'></i>
																																	</a>";
																?>
																																	<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																																		<i class="fa fa-times"></i>
																																	</a>
																<?php
																															echo "</td>
																														</tr>";
																													}
																													$rn++;
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																		// id kelas == 0
																		$ProgramID = $cekjurusan;
																		$TahunID = $cektahun;
																		$KelasID = 0;
																		$row2 = $this->m_jadwal->PTL_all_spesifik($ProgramID,$TahunID,$KelasID);
																		if($cekjurusan == "REG")
																		{
																			if($row2)
																			{
																				$rn = 1;
																				foreach($row2 as $r2)
																				{
																					if($r2->Gabungan == "Y")
																					{
																						$SubjekID = $r2->SubjekID;
																						$rs2 = $this->m_subjek->PTL_select($SubjekID);
																						$DosenID = $r2->DosenID;
																						$dsn = $this->m_dosen->PTL_select($DosenID);
																						if($dsn)
																						{
																							$nmdsn = $dsn['Nama'];
																						}
																						else
																						{
																							$nmdsn = "";
																						}
																						$JadwalID = $r2->JadwalID;
																						$HariID = $row->HariID;
																						$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																						if($cls)
																						{
																							if($cls['RuangID'] == "")
																							{
																								$ruang = "<font color='red'><b>Not Set</b></font>";
																							}
																							else
																							{
																								$ruang = $cls['RuangID'];
																							}
																							$mulai = substr($cls['JamMulai'],0,5);
																							$selesai = substr($cls['JamSelesai'],0,5);
																						}
																						else
																						{
																							$ruang = "";
																							$mulai = "";
																							$selesai = "";
																						}
																						if($cls)
																						{
																							$KelasIDGabungan = $r2->KelasIDGabungan;
																							$kelas = "";
																							$wordgab = explode(".",$KelasIDGabungan);
																							$ketemu = 0;
																							for($i=0;$i<30;$i++)
																							{
																								$wg = explode("^",@$wordgab[$i]);
																								$KelasID = @$wg[1];
																								$res = $this->m_kelas->PTL_select_kelas($KelasID);
																								if($res)
																								{
																									$kls = $res["Nama"];
																								}
																								else
																								{
																									$kls = "";
																								}
																								if($wg[0] != "")
																								{
																									$kelas .= @$wg[0].$kls.", ";
																								}
																								if($cekkelas == $KelasID)
																								{
																									$ketemu = 1;
																								}
																							}
																							if(($ketemu == 1) OR ($this->session->userdata('sch_filter_kelas') == "MERGE"))
																							{
																								echo "<tr class='warning'>
																										<td><font color='red'><b>MERGE<br/>($kelas)</b></font></td>
																										<td>
																											$rs2[Nama]
																											<br/>
																											".tgl_singkat_eng($r2->TglMulai)."
																											-
																											".tgl_singkat_eng($r2->TglSelesai)."
																										</td>
																										<td></td>
																										<td>$nmdsn</td>
																										<td>$ruang</td>
																										<td>$mulai -  $selesai</td>
																										<td>
																											<a class='btn btn-info' href='".site_url("schedule/ptl_edit/$JadwalID")."' title='JadwalID : $JadwalID ~ SubjekID : $SubjekID'>
																												<i class='fa fa-list'></i>
																											</a>";
													?>
																											<a class="btn btn-danger" href="<?php echo site_url("schedule/ptl_delete/$JadwalID/$HariID"); ?>" title="<?php echo "JadwalID : $JadwalID ~ SubjekID : $SubjekID ~ HariID : $HariID"; ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $JadwalID." - ".$HariID; ?>)?')">
																												<i class="fa fa-times"></i>
																											</a>
													<?php
																								echo "</td>
																								</tr>";
																								$rn++;
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													?>
												</tbody>
											</table>
								<?php
										}
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>