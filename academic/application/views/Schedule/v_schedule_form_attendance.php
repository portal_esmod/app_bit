		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form/$ProgramID/$TahunID/$JadwalID/$total_all"); ?>">Add New</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form_day_detail/$JadwalID/$total_all"); ?>">Add Day</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form_day_attendance/$JadwalID/$total_all"); ?>">Add Attendance (SCHED10)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												if($KelasID == 0)
												{
													$kelas = "";
													$wordgab = explode(".",$KelasIDGabungan);
													for($i=0;$i<30;$i++)
													{
														$wg = explode("^",@$wordgab[$i]);
														$KelasID = @$wg[1];
														$res = $this->m_kelas->PTL_select_kelas($KelasID);
														if($res)
														{
															$kls = $res["Nama"];
														}
														else
														{
															$kls = "";
														}
														if($i == 0)
														{
															$kelas .= @$wg[0].$kls;
														}
														else
														{
															if($wg[0] != "")
															{
																$kelas .= ", ".@$wg[0].$kls;
															}
														}
													}
													echo "<font color='red'><b>MERGE</b></font> ($kelas)";
												}
												else
												{
													$res = $this->m_kelas->PTL_select_kelas($KelasID);
													$NamaKelas = "";
													if($res)
													{
														$NamaKelas = $res["Nama"];
													}
													echo $TahunKe.$NamaKelas;
												}
											?>
										</td>
										<td>Date</td>
										<td>:</td>
										<td><?php echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai); ?></td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td>
											<?php
												$ressub = $this->m_subjek->PTL_select($SubjekID);
												$NamaSubjek = "";
												if($ressub)
												{
													$NamaSubjek = $ressub["Nama"];
												}
												echo $NamaSubjek;
											?>
										</td>
										<td>Teacher</td>
										<td>:</td>
										<td>
											<?php
												$resdos = $this->m_dosen->PTL_select($DosenID);
												$NamaDosen = "";
												if($resdos)
												{
													$NamaDosen = $resdos["Nama"];
												}
												echo $NamaDosen;
												$DosId1 = "";
												$DosId2 = "";
												$DosId3 = "";
												$DosId4 = "";
												$DosId5 = "";
												$DosId6 = "";
												if($DosenID != 0)
												{
													$DosenID = $DosenID;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen1 = "";
													if($resdos)
													{
														$NamaDosen1 = $resdos["Nama"];
													}
													$DosId1 = $DosenID;
												}
												if($DosenID2 != 0)
												{
													$DosenID = $DosenID2;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen2 = "";
													if($resdos)
													{
														$NamaDosen2 = $resdos["Nama"];
													}
													$DosId2 = $DosenID2;
												}
												if($DosenID3 != 0)
												{
													$DosenID = $DosenID3;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen3 = "";
													if($resdos)
													{
														$NamaDosen3 = $resdos["Nama"];
													}
													$DosId3 = $DosenID3;
												}
												if($DosenID4 != 0)
												{
													$DosenID = $DosenID4;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen4 = "";
													if($resdos)
													{
														$NamaDosen4 = $resdos["Nama"];
													}
													$DosId4 = $DosenID4;
												}
												if($DosenID5 != 0)
												{
													$DosenID = $DosenID5;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen5 = "";
													if($resdos)
													{
														$NamaDosen5 = $resdos["Nama"];
													}
													$DosId5 = $DosenID5;
												}
												if($DosenID6 != 0)
												{
													$DosenID = $DosenID6;
													$resdos = $this->m_dosen->PTL_select($DosenID);
													$NamaDosen6 = "";
													if($resdos)
													{
														$NamaDosen6 = $resdos["Nama"];
													}
													$DosId6 = $DosenID6;
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Total Day</td>
										<td>:</td>
										<td><?php echo "$RencanaKehadiran session / $total_all days in week = ".ceil($RencanaKehadiran/$total_all)." weeks"; ?></td>
										<td>Id Attendance</td>
										<td>:</td>
										<td><?php echo $JadwalID; ?></td>
									</tr>
								</table>
								<center><h3>Choose on what day this subject should be scheduled</h3></center>
								<form role="form" action="<?php echo site_url("schedule/ptl_form_day_attendance_save"); ?>" method="POST">
									<table class="table">
										<tbody>
											<?php
												$totatt = $RencanaKehadiran;
												echo "<tr class='success'>
														<td align='center'><b>#</b></td>
														<td align='center'><b>Date</b></td>
														<td align='center' colspan='2'><b>Time</b></td>
														<td align='center'><b>Room</b></td>
														<td align='center'><b>Teacher</b></td>
														<td align='center'><b>Notes</b></td>
													</tr>";
												$max_date = hitungHari($TglSelesai,$TglMulai) + 1;
												$nta = 0;
												$no = 1;
												$word = explode("-",$TglMulai);
												for($i=0;$i<$max_date;$i++)
												{
													$data = mktime(0,0,0,$word[1],$word[2]+$i,$word[0]);
													$date = date("Y-m-d",$data);
													$nama = date("l",$data);
													$total = $total_all;
													for($ti=1;$ti<=$total;$ti++)
													{
														$rowjadwalhari = $this->m_jadwal_hari->PTL_all_jadwal($JadwalID);
														if($rowjadwalhari)
														{
															$nrhr = 1;
															foreach($rowjadwalhari as $row)
															{
																if($ti == $nrhr)
																{
																	$HariID = $row->HariID;
																}
																$nrhr++;
															}
														}
														$reshari = $this->m_hari->PTL_select($HariID);
														if($reshari["Nama_en"] == $nama)
														{
															if($nta < $totatt)
															{
																$rowhari = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
																$AttJamMulai = "";
																$AttJamSelesai = "";
																$AttRuangID = "";
																if($rowhari)
																{
																	$AttJamMulai = $rowhari["JamMulai"];
																	$AttJamSelesai = $rowhari["JamSelesai"];
																	$AttRuangID = $rowhari["RuangID"];
																}
																$libur = 0;
																if($rowkalender)
																{
																	foreach(@$rowkalender as $rk)
																	{
																		$wordlbr = explode("-",$rk->tanggal_mulai);
																		if($rk->tanggal_selesai != "")
																		{
																			$max_date2 = hitungHari($rk->tanggal_selesai,$rk->tanggal_mulai);
																			for($i2=1;$i2<=$max_date2;$i2++)
																			{
																				$data2 = mktime(0,0,0,$wordlbr[1],$wordlbr[2]+$i2,$wordlbr[0]);
																				$date2 = date("Y-m-d",$data2);
																				if($date == $date2)
																				{
																					if($rk->tidak_kuliah == "Y")
																					{
																						$libur++;
																					}
																				}
																			}
																		}
																		if($date == $rk->tanggal_mulai)
																		{
																			if($rk->tidak_kuliah == "Y")
																			{
																				$libur++;
																			}
																		}
																	}
																}
																$peringatan = "";
																if($libur > 0)
																{
																	$peringatan = "style='background-color:#FBD6D7;' title='The date of the holiday. Switch to another date.'";
																}
																if($libur == 0)
																{
																	echo "
																		<tr>
																			<td>$no</td>
																			<td><input $peringatan type='text' name='attTanggal$no' id='dpSC$no' value='".substr($reshari["Nama_en"],0,3).", ".tgl_singkat_eng($date)."' class='form-control' required>
																				<input type='hidden' name='no$no' value='$no'>
																				<p class='check_result_attTanggal$no'></p>
																			</td>
																			<td>
																				<input $peringatan type='text' name='attJamMulai$no' value='".substr($AttJamMulai,0,5)."' id='check_JamMulai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																				<p class='check_result_attJamMulai$no'></p>
																			</td>
																			<td>
																				<input $peringatan type='text' name='attJamSelesai$no' value='".substr($AttJamSelesai,0,5)."' id='check_JamSelesai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																				<p class='check_result_attJamSelesai$no'></p>
																			</td>
																			<td>
																				<select $peringatan name='attRuangID$no' title='' id='check_RuangID$no' class='form-control round-form' required>";
																					if($rowruang)
																					{
																						foreach($rowruang as $rr)
																						{
																							echo "<option value='$rr->RuangID'";
																							if($AttRuangID == $rr->RuangID)
																							{
																								echo "selected";
																							}
																							echo ">$rr->RuangID - $rr->Nama</option>";
																						}
																					}
																				echo "</select>
																				<p class='check_result_attRuangID$no'></p>
																			</td>
																			<td>
																				<select $peringatan name='attDosenID$no' title='' class='form-control round-form' required>";
																					if($DosenID != 0)
																					{
																						echo "<option value='$DosId1'>$DosId1 - $NamaDosen1</option>";
																					}
																					if($DosenID2 != 0)
																					{
																						echo "<option value='$DosId2'>$DosId2 - $NamaDosen2</option>";
																					}
																					if($DosenID3 != 0)
																					{
																						echo "<option value='$DosId3'>$DosId3 - $NamaDosen3</option>";
																					}
																					if($DosenID4 != 0)
																					{
																						echo "<option value='$DosId4'>$DosId4 - $NamaDosen4</option>";
																					}
																					if($DosenID5 != 0)
																					{
																						echo "<option value='$DosId5'>$DosId5 - $NamaDosen5</option>";
																					}
																					if($DosenID6 != 0)
																					{
																						echo "<option value='$DosId6'>$DosId6 - $NamaDosen6</option>";
																					}
																			echo "</select>
																			</td>
																			<td><input $peringatan type='text' name='Catatan$no' class='form-control' onmouseover='this.focus()'></td>
																		</tr>
																		";
																	$nta++;
																	$no++;
																}
															}
														}
													}
												}
												$sisa = $RencanaKehadiran - ($no - 1);
												$peringatan = "";
												if($sisa > 0)
												{
													$peringatan = "style='background-color:#ED9C28;' title='Out of range.'";
												}
												for($ii=1;$ii<=$sisa;$ii++)
												{
													echo "
														<tr>
															<td>$no</td>
															<td><input $peringatan type='text' name='attTanggal$no' id='dpSC$no' value='' class='form-control' required>
																<input type='hidden' name='no$no' value='$no'>
																<p class='check_result_attTanggal$no'></p>
															</td>
															<td>
																<input $peringatan type='text' name='attJamMulai$no' value='00:00' id='check_JamMulai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																<p class='check_result_attJamMulai$no'></p>
															</td>
															<td>
																<input $peringatan type='text' name='attJamSelesai$no' value='00:00' id='check_JamSelesai$no' class='form-control' onkeydown='return angkaSaja(this, event);' onkeyup='javascript:tandaJam(this);' onmouseover='this.focus()' required>
																<p class='check_result_attJamSelesai$no'></p>
															</td>
															<td>
																<select $peringatan name='attRuangID$no' title='' id='check_RuangID$no' class='form-control round-form' required>";
																	if($rowruang)
																	{
																		foreach($rowruang as $rr)
																		{
																			echo "<option value='$rr->RuangID'>$rr->RuangID - $rr->Nama</option>";
																		}
																	}
																echo "</select>
																<p class='check_result_attRuangID$no'></p>
															</td>
															<td>
																<select $peringatan name='attDosenID$no' title='' class='form-control round-form' required>";
																	if($DosenID != 0)
																	{
																		echo "<option value='$DosId1'>$DosId1 - $NamaDosen1 A</option>";
																	}
																	if($DosenID2 != 0)
																	{
																		echo "<option value='$DosId2'>$DosId2 - $NamaDosen2 B</option>";
																	}
																	if($DosenID3 != 0)
																	{
																		echo "<option value='$DosId3'>$DosId3 - $NamaDosen3</option>";
																	}
																	if($DosenID4 != 0)
																	{
																		echo "<option value='$DosId4'>$DosId4 - $NamaDosen4</option>";
																	}
																	if($DosenID5 != 0)
																	{
																		echo "<option value='$DosId5'>$DosId5 - $NamaDosen5</option>";
																	}
																	if($DosenID6 != 0)
																	{
																		echo "<option value='$DosId6'>$DosId6 - $NamaDosen6</option>";
																	}
															echo "</select>
															</td>
															<td><input $peringatan type='text' name='Catatan$no' class='form-control' onmouseover='this.focus()'></td>
														</tr>
														";
													$no++;
												}
												echo "<input type='hidden' name='ProgramID' value='$ProgramID'>";
												echo "<input type='hidden' name='TahunID' value='$TahunID'>";
												echo "<input type='hidden' name='JadwalID' value='$JadwalID'>";
												echo "<input type='hidden' name='total' value='".($no - 1)."'>";
											?>
										</tbody>
									</table>
									<center>
										<br/>
										<a href="<?php echo site_url("schedule/ptl_form_day_detail/$JadwalID/$total_all"); ?>" class="btn btn-warning">Back</a>
										<button type="reset" class="btn btn-danger">Reset</button>
										<input type="submit" value="Finish" id="my_button" class="btn btn-primary">
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>