		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Schedule</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_edit/$JadwalID/$total_all"); ?>">Edit Schedule (SCHED13)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("schedule/ptl_edit_update"); ?>" method="POST">
										<div class="col-lg-6">
											<div class="form-group">
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<label>Class</label>
												<select name="KelasID" id="id_kelas" title="Class" class="form-control round-form" required>
													<option value="__">-- CLASS --</option>
													<?php
														$program = $ProgramID;
														if($program == "REG")
														{
															$init = "";
														}
														if($program == "INT")
														{
															$init = "O";
														}
														if($rowkelas)
														{
															foreach($rowkelas as $rk)
															{
																if($rk->ProgramID == $program)
																{
																	echo "<option value='$rk->KelasID"."__"."'";
																	if($KelasID == $rk->KelasID)
																	{
																		echo "selected";
																	}
																	echo ">$init$rk->Nama</option>";
																}
															}
														}
														if($SubjekID == "")
														{
															$sch_SubjekID = "";
														}
														else
														{
															$sch_SubjekID = $SubjekID." - ".$NamaSubjek;
														}
														if($DosenID == "")
														{
															$sch_DosenID = "";
														}
														else
														{
															$sch_DosenID = $DosenID." - ".$NamaDosen;
														}
														if($DosenID2 == "")
														{
															$sch_DosenID2 = "";
														}
														else
														{
															$sch_DosenID2 = $DosenID2." - ".$NamaDosen2;
														}
														if($DosenID3 == "")
														{
															$sch_DosenID3 = "";
														}
														else
														{
															$sch_DosenID3 = $DosenID3." - ".$NamaDosen3;
														}
													?>
													<option value="<?php echo "MERGE__".$KelasIDGabungan; ?>" <?php if($KelasID == "0"){ echo "selected"; } ?>>*** MERGE CLASS ***</option>
												</select>
												<p class="help-block"></p>
											</div>
											<span id="list_kelas"></span>
											<span id="list_hide">
												<?php
													if($KelasID == "0")
													{
														$data = "";
														$no = 0;
														$no2 = 0;
														$mapel = $this->m_kelas->dra_getKelas();
														for($i=1;$i<=3;$i++)
														{
															foreach($mapel as $mp)
															{
																if($mp['NA'] == "N")
																{
																	$no++;
																	$ck = "";
																	$ckd = $i."^".$mp["KelasID"].".";
																	if(stristr($KelasIDGabungan,$ckd))
																	{
																		$ck = "checked";
																	}
																	$data .= "<div class='col-lg-3'><input type='checkbox' name='merge$no' value='$i^$mp[KelasID]' $ck>&nbsp;&nbsp;$i$mp[Nama]</div>\n";
																	$no2++;
																}
															}
															$data .= "<div class='col-lg-12'></div>";
														}
														$data .= "<input type='hidden' name='total' value='$no'><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
														echo $data;
													}
												?>
											</span>
											<div class="form-group">
												<div class="col-lg-12">
													<br/>
													<label>Semester</label>
												</div>
												<select name="Sesi" title="Semester" class="form-control round-form" required>
													<option value=''>-- SEMESTER --</option>
													<?php
														echo "<option value='1'"; if($Sesi == 1) { echo "selected"; } echo ">SEMESTER 1</option>";
														echo "<option value='2'"; if($Sesi == 2) { echo "selected"; } echo ">SEMESTER 2</option>";
														echo "<option value='3'"; if($Sesi == 3) { echo "selected"; } echo ">SEMESTER 3</option>";
														echo "<option value='4'"; if($Sesi == 4) { echo "selected"; } echo ">SEMESTER 4</option>";
														echo "<option value='5'"; if($Sesi == 5) { echo "selected"; } echo ">SEMESTER 5</option>";
														echo "<option value='6'"; if($Sesi == 6) { echo "selected"; } echo ">SEMESTER 6</option>";
													?>
												</select>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Start Date</label>
												<input type="text" name="TglMulai" value="<?php echo $TglMulai; ?>" id="datepickerSC1" placeholder="yyyy-mm-dd" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>End Date</label>
												<input type="text" name="TglSelesai" value="<?php echo $TglSelesai; ?>" id="datepickerSC1-End" placeholder="yyyy-mm-dd" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Subjects</label>
												<input type="text" name="SubjekID" value="<?php echo $NamaSubjek; ?>" id="Subjek1" class="form-control" required>
												<p class="help-block"></p>
												<span id="hasil-cari"></span>
												<span id="hasil-hide">
													<b>Program Study</b>
													<br/>
													<?php
														$word = explode(".",$ProdiID);
														for($i=0;$i<10;$i++)
														{
															if(@$word[$i] != "")
															{
																$ProdiID = @$word[$i];
																$resprodi = $this->m_prodi->PTL_select($ProdiID);
																$prodi = "";
																if($resprodi)
																{
																	$prodi = $resprodi["Nama"];
																}
																echo "<input type='checkbox' name='prodi$i' value=".@$word[$i]." checked><b>&nbsp;&nbsp;".@$word[$i]." - $prodi $ProdiID</b><br/>";
															}
														}
													?>
												</span>
											</div>
											<div class="form-group">
												<label>Specialization</label>
												<select name="SpesialisasiID" title="Specialization" class="form-control round-form">
													<option value=''>-- SPECIALIZATION --</option>
													<?php
														if($rowspesialisasi)
														{
															foreach($rowspesialisasi as $row)
															{
																echo "<option value='$row->SpesialisasiID'";
																if($SpesialisasiID == $row->SpesialisasiID)
																{
																	echo "selected";
																}
																echo ">$row->Nama</option>";
															}
														}
													?>
												</select>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Lecturer</label>
												<input type="text" name="DosenID" value="<?php echo $NamaDosen; ?>" id="Dosen1" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 1</label>
												<input type="text" name="DosenID2" value="<?php echo $NamaDosen2; ?>" id="Dosen2" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 2</label>
												<input type="text" name="DosenID3" value="<?php echo $NamaDosen3; ?>" id="Dosen3" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 3</label>
												<input type="text" name="DosenID4" value="<?php echo $NamaDosen4; ?>" id="Dosen4" class="form-control" >
												<p class="help-block"></p>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Assistant 4</label>
												<input type="text" name="DosenID5" value="<?php echo $NamaDosen5; ?>" id="Dosen5" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 5</label>
												<input type="text" name="DosenID6" value="<?php echo $NamaDosen6; ?>" id="Dosen6" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Lecturer Attendance</label>
												<input type="int" name="TotAttendance" value="<?php echo $RencanaKehadiran; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Student Maximum Absent</label>
												<input type="text" name="TotAbsen" value="<?php echo $MaxAbsen; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Not Active?</label>
												<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "selected";}; ?> class="form-control">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Created By</label>
												<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Created Date</label>
												<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Edited By</label>
												<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Edited Date</label>
												<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("schedule"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<?php
												if($total_all == "")
												{
											?>
													<input type="submit" value="Update" id="my_button" class="btn btn-primary">
											<?php
												}
												else
												{
													echo "<input type='submit' value='Update' id='my_button2' class='btn btn-success'>
														<a href='".site_url("schedule/ptl_edit_day_detail/$JadwalID/$total_all")."' class='btn btn-primary'>Next</a>";
												}
											?>
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>