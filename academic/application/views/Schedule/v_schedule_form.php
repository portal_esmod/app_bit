		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("schedule"); ?>">Schedule</a>
							>>
							<a href="<?php echo site_url("schedule/ptl_form/$program/$tahun/$JadwalID/$total_all"); ?>">Add New (SCHED03)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $program; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$TahunID = $tahun;
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("schedule/ptl_form_day"); ?>" method="POST">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Class</label>
												<select name="KelasID" id="id_kelas" title="Class" class="form-control round-form" required>
													<option value=''>-- CLASS --</option>
													<?php
														if($program == "REG")
														{
															$init = "";
														}
														if($program == "INT")
														{
															$init = "O";
														}
														if($rowkelas)
														{
															foreach($rowkelas as $rk)
															{
																if($rk->ProgramID == $program)
																{
																	echo "<option value='$rk->KelasID'";
																	if($KelasID == $rk->KelasID)
																	{
																		echo "selected";
																	}
																	echo ">$init$rk->Nama</option>";
																}
															}
														}
														if($SubjekID == "")
														{
															$sch_SubjekID = "";
														}
														else
														{
															$ressub = $this->m_subjek->PTL_select($SubjekID);
															$NamaSub = "";
															if($ressub)
															{
																$NamaSub = $ressub['Nama'];
															}
															$sch_SubjekID = $SubjekID." - ".$NamaSub;
														}
														if($DosenID == 0)
														{
															$sch_DosenID = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID = $DosenID." - ".$NamaDosen;
														}
														if($DosenID2 == 0)
														{
															$sch_DosenID2 = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID2);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID2 = $DosenID2." - ".$NamaDosen;
														}
														if($DosenID3 == 0)
														{
															$sch_DosenID3 = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID3);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID3 = $DosenID3." - ".$NamaDosen;
														}
														if($DosenID4 == 0)
														{
															$sch_DosenID4 = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID4);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID4 = $DosenID4." - ".$NamaDosen;
														}
														if($DosenID5 == 0)
														{
															$sch_DosenID5 = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID5);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID5 = $DosenID5." - ".$NamaDosen;
														}
														if($DosenID6 == 0)
														{
															$sch_DosenID6 = "";
														}
														else
														{
															$resdos = $this->m_dosen->PTL_select($DosenID6);
															$NamaDosen = "";
															if($resdos)
															{
																$NamaDosen = $resdos['Nama'];
															}
															$sch_DosenID6 = $DosenID6." - ".$NamaDosen;
														}
													?>
													<option value="MERGE" <?php if($sch_Merge == "MERGE"){ echo "selected"; } ?>>*** MERGE CLASS ***</option>
												</select>
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<input type="hidden" name="program" value="<?php echo $program; ?>">
												<input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
												<input type="hidden" name="total_all" value="<?php echo $total_all; ?>">
												<p class="help-block"></p>
											</div>
											<span id="list_kelas"></span>
											<?php
												if($sch_Merge == "MERGE")
												{
													$data = "";
													$no = 0;
													$no2 = 0;
													$mapel = $this->m_kelas->dra_getKelas();
													for($i=1;$i<=3;$i++)
													{
														foreach($mapel as $mp)
														{
															if($mp['NA'] == "N")
															{
																$no++;
																$ck = "";
																$ckd = $i."^".$mp["KelasID"];
																if(stristr($KelasIDGabungan,$ckd))
																{
																	$ck = "checked";
																}
																$data .= "<div class='col-lg-3'><input type='checkbox' name='merge$no' value='$i^$mp[KelasID]' $ck>&nbsp;&nbsp;$i$mp[Nama]</div>\n";
																$no2++;
															}
														}
														$data .= "<div class='col-lg-12'></div>";
													}
													$data .= "<input type='hidden' name='total' value='$no'><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
													echo $data;
												}
											?>
											<div class="form-group">
												<div class="col-lg-12">
													<br/>
													<label>Semester</label>
												</div>
												<select name="Sesi" title="Semester" class="form-control round-form" required>
													<option value=''>-- SEMESTER --</option>
													<?php
														$sesi = $Sesi;
														echo "<option value='1'"; if($sesi == 1) { echo "selected"; } echo ">SEMESTER 1</option>";
														echo "<option value='2'"; if($sesi == 2) { echo "selected"; } echo ">SEMESTER 2</option>";
														echo "<option value='3'"; if($sesi == 3) { echo "selected"; } echo ">SEMESTER 3</option>";
														echo "<option value='4'"; if($sesi == 4) { echo "selected"; } echo ">SEMESTER 4</option>";
														echo "<option value='5'"; if($sesi == 5) { echo "selected"; } echo ">SEMESTER 5</option>";
														echo "<option value='6'"; if($sesi == 6) { echo "selected"; } echo ">SEMESTER 6</option>";
													?>
												</select>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Start Date</label>
												<input type="text" name="TglMulai" value="<?php echo $TglMulai; ?>" id="datepickerSC1" placeholder="yyyy-mm-dd" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>End Date</label>
												<input type="text" name="TglSelesai" value="<?php echo $TglSelesai; ?>" id="datepickerSC1-End" placeholder="yyyy-mm-dd" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Subjects</label>
												<input type="text" name="SubjekID" value="<?php echo $sch_SubjekID; ?>" id="Subjek1" class="form-control" required>
												<p class="help-block"></p>
												<span id="hasil-cari"></span>
											</div>
											<br/>
											<div class="form-group">
												<label>Specialization</label>
												<select name="SpesialisasiID" title="Specialization" class="form-control round-form">
													<option value=''>-- SPECIALIZATION --</option>
													<?php
														if($rowspesialisasi)
														{
															foreach($rowspesialisasi as $row)
															{
																echo "<option value='$row->SpesialisasiID'";
																if($SpesialisasiID == $row->SpesialisasiID)
																{
																	echo "selected";
																}
																echo ">$row->Nama</option>";
															}
														}
													?>
												</select>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Lecturer</label>
												<input type="text" name="DosenID" value="<?php echo $sch_DosenID; ?>" id="Dosen1" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 1</label>
												<input type="text" name="DosenID2" value="<?php echo $sch_DosenID2; ?>" id="Dosen2" class="form-control" >
												<p class="help-block"></p>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Assistant 2</label>
												<input type="text" name="DosenID3" value="<?php echo $sch_DosenID3; ?>" id="Dosen3" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 3</label>
												<input type="text" name="DosenID4" value="<?php echo $sch_DosenID4; ?>" id="Dosen4" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 4</label>
												<input type="text" name="DosenID5" value="<?php echo $sch_DosenID5; ?>" id="Dosen5" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 5</label>
												<input type="text" name="DosenID6" value="<?php echo $sch_DosenID6; ?>" id="Dosen6" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Lecturer Attendance</label>
												<input type="number" name="TotAttendance" value="<?php echo $RencanaKehadiran; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Student Maximum Absent</label>
												<input type="number" name="TotAbsen" value="<?php echo $MaxAbsen; ?>" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Not Active?</label>
												<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "selected";}; ?> class="form-control">
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("schedule"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<?php
												if($JadwalID == "")
												{
											?>
													<input type="submit" value="Save" id="my_button" class="btn btn-primary">
											<?php
												}
												else
												{
													echo "<input type='submit' value='Update' id='my_button2' class='btn btn-success'>
														<a href='".site_url("schedule/ptl_form_day_detail/$JadwalID/$total_all")."' class='btn btn-primary'>Next</a>";
												}
											?>
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>