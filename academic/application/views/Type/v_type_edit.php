		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Subject Type</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("type"); ?>">Subject Type</a>
							>>
							<a href="<?php echo site_url("type/ptl_edit/$JenisMKID"); ?>">Edit Subject Type</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("type/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="Singkatan" value="<?php echo $Singkatan; ?>" class="form-control" required>
											<input type="hidden" name="JenisMKID" value="<?php echo $JenisMKID; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Not Active?</label>
											</div>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("type"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>