		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subject Type</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("type"); ?>">Subject Type</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>Only one Subject Type that may be active.
								</div>
								<center><a href="<?php echo site_url("type/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
														echo "<td title='JenisMKID : $row->JenisMKID'>$no</td>
															<td>$row->Singkatan</td>
															<td>$row->Nama</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("type/ptl_edit/$row->JenisMKID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>