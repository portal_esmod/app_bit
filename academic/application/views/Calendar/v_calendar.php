		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Academic Calendar</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("calendar"); ?>">Academic Calendar (ACACA01)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a academic calendar. <a href="<?php echo site_url("tutorial/ptl_detail/ACACA01"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<?php
									if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
									{
								?>
										<center>
											<a href="<?php echo site_url("calendar/ptl_form"); ?>" class="btn btn-primary">Add New</a>
											<br/>
											<br/>
											<a href="<?php echo site_url("calendar_type"); ?>" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="<?php echo site_url("calendar/ptl_list"); ?>" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
										</center>
										<br/>
										<br/>
								<?php
									}
								?>
								<div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>