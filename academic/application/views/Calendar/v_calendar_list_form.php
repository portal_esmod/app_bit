		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Event</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("calendar"); ?>">Academic Calendar</a>
							>>
                            <a href="<?php echo site_url("calendar/ptl_list"); ?>">Event List</a>
							>>
							<a href="<?php echo site_url("calendar/ptl_list_form"); ?>">Add New Event (ACACA03)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("calendar/ptl_list_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="keterangan" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Start Date</font></label>
											<input type="text" name="tanggal_mulai" id="datepickerSC1" placeholder="yyyy-mm-dd" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>End Date</label>
											<input type="text" name="tanggal_selesai" id="datepickerSC1-End" placeholder="yyyy-mm-dd" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Start Time</label>
											<input type="text" name="jam_mulai" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" placeholder="HH:MM" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>End Time</label>
											<input type="text" name="jam_selesai" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" placeholder="HH:MM" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Type</font></label>
											<select name="id_kalender_jenis" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowjenis)
													{
														foreach($rowjenis as $rj)
														{
															echo "<option value='$rj->id_kalender_jenis'>$rj->id_kalender_jenis - $rj->nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>No Class?</label>
											</div>
											<input type="checkbox" name="tidak_kuliah" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("calendar/ptl_list"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>