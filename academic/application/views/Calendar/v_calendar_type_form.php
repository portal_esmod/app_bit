		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Type</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("calendar"); ?>">Academic Calendar</a>
							>>
                            <a href="<?php echo site_url("calendar_type"); ?>">Calendar Type</a>
							>>
							<a href="<?php echo site_url("calendar_type/ptl_form"); ?>">Add New Type (ACACA10)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("calendar_type/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="red">Code</font></label>
											<input type="text" name="id_kalender_jenis" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="nama" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Description</label>
											<textarea name="keterangan" class="form-control"></textarea>
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("calendar_type"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>