		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Calendar Type</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("calendar"); ?>">Academic Calendar</a>
							>>
                            <a href="<?php echo site_url("calendar_type"); ?>">Calendar Type (ACACA09)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of calendar type.
								</div>
								<center><a href="<?php echo site_url("calendar_type/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Description</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->na == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td title='id : $row->id'>$no</td>
															<td>$row->id_kalender_jenis</td>
															<td>$row->nama</td>
															<td>$row->keterangan</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("calendar_type/ptl_edit/$row->id")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>