		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Calendar List</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("calendar"); ?>">Academic Calendar</a>
							>>
                            <a href="<?php echo site_url("calendar/ptl_list"); ?>">Event List (ACACA02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of event.
								</div>
								<center>
									<a href="<?php echo site_url("calendar/ptl_list_form"); ?>" class="btn btn-primary">Add New</a>
									<?php
										if($rowrecord)
										{
									?>
											<br/>
											<br/>
											<a href="<?php echo site_url("calendar/ptl_delete_all"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete all data?\n\nTHIS ACTION CAN NOT BE RESTORED.')">Delete All</a>
									<?php
										}
									?>
								</center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Event Name</th>
											<th>Start Date</th>
											<th>End Date</th>
											<th>Start Time</th>
											<th>End Time</th>
											<th>No Class?</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->na == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td title='id_kalender : $row->id_kalender'>$no</td>
															<td>$row->keterangan</td>
															<td>$row->tanggal_mulai</td>
															<td>$row->tanggal_selesai</td>
															<td>$row->jam_mulai</td>
															<td>$row->jam_selesai</td>
															<td align='center'><img src='".base_url("assets/dashboard/img/$row->tidak_kuliah.gif")."'></td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("calendar/ptl_list_edit/$row->id_kalender")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>