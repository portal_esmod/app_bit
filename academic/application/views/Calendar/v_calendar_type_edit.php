		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Type</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("calendar"); ?>">Academic Calendar</a>
							>>
                            <a href="<?php echo site_url("calendar_type"); ?>">Calendar Type</a>
							>>
							<a href="<?php echo site_url("calendar_type/ptl_form"); ?>">Add New Type (ACACA12)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("calendar_type/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="red">Code</font></label>
											<input type="text" name="id_kalender_jenis" value="<?php echo $id_kalender_jenis; ?>" class="form-control" required>
											<input type="hidden" name="id" value="<?php echo $id; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="nama" value="<?php echo $nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Description</label>
											<textarea name="keterangan" class="form-control"><?php echo $keterangan; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="na" value="Y" class="form-control" <?php if($na == "Y"){ echo "checked"; } ?>>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("calendar_type"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>