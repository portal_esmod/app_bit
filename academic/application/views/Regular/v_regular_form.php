		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("regular"); ?>">Regular</a>
							>>
							<a href="<?php echo site_url("regular/ptl_form"); ?>">Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("regular/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="ProdiID" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Program</label>
											<select name="ProgramID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowprogram)
													{
														foreach($rowprogram as $rp)
														{
															echo "<option value='$rp->ProgramID'>$rp->ProgramID - $rp->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Combination Prodi?</label>
											<input type="checkbox" name="Kombinasi" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Faculty</label>
											<input type="text" name="KodeID" value="ESMOD" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="int" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Degree</label>
											<input type="text" name="Jenjang" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="Gelar" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>SIN Format</label>
											<input type="text" name="FormatNIM" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Gunakan NIM Sementara?</label>
											<input type="text" name="GunakanNIMSementara" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Can Change To Prodi</label>
											<input type="text" name="DapatPindahProdi" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Session Name</label>
											<input type="text" name="NamaSesi" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Prerequisites Check</label>
											<input type="checkbox" name="CekPrasyarat" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Total SKS Graduated</label>
											<input type="text" name="TotalSKS" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default SKS</label>
											<input type="text" name="DefSKS" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default Attendance</label>
											<input type="text" name="DefKehadiran" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default Maximun Absent</label>
											<input type="text" name="DefMaxAbsen" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Kode Prodi Dikti</label>
											<input type="text" name="NoSKDikti" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Pajak Honor Dosen</label>
											<input type="text" name="Biaya" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Official Name</label>
											<input type="text" name="Pejabat" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Titles</label>
											<input type="text" name="Jabatan" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Maximun Study</label>
											<input type="text" name="BatasStudi" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Total Session/Year</label>
											<input type="text" name="JumlahSesi" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>No SK Dikti</label>
											<input type="text" name="NoSKDikti" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of SK Dikti</label>
											<input type="text" name="TglSKDikti" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>No SK BAN</label>
											<input type="text" name="NoSKBAN" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of SK BAN</label>
											<input type="text" name="TglSKBAN" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Akreditasi</label>-->
											<input type="hidden" name="Akreditasi" class="form-control">
											<!--<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("regular"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>