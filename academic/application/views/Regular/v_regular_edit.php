		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <a href="<?php echo site_url("regular"); ?>">Regular</a>
							>>
							<a href="<?php echo site_url("regular/ptl_edit/$ProdiID"); ?>">Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("regular/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input readonly type="text" name="ProdiID" value="<?php echo $ProdiID; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Program</label>
											<select name="ProgramID" class="form-control">
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowprogram)
													{
														foreach($rowprogram as $rp)
														{
															echo "<option value='$rp->ProgramID'";
															if($rp->ProgramID == $ProgramID)
															{
																echo "selected";
															}
															echo ">$rp->ProgramID - $rp->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Combination Prodi?</label>
											<input type="checkbox" name="Kombinasi" value="Y" <?php if($Kombinasi == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Faculty</label>
											<input type="text" name="KodeID" value="<?php echo $KodeID; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="int" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Degree</label>
											<input type="text" name="Jenjang" value="<?php echo $Jenjang; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="Gelar" value="<?php echo $Gelar; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>SIN Format</label>
											<input type="text" name="FormatNIM" value="<?php echo $FormatNIM; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Gunakan NIM Sementara?</label>
											<input type="text" name="GunakanNIMSementara" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Can Change To Prodi</label>
											<input type="text" name="DapatPindahProdi" value="<?php echo $DapatPindahProdi; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Session Name</label>
											<input type="text" name="NamaSesi" value="<?php echo $NamaSesi; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Prerequisites Check</label>
											<input type="checkbox" name="CekPrasyarat" value="Y" <?php if($CekPrasyarat == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Total SKS Graduated</label>
											<input type="text" name="TotalSKS" value="<?php echo $TotalSKS; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default SKS</label>
											<input type="text" name="DefSKS" value="<?php echo $DefSKS; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default Attendance</label>
											<input type="text" name="DefKehadiran" value="<?php echo $DefKehadiran; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Default Maximun Absent</label>
											<input type="text" name="DefMaxAbsen" value="<?php echo $DefMaxAbsen; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Kode Prodi Dikti</label>
											<input type="text" name="NoSKDikti" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Pajak Honor Dosen</label>
											<input type="text" name="Biaya" class="form-control">
											<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Official Name</label>
											<input type="text" name="Pejabat" value="<?php echo $Pejabat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Titles</label>
											<input type="text" name="Jabatan" value="<?php echo $Jabatan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Maximun Study</label>
											<input type="text" name="BatasStudi" value="<?php echo $BatasStudi; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Total Session/Year</label>
											<input type="text" name="JumlahSesi" value="<?php echo $JumlahSesi; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>No SK Dikti</label>
											<input type="text" name="NoSKDikti" value="<?php echo $NoSKDikti; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of SK Dikti</label>
											<input type="text" name="TglSKDikti" value="<?php echo $TglSKDikti; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>No SK BAN</label>
											<input type="text" name="NoSKBAN" value="<?php echo $NoSKBAN; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date of SK BAN</label>
											<input type="text" name="TglSKBAN" value="<?php echo $TglSKBAN; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<!--<div class="form-group">
											<label>Accreditation</label>-->
											<input type="hidden" name="Akreditasi" value="<?php echo $Akreditasi; ?>" class="form-control">
											<!--<p class="help-block"></p>
										</div>-->
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("regular"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>