		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Schedule</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("remedial"); ?>">Short Semester</a>
							>>
							<a href="<?php echo site_url("remedial/ptl_edit/$JadwalRemedialID"); ?>">Edit Schedule (REMED09)</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("remedial/ptl_edit_update"); ?>" method="POST">
									<?php
										$tahun1 = $this->session->userdata('remedial_filter_tahun1');
										$yearID1 = "<font color='red'>(Year Code : $TahunID Not selected)</font>";
										$readonly1 = "readonly";
										if($tahun1 != "")
										{
											$yearID1 = "(Year Code : $tahun1)";
											$readonly1 = "";
										}
										$tahun2 = $this->session->userdata('remedial_filter_tahun2');
										$yearID2 = "<font color='red'>(Year Code : $TahunID2 Not selected)</font>";
										$readonly2 = "readonly";
										if($tahun2 != "")
										{
											$yearID2 = "(Year Code : $tahun2)";
											$readonly2 = "";
										}
									?>
									<div class="col-lg-6">
										<div class="form-group">
											<label>Start Date</label>
											<input type="text" name="TglMulai" value="<?php echo $TglMulai; ?>" id="datepicker1" class="form-control">
											<input type="hidden" name="JadwalRemedialID" value="<?php echo $JadwalRemedialID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>End Date</label>
											<input type="text" name="TglSelesai" value="<?php echo $TglSelesai; ?>" id="datepicker2" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Time (Start - End)</label>
											<br/>
											<div class="col-lg-3">
												<input type="text" name="JamMulai" value="<?php echo substr($JamMulai,0,5); ?>" class="form-control" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" required>
											</div>
											<div class="col-lg-3">
												<input type="text" name="JamSelesai" value="<?php echo substr($JamSelesai,0,5); ?>" class="form-control" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" required>
											</div>
											<p class="help-block"></p>
										</div>
										<br/>
										<br/>
										<div class="form-group">
											<label>Subject Regular 1 <?php echo $yearID1; ?></label>
											<input <?php echo $readonly1; ?> type="text" name="SubjekID" value="<?php echo $SubjekID; ?>" id="SubjekRemedial1" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject Regular 2 <?php echo $yearID1; ?></label>
											<input <?php echo $readonly1; ?> type="text" name="SubjekID12" value="<?php echo $SubjekID12; ?>" id="SubjekRemedial2" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject Intensive <?php echo $yearID2; ?></label>
											<input <?php echo $readonly2; ?> type="text" name="SubjekID2" value="<?php echo $SubjekID2; ?>" id="SubjekRemedial3" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Classroom Allocation</label>
											<select name="RuangID" title="" class="form-control round-form" required>
													<option value=''>-- ROOM --</option>
													<?php
														if($rowruang)
														{
															foreach($rowruang as $rr)
															{
																echo "<option value='$rr->RuangID'";
																if($RuangID == $rr->RuangID)
																{
																	echo "selected";
																}
																echo ">$rr->RuangID - $rr->Nama</option>";
															}
														}
													?>
												</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Supervisor</label>
											<input type="text" name="DosenID" value="<?php echo $DosenID; ?>" id="Dosen3" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("remedial"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<?php
												if($this->session->userdata('remedial_JadwalRemedialID') == "")
												{
											?>
													<button type="submit" class="btn btn-primary">Update</button>
											<?php
												}
												else
												{
													echo "<a href='".site_url("remedial/ptl_student_form")."' class='btn btn-primary'>Next</a>";
												}
											?>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>