		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Allocation</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("remedial"); ?>">Short Semester</a>
							>>
							<a href="<?php echo site_url("remedial/ptl_form/$JadwalRemedialID"); ?>">Add New Schedule</a>
							>>
							<a href="<?php echo site_url("remedial/ptl_student_form/$JadwalRemedialID"); ?>">Student Allocation (REMED06)</a>
                        </div>
                    </div>
                </div>
            </div>
			 <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Academic Year Regular</th>
										<th>:</th>
										<th>
											<?php
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Academic Year Intensive</th>
										<th>:</th>
										<th>
											<?php
												$TahunID = $TahunID2;
												$retahun = $this->m_year->PTL_select($TahunID);
												if($retahun)
												{
													echo $TahunID." - ".$retahun['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Regular 1</th>
										<th>:</th>
										<th>
											<?php
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Subject Regular 2</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID12;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID12." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
									</tr>
									<tr>
										<th>Subject Intensive</th>
										<th>:</th>
										<th>
											<?php
												$SubjekID = $SubjekID2;
												$ressubjek = $this->m_subjek->PTL_select($SubjekID);
												if($ressubjek)
												{
													echo $SubjekID2." - ".$ressubjek['Nama'];
												}
												else
												{
													echo "-";
												}
											?>
										</th>
										<th>Lecturer</th>
										<th>:</th>
										<th>
											<?php
												$dosen = "-";
												$d = $this->m_dosen->PTL_select($DosenID);
												$dosen = "<font color='red'><b>No Supervisor</b></font>";
												if($d)
												{
													$dosen = $DosenID." - ".$d["Nama"];
												}
												echo $dosen;
											?>
										</th>
									</tr>
								</thead>
							</table>
						</div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Students List</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<form action="<?php echo site_url("remedial/ptl_filter_exam"); ?>" method="POST">
									<select name="cekexam" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
										<option value=''>-- EXAM SCORE --</option>
										<?php
											$cekexam = $this->session->userdata('remedial_filter_exam');
											echo "<option value='A'"; if($cekexam == 'A'){ echo "selected"; } echo ">EXAM SCORE A</option>";
											echo "<option value='B'"; if($cekexam == 'B'){ echo "selected"; } echo ">EXAM SCORE B</option>";
											echo "<option value='C'"; if($cekexam == 'C'){ echo "selected"; } echo ">EXAM SCORE C</option>";
											echo "<option value='D'"; if($cekexam == 'D'){ echo "selected"; } echo ">EXAM SCORE D</option>";
											echo "<option value='E'"; if($cekexam == 'E'){ echo "selected"; } echo ">EXAM SCORE E</option>";
											echo "<option value='F'"; if($cekexam == 'F'){ echo "selected"; } echo ">EXAM SCORE F</option>";
										?>
									</select>
									<input type="hidden" name="JadwalRemedialID" value="<?php echo $JadwalRemedialID; ?>">
									<noscript><input type="submit" value="Submit"></noscript>
								</form>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Student Name & ID</th>
											<th>Sequence</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$ExamID = "";
											$grade = "";
											$Score = "";
											if($rowrecord)
											{
												$TahunID = $this->session->userdata('remedial_filter_tahun1');
												$TahunID2 = $this->session->userdata('remedial_filter_tahun2');
												if($SubjekID == 0)
												{
													$SubjekID = "";
												}
												if($SubjekID12 == 0)
												{
													$SubjekID12 = "";
												}
												if($SubjekID2 == 0)
												{
													$SubjekID2 = "";
												}
												$no = 1;
												foreach($rowrecord as $row)
												{
													if(($row->TahunID == $AddTahunID) OR ($row->TahunID == $AddTahunID2))
													{
														// echo "$row->SubjekID - $SubjekID12 - $SubjekID<br/>";
														if(($row->SubjekID == $AddSubjekID) OR ($row->SubjekID == $AddSubjekID12) OR ($row->SubjekID == $AddSubjekID2))
														{
															$TahunID = $AddTahunID;
															$TahunID2 = $AddTahunID2;
															$SubjekID = "";
															if($AddSubjekID != "0")
															{
																$SubjekID = $AddSubjekID;
															}
															$SubjekID12 = "";
															if($AddSubjekID12 != "0")
															{
																$SubjekID12 = $AddSubjekID12;
															}
															$SubjekID2 = "";
															if($AddSubjekID2 != "0")
															{
																$SubjekID2 = $AddSubjekID2;
															}
															$MhswID = $row->MhswID;
															$resexam = $this->m_exam->PTL_all_select_remedial($TahunID,$TahunID2,$SubjekID,$SubjekID12,$SubjekID2);
															$ExamID = "";
															$Score = "-";
															$grade = "";
															if($resexam)
															{
																$ExamID = $resexam['ExamID'];
																$SubjekID = $row->SubjekID;
																$rowsub = $this->m_subjek->PTL_select($SubjekID);
																$KurikulumID = "";
																if($rowsub)
																{
																	$KurikulumID = $rowsub['KurikulumID'];
																}
																$resnilaiexam = $this->m_exam_mahasiswa->PTL_all_evaluation($ExamID,$MhswID);
																if($resnilaiexam)
																{
																	$Score = $resnilaiexam['Nilai'];
																	$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
																	if($rownilai)
																	{
																		foreach($rownilai as $rn)
																		{
																			if((number_format($Score,2) >= $rn->NilaiMin) AND (number_format($Score,2) <= $rn->NilaiMax))
																			{
																				$grade = $rn->Nama;
																			}
																		}
																	}
																}
															}
															$rmhs = $this->m_mahasiswa->PTL_select($MhswID);
															$mhsw = "";
															if($rmhs)
															{
																$mhsw = $rmhs["Nama"];
															}
															// echo "$mhsw - $Score - $grade<br/>";
															$rexam = $this->m_remedial_krs->PTL_select($JadwalRemedialID,$MhswID);
															if($rexam)
															{
																$cls = "class='danger'";
																$inp = "<p align='center'>".$rexam["NomorKursi"]."</p>";
																$KRSRemedialID = $rexam["KRSRemedialID"];
															}
															else
															{
																$cls = "";
																$inp = "<input type='number' name='NomorKursi' min='1' max='$kapasitas' style='width:50px;' required>";
																$KRSRemedialID = "";
															}
															if($cekexam == "")
															{
																echo "
																	<tr $cls>
																		<td title='KRSRemedialID : $KRSRemedialID ~ ExamID : $ExamID ~ Grade : $grade ~ SubjekID : $row->SubjekID'>$no</td>
																		<td><a href='".site_url("evaluation/ptl_cari/$MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$MhswID</b></a> - <a href='".site_url("students/ptl_edit/$MhswID")."' title='Go to Personal Information' target='_blank'>$mhsw</a></td>
																			<form action='".site_url("remedial/ptl_student_form_set")."' method='POST'>
																				<td title='Exam Score: $Score'>
																					$inp
																					<br/>
																					Exam Score: $Score
																					<input type='hidden' name='MhswID' value='$MhswID'>
																					<input type='hidden' name='JadwalRemedialID' value='$JadwalRemedialID'>
																					<input type='hidden' name='SubjekID' value='$row->SubjekID'>
																					<input type='hidden' name='kapasitas' value='$kapasitas'>
																				</td>
																				<td>";
																				if($cls == "")
																				{
																					echo "<input type='submit' value='+' class='btn btn-success btn-circle'>";
																				}
																				else
																				{
																					echo "<a href='".site_url("remedial/ptl_student_form_delete/$KRSRemedialID/$JadwalRemedialID")."' class='btn btn-danger btn-circle'>-</i>";
																				}
																			echo "</td>
																			</form>";
																echo "</tr>
																	";
																$no++;
															}
															else
															{
																if($cekexam == $grade)
																{
																	echo "
																		<tr $cls>
																			<td title='KRSRemedialID : $KRSRemedialID ~ ExamID : $ExamID ~ Grade : $grade ~ SubjekID : $row->SubjekID'>$no</td>
																			<td>$row->MhswID - $mhsw $grade</td>
																				<form action='".site_url("remedial/ptl_student_form_set")."' method='POST'>
																					<td title='Exam Score: $Score'>
																						$inp
																						<br/>
																						Exam Score: $Score
																						<input type='hidden' name='MhswID' value='$MhswID'>
																						<input type='hidden' name='JadwalRemedialID' value='$JadwalRemedialID'>
																						<input type='hidden' name='SubjekID' value='$row->SubjekID'>
																						<input type='hidden' name='kapasitas' value='$kapasitas'>
																					</td>
																					<td>";
																					if($cls == "")
																					{
																						echo "<input type='submit' value='+' class='btn btn-success btn-circle'>";
																					}
																					else
																					{
																						echo "<a href='".site_url("remedial/ptl_student_form_delete/$KRSRemedialID/$JadwalRemedialID")."' class='btn btn-danger btn-circle'>-</i>";
																					}
																				echo "</td>
																				</form>";
																	echo "</tr>
																		";
																	$no++;
																}
															}
														}
													}
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Mapping Room <?php echo $RuangID; ?></center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<?php
										$klm = 1;
										for($i=1;$i<=$kapasitas;$i++)
										{
											$NomorKursi = $i;
											$rkursi = $this->m_remedial_krs->PTL_select_kursi($JadwalRemedialID,$NomorKursi);
											if($rkursi)
											{
												$KRSRemedialID = $rkursi["KRSRemedialID"];
												$MhswID = $rkursi["MhswID"];
												$mhsw = " - ".$rkursi["MhswID"];
												$r = $this->m_mahasiswa->PTL_select($MhswID);
												$foto = "foto_umum/user.jpg";
												$foto_preview = "foto_umum/user.jpg";
												$nama = "";
												if($r)
												{
													if($r["Foto"] != "")
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("ptl_storage/$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$foto_preview = "foto_umum/user.jpg";
														}
													}
													$nama = "<span class='label label-info'>$r[Nama]</span>";
												}
												$img = "
													<a class='fancybox' title='$r[MhswID] - $r[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
														<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='80px' alt='' />
													</a>
													";
											}
											else
											{
												$KRSRemedialID = "";
												$mhsw = "";
												$nama = "";
												$img = "<img src='".base_url("assets/dashboard/img/kursi.jpg")."' width='80px'/>";
											}
											if($klm == 4)
											{
												$klm = 1;
												echo "<td title='KRSRemedialID: $KRSRemedialID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
													</tr>";
											}
											else
											{
												if($klm == 1)
												{
													echo "
														<tr>
															<td title='KRSRemedialID: $KRSRemedialID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>
														";
												}
												else
												{
													echo "<td title='KRSRemedialID: $KRSRemedialID'><span class='label label-success'>$i$mhsw</span><br/>$nama<br/>$img</td>";
												}
												$klm++;
											}
										}
									?>
								</table>
								<center>
									<a href="<?php echo site_url("remedial/ptl_form/$JadwalRemedialID"); ?>" class="btn btn-warning">Back</a>
									<a href="<?php echo site_url("remedial"); ?>" class="btn btn-success">Done</a>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>