		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Short Semester</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("remedial"); ?>">Short Semester (REMED02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This page is used to create Short Semester schedule. <a href="<?php echo site_url("tutorial/ptl_detail/RMDL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table class="table">
									<tr class="info">
										<td width="10px"><b>Filter</b></td>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_tahun1"); ?>" method="POST">
												<select name="cektahun1" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR REGULAR --</option>
													<?php
														$cektahun1 = $this->session->userdata('remedial_filter_tahun1');
														if($rowtahun1)
														{
															foreach($rowtahun1 as $rt1)
															{
																$f = "";
																if($rt1->NA == "N")
																{
																	$f = "style='background-color: #5BB734;'";
																}
																echo "<option value='$rt1->TahunID' $f";
																if($cektahun1 == $rt1->TahunID)
																{
																	echo "selected";
																}
																echo ">$rt1->TahunID - $rt1->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_tahun2"); ?>" method="POST">
												<select name="cektahun2" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR INTENSIVE --</option>
													<?php
														$cektahun2 = $this->session->userdata('remedial_filter_tahun2');
														if($rowtahun2)
														{
															foreach($rowtahun2 as $rt2)
															{
																$f = "";
																if($rt2->NA == "N")
																{
																	$f = "style='background-color: #5BB734;'";
																}
																echo "<option value='$rt2->TahunID' $f";
																if($cektahun2 == $rt2->TahunID)
																{
																	echo "selected";
																}
																echo ">$rt2->TahunID - $rt2->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_dosen"); ?>" method="POST">
												<select name="cekdosen" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- TEACHER --</option>
													<?php
														$cekdosen = $this->session->userdata('remedial_filter_dosen');
														if($rowdosen)
														{
															foreach($rowdosen as $rd)
															{
																echo "<option value='$rd->Login'";
																if($cekdosen == $rd->Login)
																{
																	echo "selected";
																}
																echo ">$rd->Login - $rd->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_ruang"); ?>" method="POST">
												<select name="cekruang" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ROOM --</option>
													<?php
														$cekruang = $this->session->userdata('remedial_filter_ruang');
														if($rowruang)
														{
															foreach($rowruang as $rr)
															{
																echo "<option value='$rr->RuangID'";
																if($cekruang == $rr->RuangID)
																{
																	echo "selected";
																}
																echo ">$rr->RuangID - $rr->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<?php
										if(($this->session->userdata('remedial_filter_tahun1') != "") OR ($this->session->userdata('remedial_filter_tahun2') != ""))
										{
									?>
											<tr>
												<td width="10px"><b>Setup</b></td>
												<td>
													<table>
														<tr>
															<td><a href="<?php echo site_url("remedial/ptl_form"); ?>" class="btn btn-info">Add Short Semester</a></td>
															<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															<td><a href="<?php echo site_url("remedial/ptl_pdf_list"); ?>" class="btn btn-info">Print Schedules List</a></td>
														</tr>
													</table>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<a href="<?php echo site_url("remedial/ptl_card"); ?>" class="btn btn-info">Short Semester Card</a>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Subject Reguler 1<br/>Subject Reguler 2</th>
											<th>Subject Intensive</th>
											<th>Classroom Allocation</th>
											<th>Start Date</th>
											<th>Finish Date</th>
											<th>Time</th>
											<th>Short Semester Supervisor</th>
											<th colspan="5">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if(($this->session->userdata('remedial_filter_tahun1') != "") OR ($this->session->userdata('remedial_filter_tahun2') != ""))
											{
												if($rowrecord)
												{
													$no = 1;
													$tot = 0;
													foreach($rowrecord as $row)
													{
														$SubjekID = $row->SubjekID;
														$s1 = $this->m_subjek->PTL_select($SubjekID);
														$subjek1 = "";
														if($s1)
														{
															$subjek1 = $s1["Nama"];
														}
														$SubjekID = $row->SubjekID12;
														$s2 = $this->m_subjek->PTL_select($SubjekID);
														$subjek2 = "";
														if($s2)
														{
															$subjek2 = $s2["Nama"];
														}
														$SubjekID = $row->SubjekID2;
														$s3 = $this->m_subjek->PTL_select($SubjekID);
														$subjek3 = "";
														if($s3)
														{
															$subjek3 = $s3["Nama"];
														}
														$DosenID = $row->DosenID;
														$d = $this->m_dosen->PTL_select($DosenID);
														$dosen = "<font color='red'><b>No Supervisor</b></font>";
														if($d)
														{
															$dosen = $d["Nama"];
														}
														if($cekdosen == "")
														{
															echo "<tr class='info'>
																	<td>$no</td>
																	<td>$row->JadwalRemedialID</td>
																	<td title='$row->SubjekID - $row->SubjekID12'>$subjek1<br/>$subjek2</td>
																	<td title='$row->SubjekID2'>$subjek3</td>
																	<td>$row->UjianRuangID</td>
																	<td>".tgl_singkat_eng($row->TglMulai)."</td>
																	<td>".tgl_singkat_eng($row->TglSelesai)."</td>
																	<td>$row->JamMulai<br/>$row->JamSelesai</td>
																	<td>$dosen</td>
																	<td>
																		<a href='".site_url("remedial/ptl_edit/$row->JadwalRemedialID")."' title='Edit Exam' class='btn btn-primary'><i class='fa fa-list'></i></a>
																	</td>
																	<td>
																		<a href='".site_url("remedial/ptl_pdf_class/$row->JadwalRemedialID/$row->UjianRuangID")."' title='Print Attendance' class='btn btn-success'><i class='fa fa-print'></i></a>
																	</td>
																	<td>
																		<a href='".site_url("remedial/ptl_attendance/$row->JadwalRemedialID/$row->UjianRuangID")."' title='Set Attendance' class='btn btn-warning'><i class='fa fa-pencil'></i></a>
																	</td>
																	<td>
																		<a href='".site_url("remedial/ptl_scoring/$row->JadwalRemedialID/$row->UjianRuangID")."' title='Set Score' class='btn btn-danger'><i class='fa fa-pencil'></i></a>
																	</td>
																	<td>";
										?>
																		<a href="<?php echo site_url("remedial/ptl_delete/$row->JadwalRemedialID"); ?>" title="Delete Exam Permanenthly" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalRemedialID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
																			<i class="fa fa-times"></i>
																		</a>
										<?php
																echo "</td>
																	";
															$no++;
															$tot++;
														}
														else
														{
															if($DosenID == $cekdosen)
															{
																echo "<tr class='info'>
																		<td>$no</td>
																		<td>$row->JadwalRemedialID</td>
																		<td title='$row->SubjekID - $row->SubjekID12'>$subjek1<br/>$subjek2</td>
																		<td title='$row->SubjekID2'>$subjek3</td>
																		<td>$row->UjianRuangID</td>
																		<td>".tgl_singkat_eng($row->TglMulai)."</td>
																		<td>".tgl_singkat_eng($row->TglSelesai)."</td>
																		<td>$row->JamMulai<br/>$row->JamSelesai</td>
																		<td>$dosen</td>
																		<td>
																			<a href='".site_url("remedial/ptl_edit/$row->JadwalRemedialID")."' title='Edit Exam' class='btn btn-primary'><i class='fa fa-list'></i></a>
																		</td>
																		<td>
																			<a href='".site_url("remedial/ptl_pdf_class/$row->JadwalRemedialID/$row->RuangID")."' title='Print Attendance' class='btn btn-success'><i class='fa fa-print'></i></a>
																		</td>
																		<td>
																			<a href='".site_url("remedial/ptl_attendance/$row->JadwalRemedialID/$row->RuangID")."' title='Set Attendance' class='btn btn-warning'><i class='fa fa-pencil'></i></a>
																		</td>
																		<td>
																			<a href='".site_url("remedial/ptl_scoring/$row->JadwalRemedialID/$row->RuangID")."' title='Set Score' class='btn btn-danger'><i class='fa fa-pencil'></i></a>
																		</td>";
										?>
																		<a href="<?php echo site_url("remedial/ptl_delete/$row->JadwalRemedialID"); ?>" title="Delete Exam Permanenthly" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalRemedialID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
																			<i class="fa fa-times"></i>
																		</a>
										<?php
																echo "</td>
																		";
																$no++;
																$tot++;
															}
														}
													}
													if($tot == 0)
													{
														echo "<tr>
																<td colspan='9'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												}
												else
												{
													echo "<tr>
															<td colspan='9'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											}
											else
											{
												echo "<tr>
														<td colspan='9'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>