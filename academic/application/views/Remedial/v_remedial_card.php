			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Short Semester Card</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("remedial"); ?>">Short Semester</a>
							>>
							<a href="<?php echo site_url("remedial/ptl_card"); ?>">Short Semester Card (REMED22)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Evaluation Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('remedial_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("remedial/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('remedial_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	$f = "";
																	if($rt->NA == "N")
																	{
																		$f = "style='background-color: #5BB734;'";
																	}
																	echo "<option value='$rt->TahunID' $f";
																	if($cektahun == $rt->TahunID)
																	{
																		echo "selected";
																	}
																	echo ">$rt->TahunID - $rt->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
									<tr>
										<form role="form" id="student" action="<?php echo site_url("remedial/ptl_cari"); ?>" method="POST">
											<td colspan="4">
												<input type="text" name="cari" value="<?php echo $this->session->userdata('remedial_filter_mahasiswa'); ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
										</form>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													if($r["Foto"] == "")
													{
														$foto = "foto_umum/user.jpg";
													}
													else
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = "foto_mahasiswa/".$r["Foto"];
														}
														else
														{
															$foto = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="5"><p align="center"><img src="<?php echo base_url("ptl_storage/$foto"); ?>" width="200px"/></p></td>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td><?php echo $r["TempatLahir"].", ".tgl_indo($r["TanggalLahir"]); ?></td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td>
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td>
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td>
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
													?>
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>CODE</th>
											<th>SUBJECT</th>
											<th>ROOM</th>
											<th>DATE</th>
											<th>TIME</th>
											<th>SUPERVISOR</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												foreach($rowrecord as $row)
												{
													echo "
														<tr>
															<td title='$row->KRSRemedialID'>$row->SubjekKode</td>
															<td title='$row->SubjekID'>$row->NamaSubjek</td>
															<td>$row->UjianRuangID</td>
															<td>$row->TglMulai - $row->TglSelesai</td>
															<td>$row->JamMulai - $row->JamSelesai</td>
															<td>$row->NamaDosen</td>
														</tr>
														";
												}
											}
											else
											{
												echo "<tr>
														<td colspan='6'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
								<center>
									<?php
										if($rowrecord)
										{
											echo "<a href='".site_url("remedial/ptl_pdf_remedial_pass")."' class='btn btn-info'>Print Short Semester Pass</a>";
										}
									?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>