		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Schedule</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("today"); ?>">Schedule</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is the daily schedule. <a href="<?php echo site_url("tutorial/ptl_detail/TODY001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("today/ptl_filter_tanggal"); ?>" method="POST" id="formId">
												<input type="text" name="cektanggal" value="<?php echo $today; ?>" id="Date1" class="form-control">
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("today/ptl_filter_kelas"); ?>" method="POST">
												<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
													<option value='_'>-- CLASS --</option>
													<?php
														$cekjurusan = "REG";
														if($cekjurusan == "REG")
														{
															$init = "";
														}
														if($cekjurusan == "INT")
														{
															$init = "O";
														}
														$ni = "";
														$cekkelas = $this->session->userdata('today_filter_kelas');
														if($rowkelas)
														{
															for($i=1;$i<=3;$i++)
															{
																foreach($rowkelas as $kls)
																{
																	if($init == "")
																	{
																		$ni = $i;
																	}
																	else
																	{
																		$ni = "";
																	}
																	if($kls->ProgramID == $cekjurusan)
																	{
																		echo "<option value='".$i.$kls->Nama."'";
																		if($cekkelas == $i.$kls->Nama)
																		{
																			echo "selected";
																		}
																		echo ">$init$ni$kls->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("today/ptl_filter_dosen"); ?>" method="POST">
												<select name="cekdosen" title="Filter by Lecturer" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- TEACHERS --</option>
													<?php
														$cekdosen = $this->session->userdata('today_filter_dosen');
														if($cekjurusan != "")
														{
															if($rowdosen)
															{
																foreach($rowdosen as $dsn)
																{
																	echo "<option value='$dsn->Login'";
																	if($cekdosen == $dsn->Login)
																	{
																		echo "selected";
																	}
																	echo ">$dsn->Nama</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("today/ptl_filter_ruang"); ?>" method="POST">
												<select name="cekruang" title="Filter by Room" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ROOM --</option>
													<?php
														$cekruang = $this->session->userdata('today_filter_ruang');
														if($rowruang)
														{
															foreach($rowruang as $rg)
															{
																echo "<option value='$rg->RuangID'";
																if($rg->RuangID == $cekruang)
																{
																	echo "selected";
																}
																echo ">$rg->RuangID</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("today/ptl_filter_bentrok"); ?>" method="POST">
												<select name="cekbentrok" title="Filter by Type" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ALL SCHEDULE --</option>
													<?php
														$cekbentrok = $this->session->userdata('today_filter_bentrok');
														echo "<option value='C'"; if($cekbentrok == "C"){ echo "selected"; } echo ">Conflict Only</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<?php
									echo "<center><h2>".day_name($today).", ".tgl_singkat_eng($today)."</h2></center>";
								?>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Attendance ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Subjects</th>
											<th>Class</th>
											<th>Session</th>
											<th>Topic</th>
											<th>Lecturer</th>
											<th>Room</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											if($rowrecord)
											{
												$h = "-7";
												$hm = $h * 60;
												$ms = $hm * 60;
												$Jam = gmdate("Hi", time()-($ms));
												$TanggalSekarang = gmdate("Y-m-d", time()-($ms));
												foreach($rowrecord as $row)
												{
													$JadwalID = $row->JadwalID;
													$jdw = $this->m_jadwal->PTL_select($JadwalID);
													$sub = "";
													$kelas = "";
													if($jdw)
													{
														if($jdw["Gabungan"] == "Y")
														{
															$KelasIDGabungan = $jdw["KelasIDGabungan"];
															$wordgab = explode(".",$KelasIDGabungan);
															for($i=0;$i<30;$i++)
															{
																$wg = explode("^",@$wordgab[$i]);
																$KelasID = @$wg[1];
																$res = $this->m_kelas->PTL_select_kelas($KelasID);
																if($res)
																{
																	$kls = $res["Nama"];
																}
																else
																{
																	$kls = "";
																}
																if($i == 0)
																{
																	$kelas .= @$wg[0].$kls;
																}
																else
																{
																	if($wg[0] != "")
																	{
																		$kelas .= @$wg[0].$kls.", ";
																	}
																}
															}
														}
														else
														{
															$KelasID = $jdw["KelasID"];
															$res = $this->m_kelas->PTL_select_kelas($KelasID);
															if($res)
															{
																if($res["ProgramID"] == "INT")
																{
																	$kelas = "O".$res["Nama"];
																}
																else
																{
																	$kelas = $jdw["TahunKe"].$res["Nama"];
																}
															}
														}
														$SubjekID = $jdw["SubjekID"];
														$subjek = $this->m_subjek->PTL_select($SubjekID);
														if($subjek)
														{
															$sub = $subjek["Nama"];
														}
													}
													if(($cekkelas != "") AND ($cekkelas != "_"))
													{
														if(stristr($kelas,$cekkelas))
														{
															$MKID = $row->MKID;
															$mk = $this->m_mk->PTL_select($MKID);
															$topic = "<font color='red'><b>Not set</b></font>";
															if($mk)
															{
																$topic = $mk["Nama"];
															}
															$DosenID = $row->DosenID;
															$dosen = $this->m_dosen->PTL_select($DosenID);
															$dsn = "<font color='red'><b>Not set</b></font>";
															if($dosen)
															{
																$dsn = $dosen["Nama"];
															}
															$b1 = "";
															$b2 = "";
															if($_COOKIE["nama"] == $dsn)
															{
																$b1 = "<font color='red'><b>";
																$b2 = "</b></font>";
															}
															$PresensiID = $row->PresensiID;
															$Tanggal = $row->Tanggal;
															$JamMulai = $row->JamMulai;
															$JamSelesai = $row->JamSelesai;
															$RuangID = $row->RuangID;
															$cekkonflik1 = $this->m_presensi->PTL_all_select_conflict1($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID);
															$cekkonflik2 = $this->m_presensi->PTL_all_select_conflict2($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID);
															$Keterangan = "";
															$TotalKonflik = 0;
															if($cekkonflik1)
															{
																foreach($cekkonflik1 as $r1)
																{
																	$JadwalID = $r1->JadwalID;
																	$jdwcek = $this->m_jadwal->PTL_select($JadwalID);
																	$SubjekID = $jdwcek["SubjekID"];
																	$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																	$NamaSubjek = "";
																	if($ressubjek)
																	{
																		$NamaSubjek = $ressubjek["Nama"];
																	}
																	$Keterangan .= "$r1->PresensiID $NamaSubjek |";
																}
																$TotalKonflik++;
															}
															if($cekkonflik2)
															{
																foreach($cekkonflik2 as $r2)
																{
																	$JadwalID = $r2->JadwalID;
																	$jdwcek = $this->m_jadwal->PTL_select($JadwalID);
																	$SubjekID = $jdwcek["SubjekID"];
																	$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																	$NamaSubjek = "";
																	if($ressubjek)
																	{
																		$NamaSubjek = $ressubjek["Nama"];
																	}
																	$Keterangan .= "$r2->PresensiID $NamaSubjek |";
																}
																$TotalKonflik++;
															}
															$ck = "";
															if($TotalKonflik > 0)
															{
																$ck = "<br/><img src='".base_url("assets/dashboard/img/warning.gif")."' width='30px'><font color='red'><b>This schedule conflicts with $Keterangan</b></font>";
															}
															$JamA = str_replace(":","",substr($row->JamMulai,0,5));
															$JamB = str_replace(":","",substr($row->JamSelesai,0,5));
															if($cekbentrok != "")
															{
																if($TotalKonflik > 0)
																{
																	if(($Jam >= $JamA) AND ($Jam <= $JamB))
																	{
																		if($row->Tanggal == $TanggalSekarang)
																		{
																			echo "<tr class='warning'>";
																		}
																		else
																		{
																			echo "<tr class='info'>";
																		}
																	}
																	else
																	{
																		echo "<tr class='info'>";
																	}
																		echo "<td title='PresensiID : $row->PresensiID'>$b1$no$b2</td>
																			<td>$b1$row->PresensiID$b2</td>
																			<td>$b1$row->Tanggal$b2</td>
																			<td>$b1".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."$b2</td>
																			<td>$b1<a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$row->TahunID/$row->MKID")."' title='Go to Attendance' target='_blank'>$sub</a>$b2$ck</td>
																			<td>$b1$kelas$b2</td>
																			<td align='right'>$b1$row->Pertemuan$b2</td>
																			<td>$b1$topic$b2</td>
																			<td>$b1$dsn$b2</td>
																			<td>$b1$row->RuangID$b2</td>
																		</tr>";
																	$no++;
																}
															}
															else
															{
																if(($Jam >= $JamA) AND ($Jam <= $JamB))
																{
																	if($row->Tanggal == $TanggalSekarang)
																	{
																		echo "<tr class='warning'>";
																	}
																	else
																	{
																		echo "<tr class='info'>";
																	}
																}
																else
																{
																	echo "<tr class='info'>";
																}
																	echo "<td title='PresensiID : $row->PresensiID'>$b1$no$b2</td>
																		<td>$b1$row->PresensiID$b2</td>
																		<td>$b1$row->Tanggal$b2</td>
																		<td>$b1".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."$b2</td>
																		<td>$b1<a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$row->TahunID/$row->MKID")."' title='Go to Attendance' target='_blank'>$sub</a>$b2$ck</td>
																		<td>$b1$kelas$b2</td>
																		<td align='right'>$b1$row->Pertemuan$b2</td>
																		<td>$b1$topic$b2</td>
																		<td>$b1$dsn$b2</td>
																		<td>$b1$row->RuangID$b2</td>
																	</tr>";
																$no++;
															}
														}
													}
													else
													{
														$MKID = $row->MKID;
														$mk = $this->m_mk->PTL_select($MKID);
														$topic = "<font color='red'><b>Not set</b></font>";
														if($mk)
														{
															$topic = $mk["Nama"];
														}
														$DosenID = $row->DosenID;
														$dosen = $this->m_dosen->PTL_select($DosenID);
														$dsn = "<font color='red'><b>Not set</b></font>";
														if($dosen)
														{
															$dsn = $dosen["Nama"];
														}
														$b1 = "";
														$b2 = "";
														if($_COOKIE["nama"] == $dsn)
														{
															$b1 = "<font color='red'><b>";
															$b2 = "</b></font>";
														}
														$PresensiID = $row->PresensiID;
														$Tanggal = $row->Tanggal;
														$JamMulai = $row->JamMulai;
														$JamSelesai = $row->JamSelesai;
														$RuangID = $row->RuangID;
														$cekkonflik1 = $this->m_presensi->PTL_all_select_conflict1($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID);
														$cekkonflik2 = $this->m_presensi->PTL_all_select_conflict2($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID);
														$Keterangan = "";
														$TotalKonflik = 0;
														if($cekkonflik1)
														{
															foreach($cekkonflik1 as $r1)
															{
																$JadwalID = $r1->JadwalID;
																$jdwcek = $this->m_jadwal->PTL_select($JadwalID);
																$SubjekID = $jdwcek["SubjekID"];
																$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																$NamaSubjek = "";
																if($ressubjek)
																{
																	$NamaSubjek = $ressubjek["Nama"];
																}
																$Keterangan .= "$r1->PresensiID $NamaSubjek |";
															}
															$TotalKonflik++;
														}
														if($cekkonflik2)
														{
															foreach($cekkonflik2 as $r2)
															{
																$JadwalID = $r2->JadwalID;
																$jdwcek = $this->m_jadwal->PTL_select($JadwalID);
																$SubjekID = $jdwcek["SubjekID"];
																$ressubjek = $this->m_subjek->PTL_select($SubjekID);
																$NamaSubjek = "";
																if($ressubjek)
																{
																	$NamaSubjek = $ressubjek["Nama"];
																}
																$Keterangan .= "$r2->PresensiID $NamaSubjek |";
															}
															$TotalKonflik++;
														}
														$ck = "";
														if($TotalKonflik > 0)
														{
															$ck = "<br/><img src='".base_url("assets/dashboard/img/warning.gif")."' title='Schedule conflict detected' width='30px'><font color='red'><b>This schedule conflicts with $Keterangan</b></font>";
														}
														$JamA = str_replace(":","",substr($row->JamMulai,0,5));
														$JamB = str_replace(":","",substr($row->JamSelesai,0,5));
														if($cekbentrok != "")
														{
															if($TotalKonflik > 0)
															{
																if(($Jam >= $JamA) AND ($Jam <= $JamB))
																{
																	if($row->Tanggal == $TanggalSekarang)
																	{
																		echo "<tr class='warning'>";
																	}
																	else
																	{
																		echo "<tr class='info'>";
																	}
																}
																else
																{
																	echo "<tr class='info'>";
																}
																	echo "<td title='PresensiID : $row->PresensiID'>$b1$no$b2</td>
																		<td>$b1$row->PresensiID$b2</td>
																		<td>$b1$row->Tanggal$b2</td>
																		<td>$b1".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."$b2</td>
																		<td>$b1<a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$row->TahunID/$row->MKID")."' title='Go to Attendance' target='_blank'>$sub</a>$b2$ck</td>
																		<td>$b1$kelas$b2</td>
																		<td align='right'>$b1$row->Pertemuan$b2</td>
																		<td>$b1$topic$b2</td>
																		<td>$b1$dsn$b2</td>
																		<td>$b1$row->RuangID$b2</td>
																	</tr>";
																$no++;
															}
														}
														else
														{
															if(($Jam >= $JamA) AND ($Jam <= $JamB))
															{
																if($row->Tanggal == $TanggalSekarang)
																{
																	echo "<tr class='warning'>";
																}
																else
																{
																	echo "<tr class='info'>";
																}
															}
															else
															{
																echo "<tr class='info'>";
															}
																echo "<td title='PresensiID : $row->PresensiID'>$b1$no$b2</td>
																	<td>$b1$row->PresensiID$b2</td>
																	<td>$b1$row->Tanggal$b2</td>
																	<td>$b1".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."$b2</td>
																	<td>$b1<a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$row->TahunID/$row->MKID")."' title='Go to Attendance' target='_blank'>$sub</a>$b2$ck</td>
																	<td>$b1$kelas$b2</td>
																	<td align='right'>$b1$row->Pertemuan$b2</td>
																	<td>$b1$topic$b2</td>
																	<td>$b1$dsn$b2</td>
																	<td>$b1$row->RuangID$b2</td>
																</tr>";
															$no++;
														}
													}
												}
											}
											if($no == 1)
											{
												echo "<tr class='info'>
														<td colspan='10' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>