		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lecturer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("lecturer"); ?>">Lecturer</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>List of Lecturer.
								</div>
								<?php
									if(!stristr($_COOKIE["akses"],"LECTURER"))
									{
								?>
										<center><a href="<?php echo site_url("lecturer/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<?php
									}
								?>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Photo</th>
											<th>Lecturer ID & Name </th>
											<th>Phone<br/>Mobile</th>
											<th>Email 1 (Work)</th>
											<th>Email 2 (Private)</th>
											<?php
												if(!stristr($_COOKIE["akses"],"LECTURER"))
												{
											?>
													<th>Edit</th>
											<?php
												}
											?>
											<th>Att</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($_COOKIE["nama"] == $row->Nama)
													{
														if($row->Foto == "")
														{
															$foto = "foto_umum/user.jpg";
														}
														else
														{
															$word = explode("_-_",$row->Foto);
															$storage = $word[0];
															$foto = "foto_dosen/$storage/".@$word[1];
															$exist = file_exists_remote(base_url("ptl_storage/$foto"));
															if($exist)
															{
																$foto = $foto;
															}
															else
															{
																$foto = "foto_umum/user.jpg";
															}
														}
														if($row->NA == "Y")
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr>";
														}
														echo "<td>$no</td>
																<td><img src='".base_url("ptl_storage/$foto")."' width='50px'/></td>
																<td>$row->Login<br/>$row->Nama</td>
																<td>$row->Telephone<br/>$row->Handphone</td>
																<td>$row->Email</td>
																<td>$row->Email2</td>";
																if(!stristr($_COOKIE["akses"],"LECTURER"))
																{
																	echo "<td class='center'>
																			<a class='btn btn-info' href='".site_url("lecturer/ptl_edit/$row->Login")."'>
																				<i class='fa fa-list'></i>
																			</a>
																		</td>";
																}
															echo "<td class='center'>
																	<a class='btn btn-success' href='".site_url("lecturer/ptl_attendance/$row->Login")."'>
																		<i class='fa fa-list-alt'></i>
																	</a>
																</td>
															</tr>";
														$no++;
													}
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>