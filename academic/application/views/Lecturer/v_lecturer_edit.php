		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Lecturer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("lecturer"); ?>">Lecturer</a>
							>>
                            <a href="<?php echo site_url("lecturer/ptl_edit/$Login"); ?>">Edit Lecturer</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php echo form_open_multipart('lecturer/ptl_update',array('name' => 'contoh')); ?>
									<div class="col-lg-6">
										<center>
											<?php
												if($Foto == "")
												{
													$foto = "ptl_storage/foto_umum/user.jpg";
												}
												else
												{
													$foto = "../hris/system_storage/foto_karyawan/$Foto";
													$exist = file_exists_remote(base_url("$foto"));
													if($exist)
													{
														$foto = $foto;
													}
													else
													{
														$foto = "ptl_storage/foto_umum/user.jpg";
													}
												}
											?>
											<div class="form-group">
												<img src="<?php echo base_url("$foto"); ?>" alt="" width="200px">
												<br/>
												<br/>
												<!--<input type="file" name="userfile" accept="image/*" class="btn btn-primary">-->
												<font color="blue"><b>If you want to update your photo, please contact HR Admin.</b></font>
												<p class="help-block"></p>
											</div>
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover" id="dataTables-example">
													<thead>
														<tr>
															<th>#</th>
															<th>Year</th>
															<th>Class</th>
															<th>Total Homeroom Students</th>
															<th>Total Vise Homeroom Students</th>
														</tr>
													</thead>
													<tbody>
														<?php
															$WaliKelasID = $Login;
															if($rowrecord)
															{
																$no = 1;
																foreach($rowrecord as $row)
																{
																	$TahunID = $row->TahunID;
																	$rowkelas = $this->m_kelas->PTL_all_active();
																	if($rowkelas)
																	{
																		foreach($rowkelas as $rk)
																		{
																			$KelasID = $rk->KelasID;
																			$res = $this->m_kelas->PTL_select_kelas($KelasID);
																			$kelas = "";
																			if($res)
																			{
																				$kelas = $res['Nama'];
																			}
																			$rowhmr1 = $this->m_khs->PTL_all_homeroom1($TahunID,$KelasID,$WaliKelasID);
																			$rowhmr2 = $this->m_khs->PTL_all_homeroom2($TahunID,$KelasID,$WaliKelasID);
																			$jml1 = count($rowhmr1);
																			$jml2 = count($rowhmr2);
																			if(($jml1 > 0) OR ($jml2 > 0))
																			{
																				echo "<tr class='danger'>
																						<td>$no</td>
																						<td>$row->TahunID $row->Nama</td>
																						<td align='center'>$kelas</td>
																						<td align='right'>$jml1</td>
																						<td align='right'>$jml2</td>
																					</tr>";
																				$no++;
																			}
																		}
																	}
																}
															}
															if($no == 1)
															{
																echo "<tr>
																		<td colspan='5'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																	</tr>";
															}
														?>
													</tbody>
												</table>
											</div>
										</center>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="red">Lecturer ID</font></label>
											<input readonly type="text" name="Login" value="<?php echo $Login; ?>" class="form-control" required>
											<input type="hidden" name="foto_lama" value="<?php echo $foto_lama; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Place of Birth</font></label>
											<input type="text" name="TempatLahir" value="<?php echo $TempatLahir; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Date of Birth</font></label>
											<input type="text" name="TanggalLahir" id="datepicker1" value="<?php echo $TanggalLahir; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Gender</label>
											<br/>
											<input type="radio" name="Kelamin" value="P" <?php if($Kelamin == "P"){ echo "checked"; }; ?>>&nbsp;Man&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="Kelamin" value="W" <?php if($Kelamin == "W"){ echo "checked"; }; ?>>&nbsp;Woman
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Nationality</font></label>
											<input type="text" name="Kebangsaan" value="<?php echo $Kebangsaan; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Civil Status</label>
											<br/>
											<input type="radio" name="StatusSipil" value="B" <?php if($StatusSipil == "B"){ echo "checked"; }; ?>>&nbsp;Single&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="StatusSipil" value="K" <?php if($StatusSipil == "K"){ echo "checked"; }; ?>>&nbsp;Married&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="StatusSipil" value="D" <?php if($StatusSipil == "D"){ echo "checked"; }; ?>>&nbsp;Divorced
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Number of Children</label>
											<input type="number" name="JumlahAnak" value="<?php echo $JumlahAnak; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Religion</font></label>
											<select name="Agama" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowagama)
													{
														foreach($rowagama as $ra)
														{
															echo "<option value='$ra->Agama'";
															if($Agama == $ra->Agama)
															{
																echo "selected";
															}
															echo ">$ra->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="AlamatJakarta" class="form-control"><?php echo $AlamatJakarta; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="Telephone" value="<?php echo $Telephone; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Mobile Phone</font></label>
											<input type="text" name="Handphone" value="<?php echo $Handphone; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Work E-Mail</font></label>
											<input type="email" name="Email" value="<?php echo $Email; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Private Mail</font></label>
											<input type="email" name="Email2" value="<?php echo $Email2; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject</label>
											<div class="col-lg-12">
												<?php
													if($rowsubjek)
													{
														$no = 0;
														foreach($rowsubjek as $rs)
														{
															$no++;
															$ck = "";
															if(strpos($SubjekID,$rs->SubjekKode) !== false)
															{
																$ck = "checked";
															}
															echo "<input type='checkbox' name='sub$no' value='$rs->SubjekKode' $ck>&nbsp;$rs->Nama<br/>";
														}
													}
												?>
											</div>
											<input type="hidden" name="totA" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization</label>
											<div class="col-lg-12">
												<?php
													if($rowspc)
													{
														$n = 0;
														foreach($rowspc as $rs)
														{
															$n++;
															$ck = "";
															if(strpos($SpesialisasiID,$rs->SpesialisasiID) !== false)
															{
																$ck = "checked";
															}
															echo "<input type='checkbox' name='spc$n' value='$rs->SpesialisasiID' $ck>&nbsp;$rs->Nama<br/>";
														}
													}
												?>
											</div>
											<input type="hidden" name="totB" value="<?php echo $n; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<br/>
											<input type="radio" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; }; ?>>&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="NA" value="N" <?php if($NA == "N"){ echo "checked"; }; ?>>&nbsp;No
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created by</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date Created</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited by</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Date Edited</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("lecturer"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>