		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lecturer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("lecturer"); ?>">Lecturer</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>List of Lecturer.
								</div>
								<?php
									if(($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT"))
									{
								?>
										<center><a href="<?php echo site_url("lecturer/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<?php
									}
								?>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Photo</th>
											<th>Lecturer ID & Name </th>
											<th>Phone<br/>Mobile</th>
											<th>Email 1 (Work)</th>
											<th>Email 2 (Private)</th>
											<?php
												if(($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT"))
												{
											?>
													<th>Edit</th>
													<th>Att</th>
											<?php
												}
											?>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													$foto = "ptl_storage/foto_umum/user.jpg";
													$foto_preview = "ptl_storage/foto_umum/user.jpg";
													if($row->Foto != "")
													{
														$foto = "../hris/system_storage/foto_karyawan/$row->Foto";
														$exist = file_exists_remote(base_url("$foto"));
														if($exist)
														{
															$foto = $foto;
															$source_photo = base_url("$foto");
															$info = pathinfo($source_photo);
															$foto_preview = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
															$exist1 = file_exists_remote(base_url("$foto_preview"));
															if($exist1)
															{
																$foto_preview = $foto_preview;
															}
															else
															{
																$foto_preview = $foto;
															}
														}
														else
														{
															$foto = "ptl_storage/foto_umum/user.jpg";
															$foto_preview = "ptl_storage/foto_umum/user.jpg";
														}
													}
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td>$no</td>
															<td>
																<a class='fancybox' title='$row->Login - $row->Nama' href='".base_url("$foto")."' data-fancybox-group='gallery' >
																	<img class='img-polaroid' src='".base_url("$foto_preview")."' width='50px' alt='' />
																</a>
															</td>
															<td>$row->Login<br/>$row->Nama</td>
															<td>$row->Telephone<br/>$row->Handphone</td>
															<td>$row->Email</td>
															<td>$row->Email2</td>";
															if(($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT"))
															{
																echo "<td class='center'>
																	<a class='btn btn-info' href='".site_url("lecturer/ptl_edit/$row->Login")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
																<td class='center'>
																	<a class='btn btn-success' href='".site_url("lecturer/ptl_attendance/$row->Login")."'>
																		<i class='fa fa-list-alt'></i>
																	</a>
																</td>";
															}
													echo "</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>