		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Lecturer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("lecturer"); ?>">Lecturer</a>
							>>
                            <a href="<?php echo site_url("lecturer/ptl_form"); ?>">Add New Lecturer</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php echo form_open_multipart('lecturer/ptl_insert',array('name' => 'contoh')); ?>
									<div class="col-lg-6">
										<center>
											<?php
												$foto = "foto_umum/user.jpg";
											?>
											<div class="form-group">
												<img src="<?php echo base_url("ptl_storage/$foto"); ?>" alt="" width="200px">
												<br/>
												<br/>
												<input type="file" name="userfile" accept="image/*" class="btn btn-primary">
												<p class="help-block"></p>
											</div>
										</center>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label><font color="red">Lecturer ID</font></label>
											<input type="text" name="Login" value="" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Name</font></label>
											<input type="text" name="Nama" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Place of Birth</font></label>
											<input type="text" name="TempatLahir" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Date of Birth</font></label>
											<input type="text" name="TanggalLahir" id="datepicker1" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Gender</label>
											<br/>
											<input type="radio" name="Kelamin" value="P">&nbsp;Man&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="Kelamin" value="W">&nbsp;Woman
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Nationality</font></label>
											<input type="text" name="Kebangsaan" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Civil Status</label>
											<br/>
											<input type="radio" name="StatusSipil" value="B">&nbsp;Single&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="StatusSipil" value="K">&nbsp;Married&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="StatusSipil" value="D">&nbsp;Divorced
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Number of Children</label>
											<input type="number" name="JumlahAnak" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Religion</font></label>
											<select name="Agama" class="form-control" required>
												<option value="">-- CHOOSE --</option>
												<?php
													if($rowagama)
													{
														foreach($rowagama as $ra)
														{
															echo "<option value='$ra->Agama'>$ra->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="AlamatJakarta" class="form-control"></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="Telephone" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Mobile Phone</font></label>
											<input type="text" name="Handphone" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Work E-Mail</font></label>
											<input type="email" name="Email" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label><font color="red">Private Mail</font></label>
											<input type="email" name="Email2" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject</label>
											<div class="col-lg-12">
												<?php
													if($rowsubjek)
													{
														$no = 0;
														foreach($rowsubjek as $rs)
														{
															$no++;
															echo "<input type='checkbox' name='sub$no' value='$rs->SubjekKode'>&nbsp;$rs->Nama<br/>";
														}
													}
												?>
											</div>
											<input type="hidden" name="totA" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Specialization</label>
											<div class="col-lg-12">
												<?php
													if($rowspc)
													{
														$n = 0;
														foreach($rowspc as $rs)
														{
															$n++;
															echo "<input type='checkbox' name='spc$n' value='$rs->SpesialisasiID'>&nbsp;$rs->Nama<br/>";
														}
													}
												?>
											<input type="hidden" name="totB" value="<?php echo $n; ?>">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("lecturer"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>