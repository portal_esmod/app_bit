		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lecturer Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("lecturer"); ?>">Lecturer</a>
							>>
							<a href="<?php echo site_url("lecturer/ptl_attendance/$DosenID"); ?>">Lecturer Attendance</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>List of Lecturer Attendance.
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("lecturer/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value='<?php echo "_".$DosenID; ?>'>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('lecturer_filter_jur');
														echo "<option value='REG_$DosenID'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT_$DosenID'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC_$DosenID'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("lecturer/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('lecturer_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value='<?php echo "_".$DosenID; ?>'>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	// if($rt->NA == "N")
																	// {
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID"."_".$DosenID."' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	// }
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Year ID</th>
											<th>Subjects</th>
											<th>Program</th>
											<th>Prodi</th>
											<th>Class</th>
											<th>Session</th>
											<th>Start Date</th>
											<th>End Date</th>
											<th>Total Attendance</th>
											<th>Max Absent</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if(($cekjurusan != "") AND ($cektahun != ""))
											{
												if($rowrecord)
												{
													$no = 1;
													foreach($rowrecord as $row)
													{
														$SubjekID = $row->SubjekID;
														$ressub = $this->m_subjek->PTL_select($SubjekID);
														$subjek = "";
														if($ressub)
														{
															$subjek = $ressub["Nama"];
														}
														if($row->Gabungan == "Y")
														{
															$KelasIDGabungan = $row->KelasIDGabungan;
															$kelas = "";
															$wordgab = explode(".",$KelasIDGabungan);
															for($i=0;$i<30;$i++)
															{
																$wg = explode("^",@$wordgab[$i]);
																$KelasID = @$wg[1];
																$res = $this->m_kelas->PTL_select_kelas($KelasID);
																if($res)
																{
																	$kls = $res["Nama"];
																}
																else
																{
																	$kls = "";
																}
																if($wg[0] != "")
																{
																	$kelas .= @$wg[0].$kls.", ";
																}
															}
														}
														else
														{
															$KelasID = $row->KelasID;
															$res = $this->m_kelas->PTL_select_kelas($KelasID);
															if($res)
															{
																$kls = $res["Nama"];
															}
															else
															{
																$kls = "";
															}
															$kelas = $row->TahunKe.$kls;
														}
														if($row->NA == "Y")
														{
															echo "<tr class='danger'>";
														}
														else
														{
															echo "<tr class='info'>";
														}
														echo "<th title='JadwalID : $row->JadwalID'>$no</th>
																<th>$row->TahunID</th>
																<th>$subjek</th>
																<th>$row->ProgramID</th>
																<th>$row->ProdiID</th>
																<th>$kelas</th>
																<th>SEMESTER $row->Sesi</th>
																<th>$row->TglMulai</th>
																<th>$row->TglSelesai</th>
																<th align='right'>$row->RencanaKehadiran</th>
																<th align='right'>$row->MaxAbsen</th>
															</tr>";
														$no++;
														$JadwalID = $row->JadwalID;
														$rowpresensi = $this->m_presensi->PTL_all_select_dosen($JadwalID,$DosenID);
														if($rowpresensi)
														{
															echo "<tr>
																	<th></th>
																	<th>Ses</th>
																	<th>Date</th>
																	<th>Time</th>
																	<th>Room</th>
																	<th colspan='2'>Lecturer</th>
																	<th>Topic</th>
																	<th colspan='2'>Notes</th>
																	<th>Posting Date</th>
																</tr>";
															foreach($rowpresensi as $rp)
															{
																$DosenID = $row->DosenID;
																$dsn = $this->m_dosen->PTL_select($DosenID);
																$nmdsn = "";
																if($dsn)
																{
																	$nmdsn = $dsn['Nama'];
																}
																$MKID = $rp->MKID;
																$mkid = $this->m_mk->PTL_select($MKID);
																$nmmk = "";
																if($mkid)
																{
																	$nmmk = $mkid["Nama"];
																}
																$tglA = str_replace("-","",$rp->Tanggal);
																$tglB = str_replace("-","",substr($rp->dosen_tanggal_set,0,10));
																$tglC = $tglB - $tglA;
																$Mpl = "";
																if($tglC < 0)
																{
																	$Mpl = "-";
																}
																$selisih = $Mpl.((abs(strtotime($rp->Tanggal) - strtotime($tglB)))/(60*60*24));
																$pesan = "";
																if($selisih < '0')
																{
																	$pesan = "<a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' target='_blank' class='btn btn-info'>NOT YET TIME</a>";
																}
																else
																{
																	if(($selisih >= '0') AND ($selisih <= '1'))
																	{
																		$pesan = "<a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' target='_blank' class='btn btn-success'>ON TIME</a>";
																	}
																	else
																	{
																		if(($selisih >= '2') AND ($selisih <= '3'))
																		{
																			$pesan = "<a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' target='_blank' class='btn btn-warning'>ALMOST</a>";
																		}
																		else
																		{
																			if($selisih > '3')
																			{
																				$pesan = "<a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' target='_blank' class='btn btn-danger'>LATE</a>";
																			}
																			else
																			{
																				$pesan = "<a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' target='_blank' class='btn btn-danger'>OUT OF RANGE</a>";
																			}
																		}
																	}
																}
																echo "<tr>
																		<td></td>
																		<td title='PresensiID : $rp->PresensiID'>$rp->Pertemuan</td>
																		<td><a href='".site_url("attendance/ptl_list_attendance/$rp->PresensiID/$rp->TahunID/$rp->MKID")."' title='Go to Attendance' target='_blank'>".tgl_singkat_eng($rp->Tanggal)."</a></td>
																		<td>".substr($rp->JamMulai,0,5)." ".substr($rp->JamSelesai,0,5)."</td>
																		<td>$rp->RuangID</td>
																		<td colspan='2'>$nmdsn</td>
																		<td>$nmmk</td>
																		<td colspan='2'>$rp->Catatan</td>
																		<td title='Posted By : $rp->dosen_login_set ~ Posted Date : $rp->dosen_tanggal_set'>$pesan</td>
																	</tr>";
															}
														}
													}
												}
												else
												{
													echo "
														<tr>
															<td colspan='11' align='center'><font color='red'><b>No data available</b></font></td>
														</tr>
														";
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>