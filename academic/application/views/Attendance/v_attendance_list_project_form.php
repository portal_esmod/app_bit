		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Project Sheet</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_project_sheet_form/$JadwalID"); ?>">Add Project Sheet (ATTEN32)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<?php echo form_open_multipart('attendance/ptl_list_project_sheet_update'); ?>
										<div class="col-lg-12">
											<div class="form-group">
												<label>Project Sheet</label>
												<input type="file" name="gambar1" class="form-control">
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<input type="hidden" name="file_project" value="<?php echo $file_project; ?>">
												<input type="hidden" name="file_evaluation" value="<?php echo $file_evaluation; ?>">
												<font color="blue">If you want to upload some files, please make .rar / .zip file.</font>
											</div>
											<div class="form-group">
												<label>Evaluation Sheet</label>
												<input type="file" name="gambar2" class="form-control">
												<font color="blue">If you want to upload some files, please make .rar / .zip file.</font>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>