		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance (ATTEN02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the sixth step to set topic after you create schedule and set attendance.
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("attendance/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('att_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("attendance/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('att_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if($cektahun != "")
											{
										?>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<?php
														if(!stristr($_COOKIE["akses"],"LECTURER"))
														{
													?>
															<form action="<?php echo site_url("attendance/ptl_filter_subjek"); ?>" method="POST">
																<select name="ceksubjek" title="Filter by Subject" class="form-control round-form" onchange="this.form.submit()">
																	<option value=''>-- SUBJECT --</option>
																	<?php
																		$ceksubjek = $this->session->userdata('att_filter_subjek');
																		if($cekjurusan != "")
																		{
																			if($rowsubjek)
																			{
																				foreach($rowsubjek as $sub)
																				{
																					echo "<option value='$sub->subjek'";
																					if($ceksubjek == $sub->subjek)
																					{
																						echo "selected";
																					}
																					echo ">$sub->nama</option>";
																				}
																			}
																		}
																	?>
																</select>
																<noscript><input type="submit" value="Submit"></noscript>
															</form>
													<?php
														}
													?>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<?php
														if(!stristr($_COOKIE["akses"],"LECTURER"))
														{
													?>
															<form action="<?php echo site_url("attendance/ptl_filter_kelas"); ?>" method="POST">
																<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
																	<option value='_'>-- CLASS --</option>
																	<?php
																		if($cekjurusan == "REG")
																		{
																			$init = "";
																		}
																		if($cekjurusan == "INT")
																		{
																			$init = "O";
																		}
																		$ni = "";
																		$cekkelas = $this->session->userdata('att_filter_kelas');
																		if($cekjurusan != "")
																		{
																			if($rowkelas)
																			{
																				for($i=1;$i<=3;$i++)
																				{
																					foreach($rowkelas as $kls)
																					{
																						if($init == "")
																						{
																							$ni = $i;
																						}
																						else
																						{
																							$ni = "";
																						}
																						if($kls->ProgramID == $cekjurusan)
																						{
																							echo "<option value='".$i."_".$kls->KelasID."'";
																							if($cekkelas == $i."_".$kls->KelasID)
																							{
																								echo "selected";
																							}
																							echo ">$init$ni$kls->Nama</option>";
																						}
																					}
																				}
																			}
																		}
																	?>
																</select>
																<noscript><input type="submit" value="Submit"></noscript>
															</form>
													<?php
														}
													?>
												</td>
										<?php
											}
										?>
									</tr>
								</table>
								<?php
									if(($cekjurusan != "") AND ($cektahun != ""))
									{
								?>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>ID</th>
													<th>Subject</th>
													<th>Class</th>
													<th>Teacher</th>
													<th>Attendance</th>
													<th>Recap</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														$sebelumnya = "";
														foreach($rowrecord as $row)
														{
															$SubjekID = $row->SubjekID;
															$s = $this->m_subjek->PTL_select($SubjekID);
															$fs1 = "";
															$fs2 = "";
															if($s["NA"] == "Y")
															{
																$fs1 = "<font color='red'><b title='This subject unavailable'>";
																$fs2 = "</b></font>";
															}
															$KelasID = $row->KelasID;
															if($KelasID == 0)
															{
																if($row->KelasIDGabungan != "")
																{
																	$grup1 = explode(".",$row->KelasIDGabungan);
																	$kelas1 = explode("^",$grup1[0]);
																	$kelas2 = explode("^",$grup1[1]);
																	$KelasID = @$kelas1[1];
																	$k1 = $this->m_kelas->PTL_select_kelas($KelasID);
																	$KelasID = @$kelas2[1];
																	$k2 = $this->m_kelas->PTL_select_kelas($KelasID);
																	$kelas = $kelas1[0].@$k1["Nama"]."/".$kelas2[0].@$k2["Nama"];
																	$link_kelas = $kelas1[0].@$k1["Nama"]."/".$kelas2[0].@$k2["Nama"];
																	if($kelas == "/")
																	{
																		$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																		$link_kelas = "";
																	}
																}
																else
																{
																	$k = $this->m_kelas->PTL_select_kelas($KelasID);
																	if($k)
																	{
																		if($cekjurusan == "INT")
																		{
																			$kelas = "O".$k["Nama"];
																		}
																		else
																		{
																			$kelas = $row->TahunKe.$k["Nama"];
																		}
																		$link_kelas = $row->TahunKe.$k["Nama"];
																	}
																	else
																	{
																		$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																		$link_kelas = "";
																	}
																}
															}
															else
															{
																$k = $this->m_kelas->PTL_select_kelas($KelasID);
																if($cekjurusan == "INT")
																{
																	$kelas = "O".$k["Nama"];
																}
																else
																{
																	$kelas = $row->TahunKe.$k["Nama"];
																}
																$link_kelas = $row->TahunKe.$k["Nama"];
															}
															$DosenID = $row->DosenID;
															$d = $this->m_dosen->PTL_select($DosenID);
															$f1 = "";
															$f2 = "";
															$dosen = "";
															if($d)
															{
																if($d["NA"] == "Y")
																{
																	$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																	$f2 = "</b></font>";
																}
																$dosen = $d["Nama"];
															}
															$sebelumnya = $sebelumnya;
															$subjek = "<td title='SubjekID : $row->SubjekID'><h4>$fs1".$s["Nama"]."$fs2</h4></td>";
															$rowspan = "";
															$DosenID = $row->DosenID2;
															$d2 = $this->m_dosen->PTL_select($DosenID);
															$dosen2 = "";
															if($d2)
															{
																$dosen2 = $d2["Nama"];
															}
															$DosenID = $row->DosenID3;
															$d3 = $this->m_dosen->PTL_select($DosenID);
															$dosen3 = "";
															if($d3)
															{
																$dosen3 = $d3["Nama"];
															}
															$DosenID = $row->DosenID4;
															$d4 = $this->m_dosen->PTL_select($DosenID);
															$dosen4 = "";
															if($d4)
															{
																$dosen4 = $d4["Nama"];
															}
															$DosenID = $row->DosenID5;
															$d5 = $this->m_dosen->PTL_select($DosenID);
															$dosen5 = "";
															if($d5)
															{
																$dosen5 = $d5["Nama"];
															}
															$DosenID = $row->DosenID6;
															$d6 = $this->m_dosen->PTL_select($DosenID);
															$dosen6 = "";
															if($d6)
															{
																$dosen6 = $d6["Nama"];
															}
															if(($_COOKIE["nama"] == $dosen) OR ($_COOKIE["nama"] == $dosen2) OR ($_COOKIE["nama"] == $dosen3) OR ($_COOKIE["nama"] == $dosen4) OR ($_COOKIE["nama"] == $dosen5) OR ($_COOKIE["nama"] == $dosen6))
															{
																	if($dosen2 != "")
																	{
																		$f1 = "<font color='blue'><b title='This lecturer has resigned'>";
																		$f2 = "</b></font>";
																		$dosen = "Multiple Lecturer";
																	}
																	echo "<tr class='info'>
																			<td>$no $rowspan</td>
																			<td>$row->JadwalID</td>
																			$subjek
																			<td>$kelas</td>
																			<td>$f1$dosen$f2</td>
																			<td>
																				<a href='".site_url("attendance/ptl_list/$row->JadwalID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>";
																				if(!stristr($_COOKIE["akses"],"LECTURER"))
																				{
												?>
																					<a href="<?php echo site_url("attendance/ptl_delete/$row->JadwalID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																				}
																		echo "</td>
																			<td>
																				<a href='".site_url("attendance/ptl_pdf_recap/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-success'><i class='fa fa-print'></i></a>
																				<a href='".site_url("attendance/ptl_pdf_recap_dpm/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-info'><i class='fa fa-print'></i></a>
																			</td>
																		</tr>";
																	$no++;
															}
														}
													}
													else
													{
														echo "<tr>
																<td colspan='7'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>