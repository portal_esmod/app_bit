		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Notes</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_notes_form/$JadwalID"); ?>">Add Notes (ATTEN25)</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Students
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<form action="<?php echo site_url("attendance/ptl_notes_insert"); ?>" method="POST">
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>Photo</th>
												<th>Name</th>
												<th>Notes</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$no = 0;
												if($rowrecord)
												{
													$sebelumnya = "";
													foreach($rowrecord as $row)
													{
														$no++;
														$MhswID = $row->MhswID;
														$m = $this->m_mahasiswa->PTL_select($MhswID);
														$foto = "foto_umum/user.jpg";
														$foto_preview = "foto_umum/user.jpg";
														$nama = "";
														if($m)
														{
															if($m["Foto"] != "")
															{
																$foto = "foto_mahasiswa/".$m["Foto"];
																$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																if($exist)
																{
																	$foto = $foto;
																	$source_photo = base_url("ptl_storage/$foto");
																	$info = pathinfo($source_photo);
																	$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																	$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																	if($exist1)
																	{
																		$foto_preview = $foto_preview;
																	}
																	else
																	{
																		$foto_preview = $foto;
																	}
																}
																else
																{
																	$foto = "foto_umum/user.jpg";
																	$foto_preview = "foto_umum/user.jpg";
																}
															}
															$nama = $m["Nama"];
														}
														$TahunID = $row->TahunID;
														$k = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
														$StatusMhswID = "";
														if($k)
														{
															$StatusMhswID = $k["StatusMhswID"];
														}
														$st = $this->m_status->PTL_select($StatusMhswID);
														if($StatusMhswID == "A")
														{
															$status = "<font color='green'><b>$st[Nama]</b></font>";
														}
														else
														{
															$status = "<font color='red'><b>$st[Nama]</b></font>";
														}
														echo "<tr>
																<td>$no</td>
																<td>
																	<a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																		<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																	</a>
																</td>
																<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)</td>
																<td>
																	<input type='text' name='title$no' class='form-control' placeholder='Title'>
																	<input type='hidden' name='MhswID$no' value='$row->MhswID'>
																	<textarea name='comment$no' class='form-control' placeholder='Notes'></textarea>
																</td>
															</tr>";
													}
												}
												else
												{
													echo "<tr>
															<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											?>
										</tbody>
									</table>
									<?php
										if($rowrecord)
										{
									?>
											<center>
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<input type="hidden" name="total" value="<?php echo $no; ?>">
												<input type="submit" value="Save" id="my_button" class="btn btn-primary" onclick="return confirm('Are you sure want to input notes?\n\nTHIS ACTION CAN NOT BE RESTORED.')">
											</center>
									<?php
										}
									?>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>