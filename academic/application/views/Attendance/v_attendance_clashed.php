		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance (ATTEN27)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the sixth step to set topic after you create schedule and set attendance. <a href="<?php echo site_url("tutorial/ptl_detail/ATTD001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<center><a href="<?php echo site_url("attendance"); ?>" class="btn btn-warning">Back</a></center>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Subject</th>
											<th>Date</th>
											<th>Time</th>
											<th>Room</th>
											<th>Lecturer</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $r)
												{
													foreach($rowrecord as $row)
													{
														if($r->PresensiID != $row->PresensiID)
														{
															if(($r->Tanggal == $row->Tanggal) AND ($r->RuangID == $row->RuangID))
															{
																$JamMulaiA = str_replace(":","",$r->JamMulai);
																$JamMulaiB = str_replace(":","",$r->JamSelesai);
																$JamMulaiC = str_replace(":","",$row->JamMulai);
																$JamMulaiD = str_replace(":","",$row->JamSelesai);
																if(($JamMulaiC >= $JamMulaiA) AND ($JamMulaiC <= $JamMulaiB))
																{
																	if(($JamMulaiD >= $JamMulaiA) AND ($JamMulaiD <= $JamMulaiB))
																	{
																		$JadwalID = $r->JadwalID;
																		$resA = $this->m_jadwal->PTL_select($JadwalID);
																		$SubjekID = "";
																		$NamaSub = "";
																		if($resA)
																		{
																			$SubjekID = $resA['SubjekID'];
																			$ressubA = $this->m_subjek->PTL_select($SubjekID);
																			if($ressubA)
																			{
																				$NamaSub = $ressubA['Nama'];
																			}
																		}
																		$DosenID = $r->DosenID;
																		$resDosA = $this->m_dosen->PTL_select($DosenID);
																		$NamaDosen = "";
																		if($resDosA)
																		{
																			$NamaDosen = $resDosA['Nama'];
																		}
																		echo "<tr class='info'>
																				<td>$no</td>
																				<td>$r->PresensiID</td>
																				<td>$NamaSub</td>
																				<td>".nama_hari_eng($r->Tanggal).", ".tgl_singkat_eng($r->Tanggal)."</td>
																				<td>".substr($r->JamMulai,0,5)." - ".substr($r->JamSelesai,0,5)."</td>
																				<td>$r->RuangID</td>
																				<td>$NamaDosen</td>
																				<td>
																					<a href='".site_url("attendance/ptl_list_edit/$r->PresensiID")."' class='btn btn-info' target='_blank'><i class='fa fa-list'></i></a>
																				</td>
																			</tr>";
																		$JadwalID = $row->JadwalID;
																		$resB = $this->m_jadwal->PTL_select($JadwalID);
																		$SubjekID = "";
																		$NamaSub = "";
																		if($resB)
																		{
																			$SubjekID = $resB['SubjekID'];
																			$ressubB = $this->m_subjek->PTL_select($SubjekID);
																			if($ressubB)
																			{
																				$NamaSub = $ressubB['Nama'];
																			}
																		}
																		$DosenID = $row->DosenID;
																		$resDosB = $this->m_dosen->PTL_select($DosenID);
																		$NamaDosen = "";
																		if($resDosB)
																		{
																			$NamaDosen = $resDosB['Nama'];
																		}
																		echo "<tr class='danger'>
																				<td>&nbsp;</td>
																				<td>$row->PresensiID</td>
																				<td>$NamaSub</td>
																				<td>".nama_hari_eng($row->Tanggal).", ".tgl_singkat_eng($row->Tanggal)."</td>
																				<td>".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."</td>
																				<td>$row->RuangID</td>
																				<td>$NamaDosen</td>
																				<td>
																					<a href='".site_url("attendance/ptl_list_edit/$row->PresensiID")."' class='btn btn-info' target='_blank'><i class='fa fa-list'></i></a>
																				</td>
																			</tr>";
																			$no++;
																	}
																}
															}
														}
													}
												}
											}
											else
											{
												echo "<tr>
														<td colspan='8'><h4><b><p align='center'><font color='red'>No schedule clashed</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>