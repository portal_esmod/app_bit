		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_edit/$JadwalID"); ?>">Edit Attendance (ATTEN08)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>If you want to change the schedule, please contact the academic secretary. If you want to change the project, please contact your coordinator.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $this->session->userdata('att_filter_jur'); ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$TahunID = $this->session->userdata('att_filter_tahun');
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td><?php echo tgl_singkat_eng($Tanggal); ?></td>
										<td>Time (Start - End)</td>
										<td>:</td>
										<td><?php echo substr($JamMulai,0,5)." - ".substr($JamSelesai,0,5);?></td>
									</tr>
									<tr>
										<td>Room</td>
										<td>:</td>
										<td><?php echo $RuangID; ?></td>
										<td>Teacher</td>
										<td>:</td>
										<td>
											<?php
												$nmdos = "";
												if($DosenID == $DosenIDku)
												{
													$nmdos = $NamaDosen;
												}
												if($DosenID2 == $DosenIDku)
												{
													$nmdos = $NamaDosen2;
												}
												if($DosenID3 == $DosenIDku)
												{
													$nmdos = $NamaDosen3;
												}
												if($DosenID4 == $DosenIDku)
												{
													$nmdos = $NamaDosen4;
												}
												if($DosenID5 == $DosenIDku)
												{
													$nmdos = $NamaDosen5;
												}
												if($DosenID6 == $DosenIDku)
												{
													$nmdos = $NamaDosen6;
												}
												echo $nmdos;
												$word = explode(" - ",$nmdos);
												$Dosenini = @$word[0];
											?>
										</td>
									</tr>
									<tr>
										<td>Created By</td>
										<td>:</td>
										<td><?php echo $login_buat; ?></td>
										<td>Created Date</td>
										<td>:</td>
										<td><?php echo $tanggal_buat;?></td>
									</tr>
									<tr>
										<td>Edited By</td>
										<td>:</td>
										<td><?php echo $login_edit; ?></td>
										<td>Edited Date</td>
										<td>:</td>
										<td><?php echo $tanggal_edit;?></td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("attendance/ptl_list_update"); ?>" method="POST">
										<div class="col-lg-12">
											<div class="form-group">
												<label>Project</label>
												<select name="MKID" title="" class="form-control round-form">
													<option value="">-- CHOOSE --</option>
													<?php
														if($rowmk)
														{
															foreach($rowmk as $rm)
															{
																if($rm->Nama != "")
																{
																	echo "<option value='$rm->MKID'";
																	if($MKID == $rm->MKID)
																	{
																		echo "selected";
																	}
																	echo ">$rm->MKID - $rm->Nama</option>";
																}
															}
														}
													?>
												</select>
												<?php
													if(!$rowmk)
													{
														echo "<b><font color='red'>Your project has not been determined by your coordinator. Please contact your coordinator to set your project.</font></b>";
													}
												?>
												<p class="help-block"></p>
												<font color="red">If the project does not appear, please contact your coordinator. Your coordinator must be setup your Project for your Subject.</font>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group">
												<label>Notes</label>
												<input type="hidden" name="Tanggal" value="<?php echo $Tanggal; ?>">
												<input type="hidden" name="PresensiID" value="<?php echo $PresensiID; ?>">
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<input type="hidden" name="JamMulai" value="<?php echo substr($JamMulai,0,5); ?>">
												<input type="hidden" name="JamSelesai" value="<?php echo substr($JamSelesai,0,5); ?>">
												<input type="hidden" name="RuangID" value="<?php echo $RuangID; ?>">
												<input type="hidden" name="DosenID" value="<?php echo $Dosenini; ?>">
												<textarea name="Catatan" class="form-control"><?php echo $Catatan; ?></textarea>
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>