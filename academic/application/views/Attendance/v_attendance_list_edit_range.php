		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Range</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_edit/$PresensiID"); ?>">Edit Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_change_range/$JadwalID/$PresensiID"); ?>">Edit Range (ATTEN08-03)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("attendance/ptl_change_range_update"); ?>" method="POST">
										<div class="col-lg-12">
											<div class="form-group">
												<label>Start Date</label>
												<input type="text" name="TglMulai" value="<?php echo $TglMulai; ?>" id="dpSCRange1" placeholder="yyyy-mm-dd" class="form-control" required>
												<input type="hidden" name="PresensiID" value="<?php echo $PresensiID; ?>">
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>End Date</label>
												<input type="text" name="TglSelesai" value="<?php echo $TglSelesai; ?>" id="dpSCRange2" placeholder="yyyy-mm-dd" class="form-control" required>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Created By</label>
												<input readonly type="text" value="<?php echo $login_buat; ?>" class="form-control">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Created Date</label>
												<input readonly type="text" value="<?php echo $tanggal_buat; ?>" class="form-control">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Edited By</label>
												<input readonly type="text" value="<?php echo $login_edit; ?>" class="form-control">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Edited Date</label>
												<input readonly type="text" value="<?php echo $tanggal_edit; ?>" class="form-control">
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("attendance/ptl_list_edit/$PresensiID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Update</button>
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>