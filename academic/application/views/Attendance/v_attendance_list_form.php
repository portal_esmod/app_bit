		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_form/$JadwalID"); ?>">Add Attendance (ATTEN06)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("attendance/ptl_list_insert"); ?>" method="POST">
										<div class="col-lg-12">
											<div class="form-group">
												<label>Date</label>
												<input type="text" name="Tanggal" id="dpSC1" placeholder="yyyy-mm-dd" class="form-control" required>
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<p class="help-block"><font color="green">If the time range does not match,</font> <a href="<?php echo site_url("attendance/ptl_change_range_form/$JadwalID"); ?>" ><b>click here!</b></a></p>
											</div>
											<div class="form-group">
												<label>Time (Start - End)</label>
												<br/>
												<div class="col-lg-3">
													<input type="text" name="JamMulai" class="form-control" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" required>
												</div>
												<div class="col-lg-3">
													<input type="text" name="JamSelesai" class="form-control" onkeydown="return angkaSaja(this, event);" onkeyup="javascript:tandaJam(this);" onmouseover="this.focus()" required>
												</div>
												<p class="help-block"></p>
											</div>
											<br/>
											<br/>
											<div class="form-group">
												<label>Room</label>
												<select name="RuangID" title="" class="form-control round-form" required>
													<option value="">-- CHOOSE --</option>
													<?php
														if($rowruang)
														{
															foreach($rowruang as $rr)
															{
																echo "<option value='$rr->RuangID'>$rr->RuangID - $rr->Nama</option>";
															}
														}
													?>
												</select>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Teacher</label>
												<select name="DosenID" title="" class="form-control round-form" required>
													<option value="">-- CHOOSE --</option>
													<?php
														if($NamaDosen != "")
														{
															echo "<option value='$DosenID'>$NamaDosen</option>";
														}
														if($NamaDosen2 != "")
														{
															echo "<option value='$DosenID2'>$NamaDosen2</option>";
														}
														if($NamaDosen3 != "")
														{
															echo "<option value='$DosenID3'>$NamaDosen3</option>";
														}
														if($NamaDosen4 != "")
														{
															echo "<option value='$DosenID4'>$NamaDosen4</option>";
														}
														if($NamaDosen5 != "")
														{
															echo "<option value='$DosenID5'>$NamaDosen5</option>";
														}
														if($NamaDosen6 != "")
														{
															echo "<option value='$DosenID6'>$NamaDosen6</option>";
														}
													?>
												</select>
												<p class="help-block"><font color="green">If you want to change or add new lecturer,</font> <a href="<?php echo site_url("attendance/ptl_change_lecturer_form/$JadwalID"); ?>" ><b>click here!</b></a></p>
											</div>
											<div class="form-group">
												<label>Project</label>
												<select name="MKID" title="" class="form-control round-form">
													<option value="">-- CHOOSE --</option>
													<?php
														if($rowmk)
														{
															foreach($rowmk as $rm)
															{
																if($rm->Nama != "")
																{
																	echo "<option value='$rm->MKID'>$rm->MKID - $rm->Nama</option>";
																}
															}
														}
													?>
												</select>
												<?php
													if(!$rowmk)
													{
														echo "<b><font color='red'>Your project has not been determined by your coordinator. Please contact your coordinator to set your project.</font></b>";
													}
												?>
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Notes</label>
												<textarea name="Catatan" class="form-control"></textarea>
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Save" id="my_button" class="btn btn-primary">
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>