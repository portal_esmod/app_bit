		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Recycle Bin</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_trash/$JadwalID"); ?>">Recycle Bin (ATTEN05)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Subject Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>ID Attendance</td>
										<td>:</td>
										<td><?php echo $JadwalID; ?></td>
									</tr>
									<tr>
										<td>Academic Years</td>
										<td>:</td>
										<td>
											<?php
												$t = $this->m_year->PTL_select($TahunID);
												echo $TahunID." - ".$t["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Program</td>
										<td>:</td>
										<td>
											<?php
												if($ProgramID == "REG")
												{
													echo $ProgramID." - REGULAR";
												}
												if($ProgramID == "INT")
												{
													echo $ProgramID." - INTENSIVE";
												}
												if($ProgramID == "SC")
												{
													echo $ProgramID." - SHORT COURSE";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												if($KelasID == 0)
												{
													$grup1 = explode(".",$KelasIDGabungan);
													$kelas1 = explode("^",@$grup1[0]);
													$kelas2 = explode("^",@$grup1[1]);
													$KelasID = @$kelas1[1];
													$k1 = $this->m_kelas->PTL_select_kelas($KelasID);
													$KelasID = @$kelas2[1];
													$k2 = $this->m_kelas->PTL_select_kelas($KelasID);
													$kls = @$kelas1[0].@$k1["Nama"]."/".@$kelas2[0].@$k2["Nama"];
													if($kls == "/")
													{
														echo "<font color='red'><b>Not set</b></font>";
													}
												}
												else
												{
													$k = $this->m_kelas->PTL_select_kelas($KelasID);
													echo $TahunKe.$k["Nama"];
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td title='<?php echo "SubjekID : ".$SubjekID; ?>'>
											<?php
												$s = $this->m_subjek->PTL_select($SubjekID);;
												echo $s["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Teacher</td>
										<td>:</td>
										<td title='<?php echo "DosenID : ".$DosenID; ?>'>
											<?php
												$d = $this->m_dosen->PTL_select($DosenID);
												$f1 = "";
												$f2 = "";
												if($d)
												{
													if($d["NA"] == "Y")
													{
														echo "<font color='red'><b title='This lecturer has resigned'>
															$d[Nama]
															</b></font>";
													}
													else
													{
														echo $d["Nama"];
													}
												}
												else
												{
													echo "";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td>
											<?php
												echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai);
											?>
										</td>
									</tr>
									<tr>
										<td>Short Link</td>
										<td>:</td>
										<td><a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>" class="btn btn-success">Scoring</a></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!--<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Notes</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Student Name & ID</th>
											<th>Notes</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowcatatan)
											{
												$n = 1;
												foreach($rowcatatan as $row)
												{
													// echo "<tr>
															// <td>$n</td>
															// <td>$row->MhswID</td>
															// <td>$row->comment</td>
														// </tr>";
													$n++;
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Attendance List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<table>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>" class="btn btn-warning"><< Back</a>
										</td>
									</tr>
								</table>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Room</th>
											<th>Lecturer</th>
											<th>Topic</th>
											<th>Attendance</th>
											<th>Activate</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												$sebelumnya = "";
												foreach($rowrecord as $row)
												{
													$DosenID = $row->DosenID;
													$dsn = $this->m_dosen->PTL_select($DosenID);
													$MKID = $row->MKID;
													$mkid = $this->m_mk->PTL_select($MKID);
													$nmmk = "";
													if($mkid)
													{
														$nmmk = "$mkid[Nama]<br/>";
													}
													$nmdsn = "";
													if($dsn)
													{
														$nmdsn = $dsn['Nama'];
													}
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr class='info'>";
													}
														echo "<td>$no</td>
															<td>$row->PresensiID</td>
															<td>".tgl_singkat_eng($row->Tanggal)."</td>
															<td>".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."</td>
															<td>$row->RuangID</td>
															<td>$nmdsn</td>
															<td>$nmmk$row->Catatan</td>
															<td><a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$TahunID/$MKID")."' class='btn btn-primary btn-circle'><i class='fa fa-list'></i></a></td>
															<td>";
													if($row->NA == "Y")
													{
										?>
															<a href="<?php echo site_url("attendance/ptl_list_undelete/$row->PresensiID/$JadwalID"); ?>" class="btn btn-success btn-circle"><i class="fa fa-check"></i></a>
										<?php
													}
													else
													{
										?>
															<a href="<?php echo site_url("attendance/ptl_list_delete/$row->PresensiID/$JadwalID"); ?>" class="btn btn-danger btn-circle" onclick="return confirm('Are you sure to delete of this data <?php echo $row->PresensiID; ?>?')"><i class="fa fa-times"></i></a>
										<?php
													}
													echo "</td>
														</tr>";
													$no++;
												}
											}
											else
											{
												echo "<tr>
														<td colspan='10'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>