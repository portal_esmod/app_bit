		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Students Attendance</h1>
                </div>
            </div>
			<center>
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Attendance Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<td>Schedule ID</td>
											<td>:</td>
											<td><?php echo $JadwalID; ?></td>
										</tr>
										<tr>
											<td>Presence ID</td>
											<td>:</td>
											<td><?php echo $PresensiID; ?></td>
										</tr>
										<tr>
											<td>Subject</td>
											<td>:</td>
											<td>
												<?php
													$jdw = $this->m_jadwal->PTL_select($JadwalID);
													$SubjekID = $jdw["SubjekID"];
													$sub = $this->m_subjek->PTL_select($SubjekID);
													echo $sub["Nama"];
												?>
											</td>
										</tr>
										<tr>
											<td>Date & Time</td>
											<td>:</td>
											<td><?php echo tgl_indo($Tanggal)." <sup>$JamMulai - $JamSelesai</sup>"; ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</center>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_trash/$JadwalID"); ?>">Recycle Bin</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID"); ?>">Students Attendance (ATTEN12)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<form action="<?php echo site_url("attendance/ptl_list_attendance_set"); ?>" method="POST">
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>ID</th>
												<th>Photo</th>
												<th>Student Name & ID</th>
												<th>Spec</th>
												<th colspan="2">Attendance</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$no = 1;
												$no2 = 1;
												$cekpresensi = "";
												if($rowrecord)
												{
													foreach($rowrecord as $row)
													{
														$MhswID = $row->MhswID;
														$m = $this->m_mahasiswa->PTL_select($MhswID);
														$StatusMhswID = "";
														$nama = "";
														if($m)
														{
															$StatusMhswID = $m["StatusMhswID"];
															$nama = $m["Nama"];
														}
														if($m)
														{
															if($m["Foto"] == "")
															{
																$foto = "foto_umum/user.jpg";
															}
															else
															{
																$foto = "foto_mahasiswa/".$m["Foto"];
																$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																if($exist)
																{
																	$foto = "foto_mahasiswa/".$m["Foto"];
																}
																else
																{
																	$foto = "foto_umum/user.jpg";
																}
															}
															$nama = $m["Nama"];
														}
														else
														{
															$foto = "foto_umum/user.jpg";
															$nama = "";
														}
														$KRSID = $row->KRSID;
														$TahunID = "";
														$reskrs = $this->m_krs->PTL_select($KRSID);
														if($reskrs)
														{
															$TahunID = $reskrs['TahunID'];
															$tidak = $reskrs['NA'];
															if($reskrs['NA'] == "Y")
															{
																$pesan = "The student has drop this subject";
															}
															else
															{
																$pesan = "";
															}
														}
														else
														{
															$tidak = "Y";
															$pesan = "The SYSTEM has been delete this subject permanently";
														}
														$k = $this->m_khs->PTL_select_att($MhswID,$TahunID);
														$StatusMhswID = "";
														$suspend = "";
														if($k)
														{
															$StatusMhswID = $k["StatusMhswID"];
															$suspend = $k["suspend"];
														}
														$st = $this->m_status->PTL_select($StatusMhswID);
														if($StatusMhswID == "A")
														{
															$status = "<font color='green'><b>$st[Nama]</b></font>";
														}
														else
														{
															$status = "<font color='red'><b>$st[Nama]</b></font>";
														}
														$SpesialisasiID = $k["SpesialisasiID"];
														$s = $this->m_spesialisasi->PTL_select($SpesialisasiID);
														$spesialisasi = "";
														if($s)
														{
															$spesialisasi = $s["Nama"];
														}
														if($row->Removed == "Y")
														{
															echo "<tr class='danger'>";
														}
														else
														{
															if($tidak == "Y")
															{
																echo "<tr class='warning'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
														}
														$Finance = "";
														if($suspend == "Y")
														{
															$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
														}
														if($tidak == "Y")
														{
															echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																<td>$row->PresensiMhswID</td>
																<td><img src='".base_url("ptl_storage/$foto")."' width='50px'/></td>
																<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("student/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$m[Nama]</a> ($status)$Finance</td>
																<td>$spesialisasi</td>
																<td colspan='3' align='center'><font color='red'><b>$pesan</b></font></td>";
															$no2++;
														}
														else
														{
															echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																<td>$row->PresensiMhswID</td>
																<td><img src='".base_url("ptl_storage/$foto")."' width='50px'/></td>
																<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("student/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>
																<td>$spesialisasi</td>
																<input type='hidden' name='PresensiMhswID$no' value='$row->PresensiMhswID'/>
																";
																// $Multi = "N";
																// if(($JamMulai < "12:30:00") AND ($JamSelesai > "12:30:00"))
																// {
																	// $Multi = "Y";
																// }
																// if(($JamMulai < "15:30:00") AND ($JamSelesai > "15:30:00"))
																// {
																	// $Multi = "Y";
																// }
																// if($Multi == "Y")
																// {
																	// echo "<input type='hidden' name='multi$no' value='Y'/>";
																	// echo "<td>
																			// <select name='setpresensis$no' title='Set Presence 1' class='form-control round-form'>";
																				// if($rowjenis1)
																				// {
																					// foreach($rowjenis1 as $rj1)
																					// {
																						// echo "<option value='$rj1->JenisPresensiID'";
																						// if($cekpresensi == "")
																						// {
																							// if($rj1->JenisPresensiID == $row->JenisPresensiID)
																							// {
																								// echo "selected";
																							// }
																						// }
																						// else
																						// {
																							// if($rj1->JenisPresensiID == $cekpresensi)
																							// {
																								// echo "selected";
																							// }
																						// }
																						// echo ">$rj1->JenisPresensiID - $rj1->Nama</option>";
																					// }
																				// }
																		// echo "</select>
																		// </td>
																		// <td>
																			// <select name='setpresensid$no' title='Set Presence 2' class='form-control round-form'>";
																				// if($rowjenis2)
																				// {
																					// foreach($rowjenis2 as $rj2)
																					// {
																						// echo "<option value='$rj2->JenisPresensiID'";
																						// if($cekpresensi == "")
																						// {
																							// if($rj2->JenisPresensiID == $row->JenisPresensiID2)
																							// {
																								// echo "selected";
																							// }
																						// }
																						// else
																						// {
																							// if($rj2->JenisPresensiID == $cekpresensi)
																							// {
																								// echo "selected";
																							// }
																						// }
																						// echo ">$rj2->JenisPresensiID - $rj2->Nama</option>";
																					// }
																				// }
																		// echo "</select>
																		// </td>";
																// }
																// else
																// {
																	$font1 = "";
																	if($row->JenisPresensiID == "P"){ $font1 = "style='background-color: #C6EC50;'"; }
																	if($row->JenisPresensiID == "E"){ $font1 = "style='background-color: #D782DB;'"; }
																	if($row->JenisPresensiID == "S"){ $font1 = "style='background-color: #63B9E0;'"; }
																	if($row->JenisPresensiID == "A"){ $font1 = "style='background-color: #E83737;'"; }
																	if($row->JenisPresensiID == "L"){ $font1 = "style='background-color: #FFFF73;'"; }
																	echo "<input type='hidden' name='multi$no' value='N'/>";
																	echo "<td colspan='2'>
																			<select $font1 name='setpresensis$no' title='Set Presence 1' class='form-control round-form'>";
																				if($rowjenis1)
																				{
																					foreach($rowjenis1 as $rj)
																					{
																						echo "<option value='$rj->JenisPresensiID'";
																						if($cekpresensi == "")
																						{
																							if($row->JenisPresensiID == $rj->JenisPresensiID)
																							{
																								echo "selected";
																							}
																						}
																						else
																						{
																							if($cekpresensi == $rj->JenisPresensiID)
																							{
																								echo "selected";
																							}
																						}
																						echo ">$rj->JenisPresensiID - $rj->Nama</option>";
																					}
																				}
																		echo "</select>
																		</td>";
																}
															echo "<input type='hidden' name='KRSID$no' value='$row->KRSID'/>";
															echo "<input type='hidden' name='MKID$no' value='$MKID'/>";
														// }
														echo "</tr>";
														$no++;
														$no2++;
													}
												}
												else
												{
												
													echo "<tr>
															<td colspan='8'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											?>
										</tbody>
									</table>
									<center>
										<?php
											if($rowrecord)
											{
												echo "<input type='hidden' name='PresensiID' value='$PresensiID'/>";
												echo "<input type='hidden' name='TahunID' value='$TahunID'/>";
												echo "<input type='hidden' name='total' value='$no'/>";
											}
										?>
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>