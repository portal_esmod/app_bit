		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Schedule</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_edit/$PresensiID"); ?>">Edit Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_change_lecturer_edit/$JadwalID/$PresensiID"); ?>">Edit Schedule (ATTEN08-05)</a>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>Program</td>
										<td>:</td>
										<td><?php echo $ProgramID; ?></td>
										<td>Year Id</td>
										<td>:</td>
										<td>
											<?php
												$restahun = $this->m_year->PTL_select($TahunID);
												$namaThn = "";
												if($restahun)
												{
													$namaThn = $restahun['Nama'];
												}
												echo $TahunID." - ".$namaThn;
											?>
										</td>
									</tr>
								</table>
								<div class="row">
									<form role="form" action="<?php echo site_url("attendance/ptl_change_lecturer_update"); ?>" method="POST">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Lecturer</label>
												<input type="text" name="DosenID" value="<?php echo $NamaDosen; ?>" id="Dosen1" class="form-control" required>
												<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
												<input type="hidden" name="PresensiID" value="<?php echo $PresensiID; ?>">
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 1</label>
												<input type="text" name="DosenID2" value="<?php echo $NamaDosen2; ?>" id="Dosen2" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 2</label>
												<input type="text" name="DosenID3" value="<?php echo $NamaDosen3; ?>" id="Dosen3" class="form-control" >
												<p class="help-block"></p>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Assistant 3</label>
												<input type="text" name="DosenID4" value="<?php echo $NamaDosen4; ?>" id="Dosen4" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 4</label>
												<input type="text" name="DosenID5" value="<?php echo $NamaDosen5; ?>" id="Dosen5" class="form-control" >
												<p class="help-block"></p>
											</div>
											<div class="form-group">
												<label>Assistant 5</label>
												<input type="text" name="DosenID6" value="<?php echo $NamaDosen6; ?>" id="Dosen6" class="form-control" >
												<p class="help-block"></p>
											</div>
										</div>
										<center>
											<a href="<?php echo site_url("attendance/ptl_list_edit/$PresensiID"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<input type="submit" value="Update" id="my_button" class="btn btn-primary">
										</center>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>