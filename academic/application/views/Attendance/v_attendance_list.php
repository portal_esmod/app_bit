		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance List</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List (ATTEN04)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Subject Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>ID Attendance</td>
										<td>:</td>
										<td><?php echo $JadwalID; ?></td>
									</tr>
									<tr>
										<td>Academic Years</td>
										<td>:</td>
										<td>
											<?php
												$t = $this->m_year->PTL_select($TahunID);
												echo $TahunID." - ".$t["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Program</td>
										<td>:</td>
										<td>
											<?php
												$add = "";
												if($ProgramID == "REG")
												{
													echo $ProgramID." - REGULAR";
												}
												if($ProgramID == "INT")
												{
													echo $ProgramID." - INTENSIVE";
													$add = "O";
												}
												if($ProgramID == "SC")
												{
													echo $ProgramID." - SHORT COURSE";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Class</td>
										<td>:</td>
										<td>
											<?php
												$kelas = "";
												$link_kelas = "";
												if($KelasID == 0)
												{
													$kelas = "";
													$wordgab = explode(".",$KelasIDGabungan);
													for($i=0;$i<30;$i++)
													{
														$wg = explode("^",@$wordgab[$i]);
														$KelasID = @$wg[1];
														$res = $this->m_kelas->PTL_select_kelas($KelasID);
														if($res)
														{
															$kls = $res["Nama"];
														}
														else
														{
															$kls = "";
														}
														if($i == 0)
														{
															$kelas .= @$wg[0].$kls;
															$link_kelas .= @$wg[0].$kls;
														}
														else
														{
															if($wg[0] != "")
															{
																$kelas .= ", ".@$wg[0].$kls;
																$link_kelas .= ".".@$wg[0].$kls;
															}
														}
													}
													echo "<font color='red'><b>MERGE</b></font> ($kelas)";
												}
												else
												{
													$k = $this->m_kelas->PTL_select_kelas($KelasID);
													if($add == "")
													{
														$add = $TahunKe;
													}
													echo $add.$k["Nama"];
													$kelas = $add.$k["Nama"];
													$link_kelas .= $add.$k["Nama"];
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>:</td>
										<td title='<?php echo "SubjekID : ".$SubjekID; ?>'>
											<?php
												$s = $this->m_subjek->PTL_select($SubjekID);;
												$JenisMKID = $s["JenisMKID"];
												$resmk = $this->m_jenis_mk->PTL_select($JenisMKID);
												$namamk = "";
												if($resmk)
												{
													$namamk = strtoupper($resmk['Nama']);
												}
												echo $s["Nama"];
											?>
										</td>
									</tr>
									<tr>
										<td>Type</td>
										<td>:</td>
										<td><?php echo $namamk; ?></td>
									</tr>
									<tr>
										<td>Teacher</td>
										<td>:</td>
										<td title='<?php echo "DosenID : ".$DosenID; ?>'>
											<?php
												$d = $this->m_dosen->PTL_select($DosenID);
												$f1 = "";
												$f2 = "";
												if($d)
												{
													if($d["NA"] == "Y")
													{
														echo "<font color='red'><b title='This lecturer has resigned'>
															$d[Nama]
															</b></font>";
													}
													else
													{
														echo $d["Nama"];
													}
												}
												else
												{
													echo "";
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Date</td>
										<td>:</td>
										<td>
											<?php
												echo tgl_singkat_eng($TglMulai)." - ".tgl_singkat_eng($TglSelesai);
											?>
										</td>
									</tr>
									<tr>
										<td>Total Standard Attendance</td>
										<td>:</td>
										<td><?php echo $RencanaKehadiran." session"; ?></td>
									</tr>
									<tr>
										<td>Maximum Absent</td>
										<td>:</td>
										<td><?php echo $MaxAbsen." session"; ?></td>
									</tr>
									<tr>
										<td>Created By</td>
										<td>:</td>
										<td><?php echo $login_buat; ?></td>
									</tr>
									<tr>
										<td>Created Date</td>
										<td>:</td>
										<td><?php echo $tanggal_buat; ?></td>
									</tr>
									<tr>
										<td>Edited By</td>
										<td>:</td>
										<td><?php echo $login_edit; ?></td>
									</tr>
									<tr>
										<td>Edited Date</td>
										<td>:</td>
										<td><?php echo $tanggal_edit; ?></td>
									</tr>
									<tr>
										<td>Short Link</td>
										<td>:</td>
										<td><a href="<?php echo site_url("scoring/ptl_list/$JadwalID"); ?>" class="btn btn-success">Scoring</a></td>
									</tr>
									<tr>
										<td>Download Project Sheet</td>
										<td>:</td>
										<td>
											<?php
												if(substr($file_project,8) == "")
												{
													echo "<font color='red'>No file uploaded</font>";
												}
												else
												{
											?>
													<a href="<?php echo base_url("attendance/ptl_download_project_sheet/$JadwalID/$file_project"); ?>" title="<?php echo $file_project; ?>" class="btn btn-info" >Download</a>
											<?php
												}
											?>
										</td>
									</tr>
									<tr>
										<td>Download Evaluation Sheet</td>
										<td>:</td>
										<td>
											<?php
												if(substr($file_evaluation,8) == "")
												{
													echo "<font color='red'>No file uploaded</font>";
												}
												else
												{
											?>
													<a href="<?php echo base_url("attendance/ptl_download_evaluation_sheet/$JadwalID/$file_evaluation"); ?>" title="<?php echo $file_evaluation; ?>" class="btn btn-info" >Download</a>
											<?php
												}
											?>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Notes</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>You can leave notes on student.
								</div>
								<center><a href="<?php echo site_url("attendance/ptl_notes_form/$JadwalID"); ?>" class="btn btn-primary">Add Notes</a></center>
								<br/>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Student Name & ID</th>
											<th>Title</th>
											<th>Notes</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowcatatan)
											{
												$n = 1;
												foreach($rowcatatan as $row)
												{
													$MhswID = $row->MhswID;
													$m = $this->m_mahasiswa->PTL_select($MhswID);
													$nama = "";
													if($m)
													{
														$nama = $m['Nama'];
													}
													echo "<tr>
															<td title='noteid : $row->noteid ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$n</td>
															<td><a href='".site_url("attendance/ptl_notes_edit/$row->noteid/$JadwalID")."' title='Edit Notes'>$row->MhswID - $nama</a></td>
															<td>$row->title</td>
															<td>$row->comment</td>
														</tr>";
													$n++;
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Attendance List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Information: </a>
									<?php
										echo "<font color='blue'><b>< H = Not Yet Time.</b></font><font color='green'><b> > H until H+1 = On Time.</b></font><font color='#ED9C28'><b> > H+2 until H+3 = Almost.</b></font><font color='red'><b> > H+3 = Late.</b></font>";
									?>
									<center>
										<h3><font color="red">Project must be set for each session.</font></h3>
										<?php
											if($JenisMKID == "5")
											{
												echo "<h3><font color='red'><b>ONE PROJECT FOR ONE DAY</b></font></h3>";
											}
										?>
									</center>
								</div>
								<table>
									<tr>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<a href="<?php echo site_url("attendance"); ?>" class="btn btn-warning"><< Back</a>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<a href="<?php echo site_url("attendance/ptl_pdf_dpm/$JadwalID/$SubjekID/$TahunID/$link_kelas"); ?>" class="btn btn-info">Print DPM</a>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<a href="<?php echo site_url("attendance/ptl_pdf_dpd/$JadwalID/$SubjekID/$TahunID/$link_kelas"); ?>" class="btn btn-info">Print DPD</a>
										</td>
										<?php
											if(!stristr($_COOKIE["akses"],"LECTURER"))
											{
										?>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<a href="<?php echo site_url("attendance/ptl_list_form/$JadwalID"); ?>" class="btn btn-info">Add Attendance</a>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<a href="<?php echo site_url("attendance/ptl_list_project_sheet_form/$JadwalID"); ?>" class="btn btn-info">Add Project Sheet</a>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<a href="<?php echo site_url("attendance/ptl_list_enroll/$JadwalID/$SubjekID/$TahunID/$ProgramID/$link_kelas"); ?>" class="btn btn-success">Enroll Only This Subject</a>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<a href="<?php echo site_url("attendance/ptl_list_trash/$JadwalID"); ?>" class="btn btn-danger">Recycle Bin</a>
												</td>
										<?php
											}
										?>
									</tr>
								</table>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>ID</th>
											<th>Date</th>
											<th>Time</th>
											<th>Room</th>
											<th>Lecturer</th>
											<th>Topic</th>
											<th class="center">Remark</th>
											<th>Status</th>
											<th>Attendance</th>
											<th>Edit</th>
											<?php
												$totalTable = 11;
												if(!stristr($_COOKIE["akses"],"LECTURER"))
												{
													$totalTable = 12;
											?>
													<th>Delete</th>
											<?php
												}
											?>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$TotalStudent = count($this->m_krs->PTL_all_jadwal($JadwalID));
												$no = 1;
												$sebelumnya = "";
												foreach($rowrecord as $row)
												{
													$Tanggal = $row->Tanggal;
													$dosen_tanggal_set = $row->dosen_tanggal_set;
													if($dosen_tanggal_set == "")
													{
														$pesan = "<font color='blue'><b>NOT&nbsp;INPUTTED</b></font>";
													}
													else
													{
														$tglA = str_replace("-","",$Tanggal);
														$tglB = str_replace("-","",substr($dosen_tanggal_set,0,10));
														$tglC = $tglB - $tglA;
														$Mpl = "";
														if($tglC < 0)
														{
															$Mpl = "-";
														}
														$selisih = ((abs(strtotime($Tanggal) - strtotime($tglB)))/(60*60*24));
														$pesan = "";
														if($selisih < '0')
														{
															$pesan = "<font color='blue'><b>NOT&nbsp;YET&nbsp;TIME</b></font>";
														}
														else
														{
															if(($selisih >= '0') AND ($selisih <= '1'))
															{
																$pesan = "<font color='green'><b>ON&nbsp;TIME</b></font>";
															}
															else
															{
																if(($selisih >= '2') AND ($selisih <= '3'))
																{
																	$pesan = "<font color='#ED9C28'><b>ALMOST</b></font>";
																}
																else
																{
																	if($selisih > '3')
																	{
																		$pesan = "<font color='red'><b>LATE</b></font>";
																	}
																	else
																	{
																		$pesan = "<font color='red'><b>OUT&nbsp;OF&nbsp;RANGE</b></font>";
																	}
																}
															}
														}
													}
													$PresensiID = $row->PresensiID;
													$checklist = $this->m_presensi_mahasiswa->PTL_all_select($PresensiID);
													$checkremoved = $this->m_presensi_mahasiswa->PTL_all_select_removed($PresensiID);
													$TotalCheck = 0;
													$TotalPresent = 0;
													if($checklist)
													{
														foreach($checklist as $cl)
														{
															if($cl->JenisPresensiID == "")
															{
																$TotalCheck++;
															}
															else
															{
																$TotalPresent++;
															}
														}
													}
													if(count($checkremoved) > 0)
													{
														$TotalCheck = $TotalCheck - count($checkremoved);
														$TotalPresent = $TotalPresent + count($checkremoved) - 1;
													}
													if($checklist)
													{
														if($TotalCheck > 0)
														{
															$checkYN = "<img src='".base_url("assets/dashboard/img/X.png")."' title='Some student not set the attendance.'>";
														}
														else
														{
															if($TotalStudent != $TotalPresent)
															{
																$SisaStudent = $TotalStudent - $TotalPresent;
																$checkYN = "<img src='".base_url("assets/dashboard/img/X.png")."' title='Some student not set the attendance. $TotalPresent check, $SisaStudent uncheck from $TotalStudent students. ".count($checkremoved)."'>";
															}
															else
															{
																$checkYN = "<img src='".base_url("assets/dashboard/img/Y.gif")."' title='All student attendance was completed.'>";
															}
														}
													}
													else
													{
														$checkYN = "<img src='".base_url("assets/dashboard/img/M.png")."' title='Lecturer not set all attendance.'>";
													}
													$DosenID = $row->DosenID;
													$dsn = $this->m_dosen->PTL_select($DosenID);
													$MKID = $row->MKID;
													$mkid = $this->m_mk->PTL_select($MKID);
													$nmmk = "";
													if($mkid)
													{
														$nmmk = "<font color='red'><b>$mkid[Nama]</b></font><br/>";
													}
													$nmdsn = "";
													if($dsn)
													{
														$nmdsn = $dsn['Nama'];
													}
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr class='info'>";
													}
														echo "<td>$no</td>
															<td>$row->PresensiID</td>
															<td>".nama_hari_eng($row->Tanggal).", ".tgl_singkat_eng($row->Tanggal)."</td>
															<td>".substr($row->JamMulai,0,5)." - ".substr($row->JamSelesai,0,5)."</td>
															<td>$row->RuangID</td>
															<td>$nmdsn</td>
															<td>$nmmk$row->Catatan</td>
															<td class='center'>$pesan</td>
															<td class='center'>$checkYN</td>
															<td class='center'><a href='".site_url("attendance/ptl_list_attendance/$row->PresensiID/$TahunID/$MKID")."' class='btn btn-primary btn-circle'><i class='fa fa-list'></i></a></td>
															<td class='center'><a href='".site_url("attendance/ptl_list_edit/$row->PresensiID")."' class='btn btn-success btn-circle'><i class='fa fa-list'></i></a></td>";
															if(!stristr($_COOKIE["akses"],"LECTURER"))
															{
																echo "<td>";
																if($row->NA == "Y")
																{
										?>
																		<a href="<?php echo site_url("attendance/ptl_list_undelete/$row->PresensiID/$JadwalID"); ?>" class="btn btn-success btn-circle"><i class="fa fa-check"></i></a>
										<?php
																}
																else
																{
										?>
																		<a href="<?php echo site_url("attendance/ptl_list_delete/$row->PresensiID/$JadwalID"); ?>" class="btn btn-danger btn-circle" onclick="return confirm('Are you sure to delete of this data <?php echo $row->PresensiID; ?>?')"><i class="fa fa-times"></i></a>
										<?php
																}
															}
																echo "</td>";
													echo "</tr>";
													$no++;
												}
											}
											else
											{
												echo "<tr>
														<td colspan='$totalTable'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
				if($JenisMKID == "5")
				{
			?>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<center>Attendance Score</center>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="alert alert-info">
											<a class="alert-link">Notes: </a>You can edit Attendance Score. This page appear only for EXTRA CURRICULAR.
										</div>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>Attendance ID</th>
													<th>Attendance Status</th>
													<th>Score</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rownilai)
													{
														$no = 0;
														foreach($rownilai as $row)
														{
															$JenisPresensiID = $row->JenisPresensiID;
															$res = $this->m_jenis_presensi->PTL_select($JenisPresensiID);
															$nama = "";
															if($res)
															{
																$nama = $res["Nama"];
															}
															$no++;
															echo "<tr>
																	<td align='center' title='Nilai3ID : $row->Nilai3ID'>$row->JenisPresensiID</td>
																	<td>$nama</td>
																	<td align='right'>$row->Score</td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("attendance_score/ptl_edit/$row->Nilai3ID/ATTENDANCE/$JadwalID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																	</td>
																</tr>";
														}
													}
													else
													{
														echo "<tr>
																<td colspan='4' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php
				}
			?>
        </div>