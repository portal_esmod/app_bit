		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Notes</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_notes_edit/$noteid/$JadwalID"); ?>">Edit Notes (ATTEN29)</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Detail
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<form action="<?php echo site_url("attendance/ptl_notes_update"); ?>" method="POST">
									<table class="table">
										<tr>
											<td>
												<input type="text" name="title" value="<?php echo $title; ?>" class="form-control" placeholder="Title">
												<input type="hidden" name="noteid" value="<?php echo $noteid; ?>">
												<textarea name="comment" class="form-control" placeholder="Notes"><?php echo $comment; ?></textarea>
											</td>
										</tr>
									</table>
									<center>
										<input type="hidden" name="JadwalID" value="<?php echo $JadwalID; ?>">
										<input type="submit" value="Save" id="my_button" class="btn btn-primary">
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>