		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Students Attendance</h1>
				</div>
			</div>
				<?php
					if($warning != "")
					{
						echo "<div class='alert alert-warning'>
								<a class='alert-link'>Warning:</a> <font color='red'>You must fill out the following Students Attendance. So you can fill in the project score ($NamaProject). After you set the attendance, system will redirect automatically to scoring before.</font>
							</div>";
					}
				?>
			<center>
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Attendance Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<td>Schedule ID</td>
											<td>:</td>
											<td><?php echo $JadwalID; ?></td>
										</tr>
										<tr>
											<td>Presence ID</td>
											<td>:</td>
											<td><?php echo $PresensiID; ?></td>
										</tr>
										<tr>
											<td>Subject</td>
											<td>:</td>
											<td>
												<?php
													$jdw = $this->m_jadwal->PTL_select($JadwalID);
													$SubjekID = $jdw["SubjekID"];
													$sub = $this->m_subjek->PTL_select($SubjekID);
													$JenisMKID = $sub["JenisMKID"];
													$resmk = $this->m_jenis_mk->PTL_select($JenisMKID);
													$namamk = "";
													if($resmk)
													{
														$namamk = strtoupper($resmk['Nama']);
													}
													echo $sub["Nama"];
												?>
											</td>
										</tr>
										<tr>
											<td>Type</td>
											<td>:</td>
											<td><?php echo $namamk; ?></td>
										</tr>
										<tr>
											<td>Project</td>
											<td>:</td>
											<td>
												<?php
													if($NamaProject == "")
													{
														echo "<font color='red'><b>Not set</b></font>";
													}
													else
													{
														echo $NamaProject;
													}
												?>
											</td>
										</tr>
										<tr>
											<td>Date & Time</td>
											<td>:</td>
											<td><?php echo tgl_singkat_eng($Tanggal)." <sup>".substr($JamMulai,0,5)." - ".substr($JamSelesai,0,5)."</sup>"; ?></td>
										</tr>
										<tr>
											<td>Created By</td>
											<td>:</td>
											<td><?php echo $login_buat; ?></td>
										</tr>
										<tr>
											<td>Created Date</td>
											<td>:</td>
											<td><?php echo $tanggal_buat; ?></td>
										</tr>
										<tr>
											<td>Edited By</td>
											<td>:</td>
											<td><?php echo $login_edit; ?></td>
										</tr>
										<tr>
											<td>Edited Date</td>
											<td>:</td>
											<td><?php echo $tanggal_edit; ?></td>
										</tr>
										<tr>
											<td>Last Attendance Set By</td>
											<td>:</td>
											<td><?php echo $login_set; ?></td>
										</tr>
										<tr>
											<td>Last Attendance Set (Date)</td>
											<td>:</td>
											<td><?php echo $tanggal_set; ?></td>
										</tr>
										<tr>
											<td><font color="blue"><b>Teacher Attendance Set By</b></font></td>
											<td>:</td>
											<td><?php echo $dosen_login_set; ?></td>
										</tr>
										<tr>
											<td><font color="blue"><b>Teacher Attendance Set (Date)</b></font></td>
											<td>:</td>
											<td>
												<?php
													if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
													{
														$TglSet = "";
														$TimeSet = "";
														if($dosen_tanggal_set != "")
														{
															$word = explode(" ",$dosen_tanggal_set);
															$TglSet = $word[0];
															$TimeSet = $word[1];
														}
												?>
														<form action="<?php echo site_url("attendance/ptl_set_tanggal"); ?>" method="POST" id="formId">
															<table class="table table-striped table-bordered table-hover">
																<tr>
																	<td title="<?php echo $dosen_tanggal_set; ?>">
																		<input type="text" name="cektanggal" value="<?php echo $TglSet; ?>" id="Date1" class="form-control">
																		<input type="hidden" name="cekwaktu" value="<?php echo $TimeSet; ?>">
																		<input type="hidden" name="PresensiID" value="<?php echo $PresensiID; ?>">
																		<input type="hidden" name="TahunID" value="<?php echo $TahunID; ?>">
																		<input type="hidden" name="MKID" value="<?php echo $MKID; ?>">
																	</td>
																	<td><?php echo $dosen_tanggal_set; ?></td>
																</tr>
															</table>
														</form>
												<?php
													}
													else
													{
														echo $dosen_tanggal_set;
													}
												?>
											</td>
										</tr>
										<tr>
											<td><font color="blue"><b>Status</b></font></td>
											<td>:</td>
											<td>
												<?php
													$h = "-7";
													$hm = $h * 60;
													$ms = $hm * 60;
													$NTTanggal = gmdate("Y-m-d", time()-($ms));
													$NTtglA = str_replace("-","",$Tanggal);
													$NTtglB = str_replace("-","",$NTTanggal);
													$NTtglC = $NTtglB - $NTtglA;
													$mp = "";
													if($NTtglC < 0)
													{
														$mp = "-";
													}
													$NTselisih = $mp.((abs(strtotime($Tanggal) - strtotime($NTTanggal)))/(60*60*24));
													$notifpesan = "";
													if($NTselisih < '0')
													{
														$notifpesan = "NOT YET TIME";
													}
													else
													{
														if(($NTselisih >= '0') AND ($NTselisih <= '1'))
														{
															$notifpesan = "ON TIME";
														}
														else
														{
															if(($NTselisih >= '2') AND ($NTselisih <= '3'))
															{
																$notifpesan = "ALMOST";
															}
															else
															{
																if($NTselisih > '3')
																{
																	$notifpesan = "LATE";
																}
																else
																{
																	$notifpesan = "OUT OF RANGE";
																}
															}
														}
													}
													if($dosen_tanggal_set == "")
													{
														$pesan = "<a href='#' class='btn btn-info'>NOT INPUTTED</a>";
													}
													else
													{
														$tglA = str_replace("-","",$Tanggal);
														$tglB = str_replace("-","",substr($dosen_tanggal_set,0,10));
														$tglC = $tglB - $tglA;
														$Mpl = "";
														if($tglC < 0)
														{
															$Mpl = "-";
														}
														$selisih = ((abs(strtotime($Tanggal) - strtotime($tglB)))/(60*60*24));
														$pesan = "";
														if($selisih < '0')
														{
															$pesan = "<a href='#' class='btn btn-info'>NOT YET TIME</a>";
														}
														else
														{
															if(($selisih >= '0') AND ($selisih <= '1'))
															{
																$pesan = "<a href='#' class='btn btn-success'>ON TIME</a>";
															}
															else
															{
																if(($selisih >= '2') AND ($selisih <= '3'))
																{
																	$pesan = "<a href='#' class='btn btn-warning'>ALMOST</a>";
																}
																else
																{
																	if($selisih > '3')
																	{
																		$pesan = "<a href='#' class='btn btn-danger'>LATE</a>";
																	}
																	else
																	{
																		$pesan = "<a href='#' class='btn btn-danger'>OUT OF RANGE</a>";
																	}
																}
															}
														}
													}
													echo $pesan;
												?>
											</td>
										</tr>
										<tr>
											<td><font color="green"><b>wInformation</b></font></td>
											<td>:</td>
											<td>
												<?php echo "
															< H = Not Yet Time
															<br/>
															> H until H+1 = On Time
															<br/>
															> H+2 until H+3 = Almost
															<br/>
															> H+3 = Late
															";
												?>
											</td>
										</tr>
										<tr>
											<td>Short Link</td>
											<td>:</td>
											<td><a href="<?php echo site_url("scoring/ptl_list/$JadwalID/$MKID"); ?>" class="btn btn-success">Scoring</a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Lecturer : <?php echo $NamaDosen; ?>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<?php
										$foto = "ptl_storage/foto_umum/user.jpg";
										$foto_preview = "ptl_storage/foto_umum/user.jpg";
										$size = "172px";
										if($FotoDosen != "")
										{
											$foto = "../hris/system_storage/foto_karyawan/$FotoDosen";
											$exist = file_exists_remote(base_url("$foto"));
											if($exist)
											{
												$foto = $foto;
												$source_photo = base_url("$foto");
												$info = pathinfo($source_photo);
												$foto_preview = "../hris/system_storage/foto_karyawan/".$info["filename"]."_compress.".$info["extension"];
												$exist1 = file_exists_remote(base_url("$foto_preview"));
												if($exist1)
												{
													$foto_preview = $foto_preview;
												}
												else
												{
													$foto_preview = $foto;
												}
												$size = "155px";
											}
											else
											{
												$foto = "ptl_storage/foto_umum/user.jpg";
												$foto_preview = "ptl_storage/foto_umum/user.jpg";
											}
										}
									?>
									<a class="fancybox" title="<?php echo "$DosenID - $NamaDosen"; ?>" href="<?php echo base_url("$foto"); ?>" data-fancybox-group="gallery">
										<img class="img-polaroid" src="<?php echo base_url("$foto_preview"); ?>" width="<?php echo $size; ?>" alt="" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</center>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID"); ?>">Students Attendance (ATTEN12)</a>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>If some students do not appear, then the possibility of the student has not paid the bill of semester or academic staff has not perform enrollment. Contact Finance Team or Academic Team.
								</div>
								<?php
									if($NamaProject == "")
									{
										echo "<center><font color='red'><h3>Attendance function will appear if you have chosen a project.</h3></font></center>";
								?>
										<table class="table">
												<thead>
													<tr>
														<th>#</th>
														<th>ID</th>
														<th>Photo</th>
														<th>Student Name & ID</th>
														<th>Spec</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$no = 1;
														$no2 = 1;
														if($rowrecord)
														{
															foreach($rowrecord as $row)
															{
																$MhswID = $row->MhswID;
																$m = $this->m_mahasiswa->PTL_select($MhswID);
																$StatusMhswID = "";
																$nama = "";
																$foto = "foto_umum/user.jpg";
																$foto_preview = "foto_umum/user.jpg";
																if($m)
																{
																	if($m["Foto"] != "")
																	{
																		$foto = "foto_mahasiswa/".$m["Foto"];
																		$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																		if($exist)
																		{
																			$foto = $foto;
																			$source_photo = base_url("ptl_storage/$foto");
																			$info = pathinfo($source_photo);
																			$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																			$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																			if($exist1)
																			{
																				$foto_preview = $foto_preview;
																			}
																			else
																			{
																				$foto_preview = $foto;
																			}
																		}
																		else
																		{
																			$foto = "foto_umum/user.jpg";
																			$foto_preview = "foto_umum/user.jpg";
																		}
																	}
																	$StatusMhswID = $m["StatusMhswID"];
																	$nama = $m["Nama"];
																}
																$k = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
																$StatusMhswID = "";
																if($k)
																{
																	$StatusMhswID = $k["StatusMhswID"];
																}
																$st = $this->m_status->PTL_select($StatusMhswID);
																if($StatusMhswID == "A")
																{
																	$status = "<font color='green'><b>$st[Nama]</b></font>";
																}
																else
																{
																	$status = "<font color='red'><b>$st[Nama]</b></font>";
																}
																$SpesialisasiID = $k["SpesialisasiID"];
																$s = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																$spesialisasi = "";
																if($s)
																{
																	$spesialisasi = $s["Nama"];
																}
																$KHSID = '';
																$KRSID = $row->KRSID;
																$reskrs = $this->m_krs->PTL_select($KRSID);
																if($reskrs)
																{
																	$KHSID = $reskrs['KHSID'];
																	$tidak = $reskrs['NA'];
																	if($reskrs['NA'] == "Y")
																	{
																		$pesan = "The student has drop this subject";
																	}
																	else
																	{
																		$pesan = "";
																	}
																}
																else
																{
																	$tidak = "Y";
																	$pesan = "The SYSTEM has been delete this subject permanently";
																}
																$reskhs = $this->m_khs->PTL_select($KHSID);
																$Finance = "";
																if($reskhs)
																{
																	if($reskhs['suspend'] == "Y")
																	{
																		$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																	}
																}
																if($row->Removed == "Y")
																{
																	echo "<tr class='danger'>";
																}
																else
																{
																	if($tidak == "Y")
																	{
																		echo "<tr class='warning'>";
																	}
																	else
																	{
																		echo "<tr class='info'>";
																	}
																}
																if($tidak == "Y")
																{
																	echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																		<td><a href='".site_url("drop/ptl_search/".$row->MhswID."_-_".str_replace(" ","_",$m['Nama']))."' title='Go to Student Subject Management' target='_blank'><b>$row->PresensiMhswID</b></a></td>
																		<td>
																			<a class='fancybox' title='$row->MhswID - $m[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																				<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																			</a>
																		</td>
																		<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$m[Nama]</a> ($status)$Finance</td>
																		<td>$spesialisasi</td>";
																}
																else
																{
																	echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																		<td><a href='".site_url("drop/ptl_search/".$row->MhswID."_-_".str_replace(" ","_",$m['Nama']))."' title='Go to Student Subject Management' target='_blank'><b>$row->PresensiMhswID</b></a></td>
																		<td>
																			<a class='fancybox' title='$row->MhswID - $m[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																				<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																			</a>
																		</td>
																		<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>
																		<td>$spesialisasi</td>
																		";
																}
																echo "</tr>";
																$no++;
																$no2++;
															}
														}
														else
														{
														
															echo "<tr>
																	<td colspan='8'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																</tr>";
														}
													?>
												</tbody>
											</table>
								<?php
									}
									else
									{
								?>
										<table>
											<tr>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
											</tr>
											<tr>
												<td>
													Set All Student to
												</td>
												<td>
													&nbsp;&nbsp;:&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("attendance/ptl_set_presensi"); ?>" method="POST">
														<select name="cekpresensi" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
															<option value='<?php echo "_".$PresensiID."_".$TahunID."_".$MKID; ?>'>-- CHOOSE --</option>
															<?php
																$cekpresensi = $this->session->userdata('att_set_presensi');
																if($rowjenis1)
																{
																	foreach($rowjenis1 as $rj1)
																	{
																		echo "<option value='".$rj1->JenisPresensiID."_".$PresensiID."_".$TahunID."_".$MKID."'";
																		if($cekpresensi == $rj1->JenisPresensiID)
																		{
																			echo "selected";
																		}
																		echo ">$rj1->JenisPresensiID - $rj1->Nama</option>";
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
										<form name="form[0]" action="<?php echo site_url("attendance/ptl_list_attendance_set"); ?>" method="POST" enctype="multipart/form-data">
												</td>
											</tr>
										</table>
											<table class="table">
												<thead>
													<tr>
														<th>#</th>
														<th>ID</th>
														<th>Photo</th>
														<th>Student Name & ID</th>
														<th>Spec<br/>File</th>
														<th colspan="2">Attendance<br/>Upload&nbsp;File</th>
														<th>Remove</th>
														<th><input type="checkbox" id="checkt-all"/>Select&nbsp;All</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$no = 1;
														$no2 = 1;
														if($rowrecord)
														{
															foreach($rowrecord as $row)
															{
																$MhswID = $row->MhswID;
																$m = $this->m_mahasiswa->PTL_select($MhswID);
																$StatusMhswID = "";
																$nama = "";
																$foto = "foto_umum/user.jpg";
																$foto_preview = "foto_umum/user.jpg";
																if($m)
																{
																	if($m["Foto"] != "")
																	{
																		$foto = "foto_mahasiswa/".$m["Foto"];
																		$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																		if($exist)
																		{
																			$foto = $foto;
																			$source_photo = base_url("ptl_storage/$foto");
																			$info = pathinfo($source_photo);
																			$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																			$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																			if($exist1)
																			{
																				$foto_preview = $foto_preview;
																			}
																			else
																			{
																				$foto_preview = $foto;
																			}
																		}
																		else
																		{
																			$foto = "foto_umum/user.jpg";
																			$foto_preview = "foto_umum/user.jpg";
																		}
																	}
																	$StatusMhswID = $m["StatusMhswID"];
																	$nama = $m["Nama"];
																}
																$k = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
																$StatusMhswID = "";
																$suspend = "";
																if($k)
																{
																	$StatusMhswID = $k["StatusMhswID"];
																	$suspend = $k["suspend"];
																}
																$st = $this->m_status->PTL_select($StatusMhswID);
																if($StatusMhswID == "A")
																{
																	$status = "<font color='green'><b>$st[Nama]</b></font>";
																}
																else
																{
																	$status = "<font color='red'><b>$st[Nama]</b></font>";
																}
																if($StatusMhswID == "A")
																{
																	$SpesialisasiID = $k["SpesialisasiID"];
																	$s = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																	$spesialisasi = "";
																	if($s)
																	{
																		$spesialisasi = $s["Nama"];
																	}
																	$KRSID = $row->KRSID;
																	$reskrs = $this->m_krs->PTL_select($KRSID);
																	if($reskrs)
																	{
																		$tidak = $reskrs['NA'];
																		if($reskrs['NA'] == "Y")
																		{
																			$pesan = "The student has drop this subject";
																		}
																		else
																		{
																			$pesan = "";
																		}
																	}
																	else
																	{
																		$tidak = "Y";
																		$pesan = "The SYSTEM has been delete this subject permanently";
																	}
																	if($row->Removed == "Y")
																	{
																		echo "<tr class='danger'>";
																	}
																	else
																	{
																		if($tidak == "Y")
																		{
																			echo "<tr class='warning'>";
																		}
																		else
																		{
																			echo "<tr class='info'>";
																		}
																	}
																	$Finance = "";
																	if($suspend == "Y")
																	{
																		$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																	}
																	$resfiles = $this->m_krs2->PTL_select_spesifik($KRSID,$PresensiID);
																	$files = "";
																	if($resfiles)
																	{
																		$files = $resfiles["files"];
																	}
																	if($tidak == "Y")
																	{
																		echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																			<td><a href='".site_url("drop/ptl_search/".$row->MhswID."_-_".str_replace(" ","_",$m['Nama']))."' title='Go to Student Subject Management' target='_blank'><b>$row->PresensiMhswID</b></a></td>
																			<td>
																				<a class='fancybox' title='$row->MhswID - $m[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																					<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																				</a>
																			</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$m[Nama]</a> ($status)$Finance</td>
																			<td>$spesialisasi<br/>";
																				if($files != "")
																				{
													?>
																					<a href="<?php echo base_url(); ?>attendance/ptl_download_file/<?php echo $files; ?>" title="<?php echo $files; ?>" class="btn btn-danger btn-sm btn-round btn-line" ><?php $file_order = substr($files,-15); echo $file_order; ?></a>
													<?php
																				}
																		echo "</td>
																			<td colspan='2' align='center'><font color='red'><b>$pesan</b></font></td>
																			<td>";
													?>
																				<a href="<?php echo site_url("attendance/ptl_list_attendance_delete/$row->PresensiMhswID/$PresensiID/$TahunID/$MKID"); ?>" class="btn btn-danger btn-circle" onclick="return confirm('Are you sure want to delete this data <?php echo strtoupper($m["Nama"]); ?> from Attendance Permanently?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
													<?php
																	echo "</td>
																		<td></td>";
																	}
																	else
																	{
																		echo "<td title='PresensiMhswID : $row->PresensiMhswID ~ KRSID : $row->KRSID'>$no2</td>
																			<td><a href='".site_url("drop/ptl_search/".$row->MhswID."_-_".str_replace(" ","_",$m['Nama']))."' title='Go to Student Subject Management' target='_blank'><b>$row->PresensiMhswID</b></a></td>
																			<td>
																				<a class='fancybox' title='$row->MhswID - $m[Nama]' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																					<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																				</a>
																			</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>
																			<td>$spesialisasi<br/>";
																				if($files != "")
																				{
													?>
																					<a href="<?php echo base_url(); ?>attendance/ptl_download_file/<?php echo $files; ?>" title="<?php echo $files; ?>" class="btn btn-danger btn-sm btn-round btn-line" ><?php $file_order = substr($files,-15); echo $file_order; ?></a>
													<?php
																				}
																		echo "</td>
																			<input type='hidden' name='PresensiMhswID$no' value='$row->PresensiMhswID'/>
																			";
																			// $Multi = "N";
																			// if(($JamMulai < "12:30:00") AND ($JamSelesai > "12:30:00"))
																			// {
																				// $Multi = "Y";
																			// }
																			// if(($JamMulai < "15:30:00") AND ($JamSelesai > "15:30:00"))
																			// {
																				// $Multi = "Y";
																			// }
																			// if($Multi == "Y")
																			// {
																				// $font1 = "";
																				// $font2 = "";
																				// if($row->JenisPresensiID == "P"){ $font1 = "style='background-color: #C6EC50;'"; }
																				// if($row->JenisPresensiID == "E"){ $font1 = "style='background-color: #D782DB;'"; }
																				// if($row->JenisPresensiID == "S"){ $font1 = "style='background-color: #63B9E0;'"; }
																				// if($row->JenisPresensiID == "A"){ $font1 = "style='background-color: #E83737;'"; }
																				// if($row->JenisPresensiID == "L"){ $font1 = "style='background-color: #FFFF73;'"; }
																				// if($row->JenisPresensiID2 == "P"){ $font2 = "style='background-color: #C6EC50;'"; }
																				// if($row->JenisPresensiID2 == "E"){ $font2 = "style='background-color: #D782DB;'"; }
																				// if($row->JenisPresensiID2 == "S"){ $font2 = "style='background-color: #63B9E0;'"; }
																				// if($row->JenisPresensiID2 == "A"){ $font2 = "style='background-color: #E83737;'"; }
																				// if($row->JenisPresensiID2 == "L"){ $font2 = "style='background-color: #FFFF73;'"; }
																				// echo "<input type='hidden' name='multi$no' value='Y'/>";
																				// echo "<td>
																						// <select $font1 name='setpresensis$no' title='Set Presence 1' class='form-control round-form'>";
																							// if($rowjenis1)
																							// {
																								// foreach($rowjenis1 as $rj1)
																								// {
																									// echo "<option value='$rj1->JenisPresensiID'";
																									// if($cekpresensi == "")
																									// {
																										// if($rj1->JenisPresensiID == $row->JenisPresensiID)
																										// {
																											// echo "selected";
																										// }
																									// }
																									// else
																									// {
																										// if($rj1->JenisPresensiID == $cekpresensi)
																										// {
																											// echo "selected";
																										// }
																									// }
																									// echo ">$rj1->JenisPresensiID - $rj1->Nama</option>";
																								// }
																							// }
																					// echo "</select>
																					// </td>
																					// <td>
																						// <select $font2 name='setpresensid$no' title='Set Presence 2' class='form-control round-form'>";
																							// if($rowjenis2)
																							// {
																								// foreach($rowjenis2 as $rj2)
																								// {
																									// echo "<option value='$rj2->JenisPresensiID'";
																									// if($cekpresensi == "")
																									// {
																										// if($rj2->JenisPresensiID == $row->JenisPresensiID2)
																										// {
																											// echo "selected";
																										// }
																									// }
																									// else
																									// {
																										// if($rj2->JenisPresensiID == $cekpresensi)
																										// {
																											// echo "selected";
																										// }
																									// }
																									// echo ">$rj2->JenisPresensiID - $rj2->Nama</option>";
																								// }
																							// }
																					// echo "</select>
																					// </td>";
																			// }
																			// else
																			// {
																				$font1 = "";
																				if($row->JenisPresensiID == "P"){ $font1 = "style='background-color: #C6EC50;'"; }
																				if($row->JenisPresensiID == "E"){ $font1 = "style='background-color: #D782DB;'"; }
																				if($row->JenisPresensiID == "S"){ $font1 = "style='background-color: #63B9E0;'"; }
																				if($row->JenisPresensiID == "A"){ $font1 = "style='background-color: #E83737;'"; }
																				if($row->JenisPresensiID == "L"){ $font1 = "style='background-color: #FFFF73;'"; }
																				echo "<input type='hidden' name='multi$no' value='N'/>";
																				echo "<td colspan='2'>";
																					if($row->Removed == "Y")
																					{
																						echo "<font color='red'><b>Removed</b></font>";
																					}
																					else
																					{
																						echo "<select $font1 name='setpresensis$no' title='Set Presence 1' class='form-control round-form'>";
																								if($rowjenis1)
																								{
																									foreach($rowjenis1 as $rj)
																									{
																										echo "<option value='$rj->JenisPresensiID'";
																										if($cekpresensi == "")
																										{
																											if($row->JenisPresensiID == $rj->JenisPresensiID)
																											{
																												echo "selected";
																											}
																										}
																										else
																										{
																											if($cekpresensi == $rj->JenisPresensiID)
																											{
																												echo "selected";
																											}
																										}
																										echo ">$rj->JenisPresensiID - $rj->Nama</option>";
																									}
																								}
																						echo "</select>";
																					}
																				echo "
																					<br/>
																					<input type='file' name='gambar$no'>
																					<font color='blue'>If you want to upload some files, please make .rar / .zip file.</font>
																					</td>";
																			// }
																		echo "<input type='hidden' name='KRSID$no' value='$row->KRSID'/>";
																		echo "<input type='hidden' name='MKID$no' value='$MKID'/>";
																		echo "<input type='hidden' name='PresensiID$no' value='$PresensiID'/>";
																		echo "<input type='hidden' name='Removed$no' value='$row->Removed'/>";
																		if($row->Removed == "Y")
																		{
																			$ckd = "checked";
																			echo "<td><a href='".site_url("attendance/ptl_list_attendance_update_active/$row->PresensiMhswID/$PresensiID/$TahunID/$MKID/$row->KRSID")."' class='btn btn-success btn-circle'><i class='fa fa-check'></i></a></td>";
																		}
																		else
																		{
																			$ckd = "";
																			echo "<td><a href='".site_url("attendance/ptl_list_attendance_update/$row->PresensiMhswID/$PresensiID/$TahunID/$MKID/$row->KRSID")."' class='btn btn-warning btn-circle'><i class='fa fa-times'></i></a></td>";
																		}
																		echo "<td>
																				<input type='checkbox' name='cek$no' value='$row->PresensiMhswID' class='kategori-check' $ckd>
																				<input type='hidden' name='cekno$no' value='$row->PresensiMhswID' class='btn'>
																			</td>";
																	}
																	echo "</tr>";
																	$no++;
																	$no2++;
																}
															}
														}
														else
														{
														
															echo "<tr>
																	<td colspan='9'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																</tr>";
														}
													?>
												</tbody>
											</table>
											<center>
												<?php
													if($rowrecord)
													{
														if((!stristr($_COOKIE["akses"],"LECTURER")) AND (($_COOKIE["divisi"] == "ACADEMIC") OR ($_COOKIE["divisi"] == "PRESIDENT")))
														{
															echo "
																<select name='optional' class='form-control' required>
																	<option value='SAVE'>SAVE ATTENDANCE</option>
																	<option VALUE='REMOVE'>REMOVE ATTENDANCE</option>
																	<option VALUE='REMSAVE'>REMOVE AND SAVE ATTENDANCE</option>
																</select>
																<br/>
																";
															echo "<input type='hidden' name='warning' value='$warning'/>";
															echo "<input type='hidden' name='JadwalID' value='$JadwalID'/>";
															echo "<input type='hidden' name='MKID' value='$MKID'/>";
															echo "<input type='hidden' name='NamaDosen' value='$NamaDosen'/>";
															echo "<input type='hidden' name='notifpesan' value='$notifpesan'/>";
															echo "<input type='hidden' name='PresensiID' value='$PresensiID'/>";
															echo "<input type='hidden' name='TahunID' value='$TahunID'/>";
															echo "<input type='hidden' name='JenisMKID' value='$JenisMKID'/>";
															echo "<input type='hidden' name='total' value='$no'/>";
															echo "<input type='submit' value='Save Attendance' id='my_button' class='btn btn-info'>";
														}
														else
														{
															if($_COOKIE["nama"] == $NamaDosen)
															{
																echo "
																	<select name='optional' class='form-control' required>
																		<option value='SAVE'>SAVE ATTENDANCE</option>
																		<option VALUE='REMOVE'>REMOVE ATTENDANCE</option>
																		<option VALUE='REMSAVE'>REMOVE AND SAVE ATTENDANCE</option>
																	</select>
																	<br/>
																	";
																echo "<input type='hidden' name='warning' value='$warning'/>";
																echo "<input type='hidden' name='JadwalID' value='$JadwalID'/>";
																echo "<input type='hidden' name='MKID' value='$MKID'/>";
																echo "<input type='hidden' name='NamaDosen' value='$NamaDosen'/>";
																echo "<input type='hidden' name='notifpesan' value='$notifpesan'/>";
																echo "<input type='hidden' name='PresensiID' value='$PresensiID'/>";
																echo "<input type='hidden' name='TahunID' value='$TahunID'/>";
																echo "<input type='hidden' name='JenisMKID' value='$JenisMKID'/>";
																echo "<input type='hidden' name='total' value='$no'/>";
																echo "<input type='submit' value='Save Attendance' id='my_button' class='btn btn-info'>";
															}
															else
															{
																echo "
																	<center>
																		<font color='red'>
																			<h3>Sorry! You don't have access</h3>
																		</font>
																	</center>
																	";
															}
														}
													}
												?>
											</center>
										</form>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Activity Log
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>