		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Enroll Only This Subject</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<a href="<?php echo site_url("attendance"); ?>">Attendance</a>
							>>
							<a href="<?php echo site_url("attendance/ptl_list/$JadwalID"); ?>">Attendance List</a>
							>>
                            <a href="<?php echo site_url("attendance/ptl_list_enroll/$JadwalID/$SubjekID/$TahunID/$ProgramID/$kls"); ?>">Enroll Only This Subject (ATTEN23)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>It is only to the condition when the enrollment process has been completed and the subject is still not appear.
								</div>
								<form role="form" action="<?php echo site_url("attendance/ptl_enroll"); ?>" method="POST">
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>
													<?php
														$cek = $this->session->userdata('attendance_set_all');
														if($cek == "")
														{
															echo "<a href='".site_url("attendance/ptl_set_check_all/$JadwalID/$SubjekID/$TahunID/$ProgramID/$kls")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
														}
														else
														{
															echo "<a href='".site_url("attendance/ptl_set_uncheck_all/$JadwalID/$SubjekID/$TahunID/$ProgramID/$kls")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
														}
													?>
												</th>
												<th>Student Name & ID</th>
												<th>Program<br/>Program&nbsp;Studi</th>
												<th>Status</th>
												<th>Class</th>
												<th>Specialization</th>
												<th>Language</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$no = 1;
												$totalnotdone = 0;
												if($rowrecord)
												{
													foreach($rowrecord as $row)
													{
														$thn = $row->TahunKe;
														if($ProgramID == "INT")
														{
															$thn = "O";
														}
														$KelasID = $row->KelasID;
														$k = $this->m_kelas->PTL_select_kelas($KelasID);
														$kelas = "";
														if($k)
														{
															$kelas = $k["Nama"];
														}
														$ckd = "xxxx";
														if($kelas != "")
														{
															$ckd = $thn.$kelas;
														}
														if(stristr($kls,$ckd))
														{
															if($row->TahunID == $TahunID)
															{
																if($AddSpesialisasiID == 0)
																{
																	echo "
																		<input type='hidden' name='MhswID$no' value='$row->MhswID'>
																		<input type='hidden' name='ProgramID$no' value='$row->ProgramID'>
																		<input type='hidden' name='ProdiID$no' value='$row->ProdiID'>
																		<input type='hidden' name='TahunID$no' value='$row->TahunID'>
																		<input type='hidden' name='TahunKe$no' value='$row->TahunKe'>
																		<input type='hidden' name='KelasID$no' value='$row->KelasID'>
																		";
																	$MhswID = $row->MhswID;
																	$r = $this->m_mahasiswa->PTL_select($MhswID);
																	if($r)
																	{
																		if($r["Foto"] == "")
																		{
																			$foto = "foto_umum/user.jpg";
																		}
																		else
																		{
																			$foto = "foto_mahasiswa/".$r["Foto"];
																			$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																			if($exist)
																			{
																				$foto = $foto;
																			}
																			else
																			{
																				$foto = "foto_umum/user.jpg";
																			}
																		}
																		$nama = $r["Nama"];
																	}
																	else
																	{
																		$foto = "foto_umum/user.jpg";
																		$nama = "";
																	}
																	$ProdiID = $row->ProdiID;
																	if($row->ProgramID == "SC")
																	{
																		$KursusSingkatID = $ProdiID;
																		$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
																	}
																	else
																	{
																		$p = $this->m_prodi->PTL_select($ProdiID);
																	}
																	$StatusMhswID = $row->StatusMhswID;
																	$s = $this->m_status->PTL_select($StatusMhswID);
																	$KHSID = $row->KHSID;
																	$reskrs = $this->m_krs->PTL_select_subject($MhswID,$KHSID,$SubjekID);
																	if($reskrs)
																	{
																		if($reskrs['NA'] == "N")
																		{
																			$cls = "class='success'";
																			$krs = "<font color='green'><b>DONE</b></font>
																					<input type='hidden' name='check$no' value=''/>";
																		}
																		else
																		{
																			$cls = "class='warning'";
																			$krs = "<font color='red'><b>DROP</b></font>
																					<input type='hidden' name='check$no' value=''/>";
																		}
																	}
																	else
																	{
																		$totalnotdone++;
																		$cls = "class='danger'";
																		if($cek == "")
																		{
																			$krs = "<input type='checkbox' name='check$no' value='$KHSID'/>";
																		}
																		else
																		{
																			$krs = "<input type='checkbox' name='check$no' value='$KHSID' checked/>";
																		}
																	}
																	$resstatus = $this->m_status->PTL_select($StatusMhswID);
																	if($resstatus["StatusMhswID"] == "A")
																	{
																		$status = "<font color='green'><b>$resstatus[Nama]</b></font>";
																	}
																	else
																	{
																		$status = "<font color='red'><b>$resstatus[Nama]</b></font>";
																	}
																	$SpesialisasiID = $row->SpesialisasiID;
																	$spec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																	$spesialisasi = "";
																	if($spec)
																	{
																		$spesialisasi = $spec["Nama"];
																	}
																	echo "<tr $cls>
																			<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$no</td>
																			<td>
																				$krs
																				&nbsp;&nbsp;
																				<img src='".base_url("ptl_storage/$foto")."' width='50px'/>
																			</td>
																			<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("student/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)</td>
																			<td>$row->ProgramID<br/>$p[Nama]</td>
																			<td>$s[Nama]</td>
																			<td>$thn$kelas</td>
																			<td title='SpesialisasiID: $SpesialisasiID'>$spesialisasi</td>
																			<td>$row->languageID</td>
																		</tr>";
																	$no++;
																}
																else
																{
																	if($AddSpesialisasiID == $row->SpesialisasiID)
																	{
																		echo "
																			<input type='hidden' name='MhswID$no' value='$row->MhswID'>
																			<input type='hidden' name='ProgramID$no' value='$row->ProgramID'>
																			<input type='hidden' name='ProdiID$no' value='$row->ProdiID'>
																			<input type='hidden' name='TahunID$no' value='$row->TahunID'>
																			<input type='hidden' name='TahunKe$no' value='$row->TahunKe'>
																			<input type='hidden' name='KelasID$no' value='$row->KelasID'>
																			";
																		$MhswID = $row->MhswID;
																		$r = $this->m_mahasiswa->PTL_select($MhswID);
																		if($r)
																		{
																			if($r["Foto"] == "")
																			{
																				$foto = "foto_umum/user.jpg";
																			}
																			else
																			{
																				$foto = "foto_mahasiswa/".$r["Foto"];
																				$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																				if($exist)
																				{
																					$foto = $foto;
																				}
																				else
																				{
																					$foto = "foto_umum/user.jpg";
																				}
																			}
																			$nama = $r["Nama"];
																		}
																		else
																		{
																			$foto = "foto_umum/user.jpg";
																			$nama = "";
																		}
																		$ProdiID = $row->ProdiID;
																		if($row->ProgramID == "SC")
																		{
																			$KursusSingkatID = $ProdiID;
																			$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
																		}
																		else
																		{
																			$p = $this->m_prodi->PTL_select($ProdiID);
																		}
																		$StatusMhswID = $row->StatusMhswID;
																		$s = $this->m_status->PTL_select($StatusMhswID);
																		$KHSID = $row->KHSID;
																		$reskrs = $this->m_krs->PTL_select_subject($MhswID,$KHSID,$SubjekID);
																		if($reskrs)
																		{
																			if($reskrs['NA'] == "N")
																			{
																				$cls = "class='success'";
																				$krs = "<font color='green'><b>DONE</b></font>
																						<input type='hidden' name='check$no' value=''/>";
																			}
																			else
																			{
																				$cls = "class='warning'";
																				$krs = "<font color='red'><b>DROP</b></font>
																						<input type='hidden' name='check$no' value=''/>";
																			}
																		}
																		else
																		{
																			$totalnotdone++;
																			$cls = "class='danger'";
																			if($cek == "")
																			{
																				$krs = "<input type='checkbox' name='check$no' value='$KHSID'/>";
																			}
																			else
																			{
																				$krs = "<input type='checkbox' name='check$no' value='$KHSID' checked/>";
																			}
																		}
																		$resstatus = $this->m_status->PTL_select($StatusMhswID);
																		if($resstatus["StatusMhswID"] == "A")
																		{
																			$status = "<font color='green'><b>$resstatus[Nama]</b></font>";
																		}
																		else
																		{
																			$status = "<font color='red'><b>$resstatus[Nama]</b></font>";
																		}
																		$SpesialisasiID = $row->SpesialisasiID;
																		$spec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																		$spesialisasi = "";
																		if($spec)
																		{
																			$spesialisasi = $spec["Nama"];
																		}
																		echo "<tr $cls>
																				<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$no</td>
																				<td>
																					$krs
																					&nbsp;&nbsp;
																					<img src='".base_url("ptl_storage/$foto")."' width='50px'/>
																				</td>
																				<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("student/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)</td>
																				<td>$row->ProgramID<br/>$p[Nama]</td>
																				<td>$s[Nama]</td>
																				<td>$thn$kelas</td>
																				<td title='SpesialisasiID: $SpesialisasiID'>$spesialisasi</td>
																				<td>$row->languageID</td>
																			</tr>";
																		$no++;
																	}
																}
															}
														}
													}
												}
												if($no == 1)
												{
													echo "<tr>
															<td colspan='7'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
														</tr>";
												}
											?>
										</tbody>
									</table>
									<center>
										<?php
											if($rowrecord)
											{
												echo "
													<input type='hidden' name='total' value='$no'>
													<input type='hidden' name='JadwalID' value='$JadwalID'>
													<input type='hidden' name='SubjekID' value='$SubjekID'>
													<input type='hidden' name='TahunID' value='$TahunID'>
													<input type='hidden' name='ProgramID' value='$ProgramID'>
													<input type='hidden' name='kls' value='$kls'>
													";
												if(($no > 1) AND ($totalnotdone > 0))
												{
													echo "<input type='submit' value='Enroll' id='my_button' class='btn btn-primary'>";
												}
											}
										?>
									</center>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>