		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("attendance"); ?>">Attendance (ATTEN02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the sixth step to set topic after you create schedule and set attendance. <a href="<?php echo site_url("tutorial/ptl_detail/ATTD001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("attendance/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('att_filter_jur');
														$add = "";
														if($cekjurusan == "INT")
														{
															$add = "O";
														}
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("attendance/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('att_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if($cektahun != "")
											{
										?>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("attendance/ptl_filter_subjek"); ?>" method="POST">
														<select name="ceksubjek" title="Filter by Subject" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- SUBJECT --</option>
															<?php
																$ceksubjek = $this->session->userdata('att_filter_subjek');
																if($cekjurusan != "")
																{
																	if($rowsubjek)
																	{
																		foreach($rowsubjek as $sub)
																		{
																			echo "<option value='$sub->subjek'";
																			if($ceksubjek == $sub->subjek)
																			{
																				echo "selected";
																			}
																			echo ">$sub->nama</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("attendance/ptl_filter_dosen"); ?>" method="POST">
														<select name="cekdosen" title="Filter by Lecturer" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- TEACHERS --</option>
															<?php
																$cekdosen = $this->session->userdata('att_filter_dosen');
																if($cekjurusan != "")
																{
																	if($rowdosen)
																	{
																		foreach($rowdosen as $dsn)
																		{
																			if($dsn->NA == "N")
																			{
																				echo "<option value='$dsn->Login'";
																				if($cekdosen == $dsn->Login)
																				{
																					echo "selected";
																				}
																				echo ">$dsn->Nama</option>";
																			}
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("attendance/ptl_filter_kelas"); ?>" method="POST">
														<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
															<option value='_'>-- CLASS --</option>
															<?php
																if($cekjurusan == "REG")
																{
																	$init = "";
																}
																if($cekjurusan == "INT")
																{
																	$init = "O";
																}
																$ni = "";
																$cekkelas = $this->session->userdata('att_filter_kelas');
																if($cekjurusan != "")
																{
																	if($rowkelas)
																	{
																		for($i=1;$i<=3;$i++)
																		{
																			foreach($rowkelas as $kls)
																			{
																				if($init == "")
																				{
																					$ni = $i;
																				}
																				else
																				{
																					$ni = "";
																				}
																				if($kls->ProgramID == $cekjurusan)
																				{
																					echo "<option value='".$i."_".$kls->KelasID."'";
																					if($cekkelas == $i."_".$kls->KelasID)
																					{
																						echo "selected";
																					}
																					echo ">$init$ni$kls->Nama</option>";
																				}
																			}
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
										<?php
											}
										?>
									</tr>
								</table>
								<?php
									if(($cekjurusan != "") AND ($cektahun != ""))
									{
								?>
										<br/>
										<center><a href="<?php echo site_url("attendance/ptl_clashed"); ?>" class="btn btn-danger">Clashed</a></center>
										<table class="table">
											<thead>
												<tr>
													<th>#</th>
													<th>ID</th>
													<th>Subject</th>
													<th>Prodi</th>
													<th>Class</th>
													<th>Teacher</th>
													<th>Attendance</th>
													<th>Recap</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														$sebelumnya = "";
														foreach($rowrecord as $row)
														{
															if($cekdosen != "")
															{
																if(($cekdosen == $row->DosenID) OR ($cekdosen == $row->DosenID2) OR ($cekdosen == $row->DosenID3) OR ($cekdosen == $row->DosenID4) OR ($cekdosen == $row->DosenID5) OR ($cekdosen == $row->DosenID6))
																{
																	$SubjekID = $row->SubjekID;
																	$s = $this->m_subjek->PTL_select($SubjekID);
																	$fs1 = "";
																	$fs2 = "";
																	if($s["NA"] == "Y")
																	{
																		$fs1 = "<font color='red'><b title='This subject unavailable'>";
																		$fs2 = "</b></font>";
																	}
																	$KelasID = $row->KelasID;
																	if($KelasID == 0)
																	{
																		if($row->KelasIDGabungan != "")
																		{
																			$kelas = "";
																			$link_kelas = "";
																			$KelasIDGabungan = $row->KelasIDGabungan;
																			$wordgab = explode(".",$KelasIDGabungan);
																			for($i=0;$i<30;$i++)
																			{
																				$wg = explode("^",@$wordgab[$i]);
																				$KelasID = @$wg[1];
																				$res = $this->m_kelas->PTL_select_kelas($KelasID);
																				if($res)
																				{
																					$kls = $res["Nama"];
																				}
																				else
																				{
																					$kls = "";
																				}
																				if($i == 0)
																				{
																					$kelas .= @$wg[0].$kls;
																					$link_kelas .= @$wg[0].$kls;
																				}
																				else
																				{
																					if($wg[0] != "")
																					{
																						$kelas .= "/".@$wg[0].$kls;
																						$link_kelas .= "/".@$wg[0].$kls;
																					}
																				}
																			}
																			if($kelas == "/")
																			{
																				$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																				$link_kelas = "";
																			}
																		}
																		else
																		{
																			$k = $this->m_kelas->PTL_select_kelas($KelasID);
																			if($k)
																			{
																				if($add == "")
																				{
																					$kelas = $row->TahunKe.$k["Nama"];
																				}
																				else
																				{
																					$kelas = $add.$k["Nama"];
																				}
																				$link_kelas = $row->TahunKe.$k["Nama"];
																			}
																			else
																			{
																				$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																				$link_kelas = "";
																			}
																		}
																	}
																	else
																	{
																		$k = $this->m_kelas->PTL_select_kelas($KelasID);
																		if($add == "")
																		{
																			$kelas = $row->TahunKe.$k["Nama"];
																		}
																		else
																		{
																			$kelas = $add.$k["Nama"];
																		}
																		$link_kelas = $row->TahunKe.$k["Nama"];
																	}
																	$DosenID = $row->DosenID;
																	$d = $this->m_dosen->PTL_select($DosenID);
																	$f1 = "";
																	$f2 = "";
																	$dosen = "";
																	if($d)
																	{
																		if($d["NA"] == "Y")
																		{
																			$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																			$f2 = "</b></font>";
																		}
																		$dosen = $d["Nama"];
																	}
																	$DosenID = $row->DosenID2;
																	$d2 = $this->m_dosen->PTL_select($DosenID);
																	$dosen2 = "";
																	if($d2)
																	{
																		$dosen2 = $d2["Nama"];
																	}
																	$DosenID = $row->DosenID3;
																	$d3 = $this->m_dosen->PTL_select($DosenID);
																	$dosen3 = "";
																	if($d3)
																	{
																		$dosen3 = $d3["Nama"];
																	}
																	$DosenID = $row->DosenID4;
																	$d4 = $this->m_dosen->PTL_select($DosenID);
																	$dosen4 = "";
																	if($d4)
																	{
																		$dosen4 = $d4["Nama"];
																	}
																	$DosenID = $row->DosenID5;
																	$d5 = $this->m_dosen->PTL_select($DosenID);
																	$dosen5 = "";
																	if($d5)
																	{
																		$dosen5 = $d5["Nama"];
																	}
																	$DosenID = $row->DosenID6;
																	$d6 = $this->m_dosen->PTL_select($DosenID);
																	$dosen6 = "";
																	if($d6)
																	{
																		$dosen6 = $d6["Nama"];
																	}
																	if($sebelumnya != $row->SubjekID)
																	{
																		$SubjekID = $row->SubjekID;
																		$rows = $this->m_jadwal->PTL_all_spesifik_scoring_total($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID);
																		$rowspan = count($rows);
																		$subjek = "<td style='vertical-align:middle;' rowspan='$rowspan' title='SubjekID : $row->SubjekID'><h4>$fs1".$s["Nama"]."$fs2</h4></td>";
																		$sebelumnya = $row->SubjekID;
																	}
																	else
																	{
																		$sebelumnya = $sebelumnya;
																		$subjek = "";
																		$rowspan = "";
																	}
																	if($dosen2 != "")
																	{
																		$f1 = "<font color='blue'><b>";
																		$f2 = "</b></font>";
																		$dosen = "Multiple Lecturer";
																	}
																	echo "<tr class='info'>
																			<td>$no</td>
																			<td>$row->JadwalID</td>
																			$subjek
																			<td>$row->ProdiID</td>
																			<td>$kelas</td>
																			<td title='Assistant : $dosen2 - $dosen3 - $dosen4 - $dosen5 - $dosen6'>$f1$dosen$f2</td>
																			<td>
																				<a href='".site_url("attendance/ptl_list/$row->JadwalID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>";
												?>
																				<a href="<?php echo site_url("attendance/ptl_delete/$row->JadwalID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																		echo "</td>
																			<td>
																				<a href='".site_url("attendance/ptl_pdf_recap/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-success'><i class='fa fa-print'></i></a>
																				<a href='".site_url("attendance/ptl_pdf_recap_dpm/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-info'><i class='fa fa-print'></i></a>
																			</td>
																		</tr>";
																	$no++;
																}
															}
															else
															{
																$SubjekID = $row->SubjekID;
																$s = $this->m_subjek->PTL_select($SubjekID);
																$fs1 = "";
																$fs2 = "";
																if($s["NA"] == "Y")
																{
																	$fs1 = "<font color='red'><b title='This subject unavailable'>";
																	$fs2 = "</b></font>";
																}
																$KelasID = $row->KelasID;
																if($KelasID == 0)
																{
																	if($row->KelasIDGabungan != "")
																	{
																		$kelas = "";
																		$link_kelas = "";
																		$KelasIDGabungan = $row->KelasIDGabungan;
																		$wordgab = explode(".",$KelasIDGabungan);
																		for($i=0;$i<30;$i++)
																		{
																			$wg = explode("^",@$wordgab[$i]);
																			$KelasID = @$wg[1];
																			$res = $this->m_kelas->PTL_select_kelas($KelasID);
																			if($res)
																			{
																				$kls = $res["Nama"];
																			}
																			else
																			{
																				$kls = "";
																			}
																			if($i == 0)
																			{
																				$kelas .= @$wg[0].$kls;
																				$link_kelas .= @$wg[0].$kls;
																			}
																			else
																			{
																				if($wg[0] != "")
																				{
																					$kelas .= "/".@$wg[0].$kls;
																					$link_kelas .= "/a".@$wg[0].$kls;
																				}
																			}
																		}
																		if($kelas == "/")
																		{
																			$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																			$link_kelas = "";
																		}
																	}
																	else
																	{
																		$k = $this->m_kelas->PTL_select_kelas($KelasID);
																		if($k)
																		{
																			if($add == "")
																			{
																				$kelas = $row->TahunKe.$k["Nama"];
																			}
																			else
																			{
																				$kelas = $add.$k["Nama"];
																			}
																			$link_kelas = $row->TahunKe.$k["Nama"];
																		}
																		else
																		{
																			$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																			$link_kelas = "";
																		}
																	}
																}
																else
																{
																	$k = $this->m_kelas->PTL_select_kelas($KelasID);
																	if($add == "")
																	{
																		$kelas = $row->TahunKe.$k["Nama"];
																	}
																	else
																	{
																		$kelas = $add.$k["Nama"];
																	}
																	$link_kelas = $row->TahunKe.$k["Nama"];
																}
																$DosenID = $row->DosenID;
																$d = $this->m_dosen->PTL_select($DosenID);
																$f1 = "";
																$f2 = "";
																$dosen = "";
																if($d)
																{
																	if($d["NA"] == "Y")
																	{
																		$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																		$f2 = "</b></font>";
																	}
																	$dosen = $d["Nama"];
																}
																$DosenID = $row->DosenID2;
																$d2 = $this->m_dosen->PTL_select($DosenID);
																$dosen2 = "";
																if($d2)
																{
																	$dosen2 = $d2["Nama"];
																}
																$DosenID = $row->DosenID3;
																$d3 = $this->m_dosen->PTL_select($DosenID);
																$dosen3 = "";
																if($d3)
																{
																	$dosen3 = $d3["Nama"];
																}
																$DosenID = $row->DosenID4;
																$d4 = $this->m_dosen->PTL_select($DosenID);
																$dosen4 = "";
																if($d4)
																{
																	$dosen4 = $d4["Nama"];
																}
																$DosenID = $row->DosenID5;
																$d5 = $this->m_dosen->PTL_select($DosenID);
																$dosen5 = "";
																if($d5)
																{
																	$dosen5 = $d5["Nama"];
																}
																$DosenID = $row->DosenID6;
																$d6 = $this->m_dosen->PTL_select($DosenID);
																$dosen6 = "";
																if($d6)
																{
																	$dosen6 = $d6["Nama"];
																}
																if($sebelumnya != $row->SubjekID)
																{
																	$SubjekID = $row->SubjekID;
																	$rows = $this->m_jadwal->PTL_all_spesifik_scoring_total($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID);
																	$rowspan = count($rows);
																	$subjek = "<td style='vertical-align:middle;' rowspan='$rowspan' title='SubjekID : $row->SubjekID'><h4>$fs1".$s["Nama"]."$fs2</h4></td>";
																	$sebelumnya = $row->SubjekID;
																}
																else
																{
																	$sebelumnya = $sebelumnya;
																	$subjek = "";
																	$rowspan = "";
																}
																if($dosen2 != "")
																{
																	$f1 = "<font color='blue'><b>";
																	$f2 = "</b></font>";
																	$dosen = "Multiple Lecturer";
																}
																echo "<tr class='info'>
																		<td>$no</td>
																		<td>$row->JadwalID</td>
																		$subjek
																		<td>$row->ProdiID</td>
																		<td>$kelas</td>
																		<td title='Assistant : $dosen2 - $dosen3 - $dosen4 - $dosen5 - $dosen6'>$f1$dosen$f2</td>
																		<td>
																			<a href='".site_url("attendance/ptl_list/$row->JadwalID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>";
												?>
																			<a href="<?php echo site_url("attendance/ptl_delete/$row->JadwalID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																	echo "</td>
																		<td>
																			<a href='".site_url("attendance/ptl_pdf_recap/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-success'><i class='fa fa-print'></i></a>
																			<a href='".site_url("attendance/ptl_pdf_recap_dpm/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-info'><i class='fa fa-print'></i></a>
																		</td>
																	</tr>";
																$no++;
															}
														}
														$cekkelas = $this->session->userdata('att_filter_kelas');
														$ck = "_";
														if($cekkelas != "")
														{
															$ck = $cekkelas;
														}
														$word = explode("_",$ck);
														$Tahun = $word[0];
														$Kelas = $word[1];
														if($cekkelas != "")
														{
															if($rowrecord2)
															{
																$sebelumnya = "";
																foreach($rowrecord2 as $row)
																{
																	if($cekdosen != "")
																	{
																		if(($cekdosen == $row->DosenID) OR ($cekdosen == $row->DosenID2) OR ($cekdosen == $row->DosenID3) OR ($cekdosen == $row->DosenID4) OR ($cekdosen == $row->DosenID5) OR ($cekdosen == $row->DosenID6))
																		{
																			$SubjekID = $row->SubjekID;
																			$s = $this->m_subjek->PTL_select($SubjekID);
																			$fs1 = "";
																			$fs2 = "";
																			if($s["NA"] == "Y")
																			{
																				$fs1 = "<font color='red'><b title='This subject unavailable'>";
																				$fs2 = "</b></font>";
																			}
																			$KelasID = $row->KelasID;
																			if($KelasID == 0)
																			{
																				if($row->KelasIDGabungan != "")
																				{
																					$kelas = "";
																					$link_kelas = "";
																					$KelasIDGabungan = $row->KelasIDGabungan;
																					$wordgab = explode(".",$KelasIDGabungan);
																					for($i=0;$i<30;$i++)
																					{
																						$wg = explode("^",@$wordgab[$i]);
																						$KelasID = @$wg[1];
																						$res = $this->m_kelas->PTL_select_kelas($KelasID);
																						if($res)
																						{
																							$kls = $res["Nama"];
																						}
																						else
																						{
																							$kls = "";
																						}
																						if($i == 0)
																						{
																							$kelas .= @$wg[0].$kls;
																							$link_kelas .= @$wg[0].$kls;
																						}
																						else
																						{
																							if($wg[0] != "")
																							{
																								$kelas .= "/".@$wg[0].$kls;
																								$link_kelas .= "/".@$wg[0].$kls;
																							}
																						}
																					}
																					if($kelas == "/")
																					{
																						$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																						$link_kelas = "";
																					}
																				}
																				else
																				{
																					$k = $this->m_kelas->PTL_select_kelas($KelasID);
																					if($k)
																					{
																						if($add == "")
																						{
																							$kelas = $row->TahunKe.$k["Nama"];
																						}
																						else
																						{
																							$kelas = $add.$k["Nama"];
																						}
																						$link_kelas = $row->TahunKe.$k["Nama"];
																					}
																					else
																					{
																						$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																						$link_kelas = "";
																					}
																				}
																			}
																			else
																			{
																				$k = $this->m_kelas->PTL_select_kelas($KelasID);
																				if($add == "")
																				{
																					$kelas = $row->TahunKe.$k["Nama"];
																				}
																				else
																				{
																					$kelas = $add.$k["Nama"];
																				}
																				$link_kelas = $row->TahunKe.$k["Nama"];
																			}
																			$DosenID = $row->DosenID;
																			$d = $this->m_dosen->PTL_select($DosenID);
																			$f1 = "";
																			$f2 = "";
																			$dosen = "";
																			if($d)
																			{
																				if($d["NA"] == "Y")
																				{
																					$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																					$f2 = "</b></font>";
																				}
																				$dosen = $d["Nama"];
																			}
																			$DosenID = $row->DosenID2;
																			$d2 = $this->m_dosen->PTL_select($DosenID);
																			$dosen2 = "";
																			if($d2)
																			{
																				$dosen2 = $d2["Nama"];
																			}
																			$DosenID = $row->DosenID3;
																			$d3 = $this->m_dosen->PTL_select($DosenID);
																			$dosen3 = "";
																			if($d3)
																			{
																				$dosen3 = $d3["Nama"];
																			}
																			$DosenID = $row->DosenID4;
																			$d4 = $this->m_dosen->PTL_select($DosenID);
																			$dosen4 = "";
																			if($d4)
																			{
																				$dosen4 = $d4["Nama"];
																			}
																			$DosenID = $row->DosenID5;
																			$d5 = $this->m_dosen->PTL_select($DosenID);
																			$dosen5 = "";
																			if($d5)
																			{
																				$dosen5 = $d5["Nama"];
																			}
																			$DosenID = $row->DosenID6;
																			$d6 = $this->m_dosen->PTL_select($DosenID);
																			$dosen6 = "";
																			if($d6)
																			{
																				$dosen6 = $d6["Nama"];
																			}
																			if($sebelumnya != $row->SubjekID)
																			{
																				$SubjekID = $row->SubjekID;
																				$rows = $this->m_jadwal->PTL_all_spesifik_scoring_total($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID);
																				$rowspan = count($rows);
																				$subjek = "<td style='vertical-align:middle;' rowspan='$rowspan' title='SubjekID : $row->SubjekID'><h4>$fs1".$s["Nama"]."$fs2</h4></td>";
																				$sebelumnya = $row->SubjekID;
																			}
																			else
																			{
																				$sebelumnya = $sebelumnya;
																				$subjek = "";
																				$rowspan = "";
																			}
																			if($dosen2 != "")
																			{
																				$f1 = "<font color='blue'><b>";
																				$f2 = "</b></font>";
																				$dosen = "Multiple Lecturer";
																			}
																			echo "<tr class='info'>
																					<td>$no</td>
																					<td>$row->JadwalID</td>
																					$subjek
																					<td>$row->ProdiID</td>
																					<td>$kelas</td>
																					<td title='Assistant : $dosen2 - $dosen3 - $dosen4 - $dosen5 - $dosen6'>$f1$dosen$f2</td>
																					<td>
																						<a href='".site_url("attendance/ptl_list/$row->JadwalID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>";
												?>
																						<a href="<?php echo site_url("attendance/ptl_delete/$row->JadwalID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																				echo "</td>
																					<td>
																						<a href='".site_url("attendance/ptl_pdf_recap/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-success'><i class='fa fa-print'></i></a>
																						<a href='".site_url("attendance/ptl_pdf_recap_dpm/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-info'><i class='fa fa-print'></i></a>
																					</td>
																				</tr>";
																			$no++;
																		}
																	}
																	else
																	{
																		$SubjekID = $row->SubjekID;
																		$s = $this->m_subjek->PTL_select($SubjekID);
																		$fs1 = "";
																		$fs2 = "";
																		if($s["NA"] == "Y")
																		{
																			$fs1 = "<font color='red'><b title='This subject unavailable'>";
																			$fs2 = "</b></font>";
																		}
																		$KelasID = $row->KelasID;
																		if($KelasID == 0)
																		{
																			if($row->KelasIDGabungan != "")
																			{
																				$kelas = "";
																				$link_kelas = "";
																				$KelasIDGabungan = $row->KelasIDGabungan;
																				$wordgab = explode(".",$KelasIDGabungan);
																				for($i=0;$i<30;$i++)
																				{
																					$wg = explode("^",@$wordgab[$i]);
																					$KelasID = @$wg[1];
																					$res = $this->m_kelas->PTL_select_kelas($KelasID);
																					if($res)
																					{
																						$kls = $res["Nama"];
																					}
																					else
																					{
																						$kls = "";
																					}
																					if($i == 0)
																					{
																						$kelas .= @$wg[0].$kls;
																						$link_kelas .= @$wg[0].$kls;
																					}
																					else
																					{
																						if($wg[0] != "")
																						{
																							$kelas .= "/".@$wg[0].$kls;
																							$link_kelas .= "/a".@$wg[0].$kls;
																						}
																					}
																				}
																				if($kelas == "/")
																				{
																					$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																					$link_kelas = "";
																				}
																			}
																			else
																			{
																				$k = $this->m_kelas->PTL_select_kelas($KelasID);
																				if($k)
																				{
																					if($add == "")
																					{
																						$kelas = $row->TahunKe.$k["Nama"];
																					}
																					else
																					{
																						$kelas = $add.$k["Nama"];
																					}
																					$link_kelas = $row->TahunKe.$k["Nama"];
																				}
																				else
																				{
																					$kelas = "<font color='red'><b title='This class not set'>Not&nbsp;set</b></font>";
																					$link_kelas = "";
																				}
																			}
																		}
																		else
																		{
																			$k = $this->m_kelas->PTL_select_kelas($KelasID);
																			if($add == "")
																			{
																				$kelas = $row->TahunKe.$k["Nama"];
																			}
																			else
																			{
																				$kelas = $add.$k["Nama"];
																			}
																			$link_kelas = $row->TahunKe.$k["Nama"];
																		}
																		$DosenID = $row->DosenID;
																		$d = $this->m_dosen->PTL_select($DosenID);
																		$f1 = "";
																		$f2 = "";
																		$dosen = "";
																		if($d)
																		{
																			if($d["NA"] == "Y")
																			{
																				$f1 = "<font color='red'><b title='This lecturer has resigned'>";
																				$f2 = "</b></font>";
																			}
																			$dosen = $d["Nama"];
																		}
																		$DosenID = $row->DosenID2;
																		$d2 = $this->m_dosen->PTL_select($DosenID);
																		$dosen2 = "";
																		if($d2)
																		{
																			$dosen2 = $d2["Nama"];
																		}
																		$DosenID = $row->DosenID3;
																		$d3 = $this->m_dosen->PTL_select($DosenID);
																		$dosen3 = "";
																		if($d3)
																		{
																			$dosen3 = $d3["Nama"];
																		}
																		$DosenID = $row->DosenID4;
																		$d4 = $this->m_dosen->PTL_select($DosenID);
																		$dosen4 = "";
																		if($d4)
																		{
																			$dosen4 = $d4["Nama"];
																		}
																		$DosenID = $row->DosenID5;
																		$d5 = $this->m_dosen->PTL_select($DosenID);
																		$dosen5 = "";
																		if($d5)
																		{
																			$dosen5 = $d5["Nama"];
																		}
																		$DosenID = $row->DosenID6;
																		$d6 = $this->m_dosen->PTL_select($DosenID);
																		$dosen6 = "";
																		if($d6)
																		{
																			$dosen6 = $d6["Nama"];
																		}
																		if($dosen2 != "")
																		{
																			$f1 = "<font color='blue'><b>";
																			$f2 = "</b></font>";
																			$dosen = "Multiple Lecturer";
																		}
																		echo "<tr class='info'>
																				<td>$no</td>
																				<td>$row->JadwalID</td>
																				<td title='SubjekID : $row->SubjekID'><h4>$fs1".$s["Nama"]."$fs2</h4></td>
																				<td>$row->ProdiID</td>
																				<td>$kelas</td>
																				<td title='Assistant : $dosen2 - $dosen3 - $dosen4 - $dosen5 - $dosen6'>$f1$dosen$f2</td>
																				<td>
																					<a href='".site_url("attendance/ptl_list/$row->JadwalID")."' class='btn btn-primary'><i class='fa fa-list'></i></a>";
												?>
																					<a href="<?php echo site_url("attendance/ptl_delete/$row->JadwalID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->JadwalID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																			echo "</td>
																				<td>
																					<a href='".site_url("attendance/ptl_pdf_recap/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-success'><i class='fa fa-print'></i></a>
																					<a href='".site_url("attendance/ptl_pdf_recap_dpm/$row->JadwalID/$row->SubjekID/$row->TahunID/$link_kelas")."' class='btn btn-info'><i class='fa fa-print'></i></a>
																				</td>
																			</tr>";
																		$no++;
																	}
																}
															}
														}
													}
													else
													{
														echo "<tr>
																<td colspan='8'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>