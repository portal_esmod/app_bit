			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Transcript</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("transcript"); ?>">Student Transcript (TRANS02)</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>This page is used to view the student transcript. <a href="<?php echo site_url("tutorial/ptl_detail/TRSC001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Transcript Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<?php
									$mhsw = $this->session->userdata('eval_filter_mahasiswa');
								?>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("transcript/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('eval_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("transcript/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('eval_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("transcript/ptl_filter_status"); ?>" method="POST">
												<select name="cekstatus" title="Filter by Status" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- STATUS --</option>
													<?php
														$cekstatus = $this->session->userdata('eval_filter_status');
														if($status)
														{
															foreach($status as $row)
															{
																echo "<option value='$row->StatusMhswID'";
																if($cekstatus == $row->StatusMhswID)
																{
																	echo "selected";
																}
																echo ">$row->StatusMhswID - $row->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<form action="<?php echo site_url("transcript/ptl_filter_semester"); ?>" method="POST">
												<select name="ceksemester" title="Filter by Semester" class="form-control" onchange="this.form.submit()">
													<option value=''>-- SEMESTER --</option>
													<?php
														$ceksemester = $this->session->userdata('eval_filter_semester');
														echo "<option value='1'"; if($ceksemester == '1'){ echo "selected"; } echo ">Semester 1</option>";
														echo "<option value='2'"; if($ceksemester == '2'){ echo "selected"; } echo ">Semester 2</option>";
														echo "<option value='3'"; if($ceksemester == '3'){ echo "selected"; } echo ">Semester 3</option>";
														echo "<option value='4'"; if($ceksemester == '4'){ echo "selected"; } echo ">Semester 4</option>";
														echo "<option value='5'"; if($ceksemester == '5'){ echo "selected"; } echo ">Semester 5</option>";
														echo "<option value='6'"; if($ceksemester == '6'){ echo "selected"; } echo ">Semester 6</option>";
														$resmax = $this->m_khs->PTL_select_drop($MhswID);
														$TahunID = $resmax['tahun'];
														$result2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
														$Sesi = "";
														if($result2)
														{
															$Sesi = $result2['Sesi'];
														}
														if($ceksemester != "")
														{
															$FilterSemester = $ceksemester;
														}
														else
														{
															$FilterSemester = $Sesi;
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<form role="form" id="student" action="<?php echo site_url("transcript/ptl_cari"); ?>" method="POST">
											<td colspan="3">
												<input type="text" name="cari" value="<?php echo $mhsw; ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
										</form>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													if($r["Foto"] == "")
													{
														$foto = "foto_umum/user.jpg";
													}
													else
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
														}
														else
														{
															$foto = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="6"><p align="center"><img src="<?php echo base_url("ptl_storage/$foto"); ?>" width="200px"/></p></td>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td>
													<?php
														$tgl = tgl_singkat_eng($r["TanggalLahir"]);
														if($r["TanggalLahir"] == "NULL")
														{
															$tgl = "";
														}
														echo $r["TempatLahir"].", ".$tgl;
													?>
												</td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td>
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td>
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td>
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
														if($StatusMhswID == "L")
														{
															if($r["tanggal_lulus"] != "")
															{
																echo " - ".tgl_singkat_eng($r["tanggal_lulus"]);
															}
														}
													?>
												</td>
											</tr>
											<tr>
												<td>Class</td>
												<td>:</td>
												<td>
													<?php
														$reskhs = $this->m_khs->PTL_all_select($MhswID);
														$kelas1 = "";
														$kelas2 = "";
														$kelas3 = "";
														$spec = "";
														if($reskhs)
														{
															foreach($reskhs as $rk)
															{
																$KelasID = $rk->KelasID;
																$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
																if($reskelas)
																{
																	if($rk->TahunKe == 1)
																	{
																		if($rk->ProgramID == "INT")
																		{
																			$kelas1 = "O".$reskelas["Nama"];
																		}
																		else
																		{
																			$kelas1 = $rk->TahunKe.$reskelas["Nama"];
																		}
																	}
																	if($rk->TahunKe == 2)
																	{
																		$kelas2 = " > ".$rk->TahunKe.$reskelas["Nama"];
																	}
																	if($rk->TahunKe == 3)
																	{
																		$kelas3 = " > ".$rk->TahunKe.$reskelas["Nama"];
																	}
																}
															}
														}
														$kelas = $kelas1.$kelas2.$kelas3;
														echo $kelas;
													?>
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<center>
									<a href="<?php echo site_url("evaluation"); ?>" class="btn btn-info">Go To Evaluation</a>
									<br/>
									<br/>
								</center>
								<table class="table table-striped table-bordered table-hover">
									<tbody>
										<tr>
											<?php
												if($mhsw != "")
												{
													echo "
														<tr>
															<th rowspan='2' align='center'><p align='center'>#</p></th>
															<th rowspan='2'><p align='center'>Subject</p></th>
															<th rowspan='2'><p align='center'>Score</p></th>
															<th colspan='4'><p align='center'>Attendance</p></th>
															<th rowspan='2'><p align='center'>Minus/Plus</p></th>
															<th rowspan='2'><p align='center'>Grade Value</p></th>
															<th rowspan='2'><p align='center'>Exam Score</p></th>
															<th rowspan='2'><p align='center'>Remedial Score</p></th>
															<th rowspan='2'><p align='center'>Grade</p></th>
															<th rowspan='2'><p align='center'>Credit</p></th>
															<th rowspan='2'><p align='center'>Grade Point</p></th>
														</tr>
														<tr>
															<th><p align='center'>Excuse</p></th>
															<th><p align='center'>Sick</p></th>
															<th><p align='center'>Absent</p></th>
															<th><p align='center'>Late</p></th>
														</tr>
														";
													if($rowrecord)
													{
														$no = 1;
														$tahun = 0;
														$kumulatif = 0;
														$kumulatifsks = 0;
														$kumulatifbobot = 0;
														$totalhexc = 0;
														$totalhsic = 0;
														$totalhabs = 0;
														$totalhlat = 0;
														$ipk = 0;
														foreach($rowrecord as $row)
														{
															$totalbobot = 0;
															$jumlahsks = 0;
															$jumlahexc = 0;
															$jumlahsic = 0;
															$jumlahabs = 0;
															$jumlahlat = 0;
															if($row->Sesi <= $FilterSemester)
															{
																$kumulatif++;
																if($no == 1)
																{
																	$tahun = $tahun + 1;
																	$no++;
																}
																else
																{
																	$tahun = $tahun;
																	$no = 1;
																}
																$KelasID = $row->KelasID;
																$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
																if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
																$DosenID = $row->WaliKelasID;
																$rhr1 = $this->m_dosen->PTL_select($DosenID);
																if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
																$DosenID = $row->WaliKelasID2;
																$rhr2 = $this->m_dosen->PTL_select($DosenID);
																if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
																$SpesialisasiID = $row->SpesialisasiID;
																$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
																if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
																$b = "<font color='green' size='4'><b>";
																$thn = "";
																if($row->TahunKe == 1){ $thn = "st"; }
																if($row->TahunKe == 2){ $thn = "nd"; }
																if($row->TahunKe == 3){ $thn = "rd"; }
																$smt = "";
																if($row->Sesi == 1){ $smt = "st"; }
																if($row->Sesi == 2){ $smt = "nd"; }
																if($row->Sesi == 3){ $smt = "rd"; }
																if($row->Sesi == 4){ $smt = "th"; }
																if($row->Sesi == 5){ $smt = "th"; }
																if($row->Sesi == 6){ $smt = "th"; }
																$MhswID = $row->MhswID;
																$KHSID = $row->KHSID;
																$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
																$ips = 0;
																$jumlahmp = 0;
																$jumlahbobot = 0;
																if($rowkrs)
																{
																	if($row->suspend == "Y")
																	{
																		echo "<tr class='success'>
																				<td colspan='14'><div style='text-align:center;'><font color='red'><b><h3>Financial Problem</h3></b></font></div></td>
																			</tr>";
																	}
																	echo "
																		<tr>
																			<th colspan='14' title='TahunID: $row->TahunID'><p align='center'>$row->TahunKe$thn YEAR - $row->Sesi$smt SEMESTER</p></th>
																		</tr>";
																	$ns = 1;
																	foreach($rowkrs as $rkr)
																	{
																		$SubjekID = $rkr->SubjekID;
																		$rsub = $this->m_subjek->PTL_select($SubjekID);
																		$subjek = $rsub["Nama"];
																		$sks = $rsub["SKS"];
																		$jumlahsks = $jumlahsks + $rsub["SKS"];
																		$KurikulumID = $rsub["KurikulumID"];
																		$JenisMKID = $rsub["JenisMKID"];
																		$GradeNilai = $rkr->GradeNilai;
																		$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
																		$bobot = 0;
																		if($resbobot)
																		{
																			$bobot = $resbobot['Bobot'];
																		}
																		$KRSID = $rkr->KRSID;
																		$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																		if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																		$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																		if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																		$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																		if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																		$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																		if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																		$tottexc = $texc;
																		$jumlahexc = $jumlahexc + $tottexc;
																		if($tottexc == 0){ $tottexc = ""; }
																		$tottsic = $tsic;
																		$jumlahsic = $jumlahsic + $tottsic;
																		if($tottsic == 0){ $tottsic = ""; }
																		$tottabs = $tabs;
																		$jumlahabs = $jumlahabs + $tottabs;
																		if($tottabs == 0){ $tottabs = ""; }
																		$tottlat = $tlat;
																		$jumlahlat = $jumlahlat + $tottlat;
																		if($tottlat == 0){ $tottlat = ""; }
																		$TahunID = $rkr->TahunID;
																		
																		$exam = 0;
																		$ExamID = "";
																		$LinkExamID = "";
																		$ExamMhswID = "";
																		$LinkExamMhswID = "";
																		$TotJum = 0;
																		$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
																		if($rexmhsw1)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw1 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA1)
																				{
																					if($rexmhswA1['Nilai'] > 0)
																					{
																						$LinkExamID .= 'A'.$row->ExamID;
																						$jumexam = $jumexam + $rexmhswA1['Nilai'];
																						$ExamMhswID = $rexmhswA1['ExamMhswID'];
																						$LinkExamMhswID .= 'A'.$rexmhswA1['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
																		if($rexmhsw2)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw2 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA2)
																				{
																					if($rexmhswA2['Nilai'] > 0)
																					{
																						$LinkExamID .= 'B'.$row->ExamID;
																						$jumexam = $jumexam + $rexmhswA2['Nilai'];
																						$ExamMhswID = $rexmhswA2['ExamMhswID'];
																						$LinkExamMhswID .= 'B'.$rexmhswA2['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
																		if($rexmhsw3)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw3 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA3)
																				{
																					if($rexmhswA3['Nilai'] > 0)
																					{
																						$LinkExamID .= 'C'.$row->ExamID;
																						$jumexam = $jumexam + $rexmhswA3['Nilai'];
																						$ExamMhswID = $rexmhswA3['ExamMhswID'];
																						$LinkExamMhswID .= 'C'.$rexmhswA3['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
																		if($rexmhsw4)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw4 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA4)
																				{
																					if($rexmhswA4['Nilai'] > 0)
																					{
																						$LinkExamID .= 'D'.$row->ExamID;
																						$jumexam = $jumexam + $rexmhswA4['Nilai'];
																						$ExamMhswID = $rexmhswA4['ExamMhswID'];
																						$LinkExamMhswID .= 'D'.$rexmhswA4['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
																		if($rexmhsw5)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw5 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA5)
																				{
																					if($rexmhswA5['Nilai'] > 0)
																					{
																						$LinkExamID .= 'E'.$row->ExamID;
																						$exam = $exam + $rexmhswA5['Nilai'];
																						$ExamMhswID = $rexmhswA5['ExamMhswID'];
																						$LinkExamMhswID .= 'E'.$rexmhswA5['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
																		if($rexmhsw6)
																		{
																			$jum = 0;
																			$jumexam = 0;
																			foreach($rexmhsw6 as $row)
																			{
																				$ExamID = $row->ExamID;
																				$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																				if($rexmhswA6)
																				{
																					if($rexmhswA6['Nilai'] > 0)
																					{
																						$LinkExamID .= 'F'.$row->ExamID;
																						$exam = $exam + $rexmhswA6['Nilai'];
																						$ExamMhswID = $rexmhswA6['ExamMhswID'];
																						$LinkExamMhswID .= 'F'.$rexmhswA6['ExamMhswID'];
																						$jum++;
																						$TotJum++;
																					}
																				}
																			}
																			if($jum > 0)
																			{
																				$exam = $exam + ($jumexam / $jum);
																			}
																		}
																		if($exam == 0)
																		{
																			$exam = "-";
																		}
																		else
																		{
																			if($TotJum > 1)
																			{
																				$exam = number_format(($exam),2,'.','');
																				// $exam = number_format(($exam / $TotJum),2,'.','');
																			}
																		}
																		$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
																		$remedial = "-";
																		$KRSRemedialID = "";
																		if($resremedial)
																		{
																			$remedial = $resremedial['Nilai'];
																			$KRSRemedialID = $resremedial['KRSRemedialID'];
																		}
																		if($rkr->gradevalue == "")
																		{
																			$mp = "";
																		}
																		else
																		{
																			// $mp = number_format(($rkr->gradevalue - $rkr->NilaiAkhir),2,'.','');
																			$JenisPresensiID = "E";
																			$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																			$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
																			$mptottexc = $texc * $pr1;
																			$JenisPresensiID = "S";
																			$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																			$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
																			$mptottsic = $tsic * $pr2;
																			$JenisPresensiID = "A";
																			$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																			$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
																			$mptottabs = $tabs * $pr3;
																			$JenisPresensiID = "L";
																			$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
																			$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
																			$mptottlat = $tlat * $pr4;
																			$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
																			if($mp == 0.00)
																			{
																				$mp = "";
																			}
																			$jumlahmp = $jumlahmp + $mp;
																		}
																		$jumlahbobot = $bobot * $sks;
																		$totalbobot = $totalbobot + $jumlahbobot;
																		$optionsub = "";
																		if($rsub["Optional"] == "Y")
																		{
																			$optionsub = "<font size='1'> (Optional)</font>";
																		}
																		$StatusSubject = "";
																		if($rkr->NA == "Y")
																		{
																			$StatusSubject = "<br/><font color='red'><b>(Drop)</b></font>";
																		}
																		echo "<tr class='info'>
																				<td>$ns</td>
																				<td title='KRS ID: $rkr->KRSID ~ Subject ID: $rkr->SubjekID'><a href='".site_url("attendance/ptl_list/$rkr->JadwalID")."' title='Go to Attendance' target='_blank'>$subjek$optionsub</a><a href='".site_url("drop/ptl_search/".$MhswID."_-_".str_replace(" ","_",$r["Nama"]))."' title='Go to Student Subject Management' target='_blank'>$StatusSubject</a></td>
																				<td><p align='right'><a href='".site_url("scoring/ptl_list/$rkr->JadwalID")."' title='Go to Scoring' target='_blank'>$rkr->NilaiAkhir</a></p></td>
																				<td><p align='center'>$tottexc</p></td>
																				<td><p align='center'>$tottsic</p></td>
																				<td><p align='center'>$tottabs</p></td>
																				<td><p align='center'>$tottlat</p></td>
																				<td><p align='right'>$mp</p></td>
																				<td><p align='right'>$rkr->gradevalue</p></td>
																				<td title='ExamID : $LinkExamID ~ ExamMhswID : $LinkExamMhswID'><p align='right'>$exam</p></td>
																				<td title='KRSRemedialID : $KRSRemedialID'><p align='right'>$remedial</p></td>
																				<td><p align='center'>$rkr->GradeNilai</p></td>
																				<td><p align='center'>$sks</p></td>
																				<td><p align='right'>$jumlahbobot</p></td>
																			</tr>";
																		$ns++;
																	}
																}
															}
															$kumulatifsks = $kumulatifsks + $jumlahsks;
															$kumulatifbobot = $kumulatifbobot + $totalbobot;
															$totalhexc = $totalhexc + $jumlahexc;
															$totalhsic = $totalhsic + $jumlahsic;
															$totalhabs = $totalhabs + $jumlahabs;
															$totalhlat = $totalhlat + $jumlahlat;
														}
														if($kumulatifbobot > 0)
														{
															$ipk = number_format(($kumulatifbobot / $kumulatifsks), 2);
														}
														else
														{
															$ipk = 0;
														}
														echo "
															<tr>
																<td colspan='3' rowspan='2' align='center'>CUMULATIVE ATTENDANCES</td>
																<td align='right'>EXCUSE</td>
																<td align='right'>SICK</td>
																<td align='right'>ABSENT</td>
																<td align='right'>LATE</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td align='right'>$totalhexc</td>
																<td align='right'>$totalhsic</td>
																<td align='right'>$totalhabs</td>
																<td align='right'>$totalhlat</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='14'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='5' align='right'>Cumulative Grade points ($kumulatif semesters) :</td>
																<td align='right'>$kumulatifbobot</td>
																<td colspan='10'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='5' align='right'>Cumulative Credit earned to Date ($kumulatif semesters) :</td>
																<td align='right'>$kumulatifsks</td>
																<td colspan='10' align='center'>ACADEMIC PROGRAM MANAGER / Mr. DESILLES Patrice</td>
															</tr>
															<tr>
																<td colspan='5' align='right'>Cumulative Grade Point Average earned to Date ($kumulatif Semesters) :</td>
																<td align='right'>$ipk</td>
																<td align='right'>&nbsp;</td>
																<td align='right'>&nbsp;</td>
																<td align='right'>&nbsp;</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='14'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>GRADE</td>
																<td align='center'>REMARKS</td>
																<td align='center'>GPA</td>
																<td colspan='3' align='center'>PROJECT SCORES</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>A</td>
																<td align='center'>VERY GOOD</td>
																<td align='center'>3.3 - 4</td>
																<td colspan='3' align='center'>15.5 - 20</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>B</td>
																<td align='center'>GOOD</td>
																<td align='center'>2.75 - 3,29</td>
																<td colspan='3' align='center'>12.75 - 15,49</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>C</td>
																<td align='center'>SATISFACTORY</td>
																<td align='center'>2 - 2,74</td>
																<td colspan='3' align='center'>10 - 12,74</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>D</td>
																<td align='center'>UNSATISFACTORY</td>
																<td align='center'>1 - 1,99</td>
																<td colspan='3' align='center'>9 - 9,99</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='2' align='center'>E</td>
																<td align='center'>FAILED no credit awarded</td>
																<td align='center'>0 - 0,99</td>
																<td colspan='3' align='center'>0 - 8,99</td>
																<td colspan='7'>&nbsp;</td>
															</tr>
															<tr>
																<td colspan='4' align='center'><font size='1'>Jalan Asem Dua N 3 -5, Cipete 12410 Jakarta Selatan, INDONESIA</font></td>
																<td colspan='5' align='center'><font size='1'>Tel: +62 21 765 91 81</font></td>
																<td colspan='5' align='center'><font size='1'>Email: info@esmodjakarta.com http://esmodjakarta.com</font></td>
															</tr>
															<tr>
																<td colspan='14' align='center'><font size='1'><font color='red'>WWW.ESMOD.COM</font> - BERLIN, BEYROUTH, BORDEAUX, DAMAS, DUBAI, GUANGZHOU, ISTANBUL, <font color='red'>JAKARTA</font>, KUALA LUMPUR, KYOTO, LYON, MOSCOU, OSLO, PARIS, RENNES, ROUBAIX, SEOUL, SOUSSE, TOKYO, TUNI</font></td>
															</tr>";
													}
													else
													{
														echo "<tr>
																<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												}
											?>
										</tr>
									</tbody>
								</table>
								<center>
									<?php
										if($mhsw != "")
										{
											echo "<a href='".site_url("transcript/ptl_pdf/ESMOD%20JAKARTA")."' class='btn btn-info'>Print Transcript</a>
												<br/>
												<br/>
												<a href='".site_url("transcript/ptl_excel")."' class='btn btn-success'>Download Excel</a>";
										}
									?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>