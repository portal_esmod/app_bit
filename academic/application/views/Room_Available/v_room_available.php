		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Room Available</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("room_available"); ?>">Room Available</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>List of Room Available.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("room_available/ptl_filter_tanggal"); ?>" method="POST" id="formId">
												<input type="text" name="cektanggal" value="<?php echo $today; ?>" id="Date1" class="form-control">
											</form>
										</td>
									</tr>
								</table>
								<?php
									echo "<center><h2>".day_name($today).", ".tgl_singkat_eng($today)."</h2></center>";
								?>
								<br/>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Room</th>
											<th>Time</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											if($rowruang)
											{
												foreach($rowruang as $rr)
												{
													$RuangID = $rr->RuangID;
													$rowpresensi = $this->m_presensi->PTL_all_room_available($RuangID,$today);
													$Terpakai = "";
													if($rowpresensi)
													{
														foreach($rowpresensi as $rp)
														{
															$JamAwal = intval(substr($rp->JamMulai,0,2).substr($rp->JamMulai,3,2));
															$JamAkhir = intval(substr($rp->JamSelesai,0,2).substr($rp->JamSelesai,3,2));
															for($i=$JamAwal;$i<=$JamAkhir;$i++)
															{
																$Terpakai .= ",$i,";
															}
															// echo "<tr>
																	// <td>$rp->PresensiID</td>
																	// <td>$rp->JamMulai</td>
																	// <td>$rp->JamSelesai</td>
																// </tr>";
														}
													}
													$JmAw = 0;
													$JmAk = 2400;
													$A = 1;
													$Kosong = "";
													for($i2=$JmAw;$i2<=$JmAk;$i2++)
													{
														if(stristr($Terpakai,",".$i2.","))
														{
															if($A == 2)
															{
																$K1 = substr(sprintf('%04s', $i2),0,2);
																$K2 = substr(sprintf('%04s', $i2),2,2);
																$Kosong .= "$K1:$K2<br/>";
																$A = 1;
															}
															else
															{
																
															}
														}
														else
														{
															if($A == 1)
															{
																$K1 = substr(sprintf('%04s', $i2),0,2);
																$K2 = substr(sprintf('%04s', $i2),2,2);
																$Kosong .= "$K1:$K2 - ";
																$A++;
															}
															else
															{
																if($i2 == $JmAk)
																{
																	$K1 = substr(sprintf('%04s', $i2),0,2);
																	$K2 = substr(sprintf('%04s', $i2),2,2);
																	$Kosong .= "$K1:$K2<br/>";
																}
																else
																{
																	
																}
															}
														}
													}
													echo "<tr>
															<td>$no</td>
															<td>$rr->RuangID</td>
															<td>$Kosong</td>
														</tr>";
													$no++;
												}
											}
											else
											{
												echo "<tr>
														<td colspan='4' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>