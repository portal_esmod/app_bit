		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Specialization</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("specialization"); ?>">Specialization</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of specialization.
								</div>
								<center><a href="<?php echo site_url("specialization/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>
																<td title='SpesialisasiID : $row->SpesialisasiID'>$no</td>
																<td>$row->SpesialisasiKode</td>
																<td>$row->Nama</td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("specialization/ptl_edit/$row->SpesialisasiID")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													else
													{
														echo "<tr>
																<td title='SpesialisasiID : $row->SpesialisasiID'>$no</td>
																<td>$row->SpesialisasiKode</td>
																<td>$row->Nama</td>
																<td class='center'>
																	<a class='btn btn-info' href='".site_url("specialization/ptl_edit/$row->SpesialisasiID")."'>
																		<i class='fa fa-list'></i>
																	</a>
																</td>
															</tr>";
													}
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>