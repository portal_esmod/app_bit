		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pending Payment (New Students)</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("pending_new"); ?>">Pending Payment (New Students)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is pending payment for new students. <font color="red"><b>For more information, please contact Finance Team.</b></font>
								</div>
								<table class="table table-bordered table-striped">
									<tr class="info">
										<td>
											<form action="<?php echo site_url("pending_new/ptl_filter_pending_pmb_period"); ?>" method="POST">
												<select name="cekperiode" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PERIOD --</option>
													<?php
														if($periode)
														{
															$cekperiode = $this->session->userdata('academic_filter_pending_new_period');
															$TotPeriod = 1;
															foreach($periode as $per)
															{
																if($TotPeriod <= 5)
																{
																	echo "<option value='".$per->PMBPeriodID."'";
																	if($cekperiode == $per->PMBPeriodID)
																	{
																		echo "selected";
																	}
																	echo ">".$per->PMBPeriodID." - ".$per->Nama."</option>";
																	$TotPeriod++;
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("pending_new/ptl_filter_pending_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('academic_filter_pending_new_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("pending_new/ptl_filter_pending_prodi"); ?>" method="POST">
												<select name="cekprodi" title="Filter by Program" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PRODI --</option>
													<?php
														$cekprodi = $this->session->userdata('academic_filter_pending_new_prodi');
														if($cekjurusan == "INT")
														{
															if($programd1)
															{
																foreach($programd1 as $d1)
																{
																	echo "<option value='$d1->ProdiID'";
																	if($cekprodi == $d1->ProdiID)
																	{
																		echo "selected";
																	}
																	echo ">$d1->Jenjang - ".strtoupper($d1->Nama)." - $d1->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "REG")
														{
															if($programd3)
															{
																foreach($programd3 as $d3)
																{
																	echo "<option value='$d3->ProdiID'";
																	if($cekprodi == strtoupper($d3->ProdiID))
																	{
																		echo "selected";
																	}
																	echo ">$d3->Jenjang - ".strtoupper($d3->Nama)." - $d3->ProdiID</option>";
																}
															}
														}
														if($cekjurusan == "SC")
														{
															if($shortcourse)
															{
																foreach($shortcourse as $sc)
																{
																	echo "<option value='$sc->KursusSingkatID'";
																	if($cekprodi == $sc->KursusSingkatID)
																	{
																		echo "selected";
																	}
																	echo ">SC - ".strtoupper($sc->Nama)." - $sc->KursusSingkatID</option>";
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th><u>Applicant ID</u><br>PMB ID</th>
											<th>Name</th>
											<th><u>Status</u><br>Graduation</th>
											<th><u>Payment Form</u><br>Program</th>
											<th>Prodi</th>
											<th><u>Payment</u><br>Bipot</th>
											<th>From Marketing</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													$StatusAwalID = $row->status_awal;
													$res = $this->m_status_awal->PTL_select($StatusAwalID);
													$ProdiID = $row->ProdiID;
													$res2 = $this->m_prodi->PTL_select($ProdiID);
													$PMBID = $row->PMBID;
													$detail = $this->m_master->PTL_bipot_mhsw_select($PMBID);
													$biaya2 = 0;
													$biaya_byr = 0;
													if($detail)
													{
														foreach($detail as $r)
														{
															if($r->TrxID == "1")
															{
																$biaya2 = $biaya2 + $r->Besar;
																$biaya_byr = $biaya_byr + $r->Dibayar;
															}
														}
													}
													echo "
														<tr>
															<td>$no</td>
															<td><u>$row->AplikanID</u><br>$row->PMBID</td>
															<td>$row->Nama</td>
															<td><u>".$res['Nama']."</u><br>$row->StatusAplikanID</td>
															<td><p align='right'><u>".formatRupiah($row->Harga)."</u><br>$row->ProgramID</p></td>
															<td><u>$row->ProdiID</u><br>".@$res2['Nama']."</td>
															<td><p align='right'><u>".formatRupiah($biaya_byr)."</u><br>".formatRupiah($biaya2)."</p></td>
															<td><p align='right'><u>".formatRupiah($row->total_bayar)."</u></p></td>
														</tr>
														";
													$no++;
												}
											}
											else
											{
												echo "<tr>
														<td colspan='8' align='center'><font color='red'><b>No data available</b></font></td>
													</tr>";
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>