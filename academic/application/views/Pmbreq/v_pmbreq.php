		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Requirement for Admission</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("pmbreq"); ?>">Form Requirement for Admission</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of Form Requirement for Admission.
								</div>
								<center><a href="<?php echo site_url("pmbreq/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Sequence</th>
											<th>Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													if($row->NA == "N")
													{
														echo "<tr class='success'>";
													}
													echo "<td><p align='center'>$no</p></td>
														<td><p align='center'>$row->Urutan</p></td>
														<td>$row->Nama</td>
														<td><p align='center'>";
														
										?>
															<a class="btn btn-danger" href="<?php echo site_url("pmbreq/ptl_delete/$row->PMBSyaratID"); ?>" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->PMBSyaratID.' - '.$row->Nama; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')" title="Delete this PMB Requirement">
																<i class="fa fa-times"></i>
															</a>
										<?php
														echo "
															<a class='btn btn-info' href='".site_url("pmbreq/ptl_edit/$row->PMBSyaratID")."' title='Edit this PMB Requirement'>
																<i class='fa fa-list'></i>
															</a>
														</p></td>
													</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>