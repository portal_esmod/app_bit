		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("pmbreq"); ?>">Form Requirement for Admission</a>
							>>
							<a href="<?php echo site_url("pmbreq/ptl_form"); ?>">Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("pmbreq/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Sequence</label>
											<input type="number" name="Urutan" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Description</label>
											<textarea name="Keterangan" class="form-control" required></textarea>
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("pmbreq"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>