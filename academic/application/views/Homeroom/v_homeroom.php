		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Homeroom</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("homeroom"); ?>">Homeroom (HOMER02)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>This is the fourth step before create schedule. <a href="<?php echo site_url("tutorial/ptl_detail/HMRM001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
								</div>
								<table class="table">
									<tr class="info">
										<td width="50px"><b>Filter</b></td>
										<td>
											<form action="<?php echo site_url("homeroom/ptl_filter_jur"); ?>" method="POST">
												<select name="cekjurusan" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- PROGRAM --</option>
													<?php
														$cekjurusan = $this->session->userdata('hmr_filter_jur');
														echo "<option value='REG'"; if($cekjurusan == 'REG'){ echo "selected"; } echo ">REG - REGULAR</option>";
														echo "<option value='INT'"; if($cekjurusan == 'INT'){ echo "selected"; } echo ">INT - INTENSIVE</option>";
														echo "<option value='SC'"; if($cekjurusan == 'SC'){ echo "selected"; } echo ">SC - SHORT COURSE</option>";
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<form action="<?php echo site_url("homeroom/ptl_filter_tahun"); ?>" method="POST">
												<?php
													$cektahun = $this->session->userdata('hmr_filter_tahun');
													$font = "";
													if($rowtahun)
													{
														foreach($rowtahun as $rt)
														{
															$f = "";
															if(($cektahun == $rt->TahunID) AND ($rt->NA == "N"))
															{
																$font = "style='background-color: #FFCD41;'";
															}
														}
													}
												?>
												<select name="cektahun" title="Filter by Period" <?php echo $font; ?> class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- ACADEMIC YEAR --</option>
													<?php
														if($cekjurusan != "")
														{
															if($rowtahun)
															{
																foreach($rowtahun as $rt)
																{
																	if($rt->NA == "N")
																	{
																		$f = "";
																		if($rt->NA == "N")
																		{
																			$f = "style='background-color: #5BB734;'";
																		}
																		echo "<option value='$rt->TahunID' $f";
																		if($cektahun == $rt->TahunID)
																		{
																			echo "selected";
																		}
																		echo ">$rt->TahunID - $rt->Nama</option>";
																	}
																}
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if(($cekjurusan != "") AND ($cektahun != ""))
											{
										?>
												<td>
													<form action="<?php echo site_url("homeroom/ptl_filter_prodi"); ?>" method="POST">
														<select name="cekprodi" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- PRODI --</option>
															<?php
																$cekprodi = $this->session->userdata('hmr_filter_prodi');
																if($rowprodi)
																{
																	if($cekjurusan == "SC")
																	{
																		foreach($rowprodi as $rp)
																		{
																			echo "<option value='$rp->KursusSingkatID'";
																			if($cekprodi == $rp->KursusSingkatID)
																			{
																				echo "selected";
																			}
																			echo ">$rp->KursusSingkatID - $rp->Nama</option>";
																		}
																	}
																	else
																	{
																		foreach($rowprodi as $rp)
																		{
																			echo "<option value='$rp->ProdiID'";
																			if($cekprodi == $rp->ProdiID)
																			{
																				echo "selected";
																			}
																			echo ">$rp->ProdiID - $rp->Nama</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
												<td>
													<form action="<?php echo site_url("homeroom/ptl_filter_kelas"); ?>" method="POST">
														<select name="cekkelas" title="Filter by Class" class="form-control round-form" onchange="this.form.submit()">
															<option value="_">-- CLASS --</option>
															<?php
																if($cekjurusan == "REG")
																{
																	$init = "";
																}
																if($cekjurusan == "INT")
																{
																	$init = "O";
																}
																$ni = "";
																$cekkelas = $this->session->userdata('hmr_filter_kelas');
																if($cekjurusan != "")
																{
																	if($rowkelas)
																	{
																		for($i=1;$i<=3;$i++)
																		{
																			foreach($rowkelas as $kls)
																			{
																				if($init == "")
																				{
																					$ni = $i;
																				}
																				else
																				{
																					$ni = "";
																				}
																				if($kls->ProgramID == $cekjurusan)
																				{
																					echo "<option value='".$i."_".$kls->KelasID."'";
																					if($cekkelas == $i."_".$kls->KelasID)
																					{
																						echo "selected";
																					}
																					echo ">$init$ni$kls->Nama</option>";
																				}
																			}
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
										<?php
											}
										?>
									</tr>
									<?php
										if(!stristr($_COOKIE["akses"],"LECTURER"))
										{
											if(($cekjurusan != "") AND ($cektahun != ""))
											{
									?>
												<tr>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
												</tr>
												<tr class="info">
													<td width="50px"><b>Set</b></td>
													<td>
														<form action="<?php echo site_url("homeroom/ptl_set_head"); ?>" method="POST">
															<select name="sethead" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<option value=''>-- HOMEROOM TO ALL --</option>
																<?php
																	$sethead = $this->session->userdata('hmr_set_head');
																	if($rowdosen1)
																	{
																		foreach($rowdosen1 as $rd1)
																		{
																			if($rd1->NA == "N")
																			{
																				echo "<option value='$rd1->Login'";
																				if($sethead == $rd1->Login)
																				{
																					echo "selected";
																				}
																				echo ">$rd1->Login - $rd1->Nama</option>";
																			}
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
													<td>
														<form action="<?php echo site_url("homeroom/ptl_set_vice"); ?>" method="POST">
															<select name="setvice" title="Filter by Period" class="form-control round-form" onchange="this.form.submit()">
																<option value=''>-- VICE HOMEROOM TO ALL --</option>
																<?php
																	$setvice = $this->session->userdata('hmr_set_vice');
																	if($rowdosen2)
																	{
																		foreach($rowdosen2 as $rd)
																		{
																			if($rd->NA == "N")
																			{
																				echo "<option value='$rd->Login'";
																				if($setvice == $rd->Login)
																				{
																					echo "selected";
																				}
																				echo ">$rd->Login - $rd->Nama</option>";
																			}
																		}
																	}
																?>
															</select>
															<noscript><input type="submit" value="Submit"></noscript>
														</form>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</td>
												</tr>
									<?php
											}
										}
									?>
								</table>
								<?php
									if(($cekjurusan != "") AND ($cektahun != ""))
									{
								?>
										<form role="form" action="<?php echo site_url("homeroom/ptl_set_all"); ?>" method="POST">
											<table class="table">
												<thead>
													<tr>
														<th>#</th>
														<th colspan="2">Student Name & ID</th>
														<th>Program<br/>Program&nbsp;Studi</th>
														<th>Status</th>
														<th>Homeroom Teacher</th>
														<th>Vice Homeroom Teacher</th>
													</tr>
												</thead>
												<tbody>
													<?php
														if($rowrecord)
														{
															$no = 1;
															foreach($rowrecord as $row)
															{
																echo "
																	<input type='hidden' name='KHSID$no' value='$row->KHSID'>
																	";
																$MhswID = $row->MhswID;
																$r = $this->m_mahasiswa->PTL_select($MhswID);
																$foto = "foto_umum/user.jpg";
																$foto_preview = "foto_umum/user.jpg";
																$nama = "";
																if($r)
																{
																	if($r["Foto"] != "")
																	{
																		$foto = "foto_mahasiswa/".$r["Foto"];
																		$exist = file_exists_remote(base_url("ptl_storage/$foto"));
																		if($exist)
																		{
																			$foto = $foto;
																			$source_photo = base_url("ptl_storage/$foto");
																			$info = pathinfo($source_photo);
																			$foto_preview = "foto_mahasiswa/".$info["filename"]."_compress.".$info["extension"];
																			$exist1 = file_exists_remote(base_url("ptl_storage/$foto_preview"));
																			if($exist1)
																			{
																				$foto_preview = $foto_preview;
																			}
																			else
																			{
																				$foto_preview = $foto;
																			}
																		}
																		else
																		{
																			$foto = "foto_umum/user.jpg";
																			$foto_preview = "foto_umum/user.jpg";
																		}
																	}
																	$nama = $r["Nama"];
																}
																$ProdiID = $row->ProdiID;
																if($row->ProgramID == "SC")
																{
																	$KursusSingkatID = $ProdiID;
																	$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
																}
																else
																{
																	$p = $this->m_prodi->PTL_select($ProdiID);
																}
																$StatusMhswID = $row->StatusMhswID;
																$s = $this->m_status->PTL_select($StatusMhswID);
																if($s["StatusMhswID"] == "A")
																{
																	$status = "<font color='green'><b>$s[Nama]</b></font>";
																}
																else
																{
																	$status = "<font color='red'><b>$s[Nama]</b></font>";
																}
																$KelasID = $row->KelasID;
																$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
																$NamaKelas = "";
																if($reskelas)
																{
																	$NamaKelas = $reskelas["Nama"];
																}
																$Finance = "";
																if($row->suspend == "Y")
																{
																	$Finance = " (<font color='red'><b>Financial Problem</b></font>)";
																}
																echo "<tr class='info'>
																		<td title='KHSID : $row->KHSID ~ Created by : $row->login_buat ~ Created Date : $row->tanggal_buat ~ Edited by : $row->login_edit ~ Edited Date : $row->tanggal_edit'>$no</td>
																		<td>
																			<a class='fancybox' title='$row->MhswID - $nama' href='".base_url("ptl_storage/$foto")."' data-fancybox-group='gallery' >
																				<img class='img-polaroid' src='".base_url("ptl_storage/$foto_preview")."' width='50px' alt='' />
																			</a>
																		</td>
																		<td><a href='".site_url("evaluation/ptl_cari/$row->MhswID")."' title='Go to Student Evaluation' target='_blank'><b>$row->MhswID</b></a><br/><a href='".site_url("students/ptl_edit/$row->MhswID")."' title='Go to Personal Information' target='_blank'>$nama</a> ($status)$Finance</td>
																		<td>$row->ProgramID<br/>$p[Nama]</td>
																		<td>$s[Nama]<br/>$row->TahunKe$NamaKelas</td>
																		<td>";
																		if($row->WaliKelasID == "")
																		{
																			$sty = "style='background-color: #FF4B4B;'";
																		}
																		else
																		{
																			$DosenID = $row->WaliKelasID;
																			$d = $this->m_dosen->PTL_select($DosenID);
																			if($d)
																			{
																				if($d["NA"] == "N")
																				{
																					$sty = "";
																				}
																				else
																				{
																					$sty = "style='background-color: #FFCD41;'";
																				}
																			}
																			else
																			{
																				if($row->WaliKelasID != "")
																				{
																					$sty = "style='background-color: #FFCD41;'";
																				}
																			}
																		}
																		echo "<select name='WaliKelasID$no' title='Select Homeroom Teacher' class='form-control round-form' $sty>
																			<option value=''>-- Set Homeroom Teacher --</option>";
																		if($sethead == "")
																		{
																			if($rowdosen1)
																			{
																				foreach($rowdosen1 as $rd1)
																				{
																					if($rd1->NA == "N")
																					{
																						echo "<option value='$rd1->Login'";
																						if($row->WaliKelasID == $rd1->Login)
																						{
																							echo "selected";
																						}
																						echo ">$rd1->Login - $rd1->Nama</option>";
																					}
																				}
																			}
																		}
																		else
																		{
																			if($rowdosen1)
																			{
																				foreach($rowdosen1 as $rd2)
																				{
																					if($rd2->NA == "N")
																					{
																						echo "<option value='$rd2->Login'";
																						if($sethead == $rd2->Login)
																						{
																							echo "selected";
																						}
																						echo ">$rd2->Login - $rd2->Nama</option>";
																					}
																				}
																			}
																		}
																		if($rowdosen1)
																		{
																			foreach($rowdosen1 as $rd3)
																			{
																				if($rd3->NA == "N")
																				{
																					if($row->WaliKelasID == "")
																					{
																						$cur1 = "No data";
																					}
																					else
																					{
																						if($row->WaliKelasID == $rd3->Login)
																						{
																							$cur1 = $rd3->Nama;
																						}
																					}
																				}
																			}
																		}
																echo "</select>
																		Current: $cur1</td>
																		<td>";
																		if($row->WaliKelasID2 == "")
																		{
																			$sty = "style='background-color: #FF4B4B;'";
																		}
																		else
																		{
																			$DosenID = $row->WaliKelasID2;
																			$d = $this->m_dosen->PTL_select($DosenID);
																			if($d)
																			{
																				if($d["NA"] == "N")
																				{
																					$sty = "";
																				}
																				else
																				{
																					$sty = "style='background-color: #FFCD41;'";
																				}
																			}
																			else
																			{
																				if($row->WaliKelasID2 != "")
																				{
																					$sty = "style='background-color: #FFCD41;'";
																				}
																			}
																		}
																		echo "<select name='WaliKelasIDB$no' title='Select Vice Homeroom Teacher' class='form-control round-form' $sty>
																			<option value=''>-- Set Vice Homeroom Teacher --</option>";
																		if($setvice == "")
																		{
																			if($rowdosen2)
																			{
																				foreach($rowdosen2 as $rd)
																				{
																					if($rd->NA == "N")
																					{
																						echo "<option value='$rd->Login'";
																						if($row->WaliKelasID2 == $rd->Login)
																						{
																							echo "selected";
																						}
																						echo ">$rd->Login - $rd->Nama</option>";
																					}
																				}
																			}
																		}
																		else
																		{
																			if($rowdosen2)
																			{
																				foreach($rowdosen2 as $rd)
																				{
																					if($rd->NA == "N")
																					{
																						echo "<option value='$rd->Login'";
																						if($setvice == $rd->Login)
																						{
																							echo "selected";
																						}
																						echo ">$rd->Login - $rd->Nama</option>";
																					}
																				}
																			}
																		}
																		if($rowdosen2)
																		{
																			foreach($rowdosen2 as $rd)
																			{
																				if($rd->NA == "N")
																				{
																					if($row->WaliKelasID2 == "")
																					{
																						$cur2 = "No data";
																					}
																					else
																					{
																						if($row->WaliKelasID2 == $rd->Login)
																						{
																							$cur2 = $rd->Nama;
																						}
																					}
																				}
																			}
																		}
																echo "</select>
																		Current: $cur2</td>
																	</tr>";
																$no++;
															}
															echo "<input type='hidden' name='total' value='$no'>";
														}
														else
														{
															echo "<tr>
																	<td colspan='7'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
																</tr>";
														}
													?>
												</tbody>
											</table>
											<?php
												if(!stristr($_COOKIE["akses"],"LECTURER"))
												{
											?>
													<center><input type="submit" value="Update Homeroom" id="my_button" class="btn btn-primary" class="btn btn-info"></center>
											<?php
												}
											?>
											<br/>
											<br/>
											<br/>
										</form>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>All recorded activity for this page.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>EID</th>
											<th>Name</th>
											<th>Activity</th>
											<th>Data</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowlog)
											{
												$no = 1;
												foreach($rowlog as $row)
												{
													echo "<tr>
															<td>$no</td>
															<td>$row->id_akun</td>
															<td>$row->nama</td>
															<td>$row->aktifitas</td>
															<td>$row->data</td>
															<td>$row->tanggal_buat</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>