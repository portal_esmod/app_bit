		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Curriculum</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("curriculum"); ?>">Curriculum</a>
							>>
							<a href="<?php echo site_url("curriculum/ptl_form"); ?>">Add New Curriculum</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("curriculum/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="KurikulumKode" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Session Name</label>
											<input type="text" name="Sesi" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Total Session</label>
											<input type="text" name="JmlSesi" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("curriculum"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>