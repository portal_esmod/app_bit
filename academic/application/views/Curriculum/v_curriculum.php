		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Curriculum</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("curriculum"); ?>">Curriculum</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>Only one curriculum that may be active.
								</div>
								<center><a href="<?php echo site_url("curriculum/ptl_form"); ?>" class="btn btn-primary">Add New</a></center>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Session Name</th>
											<th>Total Session</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
														echo "<td title='KurikulumID : $row->KurikulumID'>$no</td>
															<td>$row->KurikulumKode</td>
															<td>$row->Nama</td>
															<td>$row->Sesi</td>
															<td>$row->JmlSesi</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("curriculum/ptl_edit/$row->KurikulumID")."'>
																	<i class='fa fa-list'></i>
																</a>";
															if($row->NA == "Y")
															{
																echo "<a class='btn btn-success' href='".site_url("curriculum/ptl_enabled/$row->KurikulumID")."'>
																		<i class='fa fa-check'></i>
																	</a>";
															}
															else
															{
																echo "<a class='btn btn-danger' href='".site_url("curriculum/ptl_disabled/$row->KurikulumID")."'>
																		<i class='fa fa-times'></i>
																	</a>";
															}
														echo "</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>