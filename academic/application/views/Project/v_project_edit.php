		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Project</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project"); ?>">Project</a>
							>>
							<a href="<?php echo site_url("project/ptl_edit/$MKID"); ?>">Edit Project</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("project/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Curriculum</label>
											<input readonly type="text"value="<?php echo $kurikulum; ?>" class="form-control">
											<input type="hidden" name="MKID" value="<?php echo $MKID; ?>">
											<input type="hidden" name="KurikulumID" value="<?php echo $KurikulumID; ?>">
											<input type="hidden" name="SubjekID" value="<?php echo $SubjekID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject</label>
											<input readonly type="text" value="<?php echo $subjek; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Project Code</label>
											<input type="text" name="MKKode" value="<?php echo $MKKode; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Project Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Sequence</label>
											<input type="number" name="NoUrut" value="<?php echo $NoUrut; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Abbreviation</label>
											<input type="text" name="Singkatan" value="<?php echo $Singkatan; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Details</label>
											<textarea name="Keterangan" class="form-control"><?php echo $Keterangan; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Program Studi</label>
											</div>
											<?php
												$no = 0;
												$word = explode(".",$ProdiID);
												$td3 = count($rowd3);
												if($rowd3)
												{
													foreach($rowd3 as $rd3)
													{
														$no++;
														$cd3 = "";
														for($d3=0;$d3<$td3;$d3++)
														{
															if(@$word[$d3] == $rd3->ProdiID)
															{
																$cd3 = "checked";
															}
														}
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd3->ProdiID' $cd3>&nbsp;$rd3->ProdiID - $rd3->Nama</div>";
													}
												}
												$td1 = count($rowd1);
												if($rowd1)
												{
													foreach($rowd1 as $rd1)
													{
														$no++;
														$cd1 = "";
														for($d1=0;$d1<$td1;$d1++)
														{
															if(@$word[$d1] == $rd1->ProdiID)
															{
																$cd1 = "checked";
															}
														}
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd1->ProdiID' $cd1>&nbsp;$rd1->ProdiID - $rd1->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="total" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="col-lg-12">
											<br/>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Specialization</label>
											</div>
											<?php
												$nob = 0;
												$word = explode(".",$SpesialisasiID);
												$tspc = count($rowspc);
												if($rowspc)
												{
													foreach($rowspc as $rspc)
													{
														$nob++;
														$spc = "";
														for($sp=0;$sp<$tspc;$sp++)
														{
															if(@$word[$sp] == $rspc->SpesialisasiID)
															{
																$spc = "checked";
															}
														}
														echo "<div class='col-lg-12'><input type='checkbox' name='SpesialisasiID$nob' value='$rspc->SpesialisasiID' $spc>&nbsp;$rspc->SpesialisasiKode - $rspc->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="totalb" value="<?php echo $nob; ?>">
											<p class="help-block"></p>
										</div>
										<div class="col-lg-12">
											<br/>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Optional?</label>
											</div>
											<input type="checkbox" name="Optional" value="Y" <?php if($Optional == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Not Active?</label>
											</div>
											<input type="checkbox" name="NA" value="Y" <?php if($NA == "Y"){ echo "checked"; } ?> class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("project"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>