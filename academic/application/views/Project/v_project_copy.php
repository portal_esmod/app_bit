		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Copy Projects</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project"); ?>">Project</a>
							>>
							<a href="<?php echo site_url("project/ptl_copy"); ?>">Copy Projects</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>You can copy the project of inter-curriculum.
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							The project will be copied
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("project/ptl_filter_kur1"); ?>" method="POST">
												<select name="cekkurikulum1" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- CURRICULUM --</option>
													<?php
														$cekkurikulum1 = $this->session->userdata('project_filter_kur1');
														if($rowkurikulum)
														{
															foreach($rowkurikulum as $rk)
															{
																echo "<option value='$rk->KurikulumID'";
																if($cekkurikulum1 == $rk->KurikulumID)
																{
																	echo "selected";
																}
																echo ">$rk->KurikulumKode - $rk->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if($cekkurikulum1 != "")
											{
										?>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("project/ptl_filter_sub1"); ?>" method="POST">
														<select name="ceksubjek1" title="Filter by Subject" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- SUBJECT --</option>
															<?php
																$ceksubjek1 = $this->session->userdata('project_filter_sub1');
																if($rowsubjek1)
																{
																	foreach($rowsubjek1 as $rs)
																	{
																		// if($rs->NA == "N")
																		// {
																			echo "<option value='$rs->SubjekID'";
																			if($ceksubjek1 == $rs->SubjekID)
																			{
																				echo "selected";
																			}
																			echo ">$rs->Nama</option>";
																		// }
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
										<?php
											}
										?>
									</tr>
								</table>
								<br/>
								<?php
									if($cekkurikulum1 != "")
									{
								?>
										<form role="form" action="<?php echo site_url("project/ptl_set_copy"); ?>" method="POST">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>
															<?php
																$cek = $this->session->userdata('project_set_all1');
																if($cek == "")
																{
																	echo "<a href='".site_url("project/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
																}
																else
																{
																	echo "<a href='".site_url("project/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
																}
															?>
														</th>
														<th>Subjects Name</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$n = 0;
														if($rowrecord1)
														{
															foreach($rowrecord1 as $row)
															{
																$n++;
																echo "<tr>
																		<td title='MKID : $row->MKID'>$n</td>
																		<td>";
																		if($cek == "")
																		{
																			echo "<input type='checkbox' name='cek$n' value='$row->MKID'>";
																		}
																		else
																		{
																			echo "<input type='checkbox' name='cek$n' value='$row->MKID' checked>";
																		}
																echo "</td>
																		<td>$row->Nama ($row->MKKode)</td>
																	</tr>";
															}
														}
														else
														{
															echo "
																<tr>
																	<td colspan='3' align='center'><font color='red'><b>No data available</b></font></td>
																</tr>
																";
														}
													?>
												</tbody>
												<tfoot>
													<tr>
														<th>#</th>
														<th>
															<?php
																$cek = $this->session->userdata('project_set_all1');
																if($cek == "")
																{
																	echo "<a href='".site_url("project/ptl_set_check_all1")."' class='btn btn-success'><i class='fa fa-check'></i></a>";
																}
																else
																{
																	echo "<a href='".site_url("project/ptl_set_uncheck_all1")."' class='btn btn-warning'><i class='fa fa-times'></i></a>";
																}
															?>
														</th>
														<th>
															<?php
																echo "<input type='hidden' name='total1' value='$n'>";
																echo "<button type='submit' class='btn btn-primary'>Copy Allocated</button>";
															?>
														</th>
													</tr>
												</tfoot>
											</table>
										</form>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							The copy object
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("project/ptl_filter_kur2"); ?>" method="POST">
												<select name="cekkurikulum2" title="Filter by Curriculum" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- CURRICULUM --</option>
													<?php
														$cekkurikulum2 = $this->session->userdata('project_filter_kur2');
														if($rowkurikulum)
														{
															foreach($rowkurikulum as $rk)
															{
																// if($cekkurikulum1 != $rk->KurikulumID)
																// {
																	echo "<option value='$rk->KurikulumID'";
																	if($cekkurikulum2 == $rk->KurikulumID)
																	{
																		echo "selected";
																	}
																	echo ">$rk->KurikulumKode - $rk->Nama</option>";
																// }
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<?php
											if($cekkurikulum2 != "")
											{
										?>
												<td>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</td>
												<td>
													<form action="<?php echo site_url("project/ptl_filter_sub2"); ?>" method="POST">
														<select name="ceksubjek2" title="Filter by Subject" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- SUBJECT --</option>
															<?php
																$ceksubjek2 = $this->session->userdata('project_filter_sub2');
																if($rowsubjek2)
																{
																	foreach($rowsubjek2 as $rs)
																	{
																		// if($rs->NA == "N")
																		// {
																		if($ceksubjek1 != $rs->SubjekID)
																		{
																			echo "<option value='$rs->SubjekID'";
																			if($ceksubjek2 == $rs->SubjekID)
																			{
																				echo "selected";
																			}
																			echo ">$rs->Nama</option>";
																		}
																		// }
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
												</td>
										<?php
											}
										?>
									</tr>
								</table>
								<br/>
								<?php
									if($cekkurikulum2 != "")
									{
								?>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>#</th>
													<th>Subjects Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$n2 = 0;
													if($rowrecord2)
													{
														foreach($rowrecord2 as $row)
														{
															$n2++;
															echo "<tr>
																	<td title='MKID : $row->MKID'>$n2</td>
																	<td>$row->Nama ($row->MKKode)</td>
																	<td>";
												?>
																		<a href="<?php echo site_url("project/ptl_delete/$row->MKID"); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data (<?php echo $row->MKID; ?>)?\n\nTHIS ACTION CAN NOT BE RESTORED.')"><i class="fa fa-times"></i></a>
												<?php
																	
															echo "</td>
																</tr>";
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='3' align='center'><font color='red'><b>No data available</b></font></td>
															</tr>
															";
													}
												?>
											</tbody>
											<tfoot>
												<tr>
													<th>#</th>
													<th>Subjects Name</th>
													<th>Action</th>
												</tr>
											</tfoot>
										</table>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>