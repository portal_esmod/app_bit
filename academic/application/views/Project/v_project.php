		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Project</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project"); ?>">Project</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									<a class="alert-link">Notes: </a>The following is a list of subject.
								</div>
								<table>
									<tr>
										<td>
											<form action="<?php echo site_url("project/ptl_filter_kur"); ?>" method="POST">
												<select name="cekkurikulum" title="Filter by Curriculum. Code <?php echo $this->session->userdata('project_filter_kur'); ?>" class="form-control round-form" onchange="this.form.submit()">
													<option value=''>-- CURRICULUM --</option>
													<?php
														$cekkurikulum = $this->session->userdata('project_filter_kur');
														if($rowkurikulum)
														{
															foreach($rowkurikulum as $rk)
															{
																// if($rk->NA == "N")
																// {
																	echo "<option value='$rk->KurikulumID'";
																	if($cekkurikulum == $rk->KurikulumID)
																	{
																		echo "selected";
																	}
																	echo ">$rk->KurikulumKode - $rk->Nama</option>";
																// }
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td>
											<?php
												if($cekkurikulum != "")
												{
											?>
													<form action="<?php echo site_url("project/ptl_filter_sub"); ?>" method="POST">
														<select name="ceksubjek" title="Filter by Curriculum. Code <?php echo $this->session->userdata('project_filter_sub'); ?>" class="form-control round-form" onchange="this.form.submit()">
															<option value=''>-- SUBJECT --</option>
															<?php
																$ceksubjek = $this->session->userdata('project_filter_sub');
																if($rowsubjek)
																{
																	foreach($rowsubjek as $rs)
																	{
																		if($rs->NA == "N")
																		{
																			echo "<option value='$rs->SubjekID'";
																			if($ceksubjek == $rs->SubjekID)
																			{
																				echo "selected";
																			}
																			echo ">$rs->Nama</option>";
																		}
																	}
																}
															?>
														</select>
														<noscript><input type="submit" value="Submit"></noscript>
													</form>
											<?php
												}
											?>
										</td>
									</tr>
								</table>
								<br/>
								<?php
									if($cekkurikulum != "")
									{
										if($rowsubjek)
										{
								?>
											<center>
												<a href="<?php echo site_url("project/ptl_form/$cekkurikulum/$ceksubjek"); ?>" class="btn btn-primary">Add New</a>
												<a href="<?php echo site_url("project/ptl_copy"); ?>" class="btn btn-warning"><i class="fa fa-copy"> Copy</i></a>
											</center>
								<?php
										}
								?>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<th>#</th>
													<th>Sequence</th>
													<th>Code</th>
													<th>Name</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if($rowrecord)
													{
														$no = 1;
														foreach($rowrecord as $row)
														{
															if($row->NA == "Y")
															{
																echo "<tr class='danger'>";
															}
															else
															{
																echo "<tr class='info'>";
															}
																echo "<td title='MKID : $row->MKID'>$no</td>
																	<td>$row->NoUrut</td>
																	<td>$row->MKKode</td>
																	<td>$row->Nama</td>
																	<td class='center'>
																		<a class='btn btn-info' href='".site_url("project/ptl_edit/$row->MKID")."'>
																			<i class='fa fa-list'></i>
																		</a>
																	</td>
																</tr>";
															$no++;
														}
													}
												?>
											</tbody>
										</table>
								<?php
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>