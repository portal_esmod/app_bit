		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Project</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("project"); ?>">Project</a>
							>>
							<a href="<?php echo site_url("project/ptl_form"); ?>">Add New Project</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("project/ptl_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Curriculum</label>
											<input readonly type="text"value="<?php echo $kurikulum; ?>" class="form-control">
											<input type="hidden" name="KurikulumID" value="<?php echo $KurikulumID; ?>">
											<input type="hidden" name="SubjekID" value="<?php echo $SubjekID; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Subject</label>
											<input readonly type="text" value="<?php echo $subjek; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Project Code</label>
											<input type="text" name="MKKode" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Project Name</label>
											<input type="text" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Sequence</label>
											<input type="number" name="NoUrut" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Abbreviation</label>
											<input type="text" name="Singkatan" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Details</label>
											<textarea name="Keterangan" class="form-control"></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Program Studi</label>
											</div>
											<?php
												$no = 0;
												if($rowd3)
												{
													foreach($rowd3 as $rd3)
													{
														$no++;
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd3->ProdiID'>&nbsp;$rd3->ProdiID - $rd3->Nama</div>";
													}
												}
												if($rowd1)
												{
													foreach($rowd1 as $rd1)
													{
														$no++;
														echo "<div class='col-lg-12'><input type='checkbox' name='ProdiID$no' value='$rd1->ProdiID'>&nbsp;$rd1->ProdiID - $rd1->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="total" value="<?php echo $no; ?>">
											<p class="help-block"></p>
										</div>
										<div class="col-lg-12">
											<br/>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Specialization</label>
											</div>
											<?php
												$nob = 0;
												if($rowspc)
												{
													foreach($rowspc as $rspc)
													{
														$nob++;
														echo "<div class='col-lg-12'><input type='checkbox' name='SpesialisasiID$nob' value='$rspc->SpesialisasiID'>&nbsp;$rspc->SpesialisasiKode - $rspc->Nama</div>";
													}
												}
											?>
											<input type="hidden" name="totalb" value="<?php echo $nob; ?>">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>Optional?</label>
											</div>
											<input type="checkbox" name="Optional" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("project"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>