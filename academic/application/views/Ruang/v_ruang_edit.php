		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("room"); ?>">Room</a>
							>>
							<a href="<?php echo site_url("room/ptl_edit/$RuangID"); ?>">Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("room/ptl_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input readonly type="text" name="RuangID" value="<?php echo $RuangID; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<div class="col-lg-12">
												<label>For Concentration</label>
											</div>
											<?php
												$n = 0;
												if($rowd3)
												{
													foreach($rowd3 as $d3)
													{
														$n++;
														$cek = "";
														if(stristr($ProdiID,$d3->ProdiID))
														{
															$cek = "checked";
														}
														echo "<input type='checkbox' name='prodi$n' value='$d3->ProdiID' $cek> $d3->ProdiID - $d3->Nama<br/>";
													}
												}
												if($rowd1)
												{
													foreach($rowd1 as $d1)
													{
														$n++;
														$cek = "";
														if(stristr($ProdiID,$d1->ProdiID))
														{
															$cek = "checked";
														}
														echo "<input type='checkbox' name='prodi$n' value='$d1->ProdiID' $cek> $d1->ProdiID - $d1->Nama<br/>";
													}
												}
												echo "<input type='hidden' name='total' value='$n'>";
											?>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Campus</label>
											<select name="KampusID" title="Filter by Campus" class="form-control">
												<option value=''>-- CAMPUS --</option>
												<?php
													if($rowkampus)
													{
														foreach($rowkampus as $rk)
														{
															echo "<option value='$rk->KampusID'";
															if($rk->KampusID == $KampusID)
															{
																echo "selected";
															}
															echo ">$rk->KampusID - $rk->Nama</option>";
														}
													}
												?>
											</select>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Floor</label>
											<input type="number" name="Lantai" value="<?php echo $Lantai; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>For Classroom?</label>
											<input type="checkbox" name="RuangKuliah" value="Y" class="form-control" <?php if($UntukUjian == "Y"){ echo "checked"; }?>>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Capacity</label>
											<input type="number" name="Kapasitas" value="<?php echo $Kapasitas; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Capacity Test</label>
											<input type="number" name="KapasitasUjian" value="<?php echo $KapasitasUjian; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Number of Columns Exam</label>
											<input type="number" name="KolomUjian" value="<?php echo $KolomUjian; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>For the Entrance Exam?</label>
											<input type="checkbox" name="UntukUjian" value="Y" class="form-control" <?php if($UntukUjian == "Y"){ echo "checked"; }?>>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Notes</label>
											<textarea name="Keterangan" class="form-control"><?php echo $Keterangan; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control" <?php if($NA == "Y"){ echo "checked"; }?>>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("room"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>