		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Room</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("room"); ?>">Room</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<div class="alert alert-info">
									 <a class="alert-link">Notes: </a>The following is a list of room.
								</div>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td>
											<form action="<?php echo site_url("room/ptl_filter_kampus"); ?>" method="POST">
												<select name="cekkampus" title="Filter by Campus" class="form-control" onchange="this.form.submit()">
													<option value=''>-- CAMPUS --</option>
													<?php
														$cekkampus = $this->session->userdata('room_filter_kampus');
														if($rowkampus)
														{
															foreach($rowkampus as $rk)
															{
																echo "<option value='$rk->KampusID'";
																if($cekkampus == $rk->KampusID)
																{
																	echo "selected";
																}
																echo ">$rk->KampusID - $rk->Nama</option>";
															}
														}
													?>
												</select>
												<noscript><input type="submit" value="Submit"></noscript>
											</form>
										</td>
										<td>
											<a href="<?php echo site_url("room/ptl_form"); ?>" class="btn btn-primary">Add New</a>
										</td>
										<td>
											<a href="<?php echo site_url("room/ptl_campus"); ?>" class="btn btn-success">Go To Campus</a>
										</td>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Code</th>
											<th>Name</th>
											<th>Classroom?</th>
											<th>Capacity</th>
											<th>Notes</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if($rowrecord)
											{
												$no = 1;
												foreach($rowrecord as $row)
												{
													if($row->NA == "Y")
													{
														echo "<tr class='danger'>";
													}
													else
													{
														echo "<tr>";
													}
													echo "<td>$no</td>
															<td>$row->RuangID</td>
															<td>$row->Nama</td>
															<td align='center'><img src='".base_url("assets/dashboard/img/$row->RuangKuliah.gif")."'></td>
															<td>$row->KapasitasUjian - $row->Kapasitas</td>
															<td>$row->Keterangan</td>
															<td class='center'>
																<a class='btn btn-info' href='".site_url("room/ptl_edit/$row->RuangID")."'>
																	<i class='fa fa-list'></i>
																</a>
															</td>
														</tr>";
													$no++;
												}
											}
										?>
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>