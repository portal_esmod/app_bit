		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("room"); ?>">Room</a>
							>>
							<a href="<?php echo site_url("room/ptl_campus"); ?>">Campus</a>
							>>
							<a href="<?php echo site_url("room/ptl_campus_form"); ?>">Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("room/ptl_campus_insert"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input type="text" name="KampusID" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="Alamat" class="form-control"></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="Kota" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="number" name="Telepon" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Fax</label>
											<input type="number" name="Fax" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("room/ptl_campus"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>