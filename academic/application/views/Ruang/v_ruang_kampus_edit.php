		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("room"); ?>">Room</a>
							>>
							<a href="<?php echo site_url("room/ptl_campus"); ?>">Campus</a>
							>>
							<a href="<?php echo site_url("room/ptl_campus_edit/$KampusID"); ?>">Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<form role="form" action="<?php echo site_url("room/ptl_campus_update"); ?>" method="POST">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Code</label>
											<input readonly type="text" name="KampusID" value="<?php echo $KampusID; ?>" class="form-control" required>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Name</label>
											<input type="text" name="Nama" value="<?php echo $Nama; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea name="Alamat" class="form-control"><?php echo $Alamat; ?></textarea>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="Kota" value="<?php echo $Kota; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Phone</label>
											<input type="text" name="Telepon" value="<?php echo $Telepon; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Fax</label>
											<input type="text" name="Fax" value="<?php echo $Fax; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Not Active?</label>
											<input type="checkbox" name="NA" value="Y" class="form-control" <?php if($NA == "Y"){ echo "checked"; }?>>
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created By</label>
											<input readonly type="text" name="login_buat" value="<?php echo $login_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Created Date</label>
											<input readonly type="text" name="tanggal_buat" value="<?php echo $tanggal_buat; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited By</label>
											<input readonly type="text" name="login_edit" value="<?php echo $login_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<div class="form-group">
											<label>Edited Date</label>
											<input readonly type="text" name="tanggal_edit" value="<?php echo $tanggal_edit; ?>" class="form-control">
											<p class="help-block"></p>
										</div>
										<center>
											<a href="<?php echo site_url("room/ptl_campus"); ?>" class="btn btn-warning">Back</a>
											<button type="reset" class="btn btn-danger">Reset</button>
											<button type="submit" class="btn btn-primary">Save</button>
										</center>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>