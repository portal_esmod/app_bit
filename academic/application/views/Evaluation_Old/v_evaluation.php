			<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student Evaluation</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo site_url("evaluation"); ?>">Student Evaluation</a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="alert alert-info">
				<a class="alert-link">Notes: </a>This page is used to view the student evaluation. <a href="<?php echo site_url("tutorial/ptl_detail/EVAL001"); ?>" class="btn btn-success">GO TO TUTORIAL</a>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center>Student Evaluation Detail</center>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<?php
									$mhsw = $this->session->userdata('eval_filter_mahasiswa');
								?>
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<form role="form" action="<?php echo site_url("evaluation/ptl_cari"); ?>" method="POST">
											<td colspan="3">
												<input type="text" name="cari" value="<?php echo $mhsw; ?>" id="mahasiswa" placeholder="Search the student" class="form-control" required/>
											</td>
											<td>
												<input type="submit" value="Search" class="btn btn-primary">
											</td>
										</form>
									</tr>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<?php
										$r = $this->m_mahasiswa->PTL_select($MhswID);
										if($r)
										{
									?>
											<tr>
												<?php
													if($r["Foto"] == "")
													{
														$foto = "foto_umum/user.jpg";
													}
													else
													{
														$foto = "foto_mahasiswa/".$r["Foto"];
														$exist = file_exists_remote(base_url("ptl_storage/$foto"));
														if($exist)
														{
															$foto = $foto;
														}
														else
														{
															$foto = "foto_umum/user.jpg";
														}
													}
												?>
												<td rowspan="5"><p align="center"><img src="<?php echo base_url("ptl_storage/$foto"); ?>" width="200px"/></p></td>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $r["Nama"]; ?></td>
											</tr>
											<tr>
												<td>Birth Place / Date</td>
												<td>:</td>
												<td>
													<?php
														$tgl = tgl_singkat_eng($r["TanggalLahir"]);
														if($r["TanggalLahir"] == "NULL")
														{
															$tgl = "";
														}
														echo $r["TempatLahir"].", ".$tgl;
													?>
												</td>
											</tr>
											<tr>
												<td>Program</td>
												<td>:</td>
												<td>
													<?php
														if($r["ProgramID"] == "REG") { echo "REG - REGULAR"; }
														if($r["ProgramID"] == "INT") { echo "INT - INTENSIVE"; }
														if($r["ProgramID"] == "SC") { echo "SC - SHORT COURSE"; }
													?>
												</td>
											</tr>
											<tr>
												<td>Program Study</td>
												<td>:</td>
												<td>
													<?php
														$ProdiID =  $r["ProdiID"];
														if($r["ProgramID"] == "SC")
														{
															$KursusSingkatID = $ProdiID;
															$p = $this->m_kursussingkat->PTL_select($KursusSingkatID);
														}
														else
														{
															$p = $this->m_prodi->PTL_select($ProdiID);
														}
														echo $ProdiID." - ".$p["Nama"];
													?>
												</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td>
													<?php
														$StatusMhswID = $r["StatusMhswID"];
														$s = $this->m_status->PTL_select($StatusMhswID);
														echo $s["Nama"];
													?>
												</td>
											</tr>
									<?php
										}
									?>
								</table>
								<table class="table table-striped table-bordered table-hover">
									<tbody>
										<tr>
											<?php
												if($mhsw != "")
												{
													if($rowrecord)
													{
														$no = 1;
														$tahun = 0;
														$kumulatif = 0;
														$kumulatifsks = 0;
														$kumulatifipk = 0;
														$ipk = 0;
														foreach($rowrecord as $row)
														{
															$kumulatif++;
															if($no == 1)
															{
																$tahun = $tahun + 1;
																$no++;
															}
															else
															{
																$tahun = $tahun;
																$no = 1;
															}
															$KelasID = $row->KelasID;
															$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
															if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
															$DosenID = $row->WaliKelasID;
															$rhr1 = $this->m_dosen->PTL_select($DosenID);
															if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
															$DosenID = $row->WaliKelasID2;
															$rhr2 = $this->m_dosen->PTL_select($DosenID);
															if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
															$SpesialisasiID = $row->SpesialisasiID;
															$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
															if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
															$b = "<font color='green' size='4'><b>";
															$thn = "";
															if($tahun == 1){ $thn = "st"; }
															if($tahun == 2){ $thn = "nd"; }
															if($tahun == 3){ $thn = "rd"; }
															if($r["ProgramID"] == "INT")
															{
																$TahunKe = "O";
															}
															else
															{
																$TahunKe = $tahun;
															}
															$ProgramID = $r["ProgramID"];
															$smt = "";
															if($row->Sesi == 1){ $smt = "st"; }
															if($row->Sesi == 2){ $smt = "nd"; }
															if($row->Sesi == 3){ $smt = "rd"; }
															if($row->Sesi == 4){ $smt = "th"; }
															if($row->Sesi == 5){ $smt = "th"; }
															if($row->Sesi == 6){ $smt = "th"; }
															echo "<tr class='success'>
																	<td>$b $tahun$thn&nbsp;Year</td>
																	<td>$b $row->Sesi$smt&nbsp;Semester</td>
																	<td colspan='2'>$b Class:<br/>$TahunKe$kelas</td>
																	<td colspan='2'>$b Specialization:<br/>$spe</td>
																	<td colspan='4'>$b Homeroom:<br/>$hr1</td>
																	<td colspan='4'>$b Vice Homeroom:<br/>$hr2</td>
																</tr>";
															$MhswID = $row->MhswID;
															$KHSID = $row->KHSID;
															$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
															$totalbobot = 0;
															$ips = 0;
															if($rowkrs)
															{
																echo "
																	<tr>
																		<th rowspan='2' align='center'><p align='center'>#</p></th>
																		<th rowspan='2'><p align='center'>Subject</p></th>
																		<th rowspan='2'><p align='center'>Score</p></th>
																		<th colspan='4'><p align='center'>Attendance</p></th>
																		<th rowspan='2'><p align='center'>Minus/Plus</p></th>
																		<th rowspan='2'><p align='center'>Grade Value</p></th>
																		<th rowspan='2'><p align='center'>Exam Score</p></th>
																		<th rowspan='2'><p align='center'>Remedial Score</p></th>
																		<th rowspan='2'><p align='center'>Grade</p></th>
																		<th rowspan='2'><p align='center'>Credit</p></th>
																		<th rowspan='2'><p align='center'>Grade Point</p></th>
																	</tr>
																	<tr>
																		<th><p align='center'>Excuse</p></th>
																		<th><p align='center'>Sick</p></th>
																		<th><p align='center'>Absent</p></th>
																		<th><p align='center'>Late</p></th>
																	</tr>
																	";
																$ns = 1;
																$jumlahsks = 0;
																$jumlahexc = 0;
																$jumlahsic = 0;
																$jumlahabs = 0;
																$jumlahlat = 0;
																$jumlahmp = 0;
																$jumlahbobot = 0;
																foreach($rowkrs as $rkr)
																{
																	$TahunID = $rkr->TahunID;
																	$JadwalID = $rkr->JadwalID;
																	$SubjekID = $rkr->SubjekID;
																	$rsub = $this->m_subjek->PTL_select($SubjekID);
																	$subjek = $rsub["Nama"];
																	$sks = $rsub["SKS"];
																	$jumlahsks = $jumlahsks + $rsub["SKS"];
																	$KurikulumID = $rsub["KurikulumID"];
																	$JenisMKID = $rsub["JenisMKID"];
																	$GradeNilai = $rkr->GradeNilai;
																	$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
																	$bobot = 0;
																	if($resbobot)
																	{
																		$bobot = $resbobot['Bobot'];
																	}
																	$KRSID = $rkr->KRSID;
																	$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
																	if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
																	$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
																	if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
																	$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
																	if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
																	$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
																	if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
																	$tottexc = $texc;
																	$jumlahexc = $jumlahexc + $tottexc;
																	if($tottexc == 0){ $tottexc = ""; }
																	$tottsic = $tsic;
																	$jumlahsic = $jumlahsic + $tottsic;
																	if($tottsic == 0){ $tottsic = ""; }
																	$tottabs = $tabs;
																	$jumlahabs = $jumlahabs + $tottabs;
																	if($tottabs == 0){ $tottabs = ""; }
																	$tottlat = $tlat;
																	$jumlahlat = $jumlahlat + $tottlat;
																	if($tottlat == 0){ $tottlat = ""; }
																	$TahunID = $rkr->TahunID;
																	
																	$exam = 0;
																	$ExamID = "";
																	$LinkExamID = "";
																	$ExamMhswID = "";
																	$LinkExamMhswID = "";
																	$TotJum = 0;
																	$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
																	if($rexmhsw1)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw1 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA1)
																			{
																				if($rexmhswA1['Nilai'] > 0)
																				{
																					$LinkExamID .= 'A'.$row->ExamID;
																					$jumexam = $jumexam + $rexmhswA1['Nilai'];
																					$ExamMhswID = $rexmhswA1['ExamMhswID'];
																					$LinkExamMhswID .= 'A'.$rexmhswA1['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
																	if($rexmhsw2)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw2 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA2)
																			{
																				if($rexmhswA2['Nilai'] > 0)
																				{
																					$LinkExamID .= 'B'.$row->ExamID;
																					$jumexam = $jumexam + $rexmhswA2['Nilai'];
																					$ExamMhswID = $rexmhswA2['ExamMhswID'];
																					$LinkExamMhswID .= 'B'.$rexmhswA2['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
																	if($rexmhsw3)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw3 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA3)
																			{
																				if($rexmhswA3['Nilai'] > 0)
																				{
																					$LinkExamID .= 'C'.$row->ExamID;
																					$jumexam = $jumexam + $rexmhswA3['Nilai'];
																					$ExamMhswID = $rexmhswA3['ExamMhswID'];
																					$LinkExamMhswID .= 'C'.$rexmhswA3['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
																	if($rexmhsw4)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw4 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA4)
																			{
																				if($rexmhswA4['Nilai'] > 0)
																				{
																					$LinkExamID .= 'D'.$row->ExamID;
																					$jumexam = $jumexam + $rexmhswA4['Nilai'];
																					$ExamMhswID = $rexmhswA4['ExamMhswID'];
																					$LinkExamMhswID .= 'D'.$rexmhswA4['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
																	if($rexmhsw5)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw5 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA5)
																			{
																				if($rexmhswA5['Nilai'] > 0)
																				{
																					$LinkExamID .= 'E'.$row->ExamID;
																					$exam = $exam + $rexmhswA5['Nilai'];
																					$ExamMhswID = $rexmhswA5['ExamMhswID'];
																					$LinkExamMhswID .= 'E'.$rexmhswA5['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
																	if($rexmhsw6)
																	{
																		$jum = 0;
																		$jumexam = 0;
																		foreach($rexmhsw6 as $row)
																		{
																			$ExamID = $row->ExamID;
																			$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
																			if($rexmhswA6)
																			{
																				if($rexmhswA6['Nilai'] > 0)
																				{
																					$LinkExamID .= 'F'.$row->ExamID;
																					$exam = $exam + $rexmhswA6['Nilai'];
																					$ExamMhswID = $rexmhswA6['ExamMhswID'];
																					$LinkExamMhswID .= 'F'.$rexmhswA6['ExamMhswID'];
																					$jum++;
																					$TotJum++;
																				}
																			}
																		}
																		if($jum > 0)
																		{
																			$exam = $exam + ($jumexam / $jum);
																		}
																	}
																	if($exam == 0)
																	{
																		$exam = "-";
																	}
																	else
																	{
																		if($TotJum > 1)
																		{
																			$exam = number_format(($exam / $TotJum),2,'.','');
																		}
																	}
																	$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
																	$remedial = "-";
																	$KRSRemedialID = "";
																	if($resremedial)
																	{
																		$remedial = $resremedial['Nilai'];
																		$KRSRemedialID = $resremedial['KRSRemedialID'];
																	}
																	
																	if($rkr->gradevalue == "")
																	{
																		$mp = "";
																	}
																	else
																	{
																		$mp = number_format(($rkr->gradevalue - $rkr->NilaiAkhir),2,'.','');
																		if($mp == 0.00)
																		{
																			$mp = "";
																		}
																		$jumlahmp = $jumlahmp + $mp;
																	}
																	$jumlahbobot = $bobot * $sks;
																	$totalbobot = $totalbobot + $jumlahbobot;
																	$optionsub = "";
																	if($rsub["Optional"] == "Y")
																	{
																		$optionsub = "<font size='1'> (Optional)</font>";
																	}
																	echo "<tr class='info'>
																			<td>$ns</td>
																			<td title='KRS ID: $rkr->KRSID ~ Subject ID: $rkr->SubjekID ~ JadwalID ID: $rkr->JadwalID'>$subjek$optionsub</td>
																			<td><p align='right'>$rkr->NilaiAkhir</p></td>
																			<td><p align='center'>$tottexc</p></td>
																			<td><p align='center'>$tottsic</p></td>
																			<td><p align='center'>$tottabs</p></td>
																			<td><p align='center'>$tottlat</p></td>
																			<td><p align='right'>$mp</p></td>
																			<td><p align='right'>$rkr->gradevalue</p></td>
																			<td title='ExamID : $LinkExamID ~ ExamMhswID : $LinkExamMhswID'><p align='right'>$exam</p></td>
																			<td title='KRSRemedialID : $KRSRemedialID'><p align='right'>$remedial</p></td>
																			<td><p align='center'>$rkr->GradeNilai</p></td>
																			<td><p align='center'>$sks</p></td>
																			<td><p align='right'>$jumlahbobot</p></td>
																		</tr>";
																	$rowmk = $this->m_mk->PTL_all_select($SubjekID);
																	if($rowmk)
																	{
																		foreach($rowmk as $rm)
																		{
																			$CekMKID = $rm->MKID;
																			$rescekmk = $this->m_presensi->PTL_select_cek_mk($JadwalID,$CekMKID);
																			if($rescekmk)
																			{
																				$MKID = $rm->MKID;
																				$rowkrs2 = $this->m_krs2->PTL_all_select_scoring($KRSID,$MKID);
																				$KRS2ID = "";
																				$Nilai = 0;
																				$AddNilai = 0;
																				if($rowkrs2)
																				{
																					$totNilai = 0;
																					$nkrs2 = 0;
																					foreach($rowkrs2 as $rkrs2)
																					{
																						$KRS2ID .= $rkrs2->KRS2ID.' - ';
																						if($rkrs2->Nilai > 0)
																						{
																							$totNilai = $totNilai + $rkrs2->Nilai;
																							$nkrs2++;
																						}
																						$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
																					}
																					if($nkrs2 > 0)
																					{
																						$Nilai = $totNilai / $nkrs2;
																					}
																				}
																				$tn = $Nilai + $AddNilai;
																				if($rm->Optional == "Y")
																				{
																					if($tn != 0)
																					{
																						echo "<tr class='info'>
																							<td>&nbsp;</td>
																							<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama (Optional)</font></td>
																							<td title='KRS2ID: $KRS2ID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																							<td colspan='11'>&nbsp;</td>
																						</tr>";
																					}
																				}
																				else
																				{
																					echo "<tr class='info'>
																							<td>&nbsp;</td>
																							<td title='MKID: $rm->MKID'><font size='1'>$rm->Nama</font></td>
																							<td title='KRS2ID: $KRS2ID'><p align='right'><font size='1'>".number_format($tn,2,'.','')."</font></p></td>
																							<td colspan='11'>&nbsp;</td>
																						</tr>";
																				}
																			}
																		}
																	}
																	$ns++;
																}
															}
															$kumulatifsks = $kumulatifsks + $jumlahsks;
															$ips = number_format(($totalbobot / $jumlahsks), 2);
															$kumulatifipk = $kumulatifipk + $ips;
															$ipk = number_format(($kumulatifipk / $kumulatif), 2);
															echo "
																<tr>
																	<td colspan='3' align='center'>TOTAL</td>
																	<td align='right'>$jumlahexc</td>
																	<td align='right'>$jumlahsic</td>
																	<td align='right'>$jumlahabs</td>
																	<td align='right'>$jumlahlat</td>
																	<td align='right'>$jumlahmp</td>
																	<td colspan='6'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Total Grade points earned this semester</td>
																	<td>:</td>
																	<td align='right'>$totalbobot</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Number of Credits earned this semester</td>
																	<td>:</td>
																	<td align='right'>$jumlahsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Grade Point Average earned this semester</td>
																	<td>:</td>
																	<td align='right'>$ips</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Credit earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$kumulatifsks</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='2'></td>
																	<td colspan='7'>Cumulative Grade Point Average earned to Date ($kumulatif semesters)</td>
																	<td>:</td>
																	<td align='right'>$ipk</td>
																	<td colspan='3'></td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>
																<tr>
																	<td colspan='14'>&nbsp;</td>
																</tr>";
														}
													}
													else
													{
														echo "
															<tr>
																<td colspan='4'><h4><b><p align='center'><font color='red'>No data available</font></p></b></h4></td>
															</tr>";
													}
												}
											?>
										</tr>
									</tbody>
								</table>
								<center>
									<?php
										if($mhsw != "")
										{
											echo "<a href='".site_url("evaluation/ptl_pdf/ESMOD%20JAKARTA")."' class='btn btn-info'>Print Evaluation</a>";
										}
									?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>