<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Calendar_type extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aktifitas');
			$this->load->model('m_kalender_jenis');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA09",
							'aktifitas' => "Mengakses halaman Academic Calendar - Type.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowrecord'] = $this->m_kalender_jenis->PTL_all_active();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Calendar/v_calendar_type_list',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA10",
							'aktifitas' => "Mengakses halaman Academic Calendar - Type Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowjenis'] = $this->m_kalender_jenis->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Calendar/v_calendar_type_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$id_kalender_jenis = $this->input->post('id_kalender_jenis');
			$nama = $this->input->post('nama');
			$pesan = "";
			if($id_kalender_jenis == "")
			{
				$pesan .= "Code, ";
			}
			if($nama == "")
			{
				$pesan .= "Name";
			}
			if($pesan != "")
			{
				$datalog = array(
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA11-N",
								'aktifitas' => "Mengakses halaman Academic Calendar - List Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You must fill $pesan.","../calendar_type/ptl_form");
			}
			else
			{
				$result = $this->m_kalender_jenis->PTL_select($id_kalender_jenis);
				if($result)
				{
					$datalog = array(
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ACACA11-N",
									'aktifitas' => "Mengakses halaman Academic Calendar - Type Insert.",
									'data' => "no data",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("The code has been used '$result[id_kalender_jenis] - $result[nama]'.","../calendar_type/ptl_form");
				}
				else
				{
					$datalog = array(
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ACACA11-Y",
									'aktifitas' => "Mengakses halaman Academic Calendar - Type Insert.",
									'data' => "no data",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					$data = array(
								'id_kalender_jenis' => $this->input->post('id_kalender_jenis'),
								'nama' => $this->input->post('nama'),
								'keterangan' => $this->input->post('keterangan'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_kalender_jenis->PTL_insert($data);
					echo warning("Your data successfully added.","../calendar_type");
				}
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$id = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $id,
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA12",
							'aktifitas' => "Mengakses halaman Academic Calendar - Type Edit $id.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_kalender_jenis->PTL_select($id);
			$data['id'] = $result['id'];
			$data['id_kalender_jenis'] = $result['id_kalender_jenis'];
			$data['nama'] = $result['nama'];
			$data['keterangan'] = $result['keterangan'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['na'] = $result['na'];
			$this->load->view('Portal/v_header');
			$this->load->view('Calendar/v_calendar_type_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$pesan = "";
			if($nama == "")
			{
				$pesan .= "Name";
			}
			if($pesan != "")
			{
				$datalog = array(
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "_SERVER-N",
								'aktifitas' => "Mengakses halaman Academic Calendar - Type Update $id.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You must fill $pesan.","../calendar_type/ptl_form");
			}
			else
			{
				$datalog = array(
								'pk1' => $id,
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA13-Y",
								'aktifitas' => "Mengakses halaman Academic Calendar - Type Update $id.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				if($this->input->post('na') == "") { $na = "N"; } else { $na = "Y"; }
				$data = array(
							'id_kalender_jenis' => $this->input->post('id_kalender_jenis'),
							'nama' => $this->input->post('nama'),
							'keterangan' => $this->input->post('keterangan'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'na' => $na
							);
				$this->m_kalender_jenis->PTL_update($id,$data);
				echo warning("Your data successfully updated.","../calendar_type");
			}
		}
	}
?>