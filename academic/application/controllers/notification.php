<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Notification extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_catatan2');
			$this->load->model('m_jadwal');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_notifikasi');
			$this->load->model('m_subjek');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','home');
			$id_akun = $_COOKIE["id_akun"];
			$data['rowrecord'] = $this->m_notifikasi->PTL_all_select($id_akun);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Notification/v_notification',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','home');
			$id_notifikasi = $this->uri->segment(3);
			$noteid = $this->uri->segment(4);
			$data['id_notifikasi'] = $id_notifikasi;
			$datanotif = array(
							'login_baca' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_baca' => $this->waktu
							);
			$this->m_notifikasi->PTL_update($id_notifikasi,$datanotif);
			if($noteid == 0)
			{
				$result = $this->m_notifikasi->PTL_select($id_notifikasi);
				$data['noteid'] = $result['id_notifikasi'];
				$data['MhswID'] = "";
				$data['title'] = "Notification from Lecturer";
				$data['comment'] = $result['title'];
				$data['login_buat'] = $result['login_buat'];
				$data['tanggal_buat'] = $result['tanggal_buat'];
				$data['Nama'] = "";
				$data['nama_buat'] = $result['nama_buat'];
				$data['NamaSubjek'] = "";
			}
			else
			{
				$result = $this->m_catatan2->PTL_select($noteid);
				$data['noteid'] = $result['noteid'];
				$data['MhswID'] = $result['MhswID'];
				$data['title'] = $result['title'];
				$data['comment'] = $result['comment'];
				$data['login_buat'] = $result['login_buat'];
				$data['tanggal_buat'] = $result['tanggal_buat'];
				$MhswID = $result['MhswID'];
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				$data['Nama'] = $resmhsw['Nama'];
				$resnotif = $this->m_notifikasi->PTL_select($id_notifikasi);
				$data['nama_buat'] = $resnotif['nama_buat'];
				$JadwalID = $result['JadwalID'];
				$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
				$SubjekID = $resjadwal['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$data['NamaSubjek'] = $ressub['Nama'];
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Notification/v_notification_read',$data);
			$this->load->view('Portal/v_footer');
		}
	}
?>