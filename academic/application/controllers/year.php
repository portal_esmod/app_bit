<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Year extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_khs');
			$this->load->model('m_kurikulum');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_rating_question_grup');
			$this->load->model('m_status');
			$this->load->model('m_year');
			$this->load->model('m_year_detail');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE01-01",
							'aktifitas' => "Filter halaman Academic Year - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('year_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('year_filter_jur');
			}
			redirect("year");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','year');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE01",
							'aktifitas' => "Mengakses halaman Academic Year.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('year_filter_jur');
			$data['rowrecord'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Year/v_year',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_participant()
		{
			$this->authentification();
			$this->session->set_userdata('menu','year');
			$TahunID = $this->uri->segment(3);
			// $datalog = array(
							// 'id_akun' => $_COOKIE["id_akun"],
							// 'nama' => $_COOKIE["nama"],
							// 'aplikasi' => "ACADEMIC",
							// 'menu' => $this->uri->segment(1),
							// 'submenu' => $this->uri->segment(2),
							// 'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							// 'kode_halaman' => "ACAYE02",
							// 'aktifitas' => "Mengakses halaman Academic Year - Participant $TahunID.",
							// 'data' => "no data",
							// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							// 'tanggal_buat' => $this->waktu
							// );
			// $this->m_aktifitas->PTL_insert($datalog);
			$data['TahunID'] = $TahunID;
			$data['rowrecord'] = $this->m_khs->PTL_all_total($TahunID);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Year/v_year_participant',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$TahunID = $this->uri->segment(3);
			$result = $this->m_year->PTL_select($TahunID);
			$res = $this->m_year->PTL_urut();
			$lastid = $res['LAST'] + 1;
			$ProgramID = $result['ProgramID'];
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $lastid,
							'pk3' => $ProgramID,
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE02",
							'aktifitas' => "Mengakses halaman Academic Year - Copy $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'TahunID' => $lastid,
						'ProgramID' => $result['ProgramID'],
						'Jenjang2ID' => $result['Jenjang2ID'],
						'Nama' => $result['Nama'],
						'Enrollment' => $result['Enrollment'],
						'EnrollmentEnd' => $result['EnrollmentEnd'],
						'Specialization' => $result['Specialization'],
						'SpecializationEnd' => $result['SpecializationEnd'],
						'Postpone' => $result['Postpone'],
						'PostponeEnd' => $result['PostponeEnd'],
						'Academic' => $result['Academic'],
						'AcademicEnd' => $result['AcademicEnd'],
						'Payment' => $result['Payment'],
						'PaymentEnd' => $result['PaymentEnd'],
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu,
						'NA' => 'Y'
						);
			$cek1 = $this->m_year->PTL_insert($data);
			$pesan = "";
			if(!$cek1)
			{
				$pesan .= "Master successfully copied. ";
			}
			else
			{
				$pesan .= "Master failed copied. ";
			}
			if($ProgramID == "REG")
			{
				$rowrecord = $this->m_prodi->PTL_all();
			}
			if($ProgramID == "INT")
			{
				$rowrecord = $this->m_prodi->PTL_all_d1();
			}
			if($ProgramID == "SC")
			{
				$rowrecord = $this->m_kursussingkat->PTL_all();
			}
			$cek2 = 0;
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$TahunID = $result['TahunID'];
					if($ProgramID == "SC")
					{
						$ProdiID = $row->KursusSingkatID;
					}
					else
					{
						$ProdiID = $row->ProdiID;
					}
					$rowrecord2 = $this->m_year_detail->PTL_all_spesifik($TahunID,$ProdiID);
					if($rowrecord2)
					{
						foreach($rowrecord2 as $row2)
						{
							$data = array(
										'TahunID' => $lastid,
										'ProgramID' => $ProgramID,
										'ProdiID' => $ProdiID,
										'nama' => $row2->nama,
										'tanggal_mulai' => $row2->tanggal_mulai,
										'tanggal_selesai' => $row2->tanggal_selesai,
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_year_detail->PTL_insert($data);
							$cek2++;
						}
					}
				}
			}
			if($cek2 > 0)
			{
				$pesan .= "Detail successfully copied. ";
			}
			else
			{
				$pesan .= "Detail failed copied. ";
			}
			echo warning("Your data successfully copied. $pesan","../year");
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','year');
			$jur = $this->session->userdata('year_filter_jur');
			if($jur == "")
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE03-N",
								'aktifitas' => "Mengakses halaman Academic Year - Form.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Sorry! You too long response. Please, try again.","../year");
			}
			else
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE03-Y",
								'aktifitas' => "Mengakses halaman Academic Year - Form $jur.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$h = "-7";
				$hm = $h * 60;
				$ms = $hm * 60;
				$kd = $this->session->userdata('year_filter_jur').gmdate("Y", time()-($ms))."/".(gmdate("Y", time()-($ms)) + 1);
				$data["kode_tahun"] = $kd;
				$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
				$data['rowrating'] = $this->m_rating_question_grup->PTL_all();
				$this->load->view('Portal/v_header');
				$this->load->view('Year/v_year_form',$data);
				$this->load->view('Portal/v_footer');
			}
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$jur = $this->session->userdata('year_filter_jur');
			if($jur == "")
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE04-N",
								'aktifitas' => "Mengakses halaman Academic Year - Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Sorry! You too long response. Please, try again.","../year");
			}
			else
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE04-Y",
								'aktifitas' => "Mengakses halaman Academic Year - Insert $jur.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$result = $this->m_year->PTL_urut();
				$lastid = $result['LAST'] + 1;
				$nama = $this->input->post('kode_tahun')." - ".$this->input->post('Nama')." SEMESTER";
				$data = array(
							'TahunID' => $lastid,
							'KurikulumID' => $this->input->post('KurikulumID'),
							'ProgramID' => $jur,
							'Jenjang2ID' => $this->input->post('Jenjang2ID'),
							'Nama' => $nama,
							'id_rating_question_grup' => $this->input->post('id_rating_question_grup'),
							'Enrollment' => $this->input->post('Enrollment'),
							'EnrollmentEnd' => $this->input->post('EnrollmentEnd'),
							'Specialization' => $this->input->post('Specialization'),
							'SpecializationEnd' => $this->input->post('SpecializationEnd'),
							'Postpone' => $this->input->post('Postpone'),
							'PostponeEnd' => $this->input->post('PostponeEnd'),
							'Academic' => $this->input->post('Academic'),
							'AcademicEnd' => $this->input->post('AcademicEnd'),
							'Payment' => $this->input->post('Payment'),
							'PaymentEnd' => $this->input->post('PaymentEnd'),
							'Questionnaire' => $this->input->post('Questionnaire'),
							'QuestionnaireEnd' => $this->input->post('QuestionnaireEnd'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu,
							'NA' => 'Y'
							);
				$this->m_year->PTL_insert($data);
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to('patrice@esmodjakarta.com');
				$this->email->cc('miha@esmodjakarta.com,anisa@esmodjakarta.com,rossy@esmodjakarta.com,ichaa@esmodjakarta.com,anita@esmodjakarta.com');
				$this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('NEW ACADEMIC YEAR (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					".strtoupper($_COOKIE["nama"])." was created new academic year with code <font color='green'><b>'$lastid - ".$this->input->post('KurikulumID')." - $nama'</b></font>.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Your data successfully added. Next, you have to fill in the details.","../year/ptl_detail_form/$lastid/$jur");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Your data successfully added. Next, you have to fill in the details.","../year/ptl_detail_form/$lastid/$jur");
					}
					else
					{
						echo warning("Email server is not active. Your data successfully added. Next, you have to fill in the details.","../year/ptl_detail_form/$lastid/$jur");
					}
				}
			}
		}
		
		function ptl_detail_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','year');
			$TahunID = $this->uri->segment(3);
			$ProgramID = $this->uri->segment(4);
			$ProdiID = $this->uri->segment(5);
			$id = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $ProgramID,
							'pk3' => $ProdiID,
							'pk4' => $id,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE05",
							'aktifitas' => "Mengakses halaman Academic Year - Detail Form Prodi : $ProdiID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['TahunID'] = $TahunID;
			$data['ProgramID'] = $ProgramID;
			$data['ProdiID'] = $ProdiID;
			$data['id'] = $id;
			if($ProgramID == "REG")
			{
				$data['rowrecord'] = $this->m_prodi->PTL_all();
			}
			if($ProgramID == "INT")
			{
				$data['rowrecord'] = $this->m_prodi->PTL_all_d1();
			}
			if($ProgramID == "SC")
			{
				$data['rowrecord'] = $this->m_kursussingkat->PTL_all();
			}
			$data['rowrecord2'] = $this->m_year_detail->PTL_all_spesifik($TahunID,$ProdiID);
			if($id != "")
			{
				$result = $this->m_year_detail->PTL_select($id);
				$data['id'] = $result['id'];
				$data['nama'] = $result['nama'];
				$data['tanggal_mulai'] = $result['tanggal_mulai'];
				$data['tanggal_selesai'] = $result['tanggal_selesai'];
			}
			else
			{
				$data['id'] = '';
				$data['nama'] = '';
				$data['tanggal_mulai'] = '';
				$data['tanggal_selesai'] = '';
			}
			$pk1 = $TahunID;
			$pk2 = "";
			$pk3 = "";
			$menu = "";
			$submenu = $this->uri->segment(2);
			$kode_halaman = "ACAYE05";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header');
			$this->load->view('Year/v_year_detail_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_detail_insert()
		{
			$this->authentification();
			$TahunID = $this->input->post('TahunID');
			$ProgramID = $this->input->post('ProgramID');
			$ProdiID = $this->input->post('ProdiID');
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $ProgramID,
							'pk3' => $ProdiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE06",
							'aktifitas' => "Mengakses halaman Academic Year - Detail Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'TahunID' => $TahunID,
						'ProgramID' => $ProgramID,
						'ProdiID' => $ProdiID,
						'nama' => $this->input->post('nama'),
						'tanggal_mulai' => $this->input->post('tanggal_mulai'),
						'tanggal_selesai' => $this->input->post('tanggal_selesai'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_year_detail->PTL_insert($data);
			echo warning("Your data successfully added.","../year/ptl_detail_form/$TahunID/$ProgramID/$ProdiID");
		}
		
		function ptl_detail_update()
		{
			$this->authentification();
			$id = $this->input->post('id');
			$TahunID = $this->input->post('TahunID');
			$ProgramID = $this->input->post('ProgramID');
			$ProdiID = $this->input->post('ProdiID');
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $ProgramID,
							'pk3' => $ProdiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE07",
							'aktifitas' => "Mengakses halaman Academic Year - Detail Update $id.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'nama' => $this->input->post('nama'),
						'tanggal_mulai' => $this->input->post('tanggal_mulai'),
						'tanggal_selesai' => $this->input->post('tanggal_selesai'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_year_detail->PTL_update($id,$data);
			echo warning("Your data successfully updated.","../year/ptl_detail_form/$TahunID/$ProgramID/$ProdiID");
		}
		
		function ptl_detail_delete()
		{
			$this->authentification();
			$TahunID = $this->uri->segment(3);
			$ProgramID = $this->uri->segment(4);
			$ProdiID = $this->uri->segment(5);
			$id = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $ProgramID,
							'pk3' => $ProdiID,
							'pk4' => $id,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE08",
							'aktifitas' => "Mengakses halaman Academic Year - Detail Delete $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'na' => 'Y'
						);
			$this->m_year_detail->PTL_update($id,$data);
			echo warning("Your data successfully deleted.","../year/ptl_detail_form/$TahunID/$ProgramID/$ProdiID");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','year');
			$jur = $this->session->userdata('year_filter_jur');
			if($jur == "")
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE09-N",
								'aktifitas' => "Mengakses halaman Academic Year - Edit.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Sorry! You too long response. Please, try again.","../year");
			}
			else
			{
				$TahunID = $this->uri->segment(3);
				$datalog = array(
								'pk1' => $TahunID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE09-Y",
								'aktifitas' => "Mengakses halaman Academic Year - Edit $TahunID.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$result = $this->m_year->PTL_select($TahunID);
				$data['TahunID'] = $result['TahunID'];
				$data['ProgramID'] = $result['ProgramID'];
				$data['Jenjang2ID'] = $result['Jenjang2ID'];
				if((preg_match("/REG/i", $result['Nama'])) OR (preg_match("/INT/i", $result['Nama'])) OR (preg_match("/SC/i", $result['Nama'])))
				{
					$data['Nama'] = $result['Nama'];
				}
				else
				{
					$tahun = substr($result['tanggal_buat'],0,4);
					if(is_numeric($tahun))
					{
						$thn = substr($result['tanggal_buat'],0,4);
					}
					else
					{
						$thn = substr($result['login_buat'],0,4);
					}
					$h = "-7";
					$hm = $h * 60;
					$ms = $hm * 60;
					$kd = $this->session->userdata('year_filter_jur').$thn."/".($thn + 1);
					$data['Nama'] = $kd;
				}
				$data['id_rating_question_grup'] = $result['id_rating_question_grup'];
				$data['KurikulumID'] = $result['KurikulumID'];
				$data['Nama'] = $result['Nama'];
				$data['Enrollment'] = $result['Enrollment'];
				$data['EnrollmentEnd'] = $result['EnrollmentEnd'];
				$data['Specialization'] = $result['Specialization'];
				$data['SpecializationEnd'] = $result['SpecializationEnd'];
				$data['Postpone'] = $result['Postpone'];
				$data['PostponeEnd'] = $result['PostponeEnd'];
				$data['Academic'] = $result['Academic'];
				$data['AcademicEnd'] = $result['AcademicEnd'];
				$data['Payment'] = $result['Payment'];
				$data['PaymentEnd'] = $result['PaymentEnd'];
				$data['Questionnaire'] = $result['Questionnaire'];
				$data['QuestionnaireEnd'] = $result['QuestionnaireEnd'];
				$data['login_buat'] = $result['login_buat'];
				$data['tanggal_buat'] = $result['tanggal_buat'];
				$data['login_edit'] = $result['login_edit'];
				$data['tanggal_edit'] = $result['tanggal_edit'];
				
				$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
				$data['rowrating'] = $this->m_rating_question_grup->PTL_all();
				$pk1 = $TahunID;
				$pk2 = "";
				$pk3 = "";
				$menu = "";
				$submenu = $this->uri->segment(2);
				$kode_halaman = "";
				$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
				$this->load->view('Portal/v_header');
				$this->load->view('Year/v_year_edit',$data);
				$this->load->view('Portal/v_footer');
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$TahunID = $this->input->post('TahunID');
			$jur = $this->session->userdata('year_filter_jur');
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $jur,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE10",
							'aktifitas' => "Mengakses halaman Academic Year - Update $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'Nama' =>  $this->input->post('kode_tahun'),
						'id_rating_question_grup' => $this->input->post('id_rating_question_grup'),
						'KurikulumID' => $this->input->post('KurikulumID'),
						'Enrollment' => $this->input->post('Enrollment'),
						'EnrollmentEnd' => $this->input->post('EnrollmentEnd'),
						'Specialization' => $this->input->post('Specialization'),
						'SpecializationEnd' => $this->input->post('SpecializationEnd'),
						'Postpone' => $this->input->post('Postpone'),
						'PostponeEnd' => $this->input->post('PostponeEnd'),
						'Academic' => $this->input->post('Academic'),
						'AcademicEnd' => $this->input->post('AcademicEnd'),
						'Payment' => $this->input->post('Payment'),
						'PaymentEnd' => $this->input->post('PaymentEnd'),
						'Questionnaire' => $this->input->post('Questionnaire'),
						'QuestionnaireEnd' => $this->input->post('QuestionnaireEnd'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_year->PTL_update($TahunID,$data);
			echo warning("Your data successfully updated. Next, you have to fill in the details.","../year/ptl_detail_form/$TahunID/$jur");
		}
		
		function ptl_disabled()
		{
			$this->authentification();
			$TahunID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE11",
							'aktifitas' => "Mengakses halaman Academic Year - Disabled $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_year->PTL_update($TahunID,$data);
			echo warning("Your data successfully disabled.","../year");
		}
		
		function ptl_enabled()
		{
			$this->authentification();
			$result = $this->m_year->PTL_all();
			if($result)
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE12-N",
								'aktifitas' => "Mengakses halaman Academic Year - Enabled.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Academic year may only active one. Disable all academic year and try again.","../year");
			}
			else
			{
				$TahunID = $this->uri->segment(3);
				$datalog = array(
								'pk1' => $TahunID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACAYE12-Y",
								'aktifitas' => "Mengakses halaman Academic Year - Enabled $TahunID.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$data = array(
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'NA' => 'N'
							);
				$this->m_year->PTL_update($TahunID,$data);
				echo warning("Your data successfully enabled.","../year");
			}
		}
		
		function ptl_admin_disabled()
		{
			$this->authentification();
			$TahunID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE12-Y",
							'aktifitas' => "Mengakses halaman Academic Year - Admin Disabled $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_year->PTL_update($TahunID,$data);
			$restahun = $this->m_year->PTL_select($TahunID);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to('patrice@esmodjakarta.com');
			$this->email->cc('miha@esmodjakarta.com,anisa@esmodjakarta.com,rossy@esmodjakarta.com');
			$this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('ACADEMIC YEAR NOTIFICATION (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi ".strtoupper($_COOKIE["nama"])."!
				<br/>
				<br/>
				You have <font color='red'<h3><b>CLOSED ($TahunID : $restahun[Nama])</b></h3></font> academic year. You are allowed to have access as manager. This email was sent because you click the STAR button. You can make improvements grades all semester.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data successfully disabled. Email notification will be sent.","../year");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data successfully disabled. Email notification will be sent.","../year");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully disabled.","../year");
				}
			}
		}
		
		function ptl_admin_enabled()
		{
			$this->authentification();
			$TahunID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACAYE12-Y",
							'aktifitas' => "Mengakses halaman Academic Year - Admin Enabled $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'N'
						);
			$this->m_year->PTL_update($TahunID,$data);
			$restahun = $this->m_year->PTL_select($TahunID);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to('patrice@esmodjakarta.com');
			$this->email->cc('miha@esmodjakarta.com,anisa@esmodjakarta.com,rossy@esmodjakarta.com');
			$this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('ACADEMIC YEAR NOTIFICATION (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi ".strtoupper($_COOKIE["nama"])."!
				<br/>
				<br/>
				You have <font color='green'<h3><b>OPENED ($TahunID : $restahun[Nama])</b></h3></font> academic year. You are allowed to have access as manager. This email was sent because you click the STAR button. You can make improvements grades all semester.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data successfully enabled. Email notification will be sent.","../year");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data successfully enabled. Email notification will be sent.","../year");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully enabled.","../year");
				}
			}
		}
	}
?>