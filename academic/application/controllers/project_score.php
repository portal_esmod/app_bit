<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Project_score extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_kurikulum');
			$this->load->model('m_nilai2');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_kur()
		{
			$this->authentification();
			$cekkurikulum = $this->input->post('cekkurikulum');
			if($cekkurikulum != "")
			{
				$this->session->set_userdata('ps_filter_kur',$cekkurikulum);
			}
			else
			{
				$this->session->unset_userdata('ps_filter_kur');
			}
			redirect("project_score");
		}
		
		function ptl_filter_kur1()
		{
			$this->authentification();
			$cekkurikulum1 = $this->input->post('cekkurikulum1');
			if($cekkurikulum1 != "")
			{
				$this->session->set_userdata('ps_filter_kur1',$cekkurikulum1);
			}
			else
			{
				$this->session->unset_userdata('ps_filter_kur1');
			}
			redirect("project_score/ptl_copy");
		}
		
		function ptl_filter_kur2()
		{
			$this->authentification();
			$cekkurikulum2 = $this->input->post('cekkurikulum2');
			if($cekkurikulum2 != "")
			{
				$this->session->set_userdata('ps_filter_kur2',$cekkurikulum2);
			}
			else
			{
				$this->session->unset_userdata('ps_filter_kur2');
			}
			redirect("project_score/ptl_copy");
		}
		
		function ptl_set_check_all1()
		{
			$this->authentification();
			$this->session->set_userdata('ps_set_all1','Y');
			redirect("project_score/ptl_copy");
		}
		
		function ptl_set_uncheck_all1()
		{
			$this->authentification();
			$this->session->unset_userdata('ps_set_all1');
			redirect("project_score/ptl_copy");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('ps_filter_kur');
			$data['rowrecord'] = $this->m_nilai2->PTL_all_spesifik($cekkurikulum);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Score_Project/v_score_project',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$this->load->view('Portal/v_header');
			$this->load->view('Score_Project/v_score_project_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$Lulus = "N";
			if($this->input->post('Lulus') == "Y") { $Lulus = "Y"; }
			$data = array(
						'KurikulumID' => $this->session->userdata('ps_filter_kur'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'NilaiMin' => str_replace(",",".",$this->input->post('NilaiMin')),
						'Lulus' => $Lulus,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_nilai2->PTL_insert($data);
			echo warning("Your data successfully added.","../project_score");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$Nilai2ID = $this->uri->segment(3);
			$result = $this->m_nilai2->PTL_select($Nilai2ID);
			$data['Nilai2ID'] = $result['Nilai2ID'];
			$data['Nama'] = $result['Nama'];
			$data['NilaiMin'] = $result['NilaiMin'];
			$data['Lulus'] = $result['Lulus'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Score_Project/v_score_project_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$Nilai2ID = $this->input->post('Nilai2ID');
			$Lulus = "N";
			if($this->input->post('Lulus') == "Y") { $Lulus = "Y"; }
			$NA = "N";
			if($this->input->post('NA') == "Y") { $NA = "Y"; }
			$data = array(
						'Nama' => strtoupper($this->input->post('Nama')),
						'NilaiMin' => str_replace(",",".",$this->input->post('NilaiMin')),
						'Lulus' => $Lulus,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $NA
						);
			$this->m_nilai2->PTL_update($Nilai2ID,$data);
			echo warning("Your data successfully updated.","../project_score");
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('ps_filter_kur1');
			$data['rowrecord1'] = $this->m_nilai2->PTL_all_spesifik($cekkurikulum);
			$cekkurikulum = $this->session->userdata('ps_filter_kur2');
			$data['rowrecord2'] = $this->m_nilai2->PTL_all_spesifik($cekkurikulum);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Score_Project/v_score_project_copy',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set_copy()
		{
			$this->authentification();
			$total = $this->input->post("total1");
			$nsave = 0;
			$nover = 0;
			$cekkurikulum = $this->session->userdata('ps_filter_kur2');
			if($cekkurikulum == "")
			{
				echo warning("You have not set the copy destination.","../project_score/ptl_copy");
			}
			else
			{
				for($i=1;$i<=$total;$i++)
				{
					$Nilai2ID = $this->input->post("cek$i");
					if($Nilai2ID != "")
					{
						$result = $this->m_nilai2->PTL_select($Nilai2ID);
						$Nama = $result['Nama'];
						$res = $this->m_nilai2->PTL_select_kode_nilai($Nama,$cekkurikulum);
						if($res)
						{
							$nover++;
						}
						else
						{
							$data = array(
										'NilaiID' => $result['NilaiID'],
										'ProdiID' => $result['ProdiID'],
										'KurikulumID' => $cekkurikulum,
										'KonsentrasiID' => $result['KonsentrasiID'],
										'Nama' => $result['Nama'],
										'Bobot' => $result['Bobot'],
										'Lulus' => $result['Lulus'],
										'NilaiMin' => $result['NilaiMin'],
										'NilaiMax' => $result['NilaiMax'],
										'MaxSKS' => $result['MaxSKS'],
										'HitungIPK' => $result['HitungIPK'],
										'Deskripsi' => $result['Deskripsi'],
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu,
										'NA' => $result['NA']
										);
							$this->m_nilai2->PTL_insert($data);
							$nsave++;
						}
					}
				}
				$over = "";
				if($nover > 0)
				{
					$over = " $nover data overlapping.";
				}
				if($nsave == 0)
				{
					echo warning("You don't save any data.$over","../project_score/ptl_copy");
				}
				else
				{
					echo warning("Your data has been saved. $nsave data saved.$over","../project_score/ptl_copy");
				}
			}
		}
	}
?>