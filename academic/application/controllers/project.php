<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Project extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_akun');
			$this->load->model('m_jadwal');
			$this->load->model('m_kurikulum');
			$this->load->model('m_maintenance');
			$this->load->model('m_mk');
			$this->load->model('m_prodi');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_kur()
		{
			$this->authentification();
			$cekkurikulum = $this->input->post('cekkurikulum');
			if($cekkurikulum != "")
			{
				$this->session->set_userdata('project_filter_kur',$cekkurikulum);
			}
			else
			{
				$this->session->unset_userdata('project_filter_kur');
			}
			redirect("project");
		}
		
		function ptl_filter_sub()
		{
			$this->authentification();
			$ceksubjek = $this->input->post('ceksubjek');
			if($ceksubjek != "")
			{
				$this->session->set_userdata('project_filter_sub',$ceksubjek);
			}
			else
			{
				$this->session->unset_userdata('project_filter_sub');
			}
			redirect("project");
		}
		
		function ptl_filter_kur1()
		{
			$this->authentification();
			$cekkurikulum1 = $this->input->post('cekkurikulum1');
			if($cekkurikulum1 != "")
			{
				$this->session->set_userdata('project_filter_kur1',$cekkurikulum1);
			}
			else
			{
				$this->session->unset_userdata('project_filter_kur1');
			}
			redirect("project/ptl_copy");
		}
		
		function ptl_filter_kur2()
		{
			$this->authentification();
			$cekkurikulum2 = $this->input->post('cekkurikulum2');
			if($cekkurikulum2 != "")
			{
				$this->session->set_userdata('project_filter_kur2',$cekkurikulum2);
			}
			else
			{
				$this->session->unset_userdata('project_filter_kur2');
			}
			redirect("project/ptl_copy");
		}
		
		function ptl_filter_sub1()
		{
			$this->authentification();
			$ceksubjek1 = $this->input->post('ceksubjek1');
			if($ceksubjek1 != "")
			{
				$this->session->set_userdata('project_filter_sub1',$ceksubjek1);
			}
			else
			{
				$this->session->unset_userdata('project_filter_sub1');
			}
			redirect("project/ptl_copy");
		}
		
		function ptl_filter_sub2()
		{
			$this->authentification();
			$ceksubjek2 = $this->input->post('ceksubjek2');
			if($ceksubjek2 != "")
			{
				$this->session->set_userdata('project_filter_sub2',$ceksubjek2);
			}
			else
			{
				$this->session->unset_userdata('project_filter_sub2');
			}
			redirect("project/ptl_copy");
		}
		
		function ptl_set_check_all1()
		{
			$this->authentification();
			$this->session->set_userdata('project_set_all1','Y');
			redirect("project/ptl_copy");
		}
		
		function ptl_set_uncheck_all1()
		{
			$this->authentification();
			$this->session->unset_userdata('project_set_all1');
			redirect("project/ptl_copy");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('project_filter_kur');
			$ceksubjek = $this->session->userdata('project_filter_sub');
			$data['rowrecord'] = $this->m_mk->PTL_all_spesifik($cekkurikulum,$ceksubjek);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Project/v_project',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$KurikulumID = $this->uri->segment(3);
			$data['KurikulumID'] = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$data['SubjekID'] = $this->uri->segment(4);
			$data['kurikulum'] = "";
			$reskur = $this->m_kurikulum->PTL_select($KurikulumID);
			if($reskur)
			{
				$data['kurikulum'] = $reskur['Nama'];
			}
			$data['subjek'] = "";
			$ressub = $this->m_subjek->PTL_select($SubjekID);
			if($ressub)
			{
				$data['subjek'] = $ressub['Nama'];
			}
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$data['rowspc'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Project/v_project_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$MKKode = strtoupper($this->input->post('MKKode'));
			$KurikulumID = $this->input->post('KurikulumID');
			$SubjekID = $this->input->post('SubjekID');
			$result = $this->m_mk->PTL_select_kode($MKKode,$KurikulumID,$SubjekID);
			if($result)
			{
				echo warning("The code has been used '".$result["Nama"]."', replace it with any other code.","../project/ptl_form/$KurikulumID/$SubjekID");
			}
			else
			{
				$total = $this->input->post('total');
				$prodi = "";
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("ProdiID$i") != "")
					{
						$prodi .= $this->input->post("ProdiID$i").".";
					}
				}
				$totalb = $this->input->post('totalb');
				$spc = "";
				for($i=1;$i<=$totalb;$i++)
				{
					if($this->input->post("SpesialisasiID$i") != "")
					{
						$spc .= $this->input->post("SpesialisasiID$i").".";
					}
				}
				$optional = "N";
				if($this->input->post('Optional') == "Y")
				{
					$optional = "Y";
				}
				$data = array(
							'MKKode' => $MKKode,
							'KurikulumID' => $KurikulumID,
							'SubjekID' => $SubjekID,
							'SpesialisasiID' => $spc,
							'Nama' => $this->input->post('Nama'),
							'NoUrut' => $this->input->post('NoUrut'),
							'Singkatan' => $this->input->post('Singkatan'),
							'Keterangan' => $this->input->post('Keterangan'),
							'ProdiID' => $prodi,
							'Optional' => $optional,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_mk->PTL_insert($data);
				$pesan = "<font color='green'><b>created</b></font>";
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$NamaSub = "";
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$rowkurikulum = $this->m_year->PTL_all_active_select_kurikulum($KurikulumID);
				$email = "";
				if($rowkurikulum)
				{
					foreach($rowkurikulum as $rk)
					{
						$TahunID = $rk->TahunID;
						$rowproj = $this->m_jadwal->PTL_all_select_project($TahunID,$SubjekID);
						if($rowproj)
						{
							foreach($rowproj as $rp)
							{
								$DosenID = $rp->DosenID;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID2;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID3;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID4;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID5;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID6;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
							}
						}
					}
				}
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				$EmailAkun = "";
				if($resakun)
				{
					$EmailAkun = $resakun['email'];
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$email");
				// $this->email->cc("$EmailAkun");
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('NEW PROJECT (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					Dear <b>$NamaSub</b> teachers,
					<br/>
					<br/>
					Your project with name <b>".$this->input->post('Nama')."</b> was $pesan by <font color='blue'><b>".$_COOKIE["nama"]."</b></font>
					<br/>
					<br/>
					Please check your Academic Portal Application.
					<br/>
					<br/>
					If you want to change your project, please follow this steps:
					<br/>
					1. Go to your subject in attendance menu.
					<br/>
					2. Edit project with click the green button.
					<br/>
					3. Select your project for your schedule.
					<br/>
					4. Click update button.
					<br/>
					5. Go to students attendance.
					<br/>
					6. Update your students attendance.
					<br/>
					7. Click save attendance button.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Your data successfully added.","../project");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Your data successfully added.","../project");
					}
					else
					{
						echo warning("Email server is not active. Your data successfully added.","../project");
					}
				}
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$MKID = $this->uri->segment(3);
			$result = $this->m_mk->PTL_select($MKID);
			$data['MKID'] = $result['MKID'];
			$data['MKKode'] = $result['MKKode'];
			$data['SpesialisasiID'] = $result['SpesialisasiID'];
			$data['Nama'] = $result['Nama'];
			$data['NoUrut'] = $result['NoUrut'];
			$data['Singkatan'] = $result['Singkatan'];
			$data['Keterangan'] = $result['Keterangan'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['Optional'] = $result['Optional'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['KurikulumID'] = $result['KurikulumID'];
			$data['SubjekID'] = $result['SubjekID'];
			$KurikulumID = $result['KurikulumID'];
			$SubjekID = $result['SubjekID'];
			$reskur = $this->m_kurikulum->PTL_select($KurikulumID);
			if($reskur)
			{
				$data['kurikulum'] = $reskur['Nama'];
			}
			$data['subjek'] = "";
			$ressub = $this->m_subjek->PTL_select($SubjekID);
			if($reskur)
			{
				$data['subjek'] = $ressub['Nama'];
			}
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$data['rowspc'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Project/v_project_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$MKID = $this->input->post('MKID');
			$KurikulumID = $this->input->post('KurikulumID');
			$SubjekID = $this->input->post('SubjekID');
			$MKKode = strtoupper($this->input->post('MKKode'));
			$result = $this->m_mk->PTL_select_kode($MKKode,$KurikulumID,$SubjekID);
			if(($result) AND ($MKKode != $result["MKKode"]))
			{
				echo warning("The code has been used '".$result["Nama"]."', replace it with any other code.","../project/ptl_edit/$MKKode");
			}
			else
			{
				$total = $this->input->post('total');
				$prodi = "";
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("ProdiID$i") != "")
					{
						$prodi .= $this->input->post("ProdiID$i").".";
					}
				}
				$totalb = $this->input->post('totalb');
				$spc = "";
				for($i=1;$i<=$totalb;$i++)
				{
					if($this->input->post("SpesialisasiID$i") != "")
					{
						$spc .= $this->input->post("SpesialisasiID$i").".";
					}
				}
				$optional = "N";
				if($this->input->post('Optional') == "Y")
				{
					$optional = "Y";
				}
				$na = "N";
				if($this->input->post('NA') == "Y")
				{
					$na = "Y";
				}
				$data = array(
							'MKKode' => $MKKode,
							'SpesialisasiID' => $spc,
							'Nama' => $this->input->post('Nama'),
							'NoUrut' => $this->input->post('NoUrut'),
							'Singkatan' => $this->input->post('Singkatan'),
							'Keterangan' => $this->input->post('Keterangan'),
							'ProdiID' => $prodi,
							'Optional' => $optional,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'NA' => $na
							);
				$this->m_mk->PTL_update($MKID,$data);
				$pesan = "";
				$headerpesan = "";
				if($na == "Y")
				{
					$pesan = "<font color='red'><b>deleted</b></font>";
					$headerpesan = "DELETED";
				}
				else
				{
					$pesan = "<font color='green'><b>updated</b></font>";
					$headerpesan = "UPDATED";
				}
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$NamaSub = "";
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$rowkurikulum = $this->m_year->PTL_all_active_select_kurikulum($KurikulumID);
				$email = "";
				if($rowkurikulum)
				{
					foreach($rowkurikulum as $rk)
					{
						$TahunID = $rk->TahunID;
						$rowproj = $this->m_jadwal->PTL_all_select_project($TahunID,$SubjekID);
						if($rowproj)
						{
							foreach($rowproj as $rp)
							{
								$DosenID = $rp->DosenID;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID2;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID3;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID4;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID5;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
								$DosenID = $rp->DosenID6;
								$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
								if($resdosen)
								{
									$email .= $resdosen['email'].',';
								}
							}
						}
					}
				}
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				$EmailAkun = "";
				if($resakun)
				{
					$EmailAkun = $resakun['email'];
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$email");
				// $this->email->cc("$EmailAkun");
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject("PROJECT $headerpesan (NO REPLY)");
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					Dear <b>$NamaSub</b> teachers,
					<br/>
					<br/>
					Your project with name <b>".$this->input->post('Nama')."</b> was $pesan by <font color='blue'><b>".$_COOKIE["nama"]."</b></font>
					<br/>
					<br/>
					Please check your Academic Portal Application.
					<br/>
					<br/>
					If you want to change your project, please follow this steps:
					<br/>
					1. Go to your subject in attendance menu.
					<br/>
					2. Edit project with click the green button.
					<br/>
					3. Select your project for your schedule.
					<br/>
					4. Click update button.
					<br/>
					5. Go to students attendance.
					<br/>
					6. Update your students attendance.
					<br/>
					7. Click save attendance button.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Your data successfully updated.","../project");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Your data successfully updated.","../project");
					}
					else
					{
						echo warning("Email server is not active. Your data successfully updated.","../project");
					}
				}
			}
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('project_filter_kur1');
			$ceksubjek = $this->session->userdata('project_filter_sub1');
			$data['rowsubjek1'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$data['rowrecord1'] = $this->m_mk->PTL_all_spesifik($cekkurikulum,$ceksubjek);
			$cekkurikulum = $this->session->userdata('project_filter_kur2');
			$ceksubjek = $this->session->userdata('project_filter_sub2');
			$data['rowsubjek2'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$data['rowrecord2'] = $this->m_mk->PTL_all_spesifik($cekkurikulum,$ceksubjek);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Project/v_project_copy',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set_copy()
		{
			$this->authentification();
			$total = $this->input->post("total1");
			$nsave = 0;
			$nover = 0;
			$cekkurikulum = $this->session->userdata('project_filter_kur2');
			if($cekkurikulum == "")
			{
				echo warning("You have not set the copy destination (CURRICULUM).","../project/ptl_copy");
			}
			else
			{
				$ceksubjek = $this->session->userdata('project_filter_sub2');
				if($ceksubjek == "")
				{
					echo warning("You have not set the copy destination (SUBJECT).","../project/ptl_copy");
				}
				else
				{
					for($i=1;$i<=$total;$i++)
					{
						$MKID = $this->input->post("cek$i");
						if($MKID != "")
						{
							$result = $this->m_mk->PTL_select($MKID);
							$MKKode = $result['MKKode'];
							$KurikulumID = $cekkurikulum;
							$SubjekID = $ceksubjek;
							$res = $this->m_mk->PTL_select_kode($MKKode,$KurikulumID,$SubjekID);
							if($res)
							{
								$nover++;
							}
							else
							{
								$data = array(
											'MKKode' => $MKKode,
											'KurikulumID' => $KurikulumID,
											'SubjekID' => $SubjekID,
											'SpesialisasiID' => $result['SpesialisasiID'],
											'Nama' => $result['Nama'],
											'NoUrut' => $result['NoUrut'],
											'Singkatan' => $result['Singkatan'],
											'Keterangan' => $result['Keterangan'],
											'ProdiID' => $result['ProdiID'],
											'Optional' => $result['Optional'],
											'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_buat' => $this->waktu
											);
								$this->m_mk->PTL_insert($data);
								$nsave++;
							}
						}
					}
					$over = "";
					if($nover > 0)
					{
						$over = " $nover data overlapping.";
					}
					if($nsave == 0)
					{
						echo warning("You don't save any data.$over","../project/ptl_copy");
					}
					else
					{
						echo warning("Your data has been saved. $nsave data saved.$over","../project/ptl_copy");
					}
				}
			}
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$MKID = $this->uri->segment(3);
			$result = $this->m_mk->PTL_delete($MKID);
			echo warning("Your data successfully deleted.","../project/ptl_copy");
		}
	}
?>