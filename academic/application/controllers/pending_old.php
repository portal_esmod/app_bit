<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Pending_old extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_pending_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('academic_filter_pending_old_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('academic_filter_pending_old_jur','');
			}
			redirect("pending_old");
		}
		
		function ptl_filter_pending_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('academic_filter_pending_old_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('academic_filter_pending_old_prodi','');
			}
			redirect("pending_old");
		}
		
		function ptl_filter_pending_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('academic_filter_pending_old_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('academic_filter_pending_old_tahun');
			}
			redirect("pending_old");
		}
		
		function ptl_filter_pending_tahun_aktif()
		{
			$this->authentification();
			$cektahunaktif = $this->input->post('cektahunaktif');
			if($cektahunaktif != "")
			{
				$this->session->set_userdata('academic_filter_pending_old_tahun_aktif',$cektahunaktif);
			}
			else
			{
				$this->session->unset_userdata('academic_filter_pending_old_tahun_aktif');
			}
			redirect("pending_old");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$cekjurusan = $this->session->userdata('academic_filter_pending_old_jur');
			$cekprodi = $this->session->userdata('academic_filter_pending_old_prodi');
			$cektahun = $this->session->userdata('academic_filter_pending_old_tahun');
			$cektahunaktif = $this->session->userdata('academic_filter_pending_old_tahun_aktif');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik_aktif($cekjurusan);
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahunaktif'] = $this->m_khs->PTL_distinct_tahun($cekjurusan,$cekprodi,$cektahun);
			$data['rowrecord'] = $this->m_khs->PTL_all_distinct_students($cektahunaktif,$cektahun);
			$this->load->view('Portal/v_header');
			$this->load->view('Pending_old/v_pending_old',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>