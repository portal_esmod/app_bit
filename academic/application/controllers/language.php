<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Language extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aktifitas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur1()
		{
			$this->authentification();
			$cekjurusan1 = $this->input->post('cekjurusan1');
			$datalog = array(
							'pk1' => $cekjurusan1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-01",
							'aktifitas' => "Filter halaman Language - Program 1: $cekjurusan1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan1 != "")
			{
				$this->session->set_userdata('lang_filter_jur1',$cekjurusan1);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_jur1');
			}
			redirect("language");
		}
		
		function ptl_filter_jur2()
		{
			$this->authentification();
			$cekjurusan2 = $this->input->post('cekjurusan2');
			$datalog = array(
							'pk1' => $cekjurusan2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-02",
							'aktifitas' => "Filter halaman Language - Program 2: $cekjurusan2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan2 != "")
			{
				$this->session->set_userdata('lang_filter_jur2',$cekjurusan2);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_jur2');
			}
			redirect("language");
		}
		
		function ptl_filter_tahun1()
		{
			$this->authentification();
			$cektahun1 = $this->input->post('cektahun1');
			$datalog = array(
							'pk1' => $cektahun1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-03",
							'aktifitas' => "Filter halaman Language - Year 1: $cektahun1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun1 != "")
			{
				$this->session->set_userdata('lang_filter_tahun1',$cektahun1);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_tahun1');
			}
			redirect("language");
		}
		
		function ptl_filter_tahun2()
		{
			$this->authentification();
			$cektahun2 = $this->input->post('cektahun2');
			$datalog = array(
							'pk1' => $cektahun2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-04",
							'aktifitas' => "Filter halaman Language - Year 2: $cektahun2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun2 != "")
			{
				$this->session->set_userdata('lang_filter_tahun2',$cektahun2);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_tahun2');
			}
			redirect("language");
		}
		
		function ptl_filter_prodi1()
		{
			$this->authentification();
			$cekprodi1 = $this->input->post('cekprodi1');
			$datalog = array(
							'pk1' => $cekprodi1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-05",
							'aktifitas' => "Filter halaman Language - Prodi 1: $cekprodi1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi1 != "")
			{
				$this->session->set_userdata('lang_filter_prodi1',$cekprodi1);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_prodi1');
			}
			redirect("language");
		}
		
		function ptl_filter_prodi2()
		{
			$this->authentification();
			$cekprodi2 = $this->input->post('cekprodi2');
			$datalog = array(
							'pk1' => $cekprodi2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-06",
							'aktifitas' => "Filter halaman Language - Prodi 2: $cekprodi2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi2 != "")
			{
				$this->session->set_userdata('lang_filter_prodi2',$cekprodi2);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_prodi2');
			}
			redirect("language");
		}
		
		function ptl_filter_lang()
		{
			$this->authentification();
			$ceklang = $this->input->post('ceklang');
			$datalog = array(
							'pk1' => $ceklang,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-07",
							'aktifitas' => "Filter halaman Language - Language: $ceklang.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceklang != "")
			{
				$this->session->set_userdata('lang_filter_lang',$ceklang);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_lang');
			}
			redirect("language");
		}
		
		function ptl_filter_ke1()
		{
			$this->authentification();
			$cekke1 = $this->input->post('cekke1');
			$datalog = array(
							'pk1' => $cekke1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-08",
							'aktifitas' => "Filter halaman Language - Semester 1: $cekke1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekke1 != "")
			{
				$this->session->set_userdata('lang_filter_ke1',$cekke1);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_ke1');
			}
			redirect("language");
		}
		
		function ptl_filter_ke2()
		{
			$this->authentification();
			$cekke2 = $this->input->post('cekke2');
			$datalog = array(
							'pk1' => $cekke2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-09",
							'aktifitas' => "Filter halaman Language - Semester 2: $cekke2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekke2 != "")
			{
				$this->session->set_userdata('lang_filter_ke2',$cekke2);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_ke2');
			}
			redirect("language");
		}
		
		function ptl_set_check_all1()
		{
			$this->authentification();
			$this->session->set_userdata('spc_set_all1','Y');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-10",
							'aktifitas' => "Filter halaman Language - Check All 1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("language");
		}
		
		function ptl_set_check_all2()
		{
			$this->authentification();
			$this->session->set_userdata('spc_set_all2','Y');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-11",
							'aktifitas' => "Filter halaman Language - Check All 2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("language");
		}
		
		function ptl_set_uncheck_all1()
		{
			$this->authentification();
			$this->session->unset_userdata('spc_set_all1');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-12",
							'aktifitas' => "Filter halaman Language - Uncheck All 1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("language");
		}
		
		function ptl_set_uncheck_all2()
		{
			$this->authentification();
			$this->session->unset_userdata('spc_set_all2');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-13",
							'aktifitas' => "Filter halaman Language - Uncheck All 2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("language");
		}
		
		function ptl_filter_status1()
		{
			$this->authentification();
			$cekstatus1 = $this->input->post('cekstatus1');
			$datalog = array(
							'pk1' => $cekstatus1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-14",
							'aktifitas' => "Filter halaman Language - Status 1: $cekstatus1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekstatus1 != "")
			{
				$this->session->set_userdata('lang_filter_status1',$cekstatus1);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_status1');
			}
			redirect("language");
		}
		
		function ptl_filter_status2()
		{
			$this->authentification();
			$cekstatus2 = $this->input->post('cekstatus2');
			$datalog = array(
							'pk1' => $cekstatus2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU01-15",
							'aktifitas' => "Filter halaman Language - Status 2: $cekstatus2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekstatus2 != "")
			{
				$this->session->set_userdata('lang_filter_status2',$cekstatus2);
			}
			else
			{
				$this->session->unset_userdata('lang_filter_status2');
			}
			redirect("language");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','language');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU02",
							'aktifitas' => "Mengakses halaman Language.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cektahun = $this->session->userdata('lang_filter_tahun1');
			$cekjurusan = $this->session->userdata('lang_filter_jur1');
			$data['rowprodi1'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi1'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi1'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi1'] = $this->m_kursussingkat->PTL_all();
			}
			$cekprodi = $this->session->userdata('lang_filter_prodi1');
			$ceklang = $this->session->userdata('lang_filter_lang');
			$cekke = $this->session->userdata('lang_filter_ke1');
			$data['rowtahun1'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord1'] = $this->m_khs->PTL_all_spesifik_lang($cekjurusan,$cektahun,$cekprodi,$ceklang,$cekke);
			$cektahun = $this->session->userdata('lang_filter_tahun2');
			$cekjurusan = $this->session->userdata('lang_filter_jur2');
			$data['rowprodi2'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi2'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi2'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi2'] = $this->m_kursussingkat->PTL_all();
			}
			$cekprodi = $this->session->userdata('lang_filter_prodi2');
			$cekke = $this->session->userdata('lang_filter_ke2');
			$data['rowtahun2'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord2'] = $this->m_khs->PTL_all_spesifik_no_lang($cekjurusan,$cektahun,$cekprodi,$cekke);
			$data['rowstatus'] = $this->m_status->PTL_all();
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Language/v_language',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set()
		{
			$this->authentification();
			$total = $this->input->post("total2") - 1;
			$languageID = $this->session->userdata('lang_filter_lang');
			$PROGRAM = $this->session->userdata('lang_filter_jur1');
			$PRODI = $this->session->userdata('lang_filter_prodi1');
			$SEMESTER = $this->session->userdata('lang_filter_ke1');
			$STATUS = $this->session->userdata('lang_filter_status1');
			$datalog = array(
							'pk1' => $languageID,
							'pk2' => $PROGRAM,
							'pk3' => $PRODI,
							'pk4' => $SEMESTER,
							'pk5' => $STATUS,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "LANGU03",
							'aktifitas' => "Mengakses halaman Language - Set.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$pesan = "";
			if($languageID == "")
			{
				$pesan .= "You do not choose a language.".'\n\n';
			}
			if($PROGRAM == "")
			{
				$pesan .= "You do not choose a program.".'\n\n';
			}
			if($PRODI == "")
			{
				$pesan .= "You do not choose a prodi.".'\n\n';
			}
			if($SEMESTER == "")
			{
				$pesan .= "You do not choose a semester.".'\n\n';
			}
			if($STATUS == "")
			{
				$pesan .= "You do not choose a status.".'\n\n';
			}
			if($STATUS != "A")
			{
				$pesan .= "Student status must be active.".'\n\n';
			}
			if($pesan != "")
			{
				echo warning($pesan,"../language");
			}
			else
			{
				$nsave = 0;
				for($i=1;$i<=$total;$i++)
				{
					$KHSID = $this->input->post("cek$i");
					if($KHSID != "")
					{
						$data = array(
									'languageID' => $languageID,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_khs->PTL_update($KHSID,$data);
						$res = $this->m_khs->PTL_select($KHSID);
						if($res)
						{
							if($languageID == "ENG")
							{
								$lang = "FRENCH";
							}
							if($languageID == "FRE")
							{
								$lang = "ENGLISH";
							}
							$MhswID = $res['MhswID'];
							$rowrecord = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
							if($rowrecord)
							{
								foreach($rowrecord as $row)
								{
									$SubjekID = $row->SubjekID;
									$resub = $this->m_subjek->PTL_select($SubjekID);
									if($resub)
									{
										if(strpos(strtoupper($resub['Nama']),$lang) !== false)
										{
											$KRSID = $row->KRSID;
											$this->m_krs->PTL_delete_krs($KRSID);
										}
									}
								}
							}
						}
						$nsave++;
					}
				}
				if($nsave == 0)
				{
					echo warning("You don't save any data.","../language");
				}
				else
				{
					echo warning("Your data has been saved.","../language");
				}
			}
		}
		
		function ptl_unset()
		{
			$this->authentification();
			$total = $this->input->post("total1") - 1;
			$nsave = 0;
			for($i=1;$i<=$total;$i++)
			{
				$KHSID = $this->input->post("cek$i");
				if($KHSID != "")
				{
					$data = array(
								'languageID' => "",
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$nsave++;
				}
			}
			if($nsave == 0)
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "LANGU04-N",
								'aktifitas' => "Mengakses halaman Language - Unset.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You don't save any data.","../language");
			}
			else
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "LANGU04-Y",
								'aktifitas' => "Mengakses halaman Language - Unset.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Your data has been saved.","../language");
			}
		}
	}
?>