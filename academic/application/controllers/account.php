<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Account extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$this->load->model('m_maintenance');
			$this->load->model('m_rekening');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_rekening->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Rekening/v_rekening',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$this->load->view('Portal/v_header');
			$this->load->view('Rekening/v_rekening_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('Def') == "")
			{
				$Def = "N";
			}
			else
			{
				$Def = "Y";
			}
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'RekeningID' => $this->input->post('RekeningID'),
						'Nama' => $this->input->post('Nama'),
						'Bank' => $this->input->post('Bank'),
						'Cabang' => $this->input->post('Cabang'),
						'Def' => $Def,
						'NA' => $na
						);
			$this->m_rekening->PTL_insert($data);
			echo warning("Your data successfully added.","../account");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$RekeningID = $this->uri->segment(3);
			$result = $this->m_rekening->PTL_select($RekeningID);
			$data['RekeningID'] = $result['RekeningID'];
			$data['Nama'] = $result['Nama'];
			$data['Bank'] = $result['Bank'];
			$data['Cabang'] = $result['Cabang'];
			$data['Def'] = $result['Def'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Rekening/v_rekening_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$RekeningID = $this->input->post('RekeningID');
			if($this->input->post('Def') == "")
			{
				$Def = "N";
			}
			else
			{
				$Def = "Y";
			}
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'RekeningID' => $this->input->post('RekeningID'),
						'Nama' => $this->input->post('Nama'),
						'Bank' => $this->input->post('Bank'),
						'Cabang' => $this->input->post('Cabang'),
						'Def' => $Def,
						'NA' => $na
						);
			$this->m_rekening->PTL_update($RekeningID,$data);
			echo warning("Your data successfully updated.","../account");
		}
	}
?>