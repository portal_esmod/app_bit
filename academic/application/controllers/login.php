<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Login extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->kode_verifikasi = gmdate("dis", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('auth');
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_catatan');
			$this->load->model('m_exam');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_jadwal');
			$this->load->model('m_krs2');
			$this->load->model('m_log');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_mk');
			$this->load->model('m_notifikasi');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_rating');
			$this->load->model('m_rating_question');
			$this->load->model('m_rating_question_grup');
			$this->load->model('m_subjek');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			@$is_login_academic = $this->session->userdata('is_login_academic');
			if($is_login_academic == "notlogged")
			{
				$this->session->unset_userdata('is_login_academic');
				setcookie("is_login_academic", "", time()+1);
				setcookie("is_login_academic", "", time()+1, "/");
				echo warning('You are not logged in.','../login');
			}
			@$cis_login_app = $_COOKIE["is_login_app"];
			@$cis_login_academic = $_COOKIE["is_login_academic"];
			if(($cis_login_app == "logged") AND ($cis_login_academic == "logged"))
			{
				redirect('login/ptl_home');
			}
			else
			{
				if($cis_login_academic == "logged")
				{
					echo warning('Sorry, it looks like you are already logged ...','../login/ptl_home');
				}
			}
			$this->load->view('v_login');
		}
		
		function ptl_do_login()
		{
			$this->authentification();
			$remember = $this->input->post('remember');
			if($remember == "7")
			{
				$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
				$recaptcha = $this->input->post('g-recaptcha-response');
				$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
				$response = @file_get_contents($api_url);
				$datacaptcha = json_decode($response, true);
				if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
				{
					$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
					if ($this->form_validation->run() == FALSE)
					{
						$this->index();
					}
					else
					{
						$id_akun = $this->input->post('username');
						$NamaAkun = "Unknown ($id_akun)";
						$email = "";
						$AkunNa = "Y";
						$AksesNa = "Y";
						$resakun = $this->m_akun->PTL_select($id_akun);
						if($resakun)
						{
							$NamaAkun = $resakun['nama'];
							$email = $resakun['email'];
							$AkunNa = $resakun['na'];
						}
						$resakses = $this->m_akses->PTL_select_akun($id_akun);
						if($resakses)
						{
							$AksesNa = $resakses['na'];
						}
						if(($AkunNa == "N") AND ($AkunNa == "N"))
						{
							$login = array(
										'username'=>$this->input->post('username'),
										'password'=>$this->input->post('password')
										);
							$return = $this->auth->do_login_remember($login);
							$h = "-7";
							$hm = $h * 60;
							$ms = $hm * 60;
							$ip_client = $this->log->getIpAdress();
							$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$os_client = $this->log->getOs();
							$browser_client = $this->log->getBrowser();
							$perangkat_client = $this->log->getPerangkat();
							$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
							$tanggal = gmdate("Y-m-d", time()-($ms));
							$jam = gmdate("H:i:s", time()-($ms));
							$id_akses = from_session('id_akses');
							if($return)
							{
								$word = explode("-",$tanggal);
								$dataTime = mktime(0,0,0,$word[1],$word[2]+7,$word[0]);
								$tanggal_pengingat = date("Y-m-d",$dataTime)." ".$jam;
								$data = array(
											'tanggal_pengingat' => $tanggal_pengingat,
											'verifikasi' => 'N',
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'last_login' => $waktu
											);
								$this->m_akses->PTL_update($id_akses,$data);
								
								$datadir = array(
											'id_akun' => from_session('id_akun'),
											'id_akses' => $id_akses,
											'id_cabang' => from_session('id_cabang'),
											'in' => $this->input->post('username'),
											'nama' => from_session('nama'),
											'divisi' => from_session('divisi'),
											'jabatan' => from_session('jabatan'),
											'akses' => from_session('akses'),
											'aplikasi' => 'ACADEMIC',
											'tanggal' => $tanggal,
											'jam' => $jam,
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'status' => 'LOGIN'
											);
								$this->m_log->PTL_input($datadir);
								
								$username = $this->input->post('username');
								$password = $this->input->post('password');
								$query = $this->m_akses->PTL_select_apl($username,$password);
								foreach($query->result() as $row)
								{
									$aplikasi = $row->aplikasi;
								}
								$wordd = explode("_", @$aplikasi);
								for($i=0;$i<20;$i++)
								{
									if(@$wordd[$i] == "ac"){$ac = "logged";}
									if(@$wordd[$i] == "ad"){$ad = "logged";}
									if(@$wordd[$i] == "al"){$al = "logged";}
									if(@$wordd[$i] == "ap"){$ap = "logged";}
									if(@$wordd[$i] == "as"){$as = "logged";}
									if(@$wordd[$i] == "at"){$at = "logged";}
									if(@$wordd[$i] == "bs"){$bs = "logged";}
									if(@$wordd[$i] == "ca"){$ca = "logged";}
									if(@$wordd[$i] == "cr"){$cr = "logged";}
									if(@$wordd[$i] == "dv"){$dv = "logged";}
									if(@$wordd[$i] == "fb"){$fb = "logged";}
									if(@$wordd[$i] == "fn"){$fn = "logged";}
									if(@$wordd[$i] == "ft"){$ft = "logged";}
									if(@$wordd[$i] == "hd"){$hd = "logged";}
									if(@$wordd[$i] == "hr"){$hr = "logged";}
									if(@$wordd[$i] == "jb"){$jb = "logged";}
									if(@$wordd[$i] == "lb"){$lb = "logged";}
									if(@$wordd[$i] == "lm"){$lm = "logged";}
									if(@$wordd[$i] == "ml"){$ml = "logged";}
									if(@$wordd[$i] == "qr"){$qr = "logged";}
								}
								if(@$ac == "logged")
								{
									$ip_client = $this->log->getIpAdress();
									$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
									$os_client = $this->log->getOs();
									$browser_client = $this->log->getBrowser();
									$perangkat_client = $this->log->getPerangkat();
									
									$this->load->library('email');
									$config = array();
									$config['charset'] = 'utf-8';
									$config['useragent'] = 'Codeigniter';
									$config['protocol']= "smtp";
									$config['mailtype']= "html";
									$config['smtp_host']= "mail.esmodjakarta.com";
									$config['smtp_port']= "25";
									$config['smtp_timeout']= "5";
									$config['smtp_user']= "no-reply@esmodjakarta.com";
									$config['smtp_pass']= "noreplyesmod";
									$config['crlf']="\r\n"; 
									$config['newline']="\r\n"; 
									$config['wordwrap'] = TRUE;
									$this->email->initialize($config);
									$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
									$this->email->to("$email");
									$this->email->bcc('lendra.permana@gmail.com');
									$this->email->subject('ACADEMIC - REMEMBER LOGGED IN FOR 7 DAYS (NO REPLY)');
									$this->email->message("
										<center>
											<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
											<font color='red'><h2>ACADEMIC</h2></font>
										</center>
										<br/>
										Dear <b>".strtoupper($NamaAkun)."</b>,
										<br/>
										<br/>
										This email want to inform you that you have logged in for the next 7 days in Academic Application.
										<br/>
										Contact your administrator immediately if you do not perform this action.
										<br/>
										<br/>
										<br/>
										Thanks,
										<br/>
										<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
										<center>
											<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
											Ip: $ip_client
											<br/>
											Hostname: $hostname_client
											<br/>
											OS: $os_client
											<br/>
											Browser: $browser_client
											<br/>
											Devices: $perangkat_client
										</center>
									");
									if($this->email->send())
									{
										echo warning('Welcome '.from_session('nama').'\n'.
												'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
												'Login Now  :  '.$waktu.'\n'.
												'Last Online  :  '.from_session('in').'\n'.
												'Last Logout :  '.from_session('out'),'../login/ptl_home');
									}
									else
									{
										echo warning('Welcome '.from_session('nama').'\n'.
												'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
												'Login Now  :  '.$waktu.'\n'.
												'Last Online  :  '.from_session('in').'\n'.
												'Last Logout :  '.from_session('out').' Code: ESNA.','../login/ptl_home');
									}
								}
								else
								{
									echo warning('Sorry, you do not have access to this application ...','../login/ptl_logout');
								}
							}
							else
							{
								$datadir = array(
											'id_akun' => "",
											'id_akses' => @$id_akses,
											'id_cabang' => "",
											'in' => $this->input->post('username'),
											'aplikasi' => 'ACADEMIC',
											'tanggal' => $tanggal,
											'jam' => $jam,
											'ip_client' => $ip_client,
											'hostname_client' => $hostname_client,
											'os_client' => $os_client,
											'browser_client' => $browser_client,
											'perangkat_client' => $perangkat_client,
											'status' => 'LOGIN FAILED'
											);
								$this->m_log->PTL_input($datadir);
								echo warning('Sorry, username or password you entered is incorrect ...','../login');
							}
						}
						else
						{
							$ip_client = $this->log->getIpAdress();
							$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$os_client = $this->log->getOs();
							$browser_client = $this->log->getBrowser();
							$perangkat_client = $this->log->getPerangkat();
							
							$this->load->library('email');
							$config = array();
							$config['charset'] = 'utf-8';
							$config['useragent'] = 'Codeigniter';
							$config['protocol']= "smtp";
							$config['mailtype']= "html";
							$config['smtp_host']= "mail.esmodjakarta.com";
							$config['smtp_port']= "25";
							$config['smtp_timeout']= "5";
							$config['smtp_user']= "no-reply@esmodjakarta.com";
							$config['smtp_pass']= "noreplyesmod";
							$config['crlf']="\r\n"; 
							$config['newline']="\r\n"; 
							$config['wordwrap'] = TRUE;
							$this->email->initialize($config);
							$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
							$this->email->to('wprasasti@esmodjakarta.com');
							$this->email->bcc('lendra.permana@gmail.com');
							$this->email->subject('FAILED LOGIN ATTEMPTS (NO REPLY)');
							$this->email->message("
								<center>
									<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
									<font color='red'><h2>ACADEMIC</h2></font>
								</center>
								<br/>
								<font color='red'><b>".strtoupper($NamaAkun)."</b></font> has trying to login academic application.
								<br/>
								<br/>
								If you receive this email is more than one, then the user is trying to attack your academic application.
								<br/>
								<br/>
								Data :
								<br/>
								<table>
									<tr>
										<td>Username Input</td>
										<td>:</td>
										<td>".$this->input->post('username')."</td>
									</tr>
									<tr>
										<td>Password Input</td>
										<td>:</td>
										<td>".$this->input->post('password')."</td>
									</tr>
								</table>
								<br/>
								<br/>
								<br/>
								Thanks,
								<br/>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
								<center>
									<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
									Ip: $ip_client
									<br/>
									Hostname: $hostname_client
									<br/>
									OS: $os_client
									<br/>
									Browser: $browser_client
									<br/>
									Devices: $perangkat_client
								</center>
							");
							if($this->email->send())
							{
								echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta.','../login');
							}
							else
							{
								echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta. Code: ESNA.','../login');
							}
						}
					}
				}
				else
				{
					echo warning('Please complete the form and captcha.','../login');
				}
			}
			else
			{
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$id_akun = $this->input->post('username');
					$NamaAkun = "Unknown ($id_akun)";
					$AkunNa = "Y";
					$AksesNa = "Y";
					$resakun = $this->m_akun->PTL_select($id_akun);
					if($resakun)
					{
						$NamaAkun = $resakun['nama'];
						$AkunNa = $resakun['na'];
					}
					$resakses = $this->m_akses->PTL_select_akun($id_akun);
					if($resakses)
					{
						$AksesNa = $resakses['na'];
					}
					if(($AkunNa == "N") AND ($AkunNa == "N"))
					{
						$login = array(
									'username'=>$this->input->post('username'),
									'password'=>$this->input->post('password')
									);
						$return = $this->auth->do_login($login);
						$h = "-7";
						$hm = $h * 60;
						$ms = $hm * 60;
						$ip_client = $this->log->getIpAdress();
						$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
						$os_client = $this->log->getOs();
						$browser_client = $this->log->getBrowser();
						$perangkat_client = $this->log->getPerangkat();
						$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
						$tanggal = gmdate("Y-m-d", time()-($ms));
						$jam = gmdate("H:i:s", time()-($ms));
						$id_akses = from_session('id_akses');
						if($return)
						{
							$data = array(
										'tanggal_pengingat' => '',
										'verifikasi' => 'N',
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'last_login' => $waktu
										);
							$this->m_akses->PTL_update($id_akses,$data);
							
							$datadir = array(
										'id_akun' => from_session('id_akun'),
										'id_akses' => $id_akses,
										'id_cabang' => from_session('id_cabang'),
										'in' => $this->input->post('username'),
										'nama' => from_session('nama'),
										'divisi' => from_session('divisi'),
										'jabatan' => from_session('jabatan'),
										'akses' => from_session('akses'),
										'aplikasi' => 'ACADEMIC',
										'tanggal' => $tanggal,
										'jam' => $jam,
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'status' => 'LOGIN'
										);
							$this->m_log->PTL_input($datadir);
							
							$username = $this->input->post('username');
							$password = $this->input->post('password');
							$query = $this->m_akses->PTL_select_apl($username,$password);
							foreach($query->result() as $row)
							{
								$aplikasi = $row->aplikasi;
							}
							$wordd = explode("_", @$aplikasi);
							for($i=0;$i<20;$i++)
							{
								if(@$wordd[$i] == "ac"){$ac = "logged";}
								if(@$wordd[$i] == "ad"){$ad = "logged";}
								if(@$wordd[$i] == "al"){$al = "logged";}
								if(@$wordd[$i] == "ap"){$ap = "logged";}
								if(@$wordd[$i] == "as"){$as = "logged";}
								if(@$wordd[$i] == "at"){$at = "logged";}
								if(@$wordd[$i] == "bs"){$bs = "logged";}
								if(@$wordd[$i] == "ca"){$ca = "logged";}
								if(@$wordd[$i] == "cr"){$cr = "logged";}
								if(@$wordd[$i] == "dv"){$dv = "logged";}
								if(@$wordd[$i] == "fb"){$fb = "logged";}
								if(@$wordd[$i] == "fn"){$fn = "logged";}
								if(@$wordd[$i] == "ft"){$ft = "logged";}
								if(@$wordd[$i] == "hd"){$hd = "logged";}
								if(@$wordd[$i] == "hr"){$hr = "logged";}
								if(@$wordd[$i] == "jb"){$jb = "logged";}
								if(@$wordd[$i] == "lb"){$lb = "logged";}
								if(@$wordd[$i] == "lm"){$lm = "logged";}
								if(@$wordd[$i] == "ml"){$ml = "logged";}
								if(@$wordd[$i] == "qr"){$qr = "logged";}
							}
							if(@$ac == "logged")
							{
								echo warning('Welcome '.from_session('nama').'\n'.
										'Section        :  '.from_session('divisi').' '.from_session('jabatan').'\n'.
										'Login Now  :  '.$waktu.'\n'.
										'Last Online  :  '.from_session('in').'\n'.
										'Last Logout :  '.from_session('out'),'../login/ptl_home');
							}
							else
							{
								echo warning('Sorry, you do not have access to this application ...','../login/ptl_logout');
							}
						}
						else
						{
							$datadir = array(
										'id_akun' => "",
										'id_akses' => @$id_akses,
										'id_cabang' => "",
										'in' => $this->input->post('username'),
										'aplikasi' => 'ACADEMIC',
										'tanggal' => $tanggal,
										'jam' => $jam,
										'ip_client' => $ip_client,
										'hostname_client' => $hostname_client,
										'os_client' => $os_client,
										'browser_client' => $browser_client,
										'perangkat_client' => $perangkat_client,
										'status' => 'LOGIN FAILED'
										);
							$this->m_log->PTL_input($datadir);
							echo warning('Sorry, username or password you entered is incorrect ...','../login');
						}
					}
					else
					{
						$ip_client = $this->log->getIpAdress();
						$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
						$os_client = $this->log->getOs();
						$browser_client = $this->log->getBrowser();
						$perangkat_client = $this->log->getPerangkat();
						
						$this->load->library('email');
						$config = array();
						$config['charset'] = 'utf-8';
						$config['useragent'] = 'Codeigniter';
						$config['protocol']= "smtp";
						$config['mailtype']= "html";
						$config['smtp_host']= "mail.esmodjakarta.com";
						$config['smtp_port']= "25";
						$config['smtp_timeout']= "5";
						$config['smtp_user']= "no-reply@esmodjakarta.com";
						$config['smtp_pass']= "noreplyesmod";
						$config['crlf']="\r\n"; 
						$config['newline']="\r\n"; 
						$config['wordwrap'] = TRUE;
						$this->email->initialize($config);
						$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
						$this->email->to('wprasasti@esmodjakarta.com');
						$this->email->bcc('lendra.permana@gmail.com');
						$this->email->subject('FAILED LOGIN ATTEMPTS (NO REPLY)');
						$this->email->message("
							<center>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
								<font color='red'><h2>ACADEMIC</h2></font>
							</center>
							<br/>
							<font color='red'><b>".strtoupper($NamaAkun)."</b></font> has trying to login academic application.
							<br/>
							<br/>
							If you receive this email is more than one, then the user is trying to attack your academic application.
							<br/>
							<br/>
							Data :
							<br/>
							<table>
								<tr>
									<td>Username Input</td>
									<td>:</td>
									<td>".$this->input->post('username')."</td>
								</tr>
								<tr>
									<td>Password Input</td>
									<td>:</td>
									<td>".$this->input->post('password')."</td>
								</tr>
							</table>
							<br/>
							<br/>
							<br/>
							Thanks,
							<br/>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
							<center>
								<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
								Ip: $ip_client
								<br/>
								Hostname: $hostname_client
								<br/>
								OS: $os_client
								<br/>
								Browser: $browser_client
								<br/>
								Devices: $perangkat_client
							</center>
						");
						if($this->email->send())
						{
							echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta.','../login');
						}
						else
						{
							echo warning('Sorry, you are not registered as an employee of ESMOD Jakarta. Code: ESNA.','../login');
						}
					}
				}
			}
		}
		
		function ptl_forgot()
		{
			$this->load->view('v_forgot');
		}
		
		function ptl_lupa_password()
		{
			$this->authentification();
			$secret_key = '6LeXKk4UAAAAAMwkr7iboX07f84fZESsx4kUTntF';
			$recaptcha = $this->input->post('g-recaptcha-response');
			$api_url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$recaptcha;
			$response = @file_get_contents($api_url);
			$datacaptcha = json_decode($response, true);
			if(($datacaptcha['success']) OR ($_SERVER["HTTP_HOST"] == "localhost"))
			{
				$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
				if($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{
					$h = "-7";
					$hm = $h * 60; 
					$ms = $hm * 60;
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
					$tanggal = gmdate("Y-m-d", time()-($ms));
					$jam = gmdate("H:i:s", time()-($ms));
					$tgl = gmdate("d M Y", time()-($ms));
					$email = $this->input->post('email');
					$return = $this->m_akses->PTL_email($email);
					if($return)
					{
						$datadir = array(
									'id_akun' => $return['id_akun'],
									'id_akses' => $return['id_akses'],
									'id_cabang' => $return['id_cabang'],
									'nama' => $return['nama'],
									'divisi' => $return['divisi'],
									'jabatan' => $return['jabatan'],
									'akses' => $return['akses'],
									'aplikasi' => 'ACADEMIC',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD'
									);
						$this->m_log->PTL_input($datadir);
						
						$datacat = array(
										'jenis' => 'MAINTENANCE',
										'aplikasi' => 'ACADEMIC',
										'dari' => $return['nama'],
										'dr_divisi' => $return['divisi'],
										'dr_jabatan' => $return['jabatan'],
										'pada' => 'LENDRA PERMANA',
										'pd_divisi' => 'IT',
										'pd_jabatan' => 'DEVELOPER',
										'masalah' => 'Forgot Password',
										'solusi' => 'Reset Password in Database',
										'tanggal_posting' => $waktu,
										'tanggal_done' => $waktu,
										'status' => 'DONE'
										);
						$this->m_catatan->PTL_input($datacat);
						
						$id_akses = $return['id_akses'];
						$pass = gmdate("is", time()-($ms));
						$data_reset = array(
										'password' => $pass
										);
						$this->m_akses->PTL_reset($id_akses,$data_reset);
						
						$id_akun = $return['id_akun'];
						$nama = $return['nama'];
						$resakun = $this->m_akun->PTL_select($id_akun);
						$handphone = "";
						if($resakun)
						{
							$handphone = $resakun['handphone'];
						}
						if($handphone != "")
						{
							if(substr($handphone,0,2) == "62")
							{
								$prov = "0".substr($handphone,2,3);
							}
							else
							{
								$prov = substr($handphone,0,4);
							}
							if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
							{
								$data = array(
											'id_user' => $id_akun,
											'nama' => $nama,
											'aplikasi' => 'Academic',
											'judul_sms' => 'Pengiriman Untuk Lupa Password',
											'nomor_hp' => $handphone,
											'pesan' => "ESMOD (NO-REPLY): Dear $nama. Your new password is $pass",
											'login_buat' => $id_akun."_".$nama,
											'tanggal_buat' => $this->waktu
											);
								$this->m_sms->PTL_insert($data);
							}
							else
							{
								$url = "http://103.16.199.187/masking/send_post.php";
								$rows = array(
											"username" => "esmodjkt",
											"password" => "esmj@kr00t",
											"hp" => "$handphone",
											"message" => "Dear $nama. Your new password is $pass"
											);
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_URL, $url );
								curl_setopt($curl, CURLOPT_POST, TRUE );
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
								curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
								curl_setopt($curl, CURLOPT_HEADER, FALSE );
								curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
								curl_setopt($curl, CURLOPT_TIMEOUT, 60);
								$htm = curl_exec($curl);
								if(curl_errno($curl) !== 0)
								{
									$data = array(
												'id_user' => $id_akun,
												'nama' => $nama,
												'aplikasi' => 'Academic',
												'judul_sms' => 'Pengiriman Untuk Lupa Password',
												'nomor_hp' => $handphone,
												'pesan' => "ESMOD (NO-REPLY): Dear $nama. Your new password is $pass",
												'login_buat' => $id_akun."_".$nama,
												'tanggal_buat' => $this->waktu
												);
									$this->m_sms->PTL_insert($data);
									error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
								}
								curl_close($curl);
								print_r($htm);
							}
						}
						
						$this->load->library('email');
						$config = array();
						$config['charset'] = 'utf-8';
						$config['useragent'] = 'Codeigniter';
						$config['protocol']= "smtp";
						$config['mailtype']= "html";
						$config['smtp_host']= "mail.esmodjakarta.com";
						$config['smtp_port']= "25";
						$config['smtp_timeout']= "5";
						$config['smtp_user']= "no-reply@esmodjakarta.com";
						$config['smtp_pass']= "noreplyesmod";
						$config['crlf']="\r\n"; 
						$config['newline']="\r\n"; 
						$config['wordwrap'] = TRUE;
						$this->email->initialize($config);
						$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
						$this->email->to($return['email']);
						$this->email->cc($return['email2']);
						$this->email->bcc('lendra.permana@gmail.com');
						$this->email->subject('FORGOT PASSWORD (NO REPLY)');
						$this->email->message("
							<center>
								<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
								<font color='red'><h2>ACADEMIC</h2></font>
							</center>
							<br/>
							Hi ".strtoupper($return['nama'])."!
							<br/>
							<br/>
							On this $tgl at $jam, you made a request to reset your password.
							<br/>
							<br/>
							This is your new access
							<br/>
							URL: http://app.esmodjakarta.com/academic
							<br/>
							Employee ID: <b>$return[username]</b>
							<br/>
							Your new password: <b>$pass</b>
							<br/>
							<br/>
							<br/>
							Thanks,
							<br/>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
							<center>
								<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
								Ip: $ip_client
								<br/>
								Hostname: $hostname_client
								<br/>
								OS: $os_client
								<br/>
								Browser: $browser_client
								<br/>
								Devices: $perangkat_client
							</center>
						");
						if($this->email->send())
						{
							echo warning('Verification will be sent to your email:\n\n'.$return['email'],'../login');
						}
						else
						{
							echo warning('Email server is not active. Please try again ...','../login');
						}
					}
					else
					{
						$datadir = array(
									'in' => $email,
									'aplikasi' => 'ACADEMIC',
									'tanggal' => $tanggal,
									'jam' => $jam,
									'ip_client' => $ip_client,
									'hostname_client' => $hostname_client,
									'os_client' => $os_client,
									'browser_client' => $browser_client,
									'perangkat_client' => $perangkat_client,
									'status' => 'FORGOT PASSWORD FAILED'
									);
						$this->m_log->PTL_input($datadir);
						echo warning('Sorry, your Username or Email you entered is incorrect ...','../login/ptl_forgot');
					}
				}
			}
			else
			{
				echo warning('Please complete the form and captcha.','../login/ptl_forgot');
			}
		}
	
		function ptl_logout()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			if(@$_COOKIE["id_akun"] != "")
			{
				$id_akses = @$_COOKIE["id_akses"];
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				$tanggal = gmdate("Y-m-d", time()-($ms));
				$jam = gmdate("H:i:s", time()-($ms));
				$data = array(
							'tanggal_pengingat' => '',
							'verifikasi' => 'N',
							'ip_client' => $ip_client,
							'hostname_client' => $hostname_client,
							'os_client' => $os_client,
							'browser_client' => $browser_client,
							'perangkat_client' => $perangkat_client,
							'last_logout' => $waktu
							);
				$this->m_akses->PTL_update($id_akses,$data);
				
				$datadir = array(
								'id_akun' => @$_COOKIE["id_akun"]."",
								'id_akses' => $id_akses."",
								'id_cabang' => @$_COOKIE["id_cabang"]."",
								'in' => @$_COOKIE["username"]."",
								'nama' => @$_COOKIE["nama"]."",
								'divisi' => @$_COOKIE["divisi"]."",
								'jabatan' => @$_COOKIE["jabatan"]."",
								'akses' => @$_COOKIE["akses"]."",
								'aplikasi' => 'ACADEMIC',
								'tanggal' => $tanggal,
								'jam' => $jam,
								'ip_client' => $ip_client,
								'hostname_client' => $hostname_client,
								'os_client' => $os_client,
								'browser_client' => $browser_client,
								'perangkat_client' => $perangkat_client,
								'status' => 'LOGOUT'
								);
				$this->m_log->PTL_input($datadir);
			}
			$this->auth->logout();
			echo warning('You have successfully logged out ...'.'\n'.
						'Logout Now :  '.$waktu,'../login');
		}
		
		function ptl_home()
		{
			$this->authentification();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic != 'logged') 
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','home');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACDAS01",
							'aktifitas' => "Mengakses halaman Beranda.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$id_akses = $_COOKIE["id_akses"];
			$result = $this->m_akses->PTL_select($id_akses);
			if(($result['tanggal_pengingat'] != "") AND ($result['verifikasi'] == "N"))
			{
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				$handphone = "";
				if($resakun)
				{
					$handphone = $resakun['handphone'];
				}
				if($handphone != "")
				{
					if(substr($handphone,0,2) == "62")
					{
						$prov = "0".substr($handphone,2,3);
					}
					else
					{
						$prov = substr($handphone,0,4);
					}
					if(($prov == "0896") OR ($prov == "0897") OR ($prov == "0898") OR ($prov == "0899"))
					{
						$data = array(
									'id_user' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => 'Academic',
									'judul_sms' => 'OTP untuk login 7 hari',
									'nomor_hp' => $handphone,
									'pesan' => "ESMOD (NO-REPLY): Academic App Login for 7 days. Your OTP $this->kode_verifikasi",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_sms->PTL_insert($data);
					}
					else
					{
						$url = "http://103.16.199.187/masking/send_post.php";
						$rows = array(
									"username" => "esmodjkt",
									"password" => "esmj@kr00t",
									"hp" => "$handphone",
									"message" => "Academic App Login for 7 days. Your OTP $this->kode_verifikasi"
									);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $url );
						curl_setopt($curl, CURLOPT_POST, TRUE );
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
						curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($rows) );
						curl_setopt($curl, CURLOPT_HEADER, FALSE );
						curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
						curl_setopt($curl, CURLOPT_TIMEOUT, 60);
						$htm = curl_exec($curl);
						if(curl_errno($curl) !== 0)
						{
							$data = array(
										'id_user' => $_COOKIE["id_akun"],
										'nama' => $_COOKIE["nama"],
										'aplikasi' => 'Academic',
										'judul_sms' => 'OTP untuk login 7 hari',
										'nomor_hp' => $handphone,
										'pesan' => "ESMOD (NO-REPLY): Academic App Login for 7 days. Your OTP $this->kode_verifikasi",
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu
										);
							$this->m_sms->PTL_insert($data);
							error_log('cURL error when connecting to ' . $url . ': ' . curl_error($curl));
						}
						curl_close($curl);
						print_r($htm);
					}
				}
				$data = array(
							'kode_verifikasi' => $this->kode_verifikasi
							);
				$this->m_akses->PTL_update($id_akses,$data);
				echo warning('OTP was sent.','../login/ptl_verification_sms');
			}
			else
			{
				$data['tanggal_pengingat'] = '';
				$data['last_login'] = '';
				$data['last_logout'] = '';
				$data['total_hour'] = '';
				if($result)
				{
					$data['tanggal_pengingat'] = $result['tanggal_pengingat'];
					$tin = explode(" ",$result['last_login']);
					$data['last_login'] = ' Last Login '.tgl_singkat_eng(@$tin[0]).' '.@$tin[1];
					if($result['last_logout'] == "")
					{
						$last_logout = '000-00-00 00:00:00';
					}
					else
					{
						$last_logout = $result['last_logout'];
					}
					$tout = explode(" ",$last_logout);
					$data['last_logout'] = ', Last Logout '.tgl_singkat_eng(@$tout[0]).' '.@$tout[1];
					$data['total_hour'] = ', Total Access '.selisih_jam(@$tin[1],@$tout[1]).'.';
				}
				$h = "-7";
				$hm = $h * 60;
				$ms = $hm * 60;
				$id_akun = $_COOKIE["id_akun"];
				$ultah = gmdate("m-d", time()-($ms));
				$resakun = $this->m_akun->PTL_select($id_akun);
				$DosenID = $resakun['DosenID'];
				$data['rowpresensi'] = $this->m_presensi->PTL_all_lecturer_expired($DosenID);
				$data['rowexam'] = $this->m_exam->PTL_all_lecturer_expired($DosenID);
				if(($resakun['verifikasi'] == "N") AND ($id_akun != "00001111"))
				{
					if($_SERVER["HTTP_HOST"] == "localhost")
					{
						header("Location: http://localhost/app/hris_verifikasi/personnel/lendra_edit/$id_akun/academic.xhtml");
					}
					else
					{
						header("Location: http://app.esmodjakarta.com/hris_verifikasi/personnel/lendra_edit/$id_akun/academic.xhtml");
					}
				}
				else
				{
					if(($resakun['verifikasi_handphone'] == "N") AND ($id_akun != "00001111"))
					{
						if($_SERVER["HTTP_HOST"] == "localhost")
						{
							header("Location: http://localhost/app/hris_verifikasi/personnel/lendra_personnel_handphone/$id_akun/academic.xhtml");
						}
						else
						{
							header("Location: http://app.esmodjakarta.com/hris_verifikasi/personnel/lendra_personnel_handphone/$id_akun/academic.xhtml");
						}
					}
					else
					{
						$tanggal_lahir = substr($resakun['tanggal_lahir'],5,5);
						$data['gambar_ultah'] = '';
						if($tanggal_lahir == $ultah)
						{
							$data['gambar_ultah'] = "<img src='".base_url("ptl_storage/foto_umum/ultah.gif")."' class='img-responsive' alt='Cinque Terre' width='1000'>";
						}
						$data['DosenID'] = $resakun['DosenID'];
						$result = $this->m_rating_question_grup->PTL_last();
						$tanggal_buat = $result['LAST'];
						$result2 = $this->m_rating_question_grup->PTL_select_tanggal($tanggal_buat);
						$id_rating_question_grup = $result2['id_rating_question_grup'];
						$resultrating = $this->m_rating_question_grup->PTL_select($id_rating_question_grup);
						$data['id_rating_question_grup'] = $id_rating_question_grup;
						$data['namarating'] = '';
						if($resultrating)
						{
							$data['namarating'] = $resultrating['nama'];
						}
						$data['rowrecord'] = $this->m_rating_question->PTL_all_spesifik($id_rating_question_grup);
						$data['active'] = $this->m_mahasiswa->PTL_all_active();
						$data['leave'] = $this->m_mahasiswa->PTL_all_leave();
						$data['drop_out'] = $this->m_mahasiswa->PTL_all_drop_out();
						$data['graduated'] = $this->m_mahasiswa->PTL_all_graduated();
						$data['rowrecord1'] = $this->m_akses->PTL_all_last_login();
						$data['rowrecord2'] = $this->m_akses->PTL_all_last_logout();
						$id_supervisor = $resakun['id_supervisor'];
						if($id_supervisor == "")
						{
							$data['atasan'] = $resakun['id_superior'];
							$id_akun = $resakun['id_superior'];
							$resakun = $this->m_akun->PTL_select($id_akun);
							$data['NamaAtasan'] = @$resakun['nama'];
						}
						else
						{
							$data['atasan'] = $resakun['id_supervisor'];
							$resakun = $this->m_akun->PTL_select($id_akun);
							$data['NamaAtasan'] = @$resakun['nama'];
						}
						$this->load->view('Portal/v_header');
						$this->load->view('Beranda/v_home',$data);
						$this->load->view('Portal/v_footer');
					}
				}
			}
		}
		
		function ptl_verification_sms()
		{
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic != 'logged') 
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$id_akses = $_COOKIE["id_akses"];
			$result = $this->m_akses->PTL_select($id_akses);
			$data['kode_verifikasi'] = $result['kode_verifikasi'];
			$this->load->view('v_verifikasi',$data);
		}
		
		function ptl_verification_sms_check()
		{
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic != 'logged') 
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$kode_sms = $this->input->post('kode_sms');
			$kode_verifikasi = $this->input->post('kode_verifikasi');
			if($kode_sms == $kode_verifikasi)
			{
				$id_akses = $_COOKIE["id_akses"];
				$data = array(
							'verifikasi' => 'Y'
							);
				$this->m_akses->PTL_update($id_akses,$data);
				echo warning('Validation Success!','../login/ptl_home');
			}
			else
			{
				echo warning('Invalid OTP!','../login/ptl_verification_sms');
			}
		}
		
		function ptl_profile()
		{
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic != 'logged') 
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','home');
			$id_akun = $_COOKIE["id_akun"];
			$result = $this->m_akun->PTL_select($id_akun);
			$data['id_akun'] = $result['id_akun'];
			$data['nama'] = $result['nama'];
			$data['tempat'] = $result['tempat'];
			$data['tanggal_lahir'] = $result['tanggal_lahir'];
			$data['foto'] = $result['foto'];
			$data['foto_lama'] = $result['foto'];
			$data['kebangsaan'] = $result['kebangsaan'];
			$data['status_sipil'] = $result['status_sipil'];
			$data['agama'] = $result['agama'];
			$data['alamat_domisili'] = $result['alamat_domisili'];
			$data['kota'] = $result['kota'];
			$data['kode_pos'] = $result['kode_pos'];
			$data['telepon'] = $result['telepon'];
			$data['handphone'] = $result['handphone'];
			$data['email'] = $result['email'];
			$data['email2'] = $result['email2'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$this->load->view('Portal/v_header');
			$this->load->view('Beranda/v_home_profil',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_profile_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$admin_dp = '../hris/ptl_storage/foto_karyawan/';
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "FOTO_".$_COOKIE["id_akun"]."_".str_replace(' ','_',$_COOKIE["nama"])."_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '51200';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				setcookie("nama", $this->input->post('nama'), time()+28800, "/");
				$id_akun = $this->input->post('id_akun');
				$data = array(
							'nama' => $this->input->post('nama'),
							'tempat' => $this->input->post('tempat'),
							'tanggal_lahir' => $this->input->post('tanggal_lahir'),
							'kebangsaan' => $this->input->post('kebangsaan'),
							'status_sipil' => $this->input->post('status_sipil'),
							'agama' => $this->input->post('agama'),
							'alamat_domisili' => $this->input->post('alamat_domisili'),
							'kota' => $this->input->post('kota'),
							'kode_pos' => $this->input->post('kode_pos'),
							'telepon' => $this->input->post('telepon'),
							'handphone' => $this->input->post('handphone'),
							'email' => $this->input->post('email'),
							'email2' => $this->input->post('email2'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_akun->PTL_update($id_akun,$data);
				echo warning("Profile updated.","../login/ptl_profile");
			}
			else
			{
				if($this->input->post('foto_lama') != "")
				{
					$foto_lama = $this->input->post('foto_lama');
					$dir = "../hris/ptl_storage/foto_karyawan/";
					unlink("./$dir/$foto_lama");
				}
				$source_photo = "$admin_dp/$downloadin";
				$ukuran = fsize($source_photo);
				$word = explode(" ",$ukuran);
				$pesan_file = "";
				$ukuran_akhir = "";
				if(($word[1] == "KB") OR ($word[1] == "MB"))
				{
					$pesan_file .= " File uploads are eligible for less than 10 MB.";
					if($word[1] == "KB")
					{
						if($word[0] > 128)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units KB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
					else
					{
						if($word[0] >= 1)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units MB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
				}
				else
				{
					if($word[1] == "B")
					{
						$pesan_file .=  " File too small.";
					}
					else
					{
						$pesan_file .=  " File too large.";
					}
				}
				setcookie("nama", $this->input->post('nama'), time()+28800, "/");
				setcookie("foto", $dwn, time()+28800, "/");
				$id_akun = $this->input->post('id_akun');
				$data = array(
							'nama' => $this->input->post('nama'),
							'tempat' => $this->input->post('tempat'),
							'tanggal_lahir' => $this->input->post('tanggal_lahir'),
							'foto' => $dwn,
							'kebangsaan' => $this->input->post('kebangsaan'),
							'status_sipil' => $this->input->post('status_sipil'),
							'agama' => $this->input->post('agama'),
							'alamat_domisili' => $this->input->post('alamat_domisili'),
							'kota' => $this->input->post('kota'),
							'kode_pos' => $this->input->post('kode_pos'),
							'telepon' => $this->input->post('telepon'),
							'handphone' => $this->input->post('handphone'),
							'email' => $this->input->post('email'),
							'email2' => $this->input->post('email2'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_akun->PTL_update($id_akun,$data);
				echo warning("Profile updated.$pesan_file$ukuran_akhir","../login/ptl_profile");
			}
		}
		
		function ptl_password()
		{
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic != 'logged') 
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$this->session->set_userdata('menu','home');
			$this->load->view('Portal/v_header');
			$this->load->view('Beranda/v_home_password');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_password_update()
		{
			$this->authentification();
			$username = $_COOKIE["username"];
			$dataku = $this->input->post('pass_lama');
			
			$result1 = $this->m_akses->PTL_password_lama($username,$dataku);
			if($result1)
			{
				$data = $this->input->post('pass_baru1');
				$data1 = $this->input->post('pass_baru2');
				if($data == $data1)
				{
					$this->m_akses->PTL_update_password($username,$data);
					echo warning("Password Successfully Changed.","../login/ptl_home");
				}
				else
				{
					echo warning("Confirm password is not the same. Please repeat.","../login/ptl_password");
				}
			}
			else
			{
				echo warning("Old password is incorrect. Please repeat.","../login/ptl_password");
			}
		}
		
		function ptl_maintenance()
		{
			$this->load->view('v_maintenance');
		}
	}
?>