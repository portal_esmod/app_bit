<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Schedule extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_dosen');
			$this->load->model('m_hari');
			$this->load->model('m_jadwal');
			$this->load->model('m_jadwal_hari');
			$this->load->model('m_kalender');
			$this->load->model('m_kelas');
			$this->load->model('m_krs');
			$this->load->model('m_kurikulum');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_notifikasi_lms');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_ruang');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED01-01",
							'aktifitas' => "Filter halaman Schedule - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('sch_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('sch_filter_jur');
			}
			redirect("schedule");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED01-02",
							'aktifitas' => "Filter halaman Schedule - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('sch_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('sch_filter_tahun');
			}
			redirect("schedule");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			$datalog = array(
							'pk1' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED01-05",
							'aktifitas' => "Filter halaman Schedule - Lecturer: $cekdosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekdosen != "")
			{
				$this->session->set_userdata('sch_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('sch_filter_dosen');
			}
			redirect("schedule");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			$datalog = array(
							'pk1' => $cekkelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED01-06",
							'aktifitas' => "Filter halaman Schedule - Class: $cekkelas.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkelas != "")
			{
				if($cekkelas == "_")
				{
					$this->session->unset_userdata('sch_filter_kelas');
				}
				else
				{
					$this->session->set_userdata('sch_filter_kelas',$cekkelas);
				}
			}
			else
			{
				$this->session->unset_userdata('sch_filter_kelas');
			}
			redirect("schedule");
		}
		
		function ptl_filter_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			$datalog = array(
							'pk1' => $cektype,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED01-04",
							'aktifitas' => "Filter halaman Schedule - Type: $cektype.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektype != "")
			{
				$this->session->set_userdata('sch_filter_type',$cektype);
			}
			else
			{
				$this->session->unset_userdata('sch_filter_type');
			}
			redirect("schedule");
		}
		
		function lookup_dosen()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_dosen->lookup_dosen($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->Login,
											'value' => $row->Login.' - '.$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('schedule/index',$data);
			}
		}
		
		function lookup_subjek_detail()
		{
			$cekjurusan = $this->session->userdata('sch_filter_jur');
			$TahunID = $this->session->userdata('sch_filter_tahun');
			if($cekjurusan == "REG"){ $jur = "D3"; }
			if($cekjurusan == "INT"){ $jur = "D1"; }
			if($cekjurusan == "SC"){ $jur = "SC"; }
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$a = "";
					$word = explode(".",$row->ProdiID);
					for($i=0;$i<=10;$i++)
					{
						if(@$word[$i] != "")
						{
							$ProdiID = $word[$i];
							$r = $this->m_prodi->PTL_select($ProdiID);
							$subjek = "";
							$jenjang = "";
							if($r)
							{
								$subjek = $r["Nama"];
								$jenjang = $r["Jenjang"];
							}
							if($jur == $jenjang)
							{
								$a .= "<input type='checkbox' name='prodi$i' value='".@$word[$i]."' checked>&nbsp;&nbsp;".@$word[$i]." - $subjek<br/>";
							}
						}
					}
					$b = "";
					if($a != "")
					{
						$b .= "Program Study<br/>";
					}
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => $b.$a,
											''
											);
				}
				if($a == "")
				{
					$data['response'] = 'true';
					$data['message'] = array();
					$data['message'][] = array(
											'id'=> '',
											'value' => 'Curriculum ID not set in Academic Year',
											'prodi' => '',
											''
											);
				}
			}
			if($cekjurusan == "")
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
											'id'=> '',
											'value' => 'Your session has expired',
											'prodi' => '',
											''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('schedule/index',$data);
			}
		}
		
		function ptl_merge()
		{
			$word = explode("__",$this->input->post('id_kelas'));
			$KelasIDGabungan = @$word[1];
			$data = "";
			$no = 0;
			$no2 = 0;
			$mapel = $this->m_kelas->dra_getKelas();
			if($word[0] == "MERGE")
			{
				for($i=1;$i<=3;$i++)
				{
					foreach($mapel as $mp)
					{
						$no++;
						$ck = "";
						$ckd = $i."^".$mp["KelasID"].".";
						if(stristr($KelasIDGabungan,$ckd))
						{
							$ck = "checked";
						}
						$data .= "<div class='col-lg-3'><input type='checkbox' name='merge$no' value='$i^$mp[KelasID]' $ck>&nbsp;&nbsp;$i$mp[Nama]</div>\n";
						$no2++;
					}
					$data .= "<div class='col-lg-12'></div>";
				}
				$data .= "<input type='hidden' name='total' value='$no'>";
			}
			echo $data;
		}
		
		function index()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED02",
							'aktifitas' => "Mengakses halaman Schedule.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$jur = $this->session->userdata('sch_filter_jur');
			$tahun = $this->session->userdata('sch_filter_tahun');
			$kelas = $this->session->userdata('sch_filter_kelas');
			$type = $this->session->userdata('sch_filter_type');
			if(($jur == "") AND ($tahun == "") AND ($kelas == "") AND ($type == ""))
			{
				$this->session->sess_destroy();
			}
			$this->session->set_userdata('menu','schedule');
			$data['jur'] = $this->session->userdata('sch_filter_jur');
			$cekjurusan = $data['jur'];
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			// $data['rowkelas'] = $this->m_kelas->PTL_all_active();
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				$this->load->view('Schedule/v_schedule_lecturer',$data);
			}
			else
			{
				$data['rowpending'] = $this->m_jadwal->PTL_all_pending();
				$this->load->view('Schedule/v_schedule',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$data['program'] = $this->uri->segment(3);
			$data['tahun'] = $this->uri->segment(4);
			$JadwalID = $this->uri->segment(5);
			$total_all = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $this->uri->segment(3),
							'pk2' => $this->uri->segment(4),
							'pk3' => $this->uri->segment(5),
							'pk4' => $this->uri->segment(6),
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED03",
							'aktifitas' => "Mengakses halaman Schedule - Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			if($result)
			{
				$data['JadwalID'] = $result['JadwalID'];
				$data['KelasID'] = $result['KelasID'];
				$data['SpesialisasiID'] = $result['SpesialisasiID'];
				$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
				$data['SubjekID'] = $result['SubjekID'];
				$data['Sesi'] = $result['Sesi'];
				$data['TglMulai'] = $result['TglMulai'];
				$data['TglSelesai'] = $result['TglSelesai'];
				$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
				$data['MaxAbsen'] = $result['MaxAbsen'];
				$data['DosenID'] = $result['DosenID'];
				$data['DosenID2'] = $result['DosenID2'];
				$data['DosenID3'] = $result['DosenID3'];
				$data['DosenID4'] = $result['DosenID4'];
				$data['DosenID5'] = $result['DosenID5'];
				$data['DosenID6'] = $result['DosenID6'];
				$data['NA'] = $result['NA'];
				$data['total_all'] = $total_all;
				$data['sch_Merge'] = '';
				if($result['Gabungan'] == "Y")
				{
					$data['sch_Merge'] = "MERGE";
				}
			}
			else
			{
				$data['JadwalID'] = '';
				$data['KelasID'] = '';
				$data['SpesialisasiID'] = '';
				$data['KelasIDGabungan'] = '';
				$data['SubjekID'] = '';
				$data['Sesi'] = '';
				$data['TglMulai'] = '';
				$data['TglSelesai'] = '';
				$data['RencanaKehadiran'] = '';
				$data['MaxAbsen'] = '';
				$data['DosenID'] = '';
				$data['DosenID2'] = '';
				$data['DosenID3'] = '';
				$data['DosenID4'] = '';
				$data['DosenID5'] = '';
				$data['DosenID6'] = '';
				$data['NA'] = '';
				$data['total_all'] = '';
				$data['sch_Merge'] = '';
			}
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowspesialisasi'] = $this->m_spesialisasi->PTL_all_data();
			$this->load->view('Portal/v_header');
			$this->load->view('Schedule/v_schedule_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_form_day()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED04",
							'aktifitas' => "Mengakses halaman Schedule - Form Day.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$KelasID = $this->input->post('KelasID');
			$r = $this->m_kelas->PTL_select_kelas($KelasID);
			$word = explode(" - ",$this->input->post('SubjekID'));
			$wor1 = explode(" - ",$this->input->post('DosenID'));
			$wor2 = explode(" - ",$this->input->post('DosenID2'));
			$wor3 = explode(" - ",$this->input->post('DosenID3'));
			$wor4 = explode(" - ",$this->input->post('DosenID4'));
			$wor5 = explode(" - ",$this->input->post('DosenID5'));
			$wor6 = explode(" - ",$this->input->post('DosenID6'));
			$prodi = "";
			for($i=0;$i<=10;$i++)
			{
				if($this->input->post("prodi$i") != "")
				{
					$prodi .= $this->input->post("prodi$i").".";
				}
			}
			$sesi = $this->input->post('Sesi');
			if(($sesi == 1) OR ($sesi == 2))
			{
				$ses = 1;
			}
			if(($sesi == 3) OR ($sesi == 4))
			{
				$ses = 2;
			}
			if(($sesi == 5) OR ($sesi == 6))
			{
				$ses = 3;
			}
			$KelasIDGabungan = "";
			$gab = "N";
			if(($KelasID == "MERGE") OR ($KelasID == "") OR ($KelasID == "0"))
			{
				$total = $this->input->post('total');
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("merge$i") != "")
					{
						$KelasIDGabungan .= $this->input->post("merge$i").".";
					}
				}
				$KelasID = 0;
				$gab = "Y";
			}
			$JadwalID = $this->input->post('JadwalID');
			if($JadwalID == "")
			{
				$result2 = $this->m_jadwal->PTL_urut();
				$JadwalID = $result2['LAST'] + 1;
				$data = array(
							'JadwalID' => $JadwalID,
							'SubjekID' => $word[0],
							'TahunID' => $this->input->post('tahun'),
							'ProdiID' => $prodi,
							'ProgramID' => $this->input->post('program'),
							'SpesialisasiID' => $this->input->post('SpesialisasiID'),
							'Gabungan' => $gab,
							'KelasID' => $KelasID,
							'KelasIDGabungan' => $KelasIDGabungan,
							'TahunKe' => $ses,
							'Sesi' => $sesi,
							'JenisJadwalID' => 'K',
							'TglMulai' => $this->input->post('TglMulai'),
							'TglSelesai' => $this->input->post('TglSelesai'),
							'DosenID' => $wor1[0],
							'DosenID2' => $wor2[0],
							'DosenID3' => $wor3[0],
							'DosenID4' => $wor4[0],
							'DosenID5' => $wor5[0],
							'DosenID6' => $wor6[0],
							'RencanaKehadiran' => $this->input->post('TotAttendance'),
							'KehadiranMin' => $this->input->post('TotAttendance') - $this->input->post('TotAbsen'),
							'MaxAbsen' => $this->input->post('TotAbsen'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_jadwal->PTL_insert($data);
				$this->m_jadwal_hari->PTL_delete_jadwal($JadwalID);
				$this->m_presensi->PTL_delete_jadwal($JadwalID);
				$this->m_presensi_mahasiswa->PTL_delete_jadwal($JadwalID);
				$total_all = "";
			}
			else
			{
				$total_all = $this->input->post('total_all');
				$data = array(
							'JadwalID' => $JadwalID,
							'SubjekID' => $word[0],
							'TahunID' => $this->input->post('tahun'),
							'ProdiID' => $prodi,
							'ProgramID' => $this->input->post('program'),
							'SpesialisasiID' => $this->input->post('SpesialisasiID'),
							'Gabungan' => $gab,
							'KelasID' => $KelasID,
							'KelasIDGabungan' => $KelasIDGabungan,
							'TahunKe' => $ses,
							'Sesi' => $sesi,
							'JenisJadwalID' => 'K',
							'TglMulai' => $this->input->post('TglMulai'),
							'TglSelesai' => $this->input->post('TglSelesai'),
							'DosenID' => $wor1[0],
							'DosenID2' => $wor2[0],
							'DosenID3' => $wor3[0],
							'DosenID4' => $wor4[0],
							'DosenID5' => $wor5[0],
							'DosenID6' => $wor6[0],
							'RencanaKehadiran' => $this->input->post('TotAttendance'),
							'KehadiranMin' => $this->input->post('TotAttendance') - $this->input->post('TotAbsen'),
							'MaxAbsen' => $this->input->post('TotAbsen'),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_jadwal->PTL_update($JadwalID,$data);
			}
			redirect("schedule/ptl_form_day_detail/$JadwalID/$total_all");
		}
		
		// function ptl_form_day_update()
		// {
			// $this->authentification();
			// $this->session->set_userdata('menu','schedule');
			// $KelasID = $this->input->post('KelasID');
			// $r = $this->m_kelas->PTL_select_kelas($KelasID);
			// $word = explode(" - ",$this->input->post('SubjekID'));
			// $wor1 = explode(" - ",$this->input->post('DosenID'));
			// $wor2 = explode(" - ",$this->input->post('DosenID2'));
			// $wor3 = explode(" - ",$this->input->post('DosenID3'));
			// $wor4 = explode(" - ",$this->input->post('DosenID4'));
			// $wor5 = explode(" - ",$this->input->post('DosenID5'));
			// $wor6 = explode(" - ",$this->input->post('DosenID6'));
			// $prodi = "";
			// for($i=1;$i<=5;$i++)
			// {
				// if($this->input->post("prodi$i") != "")
				// {
					// $prodi .= $this->input->post("prodi$i").".";
				// }
				// else
				// {
					// $prodi .= "";
				// }
			// }
			// $sesi = $this->input->post('Sesi');
			// if(($sesi == 1) OR ($sesi == 2))
			// {
				// $ses = 1;
			// }
			// if(($sesi == 3) OR ($sesi == 4))
			// {
				// $ses = 2;
			// }
			// if(($sesi == 5) OR ($sesi == 6))
			// {
				// $ses = 3;
			// }
			// $result2 = $this->m_jadwal->PTL_urut();
			// $JadwalID = $result2['LAST'] + 1;
			// $ses_data = array(
							// 'sch_program' => $this->input->post('program'),
							// 'sch_tahun' => $this->input->post('tahun'),
							// 'sch_NamaSubjekID' => $word[1],
							// 'sch_KelasID' => $KelasID,
							// 'sch_NamaKelasID' => $r['Nama'],
							// 'sch_SpesialisasiID' => $this->input->post('SpesialisasiID'),
							// 'sch_DosenID' => $wor1[0],
							// 'sch_NamaDosenID' => $wor1[1],
							// 'sch_DosenID2' => $wor2[0],
							// 'sch_NamaDosenID2' => $wor2[1],
							// 'sch_DosenID3' => $wor3[0],
							// 'sch_NamaDosenID3' => $wor3[1],
							// 'sch_DosenID4' => $wor4[0],
							// 'sch_NamaDosenID4' => $wor4[1],
							// 'sch_DosenID5' => $wor5[0],
							// 'sch_NamaDosenID5' => $wor5[1],
							// 'sch_DosenID6' => $wor6[0],
							// 'sch_NamaDosenID6' => $wor6[1],
							// 'sch_JadwalID' => $JadwalID,
							// 'sch_KodeID' => 'ES01',
							// 'sch_SubjekID' => $word[0],
							// 'sch_TahunID' => $this->session->userdata('sch_filter_tahun'),
							// 'sch_ProdiID' => $prodi,
							// 'sch_ProgramID' => $this->session->userdata('sch_filter_jur'),
							// 'sch_TahunKe' => $ses,
							// 'sch_Sesi' => $sesi,
							// 'sch_TglMulai' => $this->input->post('TglMulai'),
							// 'sch_TglSelesai' => $this->input->post('TglSelesai'),
							// 'sch_Kehadiran' => $this->input->post('TotAttendance'),
							// 'sch_KehadiranMin' => $this->input->post('TotAttendance'),
							// 'sch_MaxAbsen' => $this->input->post('TotAbsen'),
							// 'sch_NA' => $this->input->post('NA')
							// );
			// $this->session->set_userdata($ses_data);
			// $data['JadwalID'] = $JadwalID;
			// redirect('schedule/ptl_form_day_detail');
		// }
		
		function ptl_form_day_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$JadwalID = $this->uri->segment(3);
			$total_all = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total_all,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED05",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Detail.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['TahunID'] = $result['TahunID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['total_all'] = "";
			if($total_all != "")
			{
				$data['total_all'] = $total_all;
			}
			
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Schedule/v_schedule_form_day_detail',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form_day_insert()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED06",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'JadwalID' => $JadwalID,
						'HariID' => $this->uri->segment(4),
						'JamMulai' => '',
						'JamSelesai' => '',
						'RuangID' => '',
						'NA' => 'P'
						);
			$this->m_jadwal_hari->PTL_insert($data);
			redirect("schedule/ptl_form_day_detail/$JadwalID");
		}
		
		function ptl_form_day_delete()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$JadwalHariID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $JadwalHariID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED07",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_jadwal_hari->PTL_delete($JadwalHariID);
			redirect("schedule/ptl_form_day_detail/$JadwalID");
		}
		
		function ptl_form_day_save()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED08",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Save.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$total = $this->input->post("total");
			for($i=1;$i<=$total;$i++)
			{
				$JadwalHariID = $this->input->post("JadwalHariID$i");
				$data = array(
							'JadwalID' => $JadwalID,
							'HariID' => $this->input->post("HariID$i"),
							'JamMulai' => $this->input->post("JamMulai$i"),
							'JamSelesai' => $this->input->post("JamSelesai$i"),
							'RuangID' => $this->input->post("RuangID$i"),
							'NA' => 'N'
							);
				$this->m_jadwal_hari->PTL_update($JadwalHariID,$data);
			}
			echo warning("Your data has been saved. Next step, set attendance.","../schedule/ptl_form_day_attendance/$JadwalID/$total");
		}
		
		function ptl_form_day_save_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED09",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Save Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$total = $this->input->post("total");
			for($i=1;$i<=$total;$i++)
			{
				$JadwalHariID = $this->input->post("JadwalHariID$i");
				$data = array(
							'JadwalID' => $JadwalID,
							'HariID' => $this->input->post("HariID$i"),
							'JamMulai' => $this->input->post("JamMulai$i"),
							'JamSelesai' => $this->input->post("JamSelesai$i"),
							'RuangID' => $this->input->post("RuangID$i"),
							'NA' => 'N'
							);
				$this->m_jadwal_hari->PTL_update($JadwalHariID,$data);
			}
			echo warning("Your data has been updated. Next step, set attendance.","../schedule/ptl_form_day_attendance/$JadwalID/$total");
		}
		
		function ptl_form_day_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$JadwalID = $this->uri->segment(3);
			$total_all = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total_all,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED10",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['TahunID'] = $result['TahunID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['DosenID2'] = $result['DosenID2'];
			$data['DosenID3'] = $result['DosenID3'];
			$data['DosenID4'] = $result['DosenID4'];
			$data['DosenID5'] = $result['DosenID5'];
			$data['DosenID6'] = $result['DosenID6'];
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['total_all'] = "";
			if($total_all != "")
			{
				$data['total_all'] = $total_all;
			}
			
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowkalender'] = $this->m_kalender->PTL_all_tahun_ini();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Schedule/v_schedule_form_attendance',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form_day_attendance_save()
		{
			$this->authentification();
			$nsave = 0;
			$JadwalID = $this->input->post('JadwalID');
			$total = $this->input->post("total");
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED11",
							'aktifitas' => "Mengakses halaman Schedule - Form Day Attendance Save.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			for($i=1;$i<=$total;$i++)
			{
				$word = explode(":",$this->input->post("attJamMulai$i"));
				$jm = substr($word[0],0,1);
				if($jm < 10)
				{
					if($jm == 0)
					{
						$JamMulai = $this->input->post("attJamMulai$i");
					}
					else
					{
						$jm2 = substr($word[0],0,2);
						if($jm2 < 10)
						{
							$JamMulai = "0".$this->input->post("attJamMulai$i");
						}
						else
						{
							$JamMulai = $this->input->post("attJamMulai$i");
						}
					}
				}
				else
				{
					$JamMulai = $this->input->post("attJamMulai$i");
				}
				$wor = explode(":",$this->input->post("attJamSelesai$i"));
				$js = substr($wor[0],0,1);
				if($js < 10)
				{
					if($js == 0)
					{
						$JamSelesai = $this->input->post("attJamSelesai$i");
					}
					else
					{
						$js2 = substr($wor[0],0,2);
						if($js2 < 10)
						{
							$JamSelesai = "0".$this->input->post("attJamSelesai$i");
						}
						else
						{
							$JamSelesai = $this->input->post("attJamSelesai$i");
						}
					}
				}
				else
				{
					$JamSelesai = $this->input->post("attJamSelesai$i");
				}
				if($this->input->post("attTanggal$i") == "")
				{
					$Tanggal = "";
				}
				else
				{
					$wordt = explode(" ",$this->input->post("attTanggal$i"));
					$Tanggal = $wordt[3]."-".name_month_to_number($wordt[2])."-".$wordt[1];
				}
				if($Tanggal != "")
				{
					$nsave++;
					$data = array(
								'TahunID' => $this->input->post('TahunID'),
								'JadwalID' => $JadwalID,
								'Pertemuan' => $this->input->post("no$i"),
								'DosenID' => $this->input->post("attDosenID$i"),
								'Tanggal' => $Tanggal,
								'JamMulai' => $JamMulai,
								'JamSelesai' => $JamSelesai,
								'RuangID' => $this->input->post("attRuangID$i"),
								'Catatan' => $this->input->post("Catatan$i"),
								'NA' => 'N',
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_presensi->PTL_insert($data);
				}
			}
			$pesan = "";
			if($nsave > 0)
			{
				$pesan .= " $nsave data saved.";
			}
			$nover = $total - $nsave;
			if($nover > 0)
			{
				$pesan .= " $nover not saved by reason.";
			}
			$data = array(
						'finish' => 'Y'
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			$this->session->sess_destroy();
			$ProgramID = $this->input->post('ProgramID');
			$TahunID = $this->input->post('TahunID');
			
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$email1A = "";
			$email1B = "";
			$email2A = "";
			$email2B = "";
			$email3A = "";
			$email3B = "";
			$email4A = "";
			$email4B = "";
			$email5A = "";
			$email5B = "";
			$email6A = "";
			$email6B = "";
			$NamaDosen1 = "";
			$NamaDosen2 = "";
			$NamaDosen3 = "";
			$NamaDosen4 = "";
			$NamaDosen5 = "";
			$NamaDosen6 = "";
			$kelas = "";
			$Sesi = "";
			$TglMulai = "";
			$TglSelesai = "";
			$NamaSub = "";
			$NamaSpc = "";
			$RencanaKehadiran = "";
			if($result)
			{
				if($result['Gabungan'] == "Y")
				{
					$KelasIDGabungan = $result['Gabungan'];
					$kelas = "";
					$wordgab = explode(".",$KelasIDGabungan);
					for($i=0;$i<30;$i++)
					{
						$wg = explode("^",@$wordgab[$i]);
						$KelasID = @$wg[1];
						$res = $this->m_kelas->PTL_select_kelas($KelasID);
						if($res)
						{
							$kls = $res["Nama"];
						}
						else
						{
							$kls = "";
						}
						if($wg[0] != "")
						{
							$kelas .= @$wg[0].$kls.", ";
						}
					}
				}
				else
				{
					$KelasID = $result['KelasID'];
					$res = $this->m_kelas->PTL_select_kelas($KelasID);
					if($res)
					{
						if($result['ProgramID'] == 'INT')
						{
							$kelas = 'O'.$res["Nama"];
						}
						else
						{
							$kelas = $result['TahunKe'].$res["Nama"];
						}
					}
				}
				$Sesi = $result['Sesi'];
				$TglMulai = $result['TglMulai'];
				$TglSelesai = $result['TglSelesai'];
				$SubjekID = $result['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$SpesialisasiID = $result['SpesialisasiID'];
				$resspc = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				if($resspc)
				{
					$NamaSpc = $resspc['Nama'];
				}
				$RencanaKehadiran = $result['RencanaKehadiran'];
				$DosenID = $result['DosenID'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email1A = $resdosen['email'];
					$email1B = $resdosen['email2'];
					$NamaDosen1 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID2'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email2A = $resdosen['email'];
					$email2B = $resdosen['email2'];
					$NamaDosen2 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID3'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email3A = $resdosen['email'];
					$email3B = $resdosen['email2'];
					$NamaDosen3 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID4'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email4A = $resdosen['email'];
					$email4B = $resdosen['email2'];
					$NamaDosen4 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID5'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email5A = $resdosen['email'];
					$email5B = $resdosen['email2'];
					$NamaDosen5 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID6'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email6A = $resdosen['email'];
					$email6B = $resdosen['email2'];
					$NamaDosen6 = $resdosen['nama'];
				}
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$email1A,$email1B,$email2A,$email2B,$email3A,$email3B,$email4A,$email4B,$email5A,$email5B,$email6A,$email6B");
			$this->email->cc('anisa@esmodjakarta.com');
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject("NEW SCHEDULE CODE $JadwalID (NO REPLY)");
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Your schedule was created by <b>".$_COOKIE["nama"]."</b>.
				<br/>
				<br/>
				<b>Detail:</b>
				<table>
					<tr>
						<td>Code</td>
						<td>:</td>
						<td>$JadwalID</td>
					</tr>
					<tr>
						<td>Class</td>
						<td>:</td>
						<td>$kelas</td>
					</tr>
					<tr>
						<td>Semester</td>
						<td>:</td>
						<td>$Sesi</td>
					</tr>
					<tr>
						<td>Start Date</td>
						<td>:</td>
						<td>$TglMulai</td>
					</tr>
					<tr>
						<td>End Date</td>
						<td>:</td>
						<td>$TglSelesai</td>
					</tr>
					<tr>
						<td>Subjects</td>
						<td>:</td>
						<td>$NamaSub</td>
					</tr>
					<tr>
						<td>Specialization</td>
						<td>:</td>
						<td>$NamaSpc</td>
					</tr>
					<tr>
						<td>Session</td>
						<td>:</td>
						<td>$RencanaKehadiran</td>
					</tr>
					<tr>
						<td>Lecturer</td>
						<td>:</td>
						<td>$NamaDosen1</td>
					</tr>
					<tr>
						<td>Assistant 1</td>
						<td>:</td>
						<td>$NamaDosen2</td>
					</tr>
					<tr>
						<td>Assistant 2</td>
						<td>:</td>
						<td>$NamaDosen3</td>
					</tr>
					<tr>
						<td>Assistant 3</td>
						<td>:</td>
						<td>$NamaDosen4</td>
					</tr>
					<tr>
						<td>Assistant 4</td>
						<td>:</td>
						<td>$NamaDosen5</td>
					</tr>
					<tr>
						<td>Assistant 5</td>
						<td>:</td>
						<td>$NamaDosen6</td>
					</tr>
				</table>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data has been saved.$pesan","../schedule/ptl_direct_session_form_insert/$ProgramID/$TahunID");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data has been saved.$pesan","../schedule/ptl_direct_session_form_insert/$ProgramID/$TahunID");
				}
				else
				{
					echo warning("Email server is not active. Your data has been saved.$pesan","../schedule/ptl_direct_session_form_insert/$ProgramID/$TahunID");
				}
			}
		}
		
		function ptl_direct_session_form_insert()
		{
			$this->authentification();
			$ProgramID = $this->uri->segment(3);
			$TahunID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ProgramID,
							'pk2' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED12",
							'aktifitas' => "Mengakses halaman Schedule - Direct Session Form Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->session->set_userdata('sch_filter_jur',$ProgramID);
			$this->session->set_userdata('sch_filter_tahun',$TahunID);
			redirect("schedule");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$JadwalID = $this->uri->segment(3);
			$total_all = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total_all,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED13",
							'aktifitas' => "Mengakses halaman Schedule - Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['TahunID'] = $result['TahunID'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['Sesi'] = $result['Sesi'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['SpesialisasiID'] = $result['SpesialisasiID'];
			$data['DosenID'] = $result['DosenID'];
			$data['DosenID2'] = $result['DosenID2'];
			$data['DosenID3'] = $result['DosenID3'];
			$data['DosenID4'] = $result['DosenID4'];
			$data['DosenID5'] = $result['DosenID5'];
			$data['DosenID6'] = $result['DosenID6'];
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['MaxAbsen'] = $result['MaxAbsen'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['ProdiID'] = $result['ProdiID'];
			if($result['SubjekID'] == "")
			{
				$data['NamaSubjek'] = "";
			}
			else
			{
				$SubjekID = $result['SubjekID'];
				$ressubjek = $this->m_subjek->PTL_select($SubjekID);
				$data['NamaSubjek'] = $result['SubjekID']." - ".$ressubjek['Nama'];
			}
			if($result['DosenID'] == "")
			{
				$data['NamaDosen'] = "";
			}
			else
			{
				$DosenID = $result['DosenID'];
				$resdosen = $this->m_dosen->PTL_select($DosenID);
				if($resdosen)
				{
					$data['NamaDosen'] = $result['DosenID']." - ".$resdosen['Nama'];
				}
				else
				{
					$data['NamaDosen'] = "";
				}
			}
			if($result['DosenID2'] == "")
			{
				$data['NamaDosen2'] = "";
			}
			else
			{
				$DosenID = $result['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen2)
				{
					$data['NamaDosen2'] = $result['DosenID2']." - ".$resdosen2['Nama'];
				}
				else
				{
					$data['NamaDosen2'] = "";
				}
			}
			if($result['DosenID3'] == "")
			{
				$data['NamaDosen3'] = "";
			}
			else
			{
				$DosenID = $result['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen3)
				{
					$data['NamaDosen3'] = $result['DosenID3']." - ".$resdosen3['Nama'];
				}
				else
				{
					$data['NamaDosen3'] = "";
				}
			}
			if($result['DosenID4'] == "")
			{
				$data['NamaDosen4'] = "";
			}
			else
			{
				$DosenID = $result['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen4)
				{
					$data['NamaDosen4'] = $result['DosenID4']." - ".$resdosen4['Nama'];
				}
				else
				{
					$data['NamaDosen4'] = "";
				}
			}
			if($result['DosenID5'] == "")
			{
				$data['NamaDosen5'] = "";
			}
			else
			{
				$DosenID = $result['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen5)
				{
					$data['NamaDosen5'] = $result['DosenID5']." - ".$resdosen5['Nama'];
				}
				else
				{
					$data['NamaDosen5'] = "";
				}
			}
			if($result['DosenID6'] == "")
			{
				$data['NamaDosen6'] = "";
			}
			else
			{
				$DosenID = $result['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen6)
				{
					$data['NamaDosen6'] = $result['DosenID6']." - ".$resdosen6['Nama'];
				}
				else
				{
					$data['NamaDosen6'] = "";
				}
			}
			$data['total_all'] = "";
			if($total_all != "")
			{
				$data['total_all'] = $total_all;
			}
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowspesialisasi'] = $this->m_spesialisasi->PTL_all_data();
			$this->load->view('Portal/v_header');
			$this->load->view('Schedule/v_schedule_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_edit_update()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED14",
							'aktifitas' => "Mengakses halaman Schedule - Edit Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$wor = explode("__",$this->input->post('KelasID'));
			$KelasID = $wor[0];
			$r = $this->m_kelas->PTL_select_kelas($KelasID);
			$word = explode(" - ",$this->input->post('SubjekID'));
			$wor1 = explode(" - ",$this->input->post('DosenID'));
			$wor2 = explode(" - ",$this->input->post('DosenID2'));
			$wor3 = explode(" - ",$this->input->post('DosenID3'));
			$wor4 = explode(" - ",$this->input->post('DosenID4'));
			$wor5 = explode(" - ",$this->input->post('DosenID5'));
			$wor6 = explode(" - ",$this->input->post('DosenID6'));
			$prodi = "";
			for($i=0;$i<=10;$i++)
			{
				if($this->input->post("prodi$i") != "")
				{
					$prodi .= $this->input->post("prodi$i").".";
				}
			}
			$sesi = $this->input->post('Sesi');
			if(($sesi == 1) OR ($sesi == 2))
			{
				$ses = 1;
			}
			if(($sesi == 3) OR ($sesi == 4))
			{
				$ses = 2;
			}
			if(($sesi == 5) OR ($sesi == 6))
			{
				$ses = 3;
			}
			$JadwalID = $this->input->post('JadwalID');
			$KelasIDGabungan = "";
			$gab = "N";
			if(($KelasID == "MERGE") OR ($KelasID == "") OR ($KelasID == "0"))
			{
				$total = $this->input->post('total');
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("merge$i") != "")
					{
						$KelasIDGabungan .= $this->input->post("merge$i").".";
					}
				}
				$KelasID = 0;
				$gab = "Y";
			}
			$NA = "N";
			if($this->input->post('NA') == "Y")
			{
				$NA = "Y";
			}
			$data = array(
						'SubjekID' => $word[0],
						'ProdiID' => $prodi,
						'Gabungan' => $gab,
						'KelasID' => $KelasID,
						'KelasIDGabungan' => $KelasIDGabungan,
						'TahunKe' => $ses,
						'Sesi' => $this->input->post('Sesi'),
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'SpesialisasiID' => $this->input->post('SpesialisasiID'),
						'DosenID' => $wor1[0],
						'DosenID2' => $wor2[0],
						'DosenID3' => $wor3[0],
						'DosenID4' => $wor4[0],
						'DosenID5' => $wor5[0],
						'DosenID6' => $wor6[0],
						'RencanaKehadiran' => $this->input->post('TotAttendance'),
						'KehadiranMin' => $this->input->post('TotAttendance') - $this->input->post('TotAbsen'),
						'MaxAbsen' => $this->input->post('TotAbsen'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $NA
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			$data['JadwalID'] = $JadwalID;
			redirect("schedule/ptl_edit_day_detail/$JadwalID");
		}
		
		function ptl_edit_day_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$JadwalID = $this->uri->segment(3);
			$total_all = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total_all,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED15",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Detail.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['TahunID'] = $result['TahunID'];
			$data['SubjekID'] = $result['SubjekID'];
			$SubjekID = $result['SubjekID'];
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$data['NamaSubjek'] = $ressubjek['Nama'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$KelasID = $result['KelasID'];
			$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
			$data['NamaKelas'] = "a";
			if($reskelas)
			{
				$data['NamaKelas'] = $reskelas['Nama'];
			}
			else
			{
				$data['NamaKelas'] = "";
			}
			$data['TahunKe'] = $result['TahunKe'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['DosenID'] = $result['DosenID'];
			$DosenID = $result['DosenID'];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$data['NamaDosen'] = $resdosen['Nama'];
			$data['DosenID2'] = $result['DosenID2'];
			$data['DosenID3'] = $result['DosenID3'];
			$data['DosenID4'] = $result['DosenID4'];
			$data['DosenID5'] = $result['DosenID5'];
			$data['DosenID6'] = $result['DosenID6'];
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['MaxAbsen'] = $result['MaxAbsen'];
			$data['NA'] = $result['NA'];
			$data['total_all'] = "";
			if($total_all != "")
			{
				$data['total_all'] = $total_all;
			}
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Schedule/v_schedule_edit_day_detail',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit_day_insert()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED16",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'JadwalID' => $JadwalID,
						'HariID' => $this->uri->segment(4),
						'JamMulai' => '',
						'JamSelesai' => '',
						'RuangID' => '',
						'NA' => 'P'
						);
			$this->m_jadwal_hari->PTL_insert($data);
			redirect("schedule/ptl_edit_day_detail/$JadwalID");
		}
		
		function ptl_edit_day_delete()
		{
			$this->authentification();
			$JadwalHariID = $this->uri->segment(3);
			$JadwalID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalHariID,
							'pk2' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED17",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_jadwal_hari->PTL_delete($JadwalHariID);
			redirect("schedule/ptl_edit_day_detail/$JadwalID");
		}
		
		function ptl_edit_day_save_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post("JadwalID");
			$total = $this->input->post("total");
			$apply = $this->input->post("apply");
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $total,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED18",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Save Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			for($i=1;$i<=$total;$i++)
			{
				$JadwalHariID = $this->input->post("JadwalHariID$i");
				$word = explode(":",$this->input->post("JamMulai$i"));
				$jm = substr($word[0],0,1);
				if($jm < 10)
				{
					if($jm == 0)
					{
						$JamMulai = $this->input->post("JamMulai$i");
					}
					else
					{
						$jm2 = substr($word[0],0,2);
						if($jm2 < 10)
						{
							$JamMulai = "0".$this->input->post("JamMulai$i");
						}
						else
						{
							$JamMulai = $this->input->post("JamMulai$i");
						}
					}
				}
				else
				{
					$JamMulai = $this->input->post("JamMulai$i");
				}
				$wor = explode(":",$this->input->post("JamSelesai$i"));
				$js = substr($wor[0],0,1);
				if($js < 10)
				{
					if($js == 0)
					{
						$JamSelesai = $this->input->post("JamSelesai$i");
					}
					else
					{
						$js2 = substr($wor[0],0,2);
						if($js2 < 10)
						{
							$JamSelesai = "0".$this->input->post("JamSelesai$i");
						}
						else
						{
							$JamSelesai = $this->input->post("JamSelesai$i");
						}
					}
				}
				else
				{
					$JamSelesai = $this->input->post("JamSelesai$i");
				}
				$data = array(
							'JadwalID' => $JadwalID,
							'HariID' => $this->input->post("HariID$i"),
							'JamMulai' => $JamMulai,
							'JamSelesai' => $JamSelesai,
							'RuangID' => $this->input->post("RuangID$i")
							);
				$this->m_jadwal_hari->PTL_update($JadwalHariID,$data);
			}
			echo warning("Your data has been updated. Next step, set attendance.","../schedule/ptl_edit_day_attendance/$JadwalID/$total/$apply");
		}
		
		function ptl_edit_day_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','schedule');
			$JadwalID = $this->uri->segment(3);
			$data['total'] = $this->uri->segment(4);
			$data['apply'] = $this->uri->segment(5);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $this->uri->segment(4),
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED19",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['TahunID'] = $result['TahunID'];
			$data['SubjekID'] = $result['SubjekID'];
			$SubjekID = $result['SubjekID'];
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$data['NamaSubjek'] = $ressubjek['Nama'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$KelasID = $result['KelasID'];
			$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
			if($reskelas)
			{
				$data['NamaKelas'] = $reskelas['Nama'];
			}
			else
			{
				$data['NamaKelas'] = "";
			}
			$data['TahunKe'] = $result['TahunKe'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			if($result['DosenID'] == "")
			{
				$data['DosenID'] = "";
				$data['NamaDosen'] = "";
			}
			else
			{
				$DosenID = $result['DosenID'];
				$resdosen = $this->m_dosen->PTL_select($DosenID);
				if($resdosen)
				{
					$data['DosenID'] = $result['DosenID'];
					$data['NamaDosen'] = $resdosen['Nama'];
				}
				else
				{
					$data['DosenID'] = "";
					$data['NamaDosen'] = "";
				}
			}
			if($result['DosenID2'] == "")
			{
				$data['DosenID2'] = "";
				$data['NamaDosen2'] = "";
			}
			else
			{
				$DosenID = $result['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen2)
				{
					$data['DosenID2'] = $result['DosenID2'];
					$data['NamaDosen2'] = $resdosen2['Nama'];
				}
				else
				{
					$data['DosenID2'] = "";
					$data['NamaDosen2'] = "";
				}
			}
			if($result['DosenID3'] == "")
			{
				$data['DosenID3'] = "";
				$data['NamaDosen3'] = "";
			}
			else
			{
				$DosenID = $result['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen3)
				{
					$data['DosenID3'] = $result['DosenID3'];
					$data['NamaDosen3'] = $resdosen3['Nama'];
				}
				else
				{
					$data['DosenID3'] = "";
					$data['NamaDosen3'] = "";
				}
			}
			if($result['DosenID4'] == "")
			{
				$data['DosenID4'] = "";
				$data['NamaDosen4'] = "";
			}
			else
			{
				$DosenID = $result['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen4)
				{
					$data['DosenID4'] = $result['DosenID4'];
					$data['NamaDosen4'] = $resdosen4['Nama'];
				}
				else
				{
					$data['DosenID4'] = "";
					$data['NamaDosen4'] = "";
				}
			}
			if($result['DosenID5'] == "")
			{
				$data['DosenID5'] = "";
				$data['NamaDosen5'] = "";
			}
			else
			{
				$DosenID = $result['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen5)
				{
					$data['DosenID5'] = $result['DosenID5'];
					$data['NamaDosen5'] = $resdosen5['Nama'];
				}
				else
				{
					$data['DosenID5'] = "";
					$data['NamaDosen5'] = "";
				}
			}
			if($result['DosenID6'] == "")
			{
				$data['DosenID6'] = "";
				$data['NamaDosen6'] = "";
			}
			else
			{
				$DosenID = $result['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen6)
				{
					$data['DosenID6'] = $result['DosenID6'];
					$data['NamaDosen6'] = $resdosen6['Nama'];
				}
				else
				{
					$data['DosenID6'] = "";
					$data['NamaDosen6'] = "";
				}
			}
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['MaxAbsen'] = $result['MaxAbsen'];
			$data['NA'] = $result['NA'];
			$data['rowrecord'] = $this->m_hari->PTL_all();
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Schedule/v_schedule_edit_attendance',$data);
			$this->load->view('Portal/v_footer_table',$data);
		}
		
		function ptl_edit_day_attendance_cek_bentrok()
		{
			$this->authentification();
			$attPresensiID = mysql_real_escape_string($this->input->post("attPresensiID"));
			$wordt = explode(" ",mysql_real_escape_string($this->input->post("attTanggal")));
			$Tanggal = @$wordt[3]."-".name_month_to_number(@$wordt[2])."-".@$wordt[1];
			$JamMulai = mysql_real_escape_string($this->input->post("attJamMulai")).":00";
			$JamSelesai = mysql_real_escape_string($this->input->post("attJamSelesai")).":00";
			$RuangID = mysql_real_escape_string($this->input->post("attRuangID"));
			$result = $this->m_presensi->PTL_select_schedule_duplicate($Tanggal,$JamMulai,$JamSelesai,$RuangID);
			$JumlahBentrok = 0;
			$DataAkhir = 0;
			if($result)
			{
				$JumlahBentrok = $JumlahBentrok + 0;
				$DataAkhir = $result;
			}
			else
			{
				$JumlahBentrok = $JumlahBentrok + 1;
			}
			if($JumlahBentrok > 0)
			{
				echo 1;
			}
			else
			{
				if($DataAkhir == 0)
				{
					echo "#";
				}
				else
				{
					if($DataAkhir['PresensiID'] != $attPresensiID)
					{
						echo $DataAkhir['PresensiID'];
					}
					else
					{
						echo 1;
					}
				}
			}
		}
		
		function ptl_edit_day_attendance_save()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED20",
							'aktifitas' => "Mengakses halaman Schedule - Edit Day Attendance Save.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$nsave = 0;
			$nedit = 0;
			$ndelete = 0;
			$total = $this->input->post("total");
			for($i=1;$i<=$total;$i++)
			{
				$word = explode(":",$this->input->post("attJamMulai$i"));
				$jm = substr($word[0],0,1);
				if($jm < 10)
				{
					if($jm == 0)
					{
						$JamMulai = $this->input->post("attJamMulai$i");
					}
					else
					{
						$jm2 = substr($word[0],0,2);
						if($jm2 < 10)
						{
							$JamMulai = "0".$this->input->post("attJamMulai$i");
						}
						else
						{
							$JamMulai = $this->input->post("attJamMulai$i");
						}
					}
				}
				else
				{
					$JamMulai = $this->input->post("attJamMulai$i");
				}
				$wor = explode(":",$this->input->post("attJamSelesai$i"));
				$js = substr($wor[0],0,1);
				if($js < 10)
				{
					if($js == 0)
					{
						$JamSelesai = $this->input->post("attJamSelesai$i");
					}
					else
					{
						$js2 = substr($wor[0],0,2);
						if($js2 < 10)
						{
							$JamSelesai = "0".$this->input->post("attJamSelesai$i");
						}
						else
						{
							$JamSelesai = $this->input->post("attJamSelesai$i");
						}
					}
				}
				else
				{
					$JamSelesai = $this->input->post("attJamSelesai$i");
				}
				if($this->input->post("attTanggal$i") == "")
				{
					$Tanggal = "";
				}
				else
				{
					$wordt = explode(" ",$this->input->post("attTanggal$i"));
					$Tanggal = $wordt[3]."-".name_month_to_number($wordt[2])."-".$wordt[1];
				}
				$PresensiID = $this->input->post("attPresensiID$i");
				if($PresensiID == "")
				{
					if($Tanggal != "")
					{
						$nsave++;
						$data = array(
									'TahunID' => $this->input->post('TahunID'),
									'JadwalID' => $this->input->post('attJadwalID'),
									'Pertemuan' => $this->input->post("no$i"),
									'DosenID' => $this->input->post("attDosenID$i"),
									'Tanggal' => $Tanggal,
									'JamMulai' => $JamMulai,
									'JamSelesai' => $JamSelesai,
									'RuangID' => $this->input->post("attRuangID$i"),
									'Catatan' => $this->input->post("Catatan$i"),
									'NA' => 'N',
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_presensi->PTL_insert($data);
					}
				}
				else
				{
					if($Tanggal == "")
					{
						$ndelete++;
						$this->m_presensi->PTL_delete_presensi($PresensiID);
					}
					else
					{
						$nedit++;
						$data = array(
									'TahunID' => $this->input->post('TahunID'),
									'JadwalID' => $this->input->post("attJadwalID"),
									'Pertemuan' => $this->input->post("no$i"),
									'DosenID' => $this->input->post("attDosenID$i"),
									'Tanggal' => $Tanggal,
									'JamMulai' => $JamMulai,
									'JamSelesai' => $JamSelesai,
									'RuangID' => $this->input->post("attRuangID$i"),
									'Catatan' => $this->input->post("Catatan$i"),
									'NA' => 'N',
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_presensi->PTL_update($PresensiID,$data);
					}
				}
			}
			$pesan = "";
			if($nsave > 0)
			{
				$pesan .= " $nsave data saved.";
			}
			if($nedit > 0)
			{
				$pesan .= " $nedit data updated.";
			}
			if($ndelete > 0)
			{
				$pesan .= " $ndelete data deleted.";
			}
			$nover = $total - $nsave;
			if($nover > 0)
			{
				// $pesan .= " $nover not saved by reason.";//
			}
			$JadwalID = $this->input->post("attJadwalID");
			$data = array(
						'finish' => 'Y'
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			$this->session->sess_destroy();
			$ProgramID = $this->input->post('ProgramID');
			$TahunID = $this->input->post('TahunID');
			
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$email1A = "";
			$email1B = "";
			$email2A = "";
			$email2B = "";
			$email3A = "";
			$email3B = "";
			$email4A = "";
			$email4B = "";
			$email5A = "";
			$email5B = "";
			$email6A = "";
			$email6B = "";
			$NamaDosen1 = "";
			$NamaDosen2 = "";
			$NamaDosen3 = "";
			$NamaDosen4 = "";
			$NamaDosen5 = "";
			$NamaDosen6 = "";
			$kelas = "";
			$Sesi = "";
			$TglMulai = "";
			$TglSelesai = "";
			$NamaSub = "";
			$NamaSpc = "";
			$RencanaKehadiran = "";
			if($result)
			{
				if($result['Gabungan'] == "Y")
				{
					$KelasIDGabungan = $result['Gabungan'];
					$kelas = "";
					$wordgab = explode(".",$KelasIDGabungan);
					for($i=0;$i<30;$i++)
					{
						$wg = explode("^",@$wordgab[$i]);
						$KelasID = @$wg[1];
						$res = $this->m_kelas->PTL_select_kelas($KelasID);
						if($res)
						{
							$kls = $res["Nama"];
						}
						else
						{
							$kls = "";
						}
						if($wg[0] != "")
						{
							$kelas .= @$wg[0].$kls.", ";
						}
					}
				}
				else
				{
					$KelasID = $result['KelasID'];
					$res = $this->m_kelas->PTL_select_kelas($KelasID);
					if($res)
					{
						if($result['ProgramID'] == 'INT')
						{
							$kelas = 'O'.$res["Nama"];
						}
						else
						{
							$kelas = $result['TahunKe'].$res["Nama"];
						}
					}
				}
				$Sesi = $result['Sesi'];
				$TglMulai = $result['TglMulai'];
				$TglSelesai = $result['TglSelesai'];
				$SubjekID = $result['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$SpesialisasiID = $result['SpesialisasiID'];
				$resspc = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				if($resspc)
				{
					$NamaSpc = $resspc['Nama'];
				}
				$RencanaKehadiran = $result['RencanaKehadiran'];
				$DosenID = $result['DosenID'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email1A = $resdosen['email'];
					$email1B = $resdosen['email2'];
					$NamaDosen1 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID2'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email2A = $resdosen['email'];
					$email2B = $resdosen['email2'];
					$NamaDosen2 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID3'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email3A = $resdosen['email'];
					$email3B = $resdosen['email2'];
					$NamaDosen3 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID4'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email4A = $resdosen['email'];
					$email4B = $resdosen['email2'];
					$NamaDosen4 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID5'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email5A = $resdosen['email'];
					$email5B = $resdosen['email2'];
					$NamaDosen5 = $resdosen['nama'];
				}
				$DosenID = $result['DosenID6'];
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$email6A = $resdosen['email'];
					$email6B = $resdosen['email2'];
					$NamaDosen6 = $resdosen['nama'];
				}
			}
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$SubjekID = $resjadwal['SubjekID'];
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$NamaSubjek = "";
			if($ressubjek)
			{
				$NamaSubjek = $ressubjek["Nama"];
			}
			$rowkrs = $this->m_krs->PTL_all_jadwal($JadwalID);
			if($rowkrs)
			{
				foreach($rowkrs as $rkrs)
				{
					$MhswID = $rkrs->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$NamaMhsw = '';
					if($resmhsw)
					{
						$NamaMhsw = $resmhsw['Nama'];
					}
					$data = array(
								'MhswID' => $rkrs->MhswID,
								'JadwalID' => $JadwalID,
								'KRSID' => $rkrs->KRSID,
								'Nama' => $NamaMhsw,
								'Judul' => 'The schedule has been changed',
								'Pesan' => "Schedule for $NamaSubjek has been changed.",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_notifikasi_lms->PTL_insert($data);
				}
			}
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$email1A,$email1B,$email2A,$email2B,$email3A,$email3B,$email4A,$email4B,$email5A,$email5B,$email6A,$email6B");
			$this->email->cc('anisa@esmodjakarta.com');
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject("SCHEDULE UPDATED CODE $JadwalID (NO REPLY)");
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Your schedule was updated by <b>".$_COOKIE["nama"]."</b>.
				<br/>
				<br/>
				<b>Detail:</b>
				<table>
					<tr>
						<td>Code</td>
						<td>:</td>
						<td>$JadwalID</td>
					</tr>
					<tr>
						<td>Class</td>
						<td>:</td>
						<td>$kelas</td>
					</tr>
					<tr>
						<td>Semester</td>
						<td>:</td>
						<td>$Sesi</td>
					</tr>
					<tr>
						<td>Start Date</td>
						<td>:</td>
						<td>$TglMulai</td>
					</tr>
					<tr>
						<td>End Date</td>
						<td>:</td>
						<td>$TglSelesai</td>
					</tr>
					<tr>
						<td>Subjects</td>
						<td>:</td>
						<td>$NamaSub</td>
					</tr>
					<tr>
						<td>Specialization</td>
						<td>:</td>
						<td>$NamaSpc</td>
					</tr>
					<tr>
						<td>Session</td>
						<td>:</td>
						<td>$RencanaKehadiran</td>
					</tr>
					<tr>
						<td>Lecturer</td>
						<td>:</td>
						<td>$NamaDosen1</td>
					</tr>
					<tr>
						<td>Assistant 1</td>
						<td>:</td>
						<td>$NamaDosen2</td>
					</tr>
					<tr>
						<td>Assistant 2</td>
						<td>:</td>
						<td>$NamaDosen3</td>
					</tr>
					<tr>
						<td>Assistant 3</td>
						<td>:</td>
						<td>$NamaDosen4</td>
					</tr>
					<tr>
						<td>Assistant 4</td>
						<td>:</td>
						<td>$NamaDosen5</td>
					</tr>
					<tr>
						<td>Assistant 5</td>
						<td>:</td>
						<td>$NamaDosen6</td>
					</tr>
				</table>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data has been updated.$pesan","../schedule/ptl_direct_session_form_update/$ProgramID/$TahunID");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data has been updated.$pesan","../schedule/ptl_direct_session_form_update/$ProgramID/$TahunID");
				}
				else
				{
					echo warning("Email server is not active. Your data has been updated.$pesan","../schedule/ptl_direct_session_form_update/$ProgramID/$TahunID");
				}
			}
		}
		
		function ptl_direct_session_form_update()
		{
			$this->authentification();
			$ProgramID = $this->uri->segment(3);
			$TahunID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ProgramID,
							'pk2' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED21",
							'aktifitas' => "Mengakses halaman Schedule - Direct Session Form Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->session->set_userdata('sch_filter_jur',$ProgramID);
			$this->session->set_userdata('sch_filter_tahun',$TahunID);
			redirect("schedule");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$HariID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $HariID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED22",
							'aktifitas' => "Mengakses halaman Schedule - Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_jadwal_hari->PTL_delete_jadwal_hari($JadwalID,$HariID);
			echo warning("Your data has been deleted permanently. Next, you have to remove Attendance manually.","../schedule");
		}
		
		function ptl_pdf()
		{
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED23",
							'aktifitas' => "Mengakses halaman Schedule - PDF.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('sch_filter_jur');
			$cektahun = $this->session->userdata('sch_filter_tahun');
			$result = $this->m_hari->PTL_all();
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(29,0.7,"SCHEDULE",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(29,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,28.5,2.5);
			$this->fpdf->Line(1,2.55,28.5,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(2.5 , 1, "REFERENCE" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": ".gmdate("ymdHis", time()-($ms)) , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(2.5, 0, "NIK" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": ".$id_akun , 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(2.5, -1, "NAME", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".$nama, 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(29, -2,"SCHEDULE DETAIL", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				if($no == $tot)
				{
					if($r->HariID > 0)
					{
						$this->fpdf->SetFont("Times","B",9);
						$this->fpdf->Ln();
						$this->fpdf->Cell(27.5, 1, strtoupper($r->Nama_en), "LBTR", 0, "C");
						$this->fpdf->SetFont("Times","B",8);
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.4, 0.4, "Class", "LB", 0, "C");
						$this->fpdf->Cell(9, 0.4, "Subject", "LB", 0, "C");
						$this->fpdf->Cell(3.5, 0.4, "Date", "LB", 0, "C");
						$this->fpdf->Cell(4, 0.4, "Specialization", "LB", 0, "C");
						$this->fpdf->Cell(5.5, 0.4, "Teacher", "LB", 0, "C");
						$this->fpdf->Cell(1.1, 0.4, "Room", "LB", 0, "C");
						$this->fpdf->Cell(3, 0.4, "Time", "LBR", 0, "C");
						$row1 = $this->m_kelas->PTL_select();
						$jumkelas = count($row1);
						$nokls = 1;
						if($row1)
						{
							foreach($row1 as $r1)
							{
								$ProgramID = $cekjurusan;
								$TahunID = $cektahun;
								$KelasID = $r1->KelasID;
								$row2 = $this->m_jadwal->PTL_all_spesifik($ProgramID,$TahunID,$KelasID);
								$js = count($row2);
								$min = 0;
								if($row2)
								{
									foreach($row2 as $r2)
									{
										$JadwalID = $r2->JadwalID;
										$HariID = $r->HariID;
										$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
										if(!$cls)
										{
											$min++;
										}
									}
								}
								$rowspan = $js - $min;
								$jumruang = 0;
								if($row2)
								{
									foreach($row2 as $r2)
									{
										$JadwalID = $r2->JadwalID;
										$HariID = $r->HariID;
										$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
										if($cls)
										{
											$jumruang++;
										}
									}
								}
								$cektype = $this->session->userdata('sch_filter_type');
								if($cektype == "")
								{
									$jumruang = $jumruang + 1;
								}
								if($this->session->userdata('sch_filter_kelas') == "")
								{
									if($jumruang > 0)
									{
										$this->fpdf->Ln();
										$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
										if($row2)
										{
											$rn = 1;
											foreach($row2 as $r2)
											{
												$SubjekID = $r2->SubjekID;
												$rs2 = $this->m_subjek->PTL_select($SubjekID);
												$DosenID = $r2->DosenID;
												$dsn = $this->m_dosen->PTL_select($DosenID);
												if($dsn)
												{
													$nmdsn = $dsn['Nama'];
												}
												else
												{
													$nmdsn = "";
												}
												$JadwalID = $r2->JadwalID;
												$HariID = $r->HariID;
												$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
												if($cls)
												{
													if($cls['RuangID'] == "")
													{
														$ruang = "Not Set";
													}
													else
													{
														$ruang = $cls['RuangID'];
													}
													$mulai = $cls['JamMulai'];
													$selesai = $cls['JamSelesai'];
												}
												else
												{
													$ruang = "";
													$mulai = "";
													$selesai = "";
												}
												if($cls)
												{
													if($rn == 1)
													{
														$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
														$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
														$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
														$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
														$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
														$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
													}
													else
													{
														$this->fpdf->Ln();
														$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
														$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
														$this->fpdf->Cell(3.5 , 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
														$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
														$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
														$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
														$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
													}
													$rn++;
												}
												else
												{
													if($jumkelas == $nokls)
													{
														$this->fpdf->Cell(26.5, 0.4, "", "BR", 0, "C");
													}
													else
													{
														$this->fpdf->Cell(26.5, 0.4, "", "R", 0, "C");
													}
												}
											}
										}
										else
										{
											if($jumkelas == $nokls)
											{
												$this->fpdf->Cell(26.5, 0.4, "", "BR", 0, "C");
											}
											else
											{
												$this->fpdf->Cell(26.1, 0.4, "", "R", 0, "C");
											}
										}
									}
								}
								else
								{
									if($KelasID == $this->session->userdata('sch_filter_kelas'))
									{
										if($jumruang > 0)
										{
											$this->fpdf->Ln();
											$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
											if($row2)
											{
												$rn = 1;
												foreach($row2 as $r2)
												{
													$SubjekID = $r2->SubjekID;
													$rs2 = $this->m_subjek->PTL_select($SubjekID);
													$DosenID = $r2->DosenID;
													$dsn = $this->m_dosen->PTL_select($DosenID);
													if($dsn)
													{
														$nmdsn = $dsn['Nama'];
													}
													else
													{
														$nmdsn = "";
													}
													$JadwalID = $r2->JadwalID;
													$HariID = $r->HariID;
													$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
													if($cls)
													{
														if($cls['RuangID'] == "")
														{
															$ruang = "Not Set";
														}
														else
														{
															$ruang = $cls['RuangID'];
														}
														$mulai = $cls['JamMulai'];
														$selesai = $cls['JamSelesai'];
													}
													else
													{
														$ruang = "";
														$mulai = "";
														$selesai = "";
													}
													if($cls)
													{
														if($rn == 1)
														{
															$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
															$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
															$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
															$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
															$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
															$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
														}
														else
														{
															$this->fpdf->Ln();
															$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
															$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
															$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
															$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
															$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
															$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
															$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
														}
														$rn++;
													}
													else
													{
														// $this->fpdf->Cell(26.5, 0.4, "", "BR", 0, "C");
													}
												}
											}
											else
											{
												$this->fpdf->Cell(26.1, 0.4, "", "BR", 0, "C");
											}
										}
									}
								}
								$nokls++;
							}
						}
					}
				}
				else
				{
					if($r->HariID > 0)
					{
						$this->fpdf->SetFont("Times","B",9);
						$this->fpdf->Ln();
						$this->fpdf->Cell(27.5, 1, strtoupper($r->Nama_en), "LBTR", 0, "C");
						$this->fpdf->SetFont("Times","B",8);
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.4, 0.4, "Class", "LB", 0, "C");
						$this->fpdf->Cell(9, 0.4, "Subject", "LB", 0, "C");
						$this->fpdf->Cell(3.5, 0.4, "Date", "LB", 0, "C");
						$this->fpdf->Cell(4, 0.4, "Specialization", "LB", 0, "C");
						$this->fpdf->Cell(5.5, 0.4, "Teacher", "LB", 0, "C");
						$this->fpdf->Cell(1.1, 0.4, "Room", "LB", 0, "C");
						$this->fpdf->Cell(3, 0.4, "Time", "LBR", 0, "C");
						$row1 = $this->m_kelas->PTL_select();
						if($row1)
						{
							foreach($row1 as $r1)
							{
								$ProgramID = $cekjurusan;
								$TahunID = $cektahun;
								$KelasID = $r1->KelasID;
								$row2 = $this->m_jadwal->PTL_all_spesifik($ProgramID,$TahunID,$KelasID);
								$js = count($row2);
								$min = 0;
								if($row2)
								{
									foreach($row2 as $r2)
									{
										$JadwalID = $r2->JadwalID;
										$HariID = $r->HariID;
										$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
										if(!$cls)
										{
											$min++;
										}
									}
								}
								$rowspan = $js - $min;
								$jumruang = 0;
								if($row2)
								{
									foreach($row2 as $r2)
									{
										$JadwalID = $r2->JadwalID;
										$HariID = $r->HariID;
										$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
										if($cls)
										{
											$jumruang++;
										}
									}
								}
								$cektype = $this->session->userdata('sch_filter_type');
								if($cektype == "")
								{
									$jumruang = $jumruang + 1;
								}
								if($this->session->userdata('sch_filter_kelas') == "")
								{
									if($jumruang > 0)
									{
										$this->fpdf->Ln();
										$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
										if($row2)
										{
											$rn = 1;
											foreach($row2 as $r2)
											{
												$SubjekID = $r2->SubjekID;
												$rs2 = $this->m_subjek->PTL_select($SubjekID);
												$DosenID = $r2->DosenID;
												$dsn = $this->m_dosen->PTL_select($DosenID);
												if($dsn)
												{
													$nmdsn = $dsn['Nama'];
												}
												else
												{
													$nmdsn = "";
												}
												$JadwalID = $r2->JadwalID;
												$HariID = $r->HariID;
												$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
												if($cls)
												{
													if($cls['RuangID'] == "")
													{
														$ruang = "Not Set";
													}
													else
													{
														$ruang = $cls['RuangID'];
													}
													$mulai = $cls['JamMulai'];
													$selesai = $cls['JamSelesai'];
												}
												else
												{
													$ruang = "";
													$mulai = "";
													$selesai = "";
												}
												if($cls)
												{
													if($rn == 1)
													{
														$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
														$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
														$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
														$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
														$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
														$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
													}
													else
													{
														$this->fpdf->Ln();
														$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
														$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
														$this->fpdf->Cell(3.5 , 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
														$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
														$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
														$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
														$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
													}
													$rn++;
												}
												else
												{
													// $this->fpdf->Cell(23.5, 0.4, "", "R", 0, "C");
												}
											}
										}
										else
										{
											$this->fpdf->Cell(26.1, 0.4, "", "R", 0, "C");
										}
									}
								}
								else
								{
									if($KelasID == $this->session->userdata('sch_filter_kelas'))
									{
										if($jumruang > 0)
										{
											$this->fpdf->Ln();
											$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
											if($row2)
											{
												$rn = 1;
												foreach($row2 as $r2)
												{
													$SubjekID = $r2->SubjekID;
													$rs2 = $this->m_subjek->PTL_select($SubjekID);
													$DosenID = $r2->DosenID;
													$dsn = $this->m_dosen->PTL_select($DosenID);
													if($dsn)
													{
														$nmdsn = $dsn['Nama'];
													}
													else
													{
														$nmdsn = "";
													}
													$JadwalID = $r2->JadwalID;
													$HariID = $r->HariID;
													$cls = $this->m_jadwal_hari->PTL_select_spesifik($JadwalID,$HariID);
													if($cls)
													{
														if($cls['RuangID'] == "")
														{
															$ruang = "Not Set";
														}
														else
														{
															$ruang = $cls['RuangID'];
														}
														$mulai = $cls['JamMulai'];
														$selesai = $cls['JamSelesai'];
													}
													else
													{
														$ruang = "";
														$mulai = "";
														$selesai = "";
													}
													if($cls)
													{
														if($rn == 1)
														{
															$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
															$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
															$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
															$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
															$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
															$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
														}
														else
														{
															$this->fpdf->Ln();
															$this->fpdf->Cell(1.4, 0.4, "O".$r1->Nama, "LBTR", 0, "C");
															$this->fpdf->Cell(9, 0.4, $rs2["Nama"], "LBTR", 0, "L");
															$this->fpdf->Cell(3.5, 0.4, tgl_indo($r2->TglMulai)." - ".tgl_indo($r2->TglSelesai), "LBTR", 0, "L");
															$this->fpdf->Cell(4, 0.4, "", "LBTR", 0, "C");
															$this->fpdf->Cell(5.5, 0.4, $nmdsn, "LBTR", 0, "L");
															$this->fpdf->Cell(1.1, 0.4, $ruang, "LBTR", 0, "C");
															$this->fpdf->Cell(3, 0.4, "$mulai - $selesai", "LBTR", 0, "C");
														}
														$rn++;
													}
													else
													{
														// $this->fpdf->Cell(26.5, 0.4, "", "R", 0, "C");
													}
												}
											}
											else
											{
												$this->fpdf->Cell(26.1, 0.4, "", "R", 0, "C");
											}
										}
									}
								}
							}
						}
					}
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Schedule_$id_akun-$nama.pdf","I");
		}
		
		function ptl_delete_all()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCHED24",
							'aktifitas' => "Mengakses halaman Schedule - Delete All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_jadwal->PTL_delete($JadwalID);
			$this->m_jadwal_hari->PTL_delete_jadwal($JadwalID);
			$this->m_presensi->PTL_delete_jadwal($JadwalID);
			$this->m_presensi_mahasiswa->PTL_delete_jadwal($JadwalID);
			echo warning("Your data has been deleted permanently.","../schedule");
		}
	}
?>