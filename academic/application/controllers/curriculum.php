<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Curriculum extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_kurikulum');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$data['rowrecord'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Curriculum/v_Curriculum',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$this->load->view('Portal/v_header');
			$this->load->view('Curriculum/v_Curriculum_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'KurikulumKode' => $this->input->post('KurikulumKode'),
						'Nama' => $this->input->post('Nama'),
						'Sesi' => $this->input->post('Sesi'),
						'JmlSesi' => $this->input->post('JmlSesi'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_kurikulum->PTL_insert($data);
			echo warning("Your data successfully added.","../curriculum");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$KurikulumID = $this->uri->segment(3);
			$result = $this->m_kurikulum->PTL_select($KurikulumID);
			$data['KurikulumID'] = $result['KurikulumID'];
			$data['KurikulumKode'] = $result['KurikulumKode'];
			$data['Nama'] = $result['Nama'];
			$data['Sesi'] = $result['Sesi'];
			$data['JmlSesi'] = $result['JmlSesi'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$this->load->view('Portal/v_header');
			$this->load->view('Curriculum/v_Curriculum_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$KurikulumID = $this->input->post('KurikulumID');
			$data = array(
						'KurikulumKode' => $this->input->post('KurikulumKode'),
						'Nama' => $this->input->post('Nama'),
						'Sesi' => $this->input->post('Sesi'),
						'JmlSesi' => $this->input->post('JmlSesi'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_kurikulum->PTL_update($KurikulumID,$data);
			echo warning("Your data successfully updated.","../curriculum");
		}
		
		function ptl_disabled()
		{
			$this->authentification();
			$KurikulumID = $this->uri->segment(3);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_kurikulum->PTL_update($KurikulumID,$data);
			echo warning("Your data successfully disabled.","../curriculum");
		}
		
		function ptl_enabled()
		{
			$this->authentification();
			$result = $this->m_kurikulum->PTL_all_active();
			if(count($result) > 2)
			{
				echo warning("Curriculum may only active one. Disable all Curriculum and try again.","../Curriculum");
			}
			else
			{
				$KurikulumID = $this->uri->segment(3);
				$data = array(
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'NA' => 'N'
							);
				$this->m_kurikulum->PTL_update($KurikulumID,$data);
				echo warning("Your data successfully enabled.","../curriculum");
			}
		}
	}
?>