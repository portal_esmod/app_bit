<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Lecturer_calendar extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aktifitas');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_kalender');
			$this->load->model('m_kalender_jenis');
			$this->load->model('m_kelas');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACALC01-01",
							'aktifitas' => "Filter halaman Lecturer Calendar - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('lc_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('lc_filter_jur');
			}
			redirect("lecturer_calendar");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACALC01-02",
							'aktifitas' => "Filter halaman Lecturer Calendar - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('lc_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('lc_filter_tahun');
			}
			redirect("lecturer_calendar");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			$datalog = array(
							'pk1' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACALC01-03",
							'aktifitas' => "Filter halaman Lecturer Calendar - Lecturer: $cekdosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekdosen != "")
			{
				$this->session->set_userdata('lc_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('lc_filter_dosen');
			}
			redirect("lecturer_calendar");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','lecturer_calendar');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACALC02",
							'aktifitas' => "Mengakses halaman Lecturer Calendar.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('lc_filter_jur');
			$cektahun = $this->session->userdata('lc_filter_tahun');
			$cekdosen = $this->session->userdata('lc_filter_dosen');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			if($cektahun == "")
			{
				$dataf['rowrecord'] = "";
			}
			else
			{
				$dataf['rowrecord'] = $this->m_presensi->PTL_all_spesifik_lecturer_calendar($cektahun,$cekdosen);
			}
			$this->load->view('Portal/v_header_kalender');
			$this->load->view('Lecturer_Calendar/v_lecturer_calendar',$data);
			$this->load->view('Portal/v_footer_kalender_dosen',$dataf);
		}
	}
?>