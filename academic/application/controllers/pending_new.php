<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Pending_new extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->model('m_aplikan');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_status_awal');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_pending_pmb_period()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('academic_filter_pending_new_period',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('academic_filter_pending_new_period','');
			}
			redirect("pending_new");
		}
		
		function ptl_filter_pending_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('academic_filter_pending_new_jur',$cekjurusan);
			}
			else
			{
				$this->session->set_userdata('academic_filter_pending_new_jur','');
			}
			redirect("pending_new");
		}
		
		function ptl_filter_pending_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('academic_filter_pending_new_prodi',$cekprodi);
			}
			else
			{
				$this->session->set_userdata('academic_filter_pending_new_prodi','');
			}
			redirect("pending_new");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$cekperiode = $this->session->userdata('academic_filter_pending_new_period');
			$cekjurusan = $this->session->userdata('academic_filter_pending_new_jur');
			$cekprodi = $this->session->userdata('academic_filter_pending_new_prodi');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_spesifik_semester_pending($cekperiode,$cekjurusan,$cekprodi);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Pending_new/v_pending_new',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>