<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Homeroom extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_aktifitas');
			$this->load->model('m_dosen');
			$this->load->model('m_hari');
			$this->load->model('m_jadwal');
			$this->load->model('m_jadwal_hari');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_subjek');
			$this->load->model('m_status');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-01",
							'aktifitas' => "Filter halaman Homeroom - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('hmr_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('hmr_filter_jur');
			}
			redirect("homeroom");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-02",
							'aktifitas' => "Filter halaman Homeroom - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('hmr_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('hmr_filter_tahun');
			}
			redirect("homeroom");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			$datalog = array(
							'pk1' => $cekprodi,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-03",
							'aktifitas' => "Filter halaman Homeroom - Prodi: $cekprodi.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi != "")
			{
				$this->session->set_userdata('hmr_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('hmr_filter_prodi');
			}
			redirect("homeroom");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			$datalog = array(
							'pk1' => $cekkelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-04",
							'aktifitas' => "Filter halaman Homeroom - Class: $cekkelas.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkelas != "")
			{
				if($cekkelas == "_")
				{
					$this->session->unset_userdata('hmr_filter_kelas');
				}
				else
				{
					$this->session->set_userdata('hmr_filter_kelas',$cekkelas);
				}
			}
			else
			{
				$this->session->unset_userdata('hmr_filter_kelas');
			}
			redirect("homeroom");
		}
		
		function ptl_set_head()
		{
			$this->authentification();
			$sethead = $this->input->post('sethead');
			$datalog = array(
							'pk1' => $sethead,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-05",
							'aktifitas' => "Filter halaman Homeroom - Homeroom: $sethead.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($sethead != "")
			{
				$this->session->set_userdata('hmr_set_head',$sethead);
			}
			else
			{
				$this->session->unset_userdata('hmr_set_head');
			}
			redirect("homeroom");
		}
		
		function ptl_set_vice()
		{
			$this->authentification();
			$setvice = $this->input->post('setvice');
			$datalog = array(
							'pk1' => $setvice,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER01-06",
							'aktifitas' => "Filter halaman Homeroom - Vice Homeroom: $setvice.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($setvice != "")
			{
				$this->session->set_userdata('hmr_set_vice',$setvice);
			}
			else
			{
				$this->session->unset_userdata('hmr_set_vice');
			}
			redirect("homeroom");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','homeroom');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER02",
							'aktifitas' => "Mengakses halaman Homeroom.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('hmr_filter_jur');
			$cektahun = $this->session->userdata('hmr_filter_tahun');
			$cekprodi = $this->session->userdata('hmr_filter_prodi');
			$cekkelas = $this->session->userdata('hmr_filter_kelas');
			$ck = "";
			if($cekkelas != "")
			{
				$ck = $cekkelas;
			}
			$word = explode("_",$ck);
			$MatchTahun = @$word[0];
			$MatchKelas = @$word[1];
			$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_homeroom($cekjurusan,$cektahun,$cekprodi,$MatchTahun,$MatchKelas);
			// $data['rowrecord'] = $this->m_khs->PTL_all_spesifik($cekjurusan,$cektahun,$cekprodi,$cekkelas);
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowprodi'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi'] = $this->m_kursussingkat->PTL_all();
			}
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			// $data['rowkelas'] = $this->m_kelas->PTL_all($cekjurusan);
			$data['rowdosen1'] = $this->m_dosen->PTL_all();
			$data['rowdosen2'] = $this->m_dosen->PTL_all();
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Homeroom/v_homeroom',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set_all()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			for($n=1;$n<=$total;$n++)
			{
				$KHSID = $this->input->post("KHSID$n");
				$data = array(
							'WaliKelasID' => $this->input->post("WaliKelasID$n"),
							'WaliKelasID2' => $this->input->post("WaliKelasIDB$n"),
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
			}
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "HOMER03",
							'aktifitas' => "Mengakses halaman Homeroom - Set All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			echo warning("Your data successfully updated.","../homeroom");
		}
	}
?>