<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Subject extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_jenis_mk');
			$this->load->model('m_kurikulum');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_subjek');
			$this->load->model('m_subjek_grup');
			$this->load->model('m_subjek_percentage');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_kur()
		{
			$this->authentification();
			$cekkurikulum = $this->input->post('cekkurikulum');
			if($cekkurikulum != "")
			{
				$this->session->set_userdata('subject_filter_kur',$cekkurikulum);
			}
			else
			{
				$this->session->unset_userdata('subject_filter_kur');
			}
			redirect("subject");
		}
		
		function ptl_filter_kur1()
		{
			$this->authentification();
			$cekkurikulum1 = $this->input->post('cekkurikulum1');
			if($cekkurikulum1 != "")
			{
				$this->session->set_userdata('subject_filter_kur1',$cekkurikulum1);
			}
			else
			{
				$this->session->unset_userdata('subject_filter_kur1');
			}
			redirect("subject/ptl_copy");
		}
		
		function ptl_filter_kur2()
		{
			$this->authentification();
			$cekkurikulum2 = $this->input->post('cekkurikulum2');
			if($cekkurikulum2 != "")
			{
				$this->session->set_userdata('subject_filter_kur2',$cekkurikulum2);
			}
			else
			{
				$this->session->unset_userdata('subject_filter_kur2');
			}
			redirect("subject/ptl_copy");
		}
		
		function ptl_set_check_all1()
		{
			$this->authentification();
			$this->session->set_userdata('subject_set_all1','Y');
			redirect("subject/ptl_copy");
		}
		
		function ptl_set_uncheck_all1()
		{
			$this->authentification();
			$this->session->unset_userdata('subject_set_all1');
			redirect("subject/ptl_copy");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('subject_filter_kur');
			$data['rowrecord'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Subject/v_subject',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_percentage_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$SubjekID = $this->uri->segment(3);
			$id_subjek_percentage = $this->uri->segment(4);
			$result = $this->m_subjek->PTL_select($SubjekID);
			$data['SubjekID'] = $result['SubjekID'];
			$data['SubjekKode'] = $result['SubjekKode'];
			$data['Nama'] = $result['Nama'];
			$data['SKS'] = $result['SKS'];
			$data['Sesi'] = $result['Sesi'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['Optional'] = $result['Optional'];
			$data['rowrecord'] = $this->m_subjek_percentage->PTL_all_subjek($SubjekID);
			if($id_subjek_percentage != "")
			{
				$result = $this->m_subjek_percentage->PTL_select($id_subjek_percentage);
				$data['id_subjek_percentage'] = $result['id_subjek_percentage'];
				$data['nama'] = $result['nama'];
				$data['persentase'] = $result['persentase'];
				$this->load->view('Portal/v_header');
				$this->load->view('Subject/v_subject_percentage_edit',$data);
				$this->load->view('Portal/v_footer');
			}
			else
			{
				$this->load->view('Portal/v_header');
				$this->load->view('Subject/v_subject_percentage_form',$data);
				$this->load->view('Portal/v_footer');
			}
		}
		
		function ptl_percentage_insert()
		{
			$this->authentification();
			$SubjekID = $this->input->post('SubjekID');
			$persentase = $this->input->post('persentase');
			$Total = $this->input->post('Total');
			if($persentase > $Total)
			{
				echo warning("Sorry, your input value is wrong. Try again...","../subject/ptl_percentage_form/$SubjekID");
			}
			else
			{
				$data = array(
							'SubjekID' => $SubjekID,
							'nama' => $this->input->post('nama'),
							'persentase' => $persentase,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_subjek_percentage->PTL_insert($data);
				echo warning("Your data successfully added.","../subject/ptl_percentage_form/$SubjekID");
			}
		}
		
		function ptl_percentage_update()
		{
			$this->authentification();
			$id_subjek_percentage = $this->input->post('id_subjek_percentage');
			$SubjekID = $this->input->post('SubjekID');
			$persentase = $this->input->post('persentase');
			$Total = $this->input->post('Total');
			if($persentase > $Total)
			{
				echo warning("Sorry, your input value is wrong. Try again...","../subject/ptl_percentage_form/$SubjekID");
			}
			else
			{
				$data = array(
							'nama' => $this->input->post('nama'),
							'persentase' => $persentase,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_subjek_percentage->PTL_update($id_subjek_percentage,$data);
				echo warning("Your data successfully changed.","../subject/ptl_percentage_form/$SubjekID");
			}
		}
		
		function ptl_percentage_delete()
		{
			$this->authentification();
			$id_subjek_percentage = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$data = array(
						'na' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_subjek_percentage->PTL_update($id_subjek_percentage,$data);
			echo warning("Your data successfully deleted.","../subject/ptl_percentage_form/$SubjekID");
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$data['KurikulumID'] = $this->uri->segment(3);
			$data['rowjenis'] = $this->m_jenis_mk->PTL_all();
			$data['rowgrup'] = $this->m_subjek_grup->PTL_all();
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$this->load->view('Portal/v_header');
			$this->load->view('Subject/v_subject_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$SubjekKode = strtoupper($this->input->post('SubjekKode'));
			$KurikulumID = $this->input->post('KurikulumID');
			$result = $this->m_subjek->PTL_select_kode($SubjekKode,$KurikulumID);
			if($result)
			{
				echo warning("The code has been used '".$result["Nama"]."', replace it with any other code.","../subject/ptl_form/$KurikulumID");
			}
			else
			{
				$total = $this->input->post('total');
				$prodi = "";
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("ProdiID$i") != "")
					{
						$prodi .= $this->input->post("ProdiID$i").".";
					}
				}
				$optional = "N";
				if($this->input->post('Optional') == "Y")
				{
					$optional = "Y";
				}
				$data = array(
							'SubjekKode' => $SubjekKode,
							'KurikulumID' => $KurikulumID,
							'Nama' => $this->input->post('Nama'),
							'SKS' => $this->input->post('SKS'),
							'Sesi' => $this->input->post('Sesi'),
							'JenisMKID' => $this->input->post('JenisMKID'),
							'SubjekGroupID' => $this->input->post('SubjekGroupID'),
							'ProdiID' => $prodi,
							'Optional' => $optional,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_subjek->PTL_insert($data);
				echo warning("Your data successfully added.","../subject");
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$SubjekID = $this->uri->segment(3);
			$result = $this->m_subjek->PTL_select($SubjekID);
			$data['SubjekID'] = $result['SubjekID'];
			$data['SubjekKode'] = $result['SubjekKode'];
			$data['KurikulumID'] = $result['KurikulumID'];
			$data['Nama'] = $result['Nama'];
			$data['SKS'] = $result['SKS'];
			$data['Sesi'] = $result['Sesi'];
			$data['JenisMKID'] = $result['JenisMKID'];
			$data['SubjekGroupID'] = $result['SubjekGroupID'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['Optional'] = $result['Optional'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['rowjenis'] = $this->m_jenis_mk->PTL_all();
			$data['rowgrup'] = $this->m_subjek_grup->PTL_all();
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$this->load->view('Portal/v_header');
			$this->load->view('Subject/v_subject_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$SubjekID = $this->input->post('SubjekID');
			$KurikulumID = $this->input->post('KurikulumID');
			$SubjekKode = strtoupper($this->input->post('SubjekKode'));
			$result = $this->m_subjek->PTL_select_kode($SubjekKode,$KurikulumID);
			if(($result) AND ($SubjekID != $result["SubjekID"]))
			{
				echo warning("The code has been used '".$result["Nama"]."', replace it with any other code.","../subject/ptl_edit/$SubjekID");
			}
			else
			{
				$total = $this->input->post('total');
				$prodi = "";
				for($i=1;$i<=$total;$i++)
				{
					if($this->input->post("ProdiID$i") != "")
					{
						$prodi .= $this->input->post("ProdiID$i").".";
					}
				}
				$optional = "N";
				if($this->input->post('Optional') == "Y")
				{
					$optional = "Y";
				}
				$na = "N";
				if($this->input->post('NA') == "Y")
				{
					$na = "Y";
				}
				$data = array(
							'SubjekKode' => $SubjekKode,
							'Nama' => $this->input->post('Nama'),
							'SKS' => $this->input->post('SKS'),
							'Sesi' => $this->input->post('Sesi'),
							'JenisMKID' => $this->input->post('JenisMKID'),
							'SubjekGroupID' => $this->input->post('SubjekGroupID'),
							'ProdiID' => $prodi,
							'Optional' => $optional,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'NA' => $na
							);
				$this->m_subjek->PTL_update($SubjekID,$data);
				echo warning("Your data successfully updated.","../subject");
			}
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('subject_filter_kur1');
			$data['rowrecord1'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$cekkurikulum = $this->session->userdata('subject_filter_kur2');
			$data['rowrecord2'] = $this->m_subjek->PTL_all_spesifik($cekkurikulum);
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Subject/v_subject_copy',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set_copy()
		{
			$this->authentification();
			$total = $this->input->post("total1");
			$nsave = 0;
			$nover = 0;
			$cekkurikulum = $this->session->userdata('subject_filter_kur2');
			if($cekkurikulum == "")
			{
				echo warning("You have not set the copy destination.","../subject/ptl_copy");
			}
			else
			{
				for($i=1;$i<=$total;$i++)
				{
					$SubjekID = $this->input->post("cek$i");
					if($SubjekID != "")
					{
						$result = $this->m_subjek->PTL_select($SubjekID);
						$SubjekKode = $result['SubjekKode'];
						$res = $this->m_subjek->PTL_select_kode_cur($SubjekKode,$cekkurikulum);
						if($res)
						{
							$nover++;
						}
						else
						{
							$data = array(
										'SubjekKode' => $SubjekKode,
										'KurikulumID' => $cekkurikulum,
										'Nama' => $result['Nama'],
										'SKS' => $result['SKS'],
										'Sesi' => $result['Sesi'],
										'JenisMKID' => $result['JenisMKID'],
										'SubjekGroupID' => $result['SubjekGroupID'],
										'ProdiID' => $result['ProdiID'],
										'Optional' => $result['Optional'],
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu,
										'NA' => $result['NA']
										);
							$this->m_subjek->PTL_insert($data);
							$nsave++;
						}
					}
				}
				$over = "";
				if($nover > 0)
				{
					$over = " $nover data overlapping.";
				}
				if($nsave == 0)
				{
					echo warning("You don't save any data.$over","../subject/ptl_copy");
				}
				else
				{
					echo warning("Your data has been saved. $nsave data saved.$over","../subject/ptl_copy");
				}
			}
		}
	}
?>