<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Attendance_problem extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-01",
							'aktifitas' => "Filter halaman Attendance Problem - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('attpr_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_jur');
			}
			redirect("attendance_problem");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			$datalog = array(
							'pk1' => $cekprodi,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-02",
							'aktifitas' => "Filter halaman Attendance Problem - Prodi: $cekprodi.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi != "")
			{
				$this->session->set_userdata('attpr_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_prodi');
			}
			redirect("attendance_problem");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-03",
							'aktifitas' => "Filter halaman Attendance Problem - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('attpr_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_tahun');
			}
			$this->session->unset_userdata('attpr_filter_sorting');
			redirect("attendance_problem");
		}
		
		function ptl_filter_tingkat()
		{
			$this->authentification();
			$cektingkat = $this->input->post('cektingkat');
			$datalog = array(
							'pk1' => $cektingkat,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-04",
							'aktifitas' => "Filter halaman Attendance Problem - Level: $cektingkat.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektingkat != "")
			{
				$this->session->set_userdata('attpr_filter_tingkat',$cektingkat);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_tingkat');
			}
			$this->session->unset_userdata('attpr_filter_sorting');
			redirect("attendance_problem");
		}
		
		function ptl_filter_subjek()
		{
			$this->authentification();
			$ceksubjek = $this->input->post('ceksubjek');
			$datalog = array(
							'pk1' => $ceksubjek,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-05",
							'aktifitas' => "Filter halaman Attendance Problem - Subject: $ceksubjek.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksubjek != "")
			{
				$this->session->set_userdata('attpr_filter_subjek',$ceksubjek);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_subjek');
			}
			redirect("attendance_problem");
		}
		
		function ptl_filter_sorting()
		{
			$this->authentification();
			$ceksorting = $this->input->post('ceksorting');
			$datalog = array(
							'pk1' => $ceksorting,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-06",
							'aktifitas' => "Filter halaman Attendance Problem - Sort: $ceksorting.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksorting != "")
			{
				$this->session->set_userdata('attpr_filter_sorting',$ceksorting);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_sorting');
			}
			redirect("attendance_problem");
		}
		
		function ptl_filter_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			$datalog = array(
							'pk1' => $cekstatus,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR01-07",
							'aktifitas' => "Filter halaman Attendance Problem - Status: $cekstatus.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekstatus != "")
			{
				$this->session->set_userdata('attpr_filter_status',$cekstatus);
			}
			else
			{
				$this->session->unset_userdata('attpr_filter_status');
			}
			redirect("attendance_problem");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','evaluation');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTPR02",
							'aktifitas' => "Mengakses halaman Attendance Problem.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('attpr_filter_jur');
			$cekprodi = $this->session->userdata('attpr_filter_prodi');
			$cektahun = $this->session->userdata('attpr_filter_tahun');
			$cektingkat = $this->session->userdata('attpr_filter_tingkat');
			$ceksubjek = $this->session->userdata('attpr_filter_subjek');
			$ceksorting = $this->session->userdata('attpr_filter_sorting');
			$cekstatus = $this->session->userdata('attpr_filter_status');
			if($cekstatus == "")
			{
				$data['TotalProblem'] = "Absent";
			}
			else
			{
				$data['TotalProblem'] = $cekstatus;
			}
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik_aktif($cekjurusan);
			$data['rowprodi'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi'] = $this->m_kursussingkat->PTL_all();
			}
			if($ceksorting == "")
			{
				$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year($cekjurusan,$cekprodi,$cektahun,$cektingkat);
			}
			else
			{
				if($ceksubjek == "")
				{
					$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year_attendance_problem($cekjurusan,$cekprodi,$cektahun,$cektingkat);
				}
				else
				{
					$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year_attendance_problem_subjek($cekjurusan,$cekprodi,$cektahun,$cektingkat,$ceksubjek);
				}
			}
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if($ceksubjek == "")
			{
				$this->load->view('Attendance_problem/v_attendance_problem',$data);
			}
			else
			{
				$this->load->view('Attendance_problem/v_attendance_problem_subjek',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
	}
?>