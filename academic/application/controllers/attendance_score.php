<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Attendance_score extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_jenis_mk');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kurikulum');
			$this->load->model('m_nilai3');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_kur()
		{
			$this->authentification();
			$cekkurikulum = $this->input->post('cekkurikulum');
			if($cekkurikulum != "")
			{
				$this->session->set_userdata('att_scr_filter_kur',$cekkurikulum);
			}
			else
			{
				$this->session->unset_userdata('att_scr_filter_kur');
			}
			redirect("attendance_score");
		}
		
		function ptl_filter_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			if($cektype != "")
			{
				$this->session->set_userdata('att_scr_filter_type',$cektype);
			}
			else
			{
				$this->session->unset_userdata('att_scr_filter_type');
			}
			redirect("attendance_score");
		}
		
		function ptl_filter_kur1()
		{
			$this->authentification();
			$cekkurikulum1 = $this->input->post('cekkurikulum1');
			if($cekkurikulum1 != "")
			{
				$this->session->set_userdata('att_scr_filter_kur1',$cekkurikulum1);
			}
			else
			{
				$this->session->unset_userdata('att_scr_filter_kur1');
			}
			redirect("attendance_score/ptl_copy");
		}
		
		function ptl_filter_kur2()
		{
			$this->authentification();
			$cekkurikulum2 = $this->input->post('cekkurikulum2');
			if($cekkurikulum2 != "")
			{
				$this->session->set_userdata('att_scr_filter_kur2',$cekkurikulum2);
			}
			else
			{
				$this->session->unset_userdata('att_scr_filter_kur2');
			}
			redirect("attendance_score/ptl_copy");
		}
		
		function ptl_set_check_all1()
		{
			$this->authentification();
			$this->session->set_userdata('att_scr_set_all1','Y');
			redirect("attendance_score/ptl_copy");
		}
		
		function ptl_set_uncheck_all1()
		{
			$this->authentification();
			$this->session->unset_userdata('att_scr_set_all1');
			redirect("attendance_score/ptl_copy");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('att_scr_filter_kur');
			$cektype = $this->session->userdata('att_scr_filter_type');
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$data['rowtype'] = $this->m_jenis_mk->PTL_all();
			$data['rowrecord'] = $this->m_nilai3->PTL_all_spesifik_as($cekkurikulum,$cektype);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Score_Attendance/v_score_attendance',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$Nilai3ID = $this->uri->segment(3);
			$data['link'] = $this->uri->segment(4);
			$data['JadwalID'] = $this->uri->segment(5);
			if($data['link'] == "SCORING")
			{
				$this->session->set_userdata('menu','scoring');
			}
			else
			{
				$this->session->set_userdata('menu','system');
			}
			$result = $this->m_nilai3->PTL_select($Nilai3ID);
			$data['Nilai3ID'] = $result['Nilai3ID'];
			$data['JenisPresensiID'] = $result['JenisPresensiID'];
			$data['Score'] = $result['Score'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$JenisMKID = $result['JenisMKID'];
			if($JenisMKID == "5")
			{
				$data['MaxScore'] = 20;
			}
			else
			{
				$data['MaxScore'] = 4;
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Score_Attendance/v_score_attendance_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$Nilai3ID = $this->input->post('Nilai3ID');
			$link = $this->input->post('link');
			$data = array(
						'Score' => str_replace(",",".",$this->input->post('Score')),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_nilai3->PTL_update($Nilai3ID,$data);
			$JadwalID = $this->input->post('JadwalID');
			if($link == "SCORING")
			{
				echo warning("Your data successfully updated.","../scoring/ptl_list/$JadwalID");
			}
			else
			{
				if($link == "ATTENDANCE")
				{
					echo warning("Your data successfully updated.","../attendance/ptl_list/$JadwalID");
				}
				else
				{
					echo warning("Your data successfully updated.","../attendance_score");
				}
			}
		}
		
		function ptl_copy()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$cekkurikulum = $this->session->userdata('att_scr_filter_kur1');
			$data['rowrecord1'] = $this->m_nilai3->PTL_all_copy($cekkurikulum);
			$cekkurikulum = $this->session->userdata('att_scr_filter_kur2');
			$data['rowrecord2'] = $this->m_nilai3->PTL_all_copy($cekkurikulum);
			$data['rowjenismk'] = $this->m_jenis_mk->PTL_all();
			$data['rowkurikulum'] = $this->m_kurikulum->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Score_Attendance/v_score_attendance_copy',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_set_copy()
		{
			$this->authentification();
			$total = $this->input->post("total1");
			$nsave = 0;
			$nover = 0;
			$KurikulumID = $this->session->userdata('att_scr_filter_kur2');
			if($KurikulumID == "")
			{
				echo warning("You have not set the copy destination.","../attendance_score/ptl_copy");
			}
			else
			{
				for($i=1;$i<=$total;$i++)
				{
					$Nilai3ID = $this->input->post("cek$i");
					if($Nilai3ID != "")
					{
						$result = $this->m_nilai3->PTL_select($Nilai3ID);
						$JenisMKID = $result['JenisMKID'];
						$JenisPresensiID = $result['JenisPresensiID'];
						$res = $this->m_nilai3->PTL_select_attendance_copy($KurikulumID,$JenisMKID,$JenisPresensiID);
						if($res)
						{
							$nover++;
						}
						else
						{
							$data = array(
										'KurikulumID' => $KurikulumID,
										'JenisMKID' => $JenisMKID,
										'JenisPresensiID' => $result['JenisPresensiID'],
										'Score' => $result['Score'],
										'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_buat' => $this->waktu,
										'NA' => $result['NA']
										);
							$this->m_nilai3->PTL_insert($data);
							$nsave++;
						}
					}
				}
				$over = "";
				if($nover > 0)
				{
					$over = " $nover data overlapping.";
				}
				if($nsave == 0)
				{
					echo warning("You don't save any data.$over","../attendance_score/ptl_copy");
				}
				else
				{
					echo warning("Your data has been saved. $nsave data saved.$over","../attendance_score/ptl_copy");
				}
			}
		}
	}
?>