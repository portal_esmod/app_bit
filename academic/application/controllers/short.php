<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Short extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_outline');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_kursussingkat->PTL_all_list();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Short/v_short',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$this->load->view('Portal/v_header');
			$this->load->view('Short/v_short_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'KursusSingkatID' => strtoupper($this->input->post('KursusSingkatID')),
						'Nama' => $this->input->post('Nama'),
						'FormatNIM' => $this->input->post('FormatNIM'),
						'Waktu' => $this->input->post('Waktu').":00:00",
						'Biaya' => $this->input->post('Biaya'),
						'NA' => $na,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_kursussingkat->PTL_insert($data);
			echo warning("Your data successfully added.","../short");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$KursusSingkatID = $this->uri->segment(3);
			$result = $this->m_kursussingkat->PTL_select($KursusSingkatID);
			$data['KursusSingkatID'] = $result['KursusSingkatID'];
			$data['Nama'] = $result['Nama'];
			$data['FormatNIM'] = $result['FormatNIM'];
			$data['Waktu'] = $result['Waktu'];
			$data['Biaya'] = $result['Biaya'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Short/v_short_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$KursusSingkatID = $this->input->post('KursusSingkatID');
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'KursusSingkatID' => strtoupper($this->input->post('KursusSingkatID')),
						'Nama' => $this->input->post('Nama'),
						'FormatNIM' => $this->input->post('FormatNIM'),
						'Waktu' => $this->input->post('Waktu').":00:00",
						'Biaya' => $this->input->post('Biaya'),
						'NA' => $na,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_kursussingkat->PTL_update($KursusSingkatID,$data);
			echo warning("Your data successfully updated.","../short");
		}
		
		function ptl_outline_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['KursusSingkatID'] = $this->uri->segment(3);
			$this->load->view('Portal/v_header');
			$this->load->view('Short/v_outline_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_outline_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'KursusSingkatID' => $this->input->post('KursusSingkatID'),
						'Nama' => $this->input->post('Nama'),
						'Keterangan' => $this->input->post('Keterangan'),
						'NA' => $na,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_outline->PTL_insert($data);
			echo warning("Your data successfully added.","../short");
		}
		
		function ptl_outline_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$OutlineID = $this->uri->segment(3);
			$result = $this->m_outline->PTL_select($OutlineID);
			$data['OutlineID'] = $result['OutlineID'];
			$data['KursusSingkatID'] = $result['KursusSingkatID'];
			$data['Nama'] = $result['Nama'];
			$data['Keterangan'] = $result['Keterangan'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Short/v_outline_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_outline_update()
		{
			$this->authentification();
			$OutlineID = $this->input->post('OutlineID');
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'KursusSingkatID' => $this->input->post('KursusSingkatID'),
						'Nama' => $this->input->post('Nama'),
						'Keterangan' => $this->input->post('Keterangan'),
						'NA' => $na,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_outline->PTL_update($OutlineID,$data);
			echo warning("Your data successfully updated.","../short");
		}
	}
?>