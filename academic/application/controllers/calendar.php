<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Calendar extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_aktifitas');
			$this->load->model('m_kalender');
			$this->load->model('m_kalender_jenis');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA01",
							'aktifitas' => "Mengakses halaman Academic Calendar.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowrecord'] = $this->m_kalender->PTL_all_tahun_ini();
			$this->load->view('Portal/v_header_kalender');
			$this->load->view('Calendar/v_calendar');
			$this->load->view('Portal/v_footer_kalender',$data);
		}
		
		function ptl_list()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA02",
							'aktifitas' => "Mengakses halaman Academic Calendar - List.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowrecord'] = $this->m_kalender->PTL_all_active();
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Calendar/v_calendar_list',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_list_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA03",
							'aktifitas' => "Mengakses halaman Academic Calendar - List Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowjenis'] = $this->m_kalender_jenis->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Calendar/v_calendar_list_form',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_list_insert()
		{
			$this->authentification();
			$tanggal_mulai = $this->input->post('tanggal_mulai');
			$keterangan = $this->input->post('keterangan');
			$pesan = "";
			if($keterangan == "")
			{
				$pesan .= "Name, ";
			}
			if($tanggal_mulai == "")
			{
				$pesan .= "Start Date";
			}
			if($pesan != "")
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA04-N",
								'aktifitas' => "Mengakses halaman Academic Calendar - List Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You must fill $pesan.","../calendar/ptl_list_form");
			}
			else
			{
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA04-Y",
								'aktifitas' => "Mengakses halaman Academic Calendar - List Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$tidak_kuliah = "N";
				if($this->input->post('tidak_kuliah') == "Y") { $tidak_kuliah = "Y"; }
				$na = "N";
				if($this->input->post('na') == "Y") { $na = "Y"; }
				$tanggal_selesai = $this->input->post('tanggal_selesai');
				$jam_mulai = $this->input->post('jam_mulai');
				$jam_selesai = $this->input->post('jam_selesai');
				$data = array(
							'tanggal_mulai' => $tanggal_mulai,
							'tanggal_selesai' => $tanggal_selesai,
							'jam_mulai' => $jam_mulai,
							'jam_selesai' => $jam_selesai,
							'kode_tgl_mulai' => str_replace("-","",$tanggal_mulai),
							'kode_tgl_selesai' => str_replace("-","",$tanggal_selesai),
							'kode_tgl_jam_mulai' => str_replace(":","",$jam_mulai),
							'kode_tgl_jam_selesai' => str_replace(":","",$jam_selesai),
							'tidak_kuliah' => $tidak_kuliah,
							'id_kalender_jenis' => $this->input->post('id_kalender_jenis'),
							'keterangan' => $keterangan,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_kalender->PTL_insert($data);
				echo warning("Your data successfully added.","../calendar/ptl_list");
			}
		}
		
		function ptl_list_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$id_kalender = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $id_kalender,
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA05",
							'aktifitas' => "Mengakses halaman Academic Calendar - List Edit $id_kalender.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_kalender->PTL_select($id_kalender);
			$data['id_kalender'] = $result['id_kalender'];
			$data['keterangan'] = $result['keterangan'];
			$data['tanggal_mulai'] = $result['tanggal_mulai'];
			$data['tanggal_selesai'] = $result['tanggal_selesai'];
			$data['jam_mulai'] = $result['jam_mulai'];
			$data['jam_selesai'] = $result['jam_selesai'];
			$data['id_kalender_jenis'] = $result['id_kalender_jenis'];
			$data['tidak_kuliah'] = $result['tidak_kuliah'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['na'] = $result['na'];
			$data['rowjenis'] = $this->m_kalender_jenis->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Calendar/v_calendar_list_edit',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_list_update()
		{
			$this->authentification();
			$tanggal_mulai = $this->input->post('tanggal_mulai');
			$keterangan = $this->input->post('keterangan');
			$pesan = "";
			if($keterangan == "")
			{
				$pesan .= "Name, ";
			}
			if($tanggal_mulai == "")
			{
				$pesan .= "Start Date";
			}
			if($pesan != "")
			{
				$datalog = array(
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA06-N",
								'aktifitas' => "Mengakses halaman Academic Calendar - List Update.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You must fill $pesan.","../calendar/ptl_list_form");
			}
			else
			{
				$id_kalender = $this->input->post('id_kalender');
				$datalog = array(
								'pk1' => $id_kalender,
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA06-Y",
								'aktifitas' => "Mengakses halaman Academic Calendar - List Update $id_kalender.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$tidak_kuliah = "N";
				if($this->input->post('tidak_kuliah') == "Y") { $tidak_kuliah = "Y"; }
				$na = "N";
				if($this->input->post('na') == "Y") { $na = "Y"; }
				$tanggal_selesai = $this->input->post('tanggal_selesai');
				$jam_mulai = $this->input->post('jam_mulai');
				$jam_selesai = $this->input->post('jam_selesai');
				$data = array(
							'tanggal_mulai' => $tanggal_mulai,
							'tanggal_selesai' => $tanggal_selesai,
							'jam_mulai' => $jam_mulai,
							'jam_selesai' => $jam_selesai,
							'kode_tgl_mulai' => str_replace("-","",$tanggal_mulai),
							'kode_tgl_selesai' => str_replace("-","",$tanggal_selesai),
							'kode_tgl_jam_mulai' => str_replace(":","",$jam_mulai),
							'kode_tgl_jam_selesai' => str_replace(":","",$jam_selesai),
							'tidak_kuliah' => $tidak_kuliah,
							'id_kalender_jenis' => $this->input->post('id_kalender_jenis'),
							'keterangan' => $keterangan,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu,
							'na' => $na
							);
				$this->m_kalender->PTL_update($id_kalender,$data);
				echo warning("Your data successfully updated.","../calendar/ptl_list");
			}
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			$datalog = array(
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ACACA07",
							'aktifitas' => "Mengakses halaman Academic Calendar - Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowjenis'] = $this->m_kalender_jenis->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Calendar/v_calendar_form',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$tanggal_mulai = $this->input->post('tanggal_mulai');
			$keterangan = $this->input->post('keterangan');
			$pesan = "";
			if($keterangan == "")
			{
				$pesan .= "Name, ";
			}
			if($tanggal_mulai == "")
			{
				$pesan .= "Start Date";
			}
			if($pesan != "")
			{
				$datalog = array(
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA08-N",
								'aktifitas' => "Mengakses halaman Academic Calendar - Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You must fill $pesan.","../calendar/ptl_form");
			}
			else
			{
				$datalog = array(
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ACACA08-Y",
								'aktifitas' => "Mengakses halaman Academic Calendar - Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$tidak_kuliah = "N";
				if($this->input->post('tidak_kuliah') == "Y") { $tidak_kuliah = "Y"; }
				$na = "N";
				if($this->input->post('na') == "Y") { $na = "Y"; }
				$tanggal_selesai = $this->input->post('tanggal_selesai');
				$jam_mulai = $this->input->post('jam_mulai');
				$jam_selesai = $this->input->post('jam_selesai');
				$data = array(
							'tanggal_mulai' => $tanggal_mulai,
							'tanggal_selesai' => $tanggal_selesai,
							'jam_mulai' => $jam_mulai,
							'jam_selesai' => $jam_selesai,
							'kode_tgl_mulai' => str_replace("-","",$tanggal_mulai),
							'kode_tgl_selesai' => str_replace("-","",$tanggal_selesai),
							'kode_tgl_jam_mulai' => str_replace(":","",$jam_mulai),
							'kode_tgl_jam_selesai' => str_replace(":","",$jam_selesai),
							'tidak_kuliah' => $tidak_kuliah,
							'id_kalender_jenis' => $this->input->post('id_kalender_jenis'),
							'keterangan' => $this->input->post('keterangan'),
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_kalender->PTL_insert($data);
				echo warning("Your data successfully added.","../calendar");
			}
		}
		
		function ptl_delete_all()
		{
			$this->authentification();
			$na = "N";
			$data = array(
						'NA' => 'Y'
						);
			$this->m_kalender->PTL_update_delete_all($na,$data);
			echo warning("Your data successfully deleted.","../calendar/ptl_list");
		}
	}
?>