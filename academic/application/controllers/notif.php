<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Notif extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_aplikan_log');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','home');
			$data['rowrecord'] = $this->m_aplikan_log->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Notif/v_notif',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_detail()
		{
			$this->authentification();
			$this->session->set_userdata('menu','home');
			$kodelog = $this->uri->segment(3);
			$dat = array(
						'status' => 'READ',
						'tanggal_baca' => $this->waktu
						);
			$this->m_aplikan_log->PTL_update($kodelog,$dat);
			$result = $this->m_aplikan_log->PTL_select($kodelog);
			$data['kodelog'] = $result['kodelog'];
			$data['id_akun'] = $result['id_akun'];
			$data['presenter'] = $result['presenter'];
			$data['nama'] = $result['nama'];
			$word1 = explode("_",$result['dari']);
			$data['dari'] = $word1[1];
			$word2 = explode("_",$result['untuk']);
			$data['untuk'] = $word2[1];
			$data['aktivitas'] = $result['aktivitas'];
			$data['tanggal'] = $result['tanggal'];
			$data['status'] = $result['status'];
			$this->load->view('Portal/v_header');
			$this->load->view('Notif/v_notif_read',$data);
			$this->load->view('Portal/v_footer');
		}
	}
?>