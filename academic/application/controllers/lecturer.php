<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Lecturer extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_akun');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_kurikulum');
			$this->load->model('m_maintenance');
			$this->load->model('m_mk');
			$this->load->model('m_presensi');
			$this->load->model('m_religion');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$word = explode("_",$this->input->post('cekjurusan'));
			$cekjurusan = $word[0];
			$DosenID = $word[1];
			if($cekjurusan != "")
			{
				$this->session->set_userdata('lecturer_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('lecturer_filter_jur');
			}
			redirect("lecturer/ptl_attendance/$DosenID");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$word = explode("_",$this->input->post('cektahun'));
			$cektahun = $word[0];
			$DosenID = $word[1];
			if($cektahun != "")
			{
				$this->session->set_userdata('lecturer_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('lecturer_filter_tahun');
			}
			redirect("lecturer/ptl_attendance/$DosenID");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_dosen->PTL_all();
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				$this->load->view('Lecturer/v_lecturer_lecturer',$data);
			}
			else
			{
				$this->load->view('Lecturer/v_lecturer',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowagama'] = $this->m_religion->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowspc'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Lecturer/v_lecturer_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$admin_dp = '../hris/system_storage/foto_karyawan/';
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "FOTO_".$this->input->post('Login')."_".str_replace(' ','_',$this->input->post('nama'))."_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '51200';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			
			$totA = $this->input->post('totA');
			$SubjekID = "";
			for($iA=1;$iA<=$totA;$iA++)
			{
				if($this->input->post("sub$iA") != "")
				{
					$SubjekID .= $this->input->post("sub$iA").".";
				}
			}
			$totB = $this->input->post('totB');
			$SpesialisasiID = "";
			for($iB=1;$iB<=$totB;$iB++)
			{
				if($this->input->post("spc$iB") != "")
				{
					$SpesialisasiID .= $this->input->post("spc$iB").".";
				}
			}
			if(!$this->upload->do_upload())
			{
				$data = array(
							'Login' => $this->input->post('Login'),
							'Nama' => $this->input->post('Nama'),
							'TempatLahir' => $this->input->post('TempatLahir'),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'Kebangsaan' => $this->input->post('Kebangsaan'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'JumlahAnak' => $this->input->post('JumlahAnak'),
							'Agama' => $this->input->post('Agama'),
							'AlamatJakarta' => $this->input->post('AlamatJakarta'),
							'Telephone' => $this->input->post('Telephone'),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => $this->input->post('Email'),
							'Email2' => $this->input->post('Email2'),
							'SubjekID' => $SubjekID,
							'SpesialisasiID' => $SpesialisasiID,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_dosen->PTL_insert($data);
				echo warning("Profile Added except photo.","../lecturer");
			}
			else
			{
				$source_photo = "$admin_dp/$downloadin";
				$ukuran = fsize($source_photo);
				$word = explode(" ",$ukuran);
				$pesan_file = "";
				$ukuran_akhir = "";
				if(($word[1] == "KB") OR ($word[1] == "MB"))
				{
					$pesan_file .= " File uploads are eligible for less than 10 MB.";
					if($word[1] == "KB")
					{
						if($word[0] > 128)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units KB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
					else
					{
						if($word[0] >= 1)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units MB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
				}
				else
				{
					if($word[1] == "B")
					{
						$pesan_file .=  " File too small.";
					}
					else
					{
						$pesan_file .=  " File too large.";
					}
				}
				$data = array(
							'Login' => $this->input->post('Login'),
							'Nama' => $this->input->post('Nama'),
							'TempatLahir' => $this->input->post('TempatLahir'),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'Foto' => $dwn,
							'Kebangsaan' => $this->input->post('Kebangsaan'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'JumlahAnak' => $this->input->post('JumlahAnak'),
							'Agama' => $this->input->post('Agama'),
							'AlamatJakarta' => $this->input->post('AlamatJakarta'),
							'Telephone' => $this->input->post('Telephone'),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => $this->input->post('Email'),
							'Email2' => $this->input->post('Email2'),
							'SubjekID' => $SubjekID,
							'SpesialisasiID' => $SpesialisasiID,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
				$this->m_dosen->PTL_insert($data);
				echo warning("Profile Added.$pesan_file$ukuran_akhir","../lecturer");
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$DosenID = $this->uri->segment(3);
			$result = $this->m_dosen->PTL_select($DosenID);
			$data['Login'] = $result['Login'];
			$data['Nama'] = $result['Nama'];
			$data['TempatLahir'] = $result['TempatLahir'];
			$data['TanggalLahir'] = $result['TanggalLahir'];
			$data['Kelamin'] = $result['Kelamin'];
			$data['Foto'] = $result['Foto'];
			$data['foto_lama'] = $result['Foto'];
			$data['Kebangsaan'] = $result['Kebangsaan'];
			$data['StatusSipil'] = $result['StatusSipil'];
			$data['JumlahAnak'] = $result['JumlahAnak'];
			$data['Agama'] = $result['Agama'];
			$data['AlamatJakarta'] = $result['AlamatJakarta'];
			$data['Telephone'] = $result['Telephone'];
			$data['Handphone'] = $result['Handphone'];
			$data['Email'] = $result['Email'];
			$data['Email2'] = $result['Email2'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['SpesialisasiID'] = $result['SpesialisasiID'];
			$data['NA'] = $result['NA'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['rowrecord'] = $this->m_year->PTL_all_active();
			$data['rowagama'] = $this->m_religion->PTL_all();
			$data['rowsubjek'] = $this->m_subjek->PTL_all();
			$data['rowspc'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Lecturer/v_lecturer_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$totA = $this->input->post('totA');
			$SubjekID = "";
			for($iA=1;$iA<=$totA;$iA++)
			{
				if($this->input->post("sub$iA") != "")
				{
					$SubjekID .= $this->input->post("sub$iA").".";
				}
			}
			$totB = $this->input->post('totB');
			$SpesialisasiID = "";
			for($iB=1;$iB<=$totB;$iB++)
			{
				if($this->input->post("spc$iB") != "")
				{
					$SpesialisasiID .= $this->input->post("spc$iB").".";
				}
			}
			$DosenID = $this->input->post('Login');
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'TempatLahir' => $this->input->post('TempatLahir'),
						'TanggalLahir' => $this->input->post('TanggalLahir'),
						'Kelamin' => $this->input->post('Kelamin'),
						'Kebangsaan' => $this->input->post('Kebangsaan'),
						'StatusSipil' => $this->input->post('StatusSipil'),
						'JumlahAnak' => $this->input->post('JumlahAnak'),
						'Agama' => $this->input->post('Agama'),
						'AlamatJakarta' => $this->input->post('AlamatJakarta'),
						'Telephone' => $this->input->post('Telephone'),
						'Handphone' => $this->input->post('Handphone'),
						'Email' => $this->input->post('Email'),
						'Email2' => $this->input->post('Email2'),
						'SubjekID' => $SubjekID,
						'SpesialisasiID' => $SpesialisasiID,
						'NA' => $this->input->post('NA'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_dosen->PTL_update($DosenID,$data);
			echo warning("Profile updated.","../lecturer/ptl_edit/$DosenID");
		}
		
		function ptl_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$DosenID = $this->uri->segment(3);
			$cektahun = $this->session->userdata('lecturer_filter_tahun');
			$data['rowrecord'] = $this->m_jadwal->PTL_all_dosen($DosenID,$cektahun);
			$data['DosenID'] = $DosenID;
			$cekjurusan = $this->session->userdata('lecturer_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Lecturer/v_lecturer_attendance',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>