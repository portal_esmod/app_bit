<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Transcript extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('PHPExcel');
			$this->load->model('m_aktifitas');
			$this->load->model('m_dosen');
			$this->load->model('m_exam');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_nilai');
			$this->load->model('m_nilai3');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_remedial_krs');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$ProgramID = $this->session->userdata('eval_filter_jur');
			$TahunID = $this->session->userdata('eval_filter_tahun');
			$StatusMhswID = $this->session->userdata('eval_filter_status');
			$data['response'] = 'false';
			$query = $this->m_mahasiswa->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$MhswID = $row->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$Status = "";
					if($resmhsw)
					{
						$Status = $resmhsw["StatusMhswID"];
					}
					if($StatusMhswID == "")
					{
						$result = $this->m_khs->PTL_select_program_tahun($MhswID,$ProgramID,$TahunID,$StatusMhswID);
						if($result)
						{
							$data['message'][] = array(
													'id'=> $row->MhswID,
													'value' => $row->MhswID." - ".$row->Nama,
													''
													);
						}
					}
					else
					{
						if($Status == $StatusMhswID)
						{
							$result = $this->m_khs->PTL_select_program_tahun($MhswID,$ProgramID,$TahunID,$StatusMhswID);
							if($result)
							{
								$data['message'][] = array(
														'id'=> $row->MhswID,
														'value' => $row->MhswID." - ".$row->Nama,
														''
														);
							}
						}
					}
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('evaluation/index',$data);
			}
		}
		
		function ptl_cari()
		{
			$this->authentification();
			if($this->input->post('cari') != "")
			{
				$cari = $this->input->post('cari');
			}
			else
			{
				$cari = $this->uri->segment(3);
			}
			if($cari != "")
			{
				$this->session->set_userdata('eval_filter_mahasiswa',$cari);
			}
			else
			{
				$this->session->set_userdata('eval_filter_mahasiswa','');
			}
			$datalog = array(
							'pk1' => $cari,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "TRANS01-01",
							'aktifitas' => "Filter halaman Transcript - Search: $cari.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("transcript");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "TRANS01-02",
							'aktifitas' => "Filter halaman Transcript - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('eval_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_jur');
			}
			$this->session->unset_userdata('eval_filter_mahasiswa');
			redirect("transcript");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "TRANS01-03",
							'aktifitas' => "Filter halaman Transcript - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('eval_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_tahun');
			}
			$this->session->unset_userdata('eval_filter_mahasiswa');
			redirect("transcript");
		}
		
		function ptl_filter_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			$datalog = array(
							'pk1' => $cekstatus,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU01-05",
							'aktifitas' => "Filter halaman Evaluation - Status: $cekstatus.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekstatus != "")
			{
				$this->session->set_userdata('eval_filter_status',$cekstatus);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_status');
			}
			redirect("transcript");
		}
		
		function ptl_filter_semester()
		{
			$this->authentification();
			$ceksemester = $this->input->post('ceksemester');
			$datalog = array(
							'pk1' => $ceksemester,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "TRANS01-04",
							'aktifitas' => "Filter halaman Transcript - Semester: $ceksemester.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksemester != "")
			{
				$this->session->set_userdata('eval_filter_semester',$ceksemester);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_semester');
			}
			redirect("transcript");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','evaluation');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "TRANS02",
							'aktifitas' => "Mengakses halaman Transcript.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['id'] = "mahasiswa";
			$data['controller'] = "transcript/lookup";
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$cekjurusan = $this->session->userdata('trans_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['status'] = $this->m_status->PTL_all_data();
			$data['rowrecord'] = $this->m_khs->PTL_all_select($MhswID);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Transcript/v_transcript',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf()
		{
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$MhswID = $word[0];
			if($MhswID == "")
			{
				echo warning("Sorry! Session has expired. Try again...","../transcript");
			}
			else
			{
				$datalog = array(
								'pk1' => $TahunID,
								'pk2' => $MhswID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "TRANS03",
								'aktifitas' => "Mengakses halaman Transcript - PDF.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$data['MhswID'] = $MhswID;
				$result = $this->m_khs->PTL_all_select($MhswID);
				date_default_timezone_set('Asia/Jakarta');
				$this->fpdf->FPDF("P","cm",array(21.59, 33.02));
				$this->fpdf->SetMargins(1,1,1);
				$this->fpdf->AliasNbPages();
				$this->fpdf->AddPage();
				$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.3,5.7,0.7);
				$this->fpdf->Ln(0.2);
				$this->fpdf->SetFont("helvetica","",7);
				$this->fpdf->SetFont("Times","B",12);
				$this->fpdf->Ln(-0.5);
				$id_akun = $_COOKIE["id_akun"];
				$nama = $_COOKIE["nama"];
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				$title = "";
				$periodetahunawal = "";
				$periodetahunakhir = "";
				if($resmhsw['ProgramID'] == "REG")
				{
					$title = "THREE YEAR (6 Semesters)";
					$periodetahunawal = substr($MhswID,0,4);
					$periodetahunakhir = substr($MhswID,0,4) + 2;
				}
				if($resmhsw['ProgramID'] == "INT")
				{
					$title = "ONE YEAR (2 Semesters)";
					$periodetahunawal = substr($MhswID,0,4);
					$periodetahunakhir = substr($MhswID,0,4) + 1;
				}
				$this->fpdf->SetFont("helvetica","",8);
				$this->fpdf->Cell(6.7,0.4,"",0,0,"C");
				$this->fpdf->Cell(3,0.4,"YEAR","LBT",0,"C");
				$this->fpdf->Cell(3,0.4,"$periodetahunawal / $periodetahunakhir","LBTR",0,"C");
				$this->fpdf->Cell(1.5,0.4,"","",0,"C");
				$this->fpdf->SetFont("helvetica","",6);
				$this->fpdf->SetTextColor(238,61,65);
				$this->fpdf->Cell(5.4,0.4,"INTERNATIONAL FASHION DESIGN & BUSINESS SCHOOL","",0,"R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->SetFont("helvetica","",8);
				$this->fpdf->Ln();
				$this->fpdf->Cell(6.7,0.5,"",0,0,"C");
				$tgl = tgl_singkat_eng($resmhsw['TanggalLahir']);
				if($resmhsw['TanggalLahir'] == "NULL")
				{
					$tgl = "";
				}
				$tanggal_lahir = $resmhsw['TempatLahir'].", ".$tgl;
				$tanggal_buat = explode(" ",$resmhsw['tanggal_buat']);
				$ProdiID = $resmhsw['ProdiID'];
				$resprodi = $this->m_prodi->PTL_select($ProdiID);
				$this->fpdf->Cell(6,0.5,$title,"LBTR",0,"C");
				$this->fpdf->Cell(1.5,0.5,"","",0,"C");
				$this->fpdf->SetFont("helvetica","",10);
				$this->fpdf->SetTextColor(51,102,255);
				if($resmhsw['StatusMhswID'] == "L")
				{
					$this->fpdf->Cell(5, 0.5, "OFFICIAL TRANSCRIPT", "", 0, "C");
				}
				else
				{
					$this->fpdf->Cell(5, 0.5, "PROVISIONAL TRANSCRIPT", "", 0, "C");
				}
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Line(1,1.7,20.5,1.7);
				$this->fpdf->Line(1,1.75,20.5,1.75);
				$resmax = $this->m_khs->PTL_select_drop($MhswID);
				$TahunID = $resmax['tahun'];
				$resmaxkhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$SpesialisasiID = $resmaxkhs['SpesialisasiID'];
				$resspec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				$SpecNama = "";
				if($resspec)
				{
					$SpecNama = $resspec['Nama'];
				}
				$reskhs = $this->m_khs->PTL_all_select($MhswID);
				$kelas1 = "";
				$kelas2 = "";
				$kelas3 = "";
				if($reskhs)
				{
					foreach($reskhs as $rk)
					{
						$KelasID = $rk->KelasID;
						$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
						if($rk->TahunKe == 1)
						{
							if($resmhsw['ProgramID'] == "INT")
							{
								$kelas1 = "O".@$reskelas["Nama"];
							}
							else
							{
								$kelas1 = $rk->TahunKe.@$reskelas["Nama"];
							}
						}
						if($rk->TahunKe == 2)
						{
							$kelas2 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
						if($rk->TahunKe == 3)
						{
							$kelas3 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
					}
				}
				$ProgramID = $resmhsw["ProgramID"];
				$resprog = $this->m_program->PTL_select($ProgramID);
				$ProdiID = $resmhsw["ProdiID"];
				$resprod = $this->m_prodi->PTL_select($ProdiID);
				if($resmhsw["Foto"] == "")
				{
					$foto = "foto_umum/user.jpg";
				}
				else
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
					if($exist)
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					}
					else
					{
						$foto = "foto_umum/user.jpg";
					}
				}
				$this->fpdf->Ln(0.7);
				$this->fpdf->SetFont("Times","B",7);
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "Student name", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": $resmhsw[Nama]", "", 0, "L");
				$this->fpdf->Cell(9.5, 0.3, "", "", 0, "R");
				$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX()-2.5,$this->fpdf->getY()+0.1,2.5,3.5);
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.5, "Place / Date of Birth", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.5, ": $tanggal_lahir", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$JumlahKata = str_word_count($resmhsw['Alamat']);
				if($JumlahKata > 5)
				{
					$word = explode(" ",$resmhsw['Alamat']);
					$Kalimat = "";
					$JumlahKalimat = 0;
					$TotalPenggalan = 0;
					for($i=0;$i<=$JumlahKata;$i++)
					{
						$JumlahKalimat++;
						if($JumlahKalimat == 5)
						{
							$JumlahKalimat = 0;
							$TotalPenggalan++;
							$Kalimat .= @$word[$i]." ";
							if($TotalPenggalan == 1)
							{
								$this->fpdf->Cell(5.5, 0.3, "Address", "", 0, "R");
								$this->fpdf->SetTextColor(0,0,0);
								$this->fpdf->Cell(4, 0.3, ": $Kalimat", "", 0, "L");
								$this->fpdf->Ln();
							}
							else
							{
								if($Kalimat != " ")
								{
									$this->fpdf->Cell(5.5, 0.3, "", "", 0, "R");
									$this->fpdf->SetTextColor(0,0,0);
									$this->fpdf->Cell(4, 0.3, "  $Kalimat", "", 0, "L");
									$this->fpdf->Ln();
								}
							}
							$Kalimat = "";
						}
						else
						{
							$Kalimat .= @$word[$i]." ";
						}
					}
					if($JumlahKalimat > 0)
					{
						if($Kalimat != " ")
						{
							$this->fpdf->Cell(5.5, 0.3, "", "", 0, "R");
							$this->fpdf->SetTextColor(0,0,0);
							$this->fpdf->Cell(4, 0.3, "  $Kalimat", "", 0, "L");
							$this->fpdf->Ln();
						}
					}
				}
				else
				{
					$this->fpdf->Cell(5.5, 0.3, "Address", "", 0, "R");
					$this->fpdf->SetTextColor(0,0,0);
					$this->fpdf->Cell(4, 0.3, ": $resmhsw[Alamat]", "", 0, "L");
					$this->fpdf->Ln();
				}
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "SIN (Student Identification Number)", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": $MhswID", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "Enroll Date", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": ".tgl_singkat_eng($tanggal_buat[0]), "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "Degree", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": $resprodi[Gelar] - $resprodi[Jenjang]", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "Courses enrolled", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": ".strtoupper($resprodi['Nama']), "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.3, "Class", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": $kelas1$kelas2$kelas3", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.5, "Specialization", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": $SpecNama", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(5.5, 0.5, "Graduation Date", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(4, 0.3, ": ".tgl_singkat_eng($resmhsw['tanggal_lulus']), "", 0, "L");
				$this->fpdf->Ln(0.8);
				$this->fpdf->Cell(1.5, 1, "PERIOD", "LBTR", 0, "C");
				$this->fpdf->Cell(0.5, 1, "#", "LBTR", 0, "C");
				$this->fpdf->Cell(6, 1, "MODULE TITLE", "BTR", 0, "C");
				$this->fpdf->Cell(1.5, 1, "CODE", "BTR", 0, "C");
				$this->fpdf->Cell(4, 0.5, "Attendance", "BTR", 0, "C");
				$this->fpdf->Cell(1, 1, "GV", "TR", 0, "C");
				$this->fpdf->Cell(1, 1, "EXM", "TR", 0, "C");
				$this->fpdf->Cell(1, 1, "RMD", "TR", 0, "C");
				$this->fpdf->Cell(1, 1, "GRD", "TR", 0, "C");
				$this->fpdf->Cell(1, 1, "CRD", "TR", 0, "C");
				$this->fpdf->Cell(1, 1, "GP", "TR", 0, "C");
				$this->fpdf->Cell(1, 0.5, "", "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(9.5, 0.5, "", "", 0, "C");
				$this->fpdf->Cell(1, 0.5, "EXC", "BR", 0, "C");
				$this->fpdf->Cell(1, 0.5, "SIC", "BR", 0, "C");
				$this->fpdf->Cell(1, 0.5, "ABS", "BR", 0, "C");
				$this->fpdf->Cell(1, 0.5, "LAT", "BR", 0, "C");
				$this->fpdf->Cell(6, 0.5, "", "B", 0, "C");
				
				$ceksemester = $this->session->userdata('eval_filter_semester');
				$resmax = $this->m_khs->PTL_select_drop($MhswID);
				$TahunID = $resmax['tahun'];
				$result2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$Sesi = "";
				if($result2)
				{
					$Sesi = $result2['Sesi'];
				}
				if($ceksemester != "")
				{
					$FilterSemester = $ceksemester;
				}
				else
				{
					$FilterSemester = $Sesi;
				}
				
				$no = 1;
				$tot = count($result);
				$ta = 0;
				$t = 0;
				$tahun = 0;
				$kumulatif = 0;
				$kumulatifsks = 0;
				$kumulatifbobot = 0;
				$totalhexc = 0;
				$totalhsic = 0;
				$totalhabs = 0;
				$totalhlat = 0;
				$ipk = 0;
				foreach($result as $r)
				{
					$jumlahsks = 0;
					$jumlahexc = 0;
					$jumlahsic = 0;
					$jumlahabs = 0;
					$jumlahlat = 0;
					$jumlahmp = 0;
					$jumlahbobot = 0;
					$totalbobot = 0;
					if($r->Sesi <= $FilterSemester)
					{
						$kumulatif++;
						if($no == 1)
						{
							$tahun = $tahun + 1;
							$no++;
						}
						else
						{
							$tahun = $tahun;
							$no = 1;
						}
						$KelasID = $r->KelasID;
						$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
						if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
						$DosenID = $r->WaliKelasID;
						$rhr1 = $this->m_dosen->PTL_select($DosenID);
						if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
						$DosenID = $r->WaliKelasID2;
						$rhr2 = $this->m_dosen->PTL_select($DosenID);
						if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
						$SpesialisasiID = $r->SpesialisasiID;
						$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
						if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
						$thn = "";
						if($r->TahunKe == 1){ $thn = "st"; }
						if($r->TahunKe == 2){ $thn = "nd"; }
						if($r->TahunKe == 3){ $thn = "rd"; }
						$smt = "";
						if($r->Sesi == 1){ $smt = "st"; }
						if($r->Sesi == 2){ $smt = "nd"; }
						if($r->Sesi == 3){ $smt = "rd"; }
						if($r->Sesi == 4){ $smt = "th"; }
						if($r->Sesi == 5){ $smt = "th"; }
						if($r->Sesi == 6){ $smt = "th"; }
						$MhswID = $r->MhswID;
						$KHSID = $r->KHSID;
						$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
						$ips = 0;
						if($rowkrs)
						{
							$this->fpdf->SetFont("Times","",6);
							$totkrs = count($rowkrs);
							$ns = 1;
							foreach($rowkrs as $rkr)
							{
								$SubjekID = $rkr->SubjekID;
								$rsub = $this->m_subjek->PTL_select($SubjekID);
								$subjek = $rsub["Nama"];
								$sks = $rsub["SKS"];
								$jumlahsks = $jumlahsks + $rsub["SKS"];
								$KurikulumID = $rsub["KurikulumID"];
								$GradeNilai = $rkr->GradeNilai;
								$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
								$bobot = 0;
								if($resbobot)
								{
									$bobot = $resbobot['Bobot'];
								}
								$KRSID = $rkr->KRSID;
								$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
								if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
								$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
								if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
								$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
								if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
								$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
								if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
								$tottexc = $texc;
								$jumlahexc = $jumlahexc + $tottexc;
								if($tottexc == 0){ $tottexc = ""; }
								$tottsic = $tsic;
								$jumlahsic = $jumlahsic + $tottsic;
								if($tottsic == 0){ $tottsic = ""; }
								$tottabs = $tabs;
								$jumlahabs = $jumlahabs + $tottabs;
								if($tottabs == 0){ $tottabs = ""; }
								$tottlat = $tlat;
								$jumlahlat = $jumlahlat + $tottlat;
								if($tottlat == 0){ $tottlat = ""; }
								$TahunID = $rkr->TahunID;
								$exam = 0;
								$ExamID = "";
								$TotJum = "";
								$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
								if($rexmhsw1)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw1 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA1)
										{
											if($rexmhswA1['Nilai'] > 0)
											{
												$jumexam = $jumexam + $rexmhswA1['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
								if($rexmhsw2)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw2 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA2)
										{
											if($rexmhswA2['Nilai'] > 0)
											{
												$jumexam = $jumexam + $rexmhswA2['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
								if($rexmhsw3)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw3 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA3)
										{
											if($rexmhswA3['Nilai'] > 0)
											{
												$jumexam = $jumexam + $rexmhswA3['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
								if($rexmhsw4)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw4 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA4)
										{
											if($rexmhswA4['Nilai'] > 0)
											{
												$jumexam = $jumexam + $rexmhswA4['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
								if($rexmhsw5)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw5 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA5)
										{
											if($rexmhswA5['Nilai'] > 0)
											{
												$exam = $exam + $rexmhswA5['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
								if($rexmhsw6)
								{
									$jum = 0;
									$jumexam = 0;
									foreach($rexmhsw6 as $row)
									{
										$ExamID = $row->ExamID;
										$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
										if($rexmhswA6)
										{
											if($rexmhswA6['Nilai'] > 0)
											{
												$exam = $exam + $rexmhswA6['Nilai'];
												$jum++;
												$TotJum++;
											}
										}
									}
									if($jum > 0)
									{
										$exam = $exam + ($jumexam / $jum);
									}
								}
								if($exam == 0)
								{
									$exam = "-";
								}
								else
								{
									if($TotJum > 1)
									{
										$exam = number_format(($exam / $TotJum),2,'.','');
									}
								}
								$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
								$remedial = "-";
								$KRSRemedialID = "";
								if($resremedial)
								{
									$remedial = $resremedial['Nilai'];
									$KRSRemedialID = $resremedial['KRSRemedialID'];
								}
								$jumlahbobot = $bobot * $sks;
								$totalbobot = $totalbobot + $jumlahbobot;
								if($ns == $totkrs)
								{
									$this->fpdf->Ln();
									$this->fpdf->Cell(1.5, 0.3, "", "BLR", 0, "C");
									$this->fpdf->Cell(0.5, 0.3, $ns, "BLR", 0, "C");
									$this->fpdf->Cell(6, 0.3, substr($subjek,0,46), "BR", 0, "L");
									$this->fpdf->Cell(1.5, 0.3, $rsub['SubjekKode'], "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottexc, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottsic, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottabs, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottlat, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $rkr->gradevalue, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $exam, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $remedial, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $rkr->GradeNilai, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $sks, "BR", 0, "C");
									$this->fpdf->Cell(1, 0.3, $jumlahbobot, "BR", 0, "C");
								}
								else
								{
									$period = "";
									if($ns == 1)
									{
										$period = "$r->TahunKe$thn YEAR";
									}
									if($ns == 3)
									{
										$period = "$r->Sesi$smt";
									}
									if($ns == 4)
									{
										$period = "SEMESTER";
									}
									$this->fpdf->Ln();
									$this->fpdf->Cell(1.5, 0.3, $period, "LR", 0, "C");
									$this->fpdf->Cell(0.5, 0.3, $ns, "LR", 0, "C");
									$this->fpdf->Cell(6, 0.3, substr($subjek,0,46), "R", 0, "L");
									$this->fpdf->Cell(1.5, 0.3, $rsub['SubjekKode'], "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottexc, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottsic, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottabs, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $tottlat, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $rkr->gradevalue, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $exam, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $remedial, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $rkr->GradeNilai, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $sks, "R", 0, "C");
									$this->fpdf->Cell(1, 0.3, $jumlahbobot, "R", 0, "C");
								}
								$ns++;
							}
						}
					}
					$kumulatifsks = $kumulatifsks + $jumlahsks;
					$kumulatifbobot = $kumulatifbobot + $totalbobot;
					$totalhexc = $totalhexc + $jumlahexc;
					$totalhsic = $totalhsic + $jumlahsic;
					$totalhabs = $totalhabs + $jumlahabs;
					$totalhlat = $totalhlat + $jumlahlat;
				}
				if($kumulatifsks == 0)
				{
					$ipk = 0;
				}
				else
				{
					$ipk = number_format(($kumulatifbobot / $kumulatifsks), 2);
				}
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Ln();
				$this->fpdf->Cell(2, 0.5, "CUMULATIVE", "LR", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, "EXC", "R", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, "SIC", "R", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, "ABS", "R", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, "LAT", "R", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(2, 0.5, "ATTENDANCES", "LBR", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, $totalhexc, "BTR", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, $totalhsic, "BTR", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, $totalhabs, "BTR", 0, "C");
				$this->fpdf->Cell(1.5, 0.5, $totalhlat, "BTR", 0, "C");
				$this->fpdf->Ln(1.5);
				$this->fpdf->SetFont("Times","B",8);
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(8, 0.5, "Cumulative Grade points ($kumulatif semesters) :", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(1, 0.5, $kumulatifbobot, "", 0, "R");
				$this->fpdf->Cell(2, 0.5, "", "", 0, "R");
				$this->fpdf->Cell(8, 0.5, "", "B", 0, "R");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(8, 0.5, "Cumulative Credit earned to Date ($kumulatif semesters) :", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(1, 0.5, $kumulatifsks, "", 0, "R");
				$this->fpdf->Cell(2, 0.5, "", "", 0, "R");
				$this->fpdf->Cell(8, 0.5, "ACADEMIC PROGRAM MANAGER / Mr. DESILLES Patrice", "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->SetTextColor(51,102,255);
				$this->fpdf->Cell(8, 0.5, "Cumulative Grade Point Average earned to Date ($kumulatif Semesters) :", "", 0, "R");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(1, 0.5, $ipk, "", 0, "R");
				$this->fpdf->Cell(2, 0.5, "", "", 0, "R");
				$this->fpdf->Cell(8, 0.5, "Jakarta, ".tgl_singkat_eng($this->tanggal), "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(2, 0.4, "GRADE", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "REMARKS", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "GPA", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "PROJECT SCORES", "LBTR", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Cell(2, 0.4, "A", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "VERY GOOD", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "3.3 - 4", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "15.5 - 20", "LBTR", 0, "C");
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(1, 0.4, "Ex:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "EXEMPTED", "", 0, "L");
				$this->fpdf->Cell(1, 0.4, "C:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "CREDIT", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Cell(2, 0.4, "B", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "GOOD", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "2.75 - 3,29", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "12.75 - 15,49", "LBTR", 0, "C");
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(1, 0.4, "W:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "WITHDRAWN", "", 0, "L");
				$this->fpdf->Cell(1, 0.4, "GP:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "GRADE POINT = GRADE * CREDITS", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Cell(2, 0.4, "C", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "SATISFACTORY", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "2 - 2,74", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "10 - 12,74", "LBTR", 0, "C");
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(1, 0.4, "GV:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "GRADE VALUE", "", 0, "L");
				$this->fpdf->Cell(1, 0.4, "GPA:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "GRADE POINT AVERAGE = GP / TOTAL CREDITS", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Cell(2, 0.4, "D", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "UNSATISFACTORY", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "1 - 1,99", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "9 - 9,99", "LBTR", 0, "C");
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(1, 0.4, "R:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "REMEDIAL", "", 0, "L");
				$this->fpdf->Cell(1, 0.4, "CC:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "CUMULATIVE CREDITS", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",6);
				$this->fpdf->Cell(2, 0.4, "E", "LBTR", 0, "C");
				$this->fpdf->Cell(3.5, 0.4, "FAILED no credit awarded", "LBTR", 0, "C");
				$this->fpdf->Cell(2, 0.4, "0 - 0,99", "LBTR", 0, "C");
				$this->fpdf->Cell(2.7, 0.4, "0 - 8,99", "LBTR", 0, "C");
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(1, 0.4, "G:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, "GRADE", "", 0, "L");
				$this->fpdf->Cell(1, 0.4, "CGPA:", "", 0, "R");
				$this->fpdf->Cell(2, 0.4, ": CUMUATIVE GRADE POINT AVERAGE", "", 0, "L");
				$this->fpdf->Ln(0.5);
				$this->fpdf->SetFont("Times","",5);
				$this->fpdf->Cell(7, 0.4, "Jalan Asem Dua N 3 -5, Cipete 12410 Jakarta Selatan, INDONESIA", "", 0, "C");
				$this->fpdf->Cell(6, 0.4, "Tel: +62 21 765 91 81 Fax: +62 21 765 75 17", "", 0, "C");
				$this->fpdf->Cell(3, 0.4, "Email: info@esmodjakarta.com", "", 0, "C");
				$this->fpdf->Cell(4, 0.4, "Web: http://esmodjakarta.com", "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","",5.3);
				$this->fpdf->SetTextColor(255,0,0);
				$this->fpdf->Cell(-0.3, 0.5, "", "", 0, "L");
				$this->fpdf->Cell(1.8, 0.5, "WWW.ESMOD.COM", "", 0, "L");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(6.9, 0.5, " - BEIJING, BEYROUTH, BORDEAUX, DAMAS, DUBAI, GUANGZHOU, ISTANBUL, ", "", 0, "L");
				$this->fpdf->SetTextColor(255,0,0);
				$this->fpdf->Cell(1.35, 0.5, " JAKARTA, ", "", 0, "L");
				$this->fpdf->SetTextColor(0,0,0);
				$this->fpdf->Cell(9, 0.5, " KUALA LUMPUR, KYOTO, LYON, MOSCOU, OSLO, PARIS, RENNES, ROUBAIX, SEOUL, SOUSSE, TOKYO, TUNIS", "", 0, "C");
				$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
			}
		}
		
		function ptl_excel()
		{
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$result = $this->m_khs->PTL_all_select($MhswID);
			date_default_timezone_set('Asia/Jakarta');
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$tahun = 0;
			$kumulatif = 0;
			$kumulatifsks = 0;
			$kumulatifipk = 0;
			$ipk = 0;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Portal");
			$objPHPExcel->getProperties()->setLastModifiedBy("System");
			$objPHPExcel->getProperties()->setTitle("Student Transcript");
			$objPHPExcel->getProperties()->setSubject("Evaluasi Mahasiswa");
			$objPHPExcel->getProperties()->setDescription("Database Evaluasi");
			$objPHPExcel->getProperties()->setKeywords("Portal Data");
			$objPHPExcel->getProperties()->setCategory("Database");
			$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("N")->setAutoSize(true);;
			$center = array();
			$center['alignment'] = array();
			$center['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
			$objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
			$objPHPExcel->getActiveSheet()->mergeCells("A2:C2");
			$objPHPExcel->getActiveSheet()->mergeCells("A3:N3");
			$objPHPExcel->getActiveSheet()->setCellValue("A1", "Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan");
			$objPHPExcel->getActiveSheet()->setCellValue("A2", "Telp. (021) 7659181, Fax. (021) 7657517");
			$objPHPExcel->getActiveSheet()->getStyle("A3:N3")->applyFromArray($center);
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			if($resmhsw['StatusMhswID'] == "L")
			{
				$objPHPExcel->getActiveSheet()->setCellValue("A3", "OFFICIAL TRANSCRIPT");
			}
			else
			{
				$objPHPExcel->getActiveSheet()->setCellValue("A3", "PROVISIONAL TRANSCRIPT");
			}
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$reskhs = $this->m_khs->PTL_all_select($MhswID);
			$kelas1 = "";
			$kelas2 = "";
			$kelas3 = "";
			if($reskhs)
			{
				foreach($reskhs as $rk)
				{
					$KelasID = $rk->KelasID;
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					if($rk->TahunKe == 1)
					{
						if($resmhsw['ProgramID'] == "INT")
						{
							$kelas1 = "O".@$reskelas["Nama"];
						}
						else
						{
							$kelas1 = $rk->TahunKe.@$reskelas["Nama"];
						}
					}
					if($rk->TahunKe == 2)
					{
						$kelas2 = " > ".$rk->TahunKe.@$reskelas["Nama"];
					}
					if($rk->TahunKe == 3)
					{
						$kelas3 = " > ".$rk->TahunKe.@$reskelas["Nama"];
					}
				}
			}
			$ProgramID = $resmhsw['ProgramID'];
			$resprog = $this->m_program->PTL_select($ProgramID);
			$ProdiID = $resmhsw['ProdiID'];
			$resprod = $this->m_prodi->PTL_select($ProdiID);
			if($resmhsw["Foto"] == "")
			{
				$foto = "foto_umum/user.jpg";
			}
			else
			{
				$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				$exist = file_exists_remote(base_url("ptl_storage/$foto"));
				if($exist)
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				}
				else
				{
					$foto = "foto_umum/user.jpg";
				}
			}
			$objPHPExcel->getActiveSheet()->setCellValue("A5", "NAME");
			$objPHPExcel->getActiveSheet()->setCellValue("B5", ": $resmhsw[Nama]");
			$objPHPExcel->getActiveSheet()->setCellValue("A6", "PLACE / DATE OF BIRTH");
			$tgl = tgl_singkat_eng($resmhsw['TanggalLahir']);
			if($resmhsw['TanggalLahir'] == "NULL")
			{
				$tgl = "";
			}
			$tanggal_lahir = $resmhsw['TempatLahir'].", ".$tgl;
			$objPHPExcel->getActiveSheet()->setCellValue("B6", ": $tanggal_lahir");
			$objPHPExcel->getActiveSheet()->setCellValue("A7", "ADDRESS");
			$objPHPExcel->getActiveSheet()->setCellValue("B7", ": ".strtoupper($resmhsw['Alamat']));
			$objPHPExcel->getActiveSheet()->setCellValue("A8", "SIN");
			$objPHPExcel->getActiveSheet()->setCellValue("B8", ": $MhswID");
			$objPHPExcel->getActiveSheet()->setCellValue("A9", "ENROLL DATE");
			$tanggal_buat = explode(" ",$resmhsw['tanggal_buat']);
			$objPHPExcel->getActiveSheet()->setCellValue("B9", ": ".tgl_singkat_eng($tanggal_buat[0]));
			$objPHPExcel->getActiveSheet()->setCellValue("A10", "DEGREE");
			$objPHPExcel->getActiveSheet()->setCellValue("B10", ": ".strtoupper($resprog["Nama"]));
			$objPHPExcel->getActiveSheet()->setCellValue("A11", "COURSES ENROLLED");
			$objPHPExcel->getActiveSheet()->setCellValue("B11", ": ".strtoupper($resprod['Nama']));
			$objPHPExcel->getActiveSheet()->setCellValue("A12", "CLASS");
			$objPHPExcel->getActiveSheet()->setCellValue("B12", ": $kelas1$kelas2$kelas3");
			$objPHPExcel->getActiveSheet()->setCellValue("A13", "SPECIALIZATION");
			$resmax = $this->m_khs->PTL_select_drop($MhswID);
			$TahunID = $resmax['tahun'];
			$resmaxkhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$SpesialisasiID = $resmaxkhs['SpesialisasiID'];
			$resspec = $this->m_spesialisasi->PTL_select($SpesialisasiID);
			$SpecNama = "";
			if($resspec)
			{
				$SpecNama = $resspec['Nama'];
			}
			$objPHPExcel->getActiveSheet()->setCellValue("B13", ": $SpecNama");
			$objPHPExcel->getActiveSheet()->setCellValue("A14", "GRADUATION DATE");
			$objPHPExcel->getActiveSheet()->setCellValue("B14", ": ".tgl_singkat_eng($resmhsw['tanggal_lulus']));
			
			$objPHPExcel->getActiveSheet()->getStyle("A16:N16")->applyFromArray($center);
			$objPHPExcel->getActiveSheet()->getStyle("A17:N17")->applyFromArray($center);
			$objPHPExcel->getActiveSheet()->getStyle("A18:N18")->applyFromArray($center);
			$objPHPExcel->getActiveSheet()->getStyle("A19:N19")->applyFromArray($center);
			$objPHPExcel->getActiveSheet()->setCellValue("A18", "#");
			$objPHPExcel->getActiveSheet()->setCellValue("B18", "MODULE TITLE");
			$objPHPExcel->getActiveSheet()->setCellValue("C18", "CODE");
			$objPHPExcel->getActiveSheet()->mergeCells("D18:G18");
			$objPHPExcel->getActiveSheet()->setCellValue("D18", "ATTENDANCE");
			$objPHPExcel->getActiveSheet()->setCellValue("H18", "M/P");
			$objPHPExcel->getActiveSheet()->setCellValue("I18", "GV");
			$objPHPExcel->getActiveSheet()->setCellValue("J18", "EXM");
			$objPHPExcel->getActiveSheet()->setCellValue("K18", "RMD");
			$objPHPExcel->getActiveSheet()->setCellValue("L18", "GRD");
			$objPHPExcel->getActiveSheet()->setCellValue("M18", "CRD");
			$objPHPExcel->getActiveSheet()->setCellValue("N18", "GP");
			
			$objPHPExcel->getActiveSheet()->setCellValue("D19", "EXC");
			$objPHPExcel->getActiveSheet()->setCellValue("E19", "SIC");
			$objPHPExcel->getActiveSheet()->setCellValue("F19", "ABS");
			$objPHPExcel->getActiveSheet()->setCellValue("G19", "LAT");
			$totcol = 1;
			$totrow = 1;
			$TotalExc = 0;
			$TotalSic = 0;
			$TotalAbs = 0;
			$TotalLat = 0;
			$kumulatif = 0;
			$kumulatifsks = 0;
			$kumulatifbobot = 0;
			foreach($result as $r)
			{
				$kumulatif++;
				$MhswID = $r->MhswID;
				$KHSID = $r->KHSID;
				$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
				$totalbobot = 0;
				$ips = 0;
				if($rowkrs)
				{
					$ns = 1;
					$jumlahsks = 0;
					$jumlahexc = 0;
					$jumlahsic = 0;
					$jumlahabs = 0;
					$jumlahlat = 0;
					$jumlahmp = 0;
					$jumlahbobot = 0;
					$nkrsrow = 19;
					foreach($rowkrs as $rkr)
					{
						$TahunID = $rkr->TahunID;
						$JadwalID = $rkr->JadwalID;
						$SubjekID = $rkr->SubjekID;
						$rsub = $this->m_subjek->PTL_select($SubjekID);
						$subjek = $rsub["Nama"];
						$sks = $rsub["SKS"];
						$jumlahsks = $jumlahsks + $rsub["SKS"];
						$KurikulumID = $rsub["KurikulumID"];
						$JenisMKID = $rsub["JenisMKID"];
						$GradeNilai = $rkr->GradeNilai;
						$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
						$bobot = 0;
						if($resbobot)
						{
							$bobot = $resbobot['Bobot'];
						}
						$KRSID = $rkr->KRSID;
						$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
						if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
						$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
						if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
						$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
						if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
						$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
						if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
						$tottexc = $texc;
						$jumlahexc = $jumlahexc + $tottexc;
						if($tottexc == 0){ $tottexc = ""; }
						$tottsic = $tsic;
						$jumlahsic = $jumlahsic + $tottsic;
						if($tottsic == 0){ $tottsic = ""; }
						$tottabs = $tabs;
						$jumlahabs = $jumlahabs + $tottabs;
						if($tottabs == 0){ $tottabs = ""; }
						$tottlat = $tlat;
						$jumlahlat = $jumlahlat + $tottlat;
						if($tottlat == 0){ $tottlat = ""; }
						$TahunID = $rkr->TahunID;
						
						$exam = 0;
						$ExamID = "";
						$ExamMhswID = "";
						$TotJum = "";
						$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
						if($rexmhsw1)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw1 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA1)
								{
									if($rexmhswA1['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA1['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
						if($rexmhsw2)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw2 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA2)
								{
									if($rexmhswA2['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA2['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
						if($rexmhsw3)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw3 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA3)
								{
									if($rexmhswA3['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA3['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
						if($rexmhsw4)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw4 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA4)
								{
									if($rexmhswA4['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA4['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
						if($rexmhsw5)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw5 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA5)
								{
									if($rexmhswA5['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA5['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
						if($rexmhsw6)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw6 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA6)
								{
									if($rexmhswA6['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA6['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						if($exam == 0)
						{
							$exam = "-";
						}
						else
						{
							if($TotJum > 1)
							{
								$exam = number_format(($exam),2,'.','');
							}
						}
						$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
						$remedial = "-";
						if($resremedial)
						{
							$remedial = $resremedial['Nilai'];
						}
						
						if($rkr->gradevalue == "")
						{
							$mp = "";
						}
						else
						{
							$JenisPresensiID = "E";
							$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
							$mptottexc = $texc * $pr1;
							$JenisPresensiID = "S";
							$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
							$mptottsic = $tsic * $pr2;
							$JenisPresensiID = "A";
							$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
							$mptottabs = $tabs * $pr3;
							$JenisPresensiID = "L";
							$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
							$mptottlat = $tlat * $pr4;
							if(($JenisMKID == "5") OR ($JenisMKID == "25")) // MKID Special
							{
								$mp = "";
							}
							else
							{
								$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
							}
							if($mp == 0.00)
							{
								$mp = "";
							}
							$jumlahmp = $jumlahmp + $mp;
						}
						$jumlahbobot = $bobot * $sks;
						$totalbobot = $totalbobot + $jumlahbobot;
						$objPHPExcel->getActiveSheet()->getStyle("C".($totrow+$nkrsrow).":N".($totrow+$nkrsrow))->applyFromArray($center);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+$nkrsrow), $ns);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+$nkrsrow), substr($subjek,0,34));
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+$nkrsrow), $rsub['SubjekKode']);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+$nkrsrow), $tottexc);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($totrow+$nkrsrow), $tottsic);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($totrow+$nkrsrow), $tottabs);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($totrow+$nkrsrow), $tottlat);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($totrow+$nkrsrow), $mp);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($totrow+$nkrsrow), $rkr->gradevalue);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($totrow+$nkrsrow), $exam);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, ($totrow+$nkrsrow), $remedial);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, ($totrow+$nkrsrow), $rkr->GradeNilai);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, ($totrow+$nkrsrow), $sks);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, ($totrow+$nkrsrow), $jumlahbobot);
						$TotalExc = $TotalExc + $tottexc;
						$TotalSic = $TotalSic + $tottsic;
						$TotalAbs = $TotalAbs + $tottabs;
						$TotalLat = $TotalLat + $tottlat;
						$nkrsrow++;
						$ns++;
					}
				}
				$kumulatifsks = $kumulatifsks + $jumlahsks;
				$kumulatifbobot = $kumulatifbobot + $totalbobot;
				$totrow = $totrow + 10;
			}
			$ipk = number_format(($kumulatifbobot / $kumulatifsks), 2);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+20), "CUMULATIVE");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+20), "EXC");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+20), "SIC");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+20), "ABS");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($totrow+20), "LAT");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+21), "ATTENDANCES");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+21), $TotalExc);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+21), $TotalSic);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+21), $TotalAbs);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($totrow+21), $TotalLat);
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+24), "Cumulative Grade points ($kumulatif) :");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+24), "$kumulatifbobot");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+25), "Cumulative Credit earned to Date ($kumulatif semesters) :");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+25), "$kumulatifsks");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+26), "Cumulative Grade Point Average earned to Date ($kumulatif Semesters) :");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+26), "$ipk");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+27), "GRADE");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+27), "REMARKS");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+27), "GPA");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+27), "PROJECT SCORES");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+28), "A");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+28), "VERY GOOD");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+28), "3.3 - 4");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+28), "15.5 - 20");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+29), "B");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+29), "GOOD");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+29), "2.75 - 3,29");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+29), "12.75 - 15,49");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+30), "C");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+30), "SATISFACTORY");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+30), "2 - 2,74");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+30), "10 - 12,74");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+31), "D");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+31), "UNSATISFACTORY");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+31), "1 - 1,99");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+31), "9 - 9,99");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+32), "E");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($totrow+32), "FAILED no credit awarded");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+32), "0 - 0,99");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+32), "0 - 8,99");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($totrow+33), "Jalan Asem Dua N 3 -5, Cipete 12410 Jakarta Selatan, INDONESIA");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($totrow+33), "Tel: +62 21 765 91 81 Fax: +62 21 765 75 17");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($totrow+33), "Email: info@esmodjakarta.com");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, ($totrow+33), "Web: http://esmodjakarta.com");
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($totrow+34), "WWW.ESMOD.COM - BEIJING, BEYROUTH, BORDEAUX, DAMAS, DUBAI, GUANGZHOU, ISTANBUL,JAKARTA,KUALA LUMPUR, KYOTO, LYON, MOSCOU, OSLO, PARIS, RENNES, ROUBAIX, SEOUL, SOUSSE, TOKYO, TUNIS");
			$word = explode(" ",$resmhsw['Nama']);
			$NamaMhsw = $word[0];
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Transcript $MhswID-$NamaMhsw");
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$this->tanggal.'_Transcript_'.$MhswID.'_'.$resmhsw['Nama'].'.xlsx"');
            $objWriter->save("php://output");
        }
	}
?>