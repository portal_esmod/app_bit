<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Exam extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->model('m_aktifitas');
			$this->load->model('m_catatan2');
			$this->load->model('m_dosen');
			$this->load->model('m_exam');
			$this->load->model('m_exam_dosen');
			$this->load->model('m_exam_kursi');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_jadwal');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_ruang');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup_subjek1()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('exam/index',$data);
			}
		}
		
		function lookup_subjek2()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('exam/index',$data);
			}
		}
		
		function lookup_subjek3()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('exam_filter_tahun2');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('exam/index',$data);
			}
		}
		
		function ptl_filter_tahun1()
		{
			$this->authentification();
			$cektahun1 = $this->input->post('cektahun1');
			$datalog = array(
							'pk1' => $cektahun1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM01-01",
							'aktifitas' => "Filter halaman Exam - Year 1: $cektahun1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun1 != "")
			{
				$this->session->set_userdata('exam_filter_tahun1',$cektahun1);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_tahun1');
			}
			redirect("exam");
		}
		
		function ptl_filter_tahun2()
		{
			$this->authentification();
			$cektahun2 = $this->input->post('cektahun2');
			$datalog = array(
							'pk1' => $cektahun2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM01-02",
							'aktifitas' => "Filter halaman Exam - Year 2: $cektahun2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun2 != "")
			{
				$this->session->set_userdata('exam_filter_tahun2',$cektahun2);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_tahun2');
			}
			redirect("exam");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			$datalog = array(
							'pk1' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM01-03",
							'aktifitas' => "Filter halaman Exam - Lecturer: $cekdosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekdosen != "")
			{
				$this->session->set_userdata('exam_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_dosen');
			}
			redirect("exam");
		}
		
		function ptl_filter_ruang()
		{
			$this->authentification();
			$cekruang = $this->input->post('cekruang');
			$datalog = array(
							'pk1' => $cekruang,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM01-04",
							'aktifitas' => "Filter halaman Exam - Room: $cekruang.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekruang != "")
			{
				$this->session->set_userdata('exam_filter_ruang',$cekruang);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_ruang');
			}
			redirect("exam");
		}
		
		function lookup_dosen()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_dosen->lookup_dosen($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->Login,
											'value' => $row->Login.' - '.$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('exam/index',$data);
			}
		}
		
		function index()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM02",
							'aktifitas' => "Filter halaman Exam.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$TahunID2 = $this->session->userdata('exam_filter_tahun2');
			$RuangID = $this->session->userdata('exam_filter_ruang');
			$data['rowtahun1'] = $this->m_year->PTL_all_d3();
			$data['rowtahun2'] = $this->m_year->PTL_all_d1();
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowrecord'] = $this->m_exam->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			$thn1 = $this->session->userdata('exam_filter_tahun1');
			$thn2 = $this->session->userdata('exam_filter_tahun2');
			$dosen = $this->session->userdata('exam_filter_dosen');
			$ruang = $this->session->userdata('exam_filter_ruang');
			if(($thn1 == "") AND ($thn2 == "") AND ($dosen == "") AND ($ruang == ""))
			{
				$this->session->sess_destroy();
			}
			$this->session->set_userdata('menu','exam');
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				$this->load->view('Exam/v_exam_lecturer',$data);
			}
			else
			{
				$this->load->view('Exam/v_exam',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM03",
							'aktifitas' => "Filter halaman Exam - Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resexam = $this->m_exam->PTL_select($ExamID);
			if($resexam)
			{
				$data['ExamID'] = $resexam['ExamID'];
				$data['Tanggal'] = $resexam['Tanggal'];
				$data['JamMulai'] = substr($resexam['JamMulai'],0,5);
				$data['JamSelesai'] = substr($resexam['JamSelesai'],0,5);
				$data['JamSelesai'] = substr($resexam['JamSelesai'],0,5);
				$SubjekID = $resexam['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$data['SubjekID'] = '';
				if($ressub)
				{
					$data['SubjekID'] = $resexam['SubjekID'].' - '.$ressub['Nama'];
				}
				$SubjekID = $resexam['SubjekID12'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$data['SubjekID12'] = '';
				if($ressub)
				{
					$data['SubjekID12'] = $resexam['SubjekID12'].' - '.$ressub['Nama'];
				}
				$SubjekID = $resexam['SubjekID2'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$data['SubjekID2'] = '';
				if($ressub)
				{
					$data['SubjekID2'] = $resexam['SubjekID2'].' - '.$ressub['Nama'];
				}
				$data['RuangID'] = $resexam['RuangID'];
				$DosenID = $resexam['DosenID'];
				$resdos = $this->m_dosen->PTL_select($DosenID);
				$data['DosenID'] = '';
				if($resdos)
				{
					$data['DosenID'] = $resexam['DosenID'].' - '.$resdos['Nama'];
				}
			}
			else
			{
				$data['ExamID'] = '';
				$data['Tanggal'] = '';
				$data['JamMulai'] = '';
				$data['JamSelesai'] = '';
				$data['JamSelesai'] = '';
				$data['SubjekID'] = '';
				$data['SubjekID12'] = '';
				$data['SubjekID2'] = '';
				$data['RuangID'] = '';
				$data['DosenID'] = '';
			}
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Exam/v_exam_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$result = $this->m_exam->PTL_urut();
			$ExamID = $result['LAST'] + 1;
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM04",
							'aktifitas' => "Filter halaman Exam - Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$word = explode(" - ",$this->input->post('DosenID'));
			$data = array(
						'ExamID' => $ExamID,
						'TahunID' => $this->session->userdata('exam_filter_tahun1'),
						'TahunID2' => $this->session->userdata('exam_filter_tahun2'),
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'RuangID' => $this->input->post('RuangID'),
						'DosenID' => $word[0],
						'Tanggal' => $this->input->post('Tanggal'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_exam->PTL_insert($data);
			$dat = array(
						'ExamID' => $ExamID,
						'DosenID' => $word[0],
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_exam_dosen->PTL_insert($dat);
			echo warning("Schedule saved in temporary data until finish next steps.","../exam/ptl_student_form/$ExamID");
		}
		
		function ptl_update()
		{
			$this->authentification();
			$ExamID = $this->input->post('ExamID');
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM05",
							'aktifitas' => "Filter halaman Exam - Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$word = explode(" - ",$this->input->post('DosenID'));
			$data = array(
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'RuangID' => $this->input->post('RuangID'),
						'DosenID' => $word[0],
						'Tanggal' => $this->input->post('Tanggal'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_exam->PTL_update($ExamID,$data);
			$dat = array(
						'ExamID' => $ExamID,
						'DosenID' => $word[0],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_exam_dosen->PTL_update($ExamID,$dat);
			echo warning("Schedule updated in temporary data until finish next steps.","../exam/ptl_student_form/$ExamID");
		}
		
		function ptl_student_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM06",
							'aktifitas' => "Filter halaman Exam - Student Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resexam = $this->m_exam->PTL_select($ExamID);
			$data['ExamID'] = $ExamID;
			$data['TahunID'] = $resexam['TahunID'];
			$data['AddTahunID'] = $resexam['TahunID'];
			$AddTahunID = $resexam['TahunID'];
			$data['TahunID2'] = $resexam['TahunID2'];
			$data['AddTahunID2'] = $resexam['TahunID2'];
			$AddTahunID2 = $resexam['TahunID2'];
			$data['SubjekID'] = $resexam['SubjekID'];
			$data['AddSubjekID'] = $resexam['SubjekID'];
			$AddSubjekID = $resexam['SubjekID'];
			$data['SubjekID12'] = $resexam['SubjekID12'];
			$data['AddSubjekID12'] = $resexam['SubjekID12'];
			$AddSubjekID12 = $resexam['SubjekID12'];
			$data['SubjekID2'] = $resexam['SubjekID2'];
			$data['AddSubjekID2'] = $resexam['SubjekID2'];
			$AddSubjekID2 = $resexam['SubjekID2'];
			$RuangID = $resexam['RuangID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			if($this->session->userdata('checking') == "")
			{
				$rowrecord = $this->m_krs->PTL_all();
				$kapasitas = $result['KapasitasUjian'];
				$i=1;
				if($rowrecord)
				{
					foreach($rowrecord as $row)
					{
						if(($row->TahunID == $AddTahunID) OR ($row->TahunID == $AddTahunID2))
						{
							if(($row->SubjekID == $AddSubjekID) OR ($row->SubjekID == $AddSubjekID12) OR ($row->SubjekID == $AddSubjekID2))
							{
								if($i <= $kapasitas)
								{
									$urutan = $i;
									if(($urutan > 0) AND ($urutan <= $kapasitas))
									{
										$NomorKursi = $i;
										$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
										if($rkursi)
										{
											// Chair has been used
										}
										else
										{
											$da = array(
														'ExamID' => $ExamID,
														'NomorKursi' => $NomorKursi,
														'MhswID' => $row->MhswID,
														'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
														'tanggal_buat' => $this->waktu
														);
											$this->m_exam_kursi->PTL_insert_auto($da);
										}
									}
									else
									{
										// Seat numbers are not listed
									}
								}
								$i++;
							}
						}
					}
				}
			}
			$this->session->set_userdata('checking','exam');
			$this->load->view('Portal/v_header_table');
			$this->load->view('Exam/v_exam_student_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_student_form_set()
		{
			$this->authentification();
			$ExamID = $this->input->post('ExamID');
			$kapasitas = $this->input->post('kapasitas');
			$urutan = $this->input->post('NomorKursi');
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $kapasitas,
							'pk3' => $urutan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM07",
							'aktifitas' => "Filter halaman Exam - Student Form Set.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if(($urutan > 0) AND ($urutan <= $kapasitas))
			{
				$NomorKursi = $this->input->post('NomorKursi');
				$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
				if($rkursi)
				{
					echo warning("Sorry, chair with number '$NomorKursi' has been used.","../exam/ptl_student_form/$ExamID");
				}
				else
				{
					$data = array(
								'ExamID' => $ExamID,
								'NomorKursi' => $NomorKursi,
								'MhswID' => $this->input->post('MhswID'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_exam_kursi->PTL_insert($data);
					redirect("exam/ptl_student_form/$ExamID");
				}
			}
			else
			{
				echo warning("Seat numbers are not listed.","../exam/ptl_student_form/$ExamID");
			}
		}
		
		function ptl_student_form_delete()
		{
			$this->authentification();
			$ExamKursiID = $this->uri->segment(3);
			$ExamID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ExamKursiID,
							'pk2' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM08",
							'aktifitas' => "Filter halaman Exam - Student Form Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_exam_kursi->PTL_delete($ExamKursiID);
			redirect("exam/ptl_student_form/$ExamID");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM09",
							'aktifitas' => "Filter halaman Exam - Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_exam->PTL_select($ExamID);
			$data['ExamID'] = $result['ExamID'];
			$data['TahunID'] = $result['TahunID'];
			$data['TahunID2'] = $result['TahunID2'];
			$data['Tanggal'] = $result['Tanggal'];
			$data['JamMulai'] = $result['JamMulai'];
			$data['JamSelesai'] = $result['JamSelesai'];
			$SubjekID = $result['SubjekID'];
			$ressub1 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub1)
			{
				$data['SubjekID'] = $SubjekID.' - '.$ressub1['Nama'].' - ('.$ressub1['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID'] = $result['SubjekID'];
			}
			$SubjekID = $result['SubjekID12'];
			$ressub2 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub2)
			{
				$data['SubjekID12'] = $SubjekID.' - '.$ressub2['Nama'].' - ('.$ressub2['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID12'] = $result['SubjekID12'];
			}
			$SubjekID = $result['SubjekID2'];
			$ressub3 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub3)
			{
				$data['SubjekID2'] = $SubjekID.' - '.$ressub3['Nama'].' - ('.$ressub3['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID2'] = $result['SubjekID2'];
			}
			$data['RuangID'] = $result['RuangID'];
			
			$result2 = $this->m_exam_dosen->PTL_select($ExamID);
			$DosenID = $result2['DosenID'];
			$result3 = $this->m_dosen->PTL_select($DosenID);
			$data['DosenID'] = '';
			if($result3)
			{
				$data['DosenID'] = $result2['DosenID']." - ".$result3['Nama'];
			}
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Exam/v_exam_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_edit_update()
		{
			$this->authentification();
			$ExamID = $this->input->post('ExamID');
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM10",
							'aktifitas' => "Filter halaman Exam - Edit Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$word = explode(" - ",$this->input->post('DosenID'));
			$data = array(
						'ExamID' => $ExamID,
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'RuangID' => $this->input->post('RuangID'),
						'DosenID' => $word[0],
						'Tanggal' => $this->input->post('Tanggal'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_exam->PTL_update($ExamID,$data);
			$dat = array(
						'ExamID' => $ExamID,
						'DosenID' => $word[0],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_exam_dosen->PTL_update($ExamID,$dat);
			echo warning("Schedule saved in temporary data until finish next steps.","../exam/ptl_student_edit/$ExamID");
		}
		
		function ptl_student_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM11",
							'aktifitas' => "Filter halaman Exam - Student Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resexam = $this->m_exam->PTL_select($ExamID);
			$data['ExamID'] = $ExamID;
			$data['TahunID'] = $resexam['TahunID'];
			$data['AddTahunID'] = $resexam['TahunID'];
			$data['TahunID2'] = $resexam['TahunID2'];
			$data['AddTahunID2'] = $resexam['TahunID2'];
			$data['SubjekID'] = $resexam['SubjekID'];
			$data['AddSubjekID'] = $resexam['SubjekID'];
			$data['SubjekID12'] = $resexam['SubjekID12'];
			$data['AddSubjekID12'] = $resexam['SubjekID12'];
			$data['SubjekID2'] = $resexam['SubjekID2'];
			$data['AddSubjekID2'] = $resexam['SubjekID2'];
			$RuangID = $resexam['RuangID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Exam/v_exam_student_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_student_edit_set()
		{
			$this->authentification();
			$ExamID = $this->input->post('ExamID');
			$kapasitas = $this->input->post('kapasitas');
			$urutan = $this->input->post('NomorKursi');
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $kapasitas,
							'pk3' => $urutan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM12",
							'aktifitas' => "Filter halaman Exam - Student Edit Set.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if(($urutan > 0) AND ($urutan <= $kapasitas))
			{
				$NomorKursi = $this->input->post('NomorKursi');
				$rkursi = $this->m_exam_kursi->PTL_select_kursi($ExamID,$NomorKursi);
				if($rkursi)
				{
					echo warning("Sorry, chair with number '$NomorKursi' has been used.","../exam/ptl_student_edit/$ExamID");
				}
				else
				{
					$data = array(
								'ExamID' => $ExamID,
								'NomorKursi' => $NomorKursi,
								'MhswID' => $this->input->post('MhswID'),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_exam_kursi->PTL_insert($data);
					redirect("exam/ptl_student_edit/$ExamID");
				}
			}
			else
			{
				echo warning("Seat numbers are not listed.","../exam/ptl_student_edit/$ExamID");
			}
		}
		
		function ptl_student_edit_delete()
		{
			$this->authentification();
			$ExamKursiID = $this->uri->segment(3);
			$ExamID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ExamKursiID,
							'pk2' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM13",
							'aktifitas' => "Filter halaman Exam - Student Edit Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_exam_kursi->PTL_delete($ExamKursiID);
			redirect("exam/ptl_student_edit/$ExamID");
		}
		
		function ptl_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$RuangID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM14",
							'aktifitas' => "Filter halaman Exam - Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resexam = $this->m_exam->PTL_select($ExamID);
			$data['ExamID'] = $ExamID;
			$data['TahunID'] = $resexam['TahunID'];
			$data['TahunID2'] = $resexam['TahunID2'];
			$data['SubjekID'] = $resexam['SubjekID'];
			$data['SubjekID12'] = $resexam['SubjekID12'];
			$data['SubjekID2'] = $resexam['SubjekID2'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Exam/v_exam_attendance_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_attendance_insert()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			$nosave = 0;
			for($i=1;$i<=$total;$i++)
			{
				$ExamID = $this->input->post("ExamID$i");
				$MhswID = $this->input->post("MhswID$i");
				$res = $this->m_exam_mahasiswa->PTL_all_evaluation($ExamID,$MhswID);
				if($res)
				{
					$data = array(
								'Presensi' => $this->input->post("Presensi$i"),
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_exam_mahasiswa->PTL_update($ExamID,$MhswID,$data);
				}
				else
				{
					$data = array(
								'ExamID' => $this->input->post("ExamID$i"),
								'MhswID' => $this->input->post("MhswID$i"),
								'Presensi' => $this->input->post("Presensi$i"),
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_exam_mahasiswa->PTL_insert($data);
					$nosave++;
				}
			}
			$ExamID = $this->input->post("exam");
			$RuangID = $this->input->post("ruang");
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM15",
							'aktifitas' => "Filter halaman Exam - Attendance Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			echo warning("Your data successfully saved. $nosave new data added.","../exam/ptl_attendance/$ExamID/$RuangID");
		}
		
		function ptl_scoring()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$ExamID = $this->uri->segment(3);
			$RuangID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM16",
							'aktifitas' => "Filter halaman Exam - Scoring.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resexam = $this->m_exam->PTL_select($ExamID);
			$data['ExamID'] = $ExamID;
			$data['TahunID'] = $resexam['TahunID'];
			$data['TahunID2'] = $resexam['TahunID2'];
			$data['SubjekID'] = $resexam['SubjekID'];
			$data['SubjekID12'] = $resexam['SubjekID12'];
			$data['SubjekID2'] = $resexam['SubjekID2'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Exam/v_exam_scoring_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_scoring_insert()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			$nosave = 0;
			$pesan = "";
			$DataAktifitas = "";
			for($i=1;$i<=$total;$i++)
			{
				$ExamID = $this->input->post("ExamID$i");
				$MhswID = $this->input->post("MhswID$i");
				$nama = $this->input->post("nama$i");
				$res = $this->m_exam_mahasiswa->PTL_all_evaluation($ExamID,$MhswID);
				if($res)
				{
					$DataAktifitas .= "$ExamID-$MhswID-".$this->input->post("Nilai$i").";";
					if($res['Presensi'] == "Y")
					{
						$data = array(
									'Nilai' => $this->input->post("Nilai$i"),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_exam_mahasiswa->PTL_update($ExamID,$MhswID,$data);
					}
					else
					{
						$data = array(
									'Nilai' => 0,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_exam_mahasiswa->PTL_update($ExamID,$MhswID,$data);
						if($ExamID != "")
						{
							$pesan .= "$MhswID - $nama ($res[Presensi])".'\n';
						}
					}
				}
				else
				{
					// $data = array(
								// 'ExamID' => $this->input->post("ExamID$i"),
								// 'MhswID' => $this->input->post("MhswID$i"),
								// 'Nilai' => 0,
								// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								// 'tanggal_buat' => $this->waktu
								// );
					// $this->m_exam_mahasiswa->PTL_insert($data);
					// $pesan .= "$MhswID - $nama".'\n';
					$nosave++;
				}
			}
			$psn = "";
			if($pesan != "")
			{
				$psn = '\n\n'."Students who have not filled attendance.".'\n'.$pesan;
			}
			$ExamID = $this->input->post("exam");
			$RuangID = $this->input->post("ruang");
			$datalog = array(
							'pk1' => $ExamID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM17",
							'aktifitas' => "Filter halaman Exam - Scoring Insert.",
							'data' => $DataAktifitas,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			echo warning("Your data successfully saved.$psn","../exam/ptl_scoring/$ExamID/$RuangID");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$ExamID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $ExamID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM18",
							'aktifitas' => "Filter halaman Exam - Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_exam->PTL_delete($ExamID);
			$this->m_exam_dosen->PTL_delete_exam($ExamID);
			$this->m_exam_kursi->PTL_delete_exam($ExamID);
			$this->m_exam_mahasiswa->PTL_delete_exam($ExamID);
			echo warning("Your data successfully deleted.","../exam");
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mahasiswa->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->MhswID,
											'value' => $row->MhswID." - ".$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('exam/index',$data);
			}
		}
		
		function ptl_cari()
		{
			$this->authentification();
			$cari = $this->input->post('cari');
			$datalog = array(
							'pk1' => $cari,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM19-01",
							'aktifitas' => "Filter halaman Exam - Search: $cari.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cari != "")
			{
				$this->session->set_userdata('exam_filter_mahasiswa',$cari);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_mahasiswa');
			}
			redirect("exam/ptl_card");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM19-02",
							'aktifitas' => "Filter halaman Exam - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('exam_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_jur');
			}
			redirect("exam/ptl_card");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM19-03",
							'aktifitas' => "Filter halaman Exam - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('exam_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('exam_filter_tahun');
			}
			redirect("exam/ptl_card");
		}
		
		function ptl_filter_exam_pass_date()
		{
			$this->authentification();
			$this->session->set_userdata('exam_filter_sort_date','Y');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM19-04",
							'aktifitas' => "Filter halaman Exam - Pass Date.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("exam/ptl_card");
		}
		
		function ptl_filter_exam_pass_undate()
		{
			$this->authentification();
			$this->session->unset_userdata('exam_filter_sort_date');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM19-05",
							'aktifitas' => "Filter halaman Exam - Pass Undate.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("exam/ptl_card");
		}
		
		function ptl_card()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$data['id'] = "mahasiswa";
			$data['controller'] = "drop/lookup";
			$word = explode(" - ",$this->session->userdata('exam_filter_mahasiswa'));
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$cekjurusan = $this->session->userdata('exam_filter_jur');
			$TahunID = $this->session->userdata('exam_filter_tahun');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$sort = $this->session->userdata('exam_filter_sort_date');
			if($sort == "Y")
			{
				$data['rowrecord'] = $this->m_krs->PTL_all_select_exam_card_date($MhswID,$TahunID);
			}
			else
			{
				$data['rowrecord'] = $this->m_krs->PTL_all_select_exam_card($MhswID,$TahunID);
			}
			$datalog = array(
							'pk1' => $MhswID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM20",
							'aktifitas' => "Mengakses halaman Exam - Card.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Exam/v_exam_card',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf_list()
		{
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$TahunID2 = $this->session->userdata('exam_filter_tahun2');
			$RuangID = $this->session->userdata('exam_filter_ruang');
			$cekdosen = $this->session->userdata('dra_filter_dosen');
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $TahunID2,
							'pk3' => $RuangID,
							'pk4' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM21",
							'aktifitas' => "Mengakses halaman Exam - PDF List.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_exam->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"EXAM SCHEDULE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(0.5, 1, "#", "LBTR", 0, "C");
			$this->fpdf->Cell(1, 1, "EID", "BTR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (1,2)", "BTR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (3)", "BTR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "BTR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "BTR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "TIME", "BTR", 0, "C");
			$this->fpdf->Cell(4, 1, "SUPERVISOR", "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",7);
			foreach($result as $r)
			{
				if($no == $tot)
				{
					$SubjekID = $r->SubjekID;
					$s1 = $this->m_subjek->PTL_select($SubjekID);
					$subjek1 = "";
					if($s1)
					{
						$subjek1 = $s1["Nama"];
					}
					$SubjekID = $r->SubjekID12;
					$s2 = $this->m_subjek->PTL_select($SubjekID);
					$subjek2 = "";
					if($s2)
					{
						$subjek2 = $s2["Nama"];
					}
					$SubjekID = $r->SubjekID2;
					$s3 = $this->m_subjek->PTL_select($SubjekID);
					$subjek3 = "";
					if($s3)
					{
						$subjek3 = $s3["Nama"];
					}
					$ExamID = $r->ExamID;
					$e = $this->m_exam_dosen->PTL_select($ExamID);
					$DosenID = "";
					if($e)
					{
						$DosenID = $e["DosenID"];
						$d = $this->m_dosen->PTL_select($DosenID);
						$dosen = "No Supervisor";
						if($d)
						{
							$dosen = $d["Nama"];
						}
					}
					if($cekdosen == "")
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
						$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
						$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
						$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
						$no++;
						$tot++;
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
				else
				{
					$SubjekID = $r->SubjekID;
					$s1 = $this->m_subjek->PTL_select($SubjekID);
					$subjek1 = "";
					if($s1)
					{
						$subjek1 = $s1["Nama"];
					}
					$SubjekID = $r->SubjekID12;
					$s2 = $this->m_subjek->PTL_select($SubjekID);
					$subjek2 = "";
					if($s2)
					{
						$subjek2 = $s2["Nama"];
					}
					$SubjekID = $r->SubjekID2;
					$s3 = $this->m_subjek->PTL_select($SubjekID);
					$subjek3 = "";
					if($s3)
					{
						$subjek3 = $s3["Nama"];
					}
					$ExamID = $r->ExamID;
					$e = $this->m_exam_dosen->PTL_select($ExamID);
					$DosenID = "";
					if($e)
					{
						$DosenID = $e["DosenID"];
						$d = $this->m_dosen->PTL_select($DosenID);
						$dosen = "No Supervisor";
						if($d)
						{
							$dosen = $d["Nama"];
						}
					}
					if($cekdosen == "")
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
						$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
						$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
						$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
						$no++;
						$tot++;
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_class()
		{
			$examid = $this->uri->segment(3);
			$ruangid = $this->uri->segment(4);
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$TahunID2 = $this->session->userdata('exam_filter_tahun2');
			$RuangID = $this->session->userdata('exam_filter_ruang');
			$cekdosen = $this->session->userdata('dra_filter_dosen');
			$datalog = array(
							'pk1' => $examid,
							'pk2' => $TahunID,
							'pk3' => $TahunID2,
							'pk4' => $RuangID,
							'pk5' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM22",
							'aktifitas' => "Mengakses halaman Exam - PDF Class.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_exam->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"EXAM SCHEDULE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(0.5, 1, "#", "LTR", 0, "C");
			$this->fpdf->Cell(1, 1, "EID", "TR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (1,2)", "TR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (3)", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "TR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "TIME", "TR", 0, "C");
			$this->fpdf->Cell(4, 1, "SUPERVISOR", "TR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",7);
			foreach($result as $r)
			{
				if($no == $tot)
				{
					if($r->ExamID == $examid)
					{
						$SubjekID = $r->SubjekID;
						$s1 = $this->m_subjek->PTL_select($SubjekID);
						$subjek1 = "";
						if($s1)
						{
							$subjek1 = $s1["Nama"];
						}
						$SubjekID = $r->SubjekID12;
						$s2 = $this->m_subjek->PTL_select($SubjekID);
						$subjek2 = "";
						if($s2)
						{
							$subjek2 = $s2["Nama"];
						}
						$SubjekID = $r->SubjekID2;
						$s3 = $this->m_subjek->PTL_select($SubjekID);
						$subjek3 = "";
						if($s3)
						{
							$subjek3 = $s3["Nama"];
						}
						$ExamID = $r->ExamID;
						$e = $this->m_exam_dosen->PTL_select($ExamID);
						$DosenID = "";
						if($e)
						{
							$DosenID = $e["DosenID"];
							$d = $this->m_dosen->PTL_select($DosenID);
							$dosen = "No Supervisor";
							if($d)
							{
								$dosen = $d["Nama"];
							}
						}
						if($cekdosen == "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
				else
				{
					if($r->ExamID == $examid)
					{
						$SubjekID = $r->SubjekID;
						$s1 = $this->m_subjek->PTL_select($SubjekID);
						$subjek1 = "";
						if($s1)
						{
							$subjek1 = $s1["Nama"];
						}
						$SubjekID = $r->SubjekID12;
						$s2 = $this->m_subjek->PTL_select($SubjekID);
						$subjek2 = "";
						if($s2)
						{
							$subjek2 = $s2["Nama"];
						}
						$SubjekID = $r->SubjekID2;
						$s3 = $this->m_subjek->PTL_select($SubjekID);
						$subjek3 = "";
						if($s3)
						{
							$subjek3 = $s3["Nama"];
						}
						$ExamID = $r->ExamID;
						$e = $this->m_exam_dosen->PTL_select($ExamID);
						$DosenID = "";
						if($e)
						{
							$DosenID = $e["DosenID"];
							$d = $this->m_dosen->PTL_select($DosenID);
							$dosen = "No Supervisor";
							if($d)
							{
								$dosen = $d["Nama"];
							}
						}
						if($cekdosen == "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
						else
						{
							if($DosenID == $cekdosen)
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
								$this->fpdf->Cell(1, 1, $r->ExamID, "TR", 0, "C");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
								$this->fpdf->Cell(1.5, 0.5, $r->RuangID, "TR", 0, "C");
								$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "TR", 0, "C");
								$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
								$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
								$this->fpdf->Ln();
								$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(2, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
								$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
								$no++;
								$tot++;
							}
						}
					}
				}
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_exam_pass()
		{
			$word = explode(" - ",$this->session->userdata('exam_filter_mahasiswa'));
			$MhswID = $word[0];
			$TahunID = $this->session->userdata('exam_filter_tahun');
			$sort = $this->session->userdata('exam_filter_sort_date');
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $TahunID,
							'pk3' => $sort,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EXAM23",
							'aktifitas' => "Mengakses halaman Exam - PDF Exam Pass: $MhswID - $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($sort == "Y")
			{
				$result = $this->m_krs->PTL_all_select_exam_card_date($MhswID,$TahunID);
			}
			else
			{
				$result = $this->m_krs->PTL_all_select_exam_card($MhswID,$TahunID);
			}
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"EXAM PASS",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$reskhs = $this->m_khs->PTL_all_select($MhswID);
			$reskhs2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$kelas1 = "";
			$kelas2 = "";
			$kelas3 = "";
			if($reskhs)
			{
				foreach($reskhs as $rk)
				{
					$KelasID = $rk->KelasID;
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					if($rk->TahunKe == 1)
					{
						if($reskelas)
						{
							$kelas1 = $rk->TahunKe.$reskelas["Nama"];
						}
						else
						{
							$kelas1 = $rk->TahunKe."(not set)";
						}
					}
					if($rk->TahunKe == 2)
					{
						if($reskelas)
						{
							$kelas2 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
						else
						{
							$kelas2 = " > ".$rk->TahunKe."(not set)";
						}
					}
					if($rk->TahunKe == 3)
					{
						if($reskelas)
						{
							$kelas3 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
						else
						{
							$kelas3 = " > ".$rk->TahunKe."(not set)";
						}
					}
				}
			}
			$ProgramID = $resmhsw["ProgramID"];
			$resprog = $this->m_program->PTL_select($ProgramID);
			$ProdiID = $resmhsw["ProdiID"];
			$resprod = $this->m_prodi->PTL_select($ProdiID);
			if($resmhsw["Foto"] == "")
			{
				$foto = "foto_umum/user.jpg";
			}
			else
			{
				$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				$exist = file_exists_remote(base_url("ptl_storage/$foto"));
				if($exist)
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				}
				else
				{
					$foto = "foto_umum/user.jpg";
				}
			}
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(3, 0.5, "SIN", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $MhswID", "", 0, "L");
			$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX()+9,$this->fpdf->getY(),3,4);
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "NAME", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $resmhsw[Nama]", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "CLASS", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $kelas1$kelas2$kelas3", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "PROGRAM", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": ".strtoupper($resprog["Nama"]), "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "MAJOR", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $resmhsw[ProdiID] - ".strtoupper($resprod["Nama"]), "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "SEMESTER / YEAR", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": Semester $reskhs2[Sesi] / $restahun[Nama]", "", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1.5, 1, "CODE", "LTR", 0, "C");
			$this->fpdf->Cell(8, 1, "SUBJECT", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "TR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "TR", 0, "C");
			$this->fpdf->Cell(3, 1, "TIME", "TR", 0, "C");
			$this->fpdf->Cell(3, 1, "TEACHER SIGN", "TR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",9);
			foreach($result as $r)
			{
				$TahunID = $r->TahunID;
				$SubjekID = $r->SubjekID;
				$resexam = $this->m_exam->PTL_all_select_exam_card($TahunID,$SubjekID);
				$exam = "";
				if($resexam)
				{
					foreach($resexam as $re)
					{
						if(($re->TahunID == $TahunID) OR ($re->TahunID2 == $TahunID))
						{
							if(($re->SubjekID == $SubjekID) OR ($re->SubjekID12 == $SubjekID) OR ($re->SubjekID2 == $SubjekID))
							{
								$ExamID = $re->ExamID;
								$reskursi = $this->m_exam_kursi->PTL_select($ExamID,$MhswID);
								$ressubjek = $this->m_subjek->PTL_select($SubjekID);
								if($reskursi)
								{
									$this->fpdf->Ln();
									$this->fpdf->Cell(1.5, 1, $ressubjek["SubjekKode"], "LBTR", 0, "C");
									$this->fpdf->Cell(8, 1, $ressubjek["Nama"], "BTR", 0, "L");
									$this->fpdf->Cell(1.5, 1, $re->RuangID, "BTR", 0, "C");
									$this->fpdf->Cell(2, 1, tgl_singkat_eng($re->Tanggal), "BTR", 0, "C");
									$this->fpdf->Cell(3, 1, "$re->JamMulai - $re->JamSelesai", "BTR", 0, "C");
									$this->fpdf->Cell(3, 1, "", "BTR", 0, "L");
								}
							}
						}
					}
				}
			}
			$this->fpdf->Ln(2);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14, 0.7, "BRING THIS EXAM PASS DURING THE EXAMINATION.", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.5, "IF THE EXAM PASS IS LOST, PLEASE APPROACH STUDENT AFFAIR OFFICE FOR", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(14 , 0.5, "REPLACEMENT OF EXAM PASS ON PAYMENT OF Rp 200.000,-", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(14 , 0.5, "IF FOUND THIS CARD, PLEASE RETURN TO THE BELOW ADDRESS:", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "Jalan Asem Dua No. 3 - 5, Jakarta Selatan 12410", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "Tel. (021) 7659181 Fax.(021) 7657517", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "email: info@esmodjakarta.com", "", 0, "L");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
	}
?>