<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Remedial extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->model('m_aktifitas');
			$this->load->model('m_catatan2');
			$this->load->model('m_dosen');
			$this->load->model('m_remedial');
			$this->load->model('m_exam');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_jadwal');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_nilai');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_remedial_krs');
			$this->load->model('m_ruang');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup_subjek1()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('remedial_filter_tahun1');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('remedial/index',$data);
			}
		}
		
		function lookup_subjek2()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('remedial_filter_tahun1');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('remedial/index',$data);
			}
		}
		
		function lookup_subjek3()
		{
			$keyword = $this->input->post('term');
			$TahunID = $this->session->userdata('remedial_filter_tahun2');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											'prodi' => '',
											''
											);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year',
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('remedial/index',$data);
			}
		}
		
		function ptl_filter_tahun1()
		{
			$this->authentification();
			$cektahun1 = $this->input->post('cektahun1');
			$datalog = array(
							'pk1' => $cektahun1,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED01-01",
							'aktifitas' => "Filter halaman Remedial - Year 1: $cektahun1.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun1 != "")
			{
				$this->session->set_userdata('remedial_filter_tahun1',$cektahun1);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_tahun1');
			}
			redirect("remedial");
		}
		
		function ptl_filter_tahun2()
		{
			$this->authentification();
			$cektahun2 = $this->input->post('cektahun2');
			$datalog = array(
							'pk1' => $cektahun2,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED01-02",
							'aktifitas' => "Filter halaman Remedial - Year 2: $cektahun2.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun2 != "")
			{
				$this->session->set_userdata('remedial_filter_tahun2',$cektahun2);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_tahun2');
			}
			redirect("remedial");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			$datalog = array(
							'pk1' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED01-03",
							'aktifitas' => "Filter halaman Remedial - Lecturer: $cekdosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekdosen != "")
			{
				$this->session->set_userdata('remedial_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_dosen');
			}
			redirect("remedial");
		}
		
		function ptl_filter_ruang()
		{
			$this->authentification();
			$cekruang = $this->input->post('cekruang');
			$datalog = array(
							'pk1' => $cekruang,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED01-04",
							'aktifitas' => "Filter halaman Remedial - Room: $cekruang.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekruang != "")
			{
				$this->session->set_userdata('remedial_filter_ruang',$cekruang);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_ruang');
			}
			redirect("remedial");
		}
		
		function ptl_filter_exam()
		{
			$this->authentification();
			$JadwalRemedialID = $this->input->post('JadwalRemedialID');
			$cekexam = $this->input->post('cekexam');
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $cekexam,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED01-05",
							'aktifitas' => "Filter halaman Remedial - Exam.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekexam != "")
			{
				$this->session->set_userdata('remedial_filter_exam',$cekexam);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_exam');
			}
			redirect("remedial/ptl_student_form/$JadwalRemedialID");
		}
		
		function lookup_dosen()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_dosen->lookup_dosen($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->Login,
											'value' => $row->Login.' - '.$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('remedial/index',$data);
			}
		}
		
		function index()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED02",
							'aktifitas' => "Mengakses halaman Remedial.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$TahunID = $this->session->userdata('remedial_filter_tahun1');
			$TahunID2 = $this->session->userdata('remedial_filter_tahun2');
			$RuangID = $this->session->userdata('remedial_filter_ruang');
			$data['rowtahun1'] = $this->m_year->PTL_all_d3();
			$data['rowtahun2'] = $this->m_year->PTL_all_d1();
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowrecord'] = $this->m_remedial->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			$thn1 = $this->session->userdata('remedial_filter_tahun1');
			$thn2 = $this->session->userdata('remedial_filter_tahun2');
			$dosen = $this->session->userdata('remedial_filter_dosen');
			$ruang = $this->session->userdata('remedial_filter_ruang');
			if(($thn1 == "") AND ($thn2 == "") AND ($dosen == "") AND ($ruang == ""))
			{
				$this->session->sess_destroy();
			}
			$this->session->set_userdata('menu','exam');
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				$this->load->view('Remedial/v_remedial_lecturer',$data);
			}
			else
			{
				$this->load->view('Remedial/v_remedial',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED03",
							'aktifitas' => "Mengakses halaman Remedial - Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial->PTL_select($JadwalRemedialID);
			if($result)
			{
				$data['JadwalRemedialID'] = $result['JadwalRemedialID'];
				$data['RuangID'] = $result['UjianRuangID'];
				$data['TglMulai'] = $result['TglMulai'];
				$data['TglSelesai'] = $result['TglSelesai'];
				$data['TahunID'] = $result['TahunID'];
				$data['TahunID2'] = $result['TahunID2'];
				$data['JamMulai'] = $result['JamMulai'];
				$data['JamSelesai'] = $result['JamSelesai'];
				$data['JmlhMhsw'] = $result['JmlhMhsw'];
				$SubjekID = $result['SubjekID'];
				$ressub1 = $this->m_subjek->PTL_select($SubjekID);
				if($ressub1)
				{
					$data['SubjekID'] = $SubjekID.' - '.$ressub1['Nama'].' - ('.$ressub1['SubjekKode'].')';
				}
				else
				{
					$data['SubjekID'] = "";
				}
				$SubjekID = $result['SubjekID12'];
				$ressub2 = $this->m_subjek->PTL_select($SubjekID);
				if($ressub2)
				{
					$data['SubjekID12'] = $SubjekID.' - '.$ressub2['Nama'].' - ('.$ressub2['SubjekKode'].')';
				}
				else
				{
					$data['SubjekID12'] = "";
				}
				$SubjekID = $result['SubjekID2'];
				$ressub3 = $this->m_subjek->PTL_select($SubjekID);
				if($ressub3)
				{
					$data['SubjekID2'] = $SubjekID.' - '.$ressub3['Nama'].' - ('.$ressub3['SubjekKode'].')';
				}
				else
				{
					$data['SubjekID2'] = "";
				}
				$DosenID = $result['DosenID'];
				$result2 = $this->m_dosen->PTL_select($DosenID);
				if($result2)
				{
					$data['DosenID'] = $DosenID." - ".$result2['Nama'];
				}
				else
				{
					$data['DosenID'] = "";
				}
			}
			else
			{
				$data['JadwalRemedialID'] = '';
				$data['RuangID'] = '';
				$data['TglMulai'] = '';
				$data['TglSelesai'] = '';
				$data['TahunID'] = '';
				$data['TahunID2'] = '';
				$data['JamMulai'] = '';
				$data['JamSelesai'] = '';
				$data['JmlhMhsw'] = '';
				$data['SubjekID'] = '';
				$data['SubjekID12'] = '';
				$data['SubjekID2'] = '';
				$data['DosenID'] = '';
			}
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Remedial/v_remedial_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('DosenID'));
			$result = $this->m_remedial->PTL_urut();
			$JadwalRemedialID = $result['LAST'] + 1;
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED04",
							'aktifitas' => "Mengakses halaman Remedial - Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$data = array(
						'JadwalRemedialID' => $JadwalRemedialID,
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'UjianRuangID' => $this->input->post('RuangID'),
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'TahunID' => $this->session->userdata('remedial_filter_tahun1'),
						'TahunID2' => $this->session->userdata('remedial_filter_tahun2'),
						// 'JmlhMhsw' => $this->input->post('JmlhMhsw'),
						'DosenID' => $word[0],
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_remedial->PTL_insert($data);
			echo warning("ASchedule saved in temporary data until finish next steps.","../remedial/ptl_student_form/$JadwalRemedialID");
		}
		
		function ptl_update()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('DosenID'));
			$JadwalRemedialID = $this->input->post('JadwalRemedialID');
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED05",
							'aktifitas' => "Mengakses halaman Remedial - Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$data = array(
						'JadwalRemedialID' => $JadwalRemedialID,
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'UjianRuangID' => $this->input->post('RuangID'),
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'TahunID' => $this->session->userdata('remedial_filter_tahun1'),
						'TahunID2' => $this->session->userdata('remedial_filter_tahun2'),
						// 'JmlhMhsw' => $this->input->post('JmlhMhsw'),
						'DosenID' => $word[0],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_remedial->PTL_update($JadwalRemedialID,$data);
			echo warning("Schedule updated in temporary data until finish next steps.","../remedial/ptl_student_form/$JadwalRemedialID");
		}
		
		function ptl_student_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED06",
							'aktifitas' => "Mengakses halaman Remedial - Student Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resremedial = $this->m_remedial->PTL_select($JadwalRemedialID);
			$data['JadwalRemedialID'] = $resremedial['JadwalRemedialID'];
			$data['RuangID'] = $resremedial['UjianRuangID'];
			$data['TahunID'] = $resremedial['TahunID'];
			$data['AddTahunID'] = $resremedial['TahunID'];
			$AddTahunID = $resremedial['TahunID'];
			$data['TahunID2'] = $resremedial['TahunID2'];
			$data['AddTahunID2'] = $resremedial['TahunID2'];
			$AddTahunID2 = $resremedial['TahunID2'];
			$data['SubjekID'] = $resremedial['SubjekID'];
			$data['AddSubjekID'] = $resremedial['SubjekID'];
			$AddSubjekID = $resremedial['SubjekID'];
			$data['SubjekID12'] = $resremedial['SubjekID12'];
			$data['AddSubjekID12'] = $resremedial['SubjekID12'];
			$AddSubjekID12 = $resremedial['SubjekID12'];
			$data['SubjekID2'] = $resremedial['SubjekID2'];
			$data['AddSubjekID2'] = $resremedial['SubjekID2'];
			$AddSubjekID2 = $resremedial['SubjekID2'];
			$data['DosenID'] = $resremedial['DosenID'];
			$RuangID = $resremedial['UjianRuangID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			
			if($this->session->userdata('checking_remedial') == "")
			{
				$rowrecord = $this->m_krs->PTL_all();
				$kapasitas = $result['KapasitasUjian'];
				$i=1;
				if($rowrecord)
				{
					foreach($rowrecord as $row)
					{
						if(($row->TahunID == $AddTahunID) OR ($row->TahunID == $AddTahunID2))
						{
							if(($row->SubjekID == $AddSubjekID) OR ($row->SubjekID == $AddSubjekID12) OR ($row->SubjekID == $AddSubjekID2))
							{
								$TahunID = $AddTahunID;
								$TahunID2 = $AddTahunID2;
								$SubjekID = "";
								if($AddSubjekID != "0")
								{
									$SubjekID = $AddSubjekID;
								}
								$SubjekID12 = "";
								if($AddSubjekID12 != "0")
								{
									$SubjekID12 = $AddSubjekID12;
								}
								$SubjekID2 = "";
								if($AddSubjekID2 != "0")
								{
									$SubjekID2 = $AddSubjekID2;
								}
								$MhswID = $row->MhswID;
								$resexam = $this->m_exam->PTL_all_select_remedial($TahunID,$TahunID2,$SubjekID,$SubjekID12,$SubjekID2);
								$ExamID = "";
								$Score = "-";
								$grade = "";
								if($resexam)
								{
									$ExamID = $resexam['ExamID'];
									$SubjekID = $row->SubjekID;
									$rowsub = $this->m_subjek->PTL_select($SubjekID);
									$KurikulumID = "";
									if($rowsub)
									{
										$KurikulumID = $rowsub['KurikulumID'];
									}
									$resnilaiexam = $this->m_exam_mahasiswa->PTL_all_evaluation($ExamID,$MhswID);
									if($resnilaiexam)
									{
										$Score = $resnilaiexam['Nilai'];
										$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
										if($rownilai)
										{
											foreach($rownilai as $rn)
											{
												if((number_format($Score,2) >= $rn->NilaiMin) AND (number_format($Score,2) <= $rn->NilaiMax))
												{
													$grade = $rn->Nama;
												}
											}
										}
									}
								}
								if(($grade != "A") AND ($grade != "B") AND ($grade != "C"))
								{
									if($i <= $kapasitas)
									{
										$urutan = $i;
										if(($urutan > 0) AND ($urutan <= $kapasitas))
										{
											$NomorKursi = $i;
											$rkursi = $this->m_remedial_krs->PTL_select_kursi($JadwalRemedialID,$NomorKursi);
											if($rkursi)
											{
												// Chair has been used
											}
											else
											{
												$rkrs = $this->m_krs->PTL_select_krs($MhswID,$SubjekID);
												if($rkrs)
												{
													$da = array(
																'NomorKursi' => $NomorKursi,
																'KRSID' => $rkrs['KRSID'],
																'KHSID' => $rkrs['KHSID'],
																'MhswID' => $MhswID,
																'TahunID' => $rkrs['TahunID'],
																'SubjekID' => $rkrs['SubjekID'],
																'JadwalRemedialID' => $JadwalRemedialID,
																'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																'tanggal_buat' => $this->waktu
																);
													$this->m_remedial_krs->PTL_insert_auto($da);
												}
												else
												{
													// KRS not found
												}
											}
										}
										else
										{
											// Seat numbers are not listed
										}
									}
									$i++;
								}
							}
						}
					}
				}
			}
			$this->session->set_userdata('checking_remedial','remedial');
			
			$this->load->view('Portal/v_header_table');
			$this->load->view('Remedial/v_remedial_student_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_student_form_set()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$JadwalRemedialID = $this->input->post('JadwalRemedialID');
			$SubjekID = $this->input->post('SubjekID');
			$kapasitas = $this->input->post('kapasitas');
			$urutan = $this->input->post('NomorKursi');
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $SubjekID,
							'pk3' => $kapasitas,
							'pk4' => $urutan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED07",
							'aktifitas' => "Mengakses halaman Remedial - Student Form Set.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if(($urutan > 0) AND ($urutan <= $kapasitas))
			{
				$NomorKursi = $this->input->post('NomorKursi');
				$rkursi = $this->m_remedial_krs->PTL_select_kursi($JadwalRemedialID,$NomorKursi);
				if($rkursi)
				{
					echo warning("Sorry, chair with number '$NomorKursi' has been used.","../remedial/ptl_student_form/$JadwalRemedialID");
				}
				else
				{
					$rkrs = $this->m_krs->PTL_select_krs($MhswID,$SubjekID);
					if($rkrs)
					{
						$data = array(
									'NomorKursi' => $NomorKursi,
									'KRSID' => $rkrs['KRSID'],
									'KHSID' => $rkrs['KHSID'],
									'MhswID' => $MhswID,
									'TahunID' => $rkrs['TahunID'],
									'SubjekID' => $rkrs['SubjekID'],
									'JadwalRemedialID' => $JadwalRemedialID,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_remedial_krs->PTL_insert($data);
						redirect("remedial/ptl_student_form/$JadwalRemedialID");
					}
					else
					{
						echo warning("KRS not found.","../remedial/ptl_student_form/$JadwalRemedialID");
					}
				}
			}
			else
			{
				echo warning("Seat numbers are not listed.","../remedial/ptl_student_form/$JadwalRemedialID");
			}
		}
		
		function ptl_student_form_delete()
		{
			$this->authentification();
			$KRSRemedialID = $this->uri->segment(3);
			$JadwalRemedialID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $KRSRemedialID,
							'pk2' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED08",
							'aktifitas' => "Mengakses halaman Remedial - Student Form Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial_krs->PTL_delete($KRSRemedialID);
			redirect("remedial/ptl_student_form/$JadwalRemedialID");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED09",
							'aktifitas' => "Mengakses halaman Remedial - Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial->PTL_select($JadwalRemedialID);
			$data['JadwalRemedialID'] = $result['JadwalRemedialID'];
			$SubjekID = $result['SubjekID'];
			$ressub1 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub1)
			{
				$data['SubjekID'] = $SubjekID.' - '.$ressub1['Nama'].' - ('.$ressub1['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID'] = "";
			}
			$SubjekID = $result['SubjekID12'];
			$ressub2 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub2)
			{
				$data['SubjekID12'] = $SubjekID.' - '.$ressub2['Nama'].' - ('.$ressub2['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID12'] = "";
			}
			$SubjekID = $result['SubjekID2'];
			$ressub3 = $this->m_subjek->PTL_select($SubjekID);
			if($ressub3)
			{
				$data['SubjekID2'] = $SubjekID.' - '.$ressub3['Nama'].' - ('.$ressub3['SubjekKode'].')';
			}
			else
			{
				$data['SubjekID2'] = "";
			}
			$data['RuangID'] = $result['UjianRuangID'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['TahunID'] = $result['TahunID'];
			$data['TahunID2'] = $result['TahunID2'];
			$data['JamMulai'] = $result['JamMulai'];
			$data['JamSelesai'] = $result['JamSelesai'];
			$data['JmlhMhsw'] = $result['JmlhMhsw'];
			$data['DosenID'] = $result['DosenID'];
			
			$DosenID = $result['DosenID'];
			$result2 = $this->m_dosen->PTL_select($DosenID);
			if($result2)
			{
				$data['DosenID'] = $DosenID." - ".$result2['Nama'];
			}
			else
			{
				$data['DosenID'] = "";
			}
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Remedial/v_remedial_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_edit_update()
		{
			$this->authentification();
			$word = explode(" - ",$this->input->post('DosenID'));
			$JadwalRemedialID = $this->input->post('JadwalRemedialID');
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED10",
							'aktifitas' => "Mengakses halaman Remedial - Edit Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$SubA = "";
			$SubB = "";
			$SubC = "";
			if($this->input->post('SubjekID') != "")
			{
				$wordA = explode(" - ",$this->input->post('SubjekID'));
				$SubA = $wordA[0];
			}
			if($this->input->post('SubjekID12') != "")
			{
				$wordB = explode(" - ",$this->input->post('SubjekID12'));
				$SubB = $wordB[0];
			}
			if($this->input->post('SubjekID2') != "")
			{
				$wordC = explode(" - ",$this->input->post('SubjekID2'));
				$SubC = $wordC[0];
			}
			$data = array(
						'JadwalRemedialID' => $JadwalRemedialID,
						'SubjekID' => $SubA,
						'SubjekID12' => $SubB,
						'SubjekID2' => $SubC,
						'UjianRuangID' => $this->input->post('RuangID'),
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'TahunID' => $this->session->userdata('remedial_filter_tahun1'),
						'TahunID2' => $this->session->userdata('remedial_filter_tahun2'),
						// 'JmlhMhsw' => $this->input->post('JmlhMhsw'),
						'DosenID' => $word[0],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_remedial->PTL_update($JadwalRemedialID,$data);
			echo warning("Schedule updated in temporary data until finish next steps.","../remedial/ptl_student_edit/$JadwalRemedialID");
		}
		
		function ptl_student_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED11",
							'aktifitas' => "Mengakses halaman Remedial - Student Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resremedial = $this->m_remedial->PTL_select($JadwalRemedialID);
			$data['JadwalRemedialID'] = $resremedial['JadwalRemedialID'];
			$data['RuangID'] = $resremedial['UjianRuangID'];
			$data['TahunID'] = $resremedial['TahunID'];
			$data['AddTahunID'] = $resremedial['TahunID'];
			$data['TahunID2'] = $resremedial['TahunID2'];
			$data['AddTahunID2'] = $resremedial['TahunID2'];
			$data['SubjekID'] = $resremedial['SubjekID'];
			$data['AddSubjekID'] = $resremedial['SubjekID'];
			$data['SubjekID12'] = $resremedial['SubjekID12'];
			$data['AddSubjekID12'] = $resremedial['SubjekID12'];
			$data['SubjekID2'] = $resremedial['SubjekID2'];
			$data['AddSubjekID2'] = $resremedial['SubjekID2'];
			$data['DosenID'] = $resremedial['DosenID'];
			
			$RuangID = $resremedial['UjianRuangID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Remedial/v_remedial_student_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_student_edit_set()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$JadwalRemedialID = $this->input->post('JadwalRemedialID');
			$SubjekID = $this->input->post('SubjekID');
			$kapasitas = $this->input->post('kapasitas');
			$urutan = $this->input->post('NomorKursi');
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $JadwalRemedialID,
							'pk3' => $SubjekID,
							'pk4' => $kapasitas,
							'pk5' => $urutan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED12",
							'aktifitas' => "Mengakses halaman Remedial - Student Edit Set.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if(($urutan > 0) AND ($urutan <= $kapasitas))
			{
				$NomorKursi = $this->input->post('NomorKursi');
				$rkursi = $this->m_remedial_krs->PTL_select_kursi($JadwalRemedialID,$NomorKursi);
				if($rkursi)
				{
					echo warning("Sorry, chair with number '$NomorKursi' has been used.","../remedial/ptl_student_edit/$JadwalRemedialID");
				}
				else
				{
					$rkrs = $this->m_krs->PTL_select_krs($MhswID,$SubjekID);
					if($rkrs)
					{
						$data = array(
									'NomorKursi' => $NomorKursi,
									'KRSID' => $rkrs['KRSID'],
									'KHSID' => $rkrs['KHSID'],
									'MhswID' => $MhswID,
									'TahunID' => $rkrs['TahunID'],
									'SubjekID' => $rkrs['SubjekID'],
									'JadwalRemedialID' => $JadwalRemedialID,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_remedial_krs->PTL_insert($data);
						redirect("remedial/ptl_student_edit/$JadwalRemedialID");
					}
					else
					{
						echo warning("KRS not found.","../remedial/ptl_student_edit/$JadwalRemedialID");
					}
				}
			}
			else
			{
				echo warning("Seat numbers are not listed.","../remedial/ptl_student_edit/$JadwalRemedialID");
			}
		}
		
		function ptl_student_edit_delete()
		{
			$this->authentification();
			$KRSRemedialID = $this->uri->segment(3);
			$JadwalRemedialID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $KRSRemedialID,
							'pk2' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED13",
							'aktifitas' => "Mengakses halaman Remedial - Student Edit Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial_krs->PTL_delete($KRSRemedialID);
			redirect("remedial/ptl_student_edit/$JadwalRemedialID");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$JadwalRemedialID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED14",
							'aktifitas' => "Mengakses halaman Remedial - Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_remedial->PTL_delete($JadwalRemedialID);
			$this->m_remedial_krs->PTL_delete_jadwal($JadwalRemedialID);
			echo warning("Your data successfully deleted.","../remedial");
		}
		
		function ptl_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$RuangID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED15",
							'aktifitas' => "Mengakses halaman Remedial - Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resremedial = $this->m_remedial->PTL_select($JadwalRemedialID);
			$data['JadwalRemedialID'] = $JadwalRemedialID;
			$data['TahunID'] = $resremedial['TahunID'];
			$data['TahunID2'] = $resremedial['TahunID2'];
			$data['SubjekID'] = $resremedial['SubjekID'];
			$data['SubjekID12'] = $resremedial['SubjekID12'];
			$data['SubjekID2'] = $resremedial['SubjekID2'];
			$data['DosenID'] = $resremedial['DosenID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Remedial/v_remedial_attendance_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_attendance_insert()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			$nosave = 0;
			$DataAktifitas = "";
			for($i=1;$i<=$total;$i++)
			{
				$KRSRemedialID = $this->input->post("KRSRemedialID$i");
				$MhswID = $this->input->post("MhswID$i");
				$res = $this->m_remedial_krs->PTL_select_krs($KRSRemedialID);
				if($res)
				{
					$data = array(
								'JenisPresensiID' => $this->input->post("JenisPresensiID$i"),
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_remedial_krs->PTL_update($KRSRemedialID,$data);
				}
				else
				{
					// $data = array(
								// 'JadwalRemedialID' => $this->input->post("JadwalRemedialID$i"),
								// 'MhswID' => $this->input->post("MhswID$i"),
								// 'JenisPresensiID' => $this->input->post("JenisPresensiID$i"),
								// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								// 'tanggal_buat' => $this->waktu
								// );
					// $this->m_remedial_krs->PTL_insert($data);
					$nosave++;
				}
				$DataAktifitas .= "$KRSRemedialID-$MhswID-".$this->input->post("JenisPresensiID$i").";";
			}
			$JadwalRemedialID = $this->input->post("remedial");
			$RuangID = $this->input->post("ruang");
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED16",
							'aktifitas' => "Mengakses halaman Remedial - Attendance Insert.",
							'data' => $DataAktifitas,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$pesan = "";
			// if($nosave > 0)
			// {
				// $pesan = " $nosave data not saved.";
			// }
			echo warning("Your data successfully saved.$pesan","../remedial/ptl_attendance/$JadwalRemedialID/$RuangID");
		}
		
		function ptl_scoring()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$JadwalRemedialID = $this->uri->segment(3);
			$RuangID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED17",
							'aktifitas' => "Mengakses halaman Remedial - Scoring.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resremedial = $this->m_remedial->PTL_select($JadwalRemedialID);
			$data['JadwalRemedialID'] = $JadwalRemedialID;
			$data['TahunID'] = $resremedial['TahunID'];
			$data['TahunID2'] = $resremedial['TahunID2'];
			$data['SubjekID'] = $resremedial['SubjekID'];
			$data['SubjekID12'] = $resremedial['SubjekID12'];
			$data['SubjekID2'] = $resremedial['SubjekID2'];
			$data['DosenID'] = $resremedial['DosenID'];
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['kolom'] = $result['KolomUjian'];
			$data['kapasitas'] = $result['KapasitasUjian'];
			$data['rowrecord'] = $this->m_krs->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Remedial/v_remedial_scoring_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_scoring_insert()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			$nosave = 0;
			$pesan = "";
			$DataAktifitas = "";
			for($i=1;$i<=$total;$i++)
			{
				$KRSRemedialID = $this->input->post("KRSRemedialID$i");
				$MhswID = $this->input->post("MhswID$i");
				$nama = $this->input->post("nama$i");
				$res = $this->m_remedial_krs->PTL_select_krs($KRSRemedialID);
				if($res)
				{
					if($res['JenisPresensiID'] == "Y")
					{
						$data = array(
									'Nilai' => $this->input->post("Nilai$i"),
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_remedial_krs->PTL_update($KRSRemedialID,$data);
					}
					else
					{
						$data = array(
									'Nilai' => 0,
									'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_edit' => $this->waktu
									);
						$this->m_remedial_krs->PTL_update($KRSRemedialID,$data);
						if($KRSRemedialID != "")
						{
							$pesan .= "$MhswID - $nama ($res[JenisPresensiID])".'\n';
						}
					}
				}
				else
				{
					// $data = array(
								// 'ExamID' => $this->input->post("ExamID$i"),
								// 'MhswID' => $this->input->post("MhswID$i"),
								// 'Nilai' => 0,
								// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								// 'tanggal_buat' => $this->waktu
								// );
					// $this->m_exam_mahasiswa->PTL_insert($data);
					// $pesan .= "$MhswID - $nama".'\n';
					$nosave++;
				}
				$DataAktifitas .= "$KRSRemedialID-$MhswID-".$this->input->post("Nilai$i").";";
			}
			$psn = "";
			if($pesan != "")
			{
				$psn = '\n\n'."Students who have not filled attendance.".'\n'.$pesan;
			}
			$JadwalRemedialID = $this->input->post("remedial");
			$RuangID = $this->input->post("ruang");
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $RuangID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED18",
							'aktifitas' => "Mengakses halaman Remedial - Scoring Insert.",
							'data' => $DataAktifitas,
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			echo warning("Your data successfully saved.$psn","../remedial/ptl_scoring/$JadwalRemedialID/$RuangID");
		}
		
		function ptl_pdf_list()
		{
			$TahunID = $this->session->userdata('remedial_filter_tahun1');
			$TahunID2 = $this->session->userdata('remedial_filter_tahun2');
			$RuangID = $this->session->userdata('remedial_filter_ruang');
			$cekdosen = $this->session->userdata('remedial_filter_dosen');
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $TahunID2,
							'pk3' => $RuangID,
							'pk4' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED19",
							'aktifitas' => "Mengakses halaman Remedial - PDF List.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"REMEDIAL SCHEDULE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(0.5, 1, "#", "LBTR", 0, "C");
			$this->fpdf->Cell(1, 1, "RID", "BTR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (1,2)", "BTR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (3)", "BTR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "BTR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "BTR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "TIME", "BTR", 0, "C");
			$this->fpdf->Cell(4, 1, "SUPERVISOR", "BTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",7);
			foreach($result as $r)
			{
				if($no == $tot)
				{
					$SubjekID = $r->SubjekID;
					$s1 = $this->m_subjek->PTL_select($SubjekID);
					$subjek1 = "";
					if($s1)
					{
						$subjek1 = $s1["Nama"];
					}
					$SubjekID = $r->SubjekID12;
					$s2 = $this->m_subjek->PTL_select($SubjekID);
					$subjek2 = "";
					if($s2)
					{
						$subjek2 = $s2["Nama"];
					}
					$SubjekID = $r->SubjekID2;
					$s3 = $this->m_subjek->PTL_select($SubjekID);
					$subjek3 = "";
					if($s3)
					{
						$subjek3 = $s3["Nama"];
					}
					$DosenID = $r->DosenID;
					$e = $this->m_dosen->PTL_select($DosenID);
					$dosen = "No Supervisor";
					if($e)
					{
						$dosen = $e["Nama"];
					}
					if($cekdosen == "")
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
						$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
						$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
						$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
						$no++;
						$tot++;
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
				else
				{
					$SubjekID = $r->SubjekID;
					$s1 = $this->m_subjek->PTL_select($SubjekID);
					$subjek1 = "";
					if($s1)
					{
						$subjek1 = $s1["Nama"];
					}
					$SubjekID = $r->SubjekID12;
					$s2 = $this->m_subjek->PTL_select($SubjekID);
					$subjek2 = "";
					if($s2)
					{
						$subjek2 = $s2["Nama"];
					}
					$SubjekID = $r->SubjekID2;
					$s3 = $this->m_subjek->PTL_select($SubjekID);
					$subjek3 = "";
					if($s3)
					{
						$subjek3 = $s3["Nama"];
					}
					$DosenID = $r->DosenID;
					$e = $this->m_dosen->PTL_select($DosenID);
					$dosen = "No Supervisor";
					if($e)
					{
						$dosen = $e["Nama"];
					}
					if($cekdosen == "")
					{
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
						$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
						$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
						$this->fpdf->Ln();
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
						$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
						$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
						$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
						$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
						$no++;
						$tot++;
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_class()
		{
			$JadwalRemedialID = $this->uri->segment(3);
			$ruangid = $this->uri->segment(4);
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$TahunID2 = $this->session->userdata('exam_filter_tahun2');
			$RuangID = $this->session->userdata('exam_filter_ruang');
			$cekdosen = $this->session->userdata('dra_filter_dosen');
			$datalog = array(
							'pk1' => $JadwalRemedialID,
							'pk2' => $TahunID,
							'pk3' => $TahunID2,
							'pk4' => $RuangID,
							'pk5' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED20",
							'aktifitas' => "Mengakses halaman Remedial - PDF Class.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_remedial->PTL_all_spesifik($TahunID,$TahunID2,$RuangID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"REMEDIAL SCHEDULE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(0.5, 1, "#", "LTR", 0, "C");
			$this->fpdf->Cell(1, 1, "RID", "TR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (1,2)", "TR", 0, "C");
			$this->fpdf->Cell(4.2, 1, "SUBJECT (3)", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "TR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "TIME", "TR", 0, "C");
			$this->fpdf->Cell(4, 1, "SUPERVISOR", "TR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",7);
			foreach($result as $r)
			{
				if($no == $tot)
				{
					$DosenID = $r->DosenID;
					if($r->JadwalRemedialID == $JadwalRemedialID)
					{
						$SubjekID = $r->SubjekID;
						$s1 = $this->m_subjek->PTL_select($SubjekID);
						$subjek1 = "";
						if($s1)
						{
							$subjek1 = $s1["Nama"];
						}
						$SubjekID = $r->SubjekID12;
						$s2 = $this->m_subjek->PTL_select($SubjekID);
						$subjek2 = "";
						if($s2)
						{
							$subjek2 = $s2["Nama"];
						}
						$SubjekID = $r->SubjekID2;
						$s3 = $this->m_subjek->PTL_select($SubjekID);
						$subjek3 = "";
						if($s3)
						{
							$subjek3 = $s3["Nama"];
						}
						$e = $this->m_dosen->PTL_select($DosenID);
						$dosen = "No Supervisor";
						if($e)
						{
							$dosen = $e["Nama"];
						}
						if($cekdosen == "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
					else
					{
						if($DosenID == $cekdosen)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
					}
				}
				else
				{
					if($r->JadwalRemedialID == $JadwalRemedialID)
					{
						$SubjekID = $r->SubjekID;
						$s1 = $this->m_subjek->PTL_select($SubjekID);
						$subjek1 = "";
						if($s1)
						{
							$subjek1 = $s1["Nama"];
						}
						$SubjekID = $r->SubjekID12;
						$s2 = $this->m_subjek->PTL_select($SubjekID);
						$subjek2 = "";
						if($s2)
						{
							$subjek2 = $s2["Nama"];
						}
						$SubjekID = $r->SubjekID2;
						$s3 = $this->m_subjek->PTL_select($SubjekID);
						$subjek3 = "";
						if($s3)
						{
							$subjek3 = $s3["Nama"];
						}
						$DosenID = $r->DosenID;
						$e = $this->m_dosen->PTL_select($DosenID);
						$dosen = "No Supervisor";
						if($e)
						{
							$dosen = $e["Nama"];
						}
						if($cekdosen == "")
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
							$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
							$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
							$this->fpdf->Ln();
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
							$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
							$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
							$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
							$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
							$no++;
							$tot++;
						}
						else
						{
							if($DosenID == $cekdosen)
							{
								$this->fpdf->Ln();
								$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
								$this->fpdf->Cell(1, 1, $r->JadwalRemedialID, "TR", 0, "C");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek1,0,25), "TR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek3,0,25), "TR", 0, "L");
								$this->fpdf->Cell(1.5, 0.5, $r->UjianRuangID, "TR", 0, "C");
								$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglMulai), "TR", 0, "C");
								$this->fpdf->Cell(1.5, 0.5, $r->JamMulai, "TR", 0, "C");
								$this->fpdf->Cell(4, 0.5, $dosen, "TR", 0, "L");
								$this->fpdf->Ln();
								$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, substr($subjek2,0,25), "BR", 0, "L");
								$this->fpdf->Cell(4.2, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(1.5, 0.5, "", "BR", 0, "L");
								$this->fpdf->Cell(2, 0.5, tgl_indo($r->TglSelesai), "BR", 0, "C");
								$this->fpdf->Cell(1.5, 0.5, $r->JamSelesai, "BR", 0, "C");
								$this->fpdf->Cell(4, 0.5, "", "BR", 0, "C");
								$no++;
								$tot++;
							}
						}
					}
				}
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mahasiswa->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->MhswID,
											'value' => $row->MhswID." - ".$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('remedial/index',$data);
			}
		}
		
		function ptl_cari()
		{
			$this->authentification();
			$cari = $this->input->post('cari');
			$datalog = array(
							'pk1' => $cari,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED21-01",
							'aktifitas' => "Mengakses halaman Remedial - Search: $cari.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cari != "")
			{
				$this->session->set_userdata('remedial_filter_mahasiswa',$cari);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_mahasiswa');
			}
			redirect("remedial/ptl_card");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED21-02",
							'aktifitas' => "Mengakses halaman Remedial - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('remedial_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_jur');
			}
			redirect("remedial/ptl_card");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED21-03",
							'aktifitas' => "Mengakses halaman Remedial - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('remedial_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('remedial_filter_tahun');
			}
			redirect("remedial/ptl_card");
		}
		
		function ptl_card()
		{
			$this->authentification();
			$this->session->set_userdata('menu','exam');
			$data['id'] = "mahasiswa";
			$data['controller'] = "drop/lookup";
			$word = explode(" - ",$this->session->userdata('remedial_filter_mahasiswa'));
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$cekjurusan = $this->session->userdata('remedial_filter_jur');
			$TahunID = $this->session->userdata('remedial_filter_tahun');
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $cekjurusan,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED22",
							'aktifitas' => "Mengakses halaman Remedial - Card.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['rowrecord'] = $this->m_remedial_krs->PTL_all_spesifik($MhswID,$TahunID);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Remedial/v_remedial_card',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf_remedial_pass()
		{
			$word = explode(" - ",$this->session->userdata('remedial_filter_mahasiswa'));
			$MhswID = $word[0];
			$TahunID = $this->session->userdata('remedial_filter_tahun');
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "REMED23",
							'aktifitas' => "Mengakses halaman Remedial - PDF Remedial Pass: $MhswID - $TahunID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			// $result = $this->m_krs->PTL_all_select_exam_card($MhswID,$TahunID);
			$result = $this->m_remedial_krs->PTL_all_spesifik($MhswID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"REMEDIAL PASS",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$reskhs = $this->m_khs->PTL_all_select($MhswID);
			$reskhs2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$kelas1 = "";
			$kelas2 = "";
			$kelas3 = "";
			$ProgramID = $resmhsw["ProgramID"];
			$resprog = $this->m_program->PTL_select($ProgramID);
			$ProdiID = $resmhsw["ProdiID"];
			$resprod = $this->m_prodi->PTL_select($ProdiID);
			if($reskhs)
			{
				foreach($reskhs as $rk)
				{
					$KelasID = $rk->KelasID;
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					if($rk->TahunKe == 1)
					{
						if($ProgramID == "INT")
						{
							if($reskelas)
							{
								$kelas1 = "O".$reskelas["Nama"];
							}
							else
							{
								$kelas1 = "O"."(not set)";
							}
						}
						else
						{
							if($reskelas)
							{
								$kelas1 = $rk->TahunKe.$reskelas["Nama"];
							}
							else
							{
								$kelas1 = $rk->TahunKe."(not set)";
							}
						}
					}
					if($rk->TahunKe == 2)
					{
						if($reskelas)
						{
							$kelas2 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
						else
						{
							$kelas2 = " > ".$rk->TahunKe."(not set)";
						}
					}
					if($rk->TahunKe == 3)
					{
						if($reskelas)
						{
							$kelas3 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
						else
						{
							$kelas3 = " > ".$rk->TahunKe."(not set)";
						}
					}
				}
			}
			if($resmhsw["Foto"] == "")
			{
				$foto = "foto_umum/user.jpg";
			}
			else
			{
				$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				$exist = file_exists_remote(base_url("ptl_storage/$foto"));
				if($exist)
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
				}
				else
				{
					$foto = "foto_umum/user.jpg";
				}
			}
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Cell(3, 0.5, "SIN", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $MhswID", "", 0, "L");
			$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX()+9,$this->fpdf->getY(),3,4);
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "NAME", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $resmhsw[Nama]", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "CLASS", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $kelas1$kelas2$kelas3", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "PROGRAM", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": ".strtoupper($resprog["Nama"]), "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "MAJOR", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": $resmhsw[ProdiID] - ".strtoupper($resprod["Nama"]), "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3, 0.5, "SEMESTER / YEAR", "", 0, "L");
			$this->fpdf->Cell(4, 0.5, ": Semester $reskhs2[Sesi] / $restahun[Nama]", "", 0, "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(1.5, 1, "CODE", "LTR", 0, "C");
			$this->fpdf->Cell(8, 1, "SUBJECT", "TR", 0, "C");
			$this->fpdf->Cell(1.5, 1, "ROOM", "TR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "TR", 0, "C");
			$this->fpdf->Cell(3, 1, "TIME", "TR", 0, "C");
			$this->fpdf->Cell(3, 1, "TEACHER SIGN", "TR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",9);
			foreach($result as $r)
			{
				$this->fpdf->Ln();
				$this->fpdf->Cell(1.5, 1, $r->SubjekKode, "LBTR", 0, "C");
				$this->fpdf->Cell(8, 1, $r->NamaSubjek, "BTR", 0, "L");
				$this->fpdf->Cell(1.5, 1, $r->UjianRuangID, "BTR", 0, "C");
				$this->fpdf->Cell(2, 1, tgl_singkat_eng($r->TglMulai), "BTR", 0, "C");
				$this->fpdf->Cell(3, 1, "$r->JamMulai - $r->JamSelesai", "BTR", 0, "C");
				$this->fpdf->Cell(3, 1, "", "BTR", 0, "L");
			}
			$this->fpdf->Ln(2);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14, 0.7, "BRING THIS REMEDIAL PASS DURING THE EXAMINATION.", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.5, "IF THE REMEDIAL PASS IS LOST, PLEASE APPROACH STUDENT AFFAIR OFFICE FOR", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(14 , 0.5, "REPLACEMENT OF REMEDIAL PASS ON PAYMENT OF Rp 200.000,-", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->Cell(14 , 0.5, "IF FOUND THIS CARD, PLEASE RETURN TO THE BELOW ADDRESS:", "", 0, "L");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "Jalan Asem Dua No. 3 - 5, Jakarta Selatan 12410", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "Tel. (021) 7659181 Fax.(021) 7657517", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(14 , 0.2, "email: info@esmodjakarta.com", "", 0, "L");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Exam_List_$id_akun-$nama.pdf","I");
		}
	}
?>