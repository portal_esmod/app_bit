<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Room_available extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tahun = gmdate("Y", time()-($ms));
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi');
			$this->load->model('m_ruang');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_tanggal()
		{
			$this->authentification();
			$cektanggal = $this->input->post('cektanggal');
			if($cektanggal != "")
			{
				$this->session->set_userdata('room_avail_filter_tanggal',$cektanggal);
			}
			else
			{
				$this->session->unset_userdata('room_avail_filter_tanggal');
			}
			redirect("room_available");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-d", time()-($ms));
			$cektanggal = $this->session->userdata('room_avail_filter_tanggal');
			if($cektanggal == "")
			{
				$data['today'] = $tanggal;
				$today = $tanggal;
			}
			else
			{
				$data['today'] = $cektanggal;
				$today = $cektanggal;
			}
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Room_Available/v_room_available',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>