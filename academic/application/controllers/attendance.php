<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Attendance extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_catatan2');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_jadwal_hari');
			$this->load->model('m_jenis_mk');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_krs2');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_notifikasi');
			$this->load->model('m_notifikasi_lms');
			$this->load->model('m_mk');
			$this->load->model('m_nilai3');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_ruang');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		// Menghapus data sampah jadwal pertemuan mahasiswa
		// function listing()
		// {
			// $rowrecord = $this->m_presensi->PTL_all_listing();
			// if($rowrecord)
			// {
				// $no = 1;
				// foreach($rowrecord as $row)
				// {
					// echo $no.' - '.$row->PresensiID.'<br/>';
					// $PresensiID = $row->PresensiID;
					// $rowrecord2 = $this->m_presensi->PTL_listing_delete($PresensiID);
					// $rowrecord2 = $this->m_presensi->PTL_all_listing2($PresensiID);
					// if($rowrecord2)
					// {
						// foreach($rowrecord2 as $row2)
						// {
							// echo '-------------------'.$row2->PresensiMhswID.'<br/>';
							// $rowrecord3 = $this->m_presensi->PTL_all_listing3($PresensiID);
							// if($rowrecord3)
							// {
								// foreach($rowrecord3 as $row3)
								// {
									// if(($row3->Nilai == '0.00') AND ($row3->AdditionalNilai == '0.00'))
									// {
										// echo '==============================='.$row3->KRS2ID.' - - - - RED<br/>';
									// }
									// else
									// {
										// echo '==============================='.$row3->KRS2ID.' - - - - GREEN<br/>';
									// }
								// }
								// $this->m_presensi->PTL_listing_delete3($PresensiID);
							// }
						// }
						// $this->m_presensi->PTL_listing_delete2($PresensiID);
					// }
					// $no++;
				// }
			// }
		// }
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-01",
							'aktifitas' => "Filter halaman Attendance - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('att_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('att_filter_jur');
			}
			redirect("attendance");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-02",
							'aktifitas' => "Filter halaman Attendance - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('att_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('att_filter_tahun');
			}
			redirect("attendance");
		}
		
		function ptl_filter_subjek()
		{
			$this->authentification();
			$ceksubjek = $this->input->post('ceksubjek');
			$datalog = array(
							'pk1' => $ceksubjek,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-03",
							'aktifitas' => "Filter halaman Attendance - Subject: $ceksubjek.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksubjek != "")
			{
				$this->session->set_userdata('att_filter_subjek',$ceksubjek);
			}
			else
			{
				$this->session->unset_userdata('att_filter_subjek');
			}
			redirect("attendance");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			$datalog = array(
							'pk1' => $cekdosen,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-04",
							'aktifitas' => "Filter halaman Attendance - Lecturer: $cekdosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekdosen != "")
			{
				$this->session->set_userdata('att_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('att_filter_dosen');
			}
			redirect("attendance");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			$datalog = array(
							'pk1' => $cekkelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-05",
							'aktifitas' => "Filter halaman Attendance - Class: $cekkelas.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkelas != "")
			{
				$this->session->set_userdata('att_filter_kelas',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('att_filter_kelas');
			}
			redirect("attendance");
		}
		
		function ptl_set_presensi()
		{
			$this->authentification();
			$word = explode("_",$this->input->post('cekpresensi'));
			$cekpresensi = $word[0];
			$PresensiID = $word[1];
			$TahunID = $word[2];
			$MKID = @$word[3];
			$datalog = array(
							'pk1' => $cekpresensi,
							'pk2' => $PresensiID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN01-06",
							'aktifitas' => "Filter halaman Attendance - Set Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekpresensi != "")
			{
				$this->session->set_userdata('att_set_presensi',$cekpresensi);
			}
			else
			{
				$this->session->unset_userdata('att_set_presensi');
			}
			redirect("attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN02",
							'aktifitas' => "Mengakses halaman Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->session->unset_userdata('aksi');
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cekkelas = $this->session->userdata('att_filter_kelas');
			$ck = "_";
			if($cekkelas != "")
			{
				$ck = $cekkelas;
			}
			$word = explode("_",$ck);
			$Tahun = $word[0];
			$Kelas = $word[1];
			$data['Tahun'] = $word[0];
			$data['Kelas'] = $word[1];
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ceksubjek = $this->session->userdata('att_filter_subjek');
			$data['rowsubjek'] = $this->m_jadwal->PTL_subjek($cekjurusan,$cektahun,$Tahun,$Kelas);
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowrecord'] = $this->m_jadwal->PTL_all_spesifik_scoring($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas);
			$data['rowrecord2'] = $this->m_jadwal->PTL_all_spesifik_scoring2($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas);
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				if(stristr($_COOKIE["akses"],"COORDINATOR"))
				{
					$this->load->view('Attendance/v_attendance',$data);
				}
				else
				{
					$this->load->view('Attendance/v_attendance_lecturer',$data);
				}
			}
			else
			{
				$this->load->view('Attendance/v_attendance',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN03",
							'aktifitas' => "Mengakses halaman Attendance - Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_jadwal->PTL_delete($JadwalID);
			$this->m_jadwal_hari->PTL_delete_jadwal($JadwalID);
			$this->m_presensi->PTL_delete_jadwal($JadwalID);
			$this->m_presensi_mahasiswa->PTL_delete_jadwal($JadwalID);
			echo warning("Your data has been deleted permanently.","../attendance");
		}
		
		function ptl_list()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$this->session->unset_userdata('aksi');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN04",
							'aktifitas' => "Mengakses halaman Attendance - List.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['TahunID'] = $result['TahunID'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['MaxAbsen'] = $result['MaxAbsen'];
			$data['file_project'] = $result['file_project'];
			$data['file_evaluation'] = $result['file_evaluation'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['rowrecord'] = $this->m_presensi->PTL_all_select($JadwalID);
			$data['rowcatatan'] = $this->m_catatan2->PTL_all_jadwal($JadwalID);
			$cekgroup = "5";
			$data['rownilai'] = $this->m_nilai3->PTL_all_spesifik($cekgroup);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_list',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_list_trash()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$this->session->unset_userdata('aksi');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN05",
							'aktifitas' => "Mengakses halaman Attendance - List Trash.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['TahunID'] = $result['TahunID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['rowrecord'] = $this->m_presensi->PTL_all_select_trash($JadwalID);
			$data['rowcatatan'] = $this->m_catatan2->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_list_trash',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_list_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$this->session->unset_userdata('aksi');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN06",
							'aktifitas' => "Mengakses halaman Attendance - List Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['TahunID'] = $result['TahunID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['DosenID2'] = $result['DosenID2'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			if($result['DosenID'] == "")
			{
				$data['NamaDosen'] = "";
			}
			else
			{
				$DosenID = $result['DosenID'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen'] = "";
				if($resdosen2)
				{
					$data['NamaDosen'] = $result['DosenID']." - ".$resdosen2['Nama'];
				}
			}
			if($result['DosenID2'] == "")
			{
				$data['NamaDosen2'] = "";
			}
			else
			{
				$DosenID = $result['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen2'] = "";
				if($resdosen2)
				{
					$data['NamaDosen2'] = $result['DosenID2']." - ".$resdosen2['Nama'];
				}
			}
			if($result['DosenID3'] == "")
			{
				$data['NamaDosen3'] = "";
			}
			else
			{
				$DosenID = $result['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen3'] = "";
				if($resdosen3)
				{
					$data['NamaDosen3'] = $result['DosenID3']." - ".$resdosen3['Nama'];
				}
			}
			if($result['DosenID4'] == "")
			{
				$data['NamaDosen4'] = "";
			}
			else
			{
				$DosenID = $result['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen4'] = "";
				if($resdosen4)
				{
					$data['NamaDosen4'] = $result['DosenID4']." - ".$resdosen4['Nama'];
				}
			}
			if($result['DosenID5'] == "")
			{
				$data['NamaDosen5'] = "";
			}
			else
			{
				$DosenID = $result['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen5'] = "";
				if($resdosen5)
				{
					$data['NamaDosen5'] = $result['DosenID5']." - ".$resdosen5['Nama'];
				}
			}
			if($result['DosenID6'] == "")
			{
				$data['NamaDosen6'] = "";
			}
			else
			{
				$DosenID = $result['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen6'] = "";
				if($resdosen6)
				{
					$data['NamaDosen6'] = $result['DosenID6']." - ".$resdosen6['Nama'];
				}
			}
			$res = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = $res['ProgramID'];
			$data['TahunID'] = $res['TahunID'];
			$data['TglMulai'] = $res['TglMulai'];
			$data['TglSelesai'] = $res['TglSelesai'];
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$SubjekID = $result['SubjekID'];
			$data['rowmk'] = $this->m_mk->PTL_all_select($SubjekID);
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_form',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_list_insert()
		{
			$this->authentification();
			$this->session->unset_userdata('aksi');
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN07",
							'aktifitas' => "Mengakses halaman Attendance - List Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$TahunID = '';
			$SubjekID = '';
			if($resjadwal)
			{
				$TahunID = $resjadwal['TahunID'];
				$SubjekID = $resjadwal['SubjekID'];
			}
			$res = $this->m_presensi->PTL_urut($JadwalID);
			$word = explode(" - ",$this->input->post('DosenID'));
			$DosenID = $word[0];
			$data = array(
						'TahunID' => $TahunID,
						'JadwalID' => $JadwalID,
						'Pertemuan' => $res['LAST'] + 1,
						'DosenID' => $DosenID,
						'Tanggal' => $this->input->post('Tanggal'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'RuangID' => $this->input->post('RuangID'),
						'MKID' => $this->input->post('MKID'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => 'N',
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_presensi->PTL_insert($data);
			
			$resdos = $this->m_dosen->PTL_select($DosenID);
			$NmDsn = $DosenID;
			if($resdos)
			{
				$NmDsn = $resdos['Nama'];
			}
			$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
			$nama = "($NmDsn - Not Registered in HRIS)";
			$email = '';
			$email2 = '';
			if($resdosen)
			{
				$nama = $resdosen['nama'];
				$email = $resdosen['email'];
				$email2 = $resdosen['email2'];
			}
			$ressub = $this->m_subjek->PTL_select($SubjekID);
			$NamaSub = '';
			if($ressub)
			{
				$NamaSub = $ressub['Nama'];
			}
			$Tanggal = tgl_singkat_eng($this->input->post('Tanggal'));
			$JamMulai = $this->input->post('JamMulai');
			$JamSelesai = $this->input->post('JamSelesai');
			$RuangID = $this->input->post('RuangID');
			$MKID = $this->input->post('MKID');
			$resmk = $this->m_mk->PTL_select($MKID);
			$NamaProject = '-';
			if($resmk)
			{
				$NamaProject = $resmk['Nama'];
			}
			
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$NamaSubjek = "";
			if($ressubjek)
			{
				$NamaSubjek = $ressubjek["Nama"];
			}
			$rowkrs = $this->m_krs->PTL_all_jadwal($JadwalID);
			if($rowkrs)
			{
				foreach($rowkrs as $rkrs)
				{
					$MhswID = $rkrs->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$NamaMhsw = '';
					if($resmhsw)
					{
						$NamaMhsw = $resmhsw['Nama'];
					}
					$data = array(
								'MhswID' => $rkrs->MhswID,
								'JadwalID' => $JadwalID,
								'KRSID' => $rkrs->KRSID,
								'Nama' => $NamaMhsw,
								'Judul' => 'The schedule has been added',
								'Pesan' => "Schedule for $NamaSubjek has been added. Date $Tanggal, time $JamMulai to $JamSelesai, room $RuangID.",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_notifikasi_lms->PTL_insert($data);
				}
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$email");
			$this->email->cc("$email2,anisa@esmodjakarta.com");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('NEW ADDITIONAL SCHEDULE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px' style='max-width:300px; width:100%;'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi Mr./Mrs. ".strtoupper($nama)."!
				<br/>
				<br/>
				Academic staff has scheduled you for a subject <font color='green'><b>$NamaSub</b></font>, Academic Code $TahunID.
				<br/>
				<br/>
				<table border='1'>
					<tr>
						<th>Date</th>
						<th>Time</th>
						<th>Room</th>
						<th>Project</th>
					</tr>
					<tr>
						<td>$Tanggal</td>
						<td>$JamMulai - $JamSelesai</td>
						<td>$RuangID</td>
						<td><font color='red'><b>$NamaProject</b></font></td>
					</tr>
				</table>
				<br/>
				If your teaching schedule is today, please immediately update student attendance.
				<br/>
				<br/>
				<table>
					<tr>
						<td><b>Short Link To Schedule</b></td>
						<td>:</td>
						<td>http://app.esmodjakarta.com/academic/attendance/ptl_list/$JadwalID</td>
					</tr>
				</table>
				<br/>
				You need to login first!
				<br/>
				<br/>
				Academic staff who set your attendance: <font color='blue'><b>".strtoupper($_COOKIE["nama"])."</b></font>.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px' style='max-width:120px; width:100%;'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data has been saved.","../attendance/ptl_list/$JadwalID");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data has been saved.","../attendance/ptl_list/$JadwalID");
				}
				else
				{
					echo warning("Email server is not active. Your data has been saved.","../attendance/ptl_list/$JadwalID");
				}
			}
		}
		
		function ptl_list_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$PresensiID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $PresensiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN08",
							'aktifitas' => "Mengakses halaman Attendance - List Edit.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_presensi->PTL_select($PresensiID);
			$data['PresensiID'] = $result['PresensiID'];
			$data['JadwalID'] = $result['JadwalID'];
			$data['DosenIDku'] = $result['DosenID'];
			$data['Tanggal'] = $result['Tanggal'];
			$data['JamMulai'] = $result['JamMulai'];
			$data['JamSelesai'] = $result['JamSelesai'];
			$data['RuangID'] = $result['RuangID'];
			$data['MKID'] = $result['MKID'];
			$data['Catatan'] = $result['Catatan'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$JadwalID = $result['JadwalID'];
			$res = $this->m_jadwal->PTL_select($JadwalID);
			if($res['DosenID'] == "")
			{
				$data['DosenID'] = "";
				$data['NamaDosen'] = "";
			}
			else
			{
				$data['DosenID'] = $res['DosenID'];
				$DosenID = $res['DosenID'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				$data['NamaDosen'] = $res['DosenID']." - ".$resdosen2['Nama'];
			}
			if($res['DosenID2'] == "")
			{
				$data['DosenID2'] = "";
				$data['NamaDosen2'] = "";
			}
			else
			{
				$data['DosenID2'] = $res['DosenID2'];
				$DosenID = $res['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen2)
				{
					$data['NamaDosen2'] = $res['DosenID2']." - ".$resdosen2['Nama'];
				}
				else
				{
					$data['NamaDosen2'] = "";
				}
			}
			if($res['DosenID3'] == "")
			{
				$data['DosenID3'] = "";
				$data['NamaDosen3'] = "";
			}
			else
			{
				$data['DosenID3'] = $res['DosenID3'];
				$DosenID = $res['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen3)
				{
					$data['NamaDosen3'] = $res['DosenID3']." - ".$resdosen3['Nama'];
				}
				else
				{
					$data['NamaDosen3'] = "";
				}
			}
			if($res['DosenID4'] == "")
			{
				$data['DosenID4'] = "";
				$data['NamaDosen4'] = "";
			}
			else
			{
				$data['DosenID4'] = $res['DosenID4'];
				$DosenID = $res['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen4)
				{
					$data['NamaDosen4'] = $res['DosenID4']." - ".$resdosen4['Nama'];
				}
				else
				{
					$data['NamaDosen4'] = "";
				}
			}
			if($res['DosenID5'] == "")
			{
				$data['DosenID5'] = "";
				$data['NamaDosen5'] = "";
			}
			else
			{
				$data['DosenID5'] = $res['DosenID5'];
				$DosenID = $res['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen5)
				{
					$data['NamaDosen5'] = $res['DosenID5']." - ".$resdosen5['Nama'];
				}
				else
				{
					$data['NamaDosen5'] = "";
				}
			}
			if($res['DosenID6'] == "")
			{
				$data['DosenID6'] = "";
				$data['NamaDosen6'] = "";
			}
			else
			{
				$data['DosenID6'] = $res['DosenID6'];
				$DosenID = $res['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen6)
				{
					$data['NamaDosen6'] = $res['DosenID6']." - ".$resdosen6['Nama'];
				}
				else
				{
					$data['NamaDosen6'] = "";
				}
			}
			$res = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = $res['ProgramID'];
			$data['TahunID'] = $res['TahunID'];
			$data['TglMulai'] = $res['TglMulai'];
			$data['TglSelesai'] = $res['TglSelesai'];
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$SubjekID = $res['SubjekID'];
			$data['rowmk'] = $this->m_mk->PTL_all_select($SubjekID);
			$this->session->set_userdata('aksi','edit');
			$this->load->view('Portal/v_header');
			if(!stristr($_COOKIE["akses"],"LECTURER"))
			{
				$this->load->view('Attendance/v_attendance_list_edit',$data);
			}
			else
			{
				$this->load->view('Attendance/v_attendance_list_edit_lecturer',$data);
			}
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_change_range_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN08-01",
							'aktifitas' => "Mengakses halaman Add Attendance  - Edit Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['JadwalID'] = $JadwalID;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = "";
			$data['TahunID'] = "";
			$data['TglMulai'] = "";
			$data['TglSelesai'] = "";
			$data['login_buat'] = "";
			$data['tanggal_buat'] = "";
			$data['login_edit'] = "";
			$data['tanggal_edit'] = "";
			if($resjadwal)
			{
				$data['ProgramID'] = $resjadwal['ProgramID'];
				$data['TahunID'] = $resjadwal['TahunID'];
				$data['TglMulai'] = $resjadwal['TglMulai'];
				$data['TglSelesai'] = $resjadwal['TglSelesai'];
				$data['login_buat'] = $resjadwal['login_buat'];
				$data['tanggal_buat'] = $resjadwal['tanggal_buat'];
				$data['login_edit'] = $resjadwal['login_edit'];
				$data['tanggal_edit'] = $resjadwal['tanggal_edit'];
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_form_range',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_change_range_form_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN08-02",
							'aktifitas' => "Mengakses halaman Add Attendance - Update Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			echo warning("Range has been updated.","../attendance/ptl_list_form/$JadwalID");
		}
		
		function ptl_change_range()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$PresensiID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $PresensiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN08-03",
							'aktifitas' => "Mengakses halaman Edit Attendance - Edit Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['JadwalID'] = $JadwalID;
			$data['PresensiID'] = $PresensiID;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = "";
			$data['TahunID'] = "";
			$data['TglMulai'] = "";
			$data['TglSelesai'] = "";
			$data['login_buat'] = "";
			$data['tanggal_buat'] = "";
			$data['login_edit'] = "";
			$data['tanggal_edit'] = "";
			if($resjadwal)
			{
				$data['ProgramID'] = $resjadwal['ProgramID'];
				$data['TahunID'] = $resjadwal['TahunID'];
				$data['TglMulai'] = $resjadwal['TglMulai'];
				$data['TglSelesai'] = $resjadwal['TglSelesai'];
				$data['login_buat'] = $resjadwal['login_buat'];
				$data['tanggal_buat'] = $resjadwal['tanggal_buat'];
				$data['login_edit'] = $resjadwal['login_edit'];
				$data['tanggal_edit'] = $resjadwal['tanggal_edit'];
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_edit_range',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_change_range_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$PresensiID = $this->input->post('PresensiID');
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $PresensiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN08-04",
							'aktifitas' => "Mengakses halaman Edit Attendance - Update Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'TglMulai' => $this->input->post('TglMulai'),
						'TglSelesai' => $this->input->post('TglSelesai'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			echo warning("Range has been updated.","../attendance/ptl_list_edit/$PresensiID");
		}
		
		function ptl_list_update()
		{
			$this->authentification();
			$this->session->unset_userdata('aksi');
			$PresensiID = $this->input->post('PresensiID');
			$JadwalID = $this->input->post('JadwalID');
			$res = $this->m_presensi->PTL_urut($JadwalID);
			$DosenID = $this->input->post('DosenID');
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $JadwalID,
							'pk3' => $DosenID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN09",
							'aktifitas' => "Mengakses halaman Attendance - List Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'DosenID' => $DosenID,
						'Tanggal' => $this->input->post('Tanggal'),
						'JamMulai' => $this->input->post('JamMulai'),
						'JamSelesai' => $this->input->post('JamSelesai'),
						'RuangID' => $this->input->post('RuangID'),
						'MKID' => $this->input->post('MKID'),
						'Catatan' => $this->input->post('Catatan'),
						'NA' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi->PTL_update($PresensiID,$data);
			
			$resdos = $this->m_dosen->PTL_select($DosenID);
			$NmDsn = $DosenID;
			if($resdos)
			{
				$NmDsn = $resdos['Nama'];
			}
			$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
			$nama = "($NmDsn - Not Registered in HRIS)";
			$email = '';
			$email2 = '';
			if($resdosen)
			{
				$nama = $resdosen['nama'];
				$email = $resdosen['email'];
				$email2 = $resdosen['email2'];
			}
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$TahunID = '';
			$SubjekID = '';
			if($resjadwal)
			{
				$TahunID = $resjadwal['TahunID'];
				$SubjekID = $resjadwal['SubjekID'];
			}
			$ressub = $this->m_subjek->PTL_select($SubjekID);
			$NamaSub = '';
			if($ressub)
			{
				$NamaSub = $ressub['Nama'];
			}
			$respresensi = $this->m_presensi->PTL_select($PresensiID);
			$Tanggal = '';
			$JamMulai = '';
			$JamSelesai = '';
			$RuangID = '';
			$MKID = '';
			if($respresensi)
			{
				$Tanggal = tgl_singkat_eng($respresensi['Tanggal']);
				$JamMulai = substr($respresensi['JamMulai'],0,5);
				$JamSelesai = substr($respresensi['JamSelesai'],0,5);
				$RuangID = $respresensi['RuangID'];
				$MKID = $respresensi['MKID'];
			}
			$resmk = $this->m_mk->PTL_select($MKID);
			$NamaProject = '-';
			if($resmk)
			{
				$NamaProject = $resmk['Nama'];
			}
			
			$Tanggal = tgl_singkat_eng($this->input->post('Tanggal'));
			$JamMulai = $this->input->post('JamMulai');
			$JamSelesai = $this->input->post('JamSelesai');
			$RuangID = $this->input->post('RuangID');
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$NamaSubjek = "";
			if($ressubjek)
			{
				$NamaSubjek = $ressubjek["Nama"];
			}
			$rowkrs = $this->m_krs->PTL_all_jadwal($JadwalID);
			if($rowkrs)
			{
				foreach($rowkrs as $rkrs)
				{
					$MhswID = $rkrs->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$NamaMhsw = '';
					if($resmhsw)
					{
						$NamaMhsw = $resmhsw['Nama'];
					}
					$data = array(
								'MhswID' => $rkrs->MhswID,
								'JadwalID' => $JadwalID,
								'KRSID' => $rkrs->KRSID,
								'Nama' => $NamaMhsw,
								'Judul' => 'The schedule has been changed',
								'Pesan' => "Schedule for $NamaSubjek has been changed. Date $Tanggal, time $JamMulai to $JamSelesai, room $RuangID.",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_notifikasi_lms->PTL_insert($data);
				}
			}
			
			if((!stristr($_COOKIE["akses"],"LECTURER")) AND (!stristr($_COOKIE["divisi"],"PRESIDENT")))
			{
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$email");
				$this->email->cc("$email2");
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('LECTURER HAS BEEN SET (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px' style='max-width:300px; width:100%;'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					Hi Mr./Mrs. ".strtoupper($nama)."!
					<br/>
					<br/>
					Academic staff has scheduled you for a subject <font color='green'><b>$NamaSub</b></font>, Academic Code $TahunID.
					<br/>
					<br/>
					<table border='1'>
						<tr>
							<th>Date</th>
							<th>Time</th>
							<th>Room</th>
							<th>Project</th>
						</tr>
						<tr>
							<td>$Tanggal</td>
							<td>$JamMulai - $JamSelesai</td>
							<td>$RuangID</td>
							<td><font color='red'><b>$NamaProject</b></font></td>
						</tr>
					</table>
					<br/>
					If your teaching schedule is today, please immediately update student attendance.
					<br/>
					<br/>
					<table>
						<tr>
							<td><b>Short Link To Schedule</b></td>
							<td>:</td>
							<td>http://app.esmodjakarta.com/academic/attendance/ptl_list/$JadwalID</td>
						</tr>
						<tr>
							<td><b>Short Link To Attendance</b></td>
							<td>:</td>
							<td>http://app.esmodjakarta.com/academic/attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID</td>
						</tr>
					</table>
					<br/>
					You need to login first!
					<br/>
					<br/>
					Academic staff who set your attendance: <font color='blue'><b>".strtoupper($_COOKIE["nama"])."</b></font>.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px' style='max-width:120px; width:100%;'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Your data has been updated.","../attendance/ptl_list/$JadwalID");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Your data has been updated.","../attendance/ptl_list/$JadwalID");
					}
					else
					{
						echo warning("Email server is not active. Your data has been updated.","../attendance/ptl_list/$JadwalID");
					}
				}
			}
			else
			{
				echo warning("Your data has been updated.","../attendance/ptl_list/$JadwalID");
			}
		}
		
		function ptl_list_delete()
		{
			$this->authentification();
			$this->session->unset_userdata('aksi');
			$PresensiID = $this->uri->segment(3);
			$JadwalID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN10",
							'aktifitas' => "Mengakses halaman Attendance - List Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'NA' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi->PTL_update($PresensiID,$data);
			echo warning("Your data has been deleted.","../attendance/ptl_list/$JadwalID");
		}
		
		function ptl_list_undelete()
		{
			$this->authentification();
			$this->session->unset_userdata('aksi');
			$PresensiID = $this->uri->segment(3);
			$JadwalID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN11",
							'aktifitas' => "Mengakses halaman Attendance - List Undelete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'NA' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi->PTL_update($PresensiID,$data);
			echo warning("Your data has been activated.","../attendance/ptl_list/$JadwalID");
		}
		
		function ptl_set_tanggal()
		{
			$this->authentification();
			$cektanggal = $this->input->post('cektanggal');
			$cekwaktu = $this->input->post('cekwaktu');
			$datalog = array(
							'pk1' => $cektanggal,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN28",
							'aktifitas' => "Update status kehadiran dosen.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$PresensiID = $this->input->post('PresensiID');
			$TahunID = $this->input->post('TahunID');
			$MKID = $this->input->post('MKID');
			$data = array(
						'dosen_tanggal_set' => "$cektanggal $cekwaktu"
						);
			$this->m_presensi->PTL_update($PresensiID,$data);
			redirect("attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
		}
		
		function ptl_list_attendance()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$PresensiID = $this->uri->segment(3);
			$MKID = $this->uri->segment(5);
			$data['warning'] = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $MKID,
							'pk3' => $this->uri->segment(6),
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN12",
							'aktifitas' => "Mengakses halaman Attendance - List Attendance.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['MKID'] = $MKID;
			$resmk = $this->m_mk->PTL_select($MKID);
			$data['NamaProject'] = '';
			if($resmk)
			{
				$data['NamaProject'] = $resmk['Nama'];
			}
			$result = $this->m_presensi->PTL_select($PresensiID);
			$data['PresensiID'] = $result['PresensiID'];
			$data['JadwalID'] = $result['JadwalID'];
			$data['Tanggal'] = $result['Tanggal'];
			$data['JamMulai'] = $result['JamMulai'];
			$data['JamSelesai'] = $result['JamSelesai'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['login_set'] = $result['login_set'];
			$data['tanggal_set'] = $result['tanggal_set'];
			$data['dosen_login_set'] = $result['dosen_login_set'];
			$data['dosen_tanggal_set'] = $result['dosen_tanggal_set'];
			
			$data['DosenID'] = $result['DosenID'];
			$DosenID = $result['DosenID'];
			$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
			if($resdosen)
			{
				$data['NamaDosen'] = $resdosen['nama'];
				$data['FotoDosen'] = $resdosen['foto'];
			}
			else
			{
				$data['NamaDosen'] = '';
				$data['FotoDosen'] = '';
			}
			
			$data['TahunID'] = $this->uri->segment(4);
			$JadwalID = $result['JadwalID'];
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$SubjekID = "";
			$TahunID = "";
			if($resjadwal)
			{
				$SubjekID = $resjadwal["SubjekID"];
				$TahunID = $resjadwal["TahunID"];
			}
			$reskrs = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			if($reskrs)
			{
				foreach($reskrs as $rk)
				{
					$MhswID = $rk->MhswID;
					$repmhsw = $this->m_presensi_mahasiswa->PTL_select_att($JadwalID,$PresensiID,$MhswID);
					if(!$repmhsw)
					{
						$data_att = array(
									'JadwalID' => $JadwalID,
									'KRSID' => $rk->KRSID,
									'PresensiID' => $PresensiID,
									'MhswID' => $MhswID,
									'JenisPresensiID' => '',
									'JenisPresensiID2' => '',
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
						$this->m_presensi_mahasiswa->PTL_insert($data_att);
					}
				}
			}
			$reskrs2 = $this->m_krs->PTL_all_spesifik_scoring($JadwalID);
			if($reskrs2)
			{
				$n = 1;
				foreach($reskrs2 as $rk2)
				{
					$KRSID = $rk2->KRSID;
					$krs2 = $this->m_krs2->PTL_select_spesifik($KRSID,$PresensiID);
					if(!$krs2)
					{
						$data_krs2 = array(
									'KRSID' => $KRSID,
									'MKID' => $MKID,
									'PresensiID' => $PresensiID
									);
						$this->m_krs2->PTL_insert($data_krs2);
					}
				}
			}
			$data['rowrecord'] = $this->m_presensi_mahasiswa->PTL_all_select($PresensiID);
			$pk1 = $PresensiID;
			$pk2 = $MKID;
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			
			$data['rowjenis1'] = $this->m_jenis_presensi->PTL_all();
			$data['rowjenis2'] = $this->m_jenis_presensi->PTL_all();
			$this->load->view('Portal/v_header_table');
			if($result['NA'] == "N")
			{
				$this->load->view('Attendance/v_attendance_list_attendance',$data);
			}
			else
			{
				$this->load->view('Attendance/v_attendance_list_attendance_trash',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_list_attendance_set()
		{
			$this->authentification();
			$PresensiID = $this->input->post('PresensiID');
			$MKID = $this->input->post('MKID');
			$TahunID = $this->input->post('TahunID');
			$NamaDosen = $this->input->post('NamaDosen');
			$JadwalID = $this->input->post('JadwalID');
			if($NamaDosen == "")
			{
				$datalog = array(
								'pk1' => $PresensiID,
								'pk2' => $MKID,
								'pk3' => $TahunID,
								'pk4' => $NamaDosen,
								'pk5' => $JadwalID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ATTEN13",
								'aktifitas' => "Mengakses halaman Attendance - List Attendance Set.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
				$SubjekID = '';
				if($resjadwal)
				{
					$SubjekID = $resjadwal['SubjekID'];
				}
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$NamaSub = '';
				if($ressub)
				{
					$NamaSub = $ressub['Nama'];
				}
				$respresensi = $this->m_presensi->PTL_select($PresensiID);
				$Tanggal = '';
				$JamMulai = '';
				$JamSelesai = '';
				$RuangID = '';
				$MKID = '';
				if($respresensi)
				{
					$Tanggal = tgl_singkat_eng($respresensi['Tanggal']);
					$JamMulai = substr($respresensi['JamMulai'],0,5);
					$JamSelesai = substr($respresensi['JamSelesai'],0,5);
					$RuangID = $respresensi['RuangID'];
					$MKID = $respresensi['MKID'];
				}
				$resmk = $this->m_mk->PTL_select($MKID);
				$NamaProject = '-';
				if($resmk)
				{
					$NamaProject = $resmk['Nama'];
				}
				$id_akun = $_COOKIE["id_akun"];
				$resakun = $this->m_akun->PTL_select($id_akun);
				$email = '';
				$email2 = '';
				if($resakun)
				{
					$email = $resakun['email'];
					$email2 = $resakun['email2'];
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to('anisa@esmodjakarta.com');
				$this->email->cc("$email,$email2");
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('LECTURER NOT SET (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px' style='max-width:300px; width:100%;'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					Hi Mr./Mrs. ANISA FAJAR IRIANZA!
					<br/>
					<br/>
					This email from the system. You have not set a lecturer for schedule <font color='green'><b>$NamaSub</b></font>, Academic Code $TahunID.
					<br/>
					<br/>
					<table border='1'>
						<tr>
							<th>Date</th>
							<th>Time</th>
							<th>Room</th>
							<th>Project</th>
						</tr>
						<tr>
							<td>$Tanggal</td>
							<td>$JamMulai - $JamSelesai</td>
							<td>$RuangID</td>
							<td><font color='red'><b>$NamaProject</b></font></td>
						</tr>
					</table>
					<br/>
					You have not set a lecturer responsible for this schedule, so that the lecturer is not able to set attendance. Please immediately arrange lecturers for this schedule.
					<br/>
					<br/>
					<table>
						<tr>
							<td><b>Short Link To Schedule</b></td>
							<td>:</td>
							<td>http://app.esmodjakarta.com/academic/attendance/ptl_list/$JadwalID</td>
						</tr>
						<tr>
							<td><b>Short Link To Attendance</b></td>
							<td>:</td>
							<td>http://app.esmodjakarta.com/academic/attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID</td>
						</tr>
					</table>
					<br/>
					You need to login first!
					<br/>
					<br/>
					Teachers who tried to set attendance: <font color='blue'><b>".strtoupper($_COOKIE["nama"])."</b></font>.
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px' style='max-width:120px; width:100%;'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Lecturer for this session has not been set. Please set the lecturer or contact your Coordinator or Academic Staff. Attendance not saved. Email notification will send automatically.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Lecturer for this session has not been set. Please set the lecturer or contact your Coordinator or Academic Staff. Attendance not saved. Email notification will send automatically.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
					}
					else
					{
						echo warning("Email server is not active. Lecturer for this session has not been set. Please set the lecturer or contact your Coordinator or Academic Staff. Attendance not saved.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
					}
				}
			}
			else
			{
				$optional = $this->input->post('optional');
				$JenisMKID = $this->input->post('JenisMKID');
				$total = $this->input->post('total') - 1;
				
				$error = array();
				$no = 1;
				foreach($_FILES as $field_name => $file)
				{
					$h = "-7";
					$hm = $h * 60;
					$ms = $hm * 60;
					$storage = gmdate("Y-m", time()-($ms));
					$tgl = gmdate("ymdHis", time()-($ms));
					$download = $file['name'];
					$downloadin = $tgl."_".str_replace(' ','_',$download);
					$uploadFile = '../academic/ptl_storage/file_student/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
					$config['upload_path'] = $uploadFile;
					$config['allowed_types'] = '*';
					$config['max_size'] = '1024';
					$config['max_width'] = '1024';
					$config['max_height'] = '768';
					$config['remove_spaces'] = true;
					$config['overwrite'] = false;
					$config['file_name'] = $downloadin;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload($field_name))
					{
						$error[] = $this->upload->display_errors();
					}
					else
					{
						$gambar0 = $this->upload->data();
						$gambarku = $gambar0['file_name'];
						$KRSID = $this->input->post("KRSID$no");
						$MKID = $this->input->post("MKID$no");
						$PresensiID = $this->input->post("PresensiID$no");
						$hasil = $this->m_krs2->PTL_insert_file($KRSID,$MKID,$PresensiID,$gambarku);
					}
					$no++;
				}
				
				$nremove = 0;
				$nactive = 0;
				if(($optional == "REMOVE") OR ($optional == "REMSAVE"))
				{
					for($ic=1;$ic<=$total;$ic++)
					{
						
						$KRSID = $this->input->post("KRSID$ic");
						$MKID = $this->input->post("MKID$ic");
						$PresensiID = $this->input->post("PresensiID$ic");
						$PresensiMhswID = $this->input->post("cek$ic");
						if($PresensiMhswID != "")
						{
							$data = array(
										'Removed' => 'Y',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
							$datakrs2 = array(
											'NA' => 'Y'
											);
							$this->m_krs2->PTL_update_krs2($KRSID,$MKID,$PresensiID,$datakrs2);
							$nremove++;
						}
						else
						{
							$PresensiMhswID = $this->input->post("cekno$ic");
							$data = array(
										'Removed' => 'N',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
							$datakrs2 = array(
											'NA' => 'N'
											);
							$this->m_krs2->PTL_update_krs2($KRSID,$MKID,$PresensiID,$datakrs2);
							$nactive++;
						}
					}
				}
				$PesanRemove = "";
				if($nremove > 0)
				{
					$PesanRemove = " $nremove data removed from attendance.";
				}
				$PesanActive = "";
				if($nactive > 0)
				{
					$PesanActive = " $nactive data activated from attendance.";
				}
				$DataAktifitas = "";
				if(($optional == "SAVE") OR ($optional == "REMSAVE"))
				{
					for($i=1;$i<=$total;$i++)
					{
						$PresensiMhswID = $this->input->post("PresensiMhswID$i");
						$KRSID = $this->input->post("KRSID$i");
						$MKID = $this->input->post("MKID$i");
						$Removed = $this->input->post("Removed$i");
						if($Removed == "N")
						{
							$JenisPresensiID = $this->input->post("setpresensis$i");
							$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr1 = 0;
							if($resp1)
							{
								$pr1 = $resp1["Score"];
							}
							$Score = 0;
							$multi = $this->input->post("multi$i");
							if($multi == "Y")
							{
								$JenisPresensiID = $this->input->post("setpresensid$i");
								$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
								$pr2 = 0;
								if($resp2)
								{
									$pr2 = $resp2["Score"];
								}
								$Score = ($pr1 + $pr2) / 2;
								$data = array(
											'Multi' => 'Y',
											'JenisPresensiID' => $this->input->post("setpresensis$i"),
											'JenisPresensiID2' => $this->input->post("setpresensid$i"),
											'Nilai' => $pr1,
											'Nilai2' => $pr2,
											'Score' => $Score,
											'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_edit' => $this->waktu
											);
								$DataAktifitas .= "$PresensiMhswID-".$this->input->post("setpresensis$i").",".$this->input->post("setpresensid$i").";";
							}
							else
							{
								$Score = $pr1;
								$data = array(
											'Multi' => 'N',
											'JenisPresensiID' => $this->input->post("setpresensis$i"),
											'Nilai' => $pr1,
											'Score' => $Score,
											'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_edit' => $this->waktu
											);
								$DataAktifitas .= "$PresensiMhswID-".$this->input->post("setpresensis$i").",;";
							}
							$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
							$grade = "";
							if($Score > 0)
							{
								if($Score > 13)
								{
									$grade = "A";
								}
								if(($Score > 10) AND ($Score <= 13))
								{
									$grade = "B";
								}
								if(($Score > 9) AND ($Score <= 10))
								{
									$grade = "C";
								}
								if(($Score > 0) AND ($Score <= 9))
								{
									$grade = "D";
								}
								if($Score == 0)
								{
									$grade = "E";
								}
							}
							$reskrs2 = $this->m_krs2->PTL_select_spesifik($KRSID,$PresensiID);
							if($reskrs2)
							{
								$AdditionalNilai = $reskrs2['AdditionalNilai'];
								if($AdditionalNilai > 0)
								{
									if($Score > 0)
									{
										$Score_baru = ($AdditionalNilai + $Score) / 2;
									}
									else
									{
										$Score_baru = $AdditionalNilai + $Score;
									}
									if($Score_baru > 13)
									{
										$grade = "A";
									}
									if(($Score_baru > 10) AND ($Score_baru <= 13))
									{
										$grade = "B";
									}
									if(($Score_baru > 9) AND ($Score_baru <= 10))
									{
										$grade = "C";
									}
									if(($Score_baru > 0) AND ($Score_baru <= 9))
									{
										$grade = "D";
									}
									if($Score_baru == 0)
									{
										$grade = "";
										// $grade = "E";
									}
								}
							}
							$data_krs2 = array(
										'MKID' => $MKID,
										'Nilai' => $Score,
										'GradeNilai' => $grade
										);
							$this->m_krs2->PTL_update_att2($KRSID,$PresensiID,$data_krs2);
						}
						else
						{
							$data = array(
										'Multi' => 'N',
										'JenisPresensiID' => '',
										'Nilai' => '0',
										'Score' => '0',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
							$data_krs2 = array(
											'MKID' => $MKID,
											'Nilai' => '0',
											'GradeNilai' => ''
											);
							$this->m_krs2->PTL_update_att2($KRSID,$PresensiID,$data_krs2);
						}
					}
				}
				$this->session->unset_userdata('att_set_presensi');
				
				$psn = "";
				if($_COOKIE["nama"] == $this->input->post('NamaDosen'))
				{
					$respresensi = $this->m_presensi->PTL_select($PresensiID);
					if($respresensi)
					{
						if($respresensi['dosen_login_set'] == "")
						{
							$data = array(
										'dosen_login_set' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'dosen_tanggal_set' => $this->waktu
										);
							$this->m_presensi->PTL_update($PresensiID,$data);
							$notifpesan = $this->input->post("notifpesan");
							if($notifpesan != "")
							{
								$psn = " You $notifpesan.";
							}
						}
						else
						{
							$psn = " You update the attendance.";
						}
					}
					else
					{
						$psn = " Schedule not found.";
					}
				}
				else
				{
					$data = array(
								'login_set' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_set' => $this->waktu
								);
					$this->m_presensi->PTL_update($PresensiID,$data);
				}
				$warning = $this->input->post('warning');
				if($warning != "")
				{
					$datalog = array(
									'pk1' => $PresensiID,
									'pk2' => $MKID,
									'pk3' => $TahunID,
									'pk4' => $NamaDosen,
									'pk5' => $JadwalID,
									'id_akun' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ATTEN13-01-Y",
									'aktifitas' => "Mengakses halaman Attendance - List Attendance Set.",
									'data' => $DataAktifitas,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("Your data successfully saved. System will redirect to scoring before.$psn$PesanRemove$PesanActive","../scoring/ptl_list/$JadwalID/$MKID");
				}
				else
				{
					$datalog = array(
									'pk1' => $PresensiID,
									'pk2' => $MKID,
									'pk3' => $TahunID,
									'pk4' => $NamaDosen,
									'pk5' => $JadwalID,
									'id_akun' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ATTEN13-01-N",
									'aktifitas' => "Mengakses halaman Attendance - List Attendance Set.",
									'data' => $DataAktifitas,
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("Your data successfully saved.$psn$PesanRemove$PesanActive","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
				}
			}
		}
		
		function ptl_download_file()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/file_student/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo "File does not exist...";
			}
		}
		
		function ptl_list_attendance_update()
		{
			$this->authentification();
			$PresensiMhswID = $this->uri->segment(3);
			$PresensiID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$MKID = $this->uri->segment(6);
			$KRSID = $this->uri->segment(7);
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $MKID,
							'pk3' => $PresensiMhswID,
							'pk4' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN14",
							'aktifitas' => "Mengakses halaman Attendance - List Attendance Update Remove: $PresensiMhswID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'Removed' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
			$datakrs2 = array(
							'NA' => 'Y'
							);
			$this->m_krs2->PTL_update_krs2($KRSID,$MKID,$PresensiID,$datakrs2);
			$reskrs = $this->m_krs2->PTL_select_spesifik($KRSID,$PresensiID);
			$KRS2ID = "";
			$GradeNilai = "";
			$AdditionalNilai = "";
			if($reskrs)
			{
				$KRS2ID = $reskrs['KRS2ID'];
				$GradeNilai = $reskrs['GradeNilai'];
				$AdditionalNilai = $reskrs['AdditionalNilai'];
			}
			$data = array(
						'GradeNilai' => '',
						'AdditionalNilai' => 0
						);
			$this->m_krs2->PTL_update($KRS2ID,$data);
			$reskrs2 = $this->m_krs2->PTL_select_scoring($KRSID,$MKID);
			$KRS2ID = "";
			if($reskrs2)
			{
				$KRS2ID = $reskrs2['KRS2ID'];
			}
			$data = array(
						'GradeNilai' => $GradeNilai,
						'AdditionalNilai' => $AdditionalNilai
						);
			$this->m_krs2->PTL_update($KRS2ID,$data);
			echo warning("Your data successfully removed.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
		}
		
		function ptl_list_attendance_update_active()
		{
			$this->authentification();
			$PresensiMhswID = $this->uri->segment(3);
			$PresensiID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$MKID = $this->uri->segment(6);
			$KRSID = $this->uri->segment(7);
			$datalog = array(
							'pk1' => $PresensiID,
							'pk2' => $MKID,
							'pk3' => $PresensiMhswID,
							'pk4' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN15",
							'aktifitas' => "Mengakses halaman Attendance - List Attendance Update Active: $PresensiMhswID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'Removed' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
			$datakrs2 = array(
							'NA' => 'N'
							);
			$this->m_krs2->PTL_update_krs2($KRSID,$MKID,$PresensiID,$datakrs2);
			echo warning("Your data successfully active.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
		}
		
		function ptl_list_attendance_delete()
		{
			$this->authentification();
			$PresensiMhswID = $this->uri->segment(3);
			$PresensiID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$MKID = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $PresensiMhswID,
							'pk2' => $PresensiID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN16",
							'aktifitas' => "Mengakses halaman Attendance - List Attendance Delete.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->m_presensi_mahasiswa->PTL_delete($PresensiMhswID);
			echo warning("Your data successfully active.","../attendance/ptl_list_attendance/$PresensiID/$TahunID/$MKID");
		}
		
		function ptl_pdf_recap()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN17",
							'aktifitas' => "Mengakses halaman Attendance - PDF Recap.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ProgramID = $cekjurusan;
			$kelas = $this->uri->segment(6);
			if($ProgramID == "INT")
			{
				$kelas = "O".substr($this->uri->segment(6),1);
			}
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"STUDENT ATTENDANCE RECAPITULATION",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(6, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "", 0, "", "L");
			$this->fpdf->Cell(3, 1, "", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $cektahun - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(6, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": $kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(6, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "", 0, "", "L");
			$this->fpdf->Cell(2, -2, "", 0, "", "L");
			$this->fpdf->Cell(6, -2, "", 0, "", "L");
			$this->fpdf->Cell(2, -2, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, -2, ": ".tgl_indo($resjadwal["TglMulai"])." - ".tgl_indo($resjadwal["TglSelesai"]), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(19, -2,"ATTENDANCE DETAIL", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Ln();
			$this->fpdf->Cell(1, 1, "NO", "LBTR", 0, "C");
			$this->fpdf->Cell(8, 1, "STUDENT NAME & ID", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "CLASS", "LBTR", 0, "C");
			$this->fpdf->Cell(8, 0.5, "ATTENDANCE", "LBTR", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(11, 0.5, "", "", 0, "C");
			$this->fpdf->Cell(2, 0.5, "EXCUSE", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 0.5, "SICK", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 0.5, "ABSENT", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 0.5, "LATE", "LBTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				$MhswID = $r->MhswID;
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				if($resmhsw["Foto"] == "")
				{
					$foto = "foto_umum/user.jpg";
				}
				else
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
					if($exist)
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					}
					else
					{
						$foto = "foto_umum/user.jpg";
					}
				}
				$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$KelasID = $reskhs["KelasID"];
				$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
				$TahunKe = $reskhs["TahunKe"];
				if($ProgramID == "INT")
				{
					$TahunKe = "O";
				}
				$kelas = "";
				if($reskelas)
				{
					$kelas = $reskelas["Nama"];
				}
				$KRSID = $r->KRSID;
				$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
				if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
				$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
				if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
				$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
				if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
				$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
				if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
				if($no == $tot)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $texc, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tsic, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tabs, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tlat, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
				}
				else
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $texc, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tsic, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tabs, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, $tlat, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Attendance_Recap_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_dpm()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$kelas = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'pk4' => $kelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN18",
							'aktifitas' => "Mengakses halaman Attendance - PDF DPM.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ProgramID = $cekjurusan;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(29,0.7,"STUDENT ATTENDANCE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(29,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,28.5,2.5);
			$this->fpdf->Line(1,2.55,28.5,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, 1, ": $kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $TahunID - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(15, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": ".tgl_indo($resjadwal["TglMulai"])." - ".tgl_indo($resjadwal["TglSelesai"]), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "Please ask your student to sign in each session. In case of student who coming late, give coming time remarks inside the box.  Example :", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -3, "[Student Signature]", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -4, "Late 12:20", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(9.5, -4, "", 0, "", "L");
			$this->fpdf->Cell(3, -4, "ATTENDANCE DETAIL", 0, "", "L");
			$this->fpdf->Cell(29, -3,"", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Ln();
			$this->fpdf->Cell(1, 1, "NO", "LBTR", 0, "C");
			$this->fpdf->Cell(8, 1, "STUDENT NAME & ID", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "CLASS", "LBTR", 0, "C");
			$this->fpdf->Cell(16.5, 0.5, "DATE", "LBTR", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(11, 0.5, "", "", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, "", "LBTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				if($no == $tot)
				{
					$MhswID = $r->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				else
				{
					$MhswID = $r->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "", "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Attendance_DPM_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_recap_dpm()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN19",
							'aktifitas' => "Mengakses halaman Attendance - PDF Recap DPM.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ProgramID = $cekjurusan;
			$kelas = $this->uri->segment(6);
			if($ProgramID == "INT")
			{
				$kelas = "O".substr($this->uri->segment(6),1);
			}
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(29,0.7,"STUDENT ATTENDANCE LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(29,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,28.5,2.5);
			$this->fpdf->Line(1,2.55,28.5,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, 1, ": $kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $cektahun - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(15, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": ".tgl_indo($resjadwal["TglMulai"])." - ".tgl_indo($resjadwal["TglSelesai"]), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "Please ask your student to sign in each session. In case of student who coming late, give coming time remarks inside the box.  Example :", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -3, "[Student Signature]", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -4, "Late 12:20", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(9.5, -4, "", 0, "", "L");
			$this->fpdf->Cell(3, -4, "ATTENDANCE DETAIL", 0, "", "L");
			$this->fpdf->Cell(29, -3,"", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Ln();
			$this->fpdf->Cell(1, 1, "NO", "LBTR", 0, "C");
			$this->fpdf->Cell(8, 1, "STUDENT NAME & ID", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "CLASS", "LBTR", 0, "C");
			$this->fpdf->Cell(16.5, 0.5, "DATE", "LBTR", 0, "C");
			$this->fpdf->Ln();
			$nn = 0;
			$tgl1 = "";
			$tgl2 = "";
			$tgl3 = "";
			$tgl4 = "";
			$tgl5 = "";
			$tgl6 = "";
			$tgl7 = "";
			$tgl8 = "";
			$tgl9 = "";
			$tgl10 = "";
			$tgl11 = "";
			foreach($result as $r)
			{
				$JadwalID = $r->JadwalID;
				$rowatt = $this->m_presensi->PTL_all_select($JadwalID);
				if($rowatt)
				{
					foreach($rowatt as $ra)
					{
						$nn++;
						if($nn == 1){ $tgl1 = explode("-",$ra->Tanggal); }
						if($nn == 2){ $tgl2 = explode("-",$ra->Tanggal); }
						if($nn == 3){ $tgl3 = explode("-",$ra->Tanggal); }
						if($nn == 4){ $tgl4 = explode("-",$ra->Tanggal); }
						if($nn == 5){ $tgl5 = explode("-",$ra->Tanggal); }
						if($nn == 6){ $tgl6 = explode("-",$ra->Tanggal); }
						if($nn == 7){ $tgl7 = explode("-",$ra->Tanggal); }
						if($nn == 8){ $tgl8 = explode("-",$ra->Tanggal); }
						if($nn == 9){ $tgl9 = explode("-",$ra->Tanggal); }
						if($nn == 10){ $tgl10 = explode("-",$ra->Tanggal); }
						if($nn == 11){ $tgl11 = explode("-",$ra->Tanggal); }
					}
				}
			}
			$this->fpdf->Cell(11, 0.5, "", "", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl1[0],2,2)."/".@$tgl1[1]."/".@$tgl1[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl2[0],2,2)."/".@$tgl2[1]."/".@$tgl2[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl3[0],2,2)."/".@$tgl3[1]."/".@$tgl3[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl4[0],2,2)."/".@$tgl4[1]."/".@$tgl4[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl5[0],2,2)."/".@$tgl5[1]."/".@$tgl5[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl6[0],2,2)."/".@$tgl6[1]."/".@$tgl6[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl7[0],2,2)."/".@$tgl7[1]."/".@$tgl7[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl8[0],2,2)."/".@$tgl8[1]."/".@$tgl8[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl9[0],2,2)."/".@$tgl9[1]."/".@$tgl9[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl10[0],2,2)."/".@$tgl10[1]."/".@$tgl10[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl11[0],2,2)."/".@$tgl11[1]."/".@$tgl11[2], "LBTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				$MhswID = $r->MhswID;
				$JadwalID = $r->JadwalID;
				$rowatt = $this->m_presensi->PTL_all_select($JadwalID);
				$n = 0;
				$n1 = "";
				$n2 = "";
				$n3 = "";
				$n4 = "";
				$n5 = "";
				$n6 = "";
				$n7 = "";
				$n8 = "";
				$n9 = "";
				$n10 = "";
				$n11 = "";
				if($rowatt)
				{
					foreach($rowatt as $ra)
					{
						$n++;
						$PresensiID = $ra->PresensiID;
						$respm = $this->m_presensi_mahasiswa->PTL_select_att($JadwalID,$PresensiID,$MhswID);
						$nilai = "-";
						if($respm)
						{
							if($respm["JenisPresensiID"] == "")
							{
								$nilai = "-";
							}
							else
							{
								$nilai = $respm["JenisPresensiID"];
							}
						}
						if($n == 1){ $n1 = $nilai; }
						if($n == 2){ $n2 = $nilai; }
						if($n == 3){ $n3 = $nilai; }
						if($n == 4){ $n4 = $nilai; }
						if($n == 5){ $n5 = $nilai; }
						if($n == 6){ $n6 = $nilai; }
						if($n == 7){ $n7 = $nilai; }
						if($n == 8){ $n8 = $nilai; }
						if($n == 9){ $n9 = $nilai; }
						if($n == 10){ $n10 = $nilai; }
						if($n == 11){ $n11 = $nilai; }
					}
				}
				if($no == $tot)
				{
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n1, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n2, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n3, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n4, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n5, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n6, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n7, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n8, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n9, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n10, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n11, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				else
				{
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n1, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n2, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n3, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n4, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n5, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n6, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n7, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n8, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n9, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n10, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n11, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Attendance_DPM_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_dpd()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$kelas = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'pk4' => $kelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN20",
							'aktifitas' => "Mengakses halaman Attendance - PDF DPD.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ProgramID = $cekjurusan;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_presensi->PTL_all_select($JadwalID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"PRESENCE LIST OF LECTURERS",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(7, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "", 0, "", "L");
			$this->fpdf->Cell(3, 1, "", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $TahunID - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(7, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": $kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(7, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "", 0, "", "L");
			$this->fpdf->Cell(2, -2, "", 0, "", "L");
			$this->fpdf->Cell(7, -2, "", 0, "", "L");
			$this->fpdf->Cell(2, -2, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, -2, ": ".tgl_indo($resjadwal["TglMulai"])." - ".tgl_indo($resjadwal["TglSelesai"]), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -4, "", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(4.7, -4, "", 0, "", "L");
			$this->fpdf->Cell(3, -4, "PRESENCE DETAIL", 0, "", "L");
			$this->fpdf->Cell(29, -3,"", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5, 1, "#", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "DATE", "LBTR", 0, "C");
			$this->fpdf->Cell(3, 1, "TIME", "LBTR", 0, "C");
			$this->fpdf->Cell(11.5, 1, "TOPIC", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "INITIALS", "LBTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				if($no == $tot)
				{
					$DosenID = $r->DosenID;
					$dsn = $this->m_dosen->PTL_select($DosenID);
					$MKID = $r->MKID;
					$mkid = $this->m_mk->PTL_select($MKID);
					$nmmk = "";
					if($mkid)
					{
						$nmmk = "$mkid[Nama]";
					}
					$nmdsn = "";
					if($dsn)
					{
						$nmdsn = $dsn['Nama'];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 1, $no, "LTR", 0, "C");
					$this->fpdf->Cell(2, 1, tgl_indo($r->Tanggal), "LTR", 0, "C");
					$this->fpdf->Cell(3, 1, "$r->JamMulai - $r->JamSelesai", "LTR", 0, "C");
					$this->fpdf->Cell(11.5, 1, $nmmk, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "Room: $r->RuangID", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,0,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,55,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,110,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LBR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LBR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LBR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,165,55), "LBR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LBR", 0, "C");
				}
				else
				{
					$DosenID = $r->DosenID;
					$dsn = $this->m_dosen->PTL_select($DosenID);
					$MKID = $r->MKID;
					$mkid = $this->m_mk->PTL_select($MKID);
					$nmmk = "";
					if($mkid)
					{
						$nmmk = "$mkid[Nama]";
					}
					$nmdsn = "";
					if($dsn)
					{
						$nmdsn = $dsn['Nama'];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, $no, "LTR", 0, "C");
					$this->fpdf->Cell(2, 0.5, tgl_indo($r->Tanggal), "LTR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "$r->JamMulai - $r->JamSelesai", "LTR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, $nmmk, "LTR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "Room: $r->RuangID", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,0,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,55,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,110,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(3, 0.5, "", "LR", 0, "C");
					$this->fpdf->Cell(11.5, 0.5, substr($r->Catatan,165,55), "LR", 0, "L");
					$this->fpdf->Cell(2, 0.5, "", "LR", 0, "C");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Attendance_DPD_$id_akun-$nama.pdf","I");
		}
		
		function ptl_set_check_all()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$ProgramID = $this->uri->segment(6);
			$kls = $this->uri->segment(7);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'pk4' => $ProgramID,
							'pk5' => $kls,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN21",
							'aktifitas' => "Mengakses halaman Attendance - Set Check All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->session->set_userdata('attendance_set_all','Y');
			redirect("attendance/ptl_list_enroll/$JadwalID/$SubjekID/$TahunID/$ProgramID/$kls");
		}
		
		function ptl_set_uncheck_all()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$ProgramID = $this->uri->segment(6);
			$kls = $this->uri->segment(7);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'pk4' => $ProgramID,
							'pk5' => $kls,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN22",
							'aktifitas' => "Mengakses halaman Attendance - Set Uncheck All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->session->unset_userdata('attendance_set_all');
			redirect("attendance/ptl_list_enroll/$JadwalID/$SubjekID/$TahunID/$ProgramID/$kls");
		}
		
		function ptl_list_enroll()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$cektahun = $this->uri->segment(5);
			$cekjurusan = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $cektahun,
							'pk4' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN23",
							'aktifitas' => "Mengakses halaman Attendance - List Enroll.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$word = explode(".",$resjadwal['ProdiID']);
			$cekprodi1 = $word[0];
			$cekprodi2 = @$word[1];
			$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_enroll_attendance($cekjurusan,$cekprodi1,$cekprodi2,$cektahun);
			$data['JadwalID'] = $JadwalID;
			$data['AddSpesialisasiID'] = $resjadwal['SpesialisasiID'];
			$data['SubjekID'] = $SubjekID;
			$data['TahunID'] = $cektahun;
			$data['ProgramID'] = $cekjurusan;
			$data['kls'] = $this->uri->segment(7);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_list_enrollment',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_enroll()
		{
			$this->authentification();
			$add_JadwalID = $this->input->post('JadwalID');
			$JadwalID = $this->input->post('JadwalID');
			$add_SubjekID = $this->input->post('SubjekID');
			$add_TahunID = $this->input->post('TahunID');
			$add_ProgramID = $this->input->post('ProgramID');
			$add_kelas = $this->input->post('kls');
			$datalog = array(
							'pk1' => $add_JadwalID,
							'pk2' => $add_SubjekID,
							'pk3' => $add_TahunID,
							'pk4' => $add_ProgramID,
							'pk5' => $add_kelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN24",
							'aktifitas' => "Mengakses halaman Attendance - Enroll.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$total = $this->input->post('total') - 1;
			$nmhsw = 0;
			$nsave = 0;
			for($i=1;$i<=$total;$i++)
			{
				$KHSID = $this->input->post("check$i");
				if($KHSID != "")
				{
					$nmhsw++;
					$MhswID = $this->input->post("MhswID$i");
					$TahunID = $this->input->post("TahunID$i");
					$ProdiID = $this->input->post("ProdiID$i");
					$ProgramID = $this->input->post("ProgramID$i");
					$TahunKe = $this->input->post("TahunKe$i");
					// $rowjadwal = $this->m_jadwal->PTL_all_enroll($TahunID,$ProdiID,$ProgramID,$TahunKe);
					$rj = $this->m_jadwal->PTL_select($JadwalID);
					if($rj)
					{
						$KelasID = $this->input->post("KelasID$i");
						if($rj['Gabungan'] == "N")
						{
							if($rj['KelasID'] == $KelasID)
							{
								if($rj['SubjekID'] == $add_SubjekID)
								{
									$data = array(
												'KHSID' => $KHSID,
												'MhswID' => $this->input->post("MhswID$i"),
												'TahunID' => $this->input->post("TahunID$i"),
												'JadwalID' => $rj['JadwalID'],
												'SubjekID' => $rj['SubjekID'],
												'KodeID' => 'ES01',
												'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
												'tanggal_buat' => $this->waktu
												);
									$this->m_krs->PTL_insert($data);
									$nsave++;
								}
							}
						}
						if($rj['Gabungan'] == "Y")
						{
							if(stristr($rj['KelasIDGabungan'],"^".$KelasID."."))
							{
								if($rj['SubjekID'] == $add_SubjekID)
								{
									$data = array(
												'KHSID' => $KHSID,
												'MhswID' => $this->input->post("MhswID$i"),
												'TahunID' => $this->input->post("TahunID$i"),
												'JadwalID' => $rj['JadwalID'],
												'SubjekID' => $rj['SubjekID'],
												'KodeID' => 'ES01',
												'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
												'tanggal_buat' => $this->waktu
												);
									$this->m_krs->PTL_insert($data);
									$nsave++;
								}
							}
						}
					}
				}
			}
			if($nsave == 0)
			{
				echo warning("You don't save any data.","../attendance/ptl_list_enroll/$add_JadwalID/$add_SubjekID/$add_TahunID/$add_ProgramID/$add_kelas");
			}
			else
			{
				echo warning("Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved.","../attendance/ptl_list_enroll/$add_JadwalID/$add_SubjekID/$add_TahunID/$add_ProgramID/$add_kelas");
			}
		}
		
		function ptl_notes_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN25",
							'aktifitas' => "Mengakses halaman Attendance - Notes Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['JadwalID'] = $JadwalID;
			$data['rowrecord'] = $this->m_krs->PTL_all_spesifik_scoring($JadwalID);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_list_notes_form',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_notes_insert()
		{
			$this->authentification();
			$JadwalID = $this->input->post("JadwalID");
			$total = $this->input->post("total");
			$nsave = 0;
			for($i=1;$i<=$total;$i++)
			{
				$title = $this->input->post("title$i");
				$comment = $this->input->post("comment$i");
				if(($title != "") OR ($comment != ""))
				{
					$result = $this->m_catatan2->PTL_urut();
					$lastid = $result['LAST'];
					$noteid = $lastid + 1;
					$MhswID = $this->input->post("MhswID$i");
					$data = array(
								'noteid' => $noteid,
								'JadwalID' => $JadwalID,
								'MhswID' => $MhswID,
								'title' => $title,
								'comment' => $comment,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
					$this->m_catatan2->PTL_insert($data);
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$rowakun = $this->m_akun->PTL_all_send_notification();
					if($rowakun)
					{
						foreach($rowakun as $ra)
						{
							$datanotif = array(
											'id_akun' => $ra->id_akun,
											'nama' => $ra->nama,
											'noteid' => $noteid,
											'title' => "$_COOKIE[nama] fill out the notes for $resmhsw[Nama].",
											'nama_buat' => $_COOKIE["nama"],
											'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
											'tanggal_buat' => $this->waktu
											);
							$this->m_notifikasi->PTL_insert($datanotif);
						}
					}
					
					$nsave++;
				}
			}
			if($nsave == 0)
			{
				$datalog = array(
								'pk1' => $JadwalID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ATTEN26-N",
								'aktifitas' => "Mengakses halaman Attendance - Notes Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("You don't save any data.","../attendance/ptl_list/$JadwalID");
			}
			else
			{
				$datalog = array(
								'pk1' => $JadwalID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "ATTEN26-Y",
								'aktifitas' => "Mengakses halaman Attendance - Notes Insert.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("Your data successfully saved. '$nsave' data NOTES saved.","../attendance/ptl_list/$JadwalID");
			}
		}
		
		function ptl_notes_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$noteid = $this->uri->segment(3);
			$JadwalID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $noteid,
							'pk2' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN29",
							'aktifitas' => "Mengakses halaman Attendance - Notes Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['noteid'] = $noteid;
			$data['JadwalID'] = $JadwalID;
			$result = $this->m_catatan2->PTL_select($noteid);
			$data['noteid'] = $result['noteid'];
			$data['title'] = $result['title'];
			$data['comment'] = $result['comment'];
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_list_notes_edit',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_notes_update()
		{
			$this->authentification();
			$noteid = $this->input->post('noteid');
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $noteid,
							'pk2' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN30",
							'aktifitas' => "Mengakses halaman Attendance - Notes Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'JadwalID' => $JadwalID,
						'title' => $this->input->post('title'),
						'comment' => $this->input->post('comment'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_catatan2->PTL_update($noteid,$data);
			echo warning("Your data successfully updated.","../attendance/ptl_list/$JadwalID");
		}
		
		function ptl_clashed()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN27",
							'aktifitas' => "Mengakses halaman Attendance - Clashed.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowrecord'] = $this->m_presensi->PTL_all_active_year();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Attendance/v_attendance_clashed',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_change_lecturer_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN31-01",
							'aktifitas' => "Mengakses halaman Add Attendance  - Edit Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['JadwalID'] = $JadwalID;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = "";
			$data['TahunID'] = "";
			$data['SpesialisasiID'] = "";
			$data['RencanaKehadiran'] = "";
			$data['MaxAbsen'] = "";
			$data['login_buat'] = "";
			$data['tanggal_buat'] = "";
			$data['login_edit'] = "";
			$data['tanggal_edit'] = "";
			$data['NA'] = "";
			if($resjadwal)
			{
				$data['ProgramID'] = $resjadwal['ProgramID'];
				$data['TahunID'] = $resjadwal['TahunID'];
				$data['SpesialisasiID'] = $resjadwal['SpesialisasiID'];
				$data['RencanaKehadiran'] = $resjadwal['RencanaKehadiran'];
				$data['MaxAbsen'] = $resjadwal['MaxAbsen'];
				$data['login_buat'] = $resjadwal['login_buat'];
				$data['tanggal_buat'] = $resjadwal['tanggal_buat'];
				$data['login_edit'] = $resjadwal['login_edit'];
				$data['tanggal_edit'] = $resjadwal['tanggal_edit'];
				$data['NA'] = $resjadwal['NA'];
			}
			if($resjadwal['DosenID'] == "")
			{
				$data['NamaDosen'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID'];
				$resdosen = $this->m_dosen->PTL_select($DosenID);
				if($resdosen)
				{
					$data['NamaDosen'] = $resjadwal['DosenID']." - ".$resdosen['Nama'];
				}
				else
				{
					$data['NamaDosen'] = "";
				}
			}
			if($resjadwal['DosenID2'] == "")
			{
				$data['NamaDosen2'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen2)
				{
					$data['NamaDosen2'] = $resjadwal['DosenID2']." - ".$resdosen2['Nama'];
				}
				else
				{
					$data['NamaDosen2'] = "";
				}
			}
			if($resjadwal['DosenID3'] == "")
			{
				$data['NamaDosen3'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen3)
				{
					$data['NamaDosen3'] = $resjadwal['DosenID3']." - ".$resdosen3['Nama'];
				}
				else
				{
					$data['NamaDosen3'] = "";
				}
			}
			if($resjadwal['DosenID4'] == "")
			{
				$data['NamaDosen4'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen4)
				{
					$data['NamaDosen4'] = $resjadwal['DosenID4']." - ".$resdosen4['Nama'];
				}
				else
				{
					$data['NamaDosen4'] = "";
				}
			}
			if($resjadwal['DosenID5'] == "")
			{
				$data['NamaDosen5'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen5)
				{
					$data['NamaDosen5'] = $resjadwal['DosenID5']." - ".$resdosen5['Nama'];
				}
				else
				{
					$data['NamaDosen5'] = "";
				}
			}
			if($resjadwal['DosenID6'] == "")
			{
				$data['NamaDosen6'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen6)
				{
					$data['NamaDosen6'] = $resjadwal['DosenID6']." - ".$resdosen6['Nama'];
				}
				else
				{
					$data['NamaDosen6'] = "";
				}
			}
			$data['rowspesialisasi'] = $this->m_spesialisasi->PTL_all_data();
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_change_lecturer_form',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_change_lecturer_form_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN31-02",
							'aktifitas' => "Mengakses halaman Add Attendance - Update Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$wor1 = explode(" - ",$this->input->post('DosenID'));
			$wor2 = explode(" - ",$this->input->post('DosenID2'));
			$wor3 = explode(" - ",$this->input->post('DosenID3'));
			$wor4 = explode(" - ",$this->input->post('DosenID4'));
			$wor5 = explode(" - ",$this->input->post('DosenID5'));
			$wor6 = explode(" - ",$this->input->post('DosenID6'));
			$NA = "N";
			if($this->input->post('NA') == "Y")
			{
				$NA = "Y";
			}
			$data = array(
						'SpesialisasiID' => $this->input->post('SpesialisasiID'),
						'DosenID' => $wor1[0],
						'DosenID2' => $wor2[0],
						'DosenID3' => $wor3[0],
						'DosenID4' => $wor4[0],
						'DosenID5' => $wor5[0],
						'DosenID6' => $wor6[0],
						'RencanaKehadiran' => $this->input->post('RencanaKehadiran'),
						'KehadiranMin' => $this->input->post('RencanaKehadiran') - $this->input->post('MaxAbsen'),
						'MaxAbsen' => $this->input->post('MaxAbsen'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $NA
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			echo warning("Range has been updated.","../attendance/ptl_list_form/$JadwalID");
		}
		
		function ptl_change_lecturer_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$JadwalID = $this->uri->segment(3);
			$PresensiID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $PresensiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN31-03",
							'aktifitas' => "Mengakses halaman Edit Attendance - Edit Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['JadwalID'] = $JadwalID;
			$data['PresensiID'] = $PresensiID;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['ProgramID'] = "";
			$data['TahunID'] = "";
			$data['SpesialisasiID'] = "";
			$data['RencanaKehadiran'] = "";
			$data['MaxAbsen'] = "";
			$data['login_buat'] = "";
			$data['tanggal_buat'] = "";
			$data['login_edit'] = "";
			$data['tanggal_edit'] = "";
			$data['NA'] = "";
			if($resjadwal)
			{
				$data['ProgramID'] = $resjadwal['ProgramID'];
				$data['TahunID'] = $resjadwal['TahunID'];
				$data['SpesialisasiID'] = $resjadwal['SpesialisasiID'];
				$data['RencanaKehadiran'] = $resjadwal['RencanaKehadiran'];
				$data['MaxAbsen'] = $resjadwal['MaxAbsen'];
				$data['login_buat'] = $resjadwal['login_buat'];
				$data['tanggal_buat'] = $resjadwal['tanggal_buat'];
				$data['login_edit'] = $resjadwal['login_edit'];
				$data['tanggal_edit'] = $resjadwal['tanggal_edit'];
				$data['NA'] = $resjadwal['NA'];
			}
			if($resjadwal['DosenID'] == "")
			{
				$data['NamaDosen'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID'];
				$resdosen = $this->m_dosen->PTL_select($DosenID);
				if($resdosen)
				{
					$data['NamaDosen'] = $resjadwal['DosenID']." - ".$resdosen['Nama'];
				}
				else
				{
					$data['NamaDosen'] = "";
				}
			}
			if($resjadwal['DosenID2'] == "")
			{
				$data['NamaDosen2'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID2'];
				$resdosen2 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen2)
				{
					$data['NamaDosen2'] = $resjadwal['DosenID2']." - ".$resdosen2['Nama'];
				}
				else
				{
					$data['NamaDosen2'] = "";
				}
			}
			if($resjadwal['DosenID3'] == "")
			{
				$data['NamaDosen3'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID3'];
				$resdosen3 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen3)
				{
					$data['NamaDosen3'] = $resjadwal['DosenID3']." - ".$resdosen3['Nama'];
				}
				else
				{
					$data['NamaDosen3'] = "";
				}
			}
			if($resjadwal['DosenID4'] == "")
			{
				$data['NamaDosen4'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID4'];
				$resdosen4 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen4)
				{
					$data['NamaDosen4'] = $resjadwal['DosenID4']." - ".$resdosen4['Nama'];
				}
				else
				{
					$data['NamaDosen4'] = "";
				}
			}
			if($resjadwal['DosenID5'] == "")
			{
				$data['NamaDosen5'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID5'];
				$resdosen5 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen5)
				{
					$data['NamaDosen5'] = $resjadwal['DosenID5']." - ".$resdosen5['Nama'];
				}
				else
				{
					$data['NamaDosen5'] = "";
				}
			}
			if($resjadwal['DosenID6'] == "")
			{
				$data['NamaDosen6'] = "";
			}
			else
			{
				$DosenID = $resjadwal['DosenID6'];
				$resdosen6 = $this->m_dosen->PTL_select($DosenID);
				if($resdosen6)
				{
					$data['NamaDosen6'] = $resjadwal['DosenID6']." - ".$resdosen6['Nama'];
				}
				else
				{
					$data['NamaDosen6'] = "";
				}
			}
			$data['rowspesialisasi'] = $this->m_spesialisasi->PTL_all_data();
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_change_lecturer_edit',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_change_lecturer_update()
		{
			$this->authentification();
			$JadwalID = $this->input->post('JadwalID');
			$PresensiID = $this->input->post('PresensiID');
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $PresensiID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN31-04",
							'aktifitas' => "Mengakses halaman Edit Attendance - Update Range.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$wor1 = explode(" - ",$this->input->post('DosenID'));
			$wor2 = explode(" - ",$this->input->post('DosenID2'));
			$wor3 = explode(" - ",$this->input->post('DosenID3'));
			$wor4 = explode(" - ",$this->input->post('DosenID4'));
			$wor5 = explode(" - ",$this->input->post('DosenID5'));
			$wor6 = explode(" - ",$this->input->post('DosenID6'));
			$NA = "N";
			if($this->input->post('NA') == "Y")
			{
				$NA = "Y";
			}
			$data = array(
						'SpesialisasiID' => $this->input->post('SpesialisasiID'),
						'DosenID' => $wor1[0],
						'DosenID2' => $wor2[0],
						'DosenID3' => $wor3[0],
						'DosenID4' => $wor4[0],
						'DosenID5' => $wor5[0],
						'DosenID6' => $wor6[0],
						'RencanaKehadiran' => $this->input->post('RencanaKehadiran'),
						'KehadiranMin' => $this->input->post('RencanaKehadiran') - $this->input->post('MaxAbsen'),
						'MaxAbsen' => $this->input->post('MaxAbsen'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $NA
						);
			$this->m_jadwal->PTL_update($JadwalID,$data);
			echo warning("Lecturer data has been updated.","../attendance/ptl_list_edit/$PresensiID");
		}
		
		function ptl_list_project_sheet_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','attendance');
			$this->session->unset_userdata('aksi');
			$JadwalID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN32",
							'aktifitas' => "Mengakses halaman Attendance - Project Sheet Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $resjadwal['JadwalID'];
			$data['ProgramID'] = $resjadwal['ProgramID'];
			$data['TahunID'] = $resjadwal['TahunID'];
			$data['file_project'] = $resjadwal['file_project'];
			$data['file_evaluation'] = $resjadwal['file_evaluation'];
			$this->load->view('Portal/v_header');
			$this->load->view('Attendance/v_attendance_list_project_form',$data);
			$this->load->view('Portal/v_footer_attendance');
		}
		
		function ptl_list_project_sheet_update()
		{
			$this->authentification();
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$JadwalID = $this->input->post('JadwalID');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ATTEN33",
							'aktifitas' => "Mengakses halaman Attendance - Project Sheet Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$error = array();
			$gambar = array();
			$no = 1;
			$Emailfile_project = "";
			$Emailfile_evaluation = "";
			foreach($_FILES as $field_name => $file)
			{
				$storage = gmdate("Y-m", time()-($ms));
				$tgl = gmdate("ymdHis", time()-($ms));
				$download = $file['name'];
				$downloadin = $tgl."_".str_replace(' ','_',$download);
				if($no == 1)
				{
					$uploadFile = '../academic/ptl_storage/project_sheet/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				if($no == 2)
				{
					$uploadFile = '../academic/ptl_storage/evaluation_sheet/'.$storage;
					if(!is_dir($uploadFile))
					{
						mkdir($uploadFile, DIR_WRITE_MODE);
					}
				}
				$config['upload_path'] = $uploadFile;
				$config['allowed_types'] = '*';
				$config['max_size'] = '20480';
				$config['remove_spaces'] = true;
				$config['overwrite'] = false;
				$config['file_name'] = $downloadin;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload($field_name))
				{
					$error[] = $this->upload->display_errors();
					if($no == 1)
					{
						$file = substr($this->input->post('file_project'),8);
					}
					if($no == 2)
					{
						$file = substr($this->input->post('file_evaluation'),8);
					}
					$gambar[] = $file;
				}
				else
				{ 
					$gambar0 = $this->upload->data();
					if($no == 1)
					{
						$gambar_lama = $this->input->post('file_project');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							$Emailfile_project = $storage."_".$gambar0['file_name'];
							unlink("../academic/ptl_storage/project_sheet/".$dir."/".$file);
						}
						else
						{
							$Emailfile_project = $storage."_".$this->input->post('file_project');
						}
					}
					if($no == 2)
					{
						$gambar_lama = $this->input->post('file_evaluation');
						$dir = substr($gambar_lama,0,7);
						$file = substr($gambar_lama,8);
						if($gambar0['file_name'])
						{
							$Emailfile_evaluation = $storage."_".$gambar0['file_name'];
							unlink("../academic/ptl_storage/evaluation_sheet/".$dir."/".$file);
						}
						else
						{
							$Emailfile_evaluation = $storage."_".$this->input->post('file_evaluation');
						}
					}
					$gambar[] = $gambar0['file_name'];
				}
				$no++;
			}
			$EmailFile1 = "<font color='red'>No file uploaded. Contact your coordinator.</font>";
			if($Emailfile_project != "")
			{
				$EmailFile1 = "<a href='".base_url("download_general/ptl_download_project_sheet/$JadwalID/$Emailfile_project")."' title='Download File : $Emailfile_project'>Download</a>";
			}
			$EmailFile2 = "<font color='red'>No file uploaded. Contact your coordinator.</font>";
			if($Emailfile_evaluation != "")
			{
				$EmailFile2 = "<a href='".base_url("download_general/ptl_download_evaluation_sheet/$JadwalID/$Emailfile_evaluation")."' title='Download File : $Emailfile_project'>Download</a>";
			}
			$hasil = $this->m_jadwal->PTL_insert_file($JadwalID,$gambar,$gambar);
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$EmailDosen1 = "";
			$EmailDosen2 = "";
			$EmailDosen3 = "";
			$EmailDosen4 = "";
			$EmailDosen5 = "";
			$EmailDosen6 = "";
			$DosenID1 = $resjadwal['DosenID'];
			if($DosenID1 != "")
			{
				$DosenID = $DosenID1;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen1 = $resdosen["email"];
				}
			}
			$DosenID2 = $resjadwal['DosenID2'];
			if($DosenID2 != "")
			{
				$DosenID = $DosenID2;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen2 = $resdosen["email"];
				}
			}
			$DosenID3 = $resjadwal['DosenID3'];
			if($DosenID3 != "")
			{
				$DosenID = $DosenID3;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen3 = $resdosen["email"];
				}
			}
			$DosenID4 = $resjadwal['DosenID4'];
			if($DosenID4 != "")
			{
				$DosenID = $DosenID4;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen4 = $resdosen["email"];
				}
			}
			$DosenID5 = $resjadwal['DosenID5'];
			if($DosenID5 != "")
			{
				$DosenID = $DosenID5;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen5 = $resdosen["email"];
				}
			}
			$DosenID6 = $resjadwal['DosenID6'];
			if($DosenID6 != "")
			{
				$DosenID = $DosenID6;
				$resdosen = $this->m_akun->PTL_select_dosen($DosenID);
				if($resdosen)
				{
					$EmailDosen6 = $resdosen["email"];
				}
			}
			$SubjekID = $resjadwal['SubjekID'];
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$NamaSubjek = "";
			if($ressubjek)
			{
				$NamaSubjek = $ressubjek["Nama"];
			}
			$ProgramID = $resjadwal['ProgramID'];
			if($ProgramID == "INT")
			{
				$TahunKe = "O";
			}
			else
			{
				$TahunKe = $resjadwal['TahunKe'];
			}
			$KelasID = $resjadwal['KelasID'];
			$NamaKelas = "";
			if($KelasID == 0)
			{
				$NamaKelas = "(MERGE)";
			}
			else
			{
				$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
				if($reskelas)
				{
					$NamaKelas = $reskelas["Nama"];
				}
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			// $this->email->to("$EmailDosen1,$EmailDosen2,$EmailDosen3,$EmailDosen4,$EmailDosen5,$EmailDosen6");
			$this->email->bcc("lendra.permana@gmail.com");
			$this->email->subject('PROJECT SHEET OR EVALUATION SHEET FILE WAS UPLOADED (NO REPLY)');
			$message = "
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				<br/>
				Dear ".strtoupper($NamaSubjek)." teacher,
				<br/>
				<br/>
				Your project sheet or evaluation sheet file was uploaded.
				<br/>
				<br/>
				Detail
				<br/>
				<table>
					<tr>
						<td>Subject</td>
						<td>:</td>
						<td>".strtoupper($NamaSubjek)."</td>
					</tr>
					<tr>
						<td>Class</td>
						<td>:</td>
						<td>$TahunKe".strtoupper($NamaKelas)."</td>
					</tr>
					<tr>
						<td>Project Sheet</td>
						<td>:</td>
						<td>$EmailFile1</td>
					</tr>
					<tr>
						<td>Evaluation Sheet</td>
						<td>:</td>
						<td>$EmailFile2</td>
					</tr>
				</table>
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			";
			$this->email->message($message);
			if($this->email->send())
			{
				echo warning("Your file was uploaded.","../attendance/ptl_list/$JadwalID");
			}
			else
			{
				echo warning("Your file was uploaded. Email server is not active.","../attendance/ptl_list/$JadwalID");
			}
		}
		
		function ptl_download_project_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/project_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../attendance/ptl_list/$JadwalID");
			}
		}
		
		function ptl_download_evaluation_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/evaluation_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../attendance/ptl_list/$JadwalID");
			}
		}
	}
?>