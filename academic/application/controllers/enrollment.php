<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Enrollment extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_akses');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-01",
							'aktifitas' => "Filter halaman Enrollment - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('enr_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('enr_filter_jur');
			}
			redirect("enrollment");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			$datalog = array(
							'pk1' => $cekprodi,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-02",
							'aktifitas' => "Filter halaman Enrollment - Prodi: $cekprodi.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi != "")
			{
				$this->session->set_userdata('enr_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('enr_filter_prodi');
			}
			redirect("enrollment");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-03",
							'aktifitas' => "Filter halaman Enrollment - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('enr_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('enr_filter_tahun');
			}
			redirect("enrollment");
		}
		
		function ptl_filter_tingkat()
		{
			$this->authentification();
			$cektingkat = $this->input->post('cektingkat');
			$datalog = array(
							'pk1' => $cektingkat,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-04",
							'aktifitas' => "Filter halaman Enrollment - Level: $cektingkat.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektingkat != "")
			{
				$this->session->set_userdata('enr_filter_tingkat',$cektingkat);
			}
			else
			{
				$this->session->unset_userdata('enr_filter_tingkat');
			}
			redirect("enrollment");
		}
		
		function ptl_set_check_all()
		{
			$this->authentification();
			$this->session->set_userdata('enroll_set_all','Y');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-04",
							'aktifitas' => "Filter halaman Enrollment - Check All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("enrollment");
		}
		
		function ptl_set_uncheck_all()
		{
			$this->authentification();
			$this->session->unset_userdata('enroll_set_all');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL01-05",
							'aktifitas' => "Filter halaman Enrollment - Uncheck All.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("enrollment");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','enrollment');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL02",
							'aktifitas' => "Mengakses halaman Enrollment.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('enr_filter_jur');
			$cekprodi = $this->session->userdata('enr_filter_prodi');
			$cektahun = $this->session->userdata('enr_filter_tahun');
			$cektingkat = $this->session->userdata('enr_filter_tingkat');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik_aktif($cekjurusan);
			$data['rowprodi'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi'] = $this->m_kursussingkat->PTL_all();
			}
			$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year($cekjurusan,$cekprodi,$cektahun,$cektingkat);
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Enrollment/v_enrollment',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_enroll()
		{
			$this->authentification();
			$total = $this->input->post('total') - 1;
			$CekKelasID = "";
			for($i=1;$i<=$total;$i++)
			{
				$KHSID = $this->input->post("check$i");
				if($KHSID != "")
				{
					$KelasID = $this->input->post("KelasID$i");
					if(($KelasID == 0) OR ($KelasID == ""))
					{
						$MhswID = $this->input->post("MhswID$i");
						$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
						$Nama = '';
						if($resmhsw)
						{
							$Nama = $resmhsw['Nama'];
						}
						$CekKelasID .= 'Class for '.$MhswID.' - '.$Nama.' not set.\n';
					}
				}
			}
			if($CekKelasID != "")
			{
				echo warning('You do not save any data.\n\n'.$CekKelasID,"../enrollment");
			}
			else
			{
				$nmhsw = 0;
				$nsave = 0;
				$nosave = 0;
				$totalsave = '';
				$notsave = '';
				$Email = '';
				$Email2 = '';
				for($i=1;$i<=$total;$i++)
				{
					$KHSID = $this->input->post("check$i");
					if($KHSID != "")
					{
						$nmhsw++;
						$reskhs = $this->m_khs->PTL_select($KHSID);
						$lang = "";
						$spec = "";
						if($reskhs)
						{
							if($reskhs['languageID'] == 'ENG')
							{
								$lang = "FRENCH";
							}
							if($reskhs['languageID'] == 'FRE')
							{
								$lang = "ENGLISH";
							}
							$spec = $reskhs['SpesialisasiID'];
						}
						$MhswID = $this->input->post("MhswID$i");
						$TahunID = $this->input->post("TahunID$i");
						$ProdiID = $this->input->post("ProdiID$i");
						$ProgramID = $this->input->post("ProgramID$i");
						$TahunKe = $this->input->post("TahunKe$i");
						$resemail = $this->m_mahasiswa->PTL_select($MhswID);
						if($resemail)
						{
							$Email .= $resemail['Email'].',';
							$Email2 .= $resemail['Email2'].',';
						}
						$rowjadwal = $this->m_jadwal->PTL_all_enroll($TahunID,$ProdiID,$ProgramID,$TahunKe);
						if($rowjadwal)
						{
							foreach($rowjadwal as $rj)
							{
								$KelasID = $this->input->post("KelasID$i");
								$SubjekID = $rj->SubjekID;
								$ressub = $this->m_subjek->PTL_select($SubjekID);
								$namasub = "";
								if($ressub)
								{
									$wordsub = explode(" ",$ressub['Nama']);
									$namasub = $wordsub[0];
								}
								if(($rj->TahunID == $TahunID) AND ($rj->ProgramID == $ProgramID) AND ($rj->TahunKe == $TahunKe))
								{
									if($rj->Gabungan == "N")
									{
										if($rj->KelasID == $KelasID)
										{
											if($lang != "")
											{
												if($namasub != $lang)
												{
													if($rj->SpesialisasiID == 0)
													{
														$data = array(
																	'KHSID' => $KHSID,
																	'MhswID' => $this->input->post("MhswID$i"),
																	'TahunID' => $this->input->post("TahunID$i"),
																	'JadwalID' => $rj->JadwalID,
																	'SubjekID' => $rj->SubjekID,
																	'KodeID' => 'ES01',
																	'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																	'tanggal_buat' => $this->waktu
																	);
														$this->m_krs->PTL_insert($data);
														$totalsave .=  $rj->JadwalID." A - ; $namasub ; $lang";
														$nsave++;
													}
													else
													{
														if($rj->SpesialisasiID == $spec)
														{
															$data = array(
																		'KHSID' => $KHSID,
																		'MhswID' => $this->input->post("MhswID$i"),
																		'TahunID' => $this->input->post("TahunID$i"),
																		'JadwalID' => $rj->JadwalID,
																		'SubjekID' => $rj->SubjekID,
																		'KodeID' => 'ES01',
																		'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																		'tanggal_buat' => $this->waktu
																		);
															$this->m_krs->PTL_insert($data);
															$totalsave .=  $rj->JadwalID." B - ";
															$nsave++;
														}
														else
														{
															$notsave .= $rj->JadwalID." AA - ";
															$nosave++;
														}
													}
												}
												else
												{
													$notsave .= $rj->JadwalID." BB - ";
													$nosave++;
												}
											}
											else
											{
												if($rj->SpesialisasiID == 0)
												{
													$data = array(
																'KHSID' => $KHSID,
																'MhswID' => $this->input->post("MhswID$i"),
																'TahunID' => $this->input->post("TahunID$i"),
																'JadwalID' => $rj->JadwalID,
																'SubjekID' => $rj->SubjekID,
																'KodeID' => 'ES01',
																'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																'tanggal_buat' => $this->waktu
																);
													$this->m_krs->PTL_insert($data);
													$totalsave .=  $rj->JadwalID." C - ";
													$nsave++;
												}
												else
												{
													if($rj->SpesialisasiID == $spec)
													{
														$data = array(
																	'KHSID' => $KHSID,
																	'MhswID' => $this->input->post("MhswID$i"),
																	'TahunID' => $this->input->post("TahunID$i"),
																	'JadwalID' => $rj->JadwalID,
																	'SubjekID' => $rj->SubjekID,
																	'KodeID' => 'ES01',
																	'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																	'tanggal_buat' => $this->waktu
																	);
														$this->m_krs->PTL_insert($data);
														$totalsave .=  $rj->JadwalID." D - ";
														$nsave++;
													}
													else
													{
														$notsave .= $rj->JadwalID." CC - ";
														$nosave++;
													}
												}
											}
										}
									}
									if($rj->Gabungan == "Y")
									{
										if(strpos($rj->KelasIDGabungan,$KelasID) !== false)
										{
											if($lang != "")
											{
												if($namasub != $lang)
												{
													if($rj->SpesialisasiID == 0)
													{
														$data = array(
																	'KHSID' => $KHSID,
																	'MhswID' => $this->input->post("MhswID$i"),
																	'TahunID' => $this->input->post("TahunID$i"),
																	'JadwalID' => $rj->JadwalID,
																	'SubjekID' => $rj->SubjekID,
																	'KodeID' => 'ES01',
																	'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																	'tanggal_buat' => $this->waktu
																	);
														$this->m_krs->PTL_insert($data);
														$totalsave .=  $rj->JadwalID." E - ";
														$nsave++;
													}
													else
													{
														if($rj->SpesialisasiID == $spec)
														{
															$data = array(
																		'KHSID' => $KHSID,
																		'MhswID' => $this->input->post("MhswID$i"),
																		'TahunID' => $this->input->post("TahunID$i"),
																		'JadwalID' => $rj->JadwalID,
																		'SubjekID' => $rj->SubjekID,
																		'KodeID' => 'ES01',
																		'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																		'tanggal_buat' => $this->waktu
																		);
															$this->m_krs->PTL_insert($data);
															$totalsave .=  $rj->JadwalID." F - ";
															$nsave++;
														}
														else
														{
															$notsave .= $rj->JadwalID." DD - ";
															$nosave++;
														}
													}
												}
												else
												{
													$notsave .= $rj->JadwalID." EE - ";
													$nosave++;
												}
											}
											else
											{
												if($rj->SpesialisasiID == 0)
												{
													$data = array(
																'KHSID' => $KHSID,
																'MhswID' => $this->input->post("MhswID$i"),
																'TahunID' => $this->input->post("TahunID$i"),
																'JadwalID' => $rj->JadwalID,
																'SubjekID' => $rj->SubjekID,
																'KodeID' => 'ES01',
																'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																'tanggal_buat' => $this->waktu
																);
													$this->m_krs->PTL_insert($data);
													$totalsave .=  $rj->JadwalID." G - ";
													$nsave++;
												}
												else
												{
													if($rj->SpesialisasiID == $spec)
													{
														$data = array(
																	'KHSID' => $KHSID,
																	'MhswID' => $this->input->post("MhswID$i"),
																	'TahunID' => $this->input->post("TahunID$i"),
																	'JadwalID' => $rj->JadwalID,
																	'SubjekID' => $rj->SubjekID,
																	'KodeID' => 'ES01',
																	'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
																	'tanggal_buat' => $this->waktu
																	);
														$this->m_krs->PTL_insert($data);
														$totalsave .=  $rj->JadwalID." H - ";
														$nsave++;
													}
													else
													{
														$notsave .= $rj->JadwalID." FF - ";
														$nosave++;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if($nsave == 0)
				{
					$datalog = array(
									'id_akun' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ENROL03-N",
									'aktifitas' => "Mengakses halaman Enrollment - Enrollment.",
									'data' => "no data",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					echo warning("You don't save any data.","../enrollment");
				}
				else
				{
					$datalog = array(
									'id_akun' => $_COOKIE["id_akun"],
									'nama' => $_COOKIE["nama"],
									'aplikasi' => "ACADEMIC",
									'menu' => $this->uri->segment(1),
									'submenu' => $this->uri->segment(2),
									'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
									'kode_halaman' => "ENROL03-Y",
									'aktifitas' => "Mengakses halaman Enrollment - Enrollment.",
									'data' => "no data",
									'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
									'tanggal_buat' => $this->waktu
									);
					$this->m_aktifitas->PTL_insert($datalog);
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to("$Email");
					$this->email->cc("$Email2");
					$this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR SCHEDULE HAS BEEN SET IN THIS SEMESTER (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>ACADEMIC</h2></font>
						</center>
						<br/>
						Hi All,
						<br/>
						<br/>
						Your schedule have successfully set by the academic staff. If you want to see your schedule, please access your PORTAL LMS.
						<br/>
						URL: http://app.esmodjakarta.com/lms
						<br/>
						<br/>
						Code : $ProgramID - $ProdiID - $TahunID.
						<br/>
						<br/>
						Enrolled by $_COOKIE[nama].
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($_COOKIE["id_akun"] == "00001111")
					{
						echo warning("Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved. '$nosave' data KRS not saved.","../enrollment");
					}
					else
					{
						if($this->email->send())
						{
							echo warning("Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved. '$nosave' data KRS not saved.","../enrollment");
							// echo warning("Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved. '$nosave' data KRS not saved.","../enrollment/ptl_enroll_info_for_teachers/$ProgramID/$ProdiID/$TahunID");
						}
						else
						{
							echo warning("Email server is not active. Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved. '$nosave' data KRS not saved.","../enrollment");
							// echo warning("Email server is not active. Your data successfully saved. '$nmhsw' students saved. '$nsave' data KRS saved. '$nosave' data KRS not saved.","../enrollment/ptl_enroll_info_for_teachers/$ProgramID/$ProdiID/$TahunID");
						}
					}
				}
			}
		}
		
		// function ptl_enroll_info_for_teachers()
		// {
			// $this->authentification();
			// $ProgramID = $this->uri->segment(3);
			// $ProdiID = $this->uri->segment(4);
			// $TahunID = $this->uri->segment(5);
			// $rowakun = $this->m_akun->PTL_all_akademik();
			// $email = "";
			// if($rowakun)
			// {
				// foreach($rowakun as $ra)
				// {
					// $id_akun = $ra->id_akun;
					// $resakses = $this->m_akses->PTL_select_akun($id_akun);
					// if($resakses)
					// {
						// if($resakses['na'] == "N")
						// {
							// if($ra->email != "")
							// {
								// $email .= $ra->email.',';
							// }
						// }
					// }
				// }
			// }
			// $id_akun = $_COOKIE["id_akun"];
			// $resakun = $this->m_akun->PTL_select($id_akun);
			// $EmailAkun = "";
			// if($resakun)
			// {
				// $EmailAkun = $resakun['email'];
			// }
			
			// $ip_client = $this->log->getIpAdress();
			// $hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			// $os_client = $this->log->getOs();
			// $browser_client = $this->log->getBrowser();
			// $perangkat_client = $this->log->getPerangkat();
			
			// $this->load->library('email');
			// $config = array();
			// $config['charset'] = 'utf-8';
			// $config['useragent'] = 'Codeigniter';
			// $config['protocol']= "smtp";
			// $config['mailtype']= "html";
			// $config['smtp_host']= "mail.esmodjakarta.com";
			// $config['smtp_port']= "25";
			// $config['smtp_timeout']= "5";
			// $config['smtp_user']= "no-reply@esmodjakarta.com";
			// $config['smtp_pass']= "noreplyesmod";
			// $config['crlf']="\r\n"; 
			// $config['newline']="\r\n"; 
			// $config['wordwrap'] = TRUE;
			// $this->email->initialize($config);
			// $this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			// $this->email->to("$email");
			// $this->email->cc("$EmailAkun");
			// $this->email->bcc('lendra.permana@gmail.com');
			// $this->email->subject('YOUR STUDENTS IN THIS SEMESTER (NO REPLY)');
			// $this->email->message("
				// <center>
					// <img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					// <font color='red'><h2>ACADEMIC</h2></font>
				// </center>
				// <br/>
				// Hi All Teacher,
				// <br/>
				// <br/>
				// Your students has successfully registered by the academic staff in this semester. If you want to see your students, please access your PORTAL ACADEMIC.
				// <br/>
				// URL: http://app.esmodjakarta.com/academic
				// <br/>
				// <br/>
				// Code : $ProgramID - $ProdiID - $TahunID.
				// <br/>
				// <br/>
				// Enrolled by $_COOKIE[nama].
				// <br/>
				// <br/>
				// <br/>
				// Thanks,
				// <br/>
				// <img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				// <center>
					// <b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					// Ip: $ip_client
					// <br/>
					// Hostname: $hostname_client
					// <br/>
					// OS: $os_client
					// <br/>
					// Browser: $browser_client
					// <br/>
					// Devices: $perangkat_client
				// </center>
			// ");
			// if($this->email->send())
			// {
				// echo warning("Notification to all teacher has been sent.","../enrollment");
			// }
			// else
			// {
				// echo warning("Email server is not active. Notification to all teacher has been sent.","../enrollment");
			// }
		// }
		
		function ptl_set_class()
		{
			$this->authentification();
			$KHSID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$Sesi = $this->uri->segment(5) - 1;
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL04",
							'aktifitas' => "Mengakses halaman Enrollment - Set Class.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$Nama = '';
			$Email = '';
			$Email2 = '';
			if($res)
			{
				$Nama = $res['Nama'];
				$Email = $res['Email'];
				$Email2 = $res['Email2'];
			}
			$result = $this->m_khs->PTL_select_class_before($MhswID,$Sesi);
			$TahunKe = 0;
			$KelasID = 0;
			if($result)
			{
				if($result['ProgramID'] == "INT")
				{
					$TahunKe = "O";
				}
				else
				{
					$TahunKe = $result['TahunKe'];
				}
				$KelasID = $result['KelasID'];
			}
			$data = array(
						'KelasID' => $KelasID,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_khs->PTL_update($KHSID,$data);
			$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
			$NamaKelas = '';
			if($reskelas)
			{
				$NamaKelas = $reskelas['Nama'];
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR CLASS HAS BEEN SET (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi ".strtoupper($Nama).",
				<br/>
				<br/>
				Your class is <font color='green'><b>$TahunKe$NamaKelas</b></font> in semester ".$this->uri->segment(5).".
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data has been saved.","../enrollment");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data has been saved.","../enrollment");
				}
				else
				{
					echo warning("Email server is not active. Your data has been saved.","../enrollment");
				}
			}
		}
		
		function ptl_automatic_set_class()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "ENROL05",
							'aktifitas' => "Mengakses halaman Enrollment - Automatic Set Class.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('enr_filter_jur');
			$cekprodi = $this->session->userdata('enr_filter_prodi');
			$cektahun = $this->session->userdata('enr_filter_tahun');
			$rowrecord = $this->m_khs->PTL_all_spesifik_aktif($cekjurusan,$cekprodi,$cektahun);
			$KHSID = '';
			$MhswID = '';
			$Sesi = '';
			$no = 0;
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					if($row->KelasID == 0)
					{
						if(($row->Sesi == '2') OR ($row->Sesi == '4') OR ($row->Sesi == '6'))
						{
							if($no < 1)
							{
								$KHSID = $row->KHSID;
								$MhswID = $row->MhswID;
								$Sesi = $row->Sesi;
								$no++;
							}
						}
					}
				}
			}
			if($no > 0)
			{
				redirect("enrollment/ptl_automatic_set_class_set/$KHSID/$MhswID/$Sesi");
			}
			else
			{
				echo warning("Your data has been saved.","../enrollment");
			}
		}
		
		function ptl_automatic_set_class_set()
		{
			$this->authentification();
			$KHSID = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$Sesi = $this->uri->segment(5) - 1;
			$res = $this->m_mahasiswa->PTL_select($MhswID);
			$Nama = '';
			$Email = '';
			$Email2 = '';
			if($res)
			{
				$Nama = $res['Nama'];
				$Email = $res['Email'];
				$Email2 = $res['Email2'];
			}
			$result = $this->m_khs->PTL_select_class_before($MhswID,$Sesi);
			$TahunKe = 0;
			$KelasID = 0;
			if($result)
			{
				$ProgramID = $result['ProgramID'];
				if($ProgramID == "INT")
				{
					$TahunKe = "O";
				}
				else
				{
					$TahunKe = $result['TahunKe'];
				}
				$KelasID = $result['KelasID'];
			}
			$data = array(
						'KelasID' => $KelasID,
						'TahunKe' => $TahunKe,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_khs->PTL_update($KHSID,$data);
			$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
			$NamaKelas = '';
			if($reskelas)
			{
				$NamaKelas = $reskelas['Nama'];
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR CLASS HAS BEEN SET (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi ".strtoupper($Nama).",
				<br/>
				<br/>
				Your class is <font color='green'><b>$TahunKe$NamaKelas</b></font> in semester ".$this->uri->segment(5).".
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				redirect("enrollment/ptl_automatic_set_class");
			}
			else
			{
				if($this->email->send())
				{
					redirect("enrollment/ptl_automatic_set_class");
				}
				else
				{
					echo warning("Email server is not active. Your data has been saved.","../enrollment/ptl_automatic_set_class");
				}
			}
		}
		
		function ptl_reset()
		{
			$this->authentification();
			$KHSID = $this->uri->segment(3);
			$this->m_krs->PTL_delete_khs($KHSID);
			echo warning("KRS data has been deleted.","../enrollment");
		}
	}
?>