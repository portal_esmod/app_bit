<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Evaluation extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('PHPExcel');
			$this->load->model('m_aktifitas');
			$this->load->model('m_dosen');
			$this->load->model('m_exam');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_jenis_mk');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_krs2');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_mk');
			$this->load->model('m_nilai');
			$this->load->model('m_nilai3');
			$this->load->model('m_predikat');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_remedial_krs');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$ProgramID = $this->session->userdata('eval_filter_jur');
			$TahunID = $this->session->userdata('eval_filter_tahun');
			$StatusMhswID = $this->session->userdata('eval_filter_status');
			$data['response'] = 'false';
			$query = $this->m_mahasiswa->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$MhswID = $row->MhswID;
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					$Status = "";
					if($resmhsw)
					{
						$Status = $resmhsw["StatusMhswID"];
					}
					if($StatusMhswID == "")
					{
						$result = $this->m_khs->PTL_select_program_tahun($MhswID,$ProgramID,$TahunID,$StatusMhswID);
						if($result)
						{
							$data['message'][] = array(
													'id'=> $row->MhswID,
													'value' => $row->MhswID." - ".$row->Nama,
													''
													);
						}
					}
					else
					{
						if($Status == $StatusMhswID)
						{
							$result = $this->m_khs->PTL_select_program_tahun($MhswID,$ProgramID,$TahunID,$StatusMhswID);
							if($result)
							{
								$data['message'][] = array(
														'id'=> $row->MhswID,
														'value' => $row->MhswID." - ".$row->Nama,
														''
														);
							}
						}
					}
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('evaluation/index',$data);
			}
		}
		
		function ptl_cari()
		{
			$this->authentification();
			if($this->input->post('cari') != "")
			{
				$cari = $this->input->post('cari');
			}
			else
			{
				$cari = $this->uri->segment(3);
			}
			if($cari != "")
			{
				$this->session->set_userdata('eval_filter_mahasiswa',$cari);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_mahasiswa');
			}
			$datalog = array(
							'pk1' => $cari,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU01-01",
							'aktifitas' => "Filter halaman Evaluation - SIN: $cari.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("evaluation");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU01-02",
							'aktifitas' => "Filter halaman Evaluation - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('eval_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_jur');
			}
			$this->session->unset_userdata('eval_filter_mahasiswa');
			redirect("evaluation");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU01-03",
							'aktifitas' => "Filter halaman Evaluation - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('eval_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_tahun');
			}
			$this->session->unset_userdata('eval_filter_mahasiswa');
			redirect("evaluation");
		}
		
		function ptl_filter_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			$datalog = array(
							'pk1' => $cekstatus,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU01-04",
							'aktifitas' => "Filter halaman Evaluation - Status: $cekstatus.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekstatus != "")
			{
				$this->session->set_userdata('eval_filter_status',$cekstatus);
			}
			else
			{
				$this->session->unset_userdata('eval_filter_status');
			}
			redirect("evaluation");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','evaluation');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU02",
							'aktifitas' => "Filter halaman Evaluation.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['id'] = "mahasiswa";
			$data['controller'] = "evaluation/lookup";
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$rowrecord = $this->m_khs->PTL_all_select($MhswID);
			if($rowrecord)
			{
				foreach($rowrecord as $row)
				{
					$ProgramID = $row->ProgramID;
					$MhswID = $row->MhswID;
					$KHSID = $row->KHSID;
					$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
					if($rowkrs)
					{
						foreach($rowkrs as $rkr)
						{
							$TahunID = $rkr->TahunID;
							$JadwalID = $rkr->JadwalID;
							$SubjekID = $rkr->SubjekID;
							$KRSID = $rkr->KRSID;
							$rowsub = $this->m_subjek->PTL_select($SubjekID);
							$KurikulumID = "";
							$JenisMKID = "";
							if($rowsub)
							{
								$KurikulumID = $rowsub['KurikulumID'];
								$JenisMKID = $rowsub['JenisMKID'];
							}
							$rowmk = $this->m_mk->PTL_all_select($SubjekID);
							if($rowmk)
							{
								$NilAkhir = 0;
								$NilBagi = 0;
								foreach($rowmk as $rm)
								{
									if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 21) OR ($TahunID == 22) OR ($TahunID == 28) OR ($TahunID == 29) OR ($TahunID == 30) OR ($TahunID == 31))
									{
										if(($JenisMKID == "5") OR ($JenisMKID == "25"))
										{
											$PresensiID = "";
											$CekJenisPresensiID = "";
										}
										$MKID = $rm->MKID;
										$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
										$Nilai = 0;
										$AddNilai = 0;
										if($rowkrs2)
										{
											$totNilai = 0;
											$nkrs2 = 0;
											foreach($rowkrs2 as $rkrs2)
											{
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													$PresensiID = $rkrs2->PresensiID;
													$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
													if($resscoring)
													{
														$CekJenisPresensiID = $resscoring['JenisPresensiID'];
													}
												}
												if($rkrs2->Nilai > 0)
												{
													$totNilai = $totNilai + $rkrs2->Nilai;
													$nkrs2++;
												}
												$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
											}
											if($nkrs2 > 0)
											{
												$Nilai = $totNilai / $nkrs2;
											}
										}
										$tn = $Nilai + $AddNilai;
										if($rowkrs2)
										{
											if($rm->Optional == "Y")
											{
												if($tn != 0)
												{
													$NilAkhir = $NilAkhir + $tn;
													$NilBagi++;
												}
											}
											else
											{
												$NilAkhir = $NilAkhir + $tn;
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
													{
														
													}
													else
													{
														$NilBagi++;
													}
												}
												else
												{
													$NilBagi++;
												}
											}
										}
									}
									else
									{
										if(($JenisMKID == "5") OR ($JenisMKID == "25"))
										{
											$PresensiID = "";
											$CekJenisPresensiID = "";
										}
										$CekMKID = $rm->MKID;
										$rescekmk = $this->m_presensi->PTL_select_cek_mk_scoring($JadwalID,$CekMKID);
										if($rescekmk)
										{
											$MKID = $rm->MKID;
											$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
											$Nilai = 0;
											$AddNilai = 0;
											if($rowkrs2)
											{
												$totNilai = 0;
												$nkrs2 = 0;
												foreach($rowkrs2 as $rkrs2)
												{
													if(($JenisMKID == "5") OR ($JenisMKID == "25"))
													{
														$PresensiID = $rkrs2->PresensiID;
														$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
														if($resscoring)
														{
															$CekJenisPresensiID = $resscoring['JenisPresensiID'];
														}
													}
													if($rkrs2->Nilai > 0)
													{
														$totNilai = $totNilai + $rkrs2->Nilai;
														$nkrs2++;
													}
													$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
												}
												if($nkrs2 > 0)
												{
													$Nilai = $totNilai / $nkrs2;
												}
											}
											$tn = $Nilai + $AddNilai;
											if($rowkrs2)
											{
												if($rm->Optional == "Y")
												{
													if($tn != 0)
													{
														$NilAkhir = $NilAkhir + $tn;
														$NilBagi++;
													}
												}
												else
												{
													$NilAkhir = $NilAkhir + $tn;
													if(($JenisMKID == "5") OR ($JenisMKID == "25"))
													{
														if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
														{
															
														}
														else
														{
															$NilBagi++;
														}
													}
													else
													{
														$NilBagi++;
													}
												}
											}
										}
									}
								}
								$mp = 0;
								if($NilBagi != 0)
								{
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$rowscore = $this->m_presensi_mahasiswa->PTL_all_sum($KRSID,$MhswID);
										if($rowscore)
										{
											$JMP = 0;
											$TMP = 0;
											foreach($rowscore as $rs)
											{
												$JMP = $JMP + $rs->Score;
												$TMP++;
											}
											if($JMP > 0)
											{
												$mp = ($JMP / $TMP);
											}
										}
									}
									else
									{
										$JenisMKID = 4;
										$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
										if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
										$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
										if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
										$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
										if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
										$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
										if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
										$JenisPresensiID = "E";
										$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
										$tottexc = $texc * $pr1;
										$JenisPresensiID = "S";
										$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
										$tottsic = $tsic * $pr2;
										$JenisPresensiID = "A";
										$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
										$tottabs = $tabs * $pr3;
										$JenisPresensiID = "L";
										$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
										$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
										$tottlat = $tlat * $pr4;
										$mp = $tottexc + $tottsic + $tottabs + $tottlat;
									}
									$exam = 0;
									$ExamID = "";
									$TotJum = "";
									$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
									if($rexmhsw1)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw1 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA1)
											{
												if($rexmhswA1['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA1['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
									if($rexmhsw2)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw2 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA2)
											{
												if($rexmhswA2['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA2['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
									if($rexmhsw3)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw3 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA3)
											{
												if($rexmhswA3['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA3['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
									if($rexmhsw4)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw4 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA4)
											{
												if($rexmhswA4['Nilai'] > 0)
												{
													$jumexam = $jumexam + $rexmhswA4['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
									if($rexmhsw5)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw5 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA5)
											{
												if($rexmhswA5['Nilai'] > 0)
												{
													$exam = $exam + $rexmhswA5['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
									if($rexmhsw6)
									{
										$jum = 0;
										$jumexam = 0;
										foreach($rexmhsw6 as $row)
										{
											$ExamID = $row->ExamID;
											$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
											if($rexmhswA6)
											{
												if($rexmhswA6['Nilai'] > 0)
												{
													$exam = $exam + $rexmhswA6['Nilai'];
													$jum++;
													$TotJum++;
												}
											}
										}
										if($jum > 0)
										{
											$exam = $exam + ($jumexam / $jum);
										}
									}
									if($TotJum > 1)
									{
										$exam = $exam;
										// $exam = $exam / $TotJum;
									}
									if($exam > 0)
									{
										if(($JenisMKID == "5") OR ($JenisMKID == "25"))
										{
											$gradevalueAkhir = (($NilAkhir / $NilBagi) + $exam) / 2;
										}
										else
										{
											$gradevalueAkhir = ((($NilAkhir / $NilBagi) + $mp) + $exam) / 2;
										}
									}
									else
									{
										if(($JenisMKID == "5") OR ($JenisMKID == "25"))
										{
											$gradevalueAkhir = ($NilAkhir / $NilBagi);
										}
										else
										{
											$gradevalueAkhir = ($NilAkhir / $NilBagi) + $mp;
										}
									}
									$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
									$remedial = 0;
									if($resremedial)
									{
										$remedial = $resremedial['Nilai'];
									}
									if($remedial > 0)
									{
										$gradevalueAkhir = ($gradevalueAkhir + $remedial) / 2;
									}
									$NilaiAkhir = ($NilAkhir / $NilBagi);
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$gradevalue = ($NilAkhir / $NilBagi);
									}
									else
									{
										$gradevalue = ($NilAkhir / $NilBagi) + $mp;
									}
									$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
									$GradeNilai = "";
									if($rownilai)
									{
										foreach($rownilai as $rn)
										{
											if((number_format($gradevalueAkhir,2) >= $rn->NilaiMin) AND (number_format($gradevalueAkhir,2) <= $rn->NilaiMax))
											{
												$GradeNilai = $rn->Nama;
											}
										}
									}
									$data_krs = array(
												'NilaiAkhir' => $NilaiAkhir,
												// 'gradevalue' => $gradevalue,
												'gradevalue' => $gradevalueAkhir,
												'GradeNilai' => $GradeNilai
												);
									$this->m_krs->PTL_update_evaluation($KRSID,$data_krs);
								}
								else
								{
									if($NilBagi == 0)
									{
										$data_krs = array(
													'NilaiAkhir' => 0,
													'gradevalue' => '',
													'GradeNilai' => ''
													);
										$this->m_krs->PTL_update_evaluation($KRSID,$data_krs);
									}
								}
							}
						}
					}
				}
			}
			$cekjurusan = $this->session->userdata('eval_filter_jur');
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$data['status'] = $this->m_status->PTL_all_data();
			$data['rowrecord'] = $this->m_khs->PTL_all_select($MhswID);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Evaluation/v_evaluation',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_pdf()
		{
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$MhswID = $word[0];
			$datalog = array(
							'pk1' => $TahunID,
							'pk2' => $MhswID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVALU03",
							'aktifitas' => "Filter halaman Evaluation - PDF.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['MhswID'] = $MhswID;
			$result = $this->m_khs->PTL_all_select($MhswID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$tahun = 0;
			$kumulatif = 0;
			$kumulatifsks = 0;
			$kumulatifipk = 0;
			$ipk = 0;
			foreach($result as $r)
			{
				// START HEADER
				$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.2,5.7,0.7);
				$this->fpdf->Ln(0.2);
				$this->fpdf->SetFont("helvetica","",7);
				$this->fpdf->Cell(19,1,"Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan",0,0,"L");
				$this->fpdf->Ln(0.6);
				$this->fpdf->Cell(5,0.5,"Telp. (021) 7659181, Fax. (021) 7657517",0,0,"L");
				$this->fpdf->SetFont("Times","B",12);
				$this->fpdf->Cell(9,0.7,"STUDENT EVALUATION RESULT",0,0,"C");
				$this->fpdf->Ln();
				$id_akun = $_COOKIE["id_akun"];
				$nama = $_COOKIE["nama"];
				$this->fpdf->SetFont("helvetica","",10);
				$this->fpdf->Line(1,2.5,20,2.5);
				$this->fpdf->Line(1,2.55,20,2.55);
				
				$TahunID = $r->TahunID;
				$restahun = $this->m_year->PTL_select($TahunID);
				$NamaTahun = "";
				if($restahun)
				{
					$NamaTahun = $restahun['Nama'];
				}
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				$reskhs = $this->m_khs->PTL_all_select($MhswID);
				$kelas1 = "";
				$kelas2 = "";
				$kelas3 = "";
				if($reskhs)
				{
					foreach($reskhs as $rk)
					{
						$KelasID = $rk->KelasID;
						$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
						if($rk->TahunKe == 1)
						{
							$kelas1 = $rk->TahunKe.@$reskelas["Nama"];
						}
						if($rk->TahunKe == 2)
						{
							$kelas2 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
						if($rk->TahunKe == 3)
						{
							$kelas3 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
					}
				}
				$ProgramID = $resmhsw["ProgramID"];
				$resprog = $this->m_program->PTL_select($ProgramID);
				$ProdiID = $resmhsw["ProdiID"];
				$resprod = $this->m_prodi->PTL_select($ProdiID);
				if($resmhsw["Foto"] == "")
				{
					$foto = "foto_umum/user.jpg";
				}
				else
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
					if($exist)
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					}
					else
					{
						$foto = "foto_umum/user.jpg";
					}
				}
				$this->fpdf->Ln(0.1);
				$this->fpdf->SetFont("Times","B",9);
				$this->fpdf->Cell(3, 0.5, "NAME", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": $resmhsw[Nama]", "", 0, "L");
				$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX()+9,$this->fpdf->getY(),3,4);
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "STUDENT ID", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": $MhswID", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "PROGRAM", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": ".strtoupper($resprog["Nama"]), "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "MAJOR", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": ".strtoupper($resprod["Nama"]), "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "CLASS", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": $kelas1$kelas2$kelas3", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "ACADEMIC YEAR", "", 0, "L");
				$this->fpdf->Cell(4, 0.5, ": $NamaTahun", "", 0, "L");
				$this->fpdf->Ln(1.2);
				// END HEADER
				
				$kumulatif++;
				if($no == 1)
				{
					$tahun = $tahun + 1;
					$no++;
				}
				else
				{
					$tahun = $tahun;
					$no = 1;
				}
				$KelasID = $r->KelasID;
				$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
				if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
				$DosenID = $r->WaliKelasID;
				$rhr1 = $this->m_dosen->PTL_select($DosenID);
				if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
				$DosenID = $r->WaliKelasID2;
				$rhr2 = $this->m_dosen->PTL_select($DosenID);
				if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
				$SpesialisasiID = $r->SpesialisasiID;
				$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
				$thn = "";
				if($r->TahunKe == 1){ $thn = "st"; }
				if($r->TahunKe == 2){ $thn = "nd"; }
				if($r->TahunKe == 3){ $thn = "rd"; }
				if($r->ProgramID == "INT")
				{
					$TahunKe = "O";
				}
				else
				{
					$TahunKe = $r->TahunKe;
				}
				$smt = "";
				if($r->Sesi == 1){ $smt = "st"; }
				if($r->Sesi == 2){ $smt = "nd"; }
				if($r->Sesi == 3){ $smt = "rd"; }
				if($r->Sesi == 4){ $smt = "th"; }
				if($r->Sesi == 5){ $smt = "th"; }
				if($r->Sesi == 6){ $smt = "th"; }
				$this->fpdf->SetFont("Times","B",8);
				$this->fpdf->Ln();
				$this->fpdf->Cell(1.5, 1, "$TahunKe$thn Year", "LBT", 0, "C");
				$this->fpdf->Cell(4.5, 1, "$r->Sesi$smt Semester", "BT", 0, "C");
				$this->fpdf->Cell(2, 0.5, "Class", "T", 0, "C");
				$this->fpdf->Cell(3, 0.5, "Specialization", "T", 0, "C");
				$this->fpdf->Cell(4, 0.5, "Homeroom", "T", 0, "C");
				$this->fpdf->Cell(4, 0.5, "Vice Homeroom", "TR", 0, "C");
				$this->fpdf->Ln();
				$word1 = explode(" ",$hr1);
				$word2 = explode(" ",$hr2);
				$this->fpdf->Cell(6, 0.5, "", "", 0, "C");
				$this->fpdf->Cell(2, 0.5, $TahunKe.$kelas, "B", 0, "C");
				$this->fpdf->Cell(3, 0.5, $spe, "B", 0, "C");
				$this->fpdf->Cell(4, 0.5, $word1[0]." ".@$word1[1]." ".substr(@$word1[2],0,1), "B", 0, "C");
				$this->fpdf->Cell(4, 0.5, $word2[0]." ".@$word2[1]." ".substr(@$word2[2],0,1), "BR", 0, "C");
				$MhswID = $r->MhswID;
				$KHSID = $r->KHSID;
				$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
				$totalbobot = 0;
				$ips = 0;
				if($rowkrs)
				{
					$this->fpdf->SetFont("Times","",8);
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 1, "#", "LBTR", 0, "C");
					$this->fpdf->Cell(6, 1, "Subject", "BTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, "Score", "BTR", 0, "C");
					$this->fpdf->Cell(4, 0.5, "Attendance", "BTR", 0, "C");
					$this->fpdf->Cell(1, 1, "M/P", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "GV", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "EXM", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "RMD", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "GRD", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "CRD", "TR", 0, "C");
					$this->fpdf->Cell(1, 1, "GP", "TR", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(8, 0.5, "", "", 0, "C");
					$this->fpdf->Cell(1, 0.5, "EXC", "BR", 0, "C");
					$this->fpdf->Cell(1, 0.5, "SIC", "BR", 0, "C");
					$this->fpdf->Cell(1, 0.5, "ABS", "BR", 0, "C");
					$this->fpdf->Cell(1, 0.5, "LAT", "BR", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$this->fpdf->Cell(1, 0.5, "", "B", 0, "C");
					$ns = 1;
					$jumlahsks = 0;
					$jumlahexc = 0;
					$jumlahsic = 0;
					$jumlahabs = 0;
					$jumlahlat = 0;
					$jumlahmp = 0;
					$jumlahbobot = 0;
					foreach($rowkrs as $rkr)
					{
						$TahunID = $rkr->TahunID;
						$JadwalID = $rkr->JadwalID;
						$SubjekID = $rkr->SubjekID;
						$rsub = $this->m_subjek->PTL_select($SubjekID);
						$subjek = $rsub["Nama"];
						$sks = $rsub["SKS"];
						$jumlahsks = $jumlahsks + $rsub["SKS"];
						$KurikulumID = $rsub["KurikulumID"];
						$JenisMKID = $rsub["JenisMKID"];
						$GradeNilai = $rkr->GradeNilai;
						$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
						$bobot = 0;
						if($resbobot)
						{
							$bobot = $resbobot['Bobot'];
						}
						$KRSID = $rkr->KRSID;
						$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
						if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
						$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
						if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
						$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
						if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
						$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
						if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
						$tottexc = $texc;
						$jumlahexc = $jumlahexc + $tottexc;
						if($tottexc == 0){ $tottexc = ""; }
						$tottsic = $tsic;
						$jumlahsic = $jumlahsic + $tottsic;
						if($tottsic == 0){ $tottsic = ""; }
						$tottabs = $tabs;
						$jumlahabs = $jumlahabs + $tottabs;
						if($tottabs == 0){ $tottabs = ""; }
						$tottlat = $tlat;
						$jumlahlat = $jumlahlat + $tottlat;
						if($tottlat == 0){ $tottlat = ""; }
						$TahunID = $rkr->TahunID;
						
						$exam = 0;
						$ExamID = "";
						$ExamMhswID = "";
						$TotJum = "";
						$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
						if($rexmhsw1)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw1 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA1)
								{
									if($rexmhswA1['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA1['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
						if($rexmhsw2)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw2 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA2)
								{
									if($rexmhswA2['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA2['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
						if($rexmhsw3)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw3 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA3)
								{
									if($rexmhswA3['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA3['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
						if($rexmhsw4)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw4 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA4)
								{
									if($rexmhswA4['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA4['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
						if($rexmhsw5)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw5 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA5)
								{
									if($rexmhswA5['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA5['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
						if($rexmhsw6)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw6 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA6)
								{
									if($rexmhswA6['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA6['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						if($exam == 0)
						{
							$exam = "-";
						}
						else
						{
							if($TotJum > 1)
							{
								$exam = number_format(($exam),2,'.','');
								// $exam = number_format(($exam / $TotJum),2,'.','');
							}
						}
						$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
						$remedial = "-";
						if($resremedial)
						{
							$remedial = $resremedial['Nilai'];
						}
						
						if($rkr->gradevalue == "")
						{
							$mp = "";
						}
						else
						{
							$JenisPresensiID = "E";
							$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
							$mptottexc = $texc * $pr1;
							$JenisPresensiID = "S";
							$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
							$mptottsic = $tsic * $pr2;
							$JenisPresensiID = "A";
							$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
							$mptottabs = $tabs * $pr3;
							$JenisPresensiID = "L";
							$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
							$mptottlat = $tlat * $pr4;
							if(($JenisMKID == "5") OR ($JenisMKID == "25")) // MKID Special
							{
								$mp = "";
							}
							else
							{
								$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
							}
							if($mp == 0.00)
							{
								$mp = "";
							}
							$jumlahmp = $jumlahmp + $mp;
						}
						$jumlahbobot = $bobot * $sks;
						$totalbobot = $totalbobot + $jumlahbobot;
						$this->fpdf->Ln();
						$this->fpdf->Cell(0.5, 1, $ns, "LBTR", 0, "C");
						$this->fpdf->Cell(6, 1, substr($subjek,0,34), "BTR", 0, "L");
						$this->fpdf->Cell(1.5, 1, $rkr->NilaiAkhir, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $tottexc, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $tottsic, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $tottabs, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $tottlat, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $mp, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $rkr->gradevalue, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $exam, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $remedial, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $rkr->GradeNilai, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $sks, "BTR", 0, "C");
						$this->fpdf->Cell(1, 1, $jumlahbobot, "BTR", 0, "C");
						$rowmk = $this->m_mk->PTL_all_select($SubjekID);
						if($rowmk)
						{
							foreach($rowmk as $rm)
							{
								if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 21) OR ($TahunID == 22) OR ($TahunID == 28) OR ($TahunID == 29) OR ($TahunID == 30) OR ($TahunID == 31))
								{
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$PresensiID = "";
										$CekJenisPresensiID = "";
									}
									$MKID = $rm->MKID;
									$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
									$Nilai = 0;
									$AddNilai = 0;
									if($rowkrs2)
									{
										$totNilai = 0;
										$nkrs2 = 0;
										foreach($rowkrs2 as $rkrs2)
										{
											if(($JenisMKID == "5") OR ($JenisMKID == "25"))
											{
												$PresensiID = $rkrs2->PresensiID;
												$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
												if($resscoring)
												{
													$CekJenisPresensiID = $resscoring['JenisPresensiID'];
												}
											}
											if($rkrs2->Nilai > 0)
											{
												$totNilai = $totNilai + $rkrs2->Nilai;
												$nkrs2++;
											}
											$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
										}
										if($nkrs2 > 0)
										{
											$Nilai = $totNilai / $nkrs2;
										}
									}
									$tn = $Nilai + $AddNilai;
									if($rowkrs2)
									{
										if($rm->Optional == "Y")
										{
											if($tn != 0)
											{
												$this->fpdf->SetFont("Times","",7);
												$this->fpdf->Ln();
												$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
												$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
												$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
												$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
											}
										}
										else
										{
											if(($JenisMKID == "5") OR ($JenisMKID == "25"))
											{
												if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
												{
													$this->fpdf->SetFont("Times","",7);
													$this->fpdf->Ln();
													$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
													$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
													$this->fpdf->Cell(1.5, 0.5, "-", "R", 0, "C");
													$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
												}
												else
												{
													$this->fpdf->SetFont("Times","",7);
													$this->fpdf->Ln();
													$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
													$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
													$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
													$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
												}
											}
											else
											{
												$this->fpdf->SetFont("Times","",7);
												$this->fpdf->Ln();
												$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
												$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
												$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
												$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
											}
										}
									}
								}
								else
								{
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$PresensiID = "";
										$CekJenisPresensiID = "";
									}
									$CekMKID = $rm->MKID;
									$rescekmk = $this->m_presensi->PTL_select_cek_mk_scoring($JadwalID,$CekMKID);
									if($rescekmk)
									{
										$MKID = $rm->MKID;
										$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
										$Nilai = 0;
										$AddNilai = 0;
										if($rowkrs2)
										{
											$totNilai = 0;
											$nkrs2 = 0;
											foreach($rowkrs2 as $rkrs2)
											{
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													$PresensiID = $rkrs2->PresensiID;
													$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
													if($resscoring)
													{
														$CekJenisPresensiID = $resscoring['JenisPresensiID'];
													}
												}
												if($rkrs2->Nilai > 0)
												{
													$totNilai = $totNilai + $rkrs2->Nilai;
													$nkrs2++;
												}
												$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
											}
											if($nkrs2 > 0)
											{
												$Nilai = $totNilai / $nkrs2;
											}
										}
										$tn = $Nilai + $AddNilai;
										if($rowkrs2)
										{
											if($rm->Optional == "Y")
											{
												if($tn != 0)
												{
													$this->fpdf->SetFont("Times","",7);
													$this->fpdf->Ln();
													$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
													$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
													$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
													$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
												}
											}
											else
											{
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
													{
														$this->fpdf->SetFont("Times","",7);
														$this->fpdf->Ln();
														$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
														$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
														$this->fpdf->Cell(1.5, 0.5, "-", "R", 0, "C");
														$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
													}
													else
													{
														$this->fpdf->SetFont("Times","",7);
														$this->fpdf->Ln();
														$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
														$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
														$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
														$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
													}
												}
												else
												{
													$this->fpdf->SetFont("Times","",7);
													$this->fpdf->Ln();
													$this->fpdf->Cell(0.5, 0.5, "", "LR", 0, "C");
													$this->fpdf->Cell(6, 0.5, substr($rm->Nama,0,38), "R", 0, "L");
													$this->fpdf->Cell(1.5, 0.5, number_format($tn,2,'.',''), "R", 0, "C");
													$this->fpdf->Cell(11, 0.5, "", "R", 0, "C");
												}
											}
										}
									}
								}
							}
						}
						$ns++;
					}
				}
				$kumulatifsks = $kumulatifsks + $jumlahsks;
				$ips = number_format(($totalbobot / $jumlahsks), 2);
				$kumulatifipk = $kumulatifipk + $ips;
				$ipk = number_format(($kumulatifipk / $kumulatif), 2);
				$this->fpdf->SetFont("Times","B",9);
				$this->fpdf->Ln();
				$this->fpdf->Cell(19, 0.2, "", "T", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "GPA Semester", "", 0, "L");
				$this->fpdf->Cell(0.5, 0.5, ":", "", 0, "R");
				$this->fpdf->Cell(1, 0.5, $ips, "", 0, "R");
				$this->fpdf->Cell(8.5, 0.5, "", "", 0, "R");
				$this->fpdf->Cell(6, 0.5, "Jakarta, ".tgl_singkat_eng($this->tanggal), "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "Cumulative GPA", "", 0, "L");
				$this->fpdf->Cell(0.5, 0.5, ":", "", 0, "R");
				$this->fpdf->Cell(1, 0.5, $ipk, "", 0, "R");
				$this->fpdf->Cell(8.5, 0.5, "", "", 0, "R");
				$this->fpdf->Cell(6, 0.5, "ACADEMIC PROGRAM MANAGER", "", 0, "C");
				$this->fpdf->Ln();
				$this->fpdf->Cell(3, 0.5, "Cumulative Credits", "", 0, "L");
				$this->fpdf->Cell(0.5, 0.5, ":", "", 0, "R");
				$this->fpdf->Cell(1, 0.5, $kumulatifsks, "", 0, "R");
				$this->fpdf->Ln();
				$this->fpdf->Cell(19, 0.2, "", "", 0, "C");
				$this->fpdf->Image(base_url("ptl_storage/tanda_tangan/sign_patrice.jpg"),$this->fpdf->getX()-5.1,$this->fpdf->getY()-0.3,4,2);
				$this->fpdf->Ln();
				$this->fpdf->SetFont("Times","B",6);
				$this->fpdf->Cell(5, 0.5, "Predicate of Comulative GPA:", "", 0, "L");
				$this->fpdf->Ln();
				$this->fpdf->Cell(2.5, 0.5, "IPK (CGPA) Min", "LBT", 0, "C");
				$this->fpdf->Cell(2.5, 0.5, "IPK (CGPA) Max", "LBT", 0, "C");
				$this->fpdf->Cell(5, 0.5, "PREDICATE", "LBTR", 0, "C");
				$rowpredikat = $this->m_predikat->PTL_all_active();
				if($rowpredikat)
				{
					$cekno = 0;
					foreach($rowpredikat as $row)
					{
						$cekno++;
						if($cekno == 2)
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(2.5, 0.5, $row->IPKMin, "LBT", 0, "C");
							$this->fpdf->Cell(2.5, 0.5, $row->IPKMax, "LBT", 0, "C");
							$this->fpdf->Cell(5, 0.5, $row->NamaEn, "LBTR", 0, "L");
							$this->fpdf->Cell(3.5, 0.5, "", "", 0, "C");
							$this->fpdf->SetFont("Times","B",9);
							$this->fpdf->Cell(5, 0.5, "PATRICE A.P DESILLES", "", 0, "C");
							$this->fpdf->SetFont("Times","B",6);
						}
						else
						{
							$this->fpdf->Ln();
							$this->fpdf->Cell(2.5, 0.5, $row->IPKMin, "LBT", 0, "C");
							$this->fpdf->Cell(2.5, 0.5, $row->IPKMax, "LBT", 0, "C");
							$this->fpdf->Cell(5, 0.5, $row->NamaEn, "LBTR", 0, "L");
						}
					}
				}
				$this->fpdf->Ln();
				$ta++;
				if($ta != $tot)
				{
					$this->fpdf->AddPage();
				}
			}
			$this->fpdf->Output($this->tanggal."_-_Student Evaluation $MhswID - $resmhsw[Nama].pdf","I");
		}
		
		function ptl_excel()
		{
			$word = explode(" - ",$this->session->userdata('eval_filter_mahasiswa'));
			$TahunID = $this->session->userdata('exam_filter_tahun1');
			$MhswID = $word[0];
			$data['MhswID'] = $MhswID;
			$result = $this->m_khs->PTL_all_select($MhswID);
			date_default_timezone_set('Asia/Jakarta');
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$tahun = 0;
			$kumulatif = 0;
			$kumulatifsks = 0;
			$kumulatifipk = 0;
			$ipk = 0;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Portal");
			$objPHPExcel->getProperties()->setLastModifiedBy("System");
			$objPHPExcel->getProperties()->setTitle("Data Evaluasi");
			$objPHPExcel->getProperties()->setSubject("Evaluasi Mahasiswa");
			$objPHPExcel->getProperties()->setDescription("Database Evaluasi");
			$objPHPExcel->getProperties()->setKeywords("Portal Data");
			$objPHPExcel->getProperties()->setCategory("Database");
			$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setAutoSize(true);;
			$objPHPExcel->getActiveSheet()->getColumnDimension("N")->setAutoSize(true);;
			$totcol = 1;
			$totrow = 1;
			foreach($result as $r)
			{
				// START HEADER
				// $this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.2,5.7,0.7);
				// $this->fpdf->Ln(0.2);
				// $this->fpdf->SetFont("helvetica","",7);
				$objPHPExcel->getActiveSheet()->mergeCells("A$totrow:C$totrow");
				$objPHPExcel->getActiveSheet()->mergeCells("A".($totrow+1).":C".($totrow+1));
				$objPHPExcel->getActiveSheet()->mergeCells("A".($totrow+2).":C".($totrow+2));
				$objPHPExcel->getActiveSheet()->setCellValue("A$totrow", "Jalan Asem Dua No. 3 - 5, Cipete, Jakarta Selatan");
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+1), "Telp. (021) 7659181, Fax. (021) 7657517");
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+2), "STUDENT EVALUATION RESULT");
				$id_akun = $_COOKIE["id_akun"];
				$nama = $_COOKIE["nama"];
				$TahunID = $r->TahunID;
				$restahun = $this->m_year->PTL_select($TahunID);
				$NamaTahun = "";
				if($restahun)
				{
					$NamaTahun = $restahun['Nama'];
				}
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				$reskhs = $this->m_khs->PTL_all_select($MhswID);
				$kelas1 = "";
				$kelas2 = "";
				$kelas3 = "";
				if($reskhs)
				{
					foreach($reskhs as $rk)
					{
						$KelasID = $rk->KelasID;
						$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
						if($rk->TahunKe == 1)
						{
							$kelas1 = $rk->TahunKe.@$reskelas["Nama"];
						}
						if($rk->TahunKe == 2)
						{
							$kelas2 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
						if($rk->TahunKe == 3)
						{
							$kelas3 = " > ".$rk->TahunKe.@$reskelas["Nama"];
						}
					}
				}
				$ProgramID = $resmhsw["ProgramID"];
				$resprog = $this->m_program->PTL_select($ProgramID);
				$ProdiID = $resmhsw["ProdiID"];
				$resprod = $this->m_prodi->PTL_select($ProdiID);
				if($resmhsw["Foto"] == "")
				{
					$foto = "foto_umum/user.jpg";
				}
				else
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
					if($exist)
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					}
					else
					{
						$foto = "foto_umum/user.jpg";
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+4), "NAME");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+4), ": $resmhsw[Nama]");
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+5), "STUDENT ID");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+5), ": $MhswID");
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+6), "PROGRAM");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+6), ": ".strtoupper($resprog["Nama"]));
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+7), "MAJOR");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+7), ": ".strtoupper($resprod["Nama"]));
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+8), "CLASS");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+8), ": $kelas1$kelas2$kelas3");
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+9), "ACADEMIC YEAR");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+9), ": $NamaTahun");
				$kumulatif++;
				if($no == 1)
				{
					$tahun = $tahun + 1;
					$no++;
				}
				else
				{
					$tahun = $tahun;
					$no = 1;
				}
				$KelasID = $r->KelasID;
				$rkel = $this->m_kelas->PTL_select_kelas($KelasID);
				if($rkel) { $kelas = $rkel["Nama"]; } else { $kelas = ""; }
				$DosenID = $r->WaliKelasID;
				$rhr1 = $this->m_dosen->PTL_select($DosenID);
				if($rhr1) { $hr1 = $rhr1["Nama"]; } else { $hr1 = ""; }
				$DosenID = $r->WaliKelasID2;
				$rhr2 = $this->m_dosen->PTL_select($DosenID);
				if($rhr2) { $hr2 = $rhr2["Nama"]; } else { $hr2 = ""; }
				$SpesialisasiID = $r->SpesialisasiID;
				$rspe = $this->m_spesialisasi->PTL_select($SpesialisasiID);
				if($rspe) { $spe = $rspe["Nama"]; } else { $spe = ""; }
				$thn = "";
				if($r->TahunKe == 1){ $thn = "st"; }
				if($r->TahunKe == 2){ $thn = "nd"; }
				if($r->TahunKe == 3){ $thn = "rd"; }
				if($r->ProgramID == "INT")
				{
					$TahunKe = "O";
				}
				else
				{
					$TahunKe = $r->TahunKe;
				}
				$smt = "";
				if($r->Sesi == 1){ $smt = "st"; }
				if($r->Sesi == 2){ $smt = "nd"; }
				if($r->Sesi == 3){ $smt = "rd"; }
				if($r->Sesi == 4){ $smt = "th"; }
				if($r->Sesi == 5){ $smt = "th"; }
				if($r->Sesi == 6){ $smt = "th"; }
				$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+12), "$TahunKe$thn Year");
				$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+12), "$r->Sesi$smt Semester");
				$objPHPExcel->getActiveSheet()->setCellValue("C".($totrow+12), "Class");
				$objPHPExcel->getActiveSheet()->mergeCells("D".($totrow+12).":G".($totrow+12));
				$objPHPExcel->getActiveSheet()->setCellValue("D".($totrow+12), "Specialization");
				$objPHPExcel->getActiveSheet()->mergeCells("H".($totrow+12).":I".($totrow+12));
				$objPHPExcel->getActiveSheet()->setCellValue("H".($totrow+12), "Homeroom");
				$objPHPExcel->getActiveSheet()->mergeCells("J".($totrow+12).":N".($totrow+12));
				$objPHPExcel->getActiveSheet()->setCellValue("J".($totrow+12), "Vice Homeroom");
				$word1 = explode(" ",$hr1);
				$word2 = explode(" ",$hr2);
				$objPHPExcel->getActiveSheet()->setCellValue("C".($totrow+13), $TahunKe.$kelas);
				$objPHPExcel->getActiveSheet()->mergeCells("D".($totrow+13).":G".($totrow+13));
				$objPHPExcel->getActiveSheet()->setCellValue("D".($totrow+13), $spe);
				$objPHPExcel->getActiveSheet()->mergeCells("H".($totrow+13).":I".($totrow+13));
				$objPHPExcel->getActiveSheet()->setCellValue("H".($totrow+13), $word1[0]." ".@$word1[1]." ".substr(@$word1[2],0,1));
				$objPHPExcel->getActiveSheet()->mergeCells("J".($totrow+13).":N".($totrow+13));
				$objPHPExcel->getActiveSheet()->setCellValue("J".($totrow+13), $word2[0]." ".@$word2[1]." ".substr(@$word2[2],0,1));
				$MhswID = $r->MhswID;
				$KHSID = $r->KHSID;
				$rowkrs = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
				$totalbobot = 0;
				$ips = 0;
				if($rowkrs)
				{
					$center = array();
					$center['alignment'] = array();
					$center['alignment']['horizontal']=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
					$objPHPExcel->getActiveSheet()->getStyle("A".($totrow+12).":N".($totrow+12))->applyFromArray($center);
					$objPHPExcel->getActiveSheet()->getStyle("A".($totrow+13).":N".($totrow+13))->applyFromArray($center);
					$objPHPExcel->getActiveSheet()->getStyle("A".($totrow+14).":N".($totrow+14))->applyFromArray($center);
					$objPHPExcel->getActiveSheet()->getStyle("A".($totrow+15).":N".($totrow+15))->applyFromArray($center);
					$objPHPExcel->getActiveSheet()->setCellValue("A".($totrow+14), "#");
					$objPHPExcel->getActiveSheet()->setCellValue("B".($totrow+14), "Subject");
					$objPHPExcel->getActiveSheet()->setCellValue("C".($totrow+14), "Score");
					$objPHPExcel->getActiveSheet()->mergeCells("D".($totrow+14).":G".($totrow+14));
					$objPHPExcel->getActiveSheet()->setCellValue("D".($totrow+14), "Attendance");
					$objPHPExcel->getActiveSheet()->setCellValue("H".($totrow+14), "M/P");
					$objPHPExcel->getActiveSheet()->setCellValue("I".($totrow+14), "GV");
					$objPHPExcel->getActiveSheet()->setCellValue("J".($totrow+14), "EXM");
					$objPHPExcel->getActiveSheet()->setCellValue("K".($totrow+14), "RMD");
					$objPHPExcel->getActiveSheet()->setCellValue("L".($totrow+14), "GRD");
					$objPHPExcel->getActiveSheet()->setCellValue("M".($totrow+14), "CRD");
					$objPHPExcel->getActiveSheet()->setCellValue("N".($totrow+14), "GP");
					
					$objPHPExcel->getActiveSheet()->setCellValue("D".($totrow+15), "EXC");
					$objPHPExcel->getActiveSheet()->setCellValue("E".($totrow+15), "SIC");
					$objPHPExcel->getActiveSheet()->setCellValue("F".($totrow+15), "ABS");
					$objPHPExcel->getActiveSheet()->setCellValue("G".($totrow+15), "LAT");
					$ns = 1;
					$jumlahsks = 0;
					$jumlahexc = 0;
					$jumlahsic = 0;
					$jumlahabs = 0;
					$jumlahlat = 0;
					$jumlahmp = 0;
					$jumlahbobot = 0;
					// $nkrscol = 0;
					$nkrsrow = 17;
					foreach($rowkrs as $rkr)
					{
						$TahunID = $rkr->TahunID;
						$JadwalID = $rkr->JadwalID;
						$SubjekID = $rkr->SubjekID;
						$rsub = $this->m_subjek->PTL_select($SubjekID);
						$subjek = $rsub["Nama"];
						$sks = $rsub["SKS"];
						$jumlahsks = $jumlahsks + $rsub["SKS"];
						$KurikulumID = $rsub["KurikulumID"];
						$JenisMKID = $rsub["JenisMKID"];
						$GradeNilai = $rkr->GradeNilai;
						$resbobot = $this->m_nilai->PTL_select_bobot($KurikulumID,$GradeNilai);
						$bobot = 0;
						if($resbobot)
						{
							$bobot = $resbobot['Bobot'];
						}
						$KRSID = $rkr->KRSID;
						$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
						if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
						$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
						if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
						$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
						if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
						$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
						if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
						$tottexc = $texc;
						$jumlahexc = $jumlahexc + $tottexc;
						if($tottexc == 0){ $tottexc = ""; }
						$tottsic = $tsic;
						$jumlahsic = $jumlahsic + $tottsic;
						if($tottsic == 0){ $tottsic = ""; }
						$tottabs = $tabs;
						$jumlahabs = $jumlahabs + $tottabs;
						if($tottabs == 0){ $tottabs = ""; }
						$tottlat = $tlat;
						$jumlahlat = $jumlahlat + $tottlat;
						if($tottlat == 0){ $tottlat = ""; }
						$TahunID = $rkr->TahunID;
						
						$exam = 0;
						$ExamID = "";
						$ExamMhswID = "";
						$TotJum = "";
						$rexmhsw1 = $this->m_exam->PTL_nilai_evaluasiA1($TahunID,$SubjekID);
						if($rexmhsw1)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw1 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA1 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA1)
								{
									if($rexmhswA1['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA1['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw2 = $this->m_exam->PTL_nilai_evaluasiA2($TahunID,$SubjekID);
						if($rexmhsw2)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw2 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA2 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA2)
								{
									if($rexmhswA2['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA2['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw3 = $this->m_exam->PTL_nilai_evaluasiA3($TahunID,$SubjekID);
						if($rexmhsw3)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw3 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA3 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA3)
								{
									if($rexmhswA3['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA3['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw4 = $this->m_exam->PTL_nilai_evaluasiB1($TahunID,$SubjekID);
						if($rexmhsw4)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw4 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA4 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA4)
								{
									if($rexmhswA4['Nilai'] > 0)
									{
										$jumexam = $jumexam + $rexmhswA4['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw5 = $this->m_exam->PTL_nilai_evaluasiB2($TahunID,$SubjekID);
						if($rexmhsw5)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw5 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA5 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA5)
								{
									if($rexmhswA5['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA5['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						$rexmhsw6 = $this->m_exam->PTL_nilai_evaluasiB3($TahunID,$SubjekID);
						if($rexmhsw6)
						{
							$jum = 0;
							$jumexam = 0;
							foreach($rexmhsw6 as $row)
							{
								$ExamID = $row->ExamID;
								$rexmhswA6 = $this->m_exam_mahasiswa->PTL_nilai_evaluasi($ExamID,$MhswID);
								if($rexmhswA6)
								{
									if($rexmhswA6['Nilai'] > 0)
									{
										$exam = $exam + $rexmhswA6['Nilai'];
										$jum++;
										$TotJum++;
									}
								}
							}
							if($jum > 0)
							{
								$exam = $exam + ($jumexam / $jum);
							}
						}
						if($exam == 0)
						{
							$exam = "-";
						}
						else
						{
							if($TotJum > 1)
							{
								$exam = number_format(($exam),2,'.','');
								// $exam = number_format(($exam / $TotJum),2,'.','');
							}
						}
						$resremedial = $this->m_remedial_krs->PTL_select_evaluasi($SubjekID,$KRSID,$MhswID);
						$remedial = "-";
						if($resremedial)
						{
							$remedial = $resremedial['Nilai'];
						}
						
						if($rkr->gradevalue == "")
						{
							$mp = "";
						}
						else
						{
							$JenisPresensiID = "E";
							$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
							$mptottexc = $texc * $pr1;
							$JenisPresensiID = "S";
							$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
							$mptottsic = $tsic * $pr2;
							$JenisPresensiID = "A";
							$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
							$mptottabs = $tabs * $pr3;
							$JenisPresensiID = "L";
							$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
							$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
							$mptottlat = $tlat * $pr4;
							if(($JenisMKID == "5") OR ($JenisMKID == "25")) // MKID Special
							{
								$mp = "";
							}
							else
							{
								$mp = $mptottexc + $mptottsic + $mptottabs + $mptottlat;
							}
							if($mp == 0.00)
							{
								$mp = "";
							}
							$jumlahmp = $jumlahmp + $mp;
						}
						$jumlahbobot = $bobot * $sks;
						$totalbobot = $totalbobot + $jumlahbobot;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, (($totrow-1)+$nkrsrow), $ns);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), substr($subjek,0,34));
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), $rkr->NilaiAkhir);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, (($totrow-1)+$nkrsrow), $tottexc);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, (($totrow-1)+$nkrsrow), $tottsic);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, (($totrow-1)+$nkrsrow), $tottabs);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, (($totrow-1)+$nkrsrow), $tottlat);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, (($totrow-1)+$nkrsrow), $mp);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, (($totrow-1)+$nkrsrow), $rkr->gradevalue);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, (($totrow-1)+$nkrsrow), $exam);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, (($totrow-1)+$nkrsrow), $remedial);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, (($totrow-1)+$nkrsrow), $rkr->GradeNilai);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, (($totrow-1)+$nkrsrow), $sks);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, (($totrow-1)+$nkrsrow), $jumlahbobot);
						$rowmk = $this->m_mk->PTL_all_select($SubjekID);
						$JumlahMK = count($rowmk);
						if($rowmk)
						{
							$NoMk = 1;
							foreach($rowmk as $rm)
							{
								$nkrsrow++;
								if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 21) OR ($TahunID == 22) OR ($TahunID == 28) OR ($TahunID == 29) OR ($TahunID == 30) OR ($TahunID == 31))
								{
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$PresensiID = "";
										$CekJenisPresensiID = "";
									}
									$MKID = $rm->MKID;
									$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
									$Nilai = 0;
									$AddNilai = 0;
									if($rowkrs2)
									{
										$totNilai = 0;
										$nkrs2 = 0;
										foreach($rowkrs2 as $rkrs2)
										{
											if(($JenisMKID == "5") OR ($JenisMKID == "25"))
											{
												$PresensiID = $rkrs2->PresensiID;
												$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
												if($resscoring)
												{
													$CekJenisPresensiID = $resscoring['JenisPresensiID'];
												}
											}
											if($rkrs2->Nilai > 0)
											{
												$totNilai = $totNilai + $rkrs2->Nilai;
												$nkrs2++;
											}
											$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
										}
										if($nkrs2 > 0)
										{
											$Nilai = $totNilai / $nkrs2;
										}
									}
									$tn = $Nilai + $AddNilai;
									if($rowkrs2)
									{
										if($rm->Optional == "Y")
										{
											if($tn != 0)
											{
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "A".$rm->Nama);
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
											}
										}
										else
										{
											if(($JenisMKID == "5") OR ($JenisMKID == "25"))
											{
												if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
												{
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "B".$rm->Nama);
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), "-");
												}
												else
												{
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "C".$rm->Nama);
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
												}
											}
											else
											{
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "D".$rm->Nama);
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
											}
										}
									}
								}
								else
								{
									if(($JenisMKID == "5") OR ($JenisMKID == "25"))
									{
										$PresensiID = "";
										$CekJenisPresensiID = "";
									}
									$CekMKID = $rm->MKID;
									$rescekmk = $this->m_presensi->PTL_select_cek_mk_scoring($JadwalID,$CekMKID);
									if($rescekmk)
									{
										$MKID = $rm->MKID;
										$rowkrs2 = $this->m_krs2->PTL_all_select_scoring_evaluation($KRSID,$MKID);
										$Nilai = 0;
										$AddNilai = 0;
										if($rowkrs2)
										{
											$totNilai = 0;
											$nkrs2 = 0;
											foreach($rowkrs2 as $rkrs2)
											{
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													$PresensiID = $rkrs2->PresensiID;
													$resscoring = $this->m_presensi_mahasiswa->PTL_select_scoring($PresensiID,$MhswID);
													if($resscoring)
													{
														$CekJenisPresensiID = $resscoring['JenisPresensiID'];
													}
												}
												if($rkrs2->Nilai > 0)
												{
													$totNilai = $totNilai + $rkrs2->Nilai;
													$nkrs2++;
												}
												$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
											}
											if($nkrs2 > 0)
											{
												$Nilai = $totNilai / $nkrs2;
											}
										}
										$tn = $Nilai + $AddNilai;
										if($rowkrs2)
										{
											if($rm->Optional == "Y")
											{
												if($tn != 0)
												{
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "E".$rm->Nama);
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
												}
											}
											else
											{
												if(($JenisMKID == "5") OR ($JenisMKID == "25"))
												{
													if(($CekJenisPresensiID == "E") OR ($CekJenisPresensiID == "S"))
													{
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "F".$rm->Nama);
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), "-");
													}
													else
													{
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "G".$rm->Nama);
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
													}
												}
												else
												{
													if($NoMk == $JumlahMK)
													{
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-1)+$nkrsrow), "H".$rm->Nama);
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-1)+$nkrsrow), number_format($tn,2,'.',''));
													}
													else
													{
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, (($totrow-2)+$nkrsrow), "H".$rm->Nama);
														$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, (($totrow-2)+$nkrsrow), number_format($tn,2,'.',''));
													}
												}
											}
										}
									}
								}
								$NoMk++;
							}
						}
						else
						{
							$nkrsrow++;
						}
						$ns++;
					}
				}
				$totrow = $totrow + 250;
			}
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Data Leads');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$this->tanggal.'_Data.xlsx"');
            $objWriter->save("php://output");
        }
	}
?>