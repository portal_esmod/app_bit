<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Predicate extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_predikat');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$data['rowrecord'] = $this->m_predikat->PTL_all_active();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Predikat/v_predikat',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$this->load->view('Portal/v_header');
			$this->load->view('Predikat/v_predikat_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$data = array(
						'Grade' => strtoupper($this->input->post('Grade')),
						'IPKMin' => $this->input->post('IPKMin'),
						'IPKMax' => $this->input->post('IPKMax'),
						'NamaEn' => $this->input->post('NamaEn'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_predikat->PTL_insert($data);
			echo warning("Your data successfully added.","../predicate");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$PredikatID = $this->uri->segment(3);
			$result = $this->m_predikat->PTL_select($PredikatID);
			$data['PredikatID'] = $result['PredikatID'];
			$data['Grade'] = $result['Grade'];
			$data['IPKMin'] = $result['IPKMin'];
			$data['IPKMax'] = $result['IPKMax'];
			$data['NamaEn'] = $result['NamaEn'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Predikat/v_predikat_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$PredikatID = $this->input->post('PredikatID');
			if($this->input->post('NA') == "") { $na = "N"; } else { $na = "Y"; }
			$data = array(
						'Grade' => strtoupper($this->input->post('Grade')),
						'IPKMin' => $this->input->post('IPKMin'),
						'IPKMax' => $this->input->post('IPKMax'),
						'NamaEn' => $this->input->post('NamaEn'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $na
						);
			$this->m_predikat->PTL_update($PredikatID,$data);
			echo warning("Your data successfully updated.","../predicate");
		}
	}
?>