<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Rank extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akses');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_gpa()
		{
			$this->authentification();
			$cekgpa = $this->input->post('cekgpa');
			$datalog = array(
							'pk1' => $cekgpa,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-01",
							'aktifitas' => "Filter halaman Student Performance Report - GPA: $cekgpa.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekgpa != "")
			{
				$this->session->set_userdata('spr_filter_gpa',$cekgpa);
			}
			else
			{
				$this->session->unset_userdata('spr_filter_gpa');
			}
			redirect("rank");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-01",
							'aktifitas' => "Filter halaman Student Performance Report - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('spr_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('spr_filter_jur');
			}
			redirect("rank");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			$datalog = array(
							'pk1' => $cekprodi,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-02",
							'aktifitas' => "Filter halaman Student Performance Report - Prodi: $cekprodi.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprodi != "")
			{
				$this->session->set_userdata('spr_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('spr_filter_prodi');
			}
			redirect("rank");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-03",
							'aktifitas' => "Filter halaman Student Performance Report - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('spr_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('spr_filter_tahun');
			}
			redirect("rank");
		}
		
		// function ptl_filter_tingkat()
		// {
			// $this->authentification();
			// $cektingkat = $this->input->post('cektingkat');
			// $datalog = array(
							// 'pk1' => $cektingkat,
							// 'id_akun' => $_COOKIE["id_akun"],
							// 'nama' => $_COOKIE["nama"],
							// 'aplikasi' => "ACADEMIC",
							// 'menu' => $this->uri->segment(1),
							// 'submenu' => $this->uri->segment(2),
							// 'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							// 'kode_halaman' => "EVSPR01-04",
							// 'aktifitas' => "Filter halaman Student Performance Report - Level: $cektingkat.",
							// 'data' => "no data",
							// 'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							// 'tanggal_buat' => $this->waktu
							// );
			// $this->m_aktifitas->PTL_insert($datalog);
			// if($cektingkat != "")
			// {
				// $this->session->set_userdata('spr_filter_tingkat',$cektingkat);
			// }
			// else
			// {
				// $this->session->unset_userdata('spr_filter_tingkat');
			// }
			// redirect("rank");
		// }
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			$datalog = array(
							'pk1' => $cekkelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-05",
							'aktifitas' => "Filter halaman Student Performance Report - Class: $cekkelas.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkelas != "")
			{
				if($cekkelas == "_")
				{
					$this->session->unset_userdata('spr_filter_kelas');
				}
				else
				{
					$this->session->set_userdata('spr_filter_kelas',$cekkelas);
				}
			}
			else
			{
				$this->session->unset_userdata('spr_filter_kelas');
			}
			redirect("rank");
		}
		
		function ptl_filter_subjek()
		{
			$this->authentification();
			$ceksubjek = $this->input->post('ceksubjek');
			$datalog = array(
							'pk1' => $ceksubjek,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR01-06",
							'aktifitas' => "Filter halaman Student Performance Report - Subject: $ceksubjek.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksubjek != "")
			{
				$this->session->set_userdata('spr_filter_subjek',$ceksubjek);
			}
			else
			{
				$this->session->unset_userdata('spr_filter_subjek');
			}
			redirect("rank");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','evaluation');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "EVSPR02",
							'aktifitas' => "Mengakses halaman Student Performance Report.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('spr_filter_jur');
			$cekprodi = $this->session->userdata('spr_filter_prodi');
			$cektahun = $this->session->userdata('spr_filter_tahun');
			$cekkelas = $this->session->userdata('spr_filter_kelas');
			$ceksubjek = $this->session->userdata('spr_filter_subjek');
			$ck = "";
			if($cekkelas != "")
			{
				$ck = $cekkelas;
			}
			$word = explode("_",$ck);
			$MatchTahun = @$word[0];
			$MatchKelas = @$word[1];
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik_aktif($cekjurusan);
			$data['rowprodi'] = "";
			if($cekjurusan == "REG")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all();
			}
			if($cekjurusan == "INT")
			{
				$data['rowprodi'] = $this->m_prodi->PTL_all_d1();
			}
			if($cekjurusan == "SC")
			{
				$data['rowprodi'] = $this->m_kursussingkat->PTL_all();
			}
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			// $data['rowsubjek'] = $this->m_jadwal->PTL_all_spesifik_gpa($cekjurusan,$cekprodi,$cektahun,$cekkelas);
			if($ceksubjek == "")
			{
				$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year_gpa($cekjurusan,$cekprodi,$cektahun,$MatchTahun,$MatchKelas);
			}
			else
			{
				$data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year_gpa_subject($cekjurusan,$cekprodi,$cektahun,$MatchTahun,$MatchKelas,$ceksubjek);
			}
			// $data['rowrecord'] = $this->m_khs->PTL_all_spesifik_aktif_year($cekjurusan,$cekprodi,$cektahun,$cektingkat);
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if($ceksubjek == "")
			{
				$this->load->view('Rank/v_rank',$data);
			}
			else
			{
				$this->load->view('Rank/v_rank_subjek',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
	}
?>