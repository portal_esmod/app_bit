<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Official extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_pejabat');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_pejabat->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Pejabat/v_pejabat',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$this->load->view('Portal/v_header');
			$this->load->view('Pejabat/v_pejabat_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'KodeJabatan' => $this->input->post('KodeJabatan'),
						'Nama' => $this->input->post('Nama'),
						'NIP' => $this->input->post('NIP'),
						'Jabatan' => $this->input->post('Jabatan'),
						'NA' => $na,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_pejabat->PTL_insert($data);
			echo warning("Your data successfully added.","../official");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$PejabatID = $this->uri->segment(3);
			$result = $this->m_pejabat->PTL_select($PejabatID);
			$data['PejabatID'] = $result['PejabatID'];
			$data['Urutan'] = $result['Urutan'];
			$data['KodeJabatan'] = $result['KodeJabatan'];
			$data['Nama'] = $result['Nama'];
			$data['NIP'] = $result['NIP'];
			$data['Jabatan'] = $result['Jabatan'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Pejabat/v_pejabat_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$PejabatID = $this->input->post('PejabatID');
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'Urutan' => $this->input->post('Urutan'),
						'KodeJabatan' => $this->input->post('KodeJabatan'),
						'Nama' => $this->input->post('Nama'),
						'NIP' => $this->input->post('NIP'),
						'Jabatan' => $this->input->post('Jabatan'),
						'NA' => $na,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_pejabat->PTL_update($PejabatID,$data);
			echo warning("Your data successfully updated.","../official");
		}
	}
?>