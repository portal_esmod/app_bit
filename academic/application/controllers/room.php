<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Room extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_kampus');
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_ruang');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_kampus()
		{
			$this->authentification();
			$cekkampus = $this->input->post('cekkampus');
			if($cekkampus != "")
			{
				$this->session->set_userdata('room_filter_kampus',$cekkampus);
			}
			else
			{
				$this->session->unset_userdata('room_filter_kampus');
			}
			redirect("room");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$cekkampus = $this->session->userdata('room_filter_kampus');
			$data['rowrecord'] = $this->m_ruang->PTL_all_spesifik($cekkampus);
			$data['rowkampus'] = $this->m_kampus->PTL_all_active();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Ruang/v_ruang',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$data['rowkampus'] = $this->m_kampus->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Ruang/v_ruang_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$RuangKuliah = "N";
			if($this->input->post('RuangKuliah') == "Y"){ $RuangKuliah = "Y"; }
			$UntukUjian = "N";
			if($this->input->post('UntukUjian') == "Y"){ $UntukUjian = "Y"; }
			$NA = "N";
			if($this->input->post('NA') == "Y"){ $NA = "Y"; }
			$total = $this->input->post('total');
			$ProdiID = "";
			for($i=1;$i<=$total;$i++)
			{
				if($this->input->post("prodi$i") != "")
				{
					$ProdiID .= $this->input->post("prodi$i").".";
				}
			}
			$data = array(
						'RuangID' => $this->input->post('RuangID'),
						'Nama' => $this->input->post('Nama'),
						'ProdiID' => $ProdiID,
						'KampusID' => $this->input->post('KampusID'),
						'Lantai' => $this->input->post('Lantai'),
						'RuangKuliah' => $RuangKuliah,
						'Kapasitas' => $this->input->post('Kapasitas'),
						'KapasitasUjian' => $this->input->post('KapasitasUjian'),
						'KolomUjian' => $this->input->post('KolomUjian'),
						'UntukUjian' => $UntukUjian,
						'Keterangan' => $this->input->post('Keterangan'),
						'NA' => $NA,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_ruang->PTL_insert($data);
			echo warning("Your data successfully added.","../room");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$RuangID = $this->uri->segment(3);
			$result = $this->m_ruang->PTL_select($RuangID);
			$data['RuangID'] = $result['RuangID'];
			$data['Nama'] = $result['Nama'];
			$data['ProdiID'] = $result['ProdiID'];
			$data['KampusID'] = $result['KampusID'];
			$data['Lantai'] = $result['Lantai'];
			$data['RuangKuliah'] = $result['RuangKuliah'];
			$data['Kapasitas'] = $result['Kapasitas'];
			$data['KapasitasUjian'] = $result['KapasitasUjian'];
			$data['KolomUjian'] = $result['KolomUjian'];
			$data['UntukUjian'] = $result['UntukUjian'];
			$data['Keterangan'] = $result['Keterangan'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['rowd3'] = $this->m_prodi->PTL_all();
			$data['rowd1'] = $this->m_prodi->PTL_all_d1();
			$data['rowkampus'] = $this->m_kampus->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Ruang/v_ruang_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$RuangID = $this->input->post('RuangID');
			$RuangKuliah = "N";
			if($this->input->post('RuangKuliah') == "Y"){ $RuangKuliah = "Y"; }
			$UntukUjian = "N";
			if($this->input->post('UntukUjian') == "Y"){ $UntukUjian = "Y"; }
			$NA = "N";
			if($this->input->post('NA') == "Y"){ $NA = "Y"; }
			$total = $this->input->post('total');
			$ProdiID = "";
			for($i=1;$i<=$total;$i++)
			{
				if($this->input->post("prodi$i") != "")
				{
					$ProdiID .= $this->input->post("prodi$i").".";
				}
			}
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'ProdiID' => $ProdiID,
						'KampusID' => $this->input->post('KampusID'),
						'Lantai' => $this->input->post('Lantai'),
						'RuangKuliah' => $RuangKuliah,
						'Kapasitas' => $this->input->post('Kapasitas'),
						'KapasitasUjian' => $this->input->post('KapasitasUjian'),
						'KolomUjian' => $this->input->post('KolomUjian'),
						'UntukUjian' => $UntukUjian,
						'Keterangan' => $this->input->post('Keterangan'),
						'NA' => $NA,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_ruang->PTL_update($RuangID,$data);
			echo warning("Your data successfully updated.","../room");
		}
		
		function ptl_campus()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_kampus->PTL_all_active();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Ruang/v_ruang_kampus',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_campus_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$this->load->view('Portal/v_header');
			$this->load->view('Ruang/v_ruang_kampus_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_campus_insert()
		{
			$this->authentification();
			$NA = "N";
			if($this->input->post('NA') == "Y"){ $NA = "Y"; }
			$data = array(
						'KampusID' => $this->input->post('KampusID'),
						'Nama' => $this->input->post('Nama'),
						'Alamat' => $this->input->post('Alamat'),
						'Kota' => $this->input->post('Kota'),
						'Telepon' => $this->input->post('Telepon'),
						'Fax' => $this->input->post('Fax'),
						'NA' => $NA,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_kampus->PTL_insert($data);
			echo warning("Your data successfully added.","../room/ptl_campus");
		}
		
		function ptl_campus_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$KampusID = $this->uri->segment(3);
			$result = $this->m_kampus->PTL_select($KampusID);
			$data['KampusID'] = $result['KampusID'];
			$data['Nama'] = $result['Nama'];
			$data['Alamat'] = $result['Alamat'];
			$data['Kota'] = $result['Kota'];
			$data['Telepon'] = $result['Telepon'];
			$data['Fax'] = $result['Fax'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Ruang/v_ruang_kampus_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_campus_update()
		{
			$this->authentification();
			$KampusID = $this->input->post('KampusID');
			$NA = "N";
			if($this->input->post('NA') == "Y"){ $NA = "Y"; }
			$data = array(
						'Nama' => $this->input->post('Nama'),
						'Alamat' => $this->input->post('Alamat'),
						'Kota' => $this->input->post('Kota'),
						'Telepon' => $this->input->post('Telepon'),
						'Fax' => $this->input->post('Fax'),
						'NA' => $NA,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_kampus->PTL_update($KampusID,$data);
			echo warning("Your data successfully updated.","../room/ptl_campus");
		}
	}
?>