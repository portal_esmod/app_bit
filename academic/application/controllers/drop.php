<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Drop extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('finance');
			$this->load->helper('tanggal');
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_bayar');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function lookup()
		{
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_mahasiswa->lookup($keyword);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->MhswID,
											'value' => $row->MhswID." - ".$row->Nama,
											''
										 );
				}
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('drop/index',$data);
			}
		}
		
		function lookup_subjek()
		{
			$TahunID = $this->session->userdata('drop_filter_khs');
			$keyword = $this->input->post('term');
			$data['response'] = 'false';
			$query = $this->m_subjek->lookup_subjek_tahun($keyword,$TahunID);
			if(!empty($query))
			{
				$data['response'] = 'true';
				$data['message'] = array();
				foreach($query as $row)
				{
					$data['message'][] = array(
											'id'=> $row->SubjekID,
											'value' => $row->SubjekID.' - '.$row->Nama.' - ('.$row->SubjekKode.')',
											''
										);
				}
			}
			else
			{
				$data['response'] = 'true';
				$data['message'] = array();
				$data['message'][] = array(
										'id'=> '',
										'value' => 'Curriculum ID not set in Academic Year'.$TahunID,
										'prodi' => '',
										''
										);
			}
			if('IS_AJAX')
			{
				echo json_encode($data);
			}
			else
			{
				$this->load->view('drop/index',$data);
			}
		}
		
		function ptl_cari()
		{
			$this->authentification();
			$this->session->set_userdata('drop_filter_mahasiswa',$this->input->post('cari'));
			$datalog = array(
							'pk1' => $this->input->post('cari'),
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN01-01",
							'aktifitas' => "Filter halaman Student Subject Management - Search: ".$this->input->post('cari').".",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("drop");
		}
		
		function ptl_search()
		{
			$this->authentification();
			$cari = str_replace("_"," ",$this->uri->segment(3));
			$this->session->set_userdata('drop_filter_mahasiswa',$cari);
			$datalog = array(
							'pk1' => $cari,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN01-02",
							'aktifitas' => "Filter halaman Student Subject Management - Search: $cari.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("drop");
		}
		
		function ptl_filter_khs()
		{
			$this->authentification();
			$cekkhs = $this->input->post('cekkhs');
			$datalog = array(
							'pk1' => $cekkhs,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN01-03",
							'aktifitas' => "Filter halaman Student Subject Management - KHS: $cekkhs.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkhs != "")
			{
				$this->session->set_userdata('drop_filter_khs',$cekkhs);
			}
			else
			{
				$this->session->unset_userdata('drop_filter_khs');
			}
			redirect("drop");
		}
		
		function ptl_filter_program()
		{
			$this->authentification();
			$cekprogram = $this->input->post('cekprogram');
			$datalog = array(
							'pk1' => $cekprogram,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN01-04",
							'aktifitas' => "Filter halaman Student Subject Management - Program: $cekprogram.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekprogram != "")
			{
				$this->session->set_userdata('drop_filter_program',$cekprogram);
			}
			else
			{
				$this->session->unset_userdata('drop_filter_program');
			}
			redirect("drop");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','drop');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN02",
							'aktifitas' => "Filter halaman Student Subject Management.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$word = explode(" - ",$this->session->userdata('drop_filter_mahasiswa'));
			if($word == "")
			{
				$MhswID = "";
			}
			else
			{
				$MhswID = $word[0];
			}
			$data['mhs'] = $this->session->userdata('drop_filter_mahasiswa');
			$data['MhswID'] = $MhswID;
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$result = $this->m_khs->PTL_all_select_drop($MhswID);
			$khsid = "";
			if($result)
			{
				$n = 1;
				foreach($result as $rr)
				{
					if($n == 1)
					{
						$khsid .= $rr->KHSID;
					}
					else
					{
						$khsid .= " > ".$rr->KHSID;
					}
					$n++;
				}
			}
			$data['KHSID'] = $khsid;
			$resmax = $this->m_khs->PTL_select_drop($MhswID);
			$TahunID = $resmax['tahun'];
			$result2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$data['tahun'] = $TahunID;
			$reskhs = $this->m_khs->PTL_all_select($MhswID);
			$kelas1 = "";
			$kelas2 = "";
			$kelas3 = "";
			$spec = "";
			if($reskhs)
			{
				foreach($reskhs as $rk)
				{
					$KelasID = $rk->KelasID;
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					if($reskelas)
					{
						if($rk->TahunKe == 1)
						{
							if($rk->ProgramID == "INT")
							{
								$kelas1 = "O".$reskelas["Nama"];
							}
							else
							{
								$kelas1 = $rk->TahunKe.$reskelas["Nama"];
							}
						}
						if($rk->TahunKe == 2)
						{
							$kelas2 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
						if($rk->TahunKe == 3)
						{
							$kelas3 = " > ".$rk->TahunKe.$reskelas["Nama"];
						}
					}
				}
			}
			$data['kelas'] = $kelas1.$kelas2.$kelas3;
			if($result2)
			{
				$data['StatusMhswID'] = $result2["StatusMhswID"];
				$lang = "";
				if($result2['languageID'] == "ENG")
				{
					$lang = "ENGLISH";
				}
				if($result2['languageID'] == "FRE")
				{
					$lang = "FRENCH";
				}
				if($result2['SpesialisasiID'] != 0)
				{
					$SpesialisasiID = $result2['SpesialisasiID'];
					$resspc = $this->m_spesialisasi->PTL_select($SpesialisasiID);
					$spec = "";
					if($resspc)
					{
						$spec = $resspc["Nama"];
					}
				}
				$data['languageID'] = $lang;
				$data['Spesialisasi'] = $spec;
			}
			else
			{
				$data['StatusMhswID'] = '';
				$data['languageID'] = '';
				$data['Spesialisasi'] = '';
			}
			$cekkhs = $this->session->userdata('drop_filter_khs');
			$KHSID = "";
			if($cekkhs == "")
			{
				$res = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				if($res)
				{
					$KHSID = $res["KHSID"];
				}
			}
			else
			{
				$TahunID = $cekkhs;
				$reskhsselect = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				if($reskhsselect)
				{
					$KHSID = $reskhsselect["KHSID"];
				}
				else
				{
					if($result2)
					{
						$KHSID = $result2["KHSID"];
					}
					else
					{
						$KHSID = "";
					}
				}
			}
			$data['khs2'] = $KHSID;
			$data['rowprodi1'] = $this->m_prodi->PTL_all();
			$data['rowprodi2'] = $this->m_prodi->PTL_all_d1();
			$data['rowprodi3'] = $this->m_kursussingkat->PTL_all();
			$data['rowrecord'] = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
			$data['id'] = "mahasiswa";
			$data['controller'] = "drop/lookup";
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Drop/v_drop',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_enabled()
		{
			$this->authentification();
			$KRSID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $KRSID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN03",
							'aktifitas' => "Filter halaman Student Subject Management - Enabled: $KRSID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'N'
						);
			$this->m_krs->PTL_update($KRSID,$data);
			echo warning("Your data successfully enabled.","../drop");
		}
		
		function ptl_disabled()
		{
			$this->authentification();
			$KRSID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $KRSID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN04",
							'aktifitas' => "Filter halaman Student Subject Management - Disabled: $KRSID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => 'Y'
						);
			$this->m_krs->PTL_update($KRSID,$data);
			echo warning("Your data successfully disabled.","../drop");
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$KRSID = $this->uri->segment(3);
			$res = $this->m_krs->PTL_select($KRSID);
			if($res)
			{
				$datalog = array(
								'pk1' => $KRSID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "SSMAN05-Y",
								'aktifitas' => "Filter halaman Student Subject Management - Delete: $KRSID.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$SubjekID = $res['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				$JadwalID = $res['JadwalID'];
				$MhswID = $res['MhswID'];
				$this->m_presensi_mahasiswa->PTL_delete_krs($KRSID,$MhswID);
				$this->m_krs->PTL_delete_krs($KRSID);
				
				$return = $this->m_mahasiswa->PTL_select($MhswID);
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to($return['Email']);
				$this->email->cc($return['Email2']);
				// $this->email->bcc('lendra.permana@gmail.com');
				$this->email->subject('DROP SUBJECT (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					Dear $MhswID - ".strtoupper($return['Nama']).",
					<br/>
					<br/>
					<b>ESMOD JAKARTA</b>: Your subject ($ressub[Nama]) has been dropped by $_COOKIE[nama].
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning('Your data successfully deleted permanently.','../drop');
				}
				else
				{
					if($this->email->send())
					{
						echo warning('Your data successfully deleted permanently.','../drop');
					}
					else
					{
						echo warning('Email server is not active. Data saved ...','../drop');
					}
				}
			}
			else
			{
				$datalog = array(
								'pk1' => $KRSID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "SSMAN05-N",
								'aktifitas' => "Filter halaman Student Subject Management - Delete: $KRSID.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				echo warning("KRSID '$KRSID' not found.","../drop");
			}
		}
		
		function ptl_subject_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','drop');
			$MhswID = $this->uri->segment(3);
			$khs2 = $this->uri->segment(4);
			$data['TahunID'] = $this->uri->segment(5);
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $khs2,
							'pk3' => $this->uri->segment(5),
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN06",
							'aktifitas' => "Filter halaman Student Subject Management - Subject Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data['KHSID'] = $khs2;
			$data['MhswID'] = $result['MhswID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['ProdiID'] = $result['ProdiID'];
			$ProdiID = $result['ProdiID'];
			$result2 = $this->m_prodi->PTL_select($ProdiID);
			$data['namaProdiID'] = $result2['Nama'];
			$data['rowkhs'] = $this->m_khs->PTL_all_select($MhswID);
			$this->load->view('Portal/v_header');
			$this->load->view('Drop/v_drop_subject_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_subject_insert()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$TahunID = $this->input->post('TahunID');
			$KHSID = $this->input->post('KHSID');
			$datalog = array(
							'pk1' => $MhswID,
							'pk2' => $TahunID,
							'pk3' => $KHSID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN07",
							'aktifitas' => "Filter halaman Student Subject Management - Subject Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$res = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
			$ProdiID = "";
			$ProgramID = "";
			$KelasID = "";
			$TahunKe = "";
			$Program1 = "";
			$Program2 = "";
			$SubjekID = "";
			$NamaSubjek = "";
			if($res)
			{
				$ProdiID = $res['ProdiID'];
				$ProgramID = $res['ProgramID'];
				if($ProgramID == "COMD3")
				{
					$Program1 = "FDD3";
					$Program2 = "PDD3";
				}
				else
				{
					if($ProgramID == "COMD1")
					{
						$Program1 = "FDD1";
						$Program2 = "PDD1";
					}
					else
					{
						$Program1 = $ProgramID;
						$Program2 = $ProgramID;
					}
				}
				$KelasID = $res['KelasID'];
				$TahunKe = $res['TahunKe'];
			}
			if($KelasID == 0)
			{
				echo warning("Your data not saved because CLASS NOT SET. Try again...","../drop/ptl_subject_form/$MhswID/$KHSID/$TahunID");
			}
			else
			{
				$word = explode(" - ",$this->input->post('SubjekID'));
				$SubjekID = $word[0];
				$resjadwal = $this->m_jadwal->PTL_all_select_drop($SubjekID,$TahunID,$ProgramID);
				$nsave = 0;
				$ndup = 0;
				if($resjadwal)
				{
					foreach($resjadwal as $rj)
					{
						if(($rj->TahunID == $TahunID) AND ($rj->TahunKe == $TahunKe))
						{
							if($rj->Gabungan == "N")
							{
								if($rj->KelasID == $KelasID)
								{
									if($rj->SubjekID == $SubjekID)
									{
										$rowres1 = $this->m_krs->PTL_select_spesifik_recap($MhswID,$SubjekID,$TahunID);
										if($rowres1)
										{
											$ndup++;
										}
										else
										{
											$ressub = $this->m_subjek->PTL_select($SubjekID);
											$NamaSubjek = $ressub['Nama'];
											$data = array(
														'KHSID' => $KHSID,
														'TahunID' => $TahunID,
														'JadwalID' => $rj->JadwalID,
														'SubjekID' => $SubjekID,
														'KodeID' => 'ES01',
														'MhswID' => $MhswID,
														'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
														'tanggal_buat' => $this->waktu
														);
											$this->m_krs->PTL_insert($data);
											$nsave++;
										}
									}
								}
							}
							if($rj->Gabungan == "Y")
							{
								if(strpos($rj->KelasIDGabungan,$KelasID) !== false)
								{
									if($rj->SubjekID == $SubjekID)
									{
										$rowres2 = $this->m_krs->PTL_select_spesifik_recap($MhswID,$SubjekID,$TahunID);
										if($rowres2)
										{
											$ndup++;
										}
										else
										{
											$ressub = $this->m_subjek->PTL_select($SubjekID);
											$NamaSubjek = $ressub['Nama'];
											$data = array(
														'KHSID' => $KHSID,
														'TahunID' => $TahunID,
														'JadwalID' => $rj->JadwalID,
														'SubjekID' => $SubjekID,
														'KodeID' => 'ES01',
														'MhswID' => $MhswID,
														'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
														'tanggal_buat' => $this->waktu
														);
											$this->m_krs->PTL_insert($data);
											$nsave++;
										}
									}
								}
							}
						}
					}
				}
				if($nsave > 0)
				{
					$return = $this->m_mahasiswa->PTL_select($MhswID);
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($return['Email']);
					$this->email->cc($return['Email2']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('ADD SUBJECT (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>ACADEMIC</h2></font>
						</center>
						<br/>
						Dear $MhswID - ".strtoupper($return['Nama']).",
						<br/>
						<br/>
						<b>ESMOD JAKARTA</b>: Your subject ($NamaSubjek) has been added by $_COOKIE[nama].
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($_COOKIE["id_akun"] == "00001111")
					{
						echo warning("Your data successfully added. $nsave data saved.","../drop");
					}
					else
					{
						if($this->email->send())
						{
							echo warning("Your data successfully added. $nsave data saved.","../drop");
						}
						else
						{
							echo warning('Email server is not active. Data saved ...','../drop');
						}
					}
				}
				else
				{
					if($ndup > 0)
					{
						echo warning("This subject has been there before. Try again...","../drop/ptl_subject_form/$MhswID/$KHSID/$TahunID");
					}
					else
					{
						echo warning("Your data not saved. Try again...","../drop/ptl_subject_form/$MhswID/$KHSID/$TahunID");
					}
				}
			}
		}
		
		function ptl_change_program()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$datalog = array(
							'pk1' => $MhswID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SSMAN08",
							'aktifitas' => "Filter halaman Student Subject Management - Change Program.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$resmaxkhs = $this->m_khs->PTL_select_drop($MhswID);
			if($resmaxkhs)
			{
				$TahunID = $resmaxkhs['tahun'];
				$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				if($reskhs['Sesi'] > 1)
				{
					echo warning("Prodi can not be changed because it has passed 1 semester.","../drop");
				}
				else
				{
					$ndel = 0;
					$KHSID = $reskhs['KHSID'];
					$rowrecord = $this->m_krs->PTL_select_spesifik($MhswID,$KHSID);
					if($rowrecord)
					{
						foreach($rowrecord as $row)
						{
							$KRSID = $row->KRSID;
							$res = $this->m_krs->PTL_select($KRSID);
							if($res)
							{
								$this->m_presensi_mahasiswa->PTL_delete_krs($KRSID,$MhswID);
								$this->m_krs->PTL_delete_krs($KRSID);
								$ndel++;
							}
						}
					}
					$word = explode("_",$this->input->post('prodi'));
					$ProdiID = $word[1];
					$resprod = $this->m_prodi->PTL_select($ProdiID);
					if($resprod)
					{
						$h = "-7";
						$hm = $h * 60;
						$ms = $hm * 60;
						$tahun = gmdate("Y", time()-($ms));
						$w = explode("~",$resprod['FormatNIM']);
						$kd = $tahun.$w[2];
						$result = $this->m_mahasiswa->PTL_urut($kd);
						$lastid = $result['LAST'];
						
						$lastnourut = substr($lastid,8,4);
						$nextnourut = $lastnourut + 1;
						$next = $kd.sprintf('%04s', $nextnourut);
					}
					$ProgramID = $word[0];
					$data = array(
								'MhswID' => $next,
								'ProgramID' => $ProgramID,
								'ProdiID' => $ProdiID,
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_mahasiswa->PTL_update($MhswID,$data);
					$resyear = $this->m_year->PTL_select_active($ProgramID);
					$TahunID = "";
					if($resyear)
					{
						$TahunID = $resyear['TahunID'];
					}
					$data = array(
								'MhswID' => $next,
								'TahunID' => $TahunID,
								'ProgramID' => $ProgramID,
								'ProdiID' => $ProdiID,
								'KelasID' => '',
								'WaliKelasID' => '',
								'WaliKelasID2' => '',
								'BIPOTID' => '',
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_khs->PTL_update($KHSID,$data);
					$TahunID = $reskhs['TahunID'];
					$Sesi = $reskhs['Sesi'];
					$BIPOTID = $reskhs['BIPOTID'];
					$data = array(
								'login_hapus' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_hapus' => $this->waktu,
								'NA' => 'Y'
								);
					$this->m_master->PTL_pym_bipotmhsw_update_delete($MhswID,$TahunID,$data);
					$res = $this->m_khs->PTL_select($KHSID);
					$Deposit = "";
					$pesan = "";
					if($res)
					{
						if($res['Bayar'] > 0)
						{
							$Deposit = $res['Deposit'] + $res['Bayar'];
							$pesan = '\n\nDEPOSIT '.formatRupiah3($Deposit);
							$data = array(
										'BIPOTID' => '',
										'Deposit' => $Deposit,
										'Biaya' => '',
										'Potongan' => '',
										'Bayar' => '',
										'Tutup' => 'N',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_khs->PTL_update($KHSID,$data);
						}
						else
						{
							$data = array(
										'BIPOTID' => '',
										'Biaya' => '',
										'Potongan' => '',
										'Bayar' => '',
										'Tutup' => 'N',
										'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
										'tanggal_edit' => $this->waktu
										);
							$this->m_khs->PTL_update($KHSID,$data);
						}
					}
					
					$rowrecord = $this->m_bayar->PTL_pym_all_select($MhswID,$BIPOTID);
					if($rowrecord)
					{
						foreach($rowrecord as $row)
						{
							$BayarMhswID = $row->BayarMhswID;
							$this->m_bayar->PTL_delete($BayarMhswID);
							$this->m_bayar->PTL_delete2($BayarMhswID);
						}
					}
					$MhswID2 = $MhswID;
					$MhswID = $next;
					$resmhs = $this->m_mahasiswa->PTL_select($MhswID);
					$cari = "";
					if($resmhs)
					{
						$cari = "$resmhs[MhswID]_-_".str_replace(" ","_",$resmhs["Nama"]);
					}
					if($ndel > 0)
					{
						echo warning("Your data successfully saved. '$ndel' data KRS deleted. OLD SIN '$MhswID2', NEW SIN '$next'. The next step, you must register these students (Set CLASS, HOMEROOM). You must inform the program changes to the FINANCE DEPARTMENT because financial data has been reset for this action (Set BIPOTID).".$pesan,"../drop/ptl_search/$cari");
					}
					else
					{
						if($word == "")
						{
							echo warning("You don't save any data.","../drop");
						}
						else
						{
							echo warning("Your data successfully saved. OLD SIN '$MhswID2', NEW SIN '$next'. The next step, you must register these students (Set CLASS, HOMEROOM). You must inform the program changes to the FINANCE DEPARTMENT because financial data has been reset for this action (Set BIPOTID).".$pesan,"../drop/ptl_search/$cari");
						}
					}
				}
			}
			else
			{
				echo warning("Data not found.","../drop");
			}
		}
	}
?>