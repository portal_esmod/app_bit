<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Class_list extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_kelas');
			$this->load->model('m_maintenance');
			$this->load->model('m_spesialisasi');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('class_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('class_filter_jur');
			}
			redirect("class_list");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$cekjurusan = $this->session->userdata('class_filter_jur');
			$data['rowrecord'] = $this->m_kelas->PTL_filter($cekjurusan);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Class/v_class',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Class/v_class_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "") { $na = "N"; } else { $na = "Y"; }
			$data = array(
						'SpesialisasiID' => $this->input->post('SpesialisasiID'),
						'Nama' => $this->input->post('Nama'),
						'ProgramID' => $this->session->userdata('class_filter_jur'),
						'KapasitasMaksimum' => $this->input->post('KapasitasMaksimum'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu,
						'NA' => $na
						);
			$this->m_kelas->PTL_insert($data);
			echo warning("Your data successfully added.","../class_list");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$KelasID = $this->uri->segment(3);
			$result = $this->m_kelas->PTL_select_kelas($KelasID);
			$data['KelasID'] = $result['KelasID'];
			$data['SpesialisasiID'] = $result['SpesialisasiID'];
			$data['KodeID'] = $result['KodeID'];
			$data['Nama'] = $result['Nama'];
			$data['KapasitasMaksimum'] = $result['KapasitasMaksimum'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['rowrecord'] = $this->m_spesialisasi->PTL_all();
			$this->load->view('Portal/v_header');
			$this->load->view('Class/v_class_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$KelasID = $this->input->post('KelasID');
			if($this->input->post('NA') == "") { $na = "N"; } else { $na = "Y"; }
			$data = array(
						'SpesialisasiID' => $this->input->post('SpesialisasiID'),
						'Nama' => $this->input->post('Nama'),
						'KapasitasMaksimum' => $this->input->post('KapasitasMaksimum'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $na
						);
			$this->m_kelas->PTL_update($KelasID,$data);
			echo warning("Your data successfully updated.","../class_list");
		}
	}
?>