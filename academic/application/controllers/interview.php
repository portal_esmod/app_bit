<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Interview extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_aplikan');
			$this->load->model('m_aplikan_log');
			$this->load->model('m_grade');
			$this->load->model('m_kurikulum');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_maintenance');
			$this->load->model('m_nilai');
			$this->load->model('m_pendidikanortu');
			$this->load->model('m_pmbperiod');
			$this->load->model('m_prodi');
			$this->load->model('m_sumberinfo');
			$this->load->model('m_wawancara');
		}
		
		function authentification()
		{
			$is_login = "is_login_admission";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Maaf! Program sedang MAINTENANCE...','../login/PTL_maintenance');
			}
		}
		
		function ptl_filter_int_periode()
		{
			$this->authentification();
			$cekperiode = $this->input->post('cekperiode');
			if($cekperiode != "")
			{
				$this->session->set_userdata('int_filter_periode',$cekperiode);
			}
			else
			{
				$this->session->set_userdata('int_filter_periode','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('int_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->set_userdata('int_filter_marketing','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_type()
		{
			$this->authentification();
			$cektype = $this->input->post('cektype');
			if($cektype != "")
			{
				$this->session->set_userdata('int_filter_type',$cektype);
			}
			else
			{
				$this->session->set_userdata('int_filter_type','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			if($cekstatus != "")
			{
				$this->session->set_userdata('int_filter_status',$cekstatus);
			}
			else
			{
				$this->session->set_userdata('int_filter_status','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_program()
		{
			$this->authentification();
			$cekpogram = $this->input->post('cekpogram');
			if($cekpogram != "")
			{
				$this->session->set_userdata('int_filter_program',$cekpogram);
			}
			else
			{
				$this->session->set_userdata('int_filter_program','');
			}
			redirect("interview");
		}
		
		function ptl_filter_int_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			if($cektahun != "")
			{
				$this->session->set_userdata('int_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->set_userdata('int_filter_tahun','');
			}
			redirect("interview");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$cekperiode = $this->session->userdata('int_filter_periode');
			$cekmarketing = $this->session->userdata('int_filter_marketing');
			$cektype = $this->session->userdata('int_filter_type');
			$cekstatus = $this->session->userdata('int_filter_status');
			$cekpogram = $this->session->userdata('int_filter_program');
			$cektahun = $this->session->userdata('int_filter_tahun');
			$data['rowrecord'] = $this->m_aplikan->PTL_all_interview_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun);
			$data['periode'] = $this->m_pmbperiod->PTL_all_period();
			$data['marketing'] = $this->m_akun->PTL_all_marketing();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['tahun'] = $this->m_aplikan->PTL_tahun();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Interview/v_interview',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_test_view()
		{
			$this->session->set_userdata('menu','additional');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['file_ujian2'] = $result['file_ujian2'];
			$data['id_aplikan'] = $id_aplikan;
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header_table',$hdata);
			$this->load->view('Interview/v_interview_view',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['AplikanID'] = $result['AplikanID'];
			$data['Nama'] = $result['Nama'];
			$data['Email'] = $result['Email'];
			$data['nilai_interview'] = $result['nilai_interview'];
			$data['hasil_interview'] = $result['hasil_interview'];
			
			$data['wawancara'] = $this->m_wawancara->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('Interview/v_interview_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$AplikanID = $this->input->post('AplikanID');
			$resapl = $this->m_aplikan->PTL_select($AplikanID);
			if($this->input->post('hasil_interview') == "Y")
			{
				$hasil_interview = "Y";
			}
			else
			{
				$hasil_interview = "N";
			}
			$data = array(
						'nilai_interview' => $this->input->post('nilai_interview'),
						'hasil_interview' => $hasil_interview,
						'korektor_interview' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'INTERVIEW: Your interview has been done'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($resapl['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('YOUR INTERVIEW HAVE BEEN DONE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your interview has been done.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Data INTERVIEW with APPLICANT CODE '".$AplikanID."' successfully updated.","../interview");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Data INTERVIEW with APPLICANT CODE '".$AplikanID."' successfully updated.","../interview");
				}
				else
				{
					echo warning('Email server is not active. Please try again...','../interview');
				}
			}
		}
		
		function ptl_test_score()
		{
			$this->authentification();
			$NilaiUjian = $this->input->post('NilaiUjian');
			if(($NilaiUjian > 20) OR ($NilaiUjian < 0))
			{
				echo warning("Sorry, you enter a value outside the range (0 - 20).","../interview");
			}
			else
			{
				$AplikanID = $this->input->post('AplikanID');
				$result = $this->m_aplikan->PTL_select($AplikanID);
				if(($result["file_ujian1"] == "") AND ($result["file_ujian2"] == ""))
				{
					echo warning("Sorry, applicant '$AplikanID' is not doing the test.","../interview");
				}
				else
				{
					$rowkurikulum = $this->m_kurikulum->PTL_all_active();
					$KurikulumID = "";
					if($rowkurikulum)
					{
						foreach($rowkurikulum as $rk)
						{
							$KurikulumID = $rk->KurikulumID;
						}
					}
					$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
					$GradeNilai = "";
					if($rownilai)
					{
						foreach($rownilai as $rn)
						{
							if(($NilaiUjian >= $rn->NilaiMin) AND ($NilaiUjian <= $rn->NilaiMax))
							{
								$GradeNilai = $rn->Nama;
							}
						}
					}
					$LulusUjian = "";
					if($GradeNilai == "F")
					{
						$LulusUjian = "N";
					}
					else
					{
						if($GradeNilai == "E")
						{
							$LulusUjian = "I";
						}
						else
						{
							$LulusUjian = "Y";
						}
					}
					$data = array(
								'NilaiUjian' => $NilaiUjian,
								'LulusUjian' => $LulusUjian,
								'GradeNilai' => $GradeNilai,
								'NA' => 'N',
								'korektor' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_edit' => $this->waktu
								);
					$this->m_aplikan->PTL_update($AplikanID,$data);
					
					$da = array(
								'id_akun' => $AplikanID,
								'nama' => $_COOKIE["nama"],
								'aktivitas' => 'ONLINE TEST: Your test have been considered by marketing team'
								);
					$this->m_aplikan_log->PTL_insert($da);
					
					$ip_client = $this->log->getIpAdress();
					$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$os_client = $this->log->getOs();
					$browser_client = $this->log->getBrowser();
					$perangkat_client = $this->log->getPerangkat();
					
					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['protocol']= "smtp";
					$config['mailtype']= "html";
					$config['smtp_host']= "mail.esmodjakarta.com";
					$config['smtp_port']= "25";
					$config['smtp_timeout']= "5";
					$config['smtp_user']= "no-reply@esmodjakarta.com";
					$config['smtp_pass']= "noreplyesmod";
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
					$config['wordwrap'] = TRUE;
					$this->email->initialize($config);
					$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
					$this->email->to($result['Email']);
					// $this->email->bcc('lendra.permana@gmail.com');
					$this->email->subject('YOUR TEST HAVE BEEN CONSIDERED (NO REPLY)');
					$this->email->message("
						<center>
							<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
							<font color='red'><h2>ACADEMIC</h2></font>
						</center>
						<br/>
						Dear '<b>$AplikanID - $result[Nama]</b>',
						<br/>
						<br/>
						<b>ESMOD JAKARTA</b>: Your test have been considered by academic team.
						<br/>
						<br/>
						<center><h2><font color='green'>Your Score is <b>$NilaiUjian</b></font></h2></center>
						<br/>
						<center><h2><font color='green'>Grade <b>$GradeNilai</b></font></h2></center>
						<br/>
						<br/>
						<br/>
						Thanks,
						<br/>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
						<center>
							<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
							Ip: $ip_client
							<br/>
							Hostname: $hostname_client
							<br/>
							OS: $os_client
							<br/>
							Browser: $browser_client
							<br/>
							Devices: $perangkat_client
						</center>
					");
					if($_COOKIE["id_akun"] == "00001111")
					{
						echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully updated.","../interview");
					}
					else
					{
						if($this->email->send())
						{
							echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully updated.","../interview");
						}
						else
						{
							echo warning("Email server is not active. Please try again...","../interview");
						}
					}
				}
			}
		}
		
		function ptl_applicant_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$id_aplikan = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($id_aplikan);
			$data['AplikanID'] = $result['AplikanID'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['PMBPeriodID'] = $result['PMBPeriodID'];
			$data['foto'] = $result['foto'];
			$data['BuktiSetoran'] = $result['BuktiSetoran'];
			$data['file_ujian1'] = $result['file_ujian1'];
			$data['file_syarat'] = $result['file_syarat'];
			$data['bukti_setoran'] = $result['bukti_setoran'];
			$data['SudahBayar'] = $result['SudahBayar'];
			$data['LulusUjian'] = $result['LulusUjian'];
			$data['SyaratLengkap'] = $result['SyaratLengkap'];
			$data['diterima'] = $result['diterima'];
			$data['Nama'] = $result['Nama'];
			$data['Kelamin'] = $result['Kelamin'];
			$data['GolonganDarah'] = $result['GolonganDarah'];
			$data['WargaNegara'] = $result['WargaNegara'];
			$data['Kebangsaan'] = $result['Kebangsaan'];
			$data['TempatLahir'] = $result['TempatLahir'];
			$data['TanggalLahir'] = $result['TanggalLahir'];
			$data['umur'] = $result['umur'];
			$data['Agama'] = $result['Agama'];
			$data['TinggiBadan'] = $result['TinggiBadan'];
			$data['BeratBadan'] = $result['BeratBadan'];
			$data['Alamat'] = $result['Alamat'];
			$data['Kota'] = $result['Kota'];
			$data['RT'] = $result['RT'];
			$data['RW'] = $result['RW'];
			$data['KodePos'] = $result['KodePos'];
			$data['Propinsi'] = $result['Propinsi'];
			$data['Negara'] = $result['Negara'];
			$data['Telepon'] = $result['Telepon'];
			$data['Handphone'] = $result['Handphone'];
			$data['Email'] = $result['Email'];
			$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
			$data['AsalSekolah'] = $result['AsalSekolah'];
			$data['JurusanSekolah'] = $result['JurusanSekolah'];
			$data['TahunLulus'] = $result['TahunLulus'];
			$data['NilaiSekolah'] = $result['NilaiSekolah'];
			$data['SudahBekerja'] = $result['SudahBekerja'];
			$data['NamaAyah'] = $result['NamaAyah'];
			$data['AgamaAyah'] = $result['AgamaAyah'];
			$data['PendidikanAyah'] = $result['PendidikanAyah'];
			$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
			$data['AlamatOrtu'] = $result['AlamatOrtu'];
			$data['KotaOrtu'] = $result['KotaOrtu'];
			$data['RTOrtu'] = $result['RTOrtu'];
			$data['RWOrtu'] = $result['RWOrtu'];
			$data['KodePosOrtu'] = $result['KodePosOrtu'];
			$data['PropinsiOrtu'] = $result['PropinsiOrtu'];
			$data['NegaraOrtu'] = $result['NegaraOrtu'];
			$data['TeleponOrtu'] = $result['TeleponOrtu'];
			$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
			$data['NamaIbu'] = $result['NamaIbu'];
			$data['AgamaIbu'] = $result['AgamaIbu'];
			$data['PendidikanIbu'] = $result['PendidikanIbu'];
			$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
			$data['Pilihan1'] = $result['Pilihan1'];
			$data['Pilihan2'] = $result['Pilihan2'];
			$data['Pilihan3'] = $result['Pilihan3'];
			$data['Harga'] = $result['Harga'];
			$data['StatusProspekID'] = $result['StatusProspekID'];
			$data['NilaiUjian'] = $result['NilaiUjian'];
			$data['PresenterID'] = $result['PresenterID'];
			$data['CatatanPresenter'] = $result['CatatanPresenter'];
			$data['Catatan'] = $result['Catatan'];
			$data['SumberInformasi'] = $result['SumberInformasi'];
			$data['total_biaya'] = $result['total_biaya'];
			$data['total_bayar'] = $result['total_bayar'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['postpone'] = $result['postpone'];
			$data['NA'] = $result['NA'];
			
			$data['marketing'] = $this->m_akses->PTL_all_marketing();
			$data['pmbperiod'] = $this->m_pmbperiod->PTL_all_active();
			$data['ortu'] = $this->m_pendidikanortu->PTL_all();
			$data['d3'] = $this->m_prodi->PTL_all();
			$data['d1'] = $this->m_prodi->PTL_all_d1();
			$data['kursussingkat'] = $this->m_kursussingkat->PTL_all();
			$data['sumberinfo'] = $this->m_sumberinfo->PTL_all();
			
			$hdata['rowrecord'] = $this->m_aplikan_log->PTL_all_notif();
			$this->load->view('Portal/v_header',$hdata);
			$this->load->view('Applicant/v_applicant_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_test_delete()
		{
			$this->authentification();
			$AplikanID = $this->uri->segment(3);
			$result = $this->m_aplikan->PTL_select($AplikanID);
			$file_order = $result['file_ujian1'];
			if($file_order != '')
			{
				$direktori = substr($file_order,0,7);
				$file = substr($file_order,8);
				unlink("../applicant/ptl_storage/admisi/ujian/".$direktori."/".$file);
			}
			$data = array(
						'NilaiUjian' => '',
						'LulusUjian' => 'N',
						'GradeNilai' => '',
						'akhir_ujian' => '',
						'file_ujian1' => '',
						'file_ujian2' => '',
						'ujian' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_aplikan->PTL_update($AplikanID,$data);
			
			$da = array(
						'id_akun' => $AplikanID,
						'nama' => $_COOKIE["nama"],
						'aktivitas' => 'ONLINE TEST: Your test have been deleted by marketing team'
						);
			$this->m_aplikan_log->PTL_insert($da);
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to($result['Email']);
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('TEST DELETED (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				<b>ESMOD JAKARTA</b>: Your test is deleted by academic team.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully deleted.","../interview");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Data TEST with APPLICANT CODE '".$AplikanID."' successfully deleted.","../interview");
				}
				else
				{
					echo warning("Email server is not active. Please try again...","../interview");
				}
			}
		}
		
		function ptl_test_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../applicant/ptl_storage/admisi/ujian/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning('File does not exist...','../applicant');
			}
		}
	}
?>