<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Download_general extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->helper('download');
			$this->load->model('m_maintenance');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','calendar');
			echo "<meta http-equiv='refresh' content='0; url=http://www.esmodjakarta.com'>";
		}
		
		function ptl_download_project_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/project_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../login");
			}
		}
		
		function ptl_download_evaluation_sheet()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$filelink = $this->uri->segment(4);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$data = file_get_contents("../academic/ptl_storage/evaluation_sheet/".$direktori."/".$file);
				force_download($file,$data);
			}
			else
			{
				echo warning("File does not exist...","../login");
			}
		}
	}
?>