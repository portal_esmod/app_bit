<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Today extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_kelas');
			$this->load->model('m_mk');
			$this->load->model('m_maintenance');
			$this->load->model('m_presensi');
			$this->load->model('m_ruang');
			$this->load->model('m_subjek');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_tanggal()
		{
			$this->authentification();
			$cektanggal = $this->input->post('cektanggal');
			if($cektanggal != "")
			{
				$this->session->set_userdata('today_filter_tanggal',$cektanggal);
			}
			else
			{
				$this->session->unset_userdata('today_filter_tanggal');
			}
			redirect("today");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			if($cekkelas != "")
			{
				$this->session->set_userdata('today_filter_kelas',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('today_filter_kelas');
			}
			redirect("today");
		}
		
		function ptl_filter_dosen()
		{
			$this->authentification();
			$cekdosen = $this->input->post('cekdosen');
			if($cekdosen != "")
			{
				$this->session->set_userdata('today_filter_dosen',$cekdosen);
			}
			else
			{
				$this->session->unset_userdata('today_filter_dosen');
			}
			redirect("today");
		}
		
		function ptl_filter_ruang()
		{
			$this->authentification();
			$cekruang = $this->input->post('cekruang');
			if($cekruang != "")
			{
				$this->session->set_userdata('today_filter_ruang',$cekruang);
			}
			else
			{
				$this->session->unset_userdata('today_filter_ruang');
			}
			redirect("today");
		}
		
		function ptl_filter_bentrok()
		{
			$this->authentification();
			$cekbentrok = $this->input->post('cekbentrok');
			if($cekbentrok != "")
			{
				$this->session->set_userdata('today_filter_bentrok',$cekbentrok);
			}
			else
			{
				$this->session->unset_userdata('today_filter_bentrok');
			}
			redirect("today");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','additional');
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tanggal = gmdate("Y-m-d", time()-($ms));
			$cektanggal = $this->session->userdata('today_filter_tanggal');
			if($cektanggal == "")
			{
				$data['today'] = $tanggal;
				$today = $tanggal;
			}
			else
			{
				$data['today'] = $cektanggal;
				$today = $cektanggal;
			}
			$cekdosen = $this->session->userdata('today_filter_dosen');
			$cekruang = $this->session->userdata('today_filter_ruang');
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowdosen'] = $this->m_dosen->PTL_all();
			$data['rowruang'] = $this->m_ruang->PTL_all();
			$data['rowrecord'] = $this->m_presensi->PTL_all_today($today,$cekdosen,$cekruang);
			$this->load->view('Portal/v_header_table');
			$this->load->view('Today/v_today',$data);
			$this->load->view('Portal/v_footer_table');
		}
	}
?>