<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Students extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tahun = gmdate("Y", time()-($ms));
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_agama');
			$this->load->model('m_akses');
			$this->load->model('m_bayar');
			$this->load->model('m_catatan2');
			$this->load->model('m_download');
			$this->load->model('m_exam_kursi');
			$this->load->model('m_exam_mahasiswa');
			$this->load->model('m_jadwal');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_kursussingkat');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_master');
			$this->load->model('m_pekerjaan');
			$this->load->model('m_pendidikan');
			$this->load->model('m_predikat');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
			$this->load->model('m_remedial_krs');
			$this->load->model('m_sipil');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_tahun_masuk()
		{
			$this->authentification();
			$cektahunmasuk = $this->input->post('cektahunmasuk');
			if($cektahunmasuk != "")
			{
				$this->session->set_userdata('students_filter_tahun_masuk',$cektahunmasuk);
			}
			else
			{
				$this->session->unset_userdata('students_filter_tahun_masuk');
			}
			redirect("students");
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			if($cekjurusan != "")
			{
				$this->session->set_userdata('students_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('students_filter_jur');
			}
			redirect("students");
		}
		
		function ptl_filter_prodi()
		{
			$this->authentification();
			$cekprodi = $this->input->post('cekprodi');
			if($cekprodi != "")
			{
				$this->session->set_userdata('students_filter_prodi',$cekprodi);
			}
			else
			{
				$this->session->unset_userdata('students_filter_prodi');
			}
			redirect("students");
		}
		
		function ptl_filter_marketing()
		{
			$this->authentification();
			$cekmarketing = $this->input->post('cekmarketing');
			if($cekmarketing != "")
			{
				$this->session->set_userdata('students_filter_marketing',$cekmarketing);
			}
			else
			{
				$this->session->unset_userdata('students_filter_marketing');
			}
			redirect("students");
		}
		
		function ptl_filter_no()
		{
			$this->authentification();
			$cekno = $this->input->post('cekno');
			if($cekno != "")
			{
				$this->session->set_userdata('students_filter_no',$cekno);
			}
			else
			{
				$this->session->unset_userdata('students_filter_no');
			}
			redirect("students");
		}
		
		function ptl_filter_status()
		{
			$this->authentification();
			$cekstatus = $this->input->post('cekstatus');
			if($cekstatus != "")
			{
				$this->session->set_userdata('students_filter_status',$cekstatus);
			}
			else
			{
				$this->session->unset_userdata('students_filter_status');
			}
			redirect("students");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','students');
			if($this->session->userdata('students_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('students_filter_no');
			}
			$uri_segment = 3;
			$offset = $this->uri->segment($uri_segment);
			$data['cariproduk'] = $this->m_mahasiswa->PTL_all_cari_all($cekno,$offset);
			$jml = $this->m_mahasiswa->PTL_all_cari_jumlah_all();
			$data['cek'] = $this->m_mahasiswa->PTL_all_cari_cek_all();
			$data['total'] = count($this->m_mahasiswa->PTL_all_cari_total_all());
			$this->load->library('pagination');
			$config['base_url'] = site_url("students/index");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students/index";
			
			$data['rowtahun'] = $this->m_mahasiswa->PTL_tahun();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['marketing'] = $this->m_akses->PTL_all_marketing();
			$data['status'] = $this->m_status->PTL_all_data();
			$data['totverif'] = count($this->m_mahasiswa->PTL_all_verification());
			$data['totverifnot'] = count($this->m_mahasiswa->PTL_all_verification_not());
			$data['totverifsent'] = count($this->m_mahasiswa->PTL_all_verification_sent());
			$data['totverifall'] = count($this->m_mahasiswa->PTL_all_verification_all());
			$data['pencarian'] = '';
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function search($offset=0)
		{
			$this->authentification();
			$this->session->set_userdata('menu','students');
			if($this->session->userdata('students_filter_no') == "")
			{
				$cekno = 10;
			}
			else
			{
				$cekno = $this->session->userdata('students_filter_no');
			}
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			if(isset($_POST['cari']))
			{
				$cari = $this->input->post('cari');
				$this->session->set_userdata('sess_cari',$cari);
			}
			else
			{
				$cari = $this->uri->segment(3);
				$this->session->set_userdata('sess_cari',$cari);
			}
			$data['cariproduk'] = $this->m_mahasiswa->PTL_all_cari($cari,$cekno,$offset);
			$jml = $this->m_mahasiswa->PTL_all_cari_jumlah($cari);
			$data['cek'] = $this->m_mahasiswa->PTL_all_cari_cek($cari);
			$data['total'] = count($this->m_mahasiswa->PTL_all_cari_total($cari));
			$this->load->library('pagination');
			$config['base_url'] = site_url("students/search/$cari");
			$config['total_rows'] = $jml;
			$config['per_page'] = $cekno;
			$config['uri_segment'] = $uri_segment;
			$config['full_tag_open'] = '<div class="paging">';
			$config['prev_link']='Previous';
			$config['next_link']='Next';
			$config['full_tag_close'] = '</div>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['konten'] = "students/search/$cari";
			
			$data['rowtahun'] = $this->m_mahasiswa->PTL_tahun();
			$data['programd1'] = $this->m_prodi->PTL_all_d1();
			$data['programd3'] = $this->m_prodi->PTL_all();
			$data['shortcourse'] = $this->m_kursussingkat->PTL_all();
			$data['pencarian'] = $cari;
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_download_identity()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$da = array(
						'nama_file' => $filelink,
						'aplikasi' => 'ACADEMIC',
						'modul' => $this->uri->segment(1),
						'fungsi' => $this->uri->segment(2),
						'primary_key' => $MhswID,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_download->PTL_insert($da);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data = array(
						'total_download_identity' => ($result['total_download_identity'] + 1)
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			$setelah = substr($filelink,8);
			$string = $setelah;
			$change = array(
			"&#40;" => "(",
			"&#41;" => ")",
			"%20" => " "
			);
			$file = strtr($string,$change);
			$direktori = substr($filelink,0,7);
			$data = file_get_contents("../lms/ptl_storage/file_identitas/$direktori/$file");
			force_download($file,$data);
		}
		
		function ptl_download_parents_identity()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$da = array(
						'nama_file' => $filelink,
						'aplikasi' => 'ACADEMIC',
						'modul' => $this->uri->segment(1),
						'fungsi' => $this->uri->segment(2),
						'primary_key' => $MhswID,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_download->PTL_insert($da);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data = array(
						'total_download_identitas_ortu' => ($result['total_download_identitas_ortu'] + 1)
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			$setelah = substr($filelink,8);
			$string = $setelah;
			$change = array(
			"&#40;" => "(",
			"&#41;" => ")",
			"%20" => " "
			);
			$file = strtr($string,$change);
			$direktori = substr($filelink,0,7);
			$data = file_get_contents("../lms/ptl_storage/file_identitas/$direktori/$file");
			force_download($file,$data);
		}
		
		function ptl_download_last_diploma()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			$MhswID = $this->uri->segment(4);
			$da = array(
						'nama_file' => $filelink,
						'aplikasi' => 'ACADEMIC',
						'modul' => $this->uri->segment(1),
						'fungsi' => $this->uri->segment(2),
						'primary_key' => $MhswID,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_download->PTL_insert($da);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$data = array(
						'total_download_ijazah' => ($result['total_download_ijazah'] + 1)
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			$setelah = substr($filelink,8);
			$string = $setelah;
			$change = array(
			"&#40;" => "(",
			"&#41;" => ")",
			"%20" => " "
			);
			$file = strtr($string,$change);
			$direktori = substr($filelink,0,7);
			$data = file_get_contents("../lms/ptl_storage/file_identitas/$direktori/$file");
			force_download($file,$data);
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$MhswID = $this->uri->segment(3);
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			if($result)
			{
				$data['MhswID'] = $result['MhswID'];
				$data['certificate_id'] = $result['certificate_id'];
				$data['Nama'] = $result['Nama'];
				$data['PresenterID'] = $result['PresenterID'];
				$data['Foto'] = $result['Foto'];
				$data['foto_lama'] = $result['Foto'];
				$data['file_identitas'] = $result['file_identitas'];
				$data['file_identitas_ortu'] = $result['file_identitas_ortu'];
				$data['file_ijazah'] = $result['file_ijazah'];
				$data['TempatLahir'] = $result['TempatLahir'];
				$data['TanggalLahir'] = $result['TanggalLahir'];
				$data['Kelamin'] = $result['Kelamin'];
				$data['WargaNegara'] = $result['WargaNegara'];
				$data['Kebangsaan'] = $result['Kebangsaan'];
				$data['Agama'] = $result['Agama'];
				$data['StatusSipil'] = $result['StatusSipil'];
				$data['PendidikanTerakhir'] = $result['PendidikanTerakhir'];
				$data['TahunLulus'] = $result['TahunLulus'];
				$data['TinggiBadan'] = $result['TinggiBadan'];
				$data['BeratBadan'] = $result['BeratBadan'];
				$data['AnakKe'] = $result['AnakKe'];
				$data['JumlahSaudara'] = $result['JumlahSaudara'];
				$data['Alamat'] = $result['Alamat'];
				$data['RT'] = $result['RT'];
				$data['RW'] = $result['RW'];
				$data['Kota'] = $result['Kota'];
				$data['Propinsi'] = $result['Propinsi'];
				$data['KodePos'] = $result['KodePos'];
				$data['Negara'] = $result['Negara'];
				$data['Handphone'] = $result['Handphone'];
				$data['Email'] = $result['Email'];
				$data['Email2'] = $result['Email2'];
				$data['AlamatAsal'] = $result['AlamatAsal'];
				$data['RTAsal'] = $result['RTAsal'];
				$data['RWAsal'] = $result['RWAsal'];
				$data['KotaAsal'] = $result['KotaAsal'];
				$data['KodePosAsal'] = $result['KodePosAsal'];
				$data['NamaAyah'] = $result['NamaAyah'];
				$data['AgamaAyah'] = $result['AgamaAyah'];
				$data['PendidikanAyah'] = $result['PendidikanAyah'];
				$data['PekerjaanAyah'] = $result['PekerjaanAyah'];
				$data['AlamatOrtu'] = $result['AlamatOrtu'];
				$data['KotaOrtu'] = $result['KotaOrtu'];
				$data['KodePosOrtu'] = $result['KodePosOrtu'];
				$data['TeleponOrtu'] = $result['TeleponOrtu'];
				$data['HandphoneOrtu'] = $result['HandphoneOrtu'];
				$data['EmailOrtu'] = $result['EmailOrtu'];
				$data['NamaIbu'] = $result['NamaIbu'];
				$data['AgamaIbu'] = $result['AgamaIbu'];
				$data['PendidikanIbu'] = $result['PendidikanIbu'];
				$data['PekerjaanIbu'] = $result['PekerjaanIbu'];
				$data['TeleponIbu'] = $result['TeleponIbu'];
				$data['HandphoneIbu'] = $result['HandphoneIbu'];
				$data['EmailIbu'] = $result['EmailIbu'];
				$data['StatusMhswID'] = $result['StatusMhswID'];
				if($result['tanggal_lulus'] == "")
				{
					$data['cektanggal_lulus'] = 'YES';
					$data['tanggal_lulus'] = $this->session->userdata('students_set_tanggal_lulus');
				}
				else
				{
					$data['cektanggal_lulus'] = '';
					$data['tanggal_lulus'] = $result['tanggal_lulus'];
				}
				$data['verifikasi'] = $result['verifikasi'];
				$data['verifikasi_kode'] = $result['verifikasi_kode'];
				
				$data['marketing'] = $this->m_akses->PTL_all_marketing();
				$data['rowagama'] = $this->m_agama->PTL_all_active();
				$data['rowsipil'] = $this->m_sipil->PTL_all_active();
				$data['rowpendidikan'] = $this->m_pendidikan->PTL_all_active();
				$data['rowpekerjaan'] = $this->m_pekerjaan->PTL_all_active();
				$data['rowstatus'] = $this->m_status->PTL_all();
				$data['rowcatatan'] = $this->m_catatan2->PTL_all_select($MhswID);
				$this->load->view('Portal/v_header');
				$this->load->view('Students/v_students_edit',$data);
				$this->load->view('Portal/v_footer');
			}
			else
			{
				echo warning("Data with ID '$MhswID' does not exist in the database.","../students");
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$this->session->set_userdata('students_set_tanggal_lulus',$this->input->post('tanggal_lulus'));
			$StatusMhswID = $this->input->post('StatusMhswID');
			if($StatusMhswID == "L")
			{
				$tanggal_lulus = $this->input->post('tanggal_lulus');
			}
			else
			{
				$tanggal_lulus = "";
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$admin_dp = './ptl_storage/foto_mahasiswa';
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "FOTO_".$this->input->post('MhswID')."_".str_replace(' ','_',$this->input->post('Nama'))."_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '102400';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$MhswID = $this->input->post('MhswID');
				$data = array(
							'certificate_id' => $this->input->post('certificate_id'),
							'Nama' => $this->input->post('Nama'),
							'PresenterID' => $this->input->post('PresenterID'),
							'TempatLahir' => $this->input->post('TempatLahir'),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'WargaNegara' => $this->input->post('WargaNegara'),
							'Kebangsaan' => $this->input->post('Kebangsaan'),
							'Agama' => $this->input->post('Agama'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'PendidikanTerakhir' => $this->input->post('PendidikanTerakhir'),
							'TahunLulus' => $this->input->post('TahunLulus'),
							'TinggiBadan' => $this->input->post('TinggiBadan'),
							'BeratBadan' => $this->input->post('BeratBadan'),
							'AnakKe' => $this->input->post('AnakKe'),
							'JumlahSaudara' => $this->input->post('JumlahSaudara'),
							'Alamat' => $this->input->post('Alamat'),
							'RT' => $this->input->post('RT'),
							'RW' => $this->input->post('RW'),
							'Kota' => $this->input->post('Kota'),
							'Propinsi' => $this->input->post('Propinsi'),
							'KodePos' => $this->input->post('KodePos'),
							'Negara' => $this->input->post('Negara'),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => $this->input->post('Email'),
							'Email2' => $this->input->post('Email2'),
							'AlamatAsal' => $this->input->post('AlamatAsal'),
							'RTAsal' => $this->input->post('RTAsal'),
							'RWAsal' => $this->input->post('RWAsal'),
							'KotaAsal' => $this->input->post('KotaAsal'),
							'KodePosAsal' => $this->input->post('KodePosAsal'),
							'NamaAyah' => $this->input->post('NamaAyah'),
							'AgamaAyah' => $this->input->post('AgamaAyah'),
							'PendidikanAyah' => $this->input->post('PendidikanAyah'),
							'PekerjaanAyah' => $this->input->post('PekerjaanAyah'),
							'AlamatOrtu' => $this->input->post('AlamatOrtu'),
							'KotaOrtu' => $this->input->post('KotaOrtu'),
							'KodePosOrtu' => $this->input->post('KodePosOrtu'),
							'TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'EmailOrtu' => $this->input->post('EmailOrtu'),
							'NamaIbu' => $this->input->post('NamaIbu'),
							'AgamaIbu' => $this->input->post('AgamaIbu'),
							'PendidikanIbu' => $this->input->post('PendidikanIbu'),
							'PekerjaanIbu' => $this->input->post('PekerjaanIbu'),
							'TeleponIbu' => $this->input->post('TeleponIbu'),
							'HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'EmailIbu' => $this->input->post('EmailIbu'),
							'StatusMhswID' => $StatusMhswID,
							'tanggal_lulus' => $tanggal_lulus,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				$resmax = $this->m_khs->PTL_select_drop($MhswID);
				$TahunID = $resmax['tahun'];
				$result2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$KHSID = $result2['KHSID'];
				$data = array(
							'StatusMhswID' => $StatusMhswID,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
				echo warning("Your data successfully updated.","../students/ptl_edit/$MhswID");
			}
			else
			{
				if($this->input->post('foto_lama') != "")
				{
					$dir = "ptl_storage/foto_mahasiswa";
					$fot = $this->input->post('foto_lama');
					$info = pathinfo("./$dir/$fot");
					if($info["extension"] != "")
					{
						unlink("./$dir/$fot");
					}
				}
				$source_photo = "$admin_dp/$downloadin";
				$ukuran = fsize($source_photo);
				$word = explode(" ",$ukuran);
				$pesan_file = "";
				$ukuran_akhir = "";
				if(($word[1] == "KB") OR ($word[1] == "MB"))
				{
					$pesan_file .= " File uploads are eligible for less than 10 MB.";
					if($word[1] == "KB")
					{
						if($word[0] > 128)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units KB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
					else
					{
						if($word[0] >= 1)
						{
							$info = pathinfo($source_photo);
							$pesan_file.= " Files need to be compressed for more than 128 KB (Units MB).";
							$destination_photo = $admin_dp."/".$info["filename"]."_compress.".$info["extension"];
							$im_src = imagecreatefromjpeg($source_photo);
							$src_width = imageSX($im_src);
							$src_height = imageSY($im_src);
							$dst_width = 300;
							$dst_height = 400;
							$im = imagecreatetruecolor($dst_width,$dst_height);
							imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
							imagejpeg($im,$destination_photo);
							$ukuran_akhir =  " Previous size $ukuran, final size ".fsize($destination_photo).". Previously managed in the archive file.";
							imagedestroy($im_src);
							imagedestroy($im);
						}
						else
						{
							$pesan_file .= " The file does not need to be compressed.";
						}
					}
				}
				else
				{
					if($word[1] == "B")
					{
						$pesan_file .=  " File too small. $word[1]";
					}
					else
					{
						$pesan_file .=  " File too large.";
					}
				}
				$MhswID = $this->input->post('MhswID');
				$data = array(
							'certificate_id' => $this->input->post('certificate_id'),
							'Nama' => $this->input->post('Nama'),
							'Foto' => $dwn,
							'PresenterID' => $this->input->post('PresenterID'),
							'TempatLahir' => $this->input->post('TempatLahir'),
							'TanggalLahir' => $this->input->post('TanggalLahir'),
							'Kelamin' => $this->input->post('Kelamin'),
							'WargaNegara' => $this->input->post('WargaNegara'),
							'Kebangsaan' => $this->input->post('Kebangsaan'),
							'Agama' => $this->input->post('Agama'),
							'StatusSipil' => $this->input->post('StatusSipil'),
							'PendidikanTerakhir' => $this->input->post('PendidikanTerakhir'),
							'TahunLulus' => $this->input->post('TahunLulus'),
							'TinggiBadan' => $this->input->post('TinggiBadan'),
							'BeratBadan' => $this->input->post('BeratBadan'),
							'AnakKe' => $this->input->post('AnakKe'),
							'JumlahSaudara' => $this->input->post('JumlahSaudara'),
							'Alamat' => $this->input->post('Alamat'),
							'RT' => $this->input->post('RT'),
							'RW' => $this->input->post('RW'),
							'Kota' => $this->input->post('Kota'),
							'Propinsi' => $this->input->post('Propinsi'),
							'KodePos' => $this->input->post('KodePos'),
							'Negara' => $this->input->post('Negara'),
							'Handphone' => $this->input->post('Handphone'),
							'Email' => $this->input->post('Email'),
							'Email2' => $this->input->post('Email2'),
							'AlamatAsal' => $this->input->post('AlamatAsal'),
							'RTAsal' => $this->input->post('RTAsal'),
							'RWAsal' => $this->input->post('RWAsal'),
							'KotaAsal' => $this->input->post('KotaAsal'),
							'KodePosAsal' => $this->input->post('KodePosAsal'),
							'NamaAyah' => $this->input->post('NamaAyah'),
							'AgamaAyah' => $this->input->post('AgamaAyah'),
							'PendidikanAyah' => $this->input->post('PendidikanAyah'),
							'PekerjaanAyah' => $this->input->post('PekerjaanAyah'),
							'AlamatOrtu' => $this->input->post('AlamatOrtu'),
							'KotaOrtu' => $this->input->post('KotaOrtu'),
							'KodePosOrtu' => $this->input->post('KodePosOrtu'),
							'TeleponOrtu' => $this->input->post('TeleponOrtu'),
							'HandphoneOrtu' => $this->input->post('HandphoneOrtu'),
							'EmailOrtu' => $this->input->post('EmailOrtu'),
							'NamaIbu' => $this->input->post('NamaIbu'),
							'AgamaIbu' => $this->input->post('AgamaIbu'),
							'PendidikanIbu' => $this->input->post('PendidikanIbu'),
							'PekerjaanIbu' => $this->input->post('PekerjaanIbu'),
							'TeleponIbu' => $this->input->post('TeleponIbu'),
							'HandphoneIbu' => $this->input->post('HandphoneIbu'),
							'EmailIbu' => $this->input->post('EmailIbu'),
							'StatusMhswID' => $StatusMhswID,
							'tanggal_lulus' => $tanggal_lulus,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
				$resmax = $this->m_khs->PTL_select_drop($MhswID);
				$TahunID = $resmax['tahun'];
				$result2 = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$KHSID = $result2['KHSID'];
				$data = array(
							'StatusMhswID' => $StatusMhswID,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_khs->PTL_update($KHSID,$data);
				echo warning("Your data successfully updated.$pesan_file$ukuran_akhir","../students/ptl_edit/$MhswID");
			}
		}
		
		function ptl_not_complete()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			$data = array(
						'verifikasi' => 'N',
						'verifikasi_kode' => '',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_mahasiswa->PTL_update($MhswID,$data);
			
			$result = $this->m_mahasiswa->PTL_select($MhswID);
			$Nama = $result['Nama'];
			$Email = $result['Email'];
			$Email2 = $result['Email2'];
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$Email");
			$this->email->cc("$Email2");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('COMPLETE YOUR PROFILE (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Hi <b>$MhswID - $Nama</b>,
				<br/>
				<br/>
				We have reviewed your profile and seem not yet complete. Please complete your profile in the PORTAL LMS application.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your reminder has been sent.","../students/ptl_edit/$MhswID");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your reminder has been sent.","../students/ptl_edit/$MhswID");
				}
				else
				{
					echo warning("Email server is not active. Data saved.","../students/ptl_edit/$MhswID");
				}
			}
		}
		
		function ptl_confirm_delete()
		{
			$this->authentification();
			$data['MhswID'] = $this->uri->segment(3);
			$MhswID = $this->uri->segment(3);
			$resmhs = $this->m_mahasiswa->PTL_select($MhswID);
			$data['Nama'] = $resmhs['Nama'];
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students_delete',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_delete()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$reason = $this->input->post('reason');
			$resmhs = $this->m_mahasiswa->PTL_select($MhswID);
			$this->m_mahasiswa->PTL_delete($MhswID);
			$this->m_catatan2->PTL_delete_mahasiswa($MhswID);
			$this->m_exam_kursi->PTL_delete_mahasiswa($MhswID);
			$this->m_exam_mahasiswa->PTL_delete_mahasiswa($MhswID);
			$this->m_khs->PTL_delete_mahasiswa($MhswID);
			$this->m_krs->PTL_delete_mahasiswa($MhswID);
			$this->m_remedial_krs->PTL_delete_mahasiswa($MhswID);
			$this->m_presensi_mahasiswa->PTL_delete_mahasiswa($MhswID);
			$this->m_bayar->PTL_delete_mahasiswa($MhswID);
			$this->m_master->PTL_delete_mahasiswa($MhswID);
			
			$h = "-7";
			$hm = $h * 60; 
			$ms = $hm * 60;
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			$jam = gmdate("H:i:s", time()-($ms));
			$tgl = gmdate("d M Y", time()-($ms));
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to('patrice@esmodjakarta.com');
			$this->email->cc('anita@esmodjakarta.com,rossy@esmodjakarta.com,anisa@esmodjakarta.com,miha@esmodjakarta.com,wprasasti@esmodjakarta.com,ichaa@esmodjakarta.com,stenna@esmodjakarta.com,theresia@esmodjakarta.com');
			$this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('DELETE STUDENT (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				We hereby inform you that student with <font color='blue'><b>SIN $MhswID, Name $resmhs[Nama]</b></font> was deleted permanently by ".$_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"].".
				<br/>
				<br/>
				On this $tgl at $jam.
				<br/>
				<br/>
				<font color='red'><b>Reason</b></font> : $reason
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Student data with SIN $MhswID, Name $resmhs[Nama] was deleted permanently.","../students");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Student data with SIN $MhswID, Name $resmhs[Nama] was deleted permanently.","../students");
				}
				else
				{
					echo warning("Student data with SIN $MhswID, Name $resmhs[Nama] was deleted permanently. Email server is not active. Email not sent. Please sent manually",'../login');
				}
			}
		}
		
		function ptl_note_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$MhswID = $this->uri->segment(3);
			$data['MhswID'] = $MhswID;
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students_note_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_note_insert()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$data = array(
						'MhswID' => $this->input->post('MhswID'),
						'datemeet' => $this->input->post('datemeet'),
						'title' => $this->input->post('title'),
						'comment' => $this->input->post('comment'),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_catatan2->PTL_insert($data);
			echo warning("Your notes successfully added.","../students/ptl_edit/$MhswID");
		}
		
		function ptl_note_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['MhswID'] = $this->uri->segment(3);
			$noteid = $this->uri->segment(4);
			$result = $this->m_catatan2->PTL_select($noteid);
			$data['noteid'] = $result['noteid'];
			$data['datemeet'] = $result['datemeet'];
			$data['title'] = $result['title'];
			$data['comment'] = $result['comment'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$JadwalID = $result['JadwalID'];
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$data['NamaSubjek'] = '';
			if($resjadwal)
			{
				$SubjekID = $resjadwal['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				if($ressub)
				{
					$data['NamaSubjek'] = $ressub['Nama'];
				}
			}
			$this->load->view('Portal/v_header');
			$this->load->view('Students/v_students_note_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_note_update()
		{
			$this->authentification();
			$MhswID = $this->input->post('MhswID');
			$noteid = $this->input->post('noteid');
			$data = array(
						'datemeet' => $this->input->post('datemeet'),
						'title' => $this->input->post('title'),
						'comment' => $this->input->post('comment'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_catatan2->PTL_update($noteid,$data);
			echo warning("Your notes successfully updated.","../students/ptl_edit/$MhswID");
		}
		
		function ptl_note_delete()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			$noteid = $this->uri->segment(4);
			$data = array(
						'NA' => 'Y'
						);
			$this->m_catatan2->PTL_update($noteid,$data);
			echo warning("Your notes successfully deleted.","../students/ptl_edit/$MhswID");
		}
		
		function ptl_certificate()
		{
			$this->authentification();
			$MhswID = $this->uri->segment(3);
			$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
			$ProdiID = $resmhsw["ProdiID"];
			$resprodi = $this->m_prodi->PTL_select($ProdiID);
			$word = explode("~",$resprodi["FormatNIM"]);
			$FormatNIM = $word[2];
			$certificate_id = $resmhsw["certificate_id"];
			if($resmhsw["certificate_id"] == "")
			{
				$kd = $FormatNIM."0".$this->tahun;
				$result = $this->m_mahasiswa->PTL_urut_cid($kd);
				$lastid = $result['LAST'];
				
				$lastnourut = substr($lastid,9,6);
				$nextnourut = $lastnourut + 1;
				$certificate_id = $kd.sprintf('%06s', $nextnourut);
				$data = array(
							'certificate_id' => $certificate_id,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_mahasiswa->PTL_update($MhswID,$data);
			}
			// if($resmhsw["Foto"] == "")
			// {
				// $foto = "foto_umum/user.jpg";
			// }
			// else
			// {
				// $foto = "foto_mahasiswa/".$resmhsw["Foto"];
				// $exist = file_exists_remote(base_url("ptl_storage/$foto"));
				// if($exist)
				// {
					// $foto = "foto_mahasiswa/".$resmhsw["Foto"];
				// }
				// else
				// {
					// $foto = "foto_umum/user.jpg";
				// }
			// }
			$foto = "foto_umum/border.jpg";
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo_international.png"),$this->fpdf->getX()+1,$this->fpdf->getY()+0.9,8.5,3);
			$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX()+24,$this->fpdf->getY()+2.5,2.5,3.5);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(1.3);
			$this->fpdf->Cell(13,0.7,"",0,0,"L");
			$this->fpdf->SetTextColor(255,0,0);
			$this->fpdf->Cell(29,0.7,"ECOLE SUPERIEURE DES ARTS ET TECHNIQUES DE LA MODE FONDEE EN 1841",0,0,"L");
			$this->fpdf->SetTextColor(0,0,0);
			$this->fpdf->Ln();
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(13,0.7,"",0,0,"L");
			$this->fpdf->Cell(29,0.5,"REGISTRATION NUMBER: $certificate_id",0,0,"L");
			$this->fpdf->Ln(3);
			$this->fpdf->SetFont("helvetica","",35);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$this->fpdf->Cell(29,0.5,"CERTIFICATE",0,0,"L");
			$this->fpdf->Line(2,7.3,27.5,7.3);
			$this->fpdf->Line(2,7.35,27.5,7.35);
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$this->fpdf->Cell(6, 1, "THIS IS TO CERTIFY THAT" , 0, "", "L");
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(2, 1, "$resmhsw[Nama]", 0, "", "L");
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$ProgramID = $resmhsw["ProgramID"];
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$Keterangan = "";
			if($resprogram)
			{
				$Keterangan = $resprogram["Keterangan"];
			}
			$ProdiID = $resmhsw["ProdiID"];
			$resprodi = $this->m_prodi->PTL_select($ProdiID);
			$Prodi = "";
			if($resprodi)
			{
				$Prodi = $resprodi["Nama"];
			}
			$this->fpdf->Cell(6, 1, "HAS COMPLETED", 0, "", "L");
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(2, 1, "$Keterangan in $Prodi", 0, "", "L");
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$this->fpdf->Cell(6, 1, "AND IS DECLARED", 0, "", "L");
			$this->fpdf->SetFont("Times","B",12);
			$rowpredikat = $this->m_predikat->PTL_all_active_score_descending();
			$Predikat = "";
			if($rowpredikat)
			{
				foreach($rowpredikat as $rp)
				{
					if(($resmhsw["IPK"] >= $rp->IPKMin) AND ($resmhsw["IPK"] <= $rp->IPKMax))
					{
						$Predikat = $rp->NamaEn;
					}
				}
			}
			$this->fpdf->Cell(2, 1, "as having passed the program with $Predikat results", 0, "", "L");
			$this->fpdf->Ln(1.5);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$this->fpdf->Cell(3, 1, "PERIOD", 0, "", "L");
			$this->fpdf->Cell(4, 1, "FROM", 0, "", "L");
			$this->fpdf->SetFont("Times","B",12);
			$word = explode(" ",$resmhsw["tanggal_buat"]);
			$tanggal_buat = @$word[0];
			$this->fpdf->Cell(4, 1, tgl_lengkap_eng($tanggal_buat), 0, "", "L");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(3, 1, "TO", 0, "", "C");
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(5, 1, tgl_lengkap_eng($resmhsw["tanggal_lulus"]), 0, "", "L");
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX()+0.7,$this->fpdf->getY()+0.39,3,0.24);
			$this->fpdf->Cell(5, 1, "AT ", 0, "", "L");
			$this->fpdf->Line(2,13.3,27.5,13.3);
			$this->fpdf->Ln(1.5);
			$this->fpdf->Cell(0.9,0.7,"",0,0,"L");
			$this->fpdf->Cell(2 , 0.7, "JAKARTA,", "", 0, "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(3.6 , 0.7, tgl_lengkap_eng($resmhsw["tanggal_lulus"]), "", 0, "L");
			$this->fpdf->Ln(3);
			$this->fpdf->SetFont("Times","B",13);
			$this->fpdf->Cell(12,0.7,"",0,0,"L");
			$this->fpdf->Cell(3 , 0.5, "Mayadewi", "", 0, "C");
			$this->fpdf->Cell(5,0.7,"",0,0,"L");
			$this->fpdf->Cell(4 , 0.5, "Anne Viallon", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(12,0.7,"",0,0,"L");
			$this->fpdf->Cell(3 , 0.5, "PRESIDENT", "", 0, "C");
			$this->fpdf->Cell(5,0.7,"",0,0,"L");
			$this->fpdf->Cell(4 , 0.5, "INTERNATIONAL MANAGER", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(12,0.7,"",0,0,"L");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()+0.13,3,0.24);
			$this->fpdf->Cell(3 , 0.5, "", "", 0, "C");
			$this->fpdf->Cell(5,0.7,"",0,0,"L");
			$this->fpdf->Image(base_url("assets/dashboard/img/logo_paris.jpg"),$this->fpdf->getX()+0.5,$this->fpdf->getY()+0.13,3,0.24);
			$this->fpdf->Cell(4 , 0.5, "", "", 0, "C");
			$this->fpdf->Ln(0.5);
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(0.7,0.7,"",0,0,"L");
			$this->fpdf->SetTextColor(0,0,0);
			$this->fpdf->Cell(10.2, 0.5, "BEIJING, BEYROUTH, BORDEAUX, DAMAS, DUBAI, GUANGZHOU, ISTANBUL, ", "", 0, "L");
			$this->fpdf->SetTextColor(255,0,0);
			$this->fpdf->SetFont("Times","IB",8);
			$this->fpdf->Cell(4.3, 0.5, " JAKARTA, ", "", 0, "L");
			$this->fpdf->SetTextColor(0,0,0);
			$this->fpdf->SetFont("Times","",8);
			$this->fpdf->Cell(9, 0.5, " KUALA LUMPUR, KYOTO, LYON, MOSCOU, OSLO, PARIS, RENNES, ROUBAIX, SEOUL, SOUSSE, TOKYO, TUNIS", "", 0, "C");
			$this->fpdf->Output($this->tanggal."_Certificate - $MhswID $resmhsw[Nama].pdf","I");
		}
	}
?>