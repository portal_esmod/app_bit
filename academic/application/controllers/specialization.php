<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Specialization extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->library('log');
			$this->load->model('m_akses');
			$this->load->model('m_akun');
			$this->load->model('m_maintenance');
			$this->load->model('m_spesialisasi');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$data['rowrecord'] = $this->m_spesialisasi->PTL_all_data();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Specialization/v_specialization',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$this->load->view('Portal/v_header');
			$this->load->view('Specialization/v_specialization_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "") { $na = "N"; } else { $na = "Y"; }
			$data = array(
						'SpesialisasiKode' => $this->input->post('SpesialisasiKode'),
						'Nama' => strtoupper($this->input->post('Nama')),
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu,
						'NA' => $na
						);
			$this->m_spesialisasi->PTL_insert($data);
			
			$rowakun = $this->m_akun->PTL_all_akademik();
			$email = "";
			if($rowakun)
			{
				foreach($rowakun as $ra)
				{
					$id_akun = $ra->id_akun;
					$resakses = $this->m_akses->PTL_select_akun($id_akun);
					if($resakses)
					{
						if($resakses['na'] == "N")
						{
							if($ra->email != "")
							{
								$email .= $ra->email.',';
							}
						}
					}
				}
			}
			$id_akun = $_COOKIE["id_akun"];
			$resakun = $this->m_akun->PTL_select($id_akun);
			$EmailAkun = "";
			if($resakun)
			{
				$EmailAkun = $resakun['email'];
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$email");
			// $this->email->cc("$EmailAkun");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject('NEW SPECIALIZATION (NO REPLY)');
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Dear <b>All</b> Academic Staff,
				<br/>
				<br/>
				<br/>
				This year has been opened specialization for <font color='blue'><b>".strtoupper($this->input->post('Nama'))."</b></font>.
				<br/>
				<br/>
				Specialization was created by <b>".$_COOKIE["nama"]."</b>.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data successfully added.","../specialization");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data successfully added.","../specialization");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully added.","../specialization");
				}
			}
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','system');
			$SpesialisasiID = $this->uri->segment(3);
			$result = $this->m_spesialisasi->PTL_select($SpesialisasiID);
			$data['SpesialisasiID'] = $result['SpesialisasiID'];
			$data['SpesialisasiKode'] = $result['SpesialisasiKode'];
			$data['Nama'] = $result['Nama'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Specialization/v_specialization_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$SpesialisasiID = $this->input->post('SpesialisasiID');
			if($this->input->post('NA') == "") { $na = "N"; } else { $na = "Y"; }
			$data = array(
						'SpesialisasiKode' => $this->input->post('SpesialisasiKode'),
						'Nama' => $this->input->post('Nama'),
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu,
						'NA' => $na
						);
			$this->m_spesialisasi->PTL_update($SpesialisasiID,$data);
			
			$rowakun = $this->m_akun->PTL_all_akademik();
			$email = "";
			if($rowakun)
			{
				foreach($rowakun as $ra)
				{
					$id_akun = $ra->id_akun;
					$resakses = $this->m_akses->PTL_select_akun($id_akun);
					if($resakses)
					{
						if($resakses['na'] == "N")
						{
							$email .= $ra->email.',';
						}
					}
				}
			}
			$id_akun = $_COOKIE["id_akun"];
			$resakun = $this->m_akun->PTL_select($id_akun);
			$EmailAkun = "";
			if($resakun)
			{
				$EmailAkun = $resakun['email'];
			}
			$pesan = "";
			$headerpesan = "";
			if($na == "Y")
			{
				$pesan = "<font color='red'><b>deleted</b></font>";
				$headerpesan = "DELETED";
			}
			else
			{
				$pesan = "<font color='green'><b>updated</b></font>";
				$headerpesan = "UPDATED";
			}
			
			$ip_client = $this->log->getIpAdress();
			$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$os_client = $this->log->getOs();
			$browser_client = $this->log->getBrowser();
			$perangkat_client = $this->log->getPerangkat();
			
			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "mail.esmodjakarta.com";
			$config['smtp_port']= "25";
			$config['smtp_timeout']= "5";
			$config['smtp_user']= "no-reply@esmodjakarta.com";
			$config['smtp_pass']= "noreplyesmod";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
			$this->email->to("$email");
			// $this->email->cc("$EmailAkun");
			// $this->email->bcc('lendra.permana@gmail.com');
			$this->email->subject("SPECIALIZATION $headerpesan (NO REPLY)");
			$this->email->message("
				<center>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
					<font color='red'><h2>ACADEMIC</h2></font>
				</center>
				<br/>
				Dear <b>All</b> Academic Staff,
				<br/>
				<br/>
				<br/>
				This year has been $pesan specialization for <font color='blue'><b>".strtoupper($this->input->post('Nama'))."</b></font>.
				<br/>
				<br/>
				Specialization was updated by <b>".$_COOKIE["nama"]."</b>.
				<br/>
				<br/>
				<br/>
				Thanks,
				<br/>
				<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
				<center>
					<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
					Ip: $ip_client
					<br/>
					Hostname: $hostname_client
					<br/>
					OS: $os_client
					<br/>
					Browser: $browser_client
					<br/>
					Devices: $perangkat_client
				</center>
			");
			if($_COOKIE["id_akun"] == "00001111")
			{
				echo warning("Your data successfully updated.","../specialization");
			}
			else
			{
				if($this->email->send())
				{
					echo warning("Your data successfully updated.","../specialization");
				}
				else
				{
					echo warning("Email server is not active. Your data successfully updated.","../specialization");
				}
			}
		}
	}
?>