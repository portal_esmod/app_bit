<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Institutional extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_identity');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_identity->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Institutional/v_institutional',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$this->load->view('Portal/v_header');
			$this->load->view('Institutional/v_institutional_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'Kode' => $this->input->post('Kode'),
						'KodeInstitusi' => $this->input->post('KodeInstitusi'),
						'Nama_IN' => $this->input->post('Nama_IN'),
						'Nama_EN' => $this->input->post('Nama_EN'),
						'Yayasan' => $this->input->post('Yayasan'),
						'TglMulai' => $this->input->post('TglMulai'),
						'Alamat1' => $this->input->post('Alamat1'),
						'Kota' => $this->input->post('Kota'),
						'KodePos' => $this->input->post('KodePos'),
						'Telepon' => $this->input->post('Telepon'),
						'Fax' => $this->input->post('Fax'),
						'Email' => $this->input->post('Email'),
						'Website' => $this->input->post('Website'),
						'NoAkta' => $this->input->post('NoAkta'),
						'TglAkta' => $this->input->post('TglAkta'),
						'NoSah' => $this->input->post('NoSah'),
						'TglSah' => $this->input->post('TglSah'),
						'Logo' => $this->input->post('Logo'),
						'NA' => $na,
						);
			$this->m_identity->PTL_insert($data);
			echo warning("Your data successfully added.","../institutional");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$Kode = $this->uri->segment(3);
			$result = $this->m_identity->PTL_select($Kode);
			$data['Kode'] = $result['Kode'];
			$data['KodeInstitusi'] = $result['KodeInstitusi'];
			$data['Nama_IN'] = $result['Nama_IN'];
			$data['Nama_EN'] = $result['Nama_EN'];
			$data['Yayasan'] = $result['Yayasan'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['Alamat1'] = $result['Alamat1'];
			$data['Kota'] = $result['Kota'];
			$data['KodePos'] = $result['KodePos'];
			$data['Telepon'] = $result['Telepon'];
			$data['Fax'] = $result['Fax'];
			$data['Email'] = $result['Email'];
			$data['Website'] = $result['Website'];
			$data['NoAkta'] = $result['NoAkta'];
			$data['TglAkta'] = $result['TglAkta'];
			$data['NoSah'] = $result['NoSah'];
			$data['TglSah'] = $result['TglSah'];
			$data['Logo'] = $result['Logo'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Institutional/v_institutional_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$Kode = $this->input->post('Kode_lama');
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'Kode' => $this->input->post('Kode'),
						'KodeInstitusi' => $this->input->post('KodeInstitusi'),
						'Nama_IN' => $this->input->post('Nama_IN'),
						'Nama_EN' => $this->input->post('Nama_EN'),
						'Yayasan' => $this->input->post('Yayasan'),
						'TglMulai' => $this->input->post('TglMulai'),
						'Alamat1' => $this->input->post('Alamat1'),
						'Kota' => $this->input->post('Kota'),
						'KodePos' => $this->input->post('KodePos'),
						'Telepon' => $this->input->post('Telepon'),
						'Fax' => $this->input->post('Fax'),
						'Email' => $this->input->post('Email'),
						'Website' => $this->input->post('Website'),
						'NoAkta' => $this->input->post('NoAkta'),
						'TglAkta' => $this->input->post('TglAkta'),
						'NoSah' => $this->input->post('NoSah'),
						'TglSah' => $this->input->post('TglSah'),
						'Logo' => $this->input->post('Logo'),
						'NA' => $na,
						);
			$this->m_identity->PTL_update($Kode,$data);
			echo warning("Your data successfully updated.","../institutional");
		}
	}
?>