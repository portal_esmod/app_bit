<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Scoring extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->tanggal = gmdate("Y-m-d", time()-($ms));
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('tanggal');
			$this->load->library('fpdf');
			$this->load->library('log');
			$this->load->model('m_aktifitas');
			$this->load->model('m_akun');
			$this->load->model('m_catatan2');
			$this->load->model('m_dosen');
			$this->load->model('m_jadwal');
			$this->load->model('m_jenis_mk');
			$this->load->model('m_jenis_presensi');
			$this->load->model('m_kelas');
			$this->load->model('m_khs');
			$this->load->model('m_krs');
			$this->load->model('m_krs2');
			$this->load->model('m_mahasiswa');
			$this->load->model('m_maintenance');
			$this->load->model('m_mk');
			$this->load->model('m_nilai');
			$this->load->model('m_nilai3');
			$this->load->model('m_presensi');
			$this->load->model('m_presensi_mahasiswa');
			$this->load->model('m_program');
			$this->load->model('m_spesialisasi');
			$this->load->model('m_status');
			$this->load->model('m_subjek');
			$this->load->model('m_year');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function ptl_filter_jur()
		{
			$this->authentification();
			$cekjurusan = $this->input->post('cekjurusan');
			$datalog = array(
							'pk1' => $cekjurusan,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI01-01",
							'aktifitas' => "Filter halaman Scoring - Program: $cekjurusan.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekjurusan != "")
			{
				$this->session->set_userdata('score_filter_jur',$cekjurusan);
			}
			else
			{
				$this->session->unset_userdata('score_filter_jur');
			}
			redirect("scoring");
		}
		
		function ptl_filter_tahun()
		{
			$this->authentification();
			$cektahun = $this->input->post('cektahun');
			$datalog = array(
							'pk1' => $cektahun,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI01-02",
							'aktifitas' => "Filter halaman Scoring - Year: $cektahun.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cektahun != "")
			{
				$this->session->set_userdata('score_filter_tahun',$cektahun);
			}
			else
			{
				$this->session->unset_userdata('score_filter_tahun');
			}
			redirect("scoring");
		}
		
		function ptl_filter_subjek()
		{
			$this->authentification();
			$ceksubjek = $this->input->post('ceksubjek');
			$datalog = array(
							'pk1' => $ceksubjek,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI01-03",
							'aktifitas' => "Filter halaman Scoring - Subject: $ceksubjek.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($ceksubjek != "")
			{
				$this->session->set_userdata('score_filter_subjek',$ceksubjek);
			}
			else
			{
				$this->session->unset_userdata('score_filter_subjek');
			}
			redirect("scoring");
		}
		
		function ptl_filter_kelas()
		{
			$this->authentification();
			$cekkelas = $this->input->post('cekkelas');
			$datalog = array(
							'pk1' => $cekkelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI01-04",
							'aktifitas' => "Filter halaman Scoring - Class: $cekkelas.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($cekkelas != "")
			{
				$this->session->set_userdata('score_filter_kelas',$cekkelas);
			}
			else
			{
				$this->session->unset_userdata('score_filter_kelas');
			}
			redirect("scoring");
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','scoring');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI02",
							'aktifitas' => "Mengakses halaman Scoring.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$cekjurusan = $this->session->userdata('score_filter_jur');
			$cekkelas = $this->session->userdata('score_filter_kelas');
			$ck = "_";
			if($cekkelas != "")
			{
				$ck = $cekkelas;
			}
			$word = explode("_",$ck);
			$Tahun = $word[0];
			$Kelas = $word[1];
			$data['Tahun'] = $word[0];
			$data['Kelas'] = $word[1];
			$data['rowtahun'] = $this->m_year->PTL_all_spesifik($cekjurusan);
			$cektahun = $this->session->userdata('score_filter_tahun');
			$ceksubjek = $this->session->userdata('score_filter_subjek');
			$data['rowsubjek'] = $this->m_jadwal->PTL_subjek($cekjurusan,$cektahun,$Tahun,$Kelas);
			$data['rowkelas'] = $this->m_kelas->PTL_all();
			$data['rowrecord'] = $this->m_jadwal->PTL_all_spesifik_scoring($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas);
			$pk1 = "";
			$pk2 = "";
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$this->load->view('Portal/v_header_table');
			if(stristr($_COOKIE["akses"],"LECTURER"))
			{
				if(stristr($_COOKIE["akses"],"COORDINATOR"))
				{
					$this->load->view('Scoring/v_scoring',$data);
				}
				else
				{
					$this->load->view('Scoring/v_scoring_lecturer',$data);
				}
			}
			else
			{
				$this->load->view('Scoring/v_scoring',$data);
			}
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_filter_sequence()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$this->session->set_userdata('score_filter_sequence','Y');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI03-01",
							'aktifitas' => "Filter halaman Scoring - Sequence.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("scoring/ptl_list/$JadwalID");
		}
		
		function ptl_filter_name()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$this->session->unset_userdata('score_filter_sequence');
			$datalog = array(
							'pk1' => $JadwalID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI03-02",
							'aktifitas' => "Filter halaman Scoring - Name.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			redirect("scoring/ptl_list/$JadwalID");
		}
		
		function ptl_list()
		{
			$this->authentification();
			$this->session->set_userdata('menu','scoring');
			$JadwalID = $this->uri->segment(3);
			$data['MKID'] = $this->uri->segment(4);
			$MKID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $MKID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI04",
							'aktifitas' => "Filter halaman Scoring - List.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_jadwal->PTL_select($JadwalID);
			$data['JadwalID'] = $result['JadwalID'];
			$data['TahunID'] = $result['TahunID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['KelasID'] = $result['KelasID'];
			$data['KelasIDGabungan'] = $result['KelasIDGabungan'];
			$data['TahunKe'] = $result['TahunKe'];
			$data['SubjekID'] = $result['SubjekID'];
			$data['DosenID'] = $result['DosenID'];
			$data['TglMulai'] = $result['TglMulai'];
			$data['TglSelesai'] = $result['TglSelesai'];
			$data['RencanaKehadiran'] = $result['RencanaKehadiran'];
			$data['MaxAbsen'] = $result['MaxAbsen'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$SubjekID = $result['SubjekID'];
			$resmk = $this->m_presensi->PTL_select_mk($JadwalID,$MKID);
			if($resmk)
			{
				$data['statusmk'] = 'YES';
			}
			else
			{
				if($MKID == "")
				{
					$data['statusmk'] = 'YES';
				}
				else
				{
					$data['statusmk'] = 'NO';
				}
			}
			$pk1 = $JadwalID;
			$pk2 = $MKID;
			$pk3 = "";
			$menu = $this->uri->segment(1);
			$submenu = "";
			$kode_halaman = "";
			$data['rowlog'] = $this->m_aktifitas->PTL_all_select($pk1,$pk2,$pk3,$menu,$submenu,$kode_halaman);
			$data['rowmk'] = $this->m_mk->PTL_all_select($SubjekID);
			$data['rowrecord'] = $this->m_krs->PTL_all_spesifik_scoring($JadwalID);
			$data['rowcatatan'] = $this->m_catatan2->PTL_all_jadwal($JadwalID);
			
			$s = $this->m_subjek->PTL_select($SubjekID);
			$JenisMKID = $s["JenisMKID"];
			// $resmk = $this->m_jenis_mk->PTL_select($JenisMKID);
			// $namamk = "";
			// if($resmk)
			// {
				// $namamk = strtoupper($resmk['Nama']);
			// }
			if(($JenisMKID == "5") OR ($JenisMKID == "25"))
			{
				$cekgroup = "5";
				$data['rownilai'] = $this->m_nilai3->PTL_all_spesifik($cekgroup);
			}
			else
			{
				$data['rownilai'] = "";
			}
			$this->load->view('Portal/v_header_table');
			$this->load->view('scoring/v_scoring_list',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_list_scoring_set()
		{
			$this->authentification();
			$total = $this->input->post('total');
			$totpresensi = 0;
			$CekPresensiID = $this->input->post('PresensiID');
			for($i=1;$i<=$total;$i++)
			{
				$JenisPresensiID = $this->input->post("JenisPresensiID$i");
				if($JenisPresensiID == '0')
				{
					$CekPresensiID = $this->input->post("CekPresensiID$i");
					$totpresensi++;
				}
			}
			$JadwalID = $this->input->post('JadwalID');
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$NamaSubjek = '';
			$TahunID = '';
			$ProgramID = '';
			$ProdiID = '';
			$kelas = '';
			if($resjadwal)
			{
				$SubjekID = $resjadwal['SubjekID'];
				$ressub = $this->m_subjek->PTL_select($SubjekID);
				if($ressub)
				{
					$NamaSubjek = $ressub['Nama'];
				}
				$TahunID = $resjadwal['TahunID'];
				$ProgramID = $resjadwal['ProgramID'];
				$ProdiID = $resjadwal['ProdiID'];
				$Gabungan = $resjadwal['Gabungan'];
				$KelasIDGabungan = $resjadwal['KelasIDGabungan'];
				$TahunKe = $resjadwal['TahunKe'];
				$KelasID = $resjadwal['KelasID'];
				if($Gabungan == "Y")
				{
					$grup1 = explode(".",$KelasIDGabungan);
					$kelas1 = explode("^",$grup1[0]);
					$kelas2 = explode("^",$grup1[1]);
					$KelasID = @$kelas1[1];
					$k1 = $this->m_kelas->PTL_select_kelas($KelasID);
					$KelasID = @$kelas2[1];
					$k2 = $this->m_kelas->PTL_select_kelas($KelasID);
					$kelas = $kelas1[0].@$k1["Nama"]."/".$kelas2[0].@$k2["Nama"];
					$link_kelas = $kelas1[0].@$k1["Nama"]."/".$kelas2[0].@$k2["Nama"];
					if($kelas == "/")
					{
						$kelas = "<font color='red'><b title='This class not set'>Not set</b></font>";
					}
				}
				else
				{
					$k = $this->m_kelas->PTL_select_kelas($KelasID);
					if($k)
					{
						if($ProgramID == "INT")
						{
							$kelas = "O".$k["Nama"];
						}
						else
						{
							$kelas = $TahunKe.$k["Nama"];
						}
					}
					else
					{
						$kelas = "<font color='red'><b title='This class not set'>Not set</b></font>";
					}
				}
			}
			$MKID = $this->input->post('MKID');
			$resmk = $this->m_mk->PTL_select($MKID);
			$project = '-';
			if($resmk)
			{
				$project = $resmk['Nama'];
			}
			if($totpresensi == 0)
			{
				$Email1 = '';
				$Email2 = '';
				$total = $this->input->post('total');
				$DataAktifitas = "";
				for($i=1;$i<=$total;$i++)
				{
					$KRS2ID = $this->input->post("KRS2ID$i");
					$AdditionalNilai = $this->input->post("AdditionalNilai$i");
					$show = $this->input->post('show');
					$status_show = "N";
					if($show == "Y")
					{
						$status_show = "Y";
					}
					$data = array(
								'AdditionalNilai' => $AdditionalNilai,
								'show' => $status_show
								);
					$this->m_krs2->PTL_update($KRS2ID,$data);
					$reskrs2 = $this->m_krs2->PTL_select($KRS2ID);
					$KRSID = $reskrs2['KRSID'];
					$datakrs2 = array(
									'show' => $status_show
									);
					$this->m_krs2->PTL_update_krs2_scoring($KRSID,$MKID,$datakrs2);
					$reskkrs = $this->m_krs->PTL_select($KRSID);
					if($reskkrs)
					{
						$MhswID = $reskkrs['MhswID'];
						$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
						if($resmhsw)
						{
							$Email1 .= $resmhsw['Email'].',';
							$Email2 .= $resmhsw['Email2'].',';
						}
					}
					$Score = "";
					if($reskrs2['Nilai'] > 0)
					{
						$Score = ($AdditionalNilai + $reskrs2['Nilai']) / 2;
					}
					else
					{
						$Score = $AdditionalNilai + $reskrs2['Nilai'];
					}
					if($Score > 13)
					{
						$grade = "A";
					}
					if(($Score > 10) AND ($Score <= 13))
					{
						$grade = "B";
					}
					if(($Score > 9) AND ($Score <= 10))
					{
						$grade = "C";
					}
					if(($Score > 0) AND ($Score <= 9))
					{
						$grade = "D";
					}
					if($Score == 0)
					{
						$grade = "";
						// $grade = "E";
					}
					$data = array(
								'GradeNilai' => $grade
								);
					$this->m_krs2->PTL_update($KRS2ID,$data);
					$DataAktifitas .= "$MhswID-$Score,$grade;";
				}
				$datalog = array(
								'pk1' => $JadwalID,
								'pk2' => $MKID,
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "SCORI05-Y",
								'aktifitas' => "Mengakses halaman Scoring - List Scoring Set.",
								'data' => $DataAktifitas,
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				$id_akun = $_COOKIE["id_akun"];
				$res2 = $this->m_akun->PTL_select($id_akun);
				$NamaAkun = '';
				$Email3 = '';
				$Email4 = '';
				if($res2)
				{
					$NamaAkun = $res2['nama'];
					$Email3 = $res2['email'];
					$Email4 = $res2['email2'];
				}
				
				$ip_client = $this->log->getIpAdress();
				$hostname_client = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$os_client = $this->log->getOs();
				$browser_client = $this->log->getBrowser();
				$perangkat_client = $this->log->getPerangkat();
				
				$this->load->library('email');
				$config = array();
				$config['charset'] = 'utf-8';
				$config['useragent'] = 'Codeigniter';
				$config['protocol']= "smtp";
				$config['mailtype']= "html";
				$config['smtp_host']= "mail.esmodjakarta.com";
				$config['smtp_port']= "25";
				$config['smtp_timeout']= "5";
				$config['smtp_user']= "no-reply@esmodjakarta.com";
				$config['smtp_pass']= "noreplyesmod";
				$config['crlf']="\r\n"; 
				$config['newline']="\r\n"; 
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from('no-reply@esmodjakarta.com','ESMOD Jakarta');
				$this->email->to("$Email1");
				$this->email->cc("$Email2");
				// $this->email->bcc("lendra.permana@gmail.com,$Email3,$Email4");
				$this->email->subject('YOUR SCORE HAS BEEN SET (NO REPLY)');
				$this->email->message("
					<center>
						<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='80px' width='300px'>
						<font color='red'><h2>ACADEMIC</h2></font>
					</center>
					<br/>
					<br/>
					Dear All <b><font color='green'>".strtoupper($NamaSubjek)."</font></b> student,
					<br/>
					<br/>
					Your score has been set by Mr./Mrs. <b><font color='blue'>".strtoupper($NamaAkun)."</font></b>.
					<br/>
					<br/>
					<b>Detail:</b>
					<br/>
					<table border='1'>
						<tr>
							<td>Subject</td>
							<td>:</td>
							<td>$NamaSubjek</td>
						</tr>
						<tr>
							<td>Year ID</td>
							<td>:</td>
							<td>$TahunID</td>
						</tr>
						<tr>
							<td>Program ID</td>
							<td>:</td>
							<td>$ProgramID</td>
						</tr>
						<tr>
							<td>Prodi ID</td>
							<td>:</td>
							<td>$ProdiID</td>
						</tr>
						<tr>
							<td>Class</td>
							<td>:</td>
							<td>$kelas</td>
						</tr>
						<tr>
							<td>Project</td>
							<td>:</td>
							<td><b><font color='red'>$project</font></b></td>
						</tr>
					</table>
					<br/>
					<br/>
					Please check your PORTAL LMS : http://app.esmodjakarta.com/lms
					<br/>
					<br/>
					<b><font color='red'>You need to login first!</font></b>
					<br/>
					<br/>
					<br/>
					Thanks,
					<br/>
					<img src='https://3.bp.blogspot.com/-M9uO19Gq4u8/WCQM5baIvtI/AAAAAAAABPY/WuOSVlcQWEIKsIZZICgVn12fJ3HiwpTCQCLcB/s1600/logo.png' height='20px' width='120px'>
					<center>
						<b><h3>Supported by <a href='http://lendrapermana.com/'>Lendra Permana - IT Developer</a></h3></b>
						Ip: $ip_client
						<br/>
						Hostname: $hostname_client
						<br/>
						OS: $os_client
						<br/>
						Browser: $browser_client
						<br/>
						Devices: $perangkat_client
					</center>
				");
				if($_COOKIE["id_akun"] == "00001111")
				{
					echo warning("Your data successfully saved.","../scoring/ptl_list/$JadwalID/$MKID");
				}
				else
				{
					if($this->email->send())
					{
						echo warning("Your data successfully saved.","../scoring/ptl_list/$JadwalID/$MKID");
					}
					else
					{
						echo warning("Email server is not active. Your data successfully saved.","../scoring/ptl_list/$JadwalID/$MKID");
					}
				}
			}
			else
			{
				$TahunID = $this->input->post('TahunID');
				$datalog = array(
								'id_akun' => $_COOKIE["id_akun"],
								'nama' => $_COOKIE["nama"],
								'aplikasi' => "ACADEMIC",
								'menu' => $this->uri->segment(1),
								'submenu' => $this->uri->segment(2),
								'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
								'kode_halaman' => "SCORI05-N",
								'aktifitas' => "Mengakses halaman Scoring - List Scoring Set.",
								'data' => "no data",
								'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
								'tanggal_buat' => $this->waktu
								);
				$this->m_aktifitas->PTL_insert($datalog);
				// echo warning("Student attendance has not been set. Data not saved.","../scoring/ptl_list/$JadwalID/$MKID");
				echo warning("THERE ARE NEW STUDENTS ATTENDANCE HAS NOT BEEN SET. Data not saved. You will redirect to attendance. PLEASE RE-SAVE THE ATTENDANCE.","../attendance/ptl_list_attendance/$CekPresensiID/$TahunID/$MKID/warning");
			}
		}
		
		function ptl_list_scoring_update()
		{
			$this->authentification();
			$word = explode("_",$this->uri->segment(3));
			$PresensiMhswID = $word[0];
			$PresensiID = $word[1];
			$TahunID = $word[2];
			$datalog = array(
							'pk1' => $PresensiMhswID,
							'pk2' => $PresensiID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI06",
							'aktifitas' => "Mengakses halaman Scoring - List Scoring Update.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'Removed' => 'Y',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
			echo warning("Your data successfully removed.","../scoring/ptl_list_scoring/".$PresensiID."_".$TahunID);
		}
		
		function ptl_list_scoring_update_active()
		{
			$this->authentification();
			$word = explode("_",$this->uri->segment(3));
			$PresensiMhswID = $word[0];
			$PresensiID = $word[1];
			$TahunID = $word[2];
			$datalog = array(
							'pk1' => $PresensiMhswID,
							'pk2' => $PresensiID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI07",
							'aktifitas' => "Mengakses halaman Scoring - List Scoring Update Active.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data = array(
						'Removed' => 'N',
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_presensi_mahasiswa->PTL_update($PresensiMhswID,$data);
			echo warning("Your data successfully removed.","../scoring/ptl_list_scoring/".$PresensiID."_".$TahunID);
		}
		
		function ptl_pdf_recap()
		{
			$this->authentification();
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$link_kelas = $this->uri->segment(6);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'pk4' => $link_kelas,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI08",
							'aktifitas' => "Mengakses halaman Scoring - PDF Recap.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$ProgramID = $restahun['ProgramID'];
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("P","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(19,0.7,"STUDENT SCORING RECAPITULATION",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(19,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,20,2.5);
			$this->fpdf->Line(1,2.55,20,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(6, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, 1, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $TahunID - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(6, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": ".strtoupper(tgl_singkat_eng($resjadwal["TglMulai"]))." - ".strtoupper(tgl_singkat_eng($resjadwal["TglSelesai"])), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": $restahun[ProgramID] - ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(6, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": $link_kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(19, -2,"SCORING DETAIL", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",7);
			$this->fpdf->Ln();
			$this->fpdf->Cell(0.5, 1, "NO", "LBT", 0, "C");
			$this->fpdf->Cell(6.5, 1, "STUDENT NAME & ID", "LBT", 0, "C");
			$this->fpdf->Cell(1.5, 1, "CLASS", "LBT", 0, "C");
			$this->fpdf->Cell(4, 0.5, "ATTENDANCE", "LT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "MINUS", "LT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "G.VALUE", "LT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "GRADE", "LT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "CREDIT", "LT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "G.POINT", "LTR", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(8.5, 0.5, "", "", 0, "C");
			$this->fpdf->Cell(1, 0.5, "EXC", "LBT", 0, "C");
			$this->fpdf->Cell(1, 0.5, "SIC", "LBT", 0, "C");
			$this->fpdf->Cell(1, 0.5, "ABS", "LBT", 0, "C");
			$this->fpdf->Cell(1, 0.5, "LAT", "LBT", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "", "LB", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "", "LB", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "", "LB", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "", "LB", 0, "C");
			$this->fpdf->Cell(1.3, 0.5, "", "LBR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			$this->fpdf->SetFont("Times","",7);
			$JenisMKID = $ressubjek["JenisMKID"];
			foreach($result as $r)
			{
				$MhswID = $r->MhswID;
				$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
				if($resmhsw["Foto"] == "")
				{
					$foto = "foto_umum/user.jpg";
				}
				else
				{
					$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					$exist = file_exists_remote(base_url("ptl_storage/$foto"));
					if($exist)
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
					}
					else
					{
						$foto = "foto_umum/user.jpg";
					}
				}
				$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
				$KelasID = $reskhs["KelasID"];
				$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
				$kelas = "";
				if($reskelas)
				{
					$kelas = $reskelas["Nama"];
				}
				$KRSID = $r->KRSID;
				$rexc1 = $this->m_presensi_mahasiswa->PTL_all_presensi_excuse($KRSID,$MhswID);
				if($rexc1) { $texc = count($rexc1); } else { $texc = 0; }
				$rsic1 = $this->m_presensi_mahasiswa->PTL_all_presensi_sick($KRSID,$MhswID);
				if($rsic1) { $tsic = count($rsic1); } else { $tsic = 0; }
				$rabs1 = $this->m_presensi_mahasiswa->PTL_all_presensi_absent($KRSID,$MhswID);
				if($rabs1) { $tabs = count($rabs1); } else { $tabs = 0; }
				$rlat1 = $this->m_presensi_mahasiswa->PTL_all_presensi_late($KRSID,$MhswID);
				if($rlat1) { $tlat = count($rlat1); } else { $tlat = 0; }
				
				$JenisPresensiID = "E";
				$resp1 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
				$pr1 = 0; if($resp1){ $pr1 = $resp1["Score"]; }
				$tottexc = $texc * $pr1;
				$JenisPresensiID = "S";
				$resp2 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
				$pr2 = 0; if($resp2){ $pr2 = $resp2["Score"]; }
				$tottsic = $tsic * $pr2;
				$JenisPresensiID = "A";
				$resp3 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
				$pr3 = 0; if($resp3){ $pr3 = $resp3["Score"]; }
				$tottabs = $tabs * $pr3;
				$JenisPresensiID = "L";
				$resp4 = $this->m_nilai3->PTL_select_attendance($JenisMKID,$JenisPresensiID);
				$pr4 = 0; if($resp4){ $pr4 = $resp4["Score"]; }
				$tottlat = $tlat * $pr4;
				$mp = $tottexc + $tottsic + $tottabs + $tottlat;
				$rsub = $this->m_subjek->PTL_select($SubjekID);
				$sks = $rsub["SKS"];
				
				$rowsub = $this->m_subjek->PTL_select($SubjekID);
				$KurikulumID = "";
				if($rowsub)
				{
					$KurikulumID = $rowsub['KurikulumID'];
				}
				$rowmk = $this->m_mk->PTL_all_select($SubjekID);
				$NilAkhir = 0;
				$NilBagi = 0;
				if($rowmk)
				{
					foreach($rowmk as $rm)
					{
						if(($TahunID == 19) OR ($TahunID == 20) OR ($TahunID == 28) OR ($TahunID == 29))
						{
							$MKID = $rm->MKID;
							$rowkrs2 = $this->m_krs2->PTL_all_select_scoring($KRSID,$MKID);
							$Nilai = 0;
							$AddNilai = 0;
							if($rowkrs2)
							{
								$totNilai = 0;
								$nkrs2 = 0;
								foreach($rowkrs2 as $rkrs2)
								{
									if($rkrs2->Nilai > 0)
									{
										$totNilai = $totNilai + $rkrs2->Nilai;
										$nkrs2++;
									}
									$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
								}
								if($nkrs2 > 0)
								{
									$Nilai = $totNilai / $nkrs2;
								}
							}
							$tn = $Nilai + $AddNilai;
							if($rm->Optional == "Y")
							{
								if($tn != 0)
								{
									$NilAkhir = $NilAkhir + $tn;
									$NilBagi++;
								}
							}
							else
							{
								$NilAkhir = $NilAkhir + $tn;
								$NilBagi++;
							}
						}
						else
						{
							$CekMKID = $rm->MKID;
							$rescekmk = $this->m_presensi->PTL_select_cek_mk($JadwalID,$CekMKID);
							if($rescekmk)
							{
								$MKID = $rm->MKID;
								$rowkrs2 = $this->m_krs2->PTL_all_select_scoring($KRSID,$MKID);
								$Nilai = 0;
								$AddNilai = 0;
								if($rowkrs2)
								{
									$totNilai = 0;
									$nkrs2 = 0;
									foreach($rowkrs2 as $rkrs2)
									{
										if($rkrs2->Nilai > 0)
										{
											$totNilai = $totNilai + $rkrs2->Nilai;
											$nkrs2++;
										}
										$AddNilai = $AddNilai + $rkrs2->AdditionalNilai;
									}
									if($nkrs2 > 0)
									{
										$Nilai = $totNilai / $nkrs2;
									}
									$tn = $Nilai + $AddNilai;
									if($rm->Optional == "Y")
									{
										if($tn != 0)
										{
											$NilAkhir = $NilAkhir + $tn;
											$NilBagi++;
										}
									}
									else
									{
										$NilAkhir = $NilAkhir + $tn;
										$NilBagi++;
									}
								}
							}
						}
					}
				}
				if($NilAkhir > 0)
				{
					$gradevalueAkhir = number_format((($NilAkhir / $NilBagi)),2);
				}
				else
				{
					$gradevalueAkhir = 0.00;
				}
				$rownilai = $this->m_nilai->PTL_all_evaluation($KurikulumID);
				$GradeNilai = "";
				if($rownilai)
				{
					foreach($rownilai as $rn)
					{
						if(($gradevalueAkhir >= $rn->NilaiMin) AND ($gradevalueAkhir <= $rn->NilaiMax))
						{
							$GradeNilai = $rn->Nama;
						}
					}
				}
				
				if($no == $tot)
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 2, $no, "LT", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LT", 0, "L");
					$this->fpdf->Cell(5, 1, $r->MhswID, "LT", 0, "L");
					$this->fpdf->Cell(1.5, 1, $reskhs["TahunKe"].$kelas, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $texc, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tsic, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tabs, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tlat, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $mp, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $gradevalueAkhir, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $GradeNilai, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $sks, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $sks, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2, 1, "", "BR", 0, "C");
					$this->fpdf->Cell(5, 1, $resmhsw["Nama"], "B", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LB", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LBR", 0, "L");
				}
				else
				{
					$this->fpdf->Ln();
					$this->fpdf->Cell(0.5, 2, $no, "LT", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LT", 0, "L");
					$this->fpdf->Cell(5, 1, $r->MhswID, "LT", 0, "L");
					$this->fpdf->Cell(1.5, 1, $reskhs["TahunKe"].$kelas, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $texc, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tsic, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tabs, "LT", 0, "C");
					$this->fpdf->Cell(1, 1, $tlat, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $mp, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $gradevalueAkhir, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $GradeNilai, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $sks, "LT", 0, "C");
					$this->fpdf->Cell(1.3, 1, $sks, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2, 1, "", "R", 0, "C");
					$this->fpdf->Cell(5, 1, $resmhsw["Nama"], "", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "L", 0, "L");
					$this->fpdf->Cell(1.3, 1, "", "LR", 0, "L");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Scoring_Recap_$id_akun-$nama.pdf","I");
		}
		
		function ptl_pdf_recap_dpm()
		{
			$JadwalID = $this->uri->segment(3);
			$SubjekID = $this->uri->segment(4);
			$TahunID = $this->uri->segment(5);
			$datalog = array(
							'pk1' => $JadwalID,
							'pk2' => $SubjekID,
							'pk3' => $TahunID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "SCORI09",
							'aktifitas' => "Mengakses halaman Scoring - PDF Recap DPM.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$cekjurusan = $this->session->userdata('att_filter_jur');
			$cektahun = $this->session->userdata('att_filter_tahun');
			$ProgramID = $cekjurusan;
			$kelas = $this->uri->segment(6);
			if($ProgramID == "INT")
			{
				$kelas = "O".substr($this->uri->segment(6),1);
			}
			$resjadwal = $this->m_jadwal->PTL_select($JadwalID);
			$DosenID = $resjadwal["DosenID"];
			$resdosen = $this->m_dosen->PTL_select($DosenID);
			$ressubjek = $this->m_subjek->PTL_select($SubjekID);
			$restahun = $this->m_year->PTL_select($TahunID);
			$resprogram = $this->m_program->PTL_select($ProgramID);
			$result = $this->m_krs->PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID);
			date_default_timezone_set('Asia/Jakarta');
			$this->fpdf->FPDF("L","cm","A4");
			$this->fpdf->SetMargins(1,1,1);
			$this->fpdf->AliasNbPages();
			$this->fpdf->AddPage();
			$this->fpdf->Image(base_url("assets/dashboard/img/logo.png"),$this->fpdf->getX(),$this->fpdf->getY()-0.6,4.5,1);
			$this->fpdf->SetFont("Times","B",12);
			$this->fpdf->Cell(29,0.7,"STUDENT SCORING LIST",0,0,"C");
			$this->fpdf->Ln();
			$id_akun = $_COOKIE["id_akun"];
			$nama = $_COOKIE["nama"];
			$this->fpdf->SetFont("helvetica","",10);
			$this->fpdf->Cell(29,0.5,"ACADEMIC",0,0,"C");
			$this->fpdf->Line(1,2.5,28.5,2.5);
			$this->fpdf->Line(1,2.55,28.5,2.55);
			
			$this->fpdf->Ln(1);
			$this->fpdf->SetFont("Times","",9);
			$this->fpdf->Cell(3, 1, "SUBJECT" , 0, "", "L");
			$this->fpdf->Cell(2, 1, ": $SubjekID - ".$ressubjek["SubjekKode"]." - ".$ressubjek["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 1, "", 0, "", "L");
			$this->fpdf->Cell(2, 1, "CLASS", 0, "", "L");
			$this->fpdf->Cell(3, 1, ": $kelas", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, 0, "ACADEMIC YEAR" , 0, "", "L");
			$this->fpdf->Cell(2, 0, ": $cektahun - ".$restahun["Nama"], 0, "", "L");
			$this->fpdf->Cell(15, 0, "", 0, "", "L");
			$this->fpdf->Cell(2, 0, "TEACHER", 0, "", "L");
			$this->fpdf->Cell(3, 0, ": ".$resdosen["Nama"], 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -1, "PROGRAM", 0, "", "L");
			$this->fpdf->Cell(2, -1, ": ".strtoupper($resprogram["Nama"]), 0, "", "L");
			$this->fpdf->Cell(15, -1, "", 0, "", "L");
			$this->fpdf->Cell(2, -1, "DATE", 0, "", "L");
			$this->fpdf->Cell(3, -1, ": ".tgl_indo($resjadwal["TglMulai"])." - ".tgl_indo($resjadwal["TglSelesai"]), 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -2, "Please ask your student to sign in each session. In case of student who coming late, give coming time remarks inside the box.  Example :", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -3, "[Student Signature]", 0, "", "L");
			$this->fpdf->Ln(1);
			$this->fpdf->Cell(3, -4, "Late 12:20", 0, "", "L");
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Cell(9.5, -4, "", 0, "", "L");
			$this->fpdf->Cell(3, -4, "ATTENDANCE DETAIL", 0, "", "L");
			$this->fpdf->Cell(29, -3,"", 0, 0, "C");
			$this->fpdf->Ln(1.4);
			$this->fpdf->SetFont("Times","B",9);
			$this->fpdf->Ln();
			$this->fpdf->Cell(1, 1, "NO", "LBTR", 0, "C");
			$this->fpdf->Cell(8, 1, "STUDENT NAME & ID", "LBTR", 0, "C");
			$this->fpdf->Cell(2, 1, "CLASS", "LBTR", 0, "C");
			$this->fpdf->Cell(16.5, 0.5, "DATE", "LBTR", 0, "C");
			$this->fpdf->Ln();
			$nn = 0;
			$tgl1 = "";
			$tgl2 = "";
			$tgl3 = "";
			$tgl4 = "";
			$tgl5 = "";
			$tgl6 = "";
			$tgl7 = "";
			$tgl8 = "";
			$tgl9 = "";
			$tgl10 = "";
			$tgl11 = "";
			foreach($result as $r)
			{
				$JadwalID = $r->JadwalID;
				$rowatt = $this->m_presensi->PTL_all_select($JadwalID);
				if($rowatt)
				{
					foreach($rowatt as $ra)
					{
						$nn++;
						if($nn == 1){ $tgl1 = explode("-",$ra->Tanggal); }
						if($nn == 2){ $tgl2 = explode("-",$ra->Tanggal); }
						if($nn == 3){ $tgl3 = explode("-",$ra->Tanggal); }
						if($nn == 4){ $tgl4 = explode("-",$ra->Tanggal); }
						if($nn == 5){ $tgl5 = explode("-",$ra->Tanggal); }
						if($nn == 6){ $tgl6 = explode("-",$ra->Tanggal); }
						if($nn == 7){ $tgl7 = explode("-",$ra->Tanggal); }
						if($nn == 8){ $tgl8 = explode("-",$ra->Tanggal); }
						if($nn == 9){ $tgl9 = explode("-",$ra->Tanggal); }
						if($nn == 10){ $tgl10 = explode("-",$ra->Tanggal); }
						if($nn == 11){ $tgl11 = explode("-",$ra->Tanggal); }
					}
				}
			}
			$this->fpdf->Cell(11, 0.5, "", "", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl1[0],2,2)."/".@$tgl1[1]."/".@$tgl1[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl2[0],2,2)."/".@$tgl2[1]."/".@$tgl2[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl3[0],2,2)."/".@$tgl3[1]."/".@$tgl3[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl4[0],2,2)."/".@$tgl4[1]."/".@$tgl4[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl5[0],2,2)."/".@$tgl5[1]."/".@$tgl5[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl6[0],2,2)."/".@$tgl6[1]."/".@$tgl6[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl7[0],2,2)."/".@$tgl7[1]."/".@$tgl7[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl8[0],2,2)."/".@$tgl8[1]."/".@$tgl8[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl9[0],2,2)."/".@$tgl9[1]."/".@$tgl9[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl10[0],2,2)."/".@$tgl10[1]."/".@$tgl10[2], "LBTR", 0, "C");
			$this->fpdf->Cell(1.5, 0.5, substr(@$tgl11[0],2,2)."/".@$tgl11[1]."/".@$tgl11[2], "LBTR", 0, "C");
			$no = 1;
			$tot = count($result);
			$ta = 0;
			$t = 0;
			foreach($result as $r)
			{
				$MhswID = $r->MhswID;
				$JadwalID = $r->JadwalID;
				$rowatt = $this->m_presensi->PTL_all_select($JadwalID);
				$n = 0;
				$n1 = "";
				$n2 = "";
				$n3 = "";
				$n4 = "";
				$n5 = "";
				$n6 = "";
				$n7 = "";
				$n8 = "";
				$n9 = "";
				$n10 = "";
				$n11 = "";
				if($rowatt)
				{
					foreach($rowatt as $ra)
					{
						$n++;
						$PresensiID = $ra->PresensiID;
						$respm = $this->m_presensi_mahasiswa->PTL_select_att($JadwalID,$PresensiID,$MhswID);
						$nilai = "-";
						if($respm)
						{
							if($respm["JenisPresensiID"] == "")
							{
								$nilai = "-";
							}
							else
							{
								$nilai = $respm["JenisPresensiID"];
							}
						}
						if($n == 1){ $n1 = $nilai; }
						if($n == 2){ $n2 = $nilai; }
						if($n == 3){ $n3 = $nilai; }
						if($n == 4){ $n4 = $nilai; }
						if($n == 5){ $n5 = $nilai; }
						if($n == 6){ $n6 = $nilai; }
						if($n == 7){ $n7 = $nilai; }
						if($n == 8){ $n8 = $nilai; }
						if($n == 9){ $n9 = $nilai; }
						if($n == 10){ $n10 = $nilai; }
						if($n == 11){ $n11 = $nilai; }
					}
				}
				if($no == $tot)
				{
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n1, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n2, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n3, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n4, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n5, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n6, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n7, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n8, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n9, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n10, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n11, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				else
				{
					$resmhsw = $this->m_mahasiswa->PTL_select($MhswID);
					if($resmhsw["Foto"] == "")
					{
						$foto = "foto_umum/user.jpg";
					}
					else
					{
						$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						$exist = file_exists_remote(base_url("ptl_storage/$foto"));
						if($exist)
						{
							$foto = "foto_mahasiswa/".$resmhsw["Foto"];
						}
						else
						{
							$foto = "foto_umum/user.jpg";
						}
					}
					$reskhs = $this->m_khs->PTL_select_tahun($MhswID,$TahunID);
					$KelasID = $reskhs["KelasID"];
					$reskelas = $this->m_kelas->PTL_select_kelas($KelasID);
					$TahunKe = $reskhs["TahunKe"];
					if($ProgramID == "INT")
					{
						$TahunKe = "O";
					}
					$kelas = "";
					if($reskelas)
					{
						$kelas = $reskelas["Nama"];
					}
					$this->fpdf->Ln();
					$this->fpdf->Cell(1, 2, $no, "LBTR", 0, "C");
					$this->fpdf->Image(base_url("ptl_storage/$foto"),$this->fpdf->getX(),$this->fpdf->getY(),1.5,2);
					$this->fpdf->Cell(1.5, 2, "", "LBTR", 0, "L");
					$this->fpdf->Cell(6.5, 1, $r->MhswID, "LTR", 0, "L");
					$this->fpdf->Cell(2, 1, $TahunKe.$kelas, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n1, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n2, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n3, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n4, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n5, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n6, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n7, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n8, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n9, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n10, "LTR", 0, "C");
					$this->fpdf->Cell(1.5, 1, $n11, "LTR", 0, "C");
					$this->fpdf->Ln();
					$this->fpdf->Cell(2.5, 1, "", "R", 0, "C");
					$this->fpdf->Cell(6.5, 1, strtoupper($resmhsw["Nama"]), "LBR", 0, "L");
					$this->fpdf->Cell(2, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
					$this->fpdf->Cell(1.5, 1, "", "LBR", 0, "L");
				}
				$no++;
			}
			$this->fpdf->SetFont("Times","B",10);
			$this->fpdf->Ln(2);
			$this->fpdf->Cell(3.6 , 0.7, "ACKNOWLEDGE,", "", 0, "C");
			$this->fpdf->Ln(3);
			$this->fpdf->Cell(3.6 , 0.5, "PATRICE DESILLES", "", 0, "C");
			$this->fpdf->Ln();
			$this->fpdf->Cell(3.6 , 0.5, "ACADEMIC MANAGER", "T", 0, "C");
			
			$this->fpdf->SetY(-3);
			$this->fpdf->SetFont("Times","",10);
			$this->fpdf->Cell(9.5, 0.5, "Printed on : ".date("d/m/Y H:i:s"),0,"LR","L");
			$this->fpdf->Cell(9.5, 0.5, "Page ".$this->fpdf->PageNo()."/{nb}",0,0,"R");
			$this->fpdf->Output($this->tanggal."_Attendance_DPM_$id_akun-$nama.pdf","I");
		}
	}
?>