<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Regular extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->model('m_maintenance');
			$this->load->model('m_prodi');
			$this->load->model('m_program');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowrecord'] = $this->m_prodi->PTL_all_list();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Regular/v_regular',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$this->load->view('Portal/v_header');
			$this->load->view('Regular/v_regular_form',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			if($this->input->post('Kombinasi') == "")
			{
				$Kombinasi = "N";
			}
			else
			{
				$Kombinasi = "Y";
			}
			if($this->input->post('CekPrasyarat') == "")
			{
				$CekPrasyarat = "N";
			}
			else
			{
				$CekPrasyarat = "Y";
			}
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'ProdiID' => strtoupper($this->input->post('ProdiID')),
						'ProgramID' => $this->input->post('ProgramID'),
						'Kombinasi' => $Kombinasi,
						'KodeID' => $this->input->post('KodeID'),
						'Nama' => $this->input->post('Nama'),
						'Jenjang' => $this->input->post('Jenjang'),
						'Gelar' => $this->input->post('Gelar'),
						'FormatNIM' => $this->input->post('FormatNIM'),
						'DapatPindahProdi' => $this->input->post('DapatPindahProdi'),
						'NamaSesi' => $this->input->post('NamaSesi'),
						'CekPrasyarat' => $CekPrasyarat,
						'TotalSKS' => $this->input->post('TotalSKS'),
						'DefSKS' => $this->input->post('DefSKS'),
						'DefKehadiran' => $this->input->post('DefKehadiran'),
						'DefMaxAbsen' => $this->input->post('DefMaxAbsen'),
						'Pejabat' => $this->input->post('Pejabat'),
						'Jabatan' => $this->input->post('Jabatan'),
						'BatasStudi' => $this->input->post('BatasStudi'),
						'JumlahSesi' => $this->input->post('JumlahSesi'),
						'NoSKDikti' => $this->input->post('NoSKDikti'),
						'TglSKDikti' => $this->input->post('TglSKDikti'),
						'NoSKBAN' => $this->input->post('NoSKBAN'),
						'TglSKBAN' => $this->input->post('TglSKBAN'),
						'Akreditasi' => $this->input->post('Akreditasi'),
						'NA' => $na,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_prodi->PTL_insert($data);
			echo warning("Your data successfully added.","../regular");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','master');
			$ProdiID = $this->uri->segment(3);
			$result = $this->m_prodi->PTL_select($ProdiID);
			$data['ProdiID'] = $result['ProdiID'];
			$data['ProgramID'] = $result['ProgramID'];
			$data['Kombinasi'] = $result['Kombinasi'];
			$data['KodeID'] = $result['KodeID'];
			$data['Nama'] = $result['Nama'];
			$data['Jenjang'] = $result['Jenjang'];
			$data['Gelar'] = $result['Gelar'];
			$data['FormatNIM'] = $result['FormatNIM'];
			$data['DapatPindahProdi'] = $result['DapatPindahProdi'];
			$data['NamaSesi'] = $result['NamaSesi'];
			$data['CekPrasyarat'] = $result['CekPrasyarat'];
			$data['TotalSKS'] = $result['TotalSKS'];
			$data['DefSKS'] = $result['DefSKS'];
			$data['DefKehadiran'] = $result['DefKehadiran'];
			$data['DefMaxAbsen'] = $result['DefMaxAbsen'];
			$data['Pejabat'] = $result['Pejabat'];
			$data['Jabatan'] = $result['Jabatan'];
			$data['BatasStudi'] = $result['BatasStudi'];
			$data['JumlahSesi'] = $result['JumlahSesi'];
			$data['NoSKDikti'] = $result['NoSKDikti'];
			$data['TglSKDikti'] = $result['TglSKDikti'];
			$data['NoSKBAN'] = $result['NoSKBAN'];
			$data['TglSKBAN'] = $result['TglSKBAN'];
			$data['Akreditasi'] = $result['Akreditasi'];
			$data['login_buat'] = $result['login_buat'];
			$data['tanggal_buat'] = $result['tanggal_buat'];
			$data['login_edit'] = $result['login_edit'];
			$data['tanggal_edit'] = $result['tanggal_edit'];
			$data['NA'] = $result['NA'];
			$data['rowprogram'] = $this->m_program->PTL_all_by_urutan();
			$this->load->view('Portal/v_header');
			$this->load->view('Regular/v_regular_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_update()
		{
			$this->authentification();
			$ProdiID = $this->input->post('ProdiID');
			if($this->input->post('Kombinasi') == "")
			{
				$Kombinasi = "N";
			}
			else
			{
				$Kombinasi = "Y";
			}
			if($this->input->post('CekPrasyarat') == "")
			{
				$CekPrasyarat = "N";
			}
			else
			{
				$CekPrasyarat = "Y";
			}
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$data = array(
						'ProgramID' => $this->input->post('ProgramID'),
						'Kombinasi' => $Kombinasi,
						'KodeID' => $this->input->post('KodeID'),
						'Nama' => $this->input->post('Nama'),
						'Jenjang' => $this->input->post('Jenjang'),
						'Gelar' => $this->input->post('Gelar'),
						'FormatNIM' => $this->input->post('FormatNIM'),
						'DapatPindahProdi' => $this->input->post('DapatPindahProdi'),
						'NamaSesi' => $this->input->post('NamaSesi'),
						'CekPrasyarat' => $CekPrasyarat,
						'TotalSKS' => $this->input->post('TotalSKS'),
						'DefSKS' => $this->input->post('DefSKS'),
						'DefKehadiran' => $this->input->post('DefKehadiran'),
						'DefMaxAbsen' => $this->input->post('DefMaxAbsen'),
						'Pejabat' => $this->input->post('Pejabat'),
						'Jabatan' => $this->input->post('Jabatan'),
						'BatasStudi' => $this->input->post('BatasStudi'),
						'JumlahSesi' => $this->input->post('JumlahSesi'),
						'NoSKDikti' => $this->input->post('NoSKDikti'),
						'TglSKDikti' => $this->input->post('TglSKDikti'),
						'NoSKBAN' => $this->input->post('NoSKBAN'),
						'TglSKBAN' => $this->input->post('TglSKBAN'),
						'Akreditasi' => $this->input->post('Akreditasi'),
						'NA' => $na,
						'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_edit' => $this->waktu
						);
			$this->m_prodi->PTL_update($ProdiID,$data);
			echo warning("Your data successfully updated.","../regular");
		}
	}
?>