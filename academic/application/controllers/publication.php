<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	Class Publication extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$is_login_academic = $_COOKIE["is_login_academic"];
			if ($is_login_academic!=='logged')
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			if($_COOKIE["id_akun"] == "")
			{
				$this->session->set_userdata('is_login_academic','notlogged');
				redirect('login');
			}
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$this->waktu = gmdate("Y-m-d H:i:s", time()-($ms));
			$this->load->helper('download');
			$this->load->model('m_aktifitas');
			$this->load->model('m_maintenance');
			$this->load->model('m_berita');
		}
		
		function authentification()
		{
			$is_login = "is_login_academic";
			$result = $this->m_maintenance->PTL_select($is_login);
			if($result['na'] == "Y")
			{
				echo warning('Sorry! The program was MAINTENANCE...','../login/ptl_maintenance');
			}
		}
		
		function index()
		{
			$this->authentification();
			$this->session->set_userdata('menu','publication');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI01",
							'aktifitas' => "Mengakses halaman Publication.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$data['rowrecord'] = $this->m_berita->PTL_all();
			$this->load->view('Portal/v_header_table');
			$this->load->view('Publication/v_publication',$data);
			$this->load->view('Portal/v_footer_table');
		}
		
		function ptl_form()
		{
			$this->authentification();
			$this->session->set_userdata('menu','publication');
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI02",
							'aktifitas' => "Mengakses halaman Publication - Form.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$this->load->view('Portal/v_header');
			$this->load->view('Publication/v_publication_form');
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_insert()
		{
			$this->authentification();
			$datalog = array(
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI03",
							'aktifitas' => "Mengakses halaman Publication - Insert.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/berita/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "INFO_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '5120000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				$dwn = "";
			}
			if($this->input->post('NA') == "")
			{
				$na = "N";
			}
			else
			{
				$na = "Y";
			}
			$error = $this->upload->display_errors('','.');
			$data = array(
						'JudulBerita' => $this->input->post('JudulBerita'),
						'IsiBerita' => $this->input->post('IsiBerita'),
						'UploadFile' => $dwn,
						'NA' => $na,
						'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
						'tanggal_buat' => $this->waktu
						);
			$this->m_berita->PTL_insert($data);
			echo warning("Your data successfully added. $error","../publication");
		}
		
		function ptl_edit()
		{
			$this->authentification();
			$this->session->set_userdata('menu','publication');
			$BeritaID = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $BeritaID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI04",
							'aktifitas' => "Mengakses halaman Publication - Edit: $BeritaID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$result = $this->m_berita->PTL_select($BeritaID);
			$data['BeritaID'] = $result['BeritaID'];
			$data['JudulBerita'] = $result['JudulBerita'];
			$data['IsiBerita'] = $result['IsiBerita'];
			$data['UploadFile'] = $result['UploadFile'];
			$data['NA'] = $result['NA'];
			$this->load->view('Portal/v_header');
			$this->load->view('Publication/v_publication_edit',$data);
			$this->load->view('Portal/v_footer');
		}
		
		function ptl_download()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			$datalog = array(
							'pk1' => $filelink,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI05",
							'aktifitas' => "Mengakses halaman Publication - Download: $filelink.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$exist = file_exists_remote(base_url("./ptl_storage/berita/".$direktori."/".$file));
				if($exist)
				{
					$data = file_get_contents("./ptl_storage/berita/".$direktori."/".$file);
					force_download($file,$data);
				}
				else
				{
					echo warning('File does not exist in directory...','../publication');
				}
			}
			else
			{
				echo warning('File does not exist...','../publication');
			}
		}
		
		function ptl_download2()
		{
			$this->authentification();
			$filelink = $this->uri->segment(3);
			$BeritaID = $this->uri->segment(4);
			$datalog = array(
							'pk1' => $filelink,
							'pk2' => $BeritaID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI06",
							'aktifitas' => "Mengakses halaman Publication - Download: $filelink - $BeritaID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			if($filelink)
			{
				$setelah = substr($filelink,8);
				$string = $setelah;
				$change = array(
				"&#40;" => "(",
				"&#41;" => ")",
				"%20" => " "
				);
				$file = strtr($string,$change);
				$direktori = substr($filelink,0,7);
				$exist = file_exists_remote(base_url("./ptl_storage/berita/".$direktori."/".$file));
				if($exist)
				{
					$data = file_get_contents("./ptl_storage/berita/".$direktori."/".$file);
					force_download($file,$data);
				}
				else
				{
					echo warning('File does not exist in directory...',"../publication/ptl_edit/$BeritaID");
				}
			}
			else
			{
				echo warning('File does not exist...',"../publication/ptl_edit/$BeritaID");
			}
		}
		
		function ptl_update()
		{
			$this->authentification();
			$BeritaID = $this->input->post('BeritaID');
			$datalog = array(
							'pk1' => $BeritaID,
							'id_akun' => $_COOKIE["id_akun"],
							'nama' => $_COOKIE["nama"],
							'aplikasi' => "ACADEMIC",
							'menu' => $this->uri->segment(1),
							'submenu' => $this->uri->segment(2),
							'url' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
							'kode_halaman' => "PUBLI07",
							'aktifitas' => "Mengakses halaman Publication - Update: $BeritaID.",
							'data' => "no data",
							'login_buat' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_buat' => $this->waktu
							);
			$this->m_aktifitas->PTL_insert($datalog);
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tgl = gmdate("ymdHis", time()-($ms));
			$storage = gmdate("Y-m", time()-($ms));
			$admin_dp = 'ptl_storage/berita/'.$storage;
			if(!is_dir($admin_dp) )
			{
				mkdir($admin_dp, DIR_WRITE_MODE);
			}
			$download = $_FILES['userfile']['name'];
			$downloadin = "INFO_".$tgl."_".str_replace(' ','_',$download);
			
			$dwn = $storage."_".$downloadin;
			$config['upload_path'] = $admin_dp;
			$config['allowed_types'] = '*';
			$config['max_size'] = '5120000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['file_name'] = $downloadin;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				if($this->input->post('NA') == "")
				{
					$na = "N";
				}
				else
				{
					$na = "Y";
				}
				$error = $this->upload->display_errors('','.');
				if($this->input->post('NA') == "")
				{
					$na = "N";
				}
				else
				{
					$na = "Y";
				}
				$data = array(
							'JudulBerita' => $this->input->post('JudulBerita'),
							'IsiBerita' => $this->input->post('IsiBerita'),
							'NA' => $na,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_berita->PTL_update($BeritaID,$data);
				echo warning("MESSAGE: Besides the data file has been successfully changed. ".$error,"../publication");
			}
			else
			{
				if($this->input->post('NA') == "")
				{
					$na = "N";
				}
				else
				{
					$na = "Y";
				}
				if($this->input->post('NA') == "")
				{
					$na = "N";
				}
				else
				{
					$na = "Y";
				}
				$data = array(
							'JudulBerita' => $this->input->post('JudulBerita'),
							'IsiBerita' => $this->input->post('IsiBerita'),
							'UploadFile' => $dwn,
							'NA' => $na,
							'login_edit' => $_COOKIE["id_akun"]."_".$_COOKIE["nama"]."_".$_COOKIE["akses"],
							'tanggal_edit' => $this->waktu
							);
				$this->m_berita->PTL_update($BeritaID,$data);
				echo warning("Your data successfully updated.","../publication");
			}
		}
	}
?>