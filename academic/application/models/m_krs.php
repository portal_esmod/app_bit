<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_krs extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs');
			return $query->result();
		}
		
		function PTL_all_jadwal($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$query = $this->db->get('ac_krs');
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring($JadwalID)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_krs as a');
			$this->db->where('a.JadwalID',$JadwalID);
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_recap($JadwalID,$SubjekID,$TahunID)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_krs as a');
			$this->db->where('a.JadwalID',$JadwalID);
			$this->db->where('a.SubjekID',$SubjekID);
			$this->db->where('a.TahunID',$TahunID);
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_select_spesifik($MhswID,$KHSID)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_krs as a');
			if($MhswID != "")
			{
				$this->db->where('a.MhswID',$MhswID);
			}
			if($KHSID != "")
			{
				$this->db->where('a.KHSID',$KHSID);
			}
			$this->db->join('ac_subjek as b','b.SubjekID=a.SubjekID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_select_exam_card($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->order_by('SubjekID','ASC');
			$query = $this->db->get('ac_krs');
			return $query->result();
		}
		
		function PTL_all_select_exam_card_date($MhswID,$TahunID)
		{
			$this->db->select('a.*, b.Tanggal, b.JamMulai');
			$this->db->from('ac_krs as a');
			$this->db->where('a.MhswID',$MhswID);
			$this->db->where('a.TahunID',$TahunID);
			$this->db->join('ac_exam as b','b.ExamID=a.ExamID');
			$this->db->order_by('b.Tanggal,b.JamMulai','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_krs',$data);
			return;
		}
		
		function PTL_select($KRSID)
		{
			$this->db->where('KRSID',$KRSID);
			$query = $this->db->get('ac_krs');
			return $query->row_array();
		}
		
		function PTL_select_krs($MhswID,$SubjekID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_krs');
			return $query->row_array();
		}
		
		function PTL_select_spesifik_recap($MhswID,$SubjekID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs');
			return $query->row_array();
		}
		
		function PTL_select_subject($MhswID,$KHSID,$SubjekID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('KHSID',$KHSID);
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_krs');
			return $query->row_array();
		}
		
		function PTL_update($KRSID,$data)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->update('ac_krs',$data);
		}
		
		function PTL_update_evaluation($KRSID,$data_krs)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->update('ac_krs',$data_krs);
		}
		
		function PTL_delete($MhswID,$KHSID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('KHSID',$KHSID);
			$this->db->delete('ac_krs');
		}
		
		function PTL_delete_khs($KHSID)
		{
			$this->db->where('KHSID',$KHSID);
			$this->db->delete('ac_krs');
		}
		
		function PTL_delete_krs($KRSID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->delete('ac_krs');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_krs');
		}
	}
?>