<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_remedial extends CI_Model
	{
		function PTL_urut()
		{
			$q = $this->db->query("SELECT max(JadwalRemedialID) AS LAST FROM ac_jadwal_remedial");
			return $q->row_array();
		}
		
		function PTL_all_spesifik($TahunID,$TahunID2,$RuangID)
		{
			if($TahunID != "")
			{
				$this->db->where('TahunID',$TahunID);
			}
			if($TahunID2 != "")
			{
				$this->db->where('TahunID2',$TahunID2);
			}
			if($RuangID != "")
			{
				$this->db->where('UjianRuangID',$RuangID);
			}
			$this->db->where('NA','N');
			$this->db->order_by('TglUjian','ASC');
			$query = $this->db->get('ac_jadwal_remedial');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_jadwal_remedial',$data);
			return;
		}
		
		function PTL_select($JadwalRemedialID)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$query = $this->db->get('ac_jadwal_remedial');
			return $query->row_array();
		}
		
		function PTL_update($JadwalRemedialID,$data)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->update('ac_jadwal_remedial',$data);
		}
		
		function PTL_delete($JadwalRemedialID)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->delete('ac_jadwal_remedial');
		}
	}
?>