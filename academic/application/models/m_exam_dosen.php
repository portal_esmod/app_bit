<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_exam_dosen extends CI_Model
	{
		function PTL_insert($dat)
		{
			$this->db->insert('ac_exam_dosen',$dat);
			return;
		}
		
		function PTL_select($ExamID)
		{
			$this->db->where('ExamID',$ExamID);
			$query = $this->db->get('ac_exam_dosen');
			return $query->row_array();
		}
		
		function PTL_update($ExamID,$dat)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->update('ac_exam_dosen',$dat);
		}
		
		function PTL_delete_exam($ExamID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->delete('ac_exam_dosen');
		}
	}
?>