<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_prodi extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('Jenjang','D3');
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_prodi');
			return $query->result();
		}
		
		function PTL_all_d1()
		{
			$this->db->where('Jenjang','D1');
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_prodi');
			return $query->result();
		}
		
		function PTL_all_list()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_prodi');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_prodi',$data);
			return;
		}
		
		function PTL_select($ProdiID)
		{
			$this->db->where('ProdiID',$ProdiID);
			$query = $this->db->get('ac_prodi');
			return $query->row_array();
		}
		
		function PTL_update($ProdiID,$data)
		{
			$this->db->where('ProdiID',$ProdiID);
			$this->db->update('ac_prodi',$data);
		}
	}
?>