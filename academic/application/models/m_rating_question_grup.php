<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rating_question_grup extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_last()
		{
			$q = $this->db->query("SELECT max(tanggal_buat) as LAST FROM fb_rating_question_grup where na='N'");
			return $q->row_array();
		}
		
		function PTL_all()
		{
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('fb_rating_question_grup');
			return $query->result();
		}
		
		function PTL_select($id_rating_question_grup)
		{
			$this->db->where('id_rating_question_grup',$id_rating_question_grup);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating_question_grup');
			return $query->row_array();
		}
		
		function PTL_select_tanggal($tanggal_buat)
		{
			$this->db->where('tanggal_buat',$tanggal_buat);
			$query = $this->db->get('fb_rating_question_grup');
			return $query->row_array();
		}
	}
?>