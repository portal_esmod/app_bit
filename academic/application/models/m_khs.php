<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_khs extends CI_Model
	{
		function PTL_all_total($TahunID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_homeroom1($TahunID,$KelasID,$WaliKelasID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('KelasID',$KelasID);
			$this->db->where('WaliKelasID',$WaliKelasID);
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_homeroom2($TahunID,$KelasID,$WaliKelasID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('KelasID',$KelasID);
			$this->db->where('WaliKelasID2',$WaliKelasID);
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekjurusan,$cektahun,$cekprodi,$cekkelas)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cekkelas != "")
			{
				$this->db->where('a.KelasID',$cekkelas);
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_homeroom($cekjurusan,$cektahun,$cekprodi,$MatchTahun,$MatchKelas)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($MatchTahun != "")
			{
				$this->db->where('a.TahunKe',$MatchTahun);
			}
			if($MatchKelas != "")
			{
				$this->db->where('a.KelasID',$MatchKelas);
			}
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif($cekjurusan,$cekprodi,$cektahun)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif_year($cekjurusan,$cekprodi,$cektahun,$cektingkat)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($cektingkat != "")
			{
				$this->db->where('a.TahunKe',$cektingkat);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif_year_attendance_problem($cekjurusan,$cekprodi,$cektahun,$cektingkat)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($cektingkat != "")
			{
				$this->db->where('a.TahunKe',$cektingkat);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.TotalAbsen','DESC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif_year_attendance_problem_subjek($cekjurusan,$cekprodi,$cektahun,$cektingkat,$ceksubjek)
		{
			$this->db->select('a.*, b.Nama, c.KHSID, c.TotalAbsen');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
				$this->db->where('c.TahunID',$cektahun);
			}
			if($cektingkat != "")
			{
				$this->db->where('a.TahunKe',$cektingkat);
			}
			if($ceksubjek != "")
			{
				$this->db->where('c.SubjekID',$ceksubjek);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->join('ac_krs as c','c.KHSID=a.KHSID');
			$this->db->order_by('c.TotalAbsen','DESC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif_year_gpa($cekjurusan,$cekprodi,$cektahun,$MatchTahun,$MatchKelas)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($MatchTahun != "")
			{
				$this->db->where('a.TahunKe',$MatchTahun);
			}
			if($MatchKelas != "")
			{
				$this->db->where('a.KelasID',$MatchKelas);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('a.IPS','DESC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif_year_gpa_subject($cekjurusan,$cekprodi,$cektahun,$MatchTahun,$MatchKelas,$ceksubjek)
		{
			$this->db->select('a.*, b.*');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('a.ProdiID',$cekprodi);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($MatchTahun != "")
			{
				$this->db->where('a.TahunKe',$MatchTahun);
			}
			if($MatchKelas != "")
			{
				$this->db->where('a.KelasID',$MatchKelas);
			}
			if($ceksubjek != "")
			{
				$this->db->where('b.SubjekID',$ceksubjek);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_krs as b','b.KHSID=a.KHSID');
			$this->db->order_by('b.gradevalue','DESC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_enroll_attendance($cekjurusan,$cekprodi1,$cekprodi2,$cektahun)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_khs as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cekjurusan == "REG")
			{
				if($cekprodi1 == "IFB3")
				{
					$this->db->where('a.ProdiID',$cekprodi1);
				}
				else
				{
					if((stristr($cekprodi1.$cekprodi2,"FDD")) AND (stristr($cekprodi1.$cekprodi2,"PDD")))
					{
						$this->db->where('a.ProdiID','COMD3');
					}
					else
					{
						if($cekprodi1 != "")
						{
							$this->db->where('a.ProdiID',$cekprodi1);
							$this->db->or_where('a.ProdiID','COMD3');
						}
						if($cekprodi2 != "")
						{
							if($cekprodi1 != "")
							{
								$this->db->or_where('a.ProdiID',$cekprodi2);
								$this->db->or_where('a.ProdiID','COMD3');
							}
							else
							{
								$this->db->where('a.ProdiID',$cekprodi2);
								$this->db->or_where('a.ProdiID','COMD3');
							}
						}
					}
				}
			}
			if($cekjurusan == "INT")
			{
				if((stristr($cekprodi1.$cekprodi2,"FDD")) AND (stristr($cekprodi1.$cekprodi2,"PDD")))
				{
					$this->db->where('a.ProdiID','COMD1');
				}
				else
				{
					if($cekprodi1 != "")
					{
						$this->db->where('a.ProdiID',$cekprodi1);
						$this->db->or_where('a.ProdiID','COMD1');
					}
					if($cekprodi2 != "")
					{
						if($cekprodi1 != "")
						{
							$this->db->or_where('a.ProdiID',$cekprodi2);
							$this->db->or_where('a.ProdiID','COMD1');
						}
						else
						{
							$this->db->where('a.ProdiID',$cekprodi2);
							$this->db->or_where('a.ProdiID','COMD1');
						}
					}
				}
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_class($cekjurusan,$cektahun,$cekprodi,$cekclass,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekclass != "")
			{
				$this->db->where('KelasID',$cekclass);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_no_class($cekjurusan,$cektahun,$cekprodi,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_spc($cekjurusan,$cektahun,$cekprodi,$cekspc,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekspc != "")
			{
				$this->db->where('SpesialisasiID',$cekspc);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_no_spc($cekjurusan,$cektahun,$cekprodi,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->where('SpesialisasiID','0');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_lang($cekjurusan,$cektahun,$cekprodi,$ceklang,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($ceklang != "")
			{
				$this->db->where('languageID',$ceklang);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_spesifik_no_lang($cekjurusan,$cektahun,$cekprodi,$cekke)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			if($cekke != "")
			{
				$this->db->where('Sesi',$cekke);
			}
			else
			{
				$this->db->group_by('MhswID','ASC');
			}
			$this->db->where('SpesialisasiID','0');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_all_select_drop($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_khs');
			return $query->result();
		}
		
		function PTL_select($KHSID)
		{
			$this->db->where('KHSID',$KHSID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_att($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_class_before($MhswID,$Sesi)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('Sesi',$Sesi);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_drop($MhswID)
		{
			$this->db->select('max(TahunID) as tahun, KHSID');
			$this->db->where('MhswID',$MhswID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_tahun($MhswID,$TahunID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_select_program_tahun($MhswID,$ProgramID,$TahunID,$StatusMhswID)
		{
			$this->db->where('MhswID',$MhswID);
			if($ProgramID != "")
			{
				$this->db->where('ProgramID',$ProgramID);
			}
			if($TahunID != "")
			{
				$this->db->where('TahunID',$TahunID);
			}
			if($StatusMhswID != "")
			{
				$this->db->where('StatusMhswID',$StatusMhswID);
			}
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
		
		function PTL_update($KHSID,$data)
		{
			$this->db->where('KHSID',$KHSID);
			$this->db->update('ac_khs',$data);
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_khs');
		}
		
		function PTL_distinct_tahun($cekjurusan,$cekprodi,$cektahun)
		{
			$q = $this->db->query("SELECT distinct(substring(MhswID,1,8)) AS tahun FROM ac_khs
								WHERE ProgramID='$cekjurusan' AND ProdiID='$cekprodi' AND TahunID='$cektahun'
								");
			return $q->result();
		}
		
		function PTL_all_distinct_students($cektahunaktif,$cektahun)
		{
			$q = $this->db->query("SELECT MhswID, TahunID, Biaya, Potongan, Bayar FROM ac_khs
								WHERE MhswID like '$cektahunaktif%' AND TahunID!='$cektahun' GROUP BY MhswID ORDER BY MhswID ASC
								");
			return $q->result();
		}
		
		function PTL_select_cektahun($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$this->session->userdata('academic_filter_pending_old_tahun'));
			$query = $this->db->get('ac_khs');
			return $query->row_array();
		}
	}
?>