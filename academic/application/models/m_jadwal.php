<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jadwal extends CI_Model
	{
		function PTL_urut()
		{
			$q = $this->db->query("SELECT max(JadwalID) AS LAST FROM ac_jadwal");
			return $q->row_array();
		}
		
		function PTL_all_pending()
		{
			$this->db->like('login_buat',$_COOKIE["id_akun"]);
			$this->db->where('finish','N');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_dosen($DosenID,$cektahun)
		{
			if($this->session->userdata('lecturer_filter_jur') != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			$this->db->where('DosenID',$DosenID);
			$this->db->or_where('DosenID2',$DosenID);
			$this->db->or_where('DosenID3',$DosenID);
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_enroll($TahunID,$ProdiID,$ProgramID,$TahunKe)
		{
			$this->db->where('TahunID',$TahunID);
			if($ProdiID == "COMD3")
			{
				$this->db->like('ProdiID','PDD3');
				$this->db->or_like('ProdiID','FDD3');
			}
			else
			{
				if($ProdiID == "COMD1")
				{
					$this->db->like('ProdiID','PDD1');
					$this->db->or_like('ProdiID','FDD1');
				}
				else
				{
					$this->db->like('ProdiID',$ProdiID);
				}
			}
			$this->db->where('ProgramID',$ProgramID);
			$this->db->where('TahunKe',$TahunKe);
			$this->db->order_by('JadwalID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_spesifik($ProgramID,$TahunID,$KelasID)
		{
			if($ProgramID != "")
			{
				$this->db->where('ProgramID',$ProgramID);
			}
			if($TahunID != "")
			{
				$this->db->where('TahunID',$TahunID);
			}
			if($KelasID != "")
			{
				$this->db->where('KelasID',$KelasID);
			}
			$this->db->order_by('TahunID','DESC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_jadwal as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($ceksubjek != "")
			{
				$this->db->where('a.SubjekID',$ceksubjek);
			}
			if($Tahun != "")
			{
				$this->db->where('a.TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->where('a.KelasID',$Kelas);
				// $this->db->or_like('a.KelasIDGabungan',$Kelas);
			}
			$this->db->join('ac_subjek as b','b.SubjekID=a.SubjekID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring2($cekjurusan,$cektahun,$ceksubjek,$Tahun,$Kelas)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_jadwal as a');
			if($cekjurusan != "")
			{
				$this->db->where('a.ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('a.TahunID',$cektahun);
			}
			if($ceksubjek != "")
			{
				$this->db->where('a.SubjekID',$ceksubjek);
			}
			if($Tahun != "")
			{
				$this->db->where('a.TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->like('a.KelasIDGabungan',$Kelas);
			}
			$this->db->join('ac_subjek as b','b.SubjekID=a.SubjekID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring_total($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($Tahun != "")
			{
				$this->db->where('TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->where('KelasID',$Kelas);
			}
			if($SubjekID != "")
			{
				$this->db->where('SubjekID',$SubjekID);
			}
			$this->db->order_by('SubjekID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_spesifik_scoring_total_dosen($cekjurusan,$cektahun,$Tahun,$Kelas,$SubjekID,$DosenID)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($Tahun != "")
			{
				$this->db->where('TahunKe',$Tahun);
			}
			if($Kelas != "")
			{
				$this->db->where('KelasID',$Kelas);
			}
			if($SubjekID != "")
			{
				$this->db->where('SubjekID',$SubjekID);
			}
			if($DosenID != "")
			{
				$this->db->where('DosenID',$DosenID);
				$this->db->or_where('DosenID2',$DosenID);
				$this->db->or_where('DosenID3',$DosenID);
				$this->db->or_where('DosenID4',$DosenID);
				$this->db->or_where('DosenID5',$DosenID);
				$this->db->or_where('DosenID6',$DosenID);
			}
			$this->db->order_by('SubjekID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_select_drop($SubjekID,$TahunID,$ProgramID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('TahunID',$TahunID);
			$this->db->where('ProgramID',$ProgramID);
			$this->db->order_by('JadwalID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_all_select_project($TahunID,$SubjekID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('SubjekID',$SubjekID);
			$this->db->order_by('JadwalID','ASC');
			$query = $this->db->get('ac_jadwal');
			return $query->result();
		}
		
		function PTL_subjek($cekjurusan,$cektahun,$Tahun,$Kelas)
		{
			$j = "";
			$t = "";
			$th = "";
			$kls = "";
			if($cekjurusan != "")
			{
				$j = "AND ac_jadwal.ProgramID = '$cekjurusan'";
			}
			if($cektahun != "")
			{
				$t = "AND ac_jadwal.TahunID = '$cektahun'";
			}
			if($Tahun != "")
			{
				$th = "AND ac_jadwal.TahunKe = '$Tahun'";
			}
			if($Kelas != "")
			{
				$kls = "AND ac_jadwal.KelasID = '$Kelas'";
			}
			$q = $this->db->query("
				SELECT distinct(ac_jadwal.SubjekID) AS subjek, ac_subjek.Nama as nama
				FROM ac_subjek
				LEFT JOIN ac_jadwal ON ac_jadwal.SubjekID = ac_subjek.SubjekID
				WHERE ac_jadwal.NA = 'N' $j $t $th $kls ORDER BY ac_subjek.Nama ASC");
			return $q->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_jadwal',$data);
			return;
		}
		
		function PTL_insert_file($JadwalID,$gambar,$gambar)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$storage = gmdate("Y-m", time()-($ms));
			$folder = $storage.'_';
			$dt = array(
						'file_project'=>$folder.@$gambar[0],
						'file_evaluation'=>$folder.@$gambar[1]
						);
			$this->db->where('JadwalID',$JadwalID);
			return $this->db->update('ac_jadwal',$dt);
		}
		
		function PTL_select($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$query = $this->db->get('ac_jadwal');
			return $query->row_array();
		}
		
		function PTL_update($JadwalID,$data)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->update('ac_jadwal',$data);
		}
		
		function PTL_delete($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->delete('ac_jadwal');
		}
	}
?>