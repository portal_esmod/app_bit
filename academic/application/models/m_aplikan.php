<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_aplikan extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		// {
			// if($cekperiode != "")
			// {
				// $this->db->where('PMBPeriodID',$cekperiode);
			// }
			// if($cekmarketing != "")
			// {
				// $this->db->where('PresenterID',$cekmarketing);
			// }
			// if($cektype != "")
			// {
				// $this->db->where('StatusProspekID',$cektype);
			// }
			// if($cekstatus != "")
			// {
				// if($cekstatus == "P")
				// {
					// $this->db->where('postpone','Y');
				// }
				// else
				// {
					// $this->db->where('StatusMundur',$cekstatus);
				// }
			// }
			// if($cekpogram != "")
			// {
				// if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				// {
					// $this->db->where('ProgramID',$cekpogram);
				// }
				// else
				// {
					// $this->db->where('ProdiID',$cekpogram);
				// }
			// }
			// if($cektahun != "")
			// {
				// $this->db->like('tanggal_buat',$cektahun);
			// }
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_completeness()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('file_syarat !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_completeness_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		// {
			// if($cekperiode != "")
			// {
				// $this->db->where('PMBPeriodID',$cekperiode);
			// }
			// if($cekmarketing != "")
			// {
				// $this->db->where('PresenterID',$cekmarketing);
			// }
			// if($cektype != "")
			// {
				// $this->db->where('StatusProspekID',$cektype);
			// }
			// if($cekstatus != "")
			// {
				// if($cekstatus == "P")
				// {
					// $this->db->where('postpone','Y');
				// }
				// else
				// {
					// $this->db->where('StatusMundur',$cekstatus);
				// }
			// }
			// if($cekpogram != "")
			// {
				// if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				// {
					// $this->db->where('ProgramID',$cekpogram);
				// }
				// else
				// {
					// $this->db->where('ProdiID',$cekpogram);
				// }
			// }
			// if($cektahun != "")
			// {
				// $this->db->like('tanggal_buat',$cektahun);
			// }
			// $this->db->where('NA','N');
			// $this->db->where('file_syarat !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		// function PTL_all_interview()
		// {
			// $this->db->where('NA','N');
			// $this->db->where('ujian !=','');
			// $this->db->order_by('AplikanID','DESC');
			// $query = $this->db->get('al_aplikan');
			// return $query->result();
		// }
		
		function PTL_all_interview_spesifik($cekperiode,$cekmarketing,$cektype,$cekstatus,$cekpogram,$cektahun)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekmarketing != "")
			{
				$this->db->where('PresenterID',$cekmarketing);
			}
			if($cektype != "")
			{
				$this->db->where('StatusProspekID',$cektype);
			}
			if($cekstatus != "")
			{
				if($cekstatus == "P")
				{
					$this->db->where('postpone','Y');
				}
				else
				{
					$this->db->where('StatusMundur',$cekstatus);
				}
			}
			if($cekpogram != "")
			{
				if(($cekpogram == "INT") OR ($cekpogram == "REG") OR ($cekpogram == "SC"))
				{
					$this->db->where('ProgramID',$cekpogram);
				}
				else
				{
					$this->db->where('ProdiID',$cekpogram);
				}
			}
			if($cektahun != "")
			{
				$this->db->like('tanggal_buat',$cektahun);
			}
			$this->db->where('NA','N');
			$this->db->where('ujian !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_all_spesifik_semester_pending($cekperiode,$cekjurusan,$cekprodi)
		{
			if($cekperiode != "")
			{
				$this->db->where('PMBPeriodID',$cekperiode);
			}
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			if($cekprodi != "")
			{
				$this->db->where('ProdiID',$cekprodi);
			}
			$this->db->where('NA','N');
			$this->db->where('MhswID','');
			$this->db->where('postpone','N');
			$this->db->where('diterima','Y');
			$this->db->where('bukti_setoran !=','');
			$this->db->order_by('AplikanID','DESC');
			$query = $this->db->get('al_aplikan');
			return $query->result();
		}
		
		function PTL_tahun()
		{
			$q = $this->db->query("SELECT distinct(substring(tanggal_buat,1,4)) AS tahun FROM al_aplikan");
			return $q->result();
		}
		
		function PTL_select($AplikanID)
		{
			$this->db->where('AplikanID',$AplikanID);
			$query = $this->db->get('al_aplikan');
			return $query->row_array();
		}
		
		function PTL_update($AplikanID,$data)
		{
			$this->db->where('AplikanID',$AplikanID);
			$this->db->update('al_aplikan',$data);
		}
	}
?>