<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_exam_kursi extends CI_Model
	{
		function PTL_insert($data)
		{
			$this->db->insert('ac_exam_kursi',$data);
			return;
		}
		
		function PTL_insert_auto($da)
		{
			$this->db->insert('ac_exam_kursi',$da);
			return;
		}
		
		function PTL_select($ExamID,$MhswID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_exam_kursi');
			return $query->row_array();
		}
		
		function PTL_select_kursi($ExamID,$NomorKursi)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->where('NomorKursi',$NomorKursi);
			$query = $this->db->get('ac_exam_kursi');
			return $query->row_array();
		}
		
		// function PTL_update($TahunID,$data)
		// {
			// $this->db->where('TahunID',$TahunID);
			// $this->db->update('ac_tahun',$data);
		// }
		
		function PTL_delete($ExamKursiID)
		{
			$this->db->where('ExamKursiID',$ExamKursiID);
			$this->db->delete('ac_exam_kursi');
		}
		
		function PTL_delete_exam($ExamID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->delete('ac_exam_kursi');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_exam_kursi');
		}
	}
?>