<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_exam extends CI_Model
	{
		function PTL_urut()
		{
			$q = $this->db->query("SELECT max(ExamID) AS LAST FROM ac_exam");
			return $q->row_array();
		}
		
		function PTL_all_lecturer_expired($DosenID)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$today = gmdate("Y-m-d", time()-($ms));
			$word = explode("-",$today);
			$data = mktime(0,0,0,$word[1],$word[2]-30,$word[0]);
			$date = date("Y-m-d",$data);
			$this->db->select('a.*, b.*');
			$this->db->from('ac_exam as a');
			$this->db->where('b.DosenID',$DosenID);
			$this->db->where('a.Tanggal <=',$today);
			$this->db->where('a.Tanggal >=',$date);
			$this->db->where('a.NA','N');
			$this->db->where('b.NA','N');
			$this->db->join('ac_exam_dosen as b','b.ExamID=a.ExamID');
			$this->db->order_by('a.Tanggal','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_select_exam_card($TahunID,$SubjekID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->or_where('TahunID2',$TahunID);
			$this->db->where('SubjekID',$SubjekID);
			$this->db->or_where('SubjekID12',$SubjekID);
			$this->db->or_where('SubjekID2',$SubjekID);
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_all_select_remedial($TahunID,$TahunID2,$SubjekID,$SubjekID12,$SubjekID2)
		{
            $this->db->where('TahunID',$TahunID);
            $this->db->where('TahunID2',$TahunID2);
            $this->db->where('SubjekID',$SubjekID);
            $this->db->where('SubjekID12',$SubjekID12);
            $this->db->where('SubjekID2',$SubjekID2);
			$query = $this->db->get('ac_exam');
			return $query->row_array();
		}
		
		function PTL_all_spesifik($TahunID,$TahunID2,$RuangID)
		{
			if($TahunID != "")
			{
				$this->db->where('TahunID',$TahunID);
			}
			if($TahunID2 != "")
			{
				$this->db->where('TahunID2',$TahunID2);
			}
			if($RuangID != "")
			{
				$this->db->where('RuangID',$RuangID);
			}
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_exam',$data);
			return;
		}
		
		function PTL_select($ExamID)
		{
			$this->db->where('ExamID',$ExamID);
			$query = $this->db->get('ac_exam');
			return $query->row_array();
		}
		
		function PTL_nilai_evaluasiA1($TahunID,$SubjekID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_nilai_evaluasiA2($TahunID,$SubjekID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('SubjekID12',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_nilai_evaluasiA3($TahunID,$SubjekID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('SubjekID2',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_nilai_evaluasiB1($TahunID,$SubjekID)
		{
			$this->db->where('TahunID2',$TahunID);
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_nilai_evaluasiB2($TahunID,$SubjekID)
		{
			$this->db->where('TahunID2',$TahunID);
			$this->db->where('SubjekID12',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_nilai_evaluasiB3($TahunID,$SubjekID)
		{
			$this->db->where('TahunID2',$TahunID);
			$this->db->where('SubjekID2',$SubjekID);
			$query = $this->db->get('ac_exam');
			return $query->result();
		}
		
		function PTL_update($ExamID,$data)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->update('ac_exam',$data);
		}
		
		function PTL_delete($ExamID)
		{
			$this->db->where('ExamID',$ExamID);
			$this->db->delete('ac_exam');
		}
	}
?>