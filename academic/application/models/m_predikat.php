<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_predikat extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->where('na','N');
			// $this->db->order_by('Nama','ASC');
			// $query = $this->db->get('ac_predikat');
			// return $query->result();
		// }
		
		function PTL_all_active()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_predikat');
			return $query->result();
		}
		
		function PTL_all_active_score_descending()
		{
			$this->db->order_by('IPKMin','DESC');
			$query = $this->db->get('ac_predikat');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_predikat',$data);
			return;
		}
		
		function PTL_select($PredikatID)
		{
			$this->db->where('PredikatID',$PredikatID);
			$query = $this->db->get('ac_predikat');
			return $query->row_array();
		}
		
		function PTL_update($PredikatID,$data)
		{
			$this->db->where('PredikatID',$PredikatID);
			$this->db->update('ac_predikat',$data);
		}
	}
?>