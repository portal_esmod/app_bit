<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_krs2 extends CI_Model
	{
		function PTL_insert_file($KRSID,$MKID,$PresensiID,$gambarku)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$storage = gmdate("Y-m", time()-($ms));
			$folder = $storage.'_';
			$dt = array(
						'files'=>$folder.@$gambarku
						);
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('PresensiID',$PresensiID);
			return $this->db->update('ac_krs2',$dt);
		}
		
		function PTL_all_lecturer_expired_att($PresensiID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs2');
			return $query->result();
		}
		
		function PTL_all_lecturer_expired($PresensiID,$MKID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MKID',$MKID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs2');
			return $query->result();
		}
		
		function PTL_all_select_scoring($KRSID,$MKID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs2');
			return $query->result();
		}
		
		function PTL_all_select_scoring_evaluation($KRSID,$MKID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('show','Y');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs2');
			return $query->result();
		}
		
		function PTL_insert($data_krs2)
		{
			$this->db->insert('ac_krs2',$data_krs2);
			return;
		}
		
		function PTL_select($KRS2ID)
		{
			$this->db->where('KRS2ID',$KRS2ID);
			$query = $this->db->get('ac_krs2');
			return $query->row_array();
		}
		
		function PTL_select_scoring($KRSID,$MKID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_krs2');
			return $query->row_array();
		}
		
		function PTL_select_scoring_not_null($KRSID,$MKID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('GradeNilai !=','');
			$query = $this->db->get('ac_krs2');
			return $query->row_array();
		}
		
		function PTL_select_spesifik($KRSID,$PresensiID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('PresensiID',$PresensiID);
			$query = $this->db->get('ac_krs2');
			return $query->row_array();
		}
		
		function PTL_update($KRS2ID,$data)
		{
			$this->db->where('KRS2ID',$KRS2ID);
			$this->db->update('ac_krs2',$data);
		}
		
		function PTL_update_att1($KRSID,$PresensiID,$data_krs2)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('PresensiID',$PresensiID);
			$this->db->update('ac_krs2',$data_krs2);
		}
		
		function PTL_update_att2($KRSID,$PresensiID,$data_krs2)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('PresensiID',$PresensiID);
			$this->db->update('ac_krs2',$data_krs2);
		}
		
		function PTL_update_krs2($KRSID,$MKID,$PresensiID,$datakrs2)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->where('PresensiID',$PresensiID);
			$this->db->update('ac_krs2',$datakrs2);
		}
		
		function PTL_update_krs2_scoring($KRSID,$MKID,$datakrs2)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MKID',$MKID);
			$this->db->update('ac_krs2',$datakrs2);
		}
	}
?>