<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_remedial_krs extends CI_Model
	{
		function PTL_all_spesifik($MhswID,$TahunID)
		{
			$this->db->select('a.*, b.SubjekID, b.Nama as NamaSubjek, b.SubjekKode,
							c.TglMulai, c.TglSelesai, c.JamMulai, c.JamSelesai, c.UjianRuangID,
							d.Nama as NamaDosen');
			$this->db->from('ac_krs_remedial AS a');
			$this->db->join('ac_subjek AS b', 'b.SubjekID = a.SubjekID', 'INNER');
			$this->db->join('ac_jadwal_remedial AS c', 'c.JadwalRemedialID = a.JadwalRemedialID', 'INNER');
			$this->db->join('ac_dosen AS d', 'd.Login = c.DosenID', 'INNER');
			$this->db->where('a.NA','N');
			$this->db->where('b.NA','N');
			$this->db->where('a.MhswID',$MhswID);
			$this->db->where('a.TahunID',$TahunID);
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return FALSE;
			}
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_krs_remedial',$data);
			return;
		}
		
		function PTL_insert_auto($da)
		{
			$this->db->insert('ac_krs_remedial',$da);
			return;
		}
		
		function PTL_select_evaluasi($SubjekID,$KRSID,$MhswID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_select($JadwalRemedialID,$MhswID)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_select_krs($KRSRemedialID)
		{
			$this->db->where('KRSRemedialID',$KRSRemedialID);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_select_kursi($JadwalRemedialID,$NomorKursi)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->where('NomorKursi',$NomorKursi);
			$query = $this->db->get('ac_krs_remedial');
			return $query->row_array();
		}
		
		function PTL_update($KRSRemedialID,$data)
		{
			$this->db->where('KRSRemedialID',$KRSRemedialID);
			$this->db->update('ac_krs_remedial',$data);
		}
		
		function PTL_delete($KRSRemedialID)
		{
			$this->db->where('KRSRemedialID',$KRSRemedialID);
			$this->db->delete('ac_krs_remedial');
		}
		
		function PTL_delete_jadwal($JadwalRemedialID)
		{
			$this->db->where('JadwalRemedialID',$JadwalRemedialID);
			$this->db->delete('ac_krs_remedial');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_krs_remedial');
		}
	}
?>