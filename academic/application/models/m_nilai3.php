<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_nilai3 extends CI_Model
	{
		function PTL_all_active()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_nilai3');
			return $query->result();
		}
		
		function PTL_all_copy($cekkurikulum)
		{
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_nilai3');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekgroup)
		{
			$this->db->where('JenisMKID',$cekgroup);
			$query = $this->db->get('ac_nilai3');
			return $query->result();
		}
		
		function PTL_all_spesifik_as($cekkurikulum,$cektype)
		{
			$this->db->where('KurikulumID',$cekkurikulum);
			$this->db->where('JenisMKID',$cektype);
			$query = $this->db->get('ac_nilai3');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_nilai3',$data);
			return;
		}
		
		function PTL_select($Nilai3ID)
		{
			$this->db->where('Nilai3ID',$Nilai3ID);
			$query = $this->db->get('ac_nilai3');
			return $query->row_array();
		}
		
		function PTL_select_attendance_copy($KurikulumID,$JenisMKID,$JenisPresensiID)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->where('JenisMKID',$JenisMKID);
			$this->db->where('JenisPresensiID',$JenisPresensiID);
			$query = $this->db->get('ac_nilai3');
			return $query->row_array();
		}
		
		function PTL_select_attendance($JenisMKID,$JenisPresensiID)
		{
			$this->db->where('JenisMKID',$JenisMKID);
			$this->db->where('JenisPresensiID',$JenisPresensiID);
			$query = $this->db->get('ac_nilai3');
			return $query->row_array();
		}
		
		function PTL_update($Nilai3ID,$data)
		{
			$this->db->where('Nilai3ID',$Nilai3ID);
			$this->db->update('ac_nilai3',$data);
		}
	}
?>