<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_presensi extends CI_Model
	{
		// function PTL_all_listing()
		// {
			// $this->db->where('NA','Y');
			// $this->db->order_by('Tanggal','ASC');
			// $query = $this->db->get('ac_presensi');
			// return $query->result();
		// }
		
		// function PTL_all_listing2($PresensiID)
		// {
			// $this->db->where('PresensiID',$PresensiID);
			// $query = $this->db->get('ac_presensi_mhsw');
			// return $query->result();
		// }
		
		// function PTL_all_listing3($PresensiID)
		// {
			// $this->db->where('PresensiID',$PresensiID);
			// $query = $this->db->get('ac_krs2');
			// return $query->result();
		// }
		
		// function PTL_listing_delete($PresensiID)
		// {
			// $this->db->where('PresensiID',$PresensiID);
			// $this->db->delete('ac_presensi');
		// }
		
		// function PTL_listing_delete2($PresensiID)
		// {
			// $this->db->where('PresensiID',$PresensiID);
			// $this->db->delete('ac_presensi_mhsw');
		// }
		
		// function PTL_listing_delete3($PresensiID)
		// {
			// $this->db->where('PresensiID',$PresensiID);
			// $this->db->delete('ac_krs2');
		// }
		
		function PTL_urut($JadwalID)
		{
			$q = $this->db->query("SELECT max(Pertemuan) AS LAST FROM ac_presensi WHERE JadwalID = '$JadwalID'");
			return $q->row_array();
		}
		
		function PTL_all_active_year()
		{
			$this->db->where('TahunID',$this->session->userdata('att_filter_tahun'));
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_lecturer_expired($DosenID)
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$today = gmdate("Y-m-d", time()-($ms));
			$word = explode("-",$today);
			$data = mktime(0,0,0,$word[1],$word[2]-30,$word[0]);
			$date = date("Y-m-d",$data);
			$this->db->where('DosenID',$DosenID);
			$this->db->where('Tanggal <=',$today);
			$this->db->where('Tanggal >=',$date);
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_lecturer_expired_related($JadwalID,$MKID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('MKID',$MKID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_select($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_select_dosen($JadwalID,$DosenID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('DosenID',$DosenID);
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_select_conflict1($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID)
		{
			$this->db->where('PresensiID !=',$PresensiID);
			$this->db->where('Tanggal',$Tanggal);
			$this->db->where('JamMulai >=',$JamMulai);
			$this->db->where('JamMulai <',$JamSelesai);
			$this->db->where('RuangID',$RuangID);
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_select_conflict2($PresensiID,$Tanggal,$JamMulai,$JamSelesai,$RuangID)
		{
			$this->db->where('PresensiID !=',$PresensiID);
			$this->db->where('Tanggal',$Tanggal);
			$this->db->where('JamSelesai >',$JamMulai);
			$this->db->where('JamSelesai <=',$JamSelesai);
			$this->db->where('RuangID',$RuangID);
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_select_trash($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('NA','Y');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_spesifik_lecturer_calendar($cektahun,$cekdosen)
		{
			if($cektahun != "")
			{
				$this->db->where('TahunID',$cektahun);
			}
			if($cekdosen != "")
			{
				$this->db->where('DosenID',$cekdosen);
			}
			$this->db->where('NA','N');
			$this->db->order_by('Tanggal','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_room_available($RuangID,$today)
		{
			$this->db->where('RuangID',$RuangID);
			$this->db->where('Tanggal',$today);
			$this->db->where('NA','N');
			$this->db->order_by('JamMulai','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_all_today($today,$cekdosen,$cekruang)
		{
			if($cekdosen != "")
			{
				$this->db->where('DosenID',$cekdosen);
			}
			if($cekruang != "")
			{
				$this->db->where('RuangID',$cekruang);
			}
			$this->db->where('Tanggal',$today);
			$this->db->where('NA','N');
			$this->db->order_by('JamMulai','ASC');
			$query = $this->db->get('ac_presensi');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_presensi',$data);
			return;
		}
		
		function PTL_select($PresensiID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
		
		function PTL_select_attendance($JadwalID,$Pertemuan)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('Pertemuan',$Pertemuan);
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
		
		function PTL_select_mk($JadwalID,$MKID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('MKID',$MKID);
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
		
		function PTL_select_cek_mk($JadwalID,$CekMKID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('MKID',$CekMKID);
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
		
		function PTL_select_cek_mk_scoring($JadwalID,$CekMKID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('MKID',$CekMKID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_presensi');
			return $query->row_array();
		}
		
		function PTL_select_schedule_duplicate($Tanggal,$JamMulai,$JamSelesai,$RuangID)
		{
			$q = $this->db->query("SELECT * FROM ac_presensi WHERE Tanggal = '$Tanggal'
								AND RuangID = '$RuangID'
								AND
								(
								(JamMulai >= '$JamMulai' and JamMulai <= '$JamSelesai') and (JamSelesai >= '$JamMulai' and JamSelesai >= '$JamSelesai') OR 
								(JamMulai <= '$JamMulai' and JamMulai <= '$JamSelesai') and (JamSelesai >= '$JamMulai' and JamSelesai <= '$JamSelesai') OR 
								(JamMulai <= '$JamMulai' and JamMulai <= '$JamSelesai') and (JamSelesai >= '$JamMulai' and JamSelesai >= '$JamSelesai') OR
								(JamMulai >= '$JamMulai' and JamMulai <= '$JamSelesai') and (JamSelesai >= '$JamMulai' and JamSelesai <= '$JamSelesai')
								)
								AND NA = 'N'
								");
			return $q->row_array();
		}
		
		function PTL_update($PresensiID,$data)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->update('ac_presensi',$data);
		}
		
		function PTL_delete_jadwal($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->delete('ac_presensi');
		}
		
		function PTL_delete_presensi($PresensiID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->delete('ac_presensi');
		}
		
		// function PTL_select($StatusMhswID)
		// {
			// $this->db->where('StatusMhswID',$StatusMhswID);
			// $query = $this->db->get('ac_catatan');
			// return $query->row_array();
		// }
	}
?>