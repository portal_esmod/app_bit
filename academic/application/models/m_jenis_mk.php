<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jenis_mk extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_jenis_mk');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$query = $this->db->get('ac_jenis_mk');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_jenis_mk',$data);
			return;
		}
		
		function PTL_select($JenisMKID)
		{
			$this->db->where('JenisMKID',$JenisMKID);
			$query = $this->db->get('ac_jenis_mk');
			return $query->row_array();
		}
		
		function PTL_update($JenisMKID,$data)
		{
			$this->db->where('JenisMKID',$JenisMKID);
			$this->db->update('ac_jenis_mk',$data);
		}
	}
?>