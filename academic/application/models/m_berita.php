<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_berita extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_berita');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_berita',$data);
			return;
		}
		
		function PTL_select($BeritaID)
		{
			$this->db->where('BeritaID',$BeritaID);
			$query = $this->db->get('ac_berita');
			return $query->row_array();
		}
		
		function PTL_update($BeritaID,$data)
		{
			$this->db->where('BeritaID',$BeritaID);
			$this->db->update('ac_berita',$data);
		}
	}
?>