<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rating_question extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_all_spesifik($id_rating_question_grup)
		{
			$this->db->where('id_rating_question_grup',$id_rating_question_grup);
			$this->db->where('na','N');
			$this->db->order_by('urutan','ASC');
			$query = $this->db->get('fb_rating_question');
			return $query->result();
		}
	}
?>