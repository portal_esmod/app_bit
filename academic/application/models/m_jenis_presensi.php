<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jenis_presensi extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_jenis_presensi');
			return $query->result();
		}
		
		function PTL_select($JenisPresensiID)
		{
			$this->db->where('JenisPresensiID',$JenisPresensiID);
			$query = $this->db->get('ac_jenis_presensi');
			return $query->row_array();
		}
	}
?>