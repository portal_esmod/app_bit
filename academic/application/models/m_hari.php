<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_hari extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_hari');
			return $query->result();
		}
		
		function PTL_select($HariID)
		{
			$this->db->where('HariID',$HariID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_hari');
			return $query->row_array();
		}
	}
?>