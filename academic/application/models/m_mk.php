<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mk extends CI_Model
	{
		function PTL_all_select($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('NA','N');
			if($this->session->userdata('score_filter_sequence') == "")
			{
				$this->db->order_by('Nama','ASC');
			}
			else
			{
				$this->db->order_by('NoUrut','ASC');
			}
			$query = $this->db->get('ac_mk');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekkurikulum,$ceksubjek)
		{
			$this->db->where('KurikulumID',$cekkurikulum);
			$this->db->where('SubjekID',$ceksubjek);
			$query = $this->db->get('ac_mk');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_mk',$data);
			return;
		}
		
		function PTL_select($MKID)
		{
			$this->db->where('MKID',$MKID);
			$query = $this->db->get('ac_mk');
			return $query->row_array();
		}
		
		function PTL_select_kode($MKKode,$KurikulumID,$SubjekID)
		{
			$this->db->where('MKKode',$MKKode);
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_mk');
			return $query->row_array();
		}
		
		function PTL_update($MKID,$data)
		{
			$this->db->where('MKID',$MKID);
			$this->db->update('ac_mk',$data);
		}
		
		function PTL_delete($MKID)
		{
			$this->db->where('MKID',$MKID);
			$this->db->delete('ac_mk');
		}
	}
?>