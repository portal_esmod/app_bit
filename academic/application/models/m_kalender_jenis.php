<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kalender_jenis extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$query = $this->db->get('ac_kalender_jenis');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$query = $this->db->get('ac_kalender_jenis');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_kalender_jenis',$data);
			return;
		}
		
		function PTL_select($id)
		{
			$this->db->where('id',$id);
			$query = $this->db->get('ac_kalender_jenis');
			return $query->row_array();
		}
		
		function PTL_update($id,$data)
		{
			$this->db->where('id',$id);
			$this->db->update('ac_kalender_jenis',$data);
		}
	}
?>