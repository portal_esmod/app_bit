<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_nilai2 extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->where('NA','N');
			// $query = $this->db->get('ac_nilai2');
			// return $query->result();
		// }
		
		function PTL_all_spesifik($cekkurikulum)
		{
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_nilai2');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_nilai2',$data);
			return;
		}
		
		function PTL_select($Nilai2ID)
		{
			$this->db->where('Nilai2ID',$Nilai2ID);
			$query = $this->db->get('ac_nilai2');
			return $query->row_array();
		}
		
		function PTL_select_kode_nilai($Nama,$cekkurikulum)
		{
			$this->db->where('Nama',$Nama);
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_nilai2');
			return $query->row_array();
		}
		
		function PTL_update($Nilai2ID,$data)
		{
			$this->db->where('Nilai2ID',$Nilai2ID);
			$this->db->update('ac_nilai2',$data);
		}
	}
?>