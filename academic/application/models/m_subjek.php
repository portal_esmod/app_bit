<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_subjek extends CI_Model
	{
		function lookup_subjek($keyword)
		{
			$this->db->select('a.SubjekID as SubjekID, a.Nama as Nama,
				a.SubjekKode as SubjekKode, a.ProdiID as ProdiID, a.KurikulumID, a.NA,
				b.KurikulumID, b.NA');
			$this->db->from('ac_subjek AS a');
			$this->db->join('ac_kurikulum AS b', 'b.KurikulumID = a.KurikulumID', 'INNER');
			$this->db->where('a.NA','N');
			$this->db->where('b.NA','N');
			$this->db->like('a.Nama',$keyword);
			$this->db->limit(10);
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return FALSE;
			}
		}
		
		function lookup_subjek_tahun($keyword,$TahunID)
		{
			$this->db->select('b.SubjekID as SubjekID, b.Nama as Nama,
				b.SubjekKode as SubjekKode, b.ProdiID as ProdiID, b.KurikulumID, b.NA,
				a.TahunID');
			$this->db->from('ac_tahun AS a');
			$this->db->join('ac_subjek AS b', 'b.KurikulumID = a.KurikulumID', 'INNER');
			$this->db->where('b.NA','N');
			$this->db->like('a.TahunID',$TahunID);
			$this->db->like('b.Nama',$keyword);
			$this->db->limit(10);
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return FALSE;
			}
		}
		
		function PTL_all_spesifik($cekkurikulum)
		{
			if($cekkurikulum != "")
			{
				$this->db->where('KurikulumID',$cekkurikulum);
			}
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_subjek');
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->group_by('Nama');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_subjek');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_subjek',$data);
			return;
		}
		
		function PTL_select($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_select_kode($SubjekKode,$KurikulumID)
		{
			$this->db->where('SubjekKode',$SubjekKode);
			$this->db->where('KurikulumID',$KurikulumID);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_select_kode_cur($SubjekKode,$cekkurikulum)
		{
			$this->db->where('SubjekKode',$SubjekKode);
			$this->db->where('KurikulumID',$cekkurikulum);
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_select_subjek_nama($Nama)
		{
			$this->db->where('Nama',$Nama);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_subjek');
			return $query->row_array();
		}
		
		function PTL_update($SubjekID,$data)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->update('ac_subjek',$data);
		}
	}
?>