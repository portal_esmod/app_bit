<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_year extends CI_Model
	{
		function PTL_urut()
		{
			$q = $this->db->query("SELECT max(TahunID) AS LAST FROM ac_tahun");
			return $q->row_array();
		}
		
		function PTL_all()
		{
			$this->db->where('ProgramID',$this->session->userdata('year_filter_jur'));
			$this->db->where('NA','N');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_active_select_kurikulum($KurikulumID)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->where('NA','N');
			$this->db->order_by('TahunID','DESC');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_d3()
		{
			$this->db->where('ProgramID','REG');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_d1()
		{
			$this->db->where('ProgramID','INT');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekjurusan)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			$this->db->order_by('TahunID','DESC');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_all_spesifik_aktif($cekjurusan)
		{
			if($cekjurusan != "")
			{
				$this->db->where('ProgramID',$cekjurusan);
			}
			$this->db->where('NA','N');
			$this->db->order_by('TahunID','DESC');
			$query = $this->db->get('ac_tahun');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_tahun',$data);
			return;
		}
		
		function PTL_select($TahunID)
		{
			$this->db->where('TahunID',$TahunID);
			$query = $this->db->get('ac_tahun');
			return $query->row_array();
		}
		
		function PTL_select_active($ProgramID)
		{
			$this->db->where('ProgramID',$ProgramID);
			$this->db->where('NA','N');
			$query = $this->db->get('ac_tahun');
			return $query->row_array();
		}
		
		function PTL_update($TahunID,$data)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->update('ac_tahun',$data);
		}
	}
?>