<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_ruang extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$this->db->order_by('RuangID','ASC');
			$query = $this->db->get('ac_ruang');
			return $query->result();
		}
		
		function PTL_all_spesifik($cekkampus)
		{
			$this->db->where('KampusID',$cekkampus);
			$query = $this->db->get('ac_ruang');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_ruang',$data);
			return;
		}
		
		function PTL_select($RuangID)
		{
			$this->db->where('RuangID',$RuangID);
			$query = $this->db->get('ac_ruang');
			return $query->row_array();
		}
		
		function PTL_update($RuangID,$data)
		{
			$this->db->where('RuangID',$RuangID);
			$this->db->update('ac_ruang',$data);
		}
	}
?>