<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jadwal_hari extends CI_Model
	{
		function PTL_all_jadwal($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->order_by('JadwalHariID','ASC');
			$query = $this->db->get('ac_jadwal_hari');
			return $query->result();
		}
		
		function PTL_all_select_hari($JadwalID,$HariID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('HariID',$HariID);
			$query = $this->db->get('ac_jadwal_hari');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_jadwal_hari',$data);
			return;
		}
		
		function PTL_select_spesifik($JadwalID,$HariID)
		{
			if($JadwalID != "")
			{
				$this->db->where('JadwalID',$JadwalID);
			}
			if($HariID != "")
			{
				$this->db->where('HariID',$HariID);
			}
			$query = $this->db->get('ac_jadwal_hari');
			return $query->row_array();
		}
		
		function PTL_update($JadwalHariID,$data)
		{
			$this->db->where('JadwalHariID',$JadwalHariID);
			$this->db->update('ac_jadwal_hari',$data);
		}
		
		function PTL_delete($JadwalHariID)
		{
			$this->db->where('JadwalHariID',$JadwalHariID);
			$this->db->delete('ac_jadwal_hari');
		}
		
		function PTL_delete_jadwal($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->delete('ac_jadwal_hari');
		}
		
		function PTL_delete_jadwal_hari($JadwalID,$HariID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('HariID',$HariID);
			$this->db->delete('ac_jadwal_hari');
		}
	}
?>