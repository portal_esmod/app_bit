<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_outline extends CI_Model
	{
		function PTL_all_spesifik($KursusSingkatID)
		{
			$this->db->where('na','N');
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$query = $this->db->get('ac_outline');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_outline',$data);
			return;
		}
		
		function PTL_select($OutlineID)
		{
			$this->db->where('OutlineID',$OutlineID);
			$query = $this->db->get('ac_outline');
			return $query->row_array();
		}
		
		function PTL_update($OutlineID,$data)
		{
			$this->db->where('OutlineID',$OutlineID);
			$this->db->update('ac_outline',$data);
		}
	}
?>