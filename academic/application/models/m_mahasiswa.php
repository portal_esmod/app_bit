<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_mahasiswa extends CI_Model
	{
		function lookup($keyword)
		{
			$this->db->select('*')->from('ac_mahasiswa');
			$this->db->where('NA','N');
			$this->db->like('MhswID',$keyword);
			$this->db->or_like('Nama',$keyword);
			$this->db->limit(5);
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_urut($kd)
		{
			$q = $this->db->query("SELECT max(MhswID) AS LAST FROM ac_mahasiswa where MhswID like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_urut_cid($kd)
		{
			$q = $this->db->query("SELECT max(certificate_id) AS LAST FROM ac_mahasiswa where certificate_id like '$kd%'");
			return $q->row_array();
		}
		
		function PTL_all()
		{
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_all($limit,$offset)
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			return $this->db->get('ac_mahasiswa',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah_all()
		{
			$data = 0;
			$this->db->select("*");
			$this->db->from("ac_mahasiswa");
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek_all()
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_total_all()
		{
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari($cari,$limit,$offset)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			return $this->db->get('ac_mahasiswa',$limit,$offset);
		}
		
		function PTL_all_cari_jumlah($cari)
		{
			$data = 0;
			$this->db->select('*');
			$this->db->from('ac_mahasiswa');
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$hasil = $this->db->get();
			$data = $hasil->num_rows();
			return $data;
		}
		
		function PTL_all_cari_cek($cari)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_cari_total($cari)
		{
			$this->db->like('MhswID',$cari);
			$this->db->or_like('Nama',$cari);
			$this->db->or_like('NamaAyah',$cari);
			$this->db->or_like('NamaIbu',$cari);
			$this->db->or_like('Telepon',$cari);
			$this->db->or_like('Handphone',$cari);
			$this->db->or_like('Email',$cari);
			$this->db->or_like('Email2',$cari);
			if($this->session->userdata('students_filter_tahun_masuk') != "")
			{
				$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			}
			if($this->session->userdata('students_filter_jur') != "")
			{
				$this->db->where('ProgramID',$this->session->userdata('students_filter_jur'));
			}
			if(($this->session->userdata('students_filter_jur') != "") AND ($this->session->userdata('students_filter_prodi') != ""))
			{
				$this->db->where('ProdiID',$this->session->userdata('students_filter_prodi'));
			}
			if($this->session->userdata('students_filter_marketing') != "")
			{
				$this->db->where('PresenterID',$this->session->userdata('students_filter_marketing'));
			}
			if($this->session->userdata('students_filter_status') != "")
			{
				$this->db->where('StatusMhswID',$this->session->userdata('students_filter_status'));
			}
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		// function PTL_all_spesifik_aktif($cekjurusan,$cekprodi,$cektahun)
		// {
			// if($cekjurusan != "")
			// {
				// $this->db->where('ProgramID',$cekjurusan);
			// }
			// if($cekprodi != "")
			// {
				// $this->db->where('ProdiID',$cekprodi);
			// }
			// if($cektahun != "")
			// {
				// $this->db->where('TahunID',$cektahun);
			// }
			// $this->db->where('NA','N');
			// $this->db->order_by('Nama','ASC');
			// $query = $this->db->get('ac_mahasiswa');
			// return $query->result();
		// }
		
		function PTL_all_active()
		{
			$this->db->where('StatusMhswID','A');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_leave()
		{
			$this->db->where('StatusMhswID','C');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_drop_out()
		{
			$this->db->where('StatusMhswID','D');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_graduated()
		{
			$this->db->where('StatusMhswID','L');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_verification()
		{
			$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			$this->db->where('verifikasi','Y');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_verification_not()
		{
			$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			$this->db->where('verifikasi_kode','');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_verification_sent()
		{
			$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			$this->db->where('verifikasi_kode !=','');
			$this->db->where('verifikasi','N');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_all_verification_all()
		{
			$this->db->like('MhswID',$this->session->userdata('students_filter_tahun_masuk'));
			$this->db->where('StatusMhswID','A');
			$this->db->order_by('MhswID','ASC');
			$query = $this->db->get('ac_mahasiswa');
			return $query->result();
		}
		
		function PTL_tahun()
		{
			$q = $this->db->query("SELECT distinct(substring(MhswID,1,4)) AS tahun FROM ac_mahasiswa");
			return $q->result();
		}
		
		function PTL_select($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_mahasiswa');
			return $query->row_array();
		}
		
		function PTL_update($MhswID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->update('ac_mahasiswa',$data);
		}
		
		function PTL_delete($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_mahasiswa');
		}
	}
?>