<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_spesialisasi extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('al_spesialisasi');
			return $query->result();
		}
		
		function PTL_all_data()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('al_spesialisasi');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_spesialisasi',$data);
			return;
		}
		
		function PTL_select($SpesialisasiID)
		{
			$this->db->where('SpesialisasiID',$SpesialisasiID);
			$query = $this->db->get('al_spesialisasi');
			return $query->row_array();
		}
		
		function PTL_update($SpesialisasiID,$data)
		{
			$this->db->where('SpesialisasiID',$SpesialisasiID);
			$this->db->update('al_spesialisasi',$data);
		}
	}
?>