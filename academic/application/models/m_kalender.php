<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kalender extends CI_Model
	{
		// function PTL_all()
		// {
			// $this->db->where('na','N');
			// $query = $this->db->get('ac_kalender');
			// return $query->result();
		// }
		
		function PTL_all_active()
		{
			$this->db->where('na','N');
			$this->db->order_by('tanggal_mulai','DESC');
			$query = $this->db->get('ac_kalender');
			return $query->result();
		}
		
		function PTL_all_tahun_ini()
		{
			$h = "-7";
			$hm = $h * 60;
			$ms = $hm * 60;
			$tahun = gmdate("Y", time()-($ms));
			$this->db->like('tanggal_mulai',$tahun);
			$this->db->where('na','N');
			$query = $this->db->get('ac_kalender');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_kalender',$data);
			return;
		}
		
		function PTL_select($id_kalender)
		{
			$this->db->where('id_kalender',$id_kalender);
			$query = $this->db->get('ac_kalender');
			return $query->row_array();
		}
		
		function PTL_update($id_kalender,$data)
		{
			$this->db->where('id_kalender',$id_kalender);
			$this->db->update('ac_kalender',$data);
		}
		
		function PTL_update_delete_all($na,$data)
		{
			$this->db->where('na',$na);
			$this->db->update('ac_kalender',$data);
		}
	}
?>