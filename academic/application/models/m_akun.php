<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_akun extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function lookup_nama($keyword)
		{
			$this->db->where('na','N');
			$this->db->like('nama',$keyword);
			$query = $this->db->get('dv_akun',10);
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akun');
			return $query->result();
		}
		
		function PTL_all_akademik()
		{
			$this->db->where('na','N');
			$this->db->like('akses','ACADEMIC');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akun');
			return $query->result();
		}
		
		function PTL_all_marketing()
		{
			$this->db->where('na','N');
			$this->db->like('akses','MARKETING');
			$this->db->order_by('id_akun','DESC');
			$query = $this->db->get('dv_akun');
			return $query->result();
		}
		
		function PTL_all_send_notification()
		{
			$this->db->where('id_departemen','3');
			$this->db->where('id_jabatan !=','14');
			$this->db->where('id_jabatan !=','19');
			$this->db->where('id_jabatan !=','20');
			$this->db->order_by('nama','ASC');
			$query = $this->db->get('dv_akun');
			return $query->result();
		}
		
		function PTL_cek_status($email)
		{
			$this->db->where('email',$email);
			$query = $this->db->get('dv_akun');
			return $query->row_array();
		}
		
		function PTL_select($id_akun)
		{
			$this->db->where('id_akun',$id_akun);
			$query = $this->db->get('dv_akun');
			return $query->row_array();
		}
		
		function PTL_select_dosen($DosenID)
		{
			$this->db->where('DosenID',$DosenID);
			$query = $this->db->get('dv_akun');
			return $query->row_array();
		}
		
		function PTL_update($id_akun,$data)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->update('dv_akun',$data);
		}
		
		function PTL_update_dosen($DosenID,$data)
		{
			$this->db->where('DosenID',$DosenID);
			$this->db->update('dv_akun',$data);
		}
		
		// function PTL_insert($data)
		// {
			// $this->db->insert('dv_akun',$data);
			// return;
		// }
		
		// function PTL_jumlah_lecturer()
		// {
			// $q = $this->db->query("SELECT COUNT(akses) AS lecturer FROM dv_akun WHERE akses = 'ACADEMIC LECTURER'");
			// return $q->row_array();
		// }
		
		// function PTL_jumlah_staff()
		// {
			// $q = $this->db->query("SELECT COUNT(akses) AS staff FROM dv_akun WHERE akses != 'ACADEMIC LECTURER' AND akses != 'STUDENT'");
			// return $q->row_array();
		// }
		
		// function PTL_jumlah_student()
		// {
			// $q = $this->db->query("SELECT COUNT(akses) AS student FROM dv_akun WHERE akses = 'STUDENT'");
			// return $q->row_array();
		// }
	}
?>