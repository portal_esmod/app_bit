<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_catatan extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->where('status','UNREAD');
			$this->db->order_by('tanggal_posting','DESC');
			$query = $this->db->get('dv_catatan');
			return $query->result();
		}
		
		function PTL_all_list()
		{
			$this->db->where('na','N');
			$this->db->order_by('tanggal_posting','DESC');
			$query = $this->db->get('dv_catatan');
			return $query->result();
		}
		
		function PTL_all_pdf()
		{
			$this->db->where('na','N');
			$this->db->order_by('tanggal_posting','ASC');
			$query = $this->db->get('dv_catatan');
			return $query->result();
		}
		
		function PTL_tahun()
		{
			$q = $this->db->query("SELECT distinct(substring(tanggal_posting,1,4)) AS tahun FROM dv_catatan");
			return $q->result();
		}
		
		function PTL_input($datacat)
		{
			$query = $this->db->insert('dv_catatan',$datacat);
			return;
		}
		
		function PTL_select($id_catatan)
		{
			$this->db->where('id_catatan',$id_catatan);
			$query = $this->db->get('dv_catatan');
			return $query->row_array();
		}
		
		function PTL_select_b_t($ses_bulan,$ses_tahun)
		{
			if($ses_bulan != "")
			{
				$this->db->like('tanggal_posting',$ses_bulan);
			}
			if($ses_tahun != "")
			{
				$this->db->like('tanggal_posting',$ses_tahun);
			}
			$this->db->order_by('tanggal_posting','DESC');
			$query = $this->db->get('dv_catatan');
			return $query->result();
		}
		
		function PTL_select_b_t_pdf($ses_bulan,$ses_tahun)
		{
			if($ses_bulan != "")
			{
				$this->db->like('tanggal_posting',$ses_bulan);
			}
			if($ses_tahun != "")
			{
				$this->db->like('tanggal_posting',$ses_tahun);
			}
			$this->db->order_by('tanggal_posting','ASC');
			$query = $this->db->get('dv_catatan');
			return $query->result();
		}
		
		function PTL_update($id_catatan,$data)
		{
			$this->db->where('id_catatan',$id_catatan);
			$this->db->update('dv_catatan',$data);
		}
	}
?>