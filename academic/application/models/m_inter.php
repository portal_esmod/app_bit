<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_inter extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('WawancaraID','ASC');
			$query = $this->db->get('al_wawancara');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('al_wawancara',$data);
			return;
		}
		
		function PTL_select($WawancaraID)
		{
			$this->db->where('WawancaraID',$WawancaraID);
			$query = $this->db->get('al_wawancara');
			return $query->row_array();
		}
		
		function PTL_update($WawancaraID,$data)
		{
			$this->db->where('WawancaraID',$WawancaraID);
			$this->db->update('al_wawancara',$data);
		}
	}
?>