<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kampus extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_kampus');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$query = $this->db->get('ac_kampus');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_kampus',$data);
			return;
		}
		
		function PTL_select($KampusID)
		{
			$this->db->where('KampusID',$KampusID);
			$query = $this->db->get('ac_kampus');
			return $query->row_array();
		}
		
		function PTL_update($KampusID,$data)
		{
			$this->db->where('KampusID',$KampusID);
			$this->db->update('ac_kampus',$data);
		}
	}
?>