<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_notifikasi extends CI_Model
	{
		function PTL_all_select($id_akun)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->where('na','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_notifikasi');
			return $query->result();
		}
		
		function PTL_all_select_unread($id_akun)
		{
			$this->db->where('id_akun',$id_akun);
			$this->db->where('tanggal_baca','');
			$this->db->where('na','N');
			$this->db->order_by('tanggal_buat','DESC');
			$query = $this->db->get('ac_notifikasi');
			return $query->result();
		}
		
		function PTL_insert($datanotif)
		{
			$this->db->insert('ac_notifikasi',$datanotif);
			return;
		}
		
		function PTL_select($id_notifikasi)
		{
			$this->db->where('id_notifikasi',$id_notifikasi);
			$query = $this->db->get('ac_notifikasi');
			return $query->row_array();
		}
		
		function PTL_update($id_notifikasi,$datanotif)
		{
			$this->db->where('id_notifikasi',$id_notifikasi);
			$this->db->update('ac_notifikasi',$datanotif);
		}
	}
?>