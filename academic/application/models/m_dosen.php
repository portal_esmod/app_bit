<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_dosen extends CI_Model
	{
		function lookup_dosen($keyword)
		{
			$this->db->where('NA','N');
			$this->db->like('Nama',$keyword);
			$query = $this->db->get('ac_dosen',10);
			return $query->result();
		}
		
		function PTL_all()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_dosen');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_dosen',$data);
			return;
		}
		
		function PTL_select($DosenID)
		{
			$this->db->where('Login',$DosenID);
			$query = $this->db->get('ac_dosen');
			return $query->row_array();
		}
		
		function PTL_select_nama($Nama)
		{
			$this->db->where('Nama',$Nama);
			$query = $this->db->get('ac_dosen');
			return $query->row_array();
		}
		
		function PTL_update($DosenID,$data)
		{
			$this->db->where('Login',$DosenID);
			$this->db->update('ac_dosen',$data);
		}
	}
?>