<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_subjek_grup extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_subjek_group');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$query = $this->db->get('ac_subjek_group');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_subjek_group',$data);
			return;
		}
		
		function PTL_select($SubjekGroupID)
		{
			$this->db->where('SubjekGroupID',$SubjekGroupID);
			$query = $this->db->get('ac_subjek_group');
			return $query->row_array();
		}
		
		function PTL_update($SubjekGroupID,$data)
		{
			$this->db->where('SubjekGroupID',$SubjekGroupID);
			$this->db->update('ac_subjek_group',$data);
		}
	}
?>