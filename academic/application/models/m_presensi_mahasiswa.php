<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_presensi_mahasiswa extends CI_Model
	{
		function PTL_all_lecturer_expired($PresensiID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('JenisPresensiID','');
			$this->db->where('NA','N');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_excuse($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','E');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_sick($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','S');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_absent($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','A');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_presensi_late($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('JenisPresensiID','L');
			$this->db->where('NA','N');
			$this->db->order_by('PresensiID','ASC');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_sum($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->result();
		}
		
		function PTL_all_select($PresensiID)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_presensi_mhsw as a');
			$this->db->where('a.PresensiID',$PresensiID);
			$this->db->where('a.NA','N');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_all_select_removed($PresensiID)
		{
			$this->db->select('a.*, b.Nama');
			$this->db->from('ac_presensi_mhsw as a');
			$this->db->where('a.PresensiID',$PresensiID);
			$this->db->where('a.Removed','Y');
			$this->db->join('ac_mahasiswa as b','b.MhswID=a.MhswID');
			$this->db->order_by('b.Nama','ASC');
			$query = $this->db->get();
			return $query->result();
		}
		
		function PTL_select_att($JadwalID,$PresensiID,$MhswID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MhswID',$MhswID);
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->row_array();
		}
		
		function PTL_select_scoring($PresensiID,$MhswID)
		{
			$this->db->where('PresensiID',$PresensiID);
			$this->db->where('MhswID',$MhswID);
			$this->db->where('Removed','N');
			$query = $this->db->get('ac_presensi_mhsw');
			return $query->row_array();
		}
		
		function PTL_insert($data_att)
		{
			$this->db->insert('ac_presensi_mhsw',$data_att);
			return;
		}
		
		function PTL_update($PresensiMhswID,$data)
		{
			$this->db->where('PresensiMhswID',$PresensiMhswID);
			$this->db->update('ac_presensi_mhsw',$data);
		}
		
		function PTL_delete($PresensiMhswID)
		{
			$this->db->where('PresensiMhswID',$PresensiMhswID);
			$this->db->delete('ac_presensi_mhsw');
		}
		
		function PTL_delete_jadwal($JadwalID)
		{
			$this->db->where('JadwalID',$JadwalID);
			$this->db->delete('ac_presensi_mhsw');
		}
		
		function PTL_delete_krs($KRSID,$MhswID)
		{
			$this->db->where('KRSID',$KRSID);
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_presensi_mhsw');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('ac_presensi_mhsw');
		}
	}
?>