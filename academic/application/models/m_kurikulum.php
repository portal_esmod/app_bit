<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kurikulum extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('KurikulumKode','ASC');
			$query = $this->db->get('ac_kurikulum');
			return $query->result();
		}
		
		function PTL_all_active()
		{
			$this->db->where('NA','N');
			$query = $this->db->get('ac_kurikulum');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_kurikulum',$data);
			return;
		}
		
		function PTL_select($KurikulumID)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$query = $this->db->get('ac_kurikulum');
			return $query->row_array();
		}
		
		function PTL_update($KurikulumID,$data)
		{
			$this->db->where('KurikulumID',$KurikulumID);
			$this->db->update('ac_kurikulum',$data);
		}
	}
?>