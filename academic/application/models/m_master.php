<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_master extends CI_Model
	{
		function PTL_bipot_mhsw_select($PMBID)
		{
			$this->db->where('PMBID',$PMBID);
			$this->db->where('NA','N');
			$query = $this->db->get('fn_bipotmhsw');
			return $query->result();
		}
		
		function PTL_pym_bipotmhsw_update_delete($MhswID,$TahunID,$data)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('TahunID',$TahunID);
			$this->db->update('fn_bipotmhsw',$data);
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('fn_bipotmhsw');
		}
	}
?>