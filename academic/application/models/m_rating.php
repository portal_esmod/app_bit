<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_rating extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		
		function PTL_all_spesifik($id_rating_question,$DosenID)
		{
			$this->db->where('id_rating_question',$id_rating_question);
			$this->db->where('DosenID',$DosenID);
			$this->db->where('na','N');
			$query = $this->db->get('fb_rating');
			return $query->result();
		}
		
		function PTL_select_sum_dosen($id_rating_question,$DosenID)
		{
			$q = $this->db->query("SELECT sum(rate) AS TotalRate FROM fb_rating where id_rating_question='$id_rating_question'
								AND DosenID ='$DosenID' AND na='N'");
			return $q->row_array();
		}
	}
?>