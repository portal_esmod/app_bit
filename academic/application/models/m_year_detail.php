<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_year_detail extends CI_Model
	{
		function PTL_all_spesifik($TahunID,$ProdiID)
		{
			$this->db->where('TahunID',$TahunID);
			$this->db->where('ProdiID',$ProdiID);
			$this->db->where('na','N');
			$this->db->order_by('tanggal_mulai','ASC');
			$query = $this->db->get('ac_tahun_detail');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_tahun_detail',$data);
			return;
		}
		
		function PTL_select($id)
		{
			$this->db->where('id',$id);
			$query = $this->db->get('ac_tahun_detail');
			return $query->row_array();
		}
		
		function PTL_update($id,$data)
		{
			$this->db->where('id',$id);
			$this->db->update('ac_tahun_detail',$data);
		}
	}
?>