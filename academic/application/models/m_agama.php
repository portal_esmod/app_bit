<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_agama extends CI_Model
	{
		function PTL_all_active()
		{
			$this->db->where('NA','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('al_agama');
			return $query->result();
		}
	}
?>