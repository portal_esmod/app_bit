<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_identity extends CI_Model
	{
		function PTL_all()
		{
			$query = $this->db->get('ac_identitas');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_identitas',$data);
			return;
		}
		
		function PTL_select($Kode)
		{
			$this->db->where('Kode',$Kode);
			$query = $this->db->get('ac_identitas');
			return $query->row_array();
		}
		
		function PTL_update($Kode,$data)
		{
			$this->db->where('Kode',$Kode);
			$this->db->update('ac_identitas',$data);
		}
	}
?>