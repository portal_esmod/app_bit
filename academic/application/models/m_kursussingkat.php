<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_kursussingkat extends CI_Model
	{
		function PTL_all()
		{
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_kursussingkat');
			return $query->result();
		}
		
		function PTL_all_list()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_kursussingkat');
			return $query->result();
		}
		
		function PTL_select($KursusSingkatID)
		{
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$query = $this->db->get('ac_kursussingkat');
			return $query->row_array();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_kursussingkat',$data);
			return;
		}
		
		function PTL_update($KursusSingkatID,$data)
		{
			$this->db->where('KursusSingkatID',$KursusSingkatID);
			$this->db->update('ac_kursussingkat',$data);
		}
	}
?>