<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_jenjang extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_jenjang');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_jenjang',$data);
			return;
		}
		
		function PTL_select($JenjangID)
		{
			$this->db->where('JenjangID',$JenjangID);
			$query = $this->db->get('ac_jenjang');
			return $query->row_array();
		}
		
		function PTL_update($JenjangID,$data)
		{
			$this->db->where('JenjangID',$JenjangID);
			$this->db->update('ac_jenjang',$data);
		}
	}
?>