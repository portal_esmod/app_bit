<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_subjek_percentage extends CI_Model
	{
		function PTL_all_subjek($SubjekID)
		{
			$this->db->where('SubjekID',$SubjekID);
			$this->db->where('na','N');
			$this->db->order_by('Nama','ASC');
			$query = $this->db->get('ac_subjek_percentage');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_subjek_percentage',$data);
			return;
		}
		
		function PTL_select($id_subjek_percentage)
		{
			$this->db->where('id_subjek_percentage',$id_subjek_percentage);
			$query = $this->db->get('ac_subjek_percentage');
			return $query->row_array();
		}
		
		function PTL_update($id_subjek_percentage,$data)
		{
			$this->db->where('id_subjek_percentage',$id_subjek_percentage);
			$this->db->update('ac_subjek_percentage',$data);
		}
	}
?>