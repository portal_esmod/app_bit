<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_bayar extends CI_Model
	{
		function PTL_pym_all_select($MhswID,$BIPOTID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->where('BIPOTID',$BIPOTID);
			$this->db->order_by('BayarMhswID','DESC');
			$query = $this->db->get('fn_bayarmhsw');
			return $query->result();
		}
		
		function PTL_delete($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->delete('fn_bayarmhsw');
		}
		
		function PTL_delete2($BayarMhswID)
		{
			$this->db->where('BayarMhswID',$BayarMhswID);
			$this->db->delete('fn_bayarmhsw2');
		}
		
		function PTL_delete_mahasiswa($MhswID)
		{
			$this->db->where('MhswID',$MhswID);
			$this->db->delete('fn_bayarmhsw');
		}
	}
?>