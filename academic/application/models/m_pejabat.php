<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class M_pejabat extends CI_Model
	{
		function PTL_all()
		{
			$this->db->order_by('PejabatID','ASC');
			$query = $this->db->get('ac_pejabat');
			return $query->result();
		}
		
		function PTL_insert($data)
		{
			$this->db->insert('ac_pejabat',$data);
			return;
		}
		
		function PTL_select($PejabatID)
		{
			$this->db->where('PejabatID',$PejabatID);
			$query = $this->db->get('ac_pejabat');
			return $query->row_array();
		}
		
		function PTL_update($PejabatID,$data)
		{
			$this->db->where('PejabatID',$PejabatID);
			$this->db->update('ac_pejabat',$data);
		}
	}
?>